<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Note de version 


Codage de la version du corpus


Les deux premiers chiffres font référence à la version des programmes d'analyses

#### version 3.5

Étapes du traitement automatique :

1.	Segmentation en mots (stable)
2.	Identification des noyaux syllabiques (stable)
3.	Traitement des diérèses (stable)
4.	Traitement des "e" instables (stable)
5.	Calcul de la longueur métrique des vers (stable)
6.	Détermination du profil métrique du poème et calcul du mètre des vers (stable)
7.	Appariement des vers en rimes, détermination des schémas de rimes et de la forme globale (en développement, stade avancé)
8.	Traitement de l'extension des rimes et calcul de la PGTC (en développement)
9.	Traitement de la qualité des rimes : rimes classiques et rimes non classiques (en développement)
10.	Traitement de la ponctuométrie (stable)

#### versions 3.5.0, 3.5.1, 3.5.2 ... 
Le dernier chiffre fait référence à la version du corpus;
Le numéro est incrémenté à chaque mise en jour globale avec ajout de nouveaux textes.






