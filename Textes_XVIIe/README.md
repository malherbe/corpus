
<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>
 
### Corpus de textes du XVII<sup>e</sup> en orthographe d'époque

L'orthographe des textes du XVII<sup>e</sup> avec une typographie modernisée ou non ‒ présence ou non de s long (ſ) et de ligatures (&), notamment ‒ est incompatible avec le jeu de règles d'identification des noyaux syllabiques du programme d'analyse automatique ; problème de reconnaissance des voyelles dans des mots tels que : _cheuaux_ ou _frere_). Ce corpus a permis de tester un second jeu de règles.


  
