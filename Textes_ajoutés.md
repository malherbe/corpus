<img src="Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>


### TEXTES AJOUTÉS

#### Corpus Malherbe 3.4.1

25-11-2020

- Ajout du texte : MND_4, louis MÉNARD, FLEURS DE TOUTES SAISONS,SONNETS
- Ajout du texte : MAL_2, Stéphane MALLARMÉ, VERS DE CIRCONSTANCE
- Ajout du texte : MAE_1, Maurice MAETERLINCK, SERRES CHAUDES
- Ajout du texte : MAE_2, Maurice MAETERLINCK, QUINZE CHANSONS

#### Corpus Malherbe 3.4.2

- Ajout du texte : DLR_6, Lucie DELARUE-MARDRUS, SOUFFLES DE TEMPÊTE
- Ajout du texte : DLR_7, Lucie DELARUE-MARDRUS, A MAMAN
- Ajout du texte : DLR_8, Lucie DELARUE-MARDRUS, POÈMES MIGNONS
- Ajout du texte : DLR_9, Lucie DELARUE-MARDRUS, LES SEPT DOULEURS D'OCTOBRE
- Ajout du texte : DLR_10, Lucie DELARUE-MARDRUS, MORT ET PRINTEMPS
- Ajout du texte : DLR_11, Lucie DELARUE-MARDRUS, TEMPS PRÉSENTS
- Ajout du texte : DLR_12, Lucie DELARUE-MARDRUS, NOS SECRÈTES AMOURS
- Ajout du texte : DLR_13, Lucie DELARUE-MARDRUS, POÈMES INEDITS
- Ajout du texte : DLR_14, Lucie DELARUE-MARDRUS, LA PRÊTESSE DE TANIT
- Ajout du texte : MON_1, Robert de MONTESQUIOU, LES HORTENSIAS BLEUS
- Ajout du texte : DAU_1, Alphonse DAUDET, LES AMOUREUSES
- Ajout du texte : VEN_1, Henri VENDEL, CHANTS DU COUVRE-FEU
- Ajout du texte : TRA_1, Gabriel TRARIEUX, CONFITEOR
- Ajout du texte : BUS_1, Alfred BUSQUET, POÉSIES
- Ajout du texte : DSA_1, Alfred des ESSARTS, LA COMÉDIE DU MONDE
- Ajout du texte : AGT_1, Daniel STERN (Marie d'Agoult), POÉSIES
- Ajout du texte : HRV_1, Henriette HERVÉ, DILECTION, 1925
- Ajout du texte : DSR_1, DESROYS, Le Tabac
- Ajout du texte : DSR_2, DESROYS, La Géométrie en vers techniques, 1801
- Ajout du texte : LSR_1, Daniel LESUEUR, Poésies, 1896
- Ajout du texte : CMS_1, Georges CAMUSET, Les Sonnets du docteur, 1884
- Ajout du texte : MLT_1, Charles MONSELET, Les Vignes du Seigneur, 1854
- Ajout du texte : LAP_1, Victore de LAPRADE, LES PARFUMS DE MAGDELEINE, 1839
- Ajout du texte : LAP_2, Victore de LAPRADE, LA COLÈRE DE JÉSUS, 1840
- Ajout du texte : LAP_3, Victore de LAPRADE, PSYCHÉ, 1841
- Ajout du texte : LAP_4, Victore de LAPRADE, ODES ET POÈMES, 1844
- Ajout du texte : LAP_5, Victore de LAPRADE, POÈMES ÉVANGÉLIQUES, 1852
- Ajout du texte : LAP_6, Victore de LAPRADE, LES SYMPHONIES, 1855
- Ajout du texte : LAP_7, Victore de LAPRADE, IDYLLES HÉROÏQUES, 1858
- Ajout du texte : LAP_8, Victore de LAPRADE, LES VOIX DU SILENCE, 1864
- Ajout du texte : LAP_9, Victore de LAPRADE, PERNETTE, 1870
- Ajout du texte : LAP_10, Victore de LAPRADE, HARMODIUS, 1870
- Ajout du texte : LAP_11, Victore de LAPRADE, POÈMES CIVIQUES, 1873
- Ajout du texte : LAP_12, Victore de LAPRADE, TRIBUNS ET COURTISANS, 1875
- Ajout du texte : LAP_13, Victore de LAPRADE, LE LIVRE D'UN PÈRE, 1877
- Ajout du texte : LAP_14, Victore de LAPRADE, VARIA, 1844-1879
- Ajout du texte : LAP_15, Victore de LAPRADE, LE LIVRE DES ADIEUX, 1874-1880
- Ajout du texte : PEH_1, Émile PÉHANT, SONNETS ET POÉSIES, 1875
- Ajout du texte : CRM_1, Maurice CARÊME, BRABANT, 1967 (texte sous droits)


#### Corpus Malherbe 3.5.0

Juin 2021

- Ajout du texte : EON_1, Francis ÉON, Autre livre de Monelle, 1939
- Ajout du texte : EON_2, Francis ÉON, Le tiers livre de Monelle, 1944
- Ajout du texte : FRN_1, FRANC-NOHAIN, Les Inattentions et Sollicitudes, 1894
- Ajout du texte : TAI_2, Laurent TAILHADE, Vitraux, 1891
- Ajout du texte : TAI_3, Laurent TAILHADE, À Travers les Grouins, 1899
- Ajout du texte : BRT_1, Mélanie BOUROTTE, Le sphynx au foyer, 1883

Juillet 2021

- Ajout du texte : HEC_1, Gabriel-Antoine-Joseph HÉCART, ANAGRAMMÉANA, 1867
- Ajout du texte : CAO_1, Jean Baptiste CAOUETTE, LES VOIX INTIMES, 1892
- Ajout du texte : SSA_1, Camille SAINT-SAËNS, RIMES FAMILIÈRES, 1890
- Ajout du texte : FER_1, Albert FERLAND, MÉLODIES POÉTIQUES, 1893
- Ajout du texte : FER_2, Albert FERLAND, FEMMES RÊVÉES, 1899
- Ajout du texte : FOU_1, Georges FOUREST, LA NÉGRESSE BLONDE, 1909
- Ajout du texte : VLS_1, Léonise VALOIS, FLEURS SAUVAGES, 1910
- Ajout du texte : ROS_2, Edmond ROSTAND, UN SOIR A HERNANI, 1902
- Ajout du texte : VHR_2, Émile VERHAEREN, LES MOINES, 1895
- Ajout du texte : ANO_3, Anonyme, LE PETIT-NEVEU DE GRÉCOURT OU ÉTRENNES GAILLARDES, 1782
- Ajout du texte : VAL_6, Paul VALÉRY, PIÈCES DIVERSES, 1942
- Ajout du texte : VAL_7, Paul VALÉRY, CANTATE DE NARCISSE, 1942
- Ajout du texte : MOR_3, Jean MORÉAS, LE PÈLERIN PASSIONNÉ, 1891
- Ajout du texte : MOR_4, Jean MORÉAS, ÉRIPHYLE, 1894
- Ajout du texte : MOR_5, Jean MORÉAS, LES STANCES, 1899
- Ajout du texte : MOR_6, Jean MORÉAS, IPHIGÉNIE, 1904
- Ajout du texte : COL_1, Charles COLLÉ, CHANSONS JOYEUSES, 1765
- Ajout du texte : VHR_3, Émile VERHAEREN, LES SOIRS, 1896

Septembre 2021

- Ajout du texte : BOU_2, Louis BOUILHET, POÉSIES : FESTONS ET ASTRAGALES, 1859
- Ajout du texte : BOU_3, Louis BOUILHET, DERNIÈRES CHANSONS, 1869
- Ajout du texte : MAU_1, Guy de MAUPASSANT, DES VERS, 1908
- Ajout du texte : CHF_1, Sébastien-Roch-Nicolas de CHAMFORT, ŒUVRES COMPLÈTES (poésies), 1825

Décembre 2021

- Ajout du texte : LCA_2, louis LE CARDONNEL, CARMINA SACRA,1920
- Ajout du texte : FRT_1, Charles-Théophile FERET, LE BOURDEAU DES NEUF PUCELLES, 1912
- Ajout du texte : LOZ_1, Albert LOZEAU, POÉSIES COMPLÈTES I, 1925
- Ajout du texte : LOZ_2, Albert LOZEAU, POÉSIES COMPLÈTES II, 1925
- Ajout du texte : LOZ_3, Albert LOZEAU, POÉSIES COMPLÈTES III, 1925

février 2022

- Ajout du texte : LOY_1, Charles LOYSON, LE BONHEUR DE L'ÉTUDE : discours en vers et autres poésies, 1817
- Ajout du texte : LOY_2, Charles LOYSON, ÉPÎTRE ET ÉLÉGIES, 1819
- Ajout du texte : JAR_1, Afred JARRY, LES MINUTES DE SABLE MÉMORIAL, 1894
- Ajout du texte : VIA_1, Boris VIAN, CENT SONNETS, 1944


## Corpus vaudevilles (Le rire des vers, Université de bâle)

juin 2021

- Ajout du texte : AED_1, Étienne ARAGO et Félix-Auguste DUVERT, 27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES, 1830
- Ajout du texte : BEC_1, Nicolas BRAZIER et Pierre CARMOUCHE, LES DEMOISELLES, 1830
- Ajout du texte : EVC_1, Charles-Guillaume ÉTIENNE, VARIN et Lucien DESVERGERS, ARWED, OU LES REPRÉSAILLES, 1830
- Ajout du texte : FED_1, Louis Marie FONTAN et Charles DESNOYER, ANDRÉ LE CHANSONNIER, 1830
- Ajout du texte : HON_1, Charles HONORÉ, BONARDIN DANS LA LUNE, 1830
- Ajout du texte : MVG_1, Michel MASSON, Théodore-Ferdinand de VILLENEUVE et GABRIEL, BONAPARTE À L'ÉCOLE DE BRIENNE, 1830
- Ajout du texte : RBC_1, Michel-Nicolas BALISSON DE ROUGEMONT, Nicolas BRAZIER et Frédéric DE COURCY, LES VARIÉTÉS DE 1830, 1831
- Ajout du texte : SND_1, SAINTINE, Charles NOMBRET SAINT-LAURENT et Félix-Auguste DUVERT, BONAPARTE LIEUTENANT D'ARTILLERIE, 1830
- Ajout du texte : SVD_1, SAINTINE, Théodore-Ferdinand VILLENEUVE et Charles DUPEUTY, COMMÈRE JEANNETON, 1831
- Ajout du texte : SVD_2, SAINTINE, Théodore-Ferdinand VILLENEUVE et Charles DUPEUTY, ANGÉLIQUE, 1831
- Ajout du texte : VEM_1, Théodore-Ferdinand VILLENEUVE et Michel MASSON, À PROPOS PATRIOTIQUE, 1830
- Ajout du texte : DSE_1, Félix-Auguste  DUVERT, Ernest SAINTINE et Charles-Guillaume ÉTIENNE, CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES, 1830

juin 2022
 
- Ajout du texte : BLV_1, Jean-Nicolas BOUILLY & Émile VANDERBURCH, LE BANDEAU, 1832
- Ajout du texte : BTL_1, Léon-Lévy BRUNSWICK, Mathieu Barthélemy & Victor LHÉRIE, LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS, 1832
- Ajout du texte : DCB_1, Achille d' ARTOIS & Jules CHABOT DE BOUIN, LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE, 1832
- Ajout du texte : DDC_1, Charles DUPEUTY & Frédéric de COURCY, LE GENTILHOMME,1833
- Ajout du texte : DLV_1, Félix-Auguste DUVERT & Augustin Théodore de LAUZANNE DE VAUROUSSEL, LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE, 1832
- Ajout du texte : DMP_1, Julien de MAILLAN & DUMANOIR, L'HOMME QUI BAT SA FEMME, 1832
- Ajout du texte : FCR_1, Frédéric de COURCY & Ernest JAIME, LA MÉTEMPSYCOSE, 1832
- Ajout du texte : JRL_1, Jacques ARAGO & Louis LURINE, CHABERT, 1832
- Ajout du texte : LRL_1, Gabriel de LURIEU & Ferdinand LANGLÉ, LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE, 1832
- Ajout du texte : MNS_1, Constant MÉNISSIER, BRUNE ET BLONDE , 1832
- Ajout du texte : PBC_1, René-Charles GUILBERT DE PIXÉRÉCOURT, Nicolas BRAZIER & Pierre-François-Adolphe CARMOUCHE, LE PETIT HOMME ROUGE IMITÉE DU GENRE ANGLAIS, 1832
- Ajout du texte : SCB_3, Eugène SCRIBE & Jean-François-Alfred BAYARD, CAMILLA, OU LA SŒUR ET LE FRÈRE, 1832
- Ajout du texte : SCB_2, Eugène SCRIBE & Jean-François-Alfred BAYARD, LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE, 1831
- Ajout du texte : SVB_1, Thomas SAUVAGE & Jean-François-Alfred BAYARD, LES ROUÉS, 1833
- Ajout du texte : TVL_1, Mathieu Barthélemy THOUIN & Victor LHÉRIE, L'ART DE NE PAS MONTER SA GARDE, 1832


#### Corpus Malherbe 3.6

septembre 2022

- Ajout du texte : PRO_1, Marcel PROUST, POEMES, 1871-1922
- Ajout du texte : BLA_1, Prosper BLANCHEMAIN, POÉSIES I, 1845-1858
- Ajout du texte : GAU_9, Théophile GAUTIER, Poésies de Th. Gautier qui ne figureront pas dans ses œuvres, 1873
- Ajout du texte : DHL_1, Antoinette DESHOULIÈRES, Poésies de Madame Deshoulières, 1824
- Ajout du texte : FLO_1, Jean-Pierre Claris de FLORIAN, Fables de Florian, 1793
- Ajout du texte : FLO_2, Jean-Pierre Claris de FLORIAN, Fables de Florian (compléments), 1802
- Ajout du texte : LEM_1, Pamphile LE MAY, ESSAIS POÉTIQUES, 1865
- Ajout du texte : LEM_2, Pamphile LE MAY, ÉVANGÉLINE, 1865
- Ajout du texte : LEM_3, Pamphile LE MAY, LES VENGEANCES, 1875
- Ajout du texte : LEM_4, Pamphile LE MAY, UNE GERBE, 1879
- Ajout du texte : LEM_5, Pamphile LE MAY, LA CHAINE D'OR, 1879
- Ajout du texte : LEM_6, Pamphile LE MAY, FABLES CANADIENNES, 1882-1891
- Ajout du texte : LEM_7, Pamphile LE MAY, PETITS POÈMES, 1883
- Ajout du texte : LEM_8, Pamphile LE MAY, Les Gouttelettes, 1904
- Ajout du texte : LEM_9, Pamphile LE MAY, Les Épis, 1914
- Ajout du texte : LEM_10, Pamphile LE MAY, Reflets d'antan, 1916
- Ajout du texte : VLR_1, Nina de VILLARD, Feuillets parisiens, 1885


#### Corpus Malherbe 3.6.1


- Ajout du texte : COC1, Jean COCTEAU, La Lampe d'Aladin, 1909 (texte sous droits)
- Ajout du texte : COC2, Jean COCTEAU, Le Prince frivole, 1910 (texte sous droits)


#### Corpus Malherbe 3.7

mai 2023

- Ajout du texte : GOU_1, Émile GOUDEAU, POÈMES PARISIENS, 1897
- Ajout du texte : TRA_2, Gabriel TRARIEUX, Le Portique, 1909
- Ajout du texte : COC_1, Jean COCTEAU, La Lampe d'Aladin, 1909
- Ajout du texte : COC_2, Jean COCTEAU, Le Prince Frivole, 1910
- Ajout du texte : COC_3, Jean COCTEAU, La Danse de Sophocle, 1912
- Ajout du texte : HAN_1, Théodore HANNON, Rimes de joie, 1881
- Ajout du texte : HAN_2, Théodore HANNON, AU PAYS DE MANNEKEN-PIS, 1883
- Ajout du texte : HAN_3, Théodore HANNON, Pierrot macabre, 1886
- Ajout du texte : HAN_4, Théodore HANNON, La Valkyrigole, 1887
- Ajout du texte : HAN_5, Théodore HANNON, UNE MESSE DE MINUIT, 1888
- Ajout du texte : HAN_6, Théodore HANNON, Smylis, 1891
- Ajout du texte : HAN_7, Théodore HANNON, AU CLAIR DE LA DUNE, 1909
- Ajout du texte : HAN_8, Théodore HANNON, LE MIRLITON PRIAPIQUE, 1883
- Ajout du texte : GIR_1, Delphine GAY, Le dévouement des médecins…, 1822
- Ajout du texte : RIV_1, André RIVOIRE, Les Vierges, 1914
- Ajout du texte : RIV_2, André RIVOIRE, Berthe aux grands pieds, 1899
- Ajout du texte : RIV_3, André RIVOIRE, Le Songe de l'amour, 1909
- Ajout du texte : RIV_4, André RIVOIRE, Le Chemin de l'oubli, 1909
- Ajout du texte : RIV_5, André RIVOIRE, Le Plaisir des jours, 1914
- Ajout du texte : STB_1, Charles Augustin SAINTE-BEUVE, POÉSIES DE SAINTE-BEUVE - PREMIÈRE PARTIE, 1863
- Ajout du texte : STB_2, Charles Augustin SAINTE-BEUVE, POÉSIES DE SAINTE-BEUVE - SECONDE PARTIE, 1863
- Ajout du texte : FOU_2, Georges FOUREST, Le Géranium ovipare, 1935
- Ajout du texte : REG_3, Henri de RÉGNIER, Premiers poèmes, 1907
- Ajout du texte : SPR_1, SAINT-POL-ROUX, Anciennetés, 1903
- Ajout du texte : POR_1, Georges de PORTO-RICHE, PRIMA VERBA, 1872
- Ajout du texte : POR_2, Georges de PORTO-RICHE, Pommes d'Ève, 1874
- Ajout du texte : POR_3, Georges de PORTO-RICHE, Tout n’est pas rose, 1877
- Ajout du texte : POR_4, Georges de PORTO-RICHE, BONHEUR MANQUÉ, 1889
- Ajout du texte : POR_5, Georges de PORTO-RICHE, Quelques vers d'autrefois, 1915
- Ajout du texte : NER_4, Gérard de NERVAL, Petits châteaux de Bohême, 1853
- Ajout du texte : NER_5, Gérard de NERVAL, ŒUVRES COMPLÈTES (POÉSIES), 1877
- Ajout du texte : JBR_1, Jean-Baptiste ROUSSEAU, ŒUVRES, 1712
- Ajout du texte : PON_2, Raoul PONCHON, LA MUSE GAILLARDE, 1939
- Ajout du texte : PON_3, Raoul PONCHON, LA MUSE VAGABONDE, 1947
- Ajout du texte : PON_4, Raoul PONCHON, LA MUSE FRONDEUSE, 1971
- Ajout du texte : PON_5, Raoul PONCHON, BOUTEILLE ET VÉNUS, 1933
- Ajout du texte : BRD_1, Tristan BERNARD, SOIXANTE ANNÉES DE LYRISME INTERMITTENT, 1945


#### Corpus Malherbe 3.7.1

septembre 2023

- Ajout du texte : BIR_1, Hercule BIRAT, CHANT COMMUNISTE PAR UN HOMME QUI NE L’EST GUÈRE, 1849
- Ajout du texte : BIR_2, Hercule BIRAT, POÉSIES NARBONNAISES EN FRANÇAIS OU EN PATOIS, TOME PREMIER, 1860
- Ajout du texte : BIR_3, Hercule BIRAT, POÉSIES NARBONNAISES EN FRANÇAIS OU EN PATOIS, TOME SECOND, 1860
- Ajout du texte : BIR_4, Hercule BIRAT, Une Nouvelle Étoile télescopique, 1867
- Ajout du texte : ARE_1, Paul ARÈNE, Poésies, 1900
- Ajout du texte : TIS_1, Clair TISSEUR, PAUCA PAUCIS, 1894
- Ajout du texte : DAR_1, Pierre DARU, ŒUVRES D’HORACE - TRADUITES EN VERS, 1796
- Ajout du texte : DAR_2, Pierre DARU, LA CLÉOPÉDIE OU THÉORIE DES RÉPUTATIONS LITTÉRAIRES, 1800
- Ajout du texte : DAR_3, Pierre DARU, ÉPÎTRE A JACQUES DELILLE, 1801
- Ajout du texte : DAR_4, Pierre DARU, ÉPÎTRE A M. LE DUC DE LA ROCHEFOUCAULD, 1824
- Ajout du texte : DAR_5, Pierre DARU, DISCOURS EN VERS SUR LES FACULTÉS DE L’HOMME, 1825
- Ajout du texte : DAR_6, Pierre DARU, L’ASTRONOMIE - POÈME EN SIX CHANTS, 1830
- Ajout du texte : MLT_2, Charles MONSELET, Les Potages FEYEUX, 1869
- Ajout du texte : LMN_1, Louis LEMERCIER DE NEUVILLE, VERS DE VASE, 1906
- Ajout du texte : AGL_1, Auguste ANGELLIER, À L’AMIE PERDUE, 1896


#### Corpus Malherbe 3.7.2

octobre 2023

- Ajout du texte : PEH_1, Émile PÉHANT, SONNETS ET POÉSIES, 1875
- Ajout du texte : PDB_1, Pierre de BOUCHAUD, MÉLODIES POÉTIQUES, 1890
- Ajout du texte : PDB_2, Pierre de BOUCHAUD, Rythmes et Nombres, 1895
- Ajout du texte : PDB_3, Pierre de BOUCHAUD, Les Mirages, 1897
- Ajout du texte : PDB_4, Pierre de BOUCHAUD, LE RECUEIL DES SOUVENIRS, 1899
- Ajout du texte : PDB_5, Pierre de BOUCHAUD, LES HEURES DE LA MUSE - I, 1902
- Ajout du texte : PDB_6, Pierre de BOUCHAUD, LES HEURES DE LA MUSE - II, 1902
- Ajout du texte : PDB_7, Pierre de BOUCHAUD, Les Lauriers de l’Olympe, 1907
- Ajout du texte : PDB_8, Pierre de BOUCHAUD, Le Luth doré, 1911
- Ajout du texte : PDB_9, Pierre de BOUCHAUD, La France éternelle, 1918
- Ajout du texte : PDB_10, Pierre de BOUCHAUD, Hymne à la Victoire, 1919
- Ajout du texte : PDB_11, Pierre de BOUCHAUD, LES JOURS REFLÉTÉS, 1925
- Ajout du texte : PDB_12, Pierre de BOUCHAUD, Ode à Francis ÉON, 1925
- Ajout du texte : PDB_13, Pierre de BOUCHAUD, LES FÊTES DE LA VICTOIRE, 1919
- Ajout du texte : MAT_1, Gustave MATHIEU, PARFUMS, CHANTS ET COULEURS, 1878


#### Corpus Malherbe 3.7.3

janvier 2024

- Ajout du texte : DPT_1, Pierre DUPONT, MUSE POPULAIRE, 1875
- Ajout du texte : MRR_1, Paul MARROT, LE CHEMIN DU RIRE, 1881
- Ajout du texte : DON_1, Maurice DONNAY, AUTOUR DU CHAT NOIR, 1926
- Ajout du texte : BER_1, Pierre-Jean de BÉRANGER, ŒUVRES COMPLÈTES DE BÉRANGER, Tome I, 1839
- Ajout du texte : BER_2, Pierre-Jean de BÉRANGER, ŒUVRES COMPLÈTES DE BÉRANGER, Tome II, 1839
- Ajout du texte : BER_3, Pierre-Jean de BÉRANGER, ŒUVRES COMPLÈTES DE BÉRANGER, Tome III, 1839
- Ajout du texte : ROL_3, Maurice ROLLINAT, L’ABÎME, 1886
- Ajout du texte : ROL_4, Maurice ROLLINAT, LA NATURE, 1892
- Ajout du texte : ROL_5, Maurice ROLLINAT, LES APPARITIONS, 1896
- Ajout du texte : ROL_6, Maurice ROLLINAT, PAYSAGES ET PAYSANS, 1899
- Ajout du texte : ROL_7, Maurice ROLLINAT, LES BÊTES, 1911
- Ajout du texte : ROL_8, Maurice ROLLINAT, FIN D’ŒUVRE, 1919
- Ajout du texte : RAM_1, Jean RAMEAU, Poèmes fantasques, 1883


#### Corpus Malherbe 3.7.4

juillet 2024

## Corpus de Romain Benini (Chansons du XIXe siècle)

- Ajout du texte : ALA_1, Auguste ALAIS, 1848
- Ajout du texte : ARN_1, Constant ARNOULD, 1851
- Ajout du texte : BAI_1, Eugène BAILLET, 1856
- Ajout du texte : BAS_1, Victor BASIÈRE, 1832-1849
- Ajout du texte : CAT_1, Apollinaire CATALA, 1849
- Ajout du texte : DAL_1, Alexis DALÈS, 1850
- Ajout du texte : DEA_1, Adrien DELAIRE, 1849
- Ajout du texte : DEM_1, Hippolyte DEMANET, 1851
- Ajout du texte : DEV_1, Gustave DEVIEU, 1890
- Ajout du texte : DIO_1, Alexis DIOT, 1848
- Ajout du texte : DUB_1, Charles DUBUISSON, 1849
- Ajout du texte : DPT_2, Pierre DUPONT, 1851
- Ajout du texte : DUR_1, Louis-Charles DURAND, 1850
- Ajout du texte : FES_1, Louis FESTEAU, 1850
- Ajout du texte : GLL_1, Charles GILLE, 1851
- Ajout du texte : GRN_1, Alexandre GUÉRIN, 1850
- Ajout du texte : GUI_1, Auguste GUIGUE, 1850
- Ajout du texte : GLB_1, Anaxagore GUILBERT, 1848
- Ajout du texte : GNG_1, Jean-Charles GUINGAND, 1850
- Ajout du texte : GMN_1, L. C. (Léon GUILLEMIN), 1851
- Ajout du texte : LER_1, Gustave LEROY, 1851
- Ajout du texte : LNL_1, Auguste LOYNEL, 1848
- Ajout du texte : MOU_1, MOUSSEUX, 1848
- Ajout du texte : MUL_1, Gabriel MULOT, 1848
- Ajout du texte : DFN_1, Damaris-Florentin NOEL, 1848
- Ajout du texte : PIS_1, Alexandre PISTER, 1848
- Ajout du texte : RAB_1, Victor RABINEAU, 1848
- Ajout du texte : ROB_1, Benjamin ROBEQUIN, 1848
- Ajout du texte : SER_1, Urbain SERRE, 1850
- Ajout du texte : VLL_1, Romain VALLADIER, 1850
- Ajout du texte : VTL_1, Louis VOITELAIN, 1849

#### Corpus Malherbe 3.7.5

février 2025

- Ajout du texte : MLB_1, François de MALHERBE, 1630
- Ajout du texte : MLB_2, François de MALHERBE, 1630
- Ajout du texte : MRG_1, Mathurin RÉGNIER, 1613
- Ajout du texte : RCN_1, Honorat Bueil de RACAN, 1660
- Ajout du texte : RCN_2, Honorat Bueil de RACAN, 1660
- Ajout du texte : STA_1, Marc-Antoine Girard de SAINT-AMANT, 1651-1659
- Ajout du texte : STA_2, Marc-Antoine Girard de SAINT-AMANT, 1651-1659
- Ajout du texte : STA_3, Marc-Antoine Girard de SAINT-AMANT, 1638
- Ajout du texte : TDV_1, Théophile de VIAU, 1632
- Ajout du texte : TDV_2, Théophile de VIAU, 1632
