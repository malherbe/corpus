ETI
VRN
DVR
———
EVC_1

Charles-Guillaume ÉTIENNE
1777-1845

VARIN
1798-1869

Lucien DESVERGERS
1794-1851

════════════════════════════════════════════
ARWED, OU LES REPRÉSAILLES

1830

317 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ACTE PREMIER.
	─────────────────────────────────────────────────────
	▫ SCÈNE PREMIÈRE.
		─ poème	EVC1	"À vous hâter, Messieurs, je vous invite,"
	─────────────────────────────────────────────────────
	▫ SCÈNE II.
		─ poème	EVC2	"Oui, j’ai failli perdre la vie."
		─ poème	EVC3	"Oui, je redoute avec raison"
	─────────────────────────────────────────────────────
	▫ SCÈNE III.
		─ poème	EVC4	"Cette demande est juste et je l’appuie."
	─────────────────────────────────────────────────────
	▫ SCÈNE IV.
		─ poème	EVC5	"De vous revoir je dois bénir"
		─ poème	EVC6	"C’est un soldat aimable,"
		─ poème	EVC7	"Séparons-nous puisque le temps nous presse,"
	─────────────────────────────────────────────────────
	▫ SCÈNE V.
		─ poème	EVC8	"Je me disais, honteux de ma faiblesse,"
		─ poème	EVC9	"Mais achevez, je vous supplie."
	─────────────────────────────────────────────────────
	▫ SCÈNE VI.
	─────────────────────────────────────────────────────
	▫ SCÈNE VII.
		─ poème	EVC10	"Issu d’une illustre famille,"
		─ poème	EVC11	"Jamais je n’aurai la faiblesse"
	─────────────────────────────────────────────────────
	▫ SCÈNE VIII.
		─ poème	EVC12	"Puisqu’en ces lieux le plaisir nous convie,"
	─────────────────────────────────────────────────────
	▫ SCÈNE IX.
	─────────────────────────────────────────────────────
	▫ SCÈNE X.
		─ poème	EVC13	"Lorsqu’avec une ardeur extrême"
	─────────────────────────────────────────────────────
	▫ SCÈNE XI.
	─────────────────────────────────────────────────────
	▫ SCÈNE XII.
	─────────────────────────────────────────────────────
	▫ SCÈNE XIII.
		─ poème	EVC14	"Que pour le plaisir,"

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ACTE DEUXIÈME.
	─────────────────────────────────────────────────────
	▫ SCÈNE PREMIÈRE.
		─ poème	EVC15	"Oui, chaque jour, les jeux et la folie"
	─────────────────────────────────────────────────────
	▫ SCÈNE II.
	─────────────────────────────────────────────────────
	▫ SCÈNE III.
		─ poème	EVC16	"Laissons-lui goûter en silence"
		─ poème	EVC17	"Ah ! combien vos pleurs ont de charmes !"
		─ poème	EVC18	"Moi, l’époux de celle que j’aime !"
		─ poème	EVC19	ENSEMBLE.
	─────────────────────────────────────────────────────
	▫ SCÈNE IV.
	─────────────────────────────────────────────────────
	▫ SCÈNE V.
		─ poème	EVC20	"Il faut écouter la prudence,"
	─────────────────────────────────────────────────────
	▫ SCÈNE VI.
		─ poème	EVC21	"Je crois qu’il est une autre vie …"
		─ poème	EVC22	"De ses devoirs un soldat est l’esclave ;"
	─────────────────────────────────────────────────────
	▫ SCÈNE VII.
		─ poème	EVC23	"Cet écrit que je te destine,"
	─────────────────────────────────────────────────────
	▫ SCÈNE VIII.
		─ poème	EVC24	"Mon cher, le trépas qui moissonne"
		─ poème	EVC25	"On ne saurait m’en blâmer, j’imagine …"
	─────────────────────────────────────────────────────
	▫ SCÈNE IX.
		─ poème	EVC26	"Écoutons !"
	─────────────────────────────────────────────────────
	▫ SCÈNE X.
		─ poème	EVC27	"Voici le gage de tendresse"
	─────────────────────────────────────────────────────
	▫ SCÈNE XI.
	─────────────────────────────────────────────────────
	▫ SCÈNE XII.
	─────────────────────────────────────────────────────
	▫ SCÈNE XIII.
	─────────────────────────────────────────────────────
	▫ SCÈNE XIV ET DERNIÈRE
		─ poème	EVC28	"Hâtez-vous !"
