SCR
BYD
———
SCB_3

Eugène SCRIBE
1791-1861

Jean-François-Alfred BAYARD
1796-1853

════════════════════════════════════════════
LES TROIS MAITRESSES, OU UNE COUR D’ALLEMAGNE

COMÉDIE-VAUDEVILLE EN DEUX ACTES
24 JANVIER 1831

400 vers

─ poème	SCB30	"Vous en êtes sûr, mon ami ?"
─ poème	SCB31	"Contre les bourgeois, quoi qu’on ose,"
─ poème	SCB32	"Par des torts dont je me défends"
─ poème	SCB33	"Jeune, et maîtresse"
─ poème	SCB34	"Ah ! la rencontre est admirable !"
─ poème	SCB35	"Au revoir !"
─ poème	SCB36	"« Je reviens près de ce que j’aime,"
─ poème	SCB37	"Le torrent grossit et nous gagne."
─ poème	SCB38	"Vous chantez des airs d’Opéra"
─ poème	SCB39	"Lui, vous trahir, mademoiselle !"
─ poème	SCB40	"Où donc l’amour fidèle"
─ poème	SCB41	"N’allez-vous pas vous révolter ?"
─ poème	SCB42	"Allons, monsieur, embrassez-moi,"
─ poème	SCB43	"Ah ! par égard, mon aimable Henriette,"
─ poème	SCB44	"La liberté trompait votre courage."
─ poème	SCB45	"De tous côtés les peuples sont en armes,"
─ poème	SCB46	"Oui, je comprends ce trouble, ce langage :"
─ poème	SCB47	"Que vois-je ! … doublement coupable,"
─ poème	SCB48	"Il n’est plus temps de feindre,"
─ poème	SCB49	"Plus de chagrin, plus de tristesse,"
─ poème	SCB50	"Pour élever au premier rang"
─ poème	SCB51	"Courons ! il faut que la comtesse apprenne"
─ poème	SCB52	"C’est qu’il est une grâce…"
─ poème	SCB53	"Venez, mon cher surintendant,"
─ poème	SCB54	PREMIER COUPLET.DEUXIÈME COUPLET.
─ poème	SCB55	"Madame, un langage pareil…"
─ poème	SCB56	"Il est si bon que, par reconnaissance,"
─ poème	SCB57	"De ces projets qu’en tremblant je soupçonne,"
─ poème	SCB58	"Vous ne demandiez qu’une humble existence,"
─ poème	SCB59	CHOEUR.
─ poème	SCB60	CHOEUR,
─ poème	SCB61	"Pour lui je tremble,"
