
### Corpus : Le Rire des vers

Corpus de pièces de théâtre (vaudevilles) contenant des chansons en vers.
Ce corpus, élaboré par Lara Nugues pour sa thèse, a été préparé dans le cadre d'une collaboration avec le projet Le Rire des vers, dirigé par Anne-Sophie Bories  (Université de Bâle).

Dans ce corpus, seules les parties versifiées des chansons sont encodées en XML-TEI pour l'analyse métrique.

L'encodage XML-TEI a été réalisé par :

- Lara Nugues (doctorante, Université de Bâle)
- Louis-Geoffrey Gousset (stagiaire, Université de Caen - Normandie)
- Richard Renault (enseignant-chercheur, Université de Caen - Normandie)

[page du projet (Le Rire des vers](https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr)

Chaque répertoire d'un auteur (voir le fichier **liste_des_répertoires.txt** pour la liste des codes
des auteurs) contient un ou plusieurs répertoires correspondant chacun à une pièce de théâtre
ou à un recueil de poésies.

Chaque répertoire d'une pièce de théâtre ou d'un recueil de poésies contient trois fichiers :

* Le texte au format XML_TEI
* Le texte au format TXT
* Une table des matières

Le texte au format TXT est généré automatiquement à partir du fichier
XML au moyen du script **faire_texte_brut.sh**.

La table des matières est générée automatiquement à partir du fichier
XML au moyen du script **faire_index.sh**.
