<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Schéma de validation XML-TEI du corpus

Schéma de validation des textes du corpus au format XSD. Ce schéma s'applique aux textes avant l'analyse métrique automatique. Il ne contient pas la description des éléments et attributs introduits par le traitement automatique.

Le répertoire **xerces** contient les programmes de validation.
Un script Bash (**validation_xml.sh**) du répertoire **Scripts** permet de vérifier la validité d'un fichier XML du répertoire **Textes**.
