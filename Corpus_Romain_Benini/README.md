
<img src="Documents/Filles_du_peuple.jpg" alt="Filles du peuple" width="100"/> 


## Corpus Romain_Benini

Corpus de chansons du XIX<sup>e</sup> siècle de [Romain Benini (Sorbonne Université)](https://lettres.sorbonne-universite.fr/romain-benini)

▪ Titre du corpus d'origine :

#### ŒUVRES IMPRIMÉES À PARIS ENTRE 1848 ET 1851 DE TRENTE-ET-UN CHANSONNIERS DITS « POPULAIRES »

▪ Extension du corpus :

* 31 auteurs
* 604 poèmes
* 32 698 vers

▪ Le corpus origine (.doc) a été exporté dans un format xml et les textes ont été encodés en XML-TEI conformément au schéma de validation XSD du corpus Malherbə (XSD/TEI_Corpus_Malherbe_1.7.xsd).

▪ Le formatage XML-TEI des textes a été réalisé par :

* Richard Renault (responsable du projet)
* Tarik Bailiche (étudiant, L3 Sciences du Langage)
* Mylène Gourdeau-Morel (étudiante, L3 Sciences du Langage)
* Kylian Roussel, (étudiant, L3 Sciences du Langage)

voir le fichier **/documents/préparateurs.pdf**

▪ Ouvrage de référence du corpus, consultable en ligne : [Filles du peuple](https://books.openedition.org/enseditions/17162?lang=fr)

▪ Le répertoire **Documents** contient le corpus origine au format pdf (**CORPUS DÉFINITIF (09.09.14).pdf**) et les références
bibliographiques du corpus (**Ref_biblio_corpus_Benini.pdf**).


▪ Chaque répertoire d'un auteur (voir le fichier **liste_des_répertoires.txt** pour la liste des codes
des auteurs) contient un dossier correspondant aux chansons de l'auteur.

Chacun des dossiers contient trois fichiers :

* Le texte au format XML_TEI
* Le texte au format TXT
* Une table des matières

Le texte au format TXT est généré automatiquement à partir du fichier
XML au moyen du script **faire_texte_brut.sh**.

La table des matières est générée automatiquement à partir du fichier
XML au moyen du script **faire_index.sh**.
