Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
LNL
LNL_1

Auguste LOYNEL
1823-1849

ŒUVRES IMPRIMÉES À PARIS ENTRE 1848 ET 1851 DE TRENTE-ET-UN CHANSONNIERS DITS « POPULAIRES »
1848
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Filles du peuple ?Pour une stylistique de la chanson au XIXe siècle
Romain Benini

ENS Éditions
2021
Corpus de textes fourni par l’auteur
_________________________________________________________________
Édition de référence pour les corrections métriques :



┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



AUX MÂNES DES GÉNÉRAUX MORTS 
POUR LA RÉPUBLIQUE
Air du Forçat libéré (Morizot), ou de la Fête des Martyrs.

Braves guerriers, que la France vit naître,
Vous, qu’elle vit grandir au champs d’honneur,
Hommes de cœur, qu’elle vit apparaître,
Pour protéger sa gloire, son bonheur :
Quand l’avenir vous laissait à résoudre
Le grand problème où tendaient tous nos vœux,
Un ciel ingrat sur vos bras généreux
Aurait-il dû laisser tomber sa foudre ?…
Dormez en paix, illustres généraux,
La France en deuil pleure sur vos tombeaux !…


Pauvre Regnault, noble, sainte victime,
Toi, qui toujours fut l’ami du soldat,
Le défenseur de celui qu’on opprime,
Et l’ennemi juré de l’apostat,
Toi, qu’épargna le fusil du Kabyle,
Toi, qui blanchis sous le ciel africain,
Croyais-tu donc, mâle républicain,
Tomber martyr d’une guerre civile ?…
Dormez en paix, illustres généraux,
La France en deuil pleure sur vos tombeaux !…


Et toi, Bréa, qu’une mission sainte
Poussait au camp des hommes égarés,
Honneur à toi, qui te livras sans crainte
Aux combattants par la poudre enivrés !…
Lorsque cent voix chantaient la Marseillaise,
Qui t’aurait dit… (Nos cœurs en ont frémi !…)
Que tu devais, loin du sol ennemi,
Mourir frappé d’une balle française ?…
Dormez en paix, illustres généraux,
La France en deuil pleure sur vos tombeaux !…


O Négrier ! l’auréole de gloire
Qui couronnait ton front majestueux,
Vient de grandir… Au temple de mémoire
Tu prendras place au rang des demi-dieux !
Quel doigt maudit, quelle cause fatale
Au plomb mortel a pu te désigner ?…
Cruelle mort ! tu devais épargner
Un des débris de l’ère impériale !…,
Dormez en paix, illustres généraux,
La France en deuil pleure sur vos tombeaux !…


Vieux Duvivier, quinze jours de souffrance,
Quinze longs jours donnaient lieu d’espérer
Que tu verrais encor longtemps la France
Sous le drapeau qu’elle vient d’arborer…
Mais non !… la mort, dont la plume inflexible
Trace souvent tant d’iniques arrêts,
T’a condamné !… Faut-il que nos regrets
Et que nos pleurs la trouvent insensibles ?…,
Dormez en paix, illustres généraux,
La France en deuil pleure sur vos tombeaux !…  


Auguste Loynel.



LES MARTYRS DES 23, 24, 25 ET 26 JUIN 1848.

Air des Lanciers Polonais, ou les Hommes de la veille 

et ceux du lendemain (Gustave Leroy)

Paroles d’Auguste Loynel.

Paris se voile de ténèbres,
Pourquoi tant de fronts sérieux ?…
Pourquoi tant de crêpes funèbres ?…
Pourquoi ces pleurs dans tous les yeux ?…
A genoux ! que chacun amasse
Des fleurs pour des tombeaux chéris !
Frères ! c’est un convoi qui passe…
Saluons ces nobles débris !


Liberté ! sublime déesse,
Aurais-tu trompé notre espoir ?…
Regarde… grande prophétesse,
Là bas l’horizon est tout noir.
Autour de nous tout est souffrance,
Vainqueurs ou vaincus, votre prix
Doit être aujourd’hui l’espérance…
Saluons ces nobles débris !


Spartiates de la patrie,
Vous que le plomb a décimés,
Du tombeau quelle voix vous crie :
– « Venez défenseurs tant aimés »
A l’aspect de l’affreux carnage
Qui rougit le sol de Paris,
On se croirait dans un autre âge…
Saluons ces nobles débris !


Ils sont tombés sous la mitraille
Ces soldats ennemis des rois ;
Ils sont tombés, que nul ne raille
Ces hommes si forts de leurs droits…
Les lauriers sont dus à la gloire
De ceux que la France à nourris :
Loin d’insulter à leur mémoire,
Saluons ces nobles débris.


O février ! ton auréole
Devrait-elle donc se ternir ?…
Nous avions foi dans ce symbole,
Nous avions foi dans l’avenir.
Non, jamais les guerres civiles
Du deuil n’étoufferont les cris…
Tant de morts pourraient être utiles !
Saluons ces nobles débris.


Roulez tambours,… au cimetière
Trop tôt peut-être que l’oubli
Va recouvrir de sa poussière
Chaque cadavre enseveli.
Sur leurs tombes si la main trace
Des noms parmi nous tant chéris,
Honte à celui qui les efface
Respect à ces nobles débris.


Librairie chansonnière de Durand, rue Rambuteau, 32.


──────────────────────────────────
Paris. – Imprimerie de Beaulé et Maignand, 
Rue Jacques de Brosse, 8.




LE PEUPLE FRANÇAIS 
À LOUIS NAPOLÉON BONAPARTE
Paroles d’Auguste Loynel.
 Air des Trois Couleurs ou de : Vive Paris.

Napoléon !… au nom de ce grand homme,
Les souvenirs font battre tous les cœurs,
Héros plus grand que le César de Rome,
Il fut trahi, mais n’eût pas de vainqueurs.
Prince éloigné de ta mère patrie,
Pour célébrer la chûte d’un Tarquin,
A ses banquets la France te convie ;
Napoléon (bis), sois bon républicain.


Des partisans, ennemis de ta race,
A tes côtés viendront dire tout bas :
« De l’aigle mort, tu dois suivre la trace… »
Ces imposteurs ne les écoute pas.
En Février, la République éclose,
Tient à jamais le sceptre dans sa main :
La royauté, perdra toujours sa cause…
Napoléon (bis), sois bon républicain.


N’ignore point, qu’une énorme distance,
Doit séparer un député d’un roi,
De ton mandat, si tu sais l’importance,
Vers l’avenir regarde sans effroi.
Si tu cherchais à monter sur le trône,
Le sang pourrait arroser ton chemin…
Notre amitié, vaut bien une couronne !
Napoléon (bis), sois bon républicain.


Quand tu seras au sein de l’Assemblée
Qui doit nous faire à tous un sort meilleur,
Qu’à ta parole, elle soit ébranlée,
Sois l’avocat du pauvre travailleur.
Montre-toi fort, afin que l’on maintienne,
Les droits sacrés du peuple souverain ;
Et notre gloire un jour sera la tienne !…
Napoléon (bis), sois bon républicain.


Ne vas jamais t’éloigner de la route,
Que l’honneur seul trace pour les héros ;
Ta tâche est grande, il faut l’accomplir toute,
Pur démocrate, illustre nos drapeaux !
De tes aïeux, là haut l’étoile brille,
Brave aujourd’hui, sois le même demain :
Tu dois ton bras à la grande famille…
Napoléon (bis), sois bon républicain.



Paris,
Juin 1848..

Librairie chansonnière de Durand, rue Rambuteau, 32.


───────────────────────────────────
Paris. – Imprimerie de Beaulé et Maignand, 
Rue Jacques de Brosse, 8.




Librairie chansonnière de Durand, rue Rambuteau, 32.
LES BALS DE PARIS OU LE CARNAVAL DE 1849
Paroles du citoyen Auguste Loynel.
[IMAGE.]

Air de Charlotte-la-Républicaine.

Alerte, c’est le carnaval
Que la danse
Vive s’élance,
Musard, ce roi du bacchanal,
A donné le signal.


Il est minuit, Sara,
Quitte la Boule-Rouge,
Cours au lumineux bouge
Qu’on nomme l’Opéra ;
Sous ton masque moqueur,
Folle, que rien n’effraie,
Ris de l’amant qui paie
Avec l’ami du cœur.


Alerte, etc.






Brillant Valentino,
De grâce ouvre ta porte ;
Je vois une cohorte
De ducs incognito…
Grands seigneurs sans maisons,
Respect à leur génie,
Ruolz et compagnie
Ont doré leurs blasons…


Alerte, etc.






Montesquieu, vieux bazar,
Vive tes cuisinières,
Traînant sous tes lumières
Leurs vertus de hasard ;
Les tailleurs amoureux
Jeûnant trois jours sur quatre,
Sur elles vont s’abattre,
Allumés par leurs feux !


Alerte, etc.






Le Prado va rugir,
Grisette encor novice,
Si ta sagesse y glisse,
Garde-toi d’en rougir.
Au salon éclatant,
Où saute la noblesse ;
Celle d’une duchesse
Parfois en fait autant…


Alerte, etc.






Accordons un regret
A la pauvre Chartreuse,
Où mainte chahuteuse
Instruisit son jarret :
Sous l’archet de Carnaud,
Willis étudiantes,
Vos mœurs édifiantes
Se jugeaint par un saut.


Alerte, etc.






Voici le bal des Chiens,
Aux beautés équivoques,
Dont les galants colloques
Me semblent peu chrétiens.
Pécheurs inassouvis,
Vous dont l’instinct docile
Veut de l’amour facile,
Vous serez bien servis…


Alerte, etc.






Du bal des Accacias
Les mœurs patriarchales
Nous offrent des vestales,
N’ayant fait qu’un faux pas :
De ces chastes houris
J’adorerais les hanches,
Si leurs dents étaient blanches,
Et leurs cheveux moins gris,


Alerte, etc.






Et toi, grand bal Chicard,
Que ton orchestre appelle
La riche clientèle
Du quartier Mouffetard.
Pour ton salon coquet
Gueugueuse que j’admire
Laisse son cachemire,
Liche-à-Mort, son crochet.


Alerte, etc.





En vente : LA VOIX DU PEUPLE, ou les républicaines de 1848, un volume in-18 de 350 pages, tenant 150 Chansons démocratiques et sociales. Prix : 1f. 25c.

───────────────────────────────────────
Imp. de Beaulé et Maignand, Rue Jacques de Brosse, 8.



L’ASSOMMOIR DE BELLEVILLE.


Romance trouvée dans les vallades de Fanfan Chaloupe, chifferton2,
cané d’une apoplexie de cochon, à l’âge de 73 longes, à la lourde du sieur Riffaudez-nous, mannezingue,
à l’enseigne de la Sauterelle Éventrée, barrière de la Courtille.


Poivriers3, priez pour le repos de son âme et braillez le refrain
de sa goualante4 à tirelarigo.


Air des Moissonneurs

Bifins 5, qui n’avez que dix rades6,
J’vas vous montrer un chouett’ courant,
Pour abreuver les camarades,
Au plus bas blot7, c’est délirant !
Quand vot’ gonzess’ vous entortille,
Filez à gauch’ de la Courtille,
Vous payer un coup d’arrosoir,
A l’Assommoir.


Faut pas blaguer, le trèpe est batte8
Dans c’taudion9 i’ s’ trouv’ des rupins :
Si queuqu’s gonziers10 traîn’nt la savate,
J’en ai r’bouissé11qu’ont d’s escarpins.
Pour lâcher d’un cran l’genr’ canaille,
Il n’leur manque que des gants paille ;
Mais on n’est pas t’nu d’en avoir
A l’Assommoir.


Les meness’s12 aboul’nt par douzaines,
R’nifler leur petit fad’13 d’eau d’af’14,
Si leurs chass’s15 coul’nt comm’ des fontaines,
Un’ chopin’ ne leur coll’ pas l’taf16 :
Des fois quand l’temps s’tourne à la lance17,
C’est épatant comm’ tout ça danse…
V’la l’coup dur du matin au soir,
A l’Assommoir.


J’en r’mouch’18 qui fris’nt pas mal leur naze
A caus’ des propos incongrus
Qu’mon chiffon19, qui n’aim’ pas la gaze,
Leur lâche en mots un peu trop crus.
C’est qu’j’ai fait, foi d’Fanfan Chaloupe !
Mes étud’s au Camp de la Loupe20 :
Aussi j’conobr’21 c’qu’on doit savoir
A l’Assommoir.


Un tas d’bibons à douilles blanches22,
Sitôt qu’ils ont du carm’23 de trop,
N’attend’nt pas les fêt’s et dimanches
Pour y pincer leur coup d’sirop.
Après tout, moi, je les excuse,
Faut bien qu’la vieillesse s’amuse ;
Ell’ tient si proprement l’crachoir24
A l’Assommoir.


Comm’ je n’ fais pas fi d’la lichance25,
J’me pouss’ quéqu’fois de c’côté-là !
Un courant d’air, pas plus qu’ça de chance,
J’visit les amunch’s26, et voilà…
Ces gens qu’d’aucuns trait’nt de crapule,
Moi, j’trinque avec eux sans scrupule ;
On est égal d’vant l’abreuvoir
De l’Assommoir !

Chanson recueillie et traduite par Auguste Loynel.

Chiffonier
Poivrots
Chanson
Chiffoniers
Sous
Prix
Le public est soigné
Cet endroit
Individu
Remarqué
Femmes
Part
Eau-de-vie
Yeux
La peur
La pluie
J’en vois
Langue
Camp des fainéants
Je connais
Vieillards à cheveux blancs
De l’argent
La conversation
Boisson
Amis.



UNE CANDIDATURE EN DÈCHE OU LE DÉGOMMÉ
Paroles d’Auguste Loynel.
Air : Ah ! le bel oiseau, maman.

Est-il bien rabobiné,
C’ pauv’ cher homme
Que l’ vot’ dégomme,
Comme il est bien goupiné,
Nom d’un chien, quel chouett’ pied d’ nez !


C’ grand dégommeur d’Africains,
De craint’ que not’ peau n’ la danse,
A l’instar des Marocains,
Nous tanna l’ cuir d’importance !


Est-il bien, etc






Pour éviter les cancans
Sur sa conduite à l’eau d’ roses,
Il nous fit bâtir de camps :
Manièr’ d’arranger les choses.


Est-il bien, etc






J’ suis sûr que plus d’un Bédouin
Aime à r’brouss’ poil de tels hôtes,
Autant qu’ les combattants d’ Juin
Dont il tarauda les côtes !


Est-il bien, etc






D’un’ bonn’ provision d’ tabac
Il fit don aux patriotes,
Mais l’ peupl’ mang’ra, malgré l’ sac,
Tous ses mobil’s en gib’lottes !


Est-il bien, etc






Quand il posait sur son ch’val,
Comme un’ vrai’ pair’ de pincettes,
Plus d’un gard’ national
S’ dit tout bas : – Les deux bell’s bêtes !


Est-il bien, etc






Comm’ chacun des tourtereaux
Qui, chez l’ National rocoule,
Va s’ la casser des bureaux,
Les actions sont à la coule !


Est-il bien, etc






J’ lui conseill’rais, sous peu d’jours,
D’ craint’ qu’il n’ se trouv’ dans la pane,
Maint’nant que l’ sabr’ n’a plus cours,
De donner quéqu’s leçons d’ canne !


Est-il bien, etc






Qu’il fil’ d’autor, si son bras
N’aim’ pas la démocratie,
Trouver Nic, Nic, Nicolas,
Emp’reur d’ la Muftocratie !


Est-il bien rabobiné,
C’ pauv’ cher homme
Que l’ vot’ dégomme,
Comme il est bien goupiné,
Nom d’un chien, quel chouett’ pied d’ nez !



───────────────────────────────────────
Imp. de Beaulé et Maignand, Rue Jacques de Brosse, 8.




AUX JUGES ET AUX CONDAMNÉS 
DE LA HAUTE COUR DE BOURGES.
Paroles d’Auguste loynel.


Avril 1849..


Air de la Nostalgie (Béranger), 
ou de la Femme du prisonnier (C. Gille).

Il est fini ce drame où le scandale
S’est efforcé d’égarer les esprits ;
O Némésis ! sous ta lourde sandale
Écrase ceux qu’ont frappés nos mépris !
Frères, pour vous est gravé dans l’histoire
Un beau feuillet : doit-il s’anéantir ?…
Non, non, levez vos fronts brillants de gloire,
La voix du peuple absout chaque martyr !


Tristes jurés, vos larges consciences
Ont bien souvent des appétits gloutons,
Prolongez donc ces sales audiences,
Ne volez pas l’or que nous vous jetons…
Vous vous couvrez en vain d’un masque austère,
Vous dont la bouche enhardie à mentir
Osa fausser un sacré ministère…
La voix du peuple absout chaque martyr !


Quoi ! sans pudeur, haute-cour de justice,
Vous prononcez, sur la foi du serment,
Votre verdict, flétrissure factice,
Un tel arrêt peut-il être infâmant ?…
Hommes vendus, forgez, forgez des chaînes,
Sans redouter l’heure du repentir….
Malgré vos cœurs où grouillent tant de haines,
La voix du peuple absout chaque martyr !


Des Trestaillons seriez-vous l’avant-garde,
Juges sans noms, Tristans au petit pied ?…
Tremblez, maudits ! la main de Dieu nous garde,
Elle pourra vous broyer sans pitié.
Nobles tribuns, dignes des temps antiques,
Espoir !… Bientôt ce cri va retentir :
– « Debout ! debout ! parias politiques,
» Le voix du peuple absout chaque martyr ! »


O liberté ! vierge capricieuse,
Oublieras-tu tes ardents défenseurs ?
Tu sais combien leur vie est précieuse,
S’il faut lutter contre nos oppresseurs ?
Pour nous sauver des rois, de leurs rapines,
Quand la jeune ère est prête à s’engloutir,
Viens arracher leurs couronnes d’épines…
La voix du peuple absout chaque martyr !


Mont Saint-Michel, Golgotha de notre âge,
Les verras-tu pourrir dans tes cachots ?…
Le jour s’avance où la voix de l’orage
De tes bas-fonds effraiera les échos…
Qu’entends-je ? O ciel ! messager d’espérance,
Tu viens leur dire : « Enfants, il faut partir !
– Qui donc es-tu ?… – L’ange de délivrance !
La voix du peuple absout chaque martyr !


Propriété de l’auteur.

───────────────────────────────────────────
Paris. – Imprimerie de BEAULÉ et MAIGNAND, 
rue Jacques de Brosse, 8, 
à côté de l’Église Saint-Gervais.




Librairie chansonnière de DURAND, éditeur, rue Rambuteau, 32.
LE DERNIER MOT.
Paroles d’Auguste LOYNEL.
Air : Pas de chagrin qui ne soit oublié entre les arts et l’amitié.

Gens de toutes les politiques,
Judas plus ou moins convertis,
Monarchistes jésuitiques
Offrant à plus de vingt partis
Et vos figures rachitiques,
Et vos brevets peu garantis. (bis)
Foin des blagueurs dont la ficelle s’use,
A vous, sultans, que maint journal excuse,
Nos bouts rimés que la police accuse,
Sur nos refrains dont le peuple s’amuse,
En vain, messieurs, vous mettez l’interdit…
Le dernier mot n’en est pas dit !


Le sang bouillonnait dans les veines
De nos frères déshérités,
Bientôt, forts de leurs vieilles haines,
On vit surgir de tous côtés
Des hommes broyant sous leurs chaînes
La plus lâche des royautés.
Vous oubliez, grands faiseurs d’algarades,
Qu’en Février, lorsque les fusillades
Eurent cessé, soudain nos camarades,
Vaillants soldats noircis aux barricades,
Vous avaient fait trois longs mois de crédit…
Le dernier mot n’en est pas dit !


La Constitution savante,
Chef-d’œuvre des bleus et des blancs,
Loin de rassurer, épouvante
Par ses dogmes peu consolants ;
Pauvre fille, quoi qu’on te vante,
Un ver maudit ronge tes flancs !
Le droit sacré nous donnant droit de vivre
N’est point inscrit aux pages de ce livre,
Nous trace-t-il la route qu’il faut suivre ?
Il vient un jour où la misère enivre !
Beaux rédacteurs de ce sublime édit…
Le dernier mot n’en est pas dit !


Lorsque l’erreur, d’un voile sombre,
Cherche à couvrir tous les esprits,
Qu’un beau soleil remplace l’ombre,
Luttons, car il faut à tout prix
Arracher le vaisseau qui sombre,
La mort n’aura que nos mépris…
Pour te guérir, société morose,
De cette plaie où notre doigt se pose,
Aux charlatans n’accordons nulle clause,
Forts de nos droits, défendons notre cause !
Marchons toujours, honte à qui nous maudit !
Le dernier mot n’en est pas dit !


De leurs lois anti-sociales,
Les réacteurs font un grand cas,
Pauvres couleurs nationales,
Craignez un futur branle-bas ;
Sous les douches impériales,
Vous déteindrez, n’en doutez pas !…
Mais par bonheur un triste idiotisme
N’obscurcira jamais le divin prisme,
Brillant pour tous par le socialisme,
Quand le pouvoir frise le despotisme ;
Nous verrons bien si le Peuple applaudit…
Le dernier mot n’en est pas dit !


Voyez ces femmes et ces mères,
La douleur a creusé leurs traits,
Nous vous supplions, nous, les frères
Des travailleurs que vos décrets
Ont caserné sur vos galères,
De casser d’injustes arrêts.
Croyez-vous donc notre caste abrutie,
Fausse Thémis, justice travestie ?…
Tristes jugeurs, accordez l’amnistie,
Ou nous jouerons la dernière partie !
Ce n’est pas nous qu’un revers engourdit…
Le dernier mot n’en est pas dit !


──────────────────────────────────────
Imp. de beaulé et maignand, rue Jacques de Brosse, 8.



Librairie chansonnière de DURAND, éditeur, rue Rambuteau, 32.
[IMAGE.]
AMNISTIE ! PRIÈRE DE LA FEMME 
D’UN TRANSPORTÉ
Paroles du citoyen Auguste loynel.
Air du Retour en France, ou de Vive Paris.

Toi dont le fils mourut sur un calvaire,
Dieu tout puissant, je t’implore à genoux,
Vois mes enfants maigris par la misère,
Oh ! par pitié, jette un regard sur nous !…
Non ! non, ta main n’est pas appesantie
Sur les proscrits, ils ont dû tant souffrir…
Dieu, fais sonner l’heure de l’amnistie,
Où nous n’aurons bientôt plus qu’à mourir.


L’âtre est éteint… Le givre à ma fenêtre
A de l’hiver brodé les blanches fleurs,
Nous avons froid… Demain la faim peut-être
Par sa morsure accroîtra nos douleurs ;
Toute espérance est-elle anéantie,
Laisseras-tu notre sang s’appauvrir ?
Dieu, fais sonner l’heure de l’amnistie,
Ou nous n’aurons bientôt plus qu’à mourir !


Fallait-il donc, mon Dieu, tant de victimes
Pour assouvir la haine d’un parti ?
O défenseurs de nos droits légitimes
A vos aînés vous n’avez pas menti !
Votre passé, sublime garantie
Des plébéiens vous fera tous chérir…
Dieu, fais sonner l’heure de l’amnistie,
Ou nous n’aurons bientôt plus qu’à mourir !


Qu’entends-je, ô ciel ! une voix tutélaire
Voudrait pour tous un pardon généreux ;
Mais un ingrat, ministre impopulaire,
A refusé de souscrire à ses vœux !
Lorsqu’au pouvoir la haine est départie,
Le peuple un jour saura bien la flêtrir !…
Dieu, fais sonner l’heure de l’amnistie,
Ou nous n’aurons bientôt plus qu’à mourir !


Vous méprisez l’instant de la vengeance
De nos époux, infâmes délateurs ;
Quand nous savions supporter l’indigence,
Ne pouviez-vous rester froids spectateurs ?
Tremblez ! il est une main qui châtie,
Les fruits semés, soudain pourraient mûrir…
Dieu, fais sonner l’heure de l’amnistie,
Ou nous n’aurons bientôt plus qu’à mourir !


──────────────────────────────────────
Imp. de beaulé et maignand, rue Jacques de Brosse, 8.



CHANT PATRIOTIQUE D’UN CITOYEN 
DE LA RUE MOUFFETARD.
Paroles d’Auguste Loynel.
Air : Bon voyage, mon cher Dumolet.

Bien l’ bonsoir à la royauté,
A bas la clique !
Vive la République !
Bien l’ bonsoir à la royauté,
J’ nons plus l’honneur de boire à sa santé.


Depuis longtemps, Philippe, de racaille
Traitait son peupl’ peut-on s’ conduire ainsi ?…
S’il voulait voir le r’vers de la médaille,
Faut convenir qu’il a bien réussi.


Bien l’ bonsoir, etc.






Sans trop crier contr’ ses petit’s ficelles,
Nous l’ laissions fair’ pour tant l’ vieux Fess’-Mathieu,
Par les deux bouts allumait nos chandelles,
En s’ figurant qu’ nous n’y verrions qu’ du feu !


Bien l’ bonsoir, etc.






Il nous promit en l’an mil huit cent trente,
Un’ Chart’ vierg’, mais c’ que j’ vois d’ plus certain,
C’est que bientôt son humeur tolérante,
D’une honnêt’ femm’ sut faire une catin…


Bien l’ bonsoir, etc.






Comme l’argent grâce à ses tripotages
A son budget manquait, presque toujours,
J’aurions prié l’hospice des P’tits-Ménages,
De lui donner asil’ pour ses vieux jours…


Bien l’ bonsoir, etc.






Il n’ fallait pas avoir deux sous d’ morale
Dans la caboch’, pour dev’nir le pivot,
De ces rouag’s égrenant le scandale,
Nommés : Hébert, Duchâtel et Guizot !…


Bien l’ bonsoir, etc.






Jusqu’à la fin, l’ vieux barbon sur la brèche
Espérait voir tomber ceux qu’il dressa,
Quand on arrive à ses sept brisqu’s et mèche,
Est-c’ qu’on devrait être entêté comm’ ça ?…


Bien l’ bonsoir, etc.






En bons garçons, l’ jeudi d’ grande mémoire,
Avons nous ri quand l’trône détala ?…
Dam ! faut conv’nir si l’on en croit l’histoire,
Qu’on n’ se chauff’ pas tous les jours de c’ bois là !


Bien l’ bonsoir, etc.






Victoria, si c’ t’honnête équipage,
Vient vous d’mander à dîner sans façon,
Pour le dessert, entr’ la poire et l’ fromage,
Je vous prierais d’ leur conter ma chanson…


Bien l’ bonsoir à la royauté,
A bas la clique !
Vive la République !
Bien l’ bonsoir à la royauté,
J’ nons plus l’honneur de boire à sa santé.



Février 1848.

Propriété de l’éditeur.
[Chez Durand, éditeur, rue Rambuteau, 32 
Imp. de Beaulé et Maignand, rue Jacques de Brosse, 8.]



LES HOMMES DE L’AVENIR
Air de l’Entrée aux Tuileries (Gustave Leroy).

Si, de nos jours, aux pages de l’histoire
Le citoyen jette son œil surpris,
Qu’y trouve-t-il ? un long réquisitoire,
Où bien des noms éveillent le mépris.
Il cherche en vain, dans cet obscur dédale,
Le fil sauveur qui doit guider ses pas ;
Quand chaque route aboutit au scandale,
Hommes de cœur, ne surgirez-vous pas ?…


Un demi-siècle a passé sur nos têtes,
Depuis qu’un peuple a reconquis son lot ;
Esquif longtemps battu par les tempêtes,
La France, encor, lutte contre le flot :
Entendez-vous ces cris qui retentissent ?…
Vieux matelots ! on réclame vos bras ;
Tout près de vous des frères s’engloutissent,
Hommes de cœur, ne surgirez-vous pas ?…


En vain, on crut, aux jours de la colère,
Que le puissant allait baisser le front ;
Espoir trompeur ! notre ange tutélaire,
La liberté, semble craindre un affront !
Nos combattants cachent leurs cicatrices,
Ils ont pourtant su vaincre le trépas :
Pour rappeller d’immenses sacrifices,
Hommes de cœur, ne surgirez-vous pas ?…


Quoique nos cœurs aient toujours l’énergie
Que Dieu leur fit pour les jours orageux,
Quoique nos pieds, sur la terre rougie
D’un sang maudit, s’avancent courageux :
Lorsqu’il faudra, fatigués de la route
Que nous suivons, crier : « – Mort aux Judas !
« La liberté nous a fait banqueroute… »
Hommes de cœur, ne surgirez-vous pas ?…


Blasons d’hier, éblouissant la foule,
Vos protégés valent-ils mieux que nous ?…
Non… de nos temps où le préjugé croûle
Devant nos droits, renégats, à genoux !…
Si le progrès, cette étoile brillante,
Vient à pâlir aux yeux de ses soldats,
Pour ranimer sa flamme vacillante,
Hommes de cœur, ne surgirez-vous pas ?…


Nobles champions de notre sainte cause,
Retirez-vous de l’abîme profond
Où nous plongea plus d’une ignoble clause
D’un faux traité, dont pas un ne répond.
Craindriez-vous que les destins contraires,
Si vous tombez, ne fissent des ingrats ?…
De peur qu’un jour on doute de vos frères,
Hommes de cœur, ne surgirez-vous pas ?


Auguste Loynel.

[Le Républicain Lyrique n°2, Durand / Beaulé et Maignand.]



A LA POLOGNE.
Air : On n’entre pas dans le palais des rois (G. Leroy), Ou de : Vive Paris.

Après trente ans d’un infâme esclavage,
Pologne, enfin tu veux briser tes fers
Déjà la voix de ton mâle courage,
A retenti dans tout notre univers.
A ton réveil, plus d’un grand cœur espère
Que tes enfants de leurs droits seront forts,
Pour toi bientôt va luire un jour prospère ;
La liberté, bénira tes efforts !


Le souvenir d’un passé plein de gloire,
Vient aujourd’hui te faire ouvrir les yeux
Sur le présent, dont la triste mémoire
Accusera notre siècle oublieux.
Tu veux, pour prix de ce sang qui féconde
Les champs glacés, où tes héros sont morts,
Te replacer sur la carte du monde…
La liberté bénira tes efforts !


A peine as tu mis le pied dans l’arêne
Qu’à tes côtés surgit la trahison ;
De tes soldats on égare la haine,
Fais que tes chefs conservent la raison,
Si dans tes rangs, quelque frère perfide,
Manque au serment, frappe-le sans remords….
De Waterloo que l’exemple te guide !…
La liberté bénira tes efforts !


Nous te semblons bien pauvres de courage,
Toi qui, peut-être, espérais nos secours,
De tes bourreaux on respecte la rage,
Pour maintenir l’entente des deux cours.
Quand tu combats, cloîtrés dans nos murailles,
Des intrigants enchaînent nos transports !…
En attendant l’heure des représailles,
La liberté bénira tes efforts !


Si quelque jour, ton peuple patriote
Te fait sortir du lugubre linceul
Ou tu dormais, Pologne, pauvre ilote,
Hélas ! l’honneur n’en sera qu’à lui seul.
De l’incendie on craint les étincelles,
Nos chants guerriers suspendent leurs accords…
Mais ne crains rien, si parfois tu chancelles,
La liberté, bénira tes efforts !…


Oh ! comme toi, la Grèce, à l’agonie,
Avait osé rêver la liberté !
Pour renverser l’ignoble tyrannie
Qui l’opprimait, notre bras fut prêté.
Pologne, à nous, notre vieille alliée,
A nous bientôt de réparer nos torts….
De tus les cœurs tu n’es pas oubliée,
La liberté bénira nos efforts !…


Auguste Loynel.

[Le Républicain Lyrique n°2, Durand / Beaulé et Maignand.]



LES ORPHELINS DE JUIN 1848.
Air de la Lionne, ou de Vive Paris.

Déjà le temps a refroidi la cendre
De ces héros morts pour la liberté ;
Dans tous les cœurs, tu dois enfin descendre
Sainte union, noble fraternité !
Par tant de maux nos âmes consternées
Devraient rêver de plus heureux destins.
Français, après ces fatales journées,
N’oublions pas les pauvres orphelins !


Pauvres enfants, le sort inexorable
Aurait-il dû vous laisser sans appui ?…
Non, prévenant la guerre déplorable,
Il nous devait des jours qui n’ont pas lui.
Prions le Ciel, maître des destinées,
D’unir enfin et nos cœurs et nos mains…
Français, après ces fatales journées,
N’oublions pas les pauvres orphelins !


Ils ont payé leur dette à la patrie,
Ces combattants devenus des héros,
En défendant notre mère chérie,
Ils sont tombés près de leurs saints drapeaux.
Dans le cercueil, leurs têtes couronnées
D’un laurier vert disent leurs bulletins…
Français, après ces fatales journées,
N’oublions pas les pauvres orphelins !


En attendant quelques jours plus prospères,
Puisque le pain n’est pas accaparé,
Frères, sachons le partager en frères ;
Jésus t’a dit : ce devoir est sacré.
Chaque martyr des luttes acharnées
D’en haut assiste à nos humbles festins…
Français, après ces fatales journées,
N’oublions pas les pauvres orphelins !


Sur le passé jetons un large voile,
Que nos douleurs s’éteignent par l’oubli :
A l’horizon voyez luire une étoile,
C’est l’avenir par l’espoir embelli !
Par le mépris étouffons les menées
Des ennemis, aboyeurs clandestins…
Français, après ces fatales journées,
N’oublions pas les pauvres orphelins !


A. Loynel.

[Le Républicain Lyrique n°3, Durand / Beaulé et Maignand.]



A BAS LES PRÉTENDANTS.
Air : A bas les préjugés (G. Leroy).

Qu’ai-je entendu, ma noble République,
Quoi tu devais tomber aux mains des rois ?
Représentants de la gloire civique,
Laisseriez-vous périr ainsi nos droits ?…
Puisque le peuple a sauvé la patrie
Des fers honteux forgés par les tyrans,
Entendez-vous ce peuple qui vous crie :
A bas les prétendants !…


Voyez d’abord ce Bourbon qui s’avance,
Bien escorté par les fils d’Escobar ;
La croix en main, symbole de clémence,
De ses aïeux il réclame la part ;
Triste héros, de ta horde guerrière
Nous détestons les uniformes blancs…
A nos autels tu n’as point place… – Arrière !
A bas les prétendants !…


Et toi, le fils d’une race bannie,
As-tu donc cru, prince un peu roturier,
Que notre règne était à l’agonie,
Que nous avions oublié février ?…
Ne crains-tu pas de réveiller les haines,
En écoutant les vœux des intrigants ?…
Le sang royal circule dans tes veines…
A bas les prétendants !…


Faible débris de race impériale,
Croirais-tu donc ton chemin tout tracé ?…
Pourquoi rêver une époque fatale ?
N’y compte point ; ce temps est bien passé.
N’écoute pas l’orgueil, ce faux prophète,
S’il te poussait à déserter nos rangs,
Nous saurions bien te jeter à la tête(1) :
A bas les prétendants !…


Malheur aux fous dont la rage insensée
Ose tramer de ténébreux complots !…
Comme l’a dit un roi de la pensée :
Tous les tyrans appartiennent aux flots.
Luis à nos yeux, ère démocratique,
Oh ! prouve à tous que nos bras sont puissants !
Vive à jamais la sainte République !…
A bas les prétendants !…


Loynel.
Paroles prononcées à la Chambre par le prince Louis-Napoléon Bonaparte le 9 octobre 1848.

Nota. MM. les chanteurs qui ne connaissent pas les airs adaptés à cette chanson pourront
la mettre sur l’air de : Vive Paris, ou de la Lionne ou
des Trois Couleurs, en substituant au refrain : A bas les prétendants ce vers-ci :
Non, plus de rois ! à bas les prétendants !


[Le Républicain lyrique n°5, Durand / Beaulé et Maignand.]



AUX DÉPORTÉS.
Air des Trois Couleurs, ou de la petite Colombe.

Lorsque l’exil implacable commence,
En y songeant, devons-nous rester froids ?…
Le désespoir, cet océan immense,
Pour le franchir n’offre pas de détroits ;
La liberté peut, forte de nos haines,
Anéantir cet illégal impôt ;
Le sang français bout encor dans nos veines…
Frères proscrits, vous reviendrez bientôt !


Les défenseurs que notre République
A conservés, seront-ils aussi forts
Que ces héros, qu’un pouvoir despotique
Vient enchaîner pour prix de leurs efforts ?…
Demain le fils de quelque vieille souche
Peut revenir réclamer son tréteau,
Pour lui montrer comme on mord la cartouche,
Frères proscrits, vous reviendrez bientôt !


Pauvres enfants que berça la misère,
Nous n’avons pas oublié vos douleurs,
Car nous aussi, quand la soif nous altère,
Au lieu d’eau pure, il faut boire nos pleurs.
Si dans nos cœurs, martyrs de l’indigence,
Vos cris plaintifs ne trouvaient pas d’écho,
De leurs cercueils, nos morts crîraient : vengeance !
Frères proscrits, vous reviendrez bientôt !


Ne craignez pas que l’oubli de son voile
Dérobe aux yeux votre noble passé,
Dans le ciel noir, là-bas brille une étoile,
Phare d’espoir, c’est Dieu qui l’a placé !
A l’horizon sa lueur tremblottante,
De l’avenir est le pâle falot,
Fraternité, rends-la donc éclatante…
Frères proscrits, vous reviendrez bientôt !


Ne pleurez pas, pères, si la présence
D’êtres chéris vous manque chaque jour,
Ces orphelins que fera votre absence,
Seront nos fils jusqu’à votre retour.
Ils grandiront en dépit des entraves,
Dieu bénira ce précieux dépôt !
Pour leur apprendre à mourir en vieux braves,
Frères proscrits, vous reviendrez bientôt !


Fiers gouvernants, c’est votre jour de gloire,
Chantez en cœur l’antique Te Deum,
Allez, allez proclamer la victoire
Au carrefour, au palais, au forum.
Vaillans sabreurs, à vous ils s’abandonnent,
Tout est fini… la force est votre lot !
Honte aux bourreaux ! gloire à ceux qui pardonnent !
Frères proscrits, reviendrez-vous bientôt ?…


A. Loynel.

[Le Républicain Lyrique n°7, Durand / Beaulé et Maignand.]



