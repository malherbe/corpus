DPT
———
DPT_2

Pierre DUPONT
1821-1870

════════════════════════════════════════════
ŒUVRES IMPRIMÉES À PARIS ENTRE 1848 ET 1851 
DE TRENTE-ET-UN CHANSONNIERS DITS « POPULAIRES »

1851

1359 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DES OUVRIERS, PAR PIERRE DUPONT.
	─ poème	DPT214	LE CHANT DES OUVRIERS.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DES ÉTUDIANTS, PAR PIERRE DUPONT.
	─ poème	DPT215	LE CHANT DES ÉTUDIANTS.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DES PAYSANS, PAR PIERRE DUPONT.
	─ poème	DPT216	LE CHANT DES PAYSANS.
	─ poème	DPT217	LE CHANT DES SOLDATS.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DES TRANSPORTÉS, PAR PIERRE DUPONT.
	─ poème	DPT218	LE CHANT DES TRANSPORTÉS.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DU PAIN, PAR PIERRE DUPONT.
	─ poème	DPT219	LE CHANT DU PAIN.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LA ROMANCE DU PEUPLIER PAR PIERRE DUPONT.
	─ poème	DPT220	LA ROMANCE DU PEUPLIER.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE CHANT DU VOTE PAR PIERRE DUPONT.
	─ poème	DPT221	LE CHANT DU VOTE.
─ poème	DPT222	CHANT DES NATIONS
─ poème	DPT223	LE CHAUFFEUR DE LOCOMOTIVE.
─ poème	DPT224	LA CHANSON DE LA SOIE
─ poème	DPT225	L’HOSPITALITÉ.
─ poème	DPT226	DIEU SAUVE LA RÉPUBLIQUE.
─ poème	DPT227	LE COCHON.
─ poème	DPT228	MON ÂNE.
─ poème	DPT229	LA VÉRONIQUE.
─ poème	DPT230	KOSSUTH.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ÉPREUVE POUR LES CITOYENS MEMBRES DE LA COMMISSION DU MONUMENT.
	─ poème	DPT231	HÉGÉSIPPE MOREAU
