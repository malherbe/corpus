Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
BAS
BAS_1

Victor BASIÈRE


ŒUVRES IMPRIMÉES À PARIS ENTRE 1848 ET 1851 DE TRENTE-ET-UN CHANSONNIERS DITS « POPULAIRES »
1832-1849
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Filles du peuple ?Pour une stylistique de la chanson au XIXe siècle
Romain Benini

ENS Éditions
2021
Corpus de textes fourni par l’auteur
_________________________________________________________________
Édition de référence pour les corrections métriques :



┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



LA VOIX DU PEUPLE
Par Victor BASIÈRE.
Air :Gardez vos dieux, vos plaisirs et vos fers. Ou :
Voilà pourquoi je suis républicain.


Le peuple hait les querelles princières,
Il sait par cœur sa féodalité ;
Fiers prétendants reployez vos bannières,
Il a conquis et veut sa liberté !
Altesse ! ainsi vous nomment vos recrues,
En vous portant lourdement au pavois.
Entendez-le crier de par les rues !
Non, Monseigneur, vous n’aurez pas ma voix ! bis.


Eh quoi ! dit-il, en sa juste colère,
Le regard morne, et le front abattu
Patiemment je souffre la misère !
Et pour un roi je me serais battu ?
Ah ! c’en est trop ; mon courage civique
Par ce méfait serait mis aux abois ;
Pour commander à notre République.
Non, Monseigneur, vous, etc.


Puis il ajoute avec force ironie,
De l’Empereur neveu trop colossal !
Tu peux très-bien, de son vaste génie,
Pour t’élever, te faire un piédestal.
Triste géant ! devant son auréole
En tremblottant s’inclinèrent les Rois ;
Je ne veux pas que sa gloire on lui vole.
Non, Monseigneur, vous, etc.


Oserais-tu parler de tes conquêtes
Revendiqueur un fait de ta valeur,
Dans ton histoire on y lit que défaites
Strasbourg, Boulogne, et non gloire et grandeur.
Ton insuccès porte une rude atteinte
A ton grand nom fustigé tant de fois.
Ton Aigle encor de sang français est teinte.
Non, Monseigneur, vous, etc.


Malgré tes torts au sein de l’Helvétie
J’ai dû t’élire ici représentant.
Plus d’exilés, non ma noble patrie
Doit à ses fils son soleil éclatant.
As-tu tonné du haut de ta tribune
Contre les gens qui lacèrent nos droits ?
Chaque chartiste a contre toi rancune.
Non, Monseigneur, vous, etc.


Plus de blasons ! en cendre est le vieux trône ?
Le monde entier nous tend sa large main
Les hauts titrés reçoivent notre aumône
Et le progrès nivelle tout chemin :
De royauté la France est affranchie,
Nous existons sous de communes Lois
Je sais combien vaut une monarchie.
Non, Monseigneur, vous, etc.



[Imp. de Édouard Bautruche, rue de la Harpe, 90.]




PAROLES DU GÉNÉRAL CAVAIGNAC 
EN FAVEUR DES TRANSPORTÉS.
Air :  Tendres échos errants dans ces vallons.

Sa voix m’a dit en comblant mon espoir :
Il est encor pour tous des jours prospères ;
Moi, président du souverain pouvoir,
Je vous rendrai vos bons amis, vos frères.


(bis.)
A tout captif, qui rend la liberté,
De la patrie a cent fois mérité.


Pauvres captifs, vous reverrez le jour ;
Il ne peut pas manquer à sa parole,
En attendant votre prochain retour,
Que ma chanson vous ranime et console.


A tout captif, etc.



Femmes, bientôt reviendront vos époux
Vous consoler de leur trop longue absence.
Ce jour sera pour vous un jour bien doux,
Cent fois béni par la reconnaissance ?


A tout captif, etc.



Gentils enfants, ne vous désolez pas ;
Vous sourirez à la voix de vos pères,
Les étreignant de vos deux petits bras ;
Vous oublierez vos maux et vos misères.


A tout captif, etc.



Contre des serfs s’il avait combattu,
Il ne voudrait faire acte de clémence.
Gloire au vainqueur, et respect au vaincu !
Voilà le cri des enfants de la France.


A tout captif, etc.



Si des maudits nous poussaient au combat,
Brisons sur eux nos armes meurtrières.
Francs citoyens, ne soyons plus soldat
Que pour défendre ou garder nos frontières.


(bis.)
A tout captif, qui rend la liberté,
De la patrie a cent fois mérité.


Victor BASIERE.
Imp. de Édouard Bautruche, rue de la Harpe, 90.



APPEL AU PEUPLE !
Air :Aux armes, matelots ! (V. B.) Ou : Salut, trône d’airain. (Guilleme.)

Debout, républicains, répondez à ma voix ;
Les peuples vont s’unir, ils menacent les rois.


En nos murs l’airain tonne et gronde,
Écoutez sa puissante voix ;
Elle annonce à notre vieux monde
Le progrès signalant nos droits.
La bienfaisante et douce ivresse
S’élance joyeuse en les airs.
Elle va charmer l’univers
Par des chants de vive allégresse.


Debout, etc.



Plus de légions fratricides
Chez nous, plus de sanglants combats ;
Gardons nos armes homicides
Pour combattre les potentats ;
Cessons ces intestines guerres,
Où chacun va braver la mort
Et conserve en soi le remords
D’avoir au cœur frappé des frères.


Debout, etc.



Pour garder notre indépendance,
Repoussons chaque prétendant.
Libre, il faut que soit notre France
Forte et noble son président ;
Gardons-nous de croire aux caresses
Le passé nous en fait la loi.
Le peuple seul doit être roi,
Le peuple tient à ses promesses.


Debout, etc.



Quoi ! chaque race dynastique,
En des jours déjà fortunés,
Viendrait, en notre république,
Nous imposer ses nouveaux nés.
Pitié ? la nation est grande ;
Comprimez ce juste courroux,
Amis, protection pour tous,
L’humanité nous le commande.


Debout, etc.



Contre de futiles atômes,
Vous vous insurgez sans raison ;
Vous ne rêvez plus que fantômes,
Réaction et trahison.
Enfants, eh quoi ! de votre force
Oseriez-vous déjà douter ?
Eh bien ? sur vous j’ose compter,
S’il nous faut brûler une amorce.


Debout, etc.



Par notre entente cordiale
Maîtrisons tout ce qui détruit.
Notre œuvre toute sociale,
Déjà nous offre de son fruit :
La foudre là-bas avec rage
Gronde et menace notre Bord.
Frères, ramons avec accord
Et nous ferons tête à l’orage.


Debout, républicains, répondez à ma voix ;
Les peuples vont s’unir, ils menacent les rois.


Imp. de E. Boutruche, 90, r. de la Harpe.



SALUEZ MONSEIGNEUR.
Air :Ah ! le bel oiseau vraiment.

Saluez votre Seigneur
Mon altesse,
Avec souplesse,
Qui vous porte dans mon cœur.


Boutiquiers, petits bourgeois,
Que je respecte et que j’aime,
Vous êtes libres en choix,
Or, vous m’élirez quand même.
Saluez, etc.





Neveu de votre empereur
Je possède on peut m’en croire,
Son génie et sa valeur
Peut-être un peu moins de gloire.
Saluez, etc.





Élu votre Président
Chef de votre République,
Je ne veux moi prétendant
Que le règne monarchique.
Saluez, etc.





Je veux votre bien à tous,
C’est mon but j’ose y prétendre,
Cela soit dit entre nous
Afin de bien nous entendre,
Saluez, etc.





Pour m’élever au pavois
Chacun des blancs me protège,
Des rouges malgré les voix
J’aurai l’impérial siége.
Saluez, etc.





Mes vassaux les paysans
Déjà me prônent d’avance,
Les serfs son mes partisans
Du czar j’en ai l’assurance.
Saluez, etc.





Par un fin coup de Jarnac,
Les élections j’entrave
Pour supplanter Cavaignac
Après tout qui n’est qu’un brave.
Saluez, etc.





De mon aigle impérial
Je veux enrichir la France,
Et qu’à son bec martial
On suspende la balance.
Saluez, etc.





Une fois ton maître enfin
Peuple hardi qui me raille,
Tu marcheras droit vilain
Ou je t’impose la taille.


Saluez votre Seigneur
Mon altesse,
Avec souplesse,
Saluez votre Seigneur,
Qui vous porte dans mon cœur.



────────────────────────────────────── 

Imp. de E. Bautruche, 90, r. de la Harpe.




CHANSONS NATIONALES
DE
VICTOR BASIÈRE.




Aux préjugés point de grâce. 

A la souple adulation, 

Sur l’intrigue allons main basse 

Et sur la corruption.




 
PARIS,
CHEZ TERRY, LIBRAIRE, PALAIS-NATIONAL, 185,
PÉRISTYLE VALOIS.
Etes Libraires des Départements et de l’étranger.
1849.
[IMPRIMERIE CENTRALE DE NAPOLÉON CHAIX ET CIE RUE BERGÈRE]


MOUSTACHE,
EX-TROUPIER DU P’TIT.
Air : Je suis militaire, etc.

En avant ! courage !
Marchons les premiers !
Du cœur à l’ouvrage,
Braves ouvriers !


Enfants, quittons notre faubourg
Où la misère nous harcèle,
Fuyons ! Mais j’entends le tambour
Qui dans notre quartier rappelle :
Soutenons-nous et serrons bien les rangs,
Suivez Moustache, il est à votre tête ;
Marchons, enfants, et qu’en chœur on répète (bis)
Ce vieux refrain des Gaulois et des Francs :
Aux armes ! chassons les tyrans. (bis)


Nous voulons vivre en travaillant,
Voilà notre simple devise ;
Nous, les fils d’un peuple vaillant,
Attendrons-nous qu’on nous divise ?
Attendrons-nous, qu’affaiblis par la faim,
Et que gisant sur quelques brins de paille,
Du haut pouvoir la digne valetaille, (bis)
En nous perçant, hâte encor notre fin ?
Debout !! Levons-nous donc enfin. (bis)


Nous diront-ils ambitieux ?
Ils exploitent jusqu’à l’ordure.
Nous diront-ils audacieux ?
Ils nous provoquent par l’injure.
Prétendront-ils que l’argent nous conduit ?
Nos pieds sont nus, notre haleine est brûlante,
Notre démarche est presque chancelante (bis)
Un seul grabat orne notre réduit,
Un morceau de pain nous séduit. (bis)


Morbus nous donne le trépas,
Le gendarme nous espadonne,
La mouche pique et suit nos pas,
Le Grand-Ordre nous emprisonne,
Le fier dragon nous charge effrontément,
Et l’assommeur à cogner s’évertue,
Le vil sergent vient derrière et nous tue, (bis)
Le coq nous ruine et marche fièrement,
La faim nous mine lentement. (bis)


Nous avons pleine liberté
De tout entendre et de nous taire ;
Notre étendard est respecté
Comme les droits du prolétaire ;
Le vieux soldat reçoit un traitement
S’il a brillé dans la chouannerie ;
Ainsi le veut une chambre flétrie (bis)
N’en doutez plus, mes amis, on nous vend,
Car le milieu tourne à tout vent. (bis)


Enfants, confiez-vous à moi,
J’ai longtemps servi le grand homme,
Mon cœur ressent un doux émoi
Lorsque ma bouche vous le nomme.
Mais il n’est plus !! faut chasser ces pékins,
Au cri sacré : Vive la république !
Ce nom recèle un pouvoir électrique. (bis)
A bas le traître ! A bas tous les pasquins !
Et vivent les républicains ! (bis)


En avant ! courage !
Marchons les premiers !
Du cœur à l’ouvrage,
Braves ouvriers !



SOLDATS, SOYONS UNIS ET FRÈRES.
 Air : Laissez reposer le tonnerre.

Frappant sur son trône d’airain,
En faisant gronder son tonnerre,
Des dieux le maître souverain
Exhalait par ces mots sa bouillante colère :
Mortels, vous méprisez les fleurs
Qui ceignaient le front de vos pères ;
Sous la bannière aux trois couleurs
Vous devez être unis et frères. (bis.)


Soldat, où portes-tu tes pas ?
Je vois ta main brandir un glaive.
Arrête ! tu cours au trépas ;
Les vents sont déchaînés et la trombe s’élève ;
Laisse-la venger nos malheurs,
Plus de digue aux flots populaires ;
Sous, etc.



Regarde ce bras mutilé
Et ces nombreuses cicatrices ;
Contemple du pauvre exilé
Le signe révéré de ses brillans services.
De ses yeux vois couler des pleurs,
En lisant des lois sanguinaires.
Sous, etc.



Brisant leurs nobles boucliers,
Lorsque les trahit la victoire,
Chargés de fers et de lauriers,
On vit fuir des héros couronnés par la gloire.
Pour calmer leurs vives douleurs,
Pour mettre un terme à leurs misères,
Sous, etc.



Peux-tu parler de tes succès
Et revendiquer leur mémoire ?
Crois-tu que, teint du sang français,
Ton nom puisse embellir les pages de l’histoire.
Crois-tu que la palme aux vainqueurs
S’offre au bruit de chants funéraires.
Sous, etc.



Plus on nous accable de fers,
Et plus le despote t’opprime ;
Plus nous éprouvons de revers,
Plus de tes chefs ingrats tu deviens la victime.
Abandonne nos oppresseurs,
Oublions nos sanglantes guerres ;
Sous la bannière aux trois couleurs ;
Soldats, soyons unis et frères.



LE CUISINIER DE L’ENFER.
 Air : La nuit porte conseil.

Cuisiniers,
Pâtissiers,
Allons, du courage !
A l’ouvrage,
Qu’on s’empresse, morbleu !
De faire pétiller le feu.
Le maître des enfers
Donne une grande fête ;
Que cent mille couverts
Aux démons soient offerts.
Des marmitons des dieux
Provoquons la défaite,
Nos mets délicieux
Embaument l’air des cieux.

Marquez les sauces.

Aiguisons les couteaux,
Affilons la lardoire,
Garnissons les réchauds,
Et comblons les fourneaux ;
Mettons le feu partout
Et volons à la gloire ;
Assurons notre goût,
En goûtant le ragoût.

L’Espagnol sur glace.

Il faut qu’en ce séjour
L’on serve sur les tables
Salmis, rôts, tour à tour :
C’est le menu du jour.
Le cœur de l’immortel
Doit régaler les diables,
Enduit d’une dursel
A la sauce au gros sel.

Dressez le ragoût à la royale.

Servons les cœurs de rois
En sauce à la Tartare ;
Profitons de nos droits,
Au moins pour cette fois.
Ils sont tous des tyrans,
Et leur glaive barbare
Décime encore les rangs
De leurs peuples souffrants.

Dressez les brochets au bleu.

Ceux des valets titrés
Ne sont qu’une vétille :
Ces esclaves dorés
Sont toujours ulcérés.
Or, vite, préparons
Une sauce d’anguille ;
Ainsi nous offrirons
Ces infâmes larrons.

Dresses le gratin de mauviettes à la monarque.

Les cœurs de nos ventrus,
Mettons-les à la broche ;
Ils sont gras et dodus,
Ils donneront du jus.
Habiles en moyens
Pour garnir leur sacoche,
Des pauvres citoyens
Ils censurent les biens.

Dressez les filets d’oies bigarrades.

Des sujets du clissoir
La viande est bien mesquine ;
Je cherche en mon savoir
Dans quel plat les asseoir.
Utilisons les pots :
La sauce crapaudine
Se trouve être à propos
Pour arroser ces sots.

Dressez les canards à la chipolata.

Le cœur républicain
Est notre pièce fine ;
Voyez comme il est sain,
Comme son goût est fin.
De son suc nourricier
Formons la gélatine ;
Un jour le monde entier
Saura l’apprécier.

Dressez le suprême à l’écarlate.


L’ARTILLEUR NATIONAL.
 Air : Voilà, voilà, voilà, voilà, etc.

Un artilleur, c’est un bel homme
Qu’est joliment conditionné ;
Il fume, il boit, et faut voir comme
Il est en tout discipliné ; (bis)
Il se dévoue à sa patrie,
Il aime la plaisanterie,
Il a le cœur franc, jovial : (bis.)
Voilà (4 fois) l’artilleur national.


Il chante son couplet à table,
En aimable et gai citoyen ;
Il est toujours infatigable
Pour s’exercer à faire bien ; (bis.)
Puis il enjôle la fillette,
Et lorsqu’il la rend rondelette,
Il fredonne un air triomphal. (bis)
Voilà, etc.


Jamais il ne lança la foudre.
Sur un frère ou sur un ami ;
Jamais il ne réduit en poudre,
Que chez l’arrogant ennemi, (bis.)
Jamais il ne trahit la France
Et n’asservit l’indépendance ;
Il sait mépriser l’or vénal. (bis.)
Voilà, etc.


Il combat sur mer et sur terre,
Avec une égale valeur ;
Il est l’ami du prolétaire
Et l’ennemi du grand seigneur ; (bis.)
Il frappe au cœur le monarchique,
Et pour la fière République,
Il fait tonner l’airain brutal. (bis.)
Voilà, etc.


Des femmes enfin c’est l’idole,
Et l’ami de tout bon français ;
Sa tête est tant soit peu frivole :
C’est le garant de ses succès ; (bis.)
Il est chéri de la victoire
Et le soutien de notre gloire ;
Enfin, il est grand et loyal. (bis.)
Voilà (4 fois) l’artilleur national.



VAINCRE OU MOURIR.
Air : A boire ! à boire ! à boire !…

En avant ! plus de larmes !
Patriotes, c’est trop souffrir.
Aux armes ! (bis)
Vaincre ou mourir !


Qu’à ma voix chacun se rallie,
Courons soutenir l’Italie ;
Marchons ! le signal est donné !
Ah ! pour nous, quel beau jour, quel beau jour fortuné !
L’heure de vengeance a sonné.


En avant, etc.





Écoutez sa voix en silence,
Qui vers elle appelle la France ;
Puis ses pleurs reprennent leurs cours,
Méprisons les traités, oui les traités des cours !
Frères, volons à son secours.


En avant, etc.





Ses fils ne sont-ils pas nos frères ?
Ils ont marché sous nos bannières ;
Ils demandent la liberté,
Ils veulent proclamer la sainte égalité,
Et sur notre aide ils ont compté.


En avant, etc.





De vils sicaires les égorgent,
De leur or, de leur sang se gorgent.
Laisserons-nous les lourds Germains
En tous points accomplir leurs infâmes desseins ?
Mort à ces lâches assassins !


En avant, etc.





Soldats ! à bas la baïonnette !
Je vois maint pouvoir qui s’apprête
A massacrer les libéraux ;
Vous êtes les enfants d’un peuple de héros,
Ah ! ne soyez pas leurs bourreaux !


En avant, etc.





Prodiguer de fausses caresses,
Oublier toutes leurs promesses,
Frustrer les peuples de leurs droits,
A leurs nombreux bourreaux donner titres et croix !
Voilà les papes et les rois.


En avant, etc.






UNE LARME AU COURAGE.
Air : Et l’ordre règne à Varsovie.

Le bronze fait trêve aux échos,
La mort commande le silence,
Le glaive coûte le repos
Et Clio vers les cieux s’élance ;
Les rayons aux pâles couleurs
Éclairent un champ de carnage ;
J’y viens répandre quelques fleurs, (bis)
Donner une larme au courage.


C’est là que de jeunes héros,
Conduits par un pouvoir magique,
Ont fait trembler les vils bourreaux
De la puissance monarchique.
C’est là qu’en ces jours de malheurs
S’est joint le massacre à l’outrage.
J’y viens, etc.



Des sbires au front éhonté,
D’un sang pur arrosant la terre,
Profanaient le mot liberté
En entraînant le prolétaire.
Suivant ces perfides meneurs,
Il expira sur cette plage.
J’y viens, etc.



C’est là que de faibles remparts
Tinrent en arrêt le tonnerre,
Osèrent borner ses regards
Et braver sa mâle colère.
Ils ont dans leurs flancs protecteurs
Pour l’histoire une grande page.
J’y viens, etc.



Ivre d’un sanguinaire orgueil,
Cette aristocratique race,
Jetant les vaincus au cercueil,
A dérobé jusqu’à leur trace ;
Mais les sillons révélateurs
Les offriront au sarcophage,
Nos mains les orneront de fleurs,
Nos chants fêteront leur courage.


Un bruit sourd m’annonce minuit,
Sa voix encore au loin murmure.
J’évite un regard qui me suit :
Mon manteau cache ma blessure.
Je fuis de ces lieux la douleur,
Sur ma tête gronde un orage,
Il passe et respecte les fleurs (bis)
Qui ceignent le front du courage.



Sainte Pélagie,
1832.





