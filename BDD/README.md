<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Base de données du corpus 

* Répertoire **CSV** : Tables pour une base de données du corpus.
Les champs des fichiers CSV sont délimités par des tabulations.

* Répertoire **SQL** : requêtes SQL pour reconstituer la base de données du corpus
* un fichier sql pour chaque table
* un fichier sql pour la structure de la base de données (malherbe)
* un fichier sql pour l'ensemble structure et données de la base de données (malherbe)

* Répertoire **PDF** : fichiers de description des tables de la base de données
  Un fichier ..._sql.pdf donne le formatage des tables pour une base de données MySqL ou MariadB.

* Répertoire **TXT** : fichier TXT des données pour la carte proportionnelle interactive du corpus
  (données incluses dans le fichier **treemap_corpus.html** du répertoire HTML).
  Fichier créé par le programme Python  **statistiques_auteurs.py** du répertoire Scripts.

* Répertoire **Graphiques** : Graphiques (formats SVG et PNG) créés par le programme
  d'analyse statistique du corpus **statistiques_auteurs.py**, lancé à partir du script
  Bash **statistiques_auteurs.sh** du répertoire Scripts.

Les tables de la base de données sont encodées en UFT-8. Le nom de quelques tables comporte
une ou plusieurs lettres accentuées. Si nécessaire, renommez ces tables pour éviter tout conflit d'encodage.

