/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19  Distrib 10.6.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: malherbe
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `corpus_état`
--

DROP TABLE IF EXISTS `corpus_état`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_état` (
  `NBR_AUTEURS` int(11) NOT NULL COMMENT 'nombre d''auteurs',
  `NBR_RECUEILS` int(11) NOT NULL COMMENT 'nombre de recueils',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces de théâtre',
  `NBR_VERS` int(11) NOT NULL COMMENT 'nombre de vers',
  `NBR_MOTS` int(11) NOT NULL,
  `DATE_CORPUS` varchar(20) NOT NULL COMMENT 'date du traitement',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement (format SQL)'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='extension du corpus';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corpus_état`
--

LOCK TABLES `corpus_état` WRITE;
/*!40000 ALTER TABLE `corpus_état` DISABLE KEYS */;
INSERT INTO `corpus_état` VALUES (307,648,26,143,1,10,'février 2025','2025-02-04');
/*!40000 ALTER TABLE `corpus_état` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-04 20:03:46
