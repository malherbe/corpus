/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19  Distrib 10.6.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: malherbe
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `corpus_coauteurs`
--

DROP TABLE IF EXISTS `corpus_coauteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_coauteurs` (
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil ou de la pièce',
  `ID_AUTEURS` text NOT NULL COMMENT 'identifiants des auteurs',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des coauteurs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corpus_coauteurs`
--

LOCK TABLES `corpus_coauteurs` WRITE;
/*!40000 ALTER TABLE `corpus_coauteurs` DISABLE KEYS */;
INSERT INTO `corpus_coauteurs` VALUES ('AED_1','ARA DUV','2025-02-04'),('BEC_1','BRA CCH','2025-02-04'),('BEV_1','BEA VIC','2025-02-04'),('BLV_1','BLL VAN','2025-02-04'),('BTL_1','BRU THO LVY','2025-02-04'),('DCB_1','DRT CDB','2025-02-04'),('DDC_1','DPY COU','2025-02-04'),('DLM_1','DLM RCH','2025-02-04'),('DLV_1','DUV DLV','2025-02-04'),('DMP_1','DML DNR','2025-02-04'),('DSE_1','DUV SAI ARA','2025-02-04'),('EVC_1','ETI VRN DVR','2025-02-04'),('FCR_1','COU JAI','2025-02-04'),('FED_1','FTN DNY','2025-02-04'),('JRL_1','ARJ LUR','2025-02-04'),('LRL_1','DLU LAN','2025-02-04'),('MVG_1','MAS VIL GAB','2025-02-04'),('PBC_1','PIX BRA CCH','2025-02-04'),('RBC_1','RGM BRA COU','2025-02-04'),('SCB_2','SCR BYD','2025-02-04'),('SCB_3','SCR BYD','2025-02-04'),('SND_1','SAI NSL DUV','2025-02-04'),('SVB_1','SAU BYD','2025-02-04'),('SVD_1','SAI VIL DPY','2025-02-04'),('SVD_2','SAI VIL DPY','2025-02-04'),('TVL_1','THO LVY','2025-02-04'),('VEM_1','VIL MAS','2025-02-04');
/*!40000 ALTER TABLE `corpus_coauteurs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-04 20:03:45
