/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19  Distrib 10.6.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: malherbe
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `corpus_auteurs`
--

DROP TABLE IF EXISTS `corpus_auteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_auteurs` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `PRENOM` varchar(30) NOT NULL COMMENT 'prénom de l''auteur',
  `NOM` varchar(30) NOT NULL COMMENT 'nom de l''auteur',
  `NOM_PLUME` varchar(30) DEFAULT NULL COMMENT 'nom de plume',
  `PARTICULE` varchar(6) DEFAULT NULL COMMENT 'particule',
  `DATES_AUTEUR` text NOT NULL COMMENT 'dates de naissance et de décès',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_AUTEUR`),
  KEY `index2` (`NOM`,`NOM_PLUME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des auteurs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_auteurs_HF`
--

DROP TABLE IF EXISTS `corpus_auteurs_HF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_auteurs_HF` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `PRENOM` varchar(30) NOT NULL COMMENT 'prénom de l''auteur',
  `NOM` varchar(30) NOT NULL COMMENT 'nom de l''auteur',
  `SEXE` set('M','F') DEFAULT NULL COMMENT 'sexe de l''auteur',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_AUTEUR`),
  UNIQUE KEY `ID_AUTEUR` (`ID_AUTEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des auteurs (homme ou femme)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_coauteurs`
--

DROP TABLE IF EXISTS `corpus_coauteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_coauteurs` (
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil ou de la pièce',
  `ID_AUTEURS` text NOT NULL COMMENT 'identifiants des auteurs',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des coauteurs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_licences`
--

DROP TABLE IF EXISTS `corpus_licences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_licences` (
  `ID_RECUEIL` varchar(8) NOT NULL COMMENT 'identifiant de l''auteur',
  `CORPUS` text NOT NULL,
  `TYPE_LICENCE` set('free','restricted') NOT NULL COMMENT 'type de licence',
  `LICENCE` text NOT NULL COMMENT 'référence de la licence',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_RECUEIL`),
  KEY `index1` (`ID_RECUEIL`),
  KEY `index2` (`TYPE_LICENCE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='table des licences';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_nombres`
--

DROP TABLE IF EXISTS `corpus_nombres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_nombres` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant : auteur et recueil',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `AUTEUR` varchar(100) NOT NULL COMMENT 'nom complet de l''auteur',
  `ID_RECUEIL` varchar(8) NOT NULL COMMENT 'identifiant du recueil',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil',
  `DATE_EDITION` varchar(20) NOT NULL COMMENT 'date d''édition',
  `DATE_EDITION_INT` int(4) NOT NULL COMMENT 'date d''édition (année)',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes du recueil',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces du recueil',
  `NBR_VERS_POEMES` int(11) NOT NULL COMMENT 'nombre de vers dans les poèmes',
  `NBR_VERS_PIECES` int(11) NOT NULL COMMENT 'nombre de vers dans les pièces',
  `NBR_VERS_TOTAL` int(11) NOT NULL COMMENT 'nombre total de vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des données statistiques du corpus';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_préparation`
--

DROP TABLE IF EXISTS `corpus_préparation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_préparation` (
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil ou de la pièce de théâtre',
  `AUTEUR` varchar(100) NOT NULL COMMENT 'identifiant de l''auteur',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil ou de la pièce',
  `DATE_CREATION` varchar(20) NOT NULL COMMENT 'date de création',
  `INTERVENANT` text NOT NULL COMMENT 'personne(s) ayant contribué à la préparation du texte',
  `TACHES` text NOT NULL COMMENT 'liste des tâches effectuées',
  `DATE_PREP` varchar(4) NOT NULL COMMENT 'édition imprimée de référence ',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des participants et des tâches effectuées';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_recueils`
--

DROP TABLE IF EXISTS `corpus_recueils`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_recueils` (
  `ID` varchar(15) NOT NULL COMMENT 'identifiant : auteur et recueil',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(8) NOT NULL COMMENT 'identifiant du recueil',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil',
  `DATE_EDITION` varchar(20) NOT NULL COMMENT 'date de création',
  `DATE_EDITION_INT` int(11) NOT NULL COMMENT 'date de création (année)',
  `TYPE_TEXTE` varchar(30) NOT NULL COMMENT 'type de texte : poème ou pièce de théâtre',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces de théâtre',
  `NBR_VERS` int(11) NOT NULL COMMENT 'nombre total de vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table statistique des recueils';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_références`
--

DROP TABLE IF EXISTS `corpus_références`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_références` (
  `ID_RECUEIL` varchar(10) NOT NULL COMMENT 'identifiant du recueil',
  `AUTEUR` varchar(100) NOT NULL COMMENT 'identifiant de l''auteur',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil',
  `DATE_CREATION` varchar(20) NOT NULL COMMENT 'date de création',
  `DATE_CREATION_INT` int(11) NOT NULL COMMENT 'date de création (année)',
  `TYPE_TEXTE` varchar(10) NOT NULL COMMENT 'type de texte : poème ou pièce de théâtre',
  `FORME` varchar(100) NOT NULL COMMENT 'genre du texte (comme sous-titre)',
  `ID_POEME` varchar(10) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TITRE_POEME` text NOT NULL COMMENT 'titre du poème ou de la pièce',
  `TITRE_PARTIE` varchar(100) NOT NULL COMMENT 'titre de la partie du recueil',
  `TITRE_SOUS_PARTIE` varchar(100) NOT NULL COMMENT 'titre de la sous-partie du recueil',
  `INCIPIT` text NOT NULL COMMENT 'premier vers',
  `POEME_MANQUANT` text NOT NULL COMMENT 'présence éventuelle d''une information relative à l''absence d''un poème',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_POEME`),
  KEY `index1` (`ID_POEME`),
  KEY `index3` (`ID_RECUEIL`),
  KEY `index4` (`AUTEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table de référencement des textes : poèmes et pièces de théâtre';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_sources`
--

DROP TABLE IF EXISTS `corpus_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_sources` (
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil',
  `AUTEUR` varchar(100) NOT NULL COMMENT 'nom de l''auteur',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil ou de la pièce',
  `DATE_EDITION` varchar(50) NOT NULL COMMENT 'date d''édition (première publication)',
  `DATE_EDITION_INT` int(4) DEFAULT NULL,
  `SOURCE` text NOT NULL COMMENT 'source numérique du texte',
  `ORIGINE` text NOT NULL COMMENT 'version imprimée à l''origine de la source',
  `EDITION_REF` text NOT NULL COMMENT 'édition imprimée de référence',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_RECUEIL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des sources numériques et imprimées';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_statistiques`
--

DROP TABLE IF EXISTS `corpus_statistiques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_statistiques` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `AUTEUR` varchar(100) NOT NULL COMMENT 'nom de l''auteur',
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil',
  `TITRE_RECUEIL` varchar(100) NOT NULL COMMENT 'titre du recueil ou de la pièce',
  `DATE_CREATION` int(11) NOT NULL COMMENT 'date de création connue ou supposée',
  `SIECLE` int(11) NOT NULL COMMENT 'siècle de l''auteur',
  `NBR_RECUEILS` int(11) NOT NULL COMMENT 'nombre de recueils',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre total de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre total de pièces de théâtre',
  `NBR_VERS_POEMES` int(11) NOT NULL COMMENT 'nombre total de vers dans les poèmes',
  `NBR_VERS_PIECES` int(11) NOT NULL COMMENT 'nombre total de vers dans les pièces',
  `NBR_VERS_TOTAL` int(11) NOT NULL COMMENT 'nombre total de vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des données statistiques par siècles, auteurs et recueils';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_tags`
--

DROP TABLE IF EXISTS `corpus_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_tags` (
  `ID_RECUEIL` varchar(7) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `NUM_VERS` int(11) NOT NULL COMMENT 'numéro du vers',
  `BALISE` varchar(10) NOT NULL COMMENT 'type de balise XML-TEI introduite',
  `RAISON` varchar(20) NOT NULL COMMENT 'justification de la balise XML-TEI introduite',
  `ATTR_RAISON` varchar(20) NOT NULL COMMENT 'contenu de l''attribut @raison',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `PARTIE_MODIF` text NOT NULL COMMENT 'partie du texte modifiée',
  `AJOUT/SUBST` text NOT NULL COMMENT 'ajout ou substitution',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des ajouts de balises spéciales dans le texte';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_vers`
--

DROP TABLE IF EXISTS `corpus_vers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_vers` (
  `ID_VERS` varchar(13) NOT NULL COMMENT 'identifiant unique',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(10) DEFAULT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(12) DEFAULT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) DEFAULT NULL COMMENT 'type de texte : recueil de poèmes ou pièce de théâtre',
  `NUM_ABS` int(11) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers reconstitué',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID_VERS`),
  KEY `index2` (`ID_POEME`),
  KEY `index4` (`ID_RECUEIL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Table des vers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corpus_état`
--

DROP TABLE IF EXISTS `corpus_état`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpus_état` (
  `NBR_AUTEURS` int(11) NOT NULL COMMENT 'nombre d''auteurs',
  `NBR_RECUEILS` int(11) NOT NULL COMMENT 'nombre de recueils',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces de théâtre',
  `NBR_VERS` int(11) NOT NULL COMMENT 'nombre de vers',
  `NBR_MOTS` int(11) NOT NULL,
  `DATE_CORPUS` varchar(20) NOT NULL COMMENT 'date du traitement',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement (format SQL)'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='extension du corpus';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-04 20:03:46
