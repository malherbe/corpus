<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES CARRIÈRES DE JAUMONT</title>
				<title type="sub">OU VENGEANCE DE QUATRE PAYSANS</title>
				<title type="sub">SOUVENIR ÉPISODIQUE DE LA GUERRE ACTUELLE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLN">
					<name>
						<forename>Gabriel</forename>
						<surname>DELAUNAY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES CARRIÈRES DE JAUMONT</title>
						<author>Gabriel Delaunay</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54429491/f4.image.r=LES%20CARRI%C3%88RES%20DE%20JAUMONT</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES CARRIÈRES DE JAUMONT</title>
								<author>Gabriel Delaunay</author>
								<imprint>
									<pubPlace>BORDEAUX</pubPlace>
									<publisher>FERET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<front>
			<p>
				A Madame E.C…
				MADAME, 
				Sachant tout ce qu’un esprit aussi parfaitement français que 
				le vôtre peut trouver d’intéressant dans l’histoire de notre 
				pays, je prends la liberté de vous dédier ce petit poëme. 
				G. DELAUNAY.
			</p>
		</front>
		<body>
			<div type="poem">
				<head type="main">LES CARRIÈRES DE JAUMONT</head>
				<lg>
					<l>Vengeance ! — avaient-ils dit, dans leur âpre douleur.</l>
					<l>Leurs yeux restèrent secs, mais la haine en leur cœur</l>
					<l>Avec ses doigts d’acier fit une déchirure,</l>
					<l>Et les pleurs détournés inondaient leur blessure !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Satan avait dû voir déborder ce torrent ;</l>
					<l>Satan leur mit au cœur la rage du serpent.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Ah ! c’est qu’il est venu le temps des représailles :</l>
					<l>La vengeance a des droits en dehors des batailles,</l>
					<l>Elle peut sous les fleurs cacher des échafauds</l>
					<l>Quand les soldats déchus ne sont que des bourreaux,</l>
					<l>D’infâmes suborneurs, effroi de nos familles,</l>
					<l>Qui, jusque chez les morts, vont insulter aux filles !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Donc, quatre paysans, par l’outrage éprouvés,</l>
					<l>Avaient juré la mort des soldats réprouvés :</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>De la pitié la haine a franchi les barrières !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Jaumont à leurs pensers vient offrir des carrières</l>
					<l>Sous un crâne poudreux aux profonds creusements</l>
					<l>Zébrés de ça, de là, par des soutènements.</l>
					<l>Soudain des offensés le projet redoutable</l>
					<l>Est de vanter la place heureuse, inexpugnable,</l>
					<l>Aux avides Teutons, voulant sur les Latins</l>
					<l>Fondre de ces hauteurs en parfaits assassins :</l>
					<l>Ils s’élancent hardis sur la scène mouvante ;</l>
					<l>Des Latins prévenus l’attitude imposante</l>
					<l>Attend avec stupeur un ordre solennel :</l>
					<l>« Feu ! » nous crie une voix… oh ! spectacle éternel,</l>
					<l>Si je ferme les yeux qu’anime ma pensée !!!</l>
					<l>Des ennemis par nous la masse est repoussée,</l>
					<l>Plus loin elle recule et plus proche est la mort.</l>
					<l>Une attaque opposée a décidé son sort :</l>
					<l>Dans l’excavation l’airain fatal résonne,</l>
					<l>Le gouffre^qui s’émeut va n’épargner personne,</l>
					<l>Il palpite, il s’entr’ouvre, et les caveaux béants</l>
					<l>S’abreuvent à loisir de milliers d’Allemands !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Une seule clameur, déchirante, indicible,</l>
					<l>S’élève vers le ciel comme un remords horrible,</l>
					<l>Sortant du précipice où, canons et chevaux,</l>
					<l>Se confondent au sein d’insondables tombeaux !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Les Teutons, sans nul doute, eussent à notre place</l>
					<l>D’un hurlement féroce, inhérent à leur race,</l>
					<l>Salué le succès de ce fait glorieux !…</l>
					<l>Mais nous… muets, glacés, les larmes dans les yeux</l>
					<l>Nous mêlions à leurs voix nos ferventes prières,</l>
					<l>Afin qu’un prompt repos vînt fermer leurs paupières.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Vengeance ! avaient-ils dit, dans leur âpre douleur,</l>
					<l>Leurs yeux restèrent secs, mais la haine en leur cœur</l>
					<l>Avec ses doigts d’acier fit une déchirure,</l>
					<l>Et les pleurs détournés inondaient leur blessure !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg>
					<l>Ils n’y furent survivre, et pour n’en plus souffrir,</l>
					<l>Parmi les Allemands ils ont voulu mourir.</l>
					<l>Heureux d’être plus près témoins de leur supplice,</l>
					<l>De leur vie ils ont fait l’immortel sacrifice !!!</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>