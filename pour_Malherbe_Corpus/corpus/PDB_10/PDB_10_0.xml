<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.6.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Hymne à la Victoire</title>
				<title type="medium">Édition électronique</title>
				<author key="PDB">
					<name>
						<forename>Pierre</forename>
						<nameLink>de</nameLink>
						<surname>BOUCHAUD</surname>
					</name>
					<date from="1862" to="1925">1862-1925</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2023">2023</date>
				<idno type="local">PDB_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Hymne à la Victoire</title>
						<author>Pierre de Bouchaud</author>
					</titleStmt>
					<publicationStmt>
						<publisher>numelyo.bm-lyon.fr</publisher>
						<idno type="URL">http://numelyo.bm-lyon.fr/BML:BML_00GOO0100137001104557777</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Hymne à la Victoire</title>
								<author>Pierre de Bouchaud</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE, ÉDITEUR</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédidace et la préface ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées et lettres ligaturées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>L’apostrophe ordinaire (’) a été remplacée l’apostrophe typographique (’)</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-10-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-10-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem">
				<head type="main">Hymne à la Victoire</head>
				<lg n="1">
					<l>Trois fois salut, Victoire, à ton vol radieux !</l>
					<l>Comme un ardent soleil éblouissant les yeux,</l>
					<l><space unit="char" quantity="12"/>Tu sillonnes nos cieux</l>
					<l>Dont on voit frissonner l’azur sous ta grande aile.</l>
					<l>Ta superbe venue est un enchantement.</l>
					<l>Depuis cinquante mois nous guettions ce moment</l>
					<l><space unit="char" quantity="12"/>Où, sur le firmament,</l>
					<l>Tu te profilerais, ô Déesse immortelle.</l>
				</lg>
				<lg n="2">
					<l>Depuis cinquante mois, tandis que, chaque jour,</l>
					<l>Sous les tirs incessants des canons du pandour,</l>
					<l><space unit="char" quantity="12"/>Campagne, cité, bourg,</l>
					<l>Gomme nos preux, étaient tout hachés de blessures,</l>
					<l>Nous t’attendions d’un cœur plus ferme que le roc,</l>
					<l>Tenant bon, et sachant, solides sous le choc,</l>
					<l><space unit="char" quantity="12"/>Rendre estoc pour estoc…</l>
					<l>De ta fidélité nos âmes étaient sûres.</l>
				</lg>
				<lg n="3">
					<l>En vain de ses succès se targuait l’ennemi,</l>
					<l>Affirmant nous avoir tués plus qu’à demi ;</l>
					<l><space unit="char" quantity="12"/>Criant – qu’il soit honni</l>
					<l>Par le monde à jamais ! -– que sa cause était juste.</l>
					<l>Même quand à Paris il prétendait aller</l>
					<l>Et pensait que nos cœurs étaient prêts à crouler,</l>
					<l><space unit="char" quantity="12"/>Il ne put ébranler</l>
					<l>Notre stable espérance en toi, Victoire Auguste !</l>
				</lg>
				<lg n="4">
					<l>Même lorsque le Russe eût bassement trahi,</l>
					<l>Et que l’alleu latin, par le Hun envahi,</l>
					<l><space unit="char" quantity="12"/>Fut si fort assailli</l>
					<l>Qu’il n’avait jamais vu de pareille tourmente :</l>
					<l>Même alors, en dépit des heurts de la mousson,</l>
					<l>Nous épiions aux quatre coins de l’horizon</l>
					<l><space unit="char" quantity="12"/>Ta belle floraison</l>
					<l>Et regardions le flot monter sans épouvante.</l>
				</lg>
				<lg n="5">
					<l>D’aucuns, supportant mal les méfaits du pillard,</l>
					<l>Incriminaient, le cœur percé de part en part,</l>
					<l><space unit="char" quantity="12"/>Déesse, ton retard,</l>
					<l>Et se désespéraient de ton trop long silence.</l>
					<l>Toi, cependant, sur nous dans l’ombre tu veillais,</l>
					<l>Et nos constants exploits, dont tu t’émerveillais,</l>
					<l><space unit="char" quantity="12"/>Étroitement liaient</l>
					<l>Ton glorieux Destin à celui de la France.</l>
				</lg>
				<lg n="6">
					<l>Car nous, tes serviteurs fidèles, savions bien</l>
					<l>Que le pays pouvait compter sur ton soutien,</l>
					<l><space unit="char" quantity="12"/>S’étant fait le gardien</l>
					<l>Des franchises qu’un noir démon tenait en geôle.</l>
					<l>Et, pendant que doutaient les gens de peu de foi,</l>
					<l>Nous, tes leudes féaux, ne doutant pas de toi,</l>
					<l><space unit="char" quantity="12"/>Suivions avec émoi</l>
					<l>Ton sillage lointain au ciel de notre Gaule.</l>
				</lg>
				<lg n="7">
					<l>Tu venais. Tu venais ! On te voyait grandir.</l>
					<l>Nos yeux extasiés te regardaient bondir.</l>
					<l><space unit="char" quantity="12"/>On entendait vrombir</l>
					<l>L’espace sous l’essor de ta course empennée.</l>
					<l>Nos grands preux, nos martyrs tombés, tombant encor ;</l>
					<l>Nos prisonniers, frappés par un geôlier butor,</l>
					<l><space unit="char" quantity="12"/>Allaient donc voir leur mort</l>
					<l>Punie enfin par ta justice, ô Destinée !</l>
				</lg>
				<lg n="8">
					<l>Et te voici ! Déesse auguste, assiste-nous.</l>
					<l>Les méchants n’ont pas pu nous courber sous leurs jougs.</l>
					<l><space unit="char" quantity="12"/>Notre peuple à genoux,</l>
					<l>O Victoire, t’acclame avec reconnaissance.</l>
					<l>Car les peuples de proie, à jamais abattus,</l>
					<l>Déments qui de la Force avaient fait leurs vertus,</l>
					<l><space unit="char" quantity="12"/>Aujourd’hui ne sont plus…</l>
					<l>C’est un cycle nouveau que l’univers commence.</l>
				</lg>
				<lg n="9">
					<l>C’est la ligue des grands et des petits États,</l>
					<l>— Une fois supprimés des hautains Potentats</l>
					<l><space unit="char" quantity="12"/>Les sombres attentats,</l>
					<l>Pour maintenir sur terre une paix garantie,</l>
					<l>Car la guerre actuelle est l’évolution</l>
					<l>Qui consacre à jamais, — utile sanction —,</l>
					<l><space unit="char" quantity="12"/>Dans chaque nation</l>
					<l>L’avènement final de la démocratie.</l>
				</lg>
				<lg n="10">
					<l>Qu’en ton honneur bien haut sonnent les olifants !</l>
					<l>O Victoire, gloire à ton œuvre ! Nos enfants,</l>
					<l><space unit="char" quantity="12"/>En des jours triomphants,</l>
					<l>Vont voir à l’avenir s’épanouir leur vie.</l>
					<l>S’indignant des malheurs que le Hun déchaîna,</l>
					<l>Némésis, irritée, à son tour dégaina.</l>
					<l><space unit="char" quantity="12"/>Hosannah ! Hosannah !</l>
					<l>Ta vengeance, bon Droit, est enfin assouvie !</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>
