<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRANCE À GARIBALDI</title>
				<title type="sub">ODE</title>
				<title type="medium">Édition électronique</title>
				<author key="LAB">
					<name>
						<forename>Gustave</forename>
						<surname>LABOURT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LAB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA FRANCE À GARIBALDI, ODE</title>
						<author>GUSTAVE LABOURT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>La Sentinelle Mentonnaise</publisher>
						<pubPlace>Menton</pubPlace>
						<idno type="URI">www.basesdocumentaires-cg06.fr/archives</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRANCE À GARIBALDI, ODE</title>
								<author>GUSTAVE LABOURT</author>
								<imprint>
									<pubPlace>Menton</pubPlace>
									<publisher>La Sentinelle Mentonnaise, Journal politique et Littéraire</publisher>
									<date when="1871">28 mars 1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem">
				<head type="main">LA FRANCE À GARIBALDI</head>
				<head type="form">ODE</head>
				<head type="sub_2">
					La Sentinelle Mentonnaise, Journal politique et Littéraire, 28 mars 1900<lb/>
					(republication à l’occasion de l’inauguration du monument à Garibaldi)
				</head>
				<head type="sub_1">POÉSIE DITE AU THÉÂTRE FRANÇAIS DE NICE, LE 28 MARS 1871</head>
				<lg>
					<l>Toi qui viens de saisir ta brave et fière épée,</l>
					<l>Pour tâcher de venger la France ensanglantée…</l>
					<l>Tu fis pour la sauver un effort surhumain !</l>
					<l>Et semblable à ce noble et sublime romain…</l>
					<l>Lorsque ton dévouement nous devient inutile,</l>
					<l>Méprisant les honneurs tu rentres dans ton île</l>
					<l>Laissant les orgueilleux rêver d’ambitions.</l>
					<l>Ton rêve, à toi, ce fut le bonheur des nations.</l>
					<l>Oui ! tu rêvais la paix, tu détestais la guerre.</l>
					<l>Va, nous le savons tous… Tu voulais sur la terre</l>
					<l>Voir cesser la discorde ! et voir l’humanité</l>
					<l>Ne former qu’un grand peuple… où la fraternité</l>
					<l>Pût régner seule ! Enfin tu voulais, bon génie,</l>
					<l>Sous ton talon de fer briser la tyrannie !!</l>
					<l>Garibaldi n’est pas un vulgaire soldat,</l>
					<l>Car ce n’est qu’aux tyrans qu’il a livré combat !</l>
					<l>Un seul mot doit le peindre, et je n’en veux pas d’autre !</l>
					<l>On dit c’est un héros ! Je dis, c’est un apôtre !</l>
					<l>Son cri de ralliement le voici : Dieu puissant</l>
					<l>Donne la liberté ! mais sans verser de sang !</l>
					<l>N’est-il pas incroyable à l’époque où nous sommes</l>
					<l>De penser que l’on peut faire tuer des hommes ?</l>
					<l>Où donc est la science ? où donc est le progrès ?</l>
					<l>Si nous nous égorgeons entre nous sans regrets !</l>
					<l>Laissons à d’autres temps un moyen si sauvage</l>
					<l>Et par l’intelligence écrasons l’esclavage !</l>
					<l>Merci Garibaldi ! Merci ! C’est notre France</l>
					<l>Qui te dit par ma voix, que sa reconnaissance</l>
					<l>Est éternelle ! e pas un Français n’oubliera</l>
					<l>Le nom du grand héros qui vit à Caprera !</l>
					<l>C’est un enfant de Nice, à Nice il a grandi !</l>
					<l>C’est un enfant de Nice, à Nice il a grandi !</l>
					<l>Elle a droit d’être fière ! Une aussi grande gloire !</l>
					<l>Est une belle page au livre de l’histoire !</l>
					<l>Ta vie avec ton sang, tu nous l’offrais, grand cœur,</l>
					<l>Pour sauver notre France et venger notre honneur !</l>
					<l>Oui ! nous devons l’aimer jusqu’à l’idolâtrie !</l>
					<l>Le pays qu’on écrase est toujours ta patrie !</l>
					<l>Tu sauves l’opprimé, tu combats les tyrans</l>
					<l>Et tes braves soldats s’appellent tes enfants !</l>
					<l>Puis quand on veut payer ton sublime courage</l>
					<l>Tu vas cancer ta gloire au fond d’un hermitage,</l>
					<l>Tu n’entends plus là-bas retentir le canon.</l>
					<l>Mais des échos de gloire ont répété ton nom !</l>
					<l>Il n’est plus désormais qu’un cri dans notre France.</l>
					<l>Cri parti de nos cœurs pleins de reconnaissance</l>
					<l>Et qui retentira du nord jusqu’au midi.</l>
					<l>Vive la liberté ! Vive Garibaldi.</l>
				</lg>
				<p>
					P.S. – Aujourd’hui hélas ! Ricciotti Garibaldi, au lieu de lutter pour la liberté,
					combat avec la tyrannie. Les temps sont bien changés !
				</p>
			</div>
		</body>
	</text>
</TEI>