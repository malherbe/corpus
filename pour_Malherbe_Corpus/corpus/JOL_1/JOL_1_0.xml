<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES SOLDATS D’AUTREFOIS</title>
				<title type="medium">Édition électronique</title>
				<title type="sub_1">Poème publié dans le journal LE GAULOIS (1870)</title>
				<author key="JOL">
					<name>
						<forename>Gaston</forename>
						<surname>JOLLIVET</surname>
					</name>
					<date from="1842" to="1927">1842-1927</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">JOL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES SOLDATS D’AUTREFOIS</title>
						<author>GASTON JOLLIVET</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k519993r/f3.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Le Gaulois : littéraire et politique</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Le Gaulois : littéraire et politique</publisher>
									<date when="1870">1870-11-19</date>
								</imprint>
								<biblScope unit="issue">867</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>

		<body>


			<div type="poem">
				<head type="main">LES SOLDATS D’AUTREFOIS</head>
				<lg>
					<l>Les portraits des vieux combattants,</l>
					<l>Qu’on aimait à les voir, naguère,</l>
					<l>Au musée, à Versaille, au temps</l>
					<l>Où ce n’était pas la frontière !</l>
				</lg>
				<lg>
					<l>Tous beaux, ces preux bardés de fer !</l>
					<l>Cheveux noirs ou crinière blonde,</l>
					<l>Des bras à défier l’enfer,</l>
					<l>Une épaule à porter le monde !</l>
				</lg>
				<lg>
					<l>C’étaient les héros d’autrefois,</l>
					<l>Les paladins et les grands reîtres.</l>
					<l>L’Arioste a dit leurs exploits ;</l>
					<l>Bayard les saluait ses maîtres.</l>
				</lg>
				<lg>
					<l>Ils descendaient du vieux manoir,</l>
					<l>Ces fiers guerriers à haute taille,</l>
					<l>Et c’était plaisir de les voir</l>
					<l>Monter le cheval de bataille.</l>
				</lg>
				<lg>
					<l>L’épée au soleil reluisait.</l>
					<l>L’épouse attachait la ceinture</l>
					<l>Et l’enfant se dressant baisait</l>
					<l>Et rebaisait la lourde armure.</l>
				</lg>
				<lg>
					<l>En selle, et sus à l’ennemi !</l>
					<l>Cavaliers, au vent la bannière !</l>
					<l>On se retrouvera parmi</l>
					<l>Les vainqueurs ou dans la poussière.</l>
				</lg>
				<lg>
					<l>Et c’étaient des combats joyeux,</l>
					<l>Passes d’armes sans fin ni trêve,</l>
					<l>Corps à corps, les yeux dans les yeux,</l>
					<l>Et le glaive contre le glaive.</l>
				</lg>
				<lg>
					<l>Sur le baudrier se drapait</l>
					<l>L’écharpe aux couleurs de la belle</l>
					<l>Et l’on savait où l’on frappait</l>
					<l>Sans longue-vue et sans jumelle.</l>
				</lg>
				<lg>
					<l>Soi-même on comptait les effets</l>
					<l>De son bras, lorsque venait l’ombre ;</l>
					<l>Car des morts que l’o avait faits</l>
					<l>Le sol sanglant disait le nombre !</l>
				</lg>
				<lg>
					<l>Salut, hélas ! et sans retour,</l>
					<l>Nobles luttes du moyen âge,</l>
					<l>Où l’on se battait en plein jour,</l>
					<l>En plein soleil, en plein courage !</l>
				</lg>
				<lg>
					<l>Où l’on n’avait pas, dans son coin,</l>
					<l>Courbé sur des calculs bizarres,</l>
					<l>Un vieux savant gagnant de loin</l>
					<l>La bataille entre deux catarrhes ;</l>
				</lg>
				<lg>
					<l>Où nos bons aïeux du vieux temps,</l>
					<l>Bravant les ruses taciturnes,</l>
					<l>Laissaient la plaine aux combattants</l>
					<l>Et les bois aux voleurs nocturnes !</l>
				</lg>
			</div>



		</body>
	</text>
</TEI>