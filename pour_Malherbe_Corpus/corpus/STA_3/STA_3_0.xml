<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSON SUR LA NAISSANCE DE LOUIS XIV</title>
				<title type="sub">ŒUVRES DE SAINT-AMANT, tome IV</title>
				<title type="sub_1">(édition Jean Lagny - 1971)</title>
				<title type="medium">Une édition électronique</title>
				<author key="STA">
					<name>
						<forename>Marc-Antoine Girard</forename>
						<nameLink>de</nameLink>
						<surname>SAINT-AMANT</surname>
					</name>
					<date from="1594" to="1661">1594-1661</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2024">2024</date>
				<idno type="local">STA_3</idno>
				 <availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES</title>
						<author>Marc-Antoine Girard de Saint-Amant</author>
						<editor>Édition critique publiée par Jacques Bailbé</editor>
						<idno type="URL">http://catalogue.bnf.fr/ark:/12148/cb35111424s</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Marcel Didier</publisher>
							<date when="1971">1971</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1638">1638</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes ne sont pas incluses dans cette édition électroniques.</p>
				<p>Variante plus satisfaisante que celle de l’édition ’</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’apostrophe ordinaire (’) a été remplacée l’apostrophe typographique (’)</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="STA163">
				<head type="main">CHANSON SUR LA NAISSANCE DE LOUIS XIV</head>
				<lg n="1">
					<l><space unit="char" quantity="8"/>Nous avons un Dauphin</l>
					<l><space unit="char" quantity="8"/>Le bonheur de la France ;</l>
					<l><space unit="char" quantity="8"/>Rions, buvons sans fin</l>
					<l><space unit="char" quantity="8"/>À l’heureuse naissance.</l>
					<l>Car Dieu nous l’a donné par l’entremise</l>
					<l><space unit="char" quantity="8"/>Des Prelats de l’Eglise,</l>
					<l><space unit="char" quantity="4"/>Nous lui verrons la barbe grise.</l>
				</lg>
				<lg n="2">
					<l><space unit="char" quantity="8"/>Lorsque ce Dieu donné</l>
					<l><space unit="char" quantity="8"/>Aura pris sa croissance,</l>
					<l><space unit="char" quantity="8"/>Il sera couronné</l>
					<l><space unit="char" quantity="8"/>Le plus grand Roi de France :</l>
					<l>L’Espagne, l’Empereur et l’Italie</l>
					<l><space unit="char" quantity="8"/>Et le Roi de Hongrie</l>
					<l><space unit="char" quantity="4"/>En mourront de peur et d’envie.</l>
				</lg>
				<lg n="3">
					<l><space unit="char" quantity="8"/>La ville de Paris</l>
					<l><space unit="char" quantity="8"/>Se montra sans pareille,</l>
					<l><space unit="char" quantity="8"/>En festins, jeux, et ris</l>
					<l><space unit="char" quantity="8"/>Le monde fit merveille.</l>
					<l>Chacun de s’ennyvrer faisoit sa gloire</l>
					<l><space unit="char" quantity="8"/>Buvant à sa memoire,</l>
					<l><space unit="char" quantity="4"/>Aussi bien Jean comme Gregoire.</l>
				</lg>
				<lg n="4">
					<l><space unit="char" quantity="8"/>Au millieu du Ruisseau</l>
					<l><space unit="char" quantity="8"/>Etoit la nape mise,</l>
					<l><space unit="char" quantity="8"/>Et qui buvoit de l’eau</l>
					<l><space unit="char" quantity="8"/>Etoit mis en chemise.</l>
					<l>Ce n’étoit rien que feux, jeux et lanternes</l>
					<l><space unit="char" quantity="8"/>Toujours dans les tavernes ;</l>
					<l><space unit="char" quantity="4"/>Si je ne dis vrai, qu’on me berne.</l>
				</lg>
				<lg n="5">
					<l><space unit="char" quantity="8"/>Ce qui fut bien plaisant</l>
					<l><space unit="char" quantity="8"/>Fut monsieur de Ralière ;</l>
					<l><space unit="char" quantity="8"/>Ce brave Partisan</l>
					<l><space unit="char" quantity="8"/>Fit faire une Barriere</l>
					<l>De douze à quinze muids où tout le monde</l>
					<l><space unit="char" quantity="8"/>Assis tout à la ronde</l>
					<l><space unit="char" quantity="4"/>S’amusoit à tirer la bonde.</l>
				</lg>
				<lg n="6">
					<l><space unit="char" quantity="8"/>Monsieur de Benjamin,</l>
					<l><space unit="char" quantity="8"/>Des ecuiers la source,</l>
					<l><space unit="char" quantity="8"/>Fit planter un Dauphin</l>
					<l><space unit="char" quantity="8"/>Au millieu de sa course,</l>
					<l>Où six francs chevaliers, avec la lance,</l>
					<l><space unit="char" quantity="8"/>Faisoient la reverence,</l>
					<l><space unit="char" quantity="4"/>Puis alloient brider la potence.</l>
				</lg>
				<lg n="7">
					<l><space unit="char" quantity="8"/>Le feu fut merveilleux</l>
					<l><space unit="char" quantity="8"/>Dans la place de Greve,</l>
					<l><space unit="char" quantity="8"/>Et quasi jusqu’aux cieux</l>
					<l><space unit="char" quantity="8"/>La machine s’esleve.</l>
					<l>Minerve y paroissoit de belle taille,</l>
					<l><space unit="char" quantity="8"/>Toute en cotte de maille,</l>
					<l><space unit="char" quantity="4"/>Mettant tout son monde en bataille.</l>
				</lg>
				<lg n="8">
					<l><space unit="char" quantity="8"/>Au millieu du Pont-neuf,</l>
					<l><space unit="char" quantity="8"/>Près du Cheval de Bronze,</l>
					<l><space unit="char" quantity="8"/>Depuis huit jusqu’à neuf,</l>
					<l><space unit="char" quantity="8"/>Depuis dix jusqu’à onze,</l>
					<l>On fit un si grand feu qu’on eut grand’peine</l>
					<l><space unit="char" quantity="8"/>Pour la Samaritaine,</l>
					<l><space unit="char" quantity="4"/>Et pour ne pas bruler la Seine.</l>
				</lg>
				<lg n="9">
					<l><space unit="char" quantity="8"/>Enfin tout notre espoir</l>
					<l><space unit="char" quantity="8"/>Etoit que notre Reine</l>
					<l><space unit="char" quantity="8"/>Quelque jour nous fit voir</l>
					<l><space unit="char" quantity="8"/>Sa Couche souveraine,</l>
					<l>Nous donnant un Dauphin pour bon presage.</l>
					<l><space unit="char" quantity="8"/>Il est beau, bon, et sage,</l>
					<l><space unit="char" quantity="4"/>Et fera merveille en son age.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>
