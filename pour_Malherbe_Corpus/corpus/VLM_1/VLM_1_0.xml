<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE BAISER DE L’ALSACIENNE</title>
				<title type="medium">Édition électronique</title>
				<author key="VLM">
					<name>
						<forename>Germain</forename>
						<surname>GIRARD</surname>
						<addName type="pen_name">VILLEMER</addName>
					</name>
					<date from="1842" to="1892">1842-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VLM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE BAISER DE L’ALSACIENNE</title>
						<author>VILLEMER</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Eveillard et Jacquot, Editeurs</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>

			<div type="poem">
				<head type="main">LE BAISER DE L’ALSACIENNE</head>
				<head type="form">Dit par M. Delaunay de la Comédie Française</head>
				<lg>
					<l>Le soleil de Juillet illuminait l’Alsace.</l>
					<l>Les appels du clairon éclataient dans l’espace…</l>
					<l>Tout le jour, défilaient, en chantant, pleins d’entrain,</l>
					<l>Des régiments français s’en allant vers le Rhin…</l>
					<l>Sur la place publique, auprès de la fontaine,</l>
					<l>De son regard rêveur une jeune Alsacienne</l>
					<l>Depuis un long moment suivait avec amour</l>
					<l>Les zouaves préparant leur campement d’un jour :</l>
					<l>Quand l’un d’eux, un sergent à la mine éveillée,</l>
					<l>Se dirigea vers la fontaine ensoleillée :</l>
					<l>— Dites, la belle enfant, fit-il, portant la main</l>
					<l>A son turban tout gris des poudres du chemin,</l>
					<l>D’un peu d’eau, s’il vous plait, me ferez-vous la grâce ?</l>
				</lg>
				<lg>
					<l>A cette question la blonde enfant d’Alsace</l>
					<l part="I">Sourit et lui tendit son vase :</l>
				</lg>
				<lg>
					<l part="F">« Assurément,</l>
					<l>Dit-elle, j’en aurais pour tout le régiment.</l>
					<l>Buvez sans crainte l’eau de la source française,</l>
					<l>Buvez, petit sergent, buvez tout à votre aise ! »</l>
				</lg>
				<lg>
					<l>Mais quoiqu’elle insistât le sergent ne but pas,</l>
					<l>Et les yeux dans ses yeux, il ajouta plus bas :</l>
					<l>« Non, vous d’abord, buvez… Ma soif n’est pas pressée…</l>
					<l>Buvez ! Je voudrais tant savoir votre pensée ! »</l>
				</lg>
				<lg>
					<l>Riant de son caprice, elle but lentement</l>
					<l>Et de nouveau lui dit : — Buvez donc maintenant…</l>
				</lg>
				<lg>
					<l>Il étancha sa soif, il but avec ivresse</l>
					<l>Et puis la regardant soudain avec tendresse :</l>
					<l>« Je ne sais rien, dit-il, de ce que vous pensez ;</l>
					<l>Mais si vous permettez, voilà ce que je sais.</l>
					<l>Je voudrais bien, avant d’affronter la bataille,</l>
					<l>Pouvoir dans mes dix doigts enlacer votre taille,</l>
					<l>Et vous prendre un baiser, un seul, là, mais bien doux !</l>
					<l>Dites , la belle enfant, dites, permettez-vous ! »</l>
				</lg>
				<lg>
					<l>Elle se défendit : C’est un peu trop d’audace,</l>
					<l>Dit-elle en souriant, pour un sergent qui passe !</l>
					<l>Nos garçons ne vont pas si vite par ici…</l>
					<l>Bah ! c’est qu’on est pressée, dit-il, riant aussi…</l>
					<l part="I" ana="unanalyzable">Et puis… , voilà je suis parisien !</l>
				</lg>
				<lg>
					<l part="F" ana="unanalyzable">L’Alsacienne</l>
					<l ana="unanalyzable">......................................................................................</l>
				</lg>
				<lg>
					<l>Mais vous êtes français, dit-elle, c’est assez !</l>
					<l>Je sens que tout en moi frémit quand vous passez…</l>
					<l>Faites votre devoir, marchez à la victoire…</l>
					<l>Et moi, je vous promets, et vous pouvez m’en croire,</l>
					<l>Si loin que le combat puisse entrainer vos pas,</l>
					<l>Qu’au retour mon baiser ne vous manquera pas. »</l>
				</lg>
				<lg>
					<l>Le sergent s’éloigna rêveur. Cette fillette</l>
					<l>A la parole grave avait troublé sa tête…</l>
					<l>Il n’avait qu’un désir, lui parler, la revoir…</l>
					<l>Sous sa fenêtre, il vint encor rêver le soir ;</l>
					<l>Quand le clairon sonna le départ à l’aurore,</l>
					<l>En repliant sa tente, il rêvait d’elle encore…</l>
					<l>Mais comme il s’éloignait, suivant le régiment,</l>
					<l>Soudain elle apparut, souriant doucement</l>
					<l>Et de vergiss-mein-nicht lui jetant une gerbe,</l>
					<l>Elle semblait lui dire en un geste superbe :</l>
					<l>« Va ! sergent ! souviens-toi ! moi je me souviendrai !</l>
					<l>Mérite mon baiser, je te l’apporterai ! »</l>
				</lg>
				<lg>
					<l>Wissembourg ! Wissembourg ! L’Allemagne insolente</l>
					<l>Pourra nous reprocher cette tache sanglante !</l>
					<l>Lorsqu’à vingt allemands tout le jour un français</l>
					<l>Tient tête, sans faiblir, c’est encor le succès.</l>
					<l>Furent-ils des vainqueurs ces soldats qui dans l’ombre,</l>
					<l>Foudroyaient à coup sûr, confiants dans leur nombre !</l>
					<l>Non ! le vainqueur du jour, ce fut ce beau martyr,</l>
					<l>Douai, qui ne pouvant plus vaincre, sut mourir…</l>
					<l>Ce furent ces héros, luttant cent contre mille,</l>
					<l>Qui sans espoir, sachant tout effort inutile,</l>
					<l>S’en allèrent, le rire aux dents, sous le drapeau,</l>
					<l>Dans les rangs allemands se creuser un tombeau !</l>
					<l>Wissembourg ! Wissembourg ! Laisse donc l’Allemagne</l>
					<l>Célébrer ce début de l’horrible campagne :</l>
					<l>Les tombes des héros fleuriront, quelque-jour,</l>
					<l>Et les vaincus auront la victoire à leur tour !</l>
				</lg>
				<lg>
					<l>C’est fini. La journée, hélas ! est consommée,</l>
					<l>Les cadavres sanglants de tout un corps d’armée</l>
					<l>Dans des ruisseaux de sang sommeillent pour jamais</l>
					<l>Et Wissembourg n’est plus qu’un cercueil désormais.</l>
					<l>La ferme où tout riait la veille est morne et sombre,</l>
					<l>Et ses vieux murs troués par des boulets sans nombre,</l>
					<l>Ouverts comme une porte immense jusqu’au toit,</l>
					<l>Semblent dire la mort : Entre comme chez toi !</l>
					<l>Le vieux moulin s’est tû. Son aile pend meurtrie,</l>
					<l>Et du ruisseau qui court à travers la prairie,</l>
					<l>Le sang a coloré l’eau si pure jadis.</l>
					<l>Sur sa rive, parmi les doux myosotis</l>
					<l>Sous le grand saule où le soleil couchant se joue,</l>
					<l>La poitrine sanglante et la mort à la joue</l>
					<l>Le sergent agonise. Il a fait son devoir</l>
					<l>Et payé son tribut à l’affreux désespoir.</l>
					<l>Au moment d’expirer, il ouvre sa tunique :</l>
					<l>Sa main y cherche encor une chère relique,</l>
					<l>Les fleurs de l’Alsacienne. Hélas ! il espérait</l>
					<l>Dans ce doux souvenir. Sans rien dire, en secret,</l>
					<l>Avant de s’élancer dans la mêlée horrible,</l>
					<l>Il pensait en son cœur : Non ! ce n’est pas possible !</l>
					<l>Je ne peux pas mourir ! Je garde cet espoir,</l>
					<l>Elle me l’a promis et je dois la revoir !</l>
				</lg>
				<lg>
					<l>Pourtant il va mourir ! Sur ses lèvres glacées,</l>
					<l>C’est vainement qu’il tient les chères fleurs pressées.</l>
					<l>L’Alsacienne, échappant à son rêve éperdu,</l>
					<l>Ne vient pas lui donner le baiser attendu…</l>
					<l>Tout à coup un appel a frappé son oreille…</l>
					<l>Aux portes de la mort le mourant se réveille</l>
					<l>Quelqu’un vient… Une voix soupire en le nommant :</l>
					<l>« O Dieu ! permettez-moi de tenir mon serment ! »</l>
					<l>C’est elle… Elle le cherche… Elle approche… Elle arrive,</l>
					<l>Un noir pressentiment lui fait suivre la rive</l>
					<l>Où les vergiss-mein-nicht ouvrent leurs doux yeux bleus.</l>
					<l>Soudain elle le voit, il est là, sous ses yeux,</l>
					<l>Sanglant, expirant, mort… Un suprême sourire</l>
					<l>Sur sa lèvre glacée erre et semble lui dire :</l>
					<l>« J’attendais ton baiser… Oh ! pourquoi m’oublier ! »</l>
				</lg>
				<lg>
					<l>L’Alsacienne en pleurant vient de s’agenouiller,</l>
					<l>Elle lave le sang qui coule des blessures,</l>
					<l>Mais en vain sa douleur se répand en murmures,</l>
					<l>En vain sa douce voix s’épuise à répéter :</l>
					<l>« C’est moi ! c’est mon baiser que je viens t’apporter ! »</l>
					<l>Le petit sergent dort dans la paix éternelle.</l>
				</lg>
				<lg>
					<l>Alors, d’un long baiser l’alsacienne fidèle</l>
					<l>Ferme ses yeux éteints, et puis, se redressant,</l>
					<l>Frémissante, la lèvre encor rouge de sang :</l>
					<l>« Prussiens ! souvenez-vous comme moi, cria-t-elle,</l>
					<l>Vous venez de sceller l’union immortelle.</l>
					<l>De cet hymen sanglant la haine doit germer,</l>
					<l>Et je vous hais autant que j’aurais dû l’aimer ! »</l>
				</lg>
				<lg>
					<l>Sur la place publique, auprès de la fontaine</l>
					<l>On voit depuis ce jour revenir l’Alsacienne,</l>
					<l>Ses cheveux blonds noués d’un large ruban noir…</l>
					<l>Sur son front est écrit : Je suis le désespoir !</l>
					<l>Là-bas, les paysans qui savent son épreuve,</l>
					<l>La saluant bien bas, murmurent : C’est la veuve !</l>
					<l>Les prussiens ont tué son sergent, et depuis,</l>
					<l>Vouée à sa vengeance, elle attend près du puits</l>
					<l>Des régiments français la prochaine arrivée…</l>
					<l>Ah ! sonne, heure bénie ! heure ardemment rêvée !</l>
					<l>Soleil de la revanche, éclate au fond des cieux !</l>
					<l>Debout, français ! frappons sans pitié, furieux,</l>
					<l>Et qu’aux veuves en deuil rendant enfin justice,</l>
					<l>Chaque puits Alsacien de sang prussien s’emplisse !</l>
				</lg>
			</div>

		</body>
	</text>
</TEI>
