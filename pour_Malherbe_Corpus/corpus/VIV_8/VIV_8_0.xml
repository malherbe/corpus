<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Dans un Coin de Violettes</title>
				<title type="medium">Édition électronique</title>
				<author key="VIV">
					<name>
						<forename>Renée</forename>
						<surname>VIVIEN</surname>
					</name>
					<date from="1877" to="1909">1877-1909</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">VIV_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Dans un Coin de Violettes</title>
						<author>Renée Vivien</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Dans_un_coin_de_violettes</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Dans un Coin de Violettes</title>
								<author>Renée Vivien</author>
								<idno type="URI">https://fr.wikisource.org/wiki/Fichier:Vivien_-_Dans_un_Coin_de_Violettes,_1910.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>E. SANSOT et Cie</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avertissement et la préface ne sont pas inclus dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Les corrections métriques ont été reportées sur le texte de wikisource.</p>
				</correction>
				<normalization>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les mots en petites capitales en début d’incipit ont été correctement formatés.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-04-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="VIV272">
				<head type="main">SOUS LA PROTECTION DES VIOLETTES</head>
				<lg n="1">
					<l>Je place sous la protection des violettes</l>
					<l>Mes adorations très humblement muettes…</l>
					<l><space unit="char" quantity="12"/>Ô vous les violettes !</l>
				</lg>
				<lg n="2">
					<l>Vous qui savez, par la puissance du parfum,</l>
					<l>Évoquer telle voix, et tel long regard brun…</l>
					<l><space unit="char" quantity="12"/>Puissance du parfum !</l>
				</lg>
				<lg n="3">
					<l>Exaucez le grand cri de celle qui vous aime</l>
					<l>Et sachez parfumer ma vie et mon poème</l>
					<l><space unit="char" quantity="12"/>Sachant que je vous aime.</l>
				</lg>
				<lg n="4">
					<l>Je suis lasse de lys, je suis lasse des roses,</l>
					<l>De leur haute splendeur, de leurs fraîcheurs écloses,</l>
					<l>De toute la beauté des grands lys et des roses.</l>
				</lg>
				<lg n="5">
					<l>Votre odeur s’exaspère en l’ombre et dans le soir,</l>
					<l>Violettes, ô fleurs douces au désespoir,</l>
					<l><space unit="char" quantity="12"/>Violettes du soir !</l>
				</lg>
			</div>
			<div type="poem" key="VIV273">
				<head type="main">AMOUR</head>
				<lg n="1">
					<l>Mirage de la mer sous la lune, ô l’Amour !</l>
					<l>Toi qui déçois, toi qui parais pour disparaître</l>
					<l>Et pour mentir et pour mourir et pour renaître,</l>
					<l>Toi qui crains le regard juste et sage du jour !</l>
				</lg>
				<lg n="2">
					<l>Toi qu’on nourrit de songe et de mélancolie,</l>
					<l>Inexplicable autant que le souffle du vent</l>
					<l>Et toujours inégal, injuste trop souvent,</l>
					<l>Je te crains à l’égal de ta sœur la folie !</l>
				</lg>
				<lg n="3">
					<l>Je te crains, je te hais et pourtant tu m’attires</l>
					<l>Puisque aussi le fatal est proche du divin.</l>
					<l>Voici qu’il m’est donné de te connaître enfin,</l>
					<l>Et je mourrais pour l’un de tes moindres sourires !</l>
				</lg>
			</div>
			<div type="poem" key="VIV274">
				<head type="main">INSPIRATION</head>
				<lg n="1">
					<l>L’esprit souffle… Et le vent emporte les paroles</l>
					<l>Qui vacillent ainsi que les musiques folles.</l>
				</lg>
				<lg n="2">
					<l>Inexplicable autant que l’amour et la foi,</l>
					<l>Ô l’Inspiration ! reviens bientôt vers moi !</l>
				</lg>
				<lg n="3">
					<l>Reviens comme le vent qui chante et se lamente,</l>
					<l>Reviens comme une haleine implacable ou démente !</l>
				</lg>
				<lg n="4">
					<l>Reviens comme le vent qui m’inspira l’amour,</l>
					<l>Et je t’accueillerai, dans l’instant du retour,</l>
				</lg>
				<lg n="5">
					<l>Avec l’emportement et l’angoisse démente</l>
					<l>Qu’inspire le retour d’une infidèle amante !</l>
				</lg>
			</div>
			<div type="poem" key="VIV275">
				<head type="main">LES SEPT LYS DE MARIE</head>
				<lg n="1">
					<l>Le Sept Lys ont fleuri devant l’antique porche.</l>
					<l>Chacun d’eux est plus long et plus droit qu’une torche,</l>
					<l>Leurs pistils sont pareils à des flammes de torche.</l>
				</lg>
				<lg n="2">
					<l>Les Sept Lys ont fleuri miraculeusement</l>
					<l>Dans le silence auguste et dans l’ombre, au moment</l>
					<l>Où s’élève le Christ, miraculeusement…</l>
				</lg>
				<lg n="3">
					<l>Sous l’imposition des mains saintes du prêtre</l>
					<l>Dans l’ombre et dans l’encens on les vit apparaître…</l>
					<l>Le peuple vit alors sourire le vieux prêtre…</l>
				</lg>
				<lg n="4">
					<l>Et tous les contemplaient avec des yeux d’amour.</l>
					<l>Le prêtre dit, portant ses regards à l’entour :</l>
					<l>« Mes frères, contemplons les fleurs du Saint-Amour ! »</l>
				</lg>
				<lg n="5">
					<l>Leur parfum s’exhalait vers la Divine Image.</l>
					<l>Tous ont compris le sens du glorieux Message</l>
					<l>Sur l’autel où Marie écoute le Message</l>
				</lg>
				<lg n="6">
					<l>Et les Lys répandaient une paix autour d’eux</l>
					<l>Et l’Hostie avait moins de rayonnement qu’eux,</l>
					<l>La transparente Hostie était moins blanche qu’eux…</l>
				</lg>
				<lg n="7">
					<l>Apparaissez encore, ô Sept Lys de Marie,</l>
					<l>Au moment où la foule à genoux pleure et prie !</l>
					<l>Apparaissez encore en l’honneur de Marie !</l>
				</lg>
			</div>
			<div type="poem" key="VIV276">
				<head type="main">MON PARADIS</head>
				<lg n="1">
					<l>Mon Paradis est un doux pré de violettes</l>
					<l>Où le chant régnera sur des âmes muettes.</l>
					<l>Mon Ciel est un beau chant parmi les violettes.</l>
				</lg>
				<lg n="2">
					<l>Mon Ciel est la très calme éternité du soir</l>
					<l>Où le regard se fait plus profond pour mieux voir</l>
					<l>Et c’est l’Éternité dans le ciel d’un beau soir…</l>
				</lg>
				<lg n="3">
					<l>Mon Paradis est une éternelle musique.</l>
					<l>Qui s’exhale divine allégresse rythmique…</l>
					<l>Mon Paradis est le règne de la musique.</l>
				</lg>
				<lg n="4">
					<l>Car ce sera, là-haut, le triomphe du chant,</l>
					<l>Le règne de la paix dans le Ciel du couchant,</l>
					<l>Où rien ne survit plus que l’amour et le chant.</l>
				</lg>
			</div>
			<div type="poem" key="VIV277">
				<head type="main">RESSOUVENIR</head>
				<lg n="1">
					<l><hi rend="smallcap">Ô</hi> passé des chants doux ! ô l’autrefois des fleurs !…</l>
					<l>Je chante ici le chant des anciennes douleurs.</l>
				</lg>
				<lg n="2">
					<l>Je le chante, sans pleurs et sans haine à voix basse,</l>
					<l>Comme on se bercerait d’une musique lasse…</l>
				</lg>
				<lg n="3">
					<l>Profond, irrépressible, autant que le soupir,</l>
					<l>S’échappe de mon cœur le mauvais souvenir…</l>
				</lg>
				<lg n="4">
					<l>Je vois s’abandonner mon âme lente et lasse</l>
					<l>Au charme des bruits doux, de la lumière basse.</l>
				</lg>
				<lg n="5">
					<l>Que vont envelopper les anciennes douleurs ?…</l>
					<l>Ô l’autrefois des chants ! ô le passé des fleurs !</l>
				</lg>
			</div>
			<div type="poem" key="VIV278">
				<head type="main">INVOCATION À LA LUNE</head>
				<lg n="1">
					<l><hi rend="smallcap">Ô</hi> Lune chasseresse aux flèches très légères,</l>
					<l>Viens détruire d’un trait mes amours mensongères !</l>
					<l>Viens détruire les faux baisers, les faux espoirs,</l>
					<l>Toi dont les traits ont su percer les troupeaux noirs !</l>
				</lg>
				<lg n="2">
					<l>Toi qui fus autrefois l’Amie et la Maîtresse,</l>
					<l>Incline-toi vers moi, dans ma grande détresse !…</l>
					<l>Dis-moi que nul regard n’est divinement beau</l>
					<l>Pour qui sait contempler le grand regard de l’eau !…</l>
				</lg>
				<lg n="3">
					<l>Ô Lune, toi qui sais disperser les mensonges,</l>
					<l>Éloigne le troupeau serré des mauvais songes !</l>
					<l>Et, daignant aiguiser l’arc d’argent bleu qui luit,</l>
					<l>Accorde-moi l’espoir d’un rayon dans la nuit !</l>
				</lg>
				<lg n="4">
					<l>Ô Lune, toi qui sais rendre l’âme à soi-même</l>
					<l>Dans sa vérité froide, indifférente et blême !</l>
					<l>Ô toi, victorieuse adversaire du jour,</l>
					<l>Accorde-moi le don d’échapper à l’amour !</l>
				</lg>
			</div>
			<div type="poem" key="VIV279">
				<head type="main">LA PROMESSE DES FÉES</head>
				<lg n="1">
					<l>Le vent du soir portait des chansons par bouffées,</l>
					<l>Et, par lui, je reçus la promesse des Fées…</l>
				</lg>
				<lg n="2">
					<l>Avec des mots très doux, les elfes m’ont promis</l>
					<l>D’être immanquablement mes fidèles amis.</l>
				</lg>
				<lg n="3">
					<l>Mais n’attachez jamais votre âme à leurs paroles,</l>
					<l>Un Elfe est tôt enfui, souffle vif d’ailes folles !…</l>
				</lg>
				<lg n="4">
					<l>Leur vol tourbillonnait, vague comme un parfum.</l>
					<l>Cependant tous semblaient obéir à quelqu’un.</l>
				</lg>
				<lg n="5">
					<l>La première portait sur son front découvert</l>
					<l>Une couronne d’or… Son manteau semblait vert.</l>
				</lg>
				<lg n="6">
					<l>Et la couronne d’or, brûlant comme la flamme,</l>
					<l>Rayonnait au-dessus d’un visage de femme.</l>
				</lg>
				<lg n="7">
					<l>Malgré l’étonnement d’un cœur audacieux,</l>
					<l>Je ne pus endurer la splendeur de ses yeux…</l>
				</lg>
				<lg n="8">
					<l>Car j’entendais un bruit d’étreintes étouffées…</l>
					<l>Aussi j’ai voulu fuir l’amour fatal des Fées…</l>
				</lg>
				<lg n="9">
					<l>Mais, devant ce bonheur mêlé d’un si grand mal,</l>
					<l>Ne regrettais-je pas un peu l’amour fatal !</l>
				</lg>
			</div>
			<div type="poem" key="VIV280">
				<head type="main">PRÉSENCE</head>
				<lg n="1">
					<l>Ta présence me donne une heure de jeunesse,</l>
					<l>Il semble que mon mal se ralentit, puis cesse,</l>
					<l>Car c’est toi mon bonheur et c’est toi ma jeunesse !</l>
				</lg>
				<lg n="2">
					<l>Ô parfum de ta robe ! Ô fraîcheur de ton front !</l>
					<l>Jamais les cruels temps futurs n’obscurciront</l>
					<l>Cette douce clarté de tes yeux, de ton front !</l>
				</lg>
				<lg n="3">
					<l>Tu m’apportes ta voix, ta présence et ton rire,</l>
					<l>Et je t’attends, je te contemple, et je t’admire.</l>
					<l>En moi rayonne encor la splendeur de ton rire !</l>
				</lg>
				<lg n="4">
					<l>Sous le rayonnement solaire de tes yeux,</l>
					<l>Ô jeune et belle autant que le furent les dieux !</l>
					<l>Il me semble oublier mon cœur qui se fait vieux !</l>
				</lg>
			</div>
			<div type="poem" key="VIV281">
				<head type="main">RÉSURRECTION</head>
				<lg n="1">
					<l>Et je t’aime ! Et voici que s’épand dans mes moëlles</l>
					<l>Miraculeusement la clarté des étoiles,</l>
					<l>Belle que je choisis pour Reine des étoiles !</l>
				</lg>
				<lg n="2">
					<l>Me voici revenue à la vie, à l’amour</l>
					<l>Qui transfigure en or les choses d’alentour,</l>
					<l>Au charme du poème, au rire de l’amour.</l>
				</lg>
				<lg n="3">
					<l>Tantôt je m’enfonçais dans l’horreur des ténèbres</l>
					<l>Et je portais en moi des visions funèbres</l>
					<l>Ah ! l’horreur, ah ! l’horreur tenace des ténèbres !</l>
				</lg>
				<lg n="4">
					<l>Mais voici le matin… Nous voici toutes deux</l>
					<l>Vivantes… C’en est fait de mes songes hideux.</l>
					<l>Comme par le passé, Chère, nous sommes deux.</l>
				</lg>
				<lg n="5">
					<l>Ô bonheur de me voir revenue à la vie !</l>
					<l>Car l’aurore s’est faite en mon âme ravie ;</l>
					<l>Miraculeusement, je vois rire la vie !…</l>
				</lg>
				<lg n="6">
					<l>Voici que l’univers me donne moins d’effroi,</l>
					<l>Très chère, puisque enfin me voici près de toi,</l>
					<l>Et je n’ai plus d’angoisse et je n’ai plus d’effroi !</l>
				</lg>
			</div>
			<div type="poem" key="VIV282">
				<head type="main">OISEAUX DANS LA NUIT…</head>
				<lg n="1">
					<l>Cette nuit, des oiseaux ont chanté dans mon cœur…</l>
					<l>C’était la bonne fin de l’ancienne rancœur…</l>
					<l>J’écoutais ces oiseaux qui chantaient dans mon cœur.</l>
				</lg>
				<lg n="2">
					<l>Dans ma grande douleur, la nuit me fut clémente</l>
					<l>Et tendre autant que peut se montrer une amante.</l>
					<l>Ce fut la rare nuit qui se montra clémente.</l>
				</lg>
				<lg n="3">
					<l>Dans ton ombre, j’ouïs le chant de ses oiseaux.</l>
					<l>Et je dormis enfin… Mes songes furent beaux</l>
					<l>Pour avoir entendu le chant de ces oiseaux…</l>
				</lg>
			</div>
			<div type="poem" key="VIV283">
				<head type="main">NOTRE HEURE</head>
				<lg n="1">
					<l>Écoute le doux bruit de cette heure que j’aime</l>
					<l>Et qui passe et qui fuit et meurt en un poème !</l>
				</lg>
				<lg n="2">
					<l>Écoute ce doux bruit tranquille et passager</l>
					<l>Des ailes de l’Instant qui s’envole, léger !</l>
				</lg>
				<lg n="3">
					<l>Je crois que ma douleur n’est que celle d’un autre…</l>
					<l>Et cette heure est à nous comme une chose nôtre…</l>
				</lg>
				<lg n="4">
					<l>Car cette heure ne peut être à d’autres qu’à nous,</l>
					<l>Avec son doux parfum et son glissement doux…</l>
				</lg>
				<lg n="5">
					<l>Elle est pareille à la chanson basse qui leurre</l>
					<l>Et qui vient de la mer… Ah ! retenir notre heure !</l>
				</lg>
				<lg n="6">
					<l>Ô triste enchantement de se dire : Jamais</l>
					<l>Je ne retrouverai cette heure que j’aimais !</l>
				</lg>
			</div>
			<div type="poem" key="VIV284">
				<head type="main">ÉTONNEMENT DEVANT LE JOUR</head>
				<lg n="1">
					<l>Mes yeux sont éblouis du jour que je revois !</l>
					<l>L’ayant cru défier pour la dernière fois.</l>
				</lg>
				<lg n="2">
					<l>Mes yeux sont étonnés de revoir cette aurore,</l>
					<l>Ainsi, moi qui souffris autant, je vis encore !</l>
				</lg>
				<lg n="3">
					<l>Je vis encor, je souffre et peux encor souffrir…</l>
					<l>Sans exhaler mon cœur dans un dernier soupir !</l>
				</lg>
				<lg n="4">
					<l>Mais comment puis-je ainsi voir la lumière en face,</l>
					<l>Moi dont le cœur est lourd et dont l’âme est si lasse ?</l>
				</lg>
				<lg n="5">
					<l>Ô mon destin mauvais… Je suis devant l’amour</l>
					<l>Un adversaire nu… Voici venir le jour !…</l>
				</lg>
				<lg n="6">
					<l>Moi dont l’être est plus las que le dernier automne</l>
					<l>Qui se meurt sur les lacs, je vis… Et je m’étonne !</l>
				</lg>
			</div>
			<div type="poem" key="VIV285">
				<head type="main">LA LUNE CONSOLATRICE</head>
				<lg n="1">
					<l>Et voici que mon cœur s’épanouit et rit…</l>
					<l>Moi qui longtemps souffris, me voici consolée</l>
					<l>Par ce noir violet d’une nuit étoilée,</l>
					<l>Moi qui ne savais point que la lune guérit !</l>
				</lg>
				<lg n="2">
					<l>Moi qui ne savais point que la lune console</l>
					<l>De tout le chagrin lourd, de toute la rancœur !</l>
					<l>Sa consolation illumine le cœur</l>
					<l>D’un rayon éloquent autant qu’une parole.</l>
				</lg>
				<lg n="3">
					<l>Et d’un rayon furtif comme un furtif bienfait</l>
					<l>Elle se glisse au fond torturé de mon âme,</l>
					<l>Elle se glisse avec une douceur de femme.</l>
					<l>Et c’est insinuant comme un obscur bienfait.</l>
				</lg>
				<lg n="4">
					<l>Comme un obscur bienfait s’insinue, elle glisse…</l>
					<l>Tout le ciel émergeant de l’ombre est radieux.</l>
					<l>Éternellement chère à mon cœur, à mes yeux,</l>
					<l>Sois louée à jamais, Lune consolatrice !</l>
				</lg>
			</div>
			<div type="poem" key="VIV286">
				<head type="main">ABSENCE</head>
				<lg n="1">
					<l><hi rend="smallcap">Ô</hi> Femme au cœur de qui mon triste cœur a cru,</l>
					<l>Je te convoite, ainsi qu’un trésor disparu.</l>
				</lg>
				<lg n="2">
					<l>Je te maudis, mais en t’aimant… Mon cœur bizarre</l>
					<l>Te recherche, Émeraude admirablement rare !</l>
				</lg>
				<lg n="3">
					<l>Que je suis exilée ! Et que pèse le temps,</l>
					<l>Malgré le beau soleil des midis éclatants !</l>
				</lg>
				<lg n="4">
					<l>Retombant chaque soir dans un amer silence,</l>
					<l>Je pleure sur le plus grand des maux : sur l’absence !…</l>
				</lg>
			</div>
			<div type="poem" key="VIV287">
				<head type="main">À L’ENNEMIE AIMÉE</head>
				<lg n="1">
					<l>Tes mains ont saccagé mes trésors les plus rares,</l>
					<l>Et mon cœur est captif entre tes mains barbares.</l>
				</lg>
				<lg n="2">
					<l>Tu secouas au vent du nord tes longs cheveux</l>
					<l>Et j’ai dit aussitôt : Je veux ce que tu veux.</l>
				</lg>
				<lg n="3">
					<l>Mais je te hais pourtant d’être ainsi ton domaine,</l>
					<l>Ta serve… Mais je sens que ma révolte est vaine.</l>
				</lg>
				<lg n="4">
					<l>Je te hais cependant d’avoir subi tes lois,</l>
					<l>D’avoir senti mon cœur près de ton cœur sournois…</l>
				</lg>
				<lg n="5">
					<l>Et parfois je regrette, en cette splendeur rare</l>
					<l>Qu’est pour moi ton amour, la liberté barbare…</l>
				</lg>
			</div>
			<div type="poem" key="VIV288">
				<head type="main">ESSENTIELLE</head>
				<lg n="1">
					<l>Ainsi, l’on se contemple avec des yeux sacrés</l>
					<l>Devant l’autel des mers et sur l’autel des prés…</l>
				</lg>
				<lg n="2">
					<l>Toi dont la chevelure en plis d’or illumine,</l>
					<l>Tu m’as fait partager ton essence divine…</l>
				</lg>
				<lg n="3">
					<l>Et tu m’as emportée au fond même du ciel,</l>
					<l>Ô toi que l’on adore, ô l’Être Essentiel !</l>
				</lg>
				<lg n="4">
					<l>Tes yeux ont le regard que n’ont point d’autres femmes…</l>
					<l>Et ce fut, pour nous, comme une rencontre d’âmes.</l>
				</lg>
				<lg n="5">
					<l>Mon cœur nouveau renaît de mon cœur d’autrefois…</l>
					<l>Que dire de tes yeux ? Que dire de ta voix ?</l>
				</lg>
				<lg n="6">
					<l>Ô ma splendeur parfaite, ô ma Toute Adorée !</l>
					<l>La mer était en nous, unie à l’empyrée !</l>
				</lg>
			</div>
			<div type="poem" key="VIV289">
				<head type="main">TERREUR DU MENSONGE</head>
				<lg n="1">
					<l>Oui, j’endure aujourd’hui le pire des tourments,</l>
					<l>Tu m’as menti… Tu m’as trompé… Et tu me mens !…</l>
				</lg>
				<lg n="2">
					<l>Mensonge caressant qui glisse de ta bouche !</l>
					<l>Ô serment que l’on croit, ô parole qui touche !</l>
				</lg>
				<lg n="3">
					<l>Ô multiples douleurs qui s’abattent sur vous</l>
					<l>Ainsi qu’un petit vent pluvieusement doux !…</l>
				</lg>
				<lg n="4">
					<l>Comme un lilas ne peut devenir asphodèle,</l>
					<l>Jamais tu ne seras ni franche ni fidèle.</l>
				</lg>
				<lg n="5">
					<l>Tu seras celle-là qui se dérobe et fuit</l>
					<l>Plus sinueusement qu’un démon dans la nuit.</l>
				</lg>
				<lg n="6">
					<l>Ô toi que j’aime encor ! L’horreur de ton mensonge</l>
					<l>Est dans mon cœur amer… Il me mord, il me ronge…</l>
				</lg>
				<lg n="7">
					<l>Je suis lasse d’avoir suivi les noirs chemins…</l>
					<l>Col frêle qu’on voudrait prendre entre ses deux mains !</l>
				</lg>
			</div>
			<div type="poem" key="VIV290">
				<head type="main">SANCTUAIRE D’ASIE</head>
				<lg n="1">
					<l>J’abriterai dans mon sanctuaire d’Asie</l>
					<l>Mon éternel besoin d’ombre et de poésie.</l>
				</lg>
				<lg n="2">
					<l>Là-bas, guettant les mille et trois Dieux aux pieds d’or,</l>
					<l>Des prêtres, jour et nuit, veillent sur leur trésor.</l>
				</lg>
				<lg n="3">
					<l>Oui, désespérément, je fixe mon exode</l>
					<l>Vers ce refuge énorme et sombre de pagode,</l>
				</lg>
				<lg n="4">
					<l>Où, dressant vers le ciel les lotus léthéens,</l>
					<l>Les étangs dorment leurs sommeils paludéens.</l>
				</lg>
			</div>
			<div type="poem" key="VIV291">
				<head type="main">L’AILE BRISÉE</head>
				<lg n="1">
					<l>Elle est venue avec ses cheveux et sa robe,</l>
					<l>Sa robe de beau pourpre et ses beaux cheveux d’or !</l>
				</lg>
				<lg n="2">
					<l>Et mon âme aussitôt a pris un prompt essor</l>
					<l>Dans l’ivresse du cher instant que l’on dérobe !…</l>
				</lg>
				<lg n="3">
					<l>Mon cœur lourd est léger comme une bulle d’or,</l>
					<l>Puisque je la revois près de moi revenue !</l>
				</lg>
				<lg n="4">
					<l>Et comme en un miracle, apparue, advenue,</l>
					<l>Une aile de chimère a repris son essor !</l>
				</lg>
			</div>
			<div type="poem" key="VIV292">
				<head type="main">MAINS SUR UN FRONT DE MALADE</head>
				<lg n="1">
					<l>C’est l’imposition fraîche et lente des mains</l>
					<l>Sur mon front que remplit l’horreur des lendemains,</l>
					<l>Ô bénédiction suave de Ses mains !</l>
				</lg>
				<lg n="2">
					<l>Les douces mains de femme ont des gestes de prêtre</l>
					<l>Et répandent en vous la paix et le bien-être,</l>
					<l>La consolation que vient donner le prêtre !</l>
				</lg>
				<lg n="3">
					<l>Elles n’apprennent point le geste qui guérit,</l>
					<l>Elles l’ont toujours su… Dans l’horreur de la nuit</l>
					<l>Cette imposition très calme nous guérit…</l>
				</lg>
				<lg n="4">
					<l>Apaise mon grand mal, de tes mains secourables,</l>
					<l>Tandis que l’heure glisse aux sabliers des sables,</l>
					<l>Car le bienfait me vient de tes mains secourables !</l>
				</lg>
				<lg n="5">
					<l>Donne-moi ta fraîcheur et donne-moi ta paix !</l>
					<l>Et calme le démon qui sur moi se repaît,</l>
					<l>En signant sur mon front le geste de la paix !</l>
				</lg>
			</div>
			<div type="poem" key="VIV293">
				<head type="main">POUR MON CŒUR</head>
				<lg n="1">
					<l>Mystérieux, amer et terrible, ô mon cœur,</l>
					<l>Éloigne enfin de toi la haine et la rancœur !</l>
				</lg>
				<lg n="2">
					<l>Sache combien est grand ce bienfait qu’on te donne</l>
					<l>De pouvoir pardonner, ô mon cœur ! et pardonne !</l>
				</lg>
				<lg n="3">
					<l>Ne garde plus l’amer souvenir des joies dues !</l>
					<l>Et qu’il soit comme un mot effacé sur les nues !</l>
				</lg>
				<lg n="4">
					<l>Sois léger et sois doux comme l’ombre d’une aile,</l>
					<l>Ô mauvais cœur, tenace et méchant et fidèle !</l>
				</lg>
				<lg n="5">
					<l>Ô mon cœur ! exhalant, dans un vaste soupir,</l>
					<l>Le pardon retenu, sache enfin t’attendrir !…</l>
				</lg>
			</div>
			<div type="poem" key="VIV294">
				<head type="main">POUR LE LYS</head>
				<lg n="1">
					<l><hi rend="smallcap">Ô</hi> Toi, Femme que j’aime ! Ô Lys irréprochable !</l>
					<l>Très chère qu’on ne peut approcher qu’à genoux,</l>
					<l>Lève sur moi tes yeux si doux et ton front doux !</l>
					<l>Et que le repas soit comme la Sainte Table.</l>
				</lg>
				<lg n="2">
					<l>Réveille, avec ta voix, mes rêves somnolents.</l>
					<l>Voyant mon front fiévreux, accablé par les rêves,</l>
					<l>Toute droite, dans la pourpre et l’or tu te lèves,</l>
					<l>Toujours silencieuse, avec tes gestes lents.</l>
				</lg>
				<lg n="3">
					<l>Ô l’Image divine ! Ô la Femme que j’aime !</l>
					<l>Qui fais que je m’éveille avec la face au jour</l>
					<l>Et qui, par le pouvoir immense de l’amour,</l>
					<l>As fait que le matin m’est apparu moins blême.</l>
				</lg>
				<lg n="4">
					<l>Ô puissance ! ô beauté de la Femme que j’aime !</l>
				</lg>
			</div>
			<div type="poem" key="VIV295">
				<head type="main">ÉMERVEILLEMENT</head>
				<lg n="1">
					<l>Avec l’étonnement de mes regards, je vis,</l>
					<l>Le chœur des beaux rayons de lune aux tons bleuis.</l>
				</lg>
				<lg n="2">
					<l>Et mes regards étaient stupéfaits et ravis…</l>
					<l>Avec mes yeux ouverts grandement je les vis.</l>
				</lg>
				<lg n="3">
					<l>C’est pourquoi maintes fois, au hasard d’une veille,</l>
					<l>Ouvert sur l’infini, mon regard s’émerveille.</l>
				</lg>
			</div>
			<div type="poem" key="VIV296">
				<head type="main">AMOUR MÉPRISABLE</head>
				<lg n="1">
					<l>L’Amour dont je subis l’abominable loi</l>
					<l>M’attire vers ce que je crains le plus, vers Toi !</l>
				</lg>
				<lg n="2">
					<l>Tu fus et tu seras l’Inconnue ennemie…</l>
					<l>Je t’adore en pleurant, ô si mauvaise amie !</l>
				</lg>
				<lg n="3">
					<l>Car voici la raison de mon tourment infâme :</l>
					<l>Je ne surprendrai pas le regard de ton âme.</l>
				</lg>
				<lg n="4">
					<l>C’est pourquoi je te hais, c’est pourquoi je te crains…</l>
					<l>J’appelle un autre amour, d’autres yeux, d’autres mains,</l>
				</lg>
				<lg n="5">
					<l>Et surtout, pour calmer la plainte qui s’élève</l>
					<l>Du fond de mon cœur las, un rêve, un divin rêve !</l>
				</lg>
			</div>
			<div type="poem" key="VIV297">
				<head type="main">AMOUR, TOI LE LARRON…</head>
				<lg n="1">
					<l>Amour, toi, le larron éternel, qui dérobes</l>
					<l>Les lourds trésors des cœurs et le secret des robes !</l>
				</lg>
				<lg n="2">
					<l>Tu te glisses et te dissimules la nuit,</l>
					<l>Et ton pas est le pas du traître qui s’enfuit…</l>
				</lg>
				<lg n="3">
					<l>Ton pas est plus léger que le doux pas du Songe !</l>
					<l>Et l’on n’entend jamais ce bruit sournois qui ronge.</l>
				</lg>
				<lg n="4">
					<l>N’as-tu point d’amitié ? N’as-tu point de raison ?</l>
					<l>Voici que s’insinue en mon cœur ton poison.</l>
				</lg>
				<lg n="5">
					<l>Épargne-moi ! Vois mon visage et mon front blême…</l>
					<l>Mon ennemi, l’Amour, je te hais et je t’aime.</l>
				</lg>
			</div>
			<div type="poem" key="VIV298">
				<head type="main">VEILLÉE HEUREUSE</head>
				<lg n="1">
					<l>J’épie avec amour, ton sommeil dans la nuit :</l>
					<l>Ton front a revêtu la majesté de l’ombre,</l>
					<l>Tout son enchantement et son prestige sombre…</l>
					<l>Et l’heure, comme une eau nocturne, coule et fuit !</l>
				</lg>
				<lg n="2">
					<l>Tu dors auprès de moi, comme un enfant… J’écoute</l>
					<l>Ton souffle doux et faible et presque musical</l>
					<l>S’élevant, s’abaissant, selon un rythme égal…</l>
					<l>Ton âme, loin de moi, suit une longue route…</l>
				</lg>
				<lg n="3">
					<l>Tes yeux lassés sont clos, ô visage parfait !</l>
					<l>Te contemplant ainsi, j’écoute, ô mon amante !</l>
					<l>Comme un chant très lointain, ton haleine dormante,</l>
					<l>Je l’entends, et mon cœur est doux et satisfait.</l>
				</lg>
			</div>
			<div type="poem" key="VIV299">
				<head type="main">PRIÈRE AUX VIOLETTES</head>
				<lg n="1">
					<l>Sous la protection humble des violettes</l>
					<l>Je remets les soupirs et les douleurs muettes</l>
					<l>Qui viennent m’assiéger ce soir… Ce trop beau soir !…</l>
				</lg>
				<lg n="2">
					<l>Dans cet effondrement du final désespoir</l>
					<l>Leur parfum est semblable aux prières des Saintes…</l>
					<l>Ô fleur entre les fleurs ! Ô violettes saintes !</l>
				</lg>
				<lg n="3">
					<l>Lorsqu’enfin, en un temps, s’arrêtera mon cœur</l>
					<l>Las de larmes, et tout enivré de rancœur,</l>
					<l>Qu’une pieuse main les pose sur mon cœur !</l>
				</lg>
				<lg n="4">
					<l>Vous me ferez alors oublier, Violettes !</l>
					<l>Le long mal qui sévit dans le cœur des poètes…</l>
					<l>Je dormirai dans la douceur des violettes !</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>