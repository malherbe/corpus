Po�sies.

Par Anatole France (1844-1924)











TABLE DES MATIERES
Le Mauvais Ouvrier.
La Sagesse Des Griffons.
La Danse des morts.
La Part De Madeleine.
Denys Tyran De Syracuse.
Les L�gions De Varus.
L'Autographe.
Sonnet.












Le Mauvais Ouvrier.

Ma�tre Laurent Coster, coeur plein de po�sie, 
Quitte les compagnons qui du matin au soir, 
Vignerons de l�esprit, font g�mir le pressoir; 
Et Coster va r�vant selon sa fantaisie. 
Car il aime d�amour le d�mon Aspasie. 
Sur son banc, � l��glise, il va parfois s�asseoir, 
Et voit flotter dans la vapeur de l�encensoir 
La dame de l�enfer que son �me a choisie;
 
Ou bien encor, tout seul au bord d�un puits mousseux, 
Joignant ses belles mains d�ouvrier paresseux, 
Il �coute sans fin la sir�ne qui chante. 
Et je ne sais non plus travailler ni prier; 
Je suis, comme Coster, un mauvais ouvrier, 
� cause des beaut�s d�une femme m�chante









Sonnet.

Elle a des yeux d�acier; ses cheveux noirs et lourds 
Ont le lustre azur� des plumes d�hirondelle; 
Blanche � force de nuit amass�e autour d�elle, 
Elle erre sur les monts et dans les carrefours. 
Et nocturne, elle emporte � travers les cieux sourds, 
Dans le champ s�pulcral o� fleurit l�asphod�le, 
La p�le jeune fille id�ale, et fid�le 
� quelque r�ve altier d�impossibles amours. 
Vierge, elle aime le sang des vierges; et, farouche, 
Elle entr�ouvre la fleur fun�bre de sa bouche 
Et d�un sourire froid �claire ses p�leurs, 
Lorsque, pr�te � subir une peine inconnue, 
La victime aux cheveux de miel charg�s de fleurs, 
Mourante et les yeux blancs, offre sa gorge nue. 











La Sagesse Des Griffons.

C��tait la nuit ardente et le retour du bal;
Vaincue et triomphante et chastement lascive,
Elle disait d�un ton de bien-�tre: J�ai mal!
Les roses s�effeuillaient sur sa t�te pensive
O� murmurait encor l��me des violons;
Son pied avait parfois un spasme m�lodique.
Le mouchoir de dentelle au bout de ses doigts longs
Glissait; et sur les bras du fauteuil h�raldique,

Ses bras minces et blancs s�allongeaient mollement,
Nus, et laissaient tomber le fragile corsage,
Si bien que, sur le sein, � chaque battement,
L�ombre qui rend songeur se creusait davantage
Dans la blancheur de sa chair de cam�lia.
Mais soulevant ses bras, lianes odorantes,
Lentement sur mon col, douce, elle les lia,
Et soupira: Toujours! de ses l�vres mourantes.
Sur sa t�te d�enfant pench�e au poids des fleurs
Le dossier droit et haut montait lourd de t�n�bres,
Et sur sa nuque folle aux neigeuses fra�cheurs
Les Griffons lampass�s prenaient des airs fun�bres,
Car ils rem�moraient, en de calmes ennuis,
La longue obsession de leurs regards de ch�ne:
Les bras �vanouis des anciennes nuits
Qui tous voulaient jeter une �ternelle cha�ne,
Insens�s! sur le cou docile de l�aim�,
Ne sachant pas qu�au fond des demeures affreuses,
Tout seuls, pli�s en croix sur le sein accalm�,
Ils s�en iraient o� vont les bras des amoureuses.
Car les Griffons debout au chevet f�odal,
Chim�riques t�moins de mes belles chim�res,
S��taient enfin lass�s d�entendre, apr�s le bal,
Les serments �ternels des bouches �ph�m�res.











La Danse Des Morts.

Dans les si�cles de foi, surtout dans les derniers,
La grand' danse macabre �tait fr�quemment peinte
Au v�lin des missels comme aux murs des charniers.

Je crois que cette image �difiante et sainte
Mettait un peu d'espoir au fond du d�sespoir,
Et que les pauvres gens la regardaient sans crainte.

Ce n'est pas que la mort leur f�t douce � pr�voir;
Dieu r�gnait dans le ciel et le roi sur la terre:
Pour eux mourir, c'�tait passer du gris au noir.

Mais le ma�tre imagier qui, d'une touche aust�re,
Peignait ce simulacre, � genoux et priant,
Moine, y savait souffler la paix du monast�re.

Sous les pas des danseurs on voit l'enfer b�ant:
Le branle d'un squelette et d'un vif sur un gouffre,
C'est bien affreux, mais moins pourtant que le n�ant.

On croit en regardant qu'on avale du soufre,
Et c'est piti� de voir s'ab�mer sans retour
Sous la chair qui se tord la pauvre �me qui souffre.

Oui, mais dans cette nuit �tal�e au grand jour
On sent l'�lan commun de la pens�e humaine,
On sent la foi profonde. -Et la foi, c'est l'amour!

C'est l�, c'est cet amour triste qui rass�r�ne.
Les mourants sont pensifs, mais ne se plaignent pas,
Et la troupe est tr�s-douce � la Mort qui la m�ne.

On se tient en bon ordre et l'on marche au compas;
Une musique un peu faible et presque c�line
Marque discr�tement et dolemment le pas:

Un squelette est debout pin�ant la mandoline,
Et, comme un amoureux, sous son large chapeau,
Cache son front de vieil ivoire qu'il incline.

Son compagnon applique un rustique pipeau
Contre ses belles dents blanches et toutes nues,
Ou des os de sa main frappe un disque de peau.

Un squelette de femme aux mines ing�nues
�veille de ses doigts les touches d'un clavier,
Comme sainte C�cile assise sur les nues.

Cet orchestre si doux ne saurait convier
Les vivants au Sabbat, et, pour mener la ronde,
Satan aurait vraiment bien tort de l'envier.

C'est que Dieu, voyez-vous, tient encor le vieux monde.
Voici venir d'abord le Pape et l'Empereur,
Et tout le peuple suit dans une paix profonde.

Car le baron a foi, comme le laboureur,
En tout ce qu'ont chant� David et la Sibylle.
Leur marche est s�re: ils vont illumin�s d'horreur.

Mais la vierge s'�tonne, et, quand la main habile
Du squelette lui prend la taille en amoureux,
Un frisson fait bondir sa belle chair nubile;

Puis, les cils clos, aux bras du danseur aux yeux creux
Elle exhale des mots charmants d'�pithalame,
Car elle est fianc�e au Christ, le divin preux.

Le chevalier errant trouve une �trange dame;
Sur ses c�tes � jour pend, comme sur un gril,
Un reste noir de peau qui fut un sein de femme;

Mais il songe avoir vu dans un bois, en avril,
Une belle duchesse avec sa haquen�e;
Il compte la revoir au ciel. Ainsi soit-il!

Le page, dont la joue est une fleur fan�e,
Va dansant vers l'enfer en un tr�s-doux maintien,
Car il sait clairement que sa dame est damn�e.

L'aveugle besacier ne danserait pas bien,
Mais, sans souffler, la Mort, en discr�te personne,
Coupe tout simplement la corde de son chien:

En suivant � t�tons quelque grelot qui sonne,
L'aveugle s'en va seul tout droit changer de nuit,
Non sans avoir beaucoup jur�. Dieu lui pardone

Il ferme ainsi le bal habilement conduit;
Et tous, porteurs de sceptre et tra�neurs de rapi�re,
S'en sont all�s dormir sans r�volte et sans-bruit.

Ils comptent bien qu'un jour le l�vrier de pierre,
Sous leurs rigides pieds couch� fid�lement,
Saura se r�veiller et l�cher leur paupi�re.

Ils savent que les noirs clairons du jugement,
Qu'on entendra sonner sur chaque s�pulture,
Agiteront leurs os d'un grand tressaillement,

Et que la Mort stupide et la p�le Nature
Verront surgir alors sur les tombeaux ouverts
Le corps ressuscit� de toute cr�ature.

La chair des fils d'Adam sera reprise aux vers;
La Mort mourra: la faim d�truira l'affam�e,
Lorsque l'�ternit� prendra tout l'univers.

Et, m�l�s aux martyrs, belle et candide arm�e,
Les �poux reverront, ceinte d'un nimbe d'or,
Dans les longs plis du lin passer la bien-aim�e.

Mais les couples dont l'Ange aura bris� l'essor,
Sur la berge o� le souffre ardent roule en grands fleuves,
Oui, ceux-l� souffriront: donc ils vivront encor!

Les tragiques amants et les sanglantes veuves,
Voltigeant enlac�s dans leur cercle de fer,
Soupireront sans rin des paroles tr�s-neuves.

Oh! bienheureux ceux-l� qui croyaient � l'Enfer.












La Part De Madeleine.

L'ombre versait au flanc des monts sa paix b�nie,
Le chemin �tait bleu, le feuillage �tait noir,
Et les palmiers tremblaient d'amour au vent du soir.
L'enfant de Magdala, la fleur de B�thanie,

G�missait dans la pourpre et l'azur des coussins.
Le grand �pervier d'or des femmes �trang�res
Agrafait sur son front les �toffes l�g�res;
La myrrhe ti�dissait dans l'ombre de ses seins;

Ses doigts, o� les parfums des jeunes chevelures
Avaient laiss� leur �me et s'exhalaient encor
Autour du scarab�e et des talismans d'or,
Gardaient des souvenirs pareils � des br�lures.

Or elle ha�ssait ce corps qui lui fut cher;
Tous les baisers re�us lui revenaient aux l�vres
Avec l'�cre saveur des d�go�ts et des fi�vres.
Madeleine �tait triste et souffrait dans sa chair;

Et ses l�vres, ainsi qu'une grenade m�re,
Entr'ouvrant leur rubis sous la fra�cheur du ciel,
L'abeille des regrets y mit son �cre miel,
Et le vent qui passait recueillit ce murmure:

� J'avais soif, et j'ai ceint mon front d'amour fleuri;
J'ai pris la bonne part des choses de ce monde,
Et cependant, mon Dieu, ma tristesse est profonde,
Et voici que mon coeur est comme un puits tari!

� Mon �me est comparable � la citerne vide
Sur qui le chamelier ne penche plus son front;
Et l'amour des meilleurs d'entre ceux qui mourront
Est tomb� goutte � goutte au fond du gouffre avide.

� Je n'ai bu que la soif aux l�vres des amants:
Ils sont faits de limon, tous les fils de la m�re;
La fleur de leurs baisers laisse une cendre am�re,
L'�treinte de leurs bras est un choc d'ossements.

� Je brisais malgr� moi l'argile de leur cha�ne.
Seigneur! Seigneur! ce qui n'est plus ne fut jamais!
Leurs souvenirs �taient des morts que j'embaumais
Et qui n'exhalaient plus qu'� peine un peu de haine.

� Et je criais, voyant mon espoir achev�:
� Pleureuses, allumez l'encens devant ma porte,
� Appr�tez un drap d'or: la Madeleine est morte,
� Car �tant la Chercheuse elle n'a pas trouv�! �

� Et j'ouvrais de nouveau mes bras comme des palmes;
J'�tendais mes bras nus tout parfum�s d'amour,
Pour qu'une �me vivante y v�nt dormir un jour,
Et je r�vais encor les vastes amours calmes!

� Le Silence entendit ma voix, qui soupirait
Disant: � La perle dort dans le secret des ondes;
� Or je veux me baigner dans des amours profondes
� Comme tes belles eaux, lac de G�n�sareth!

� Que votre chaste haleine � mon souffle se m�le,
� Tranquilles fleurs des eaux, afin que le baiser
� Que sur le front �lu ma l�vre ira poser,
� Calme comme la mort, soit infini comme elle! �

� Telle je soupirais au bord du lac natal,
Mais sur mes flancs bless�s une mauvaise flamme,
Rebelle, d�vorait ma chair avec mon �me,
Et voici que je meurs sur mon lit de santal.

� Pourtant, j'accepte encor la part de Madeleine
J'avais choisi l'amour et j'avais eu raison.
Comme Marthe, ma soeur, qui garda la maison,
Je n'aurai point pes� la farine ou la laine;

� La jarre, au ventre lourd d'olives ou de vin,
Dans les soins du cellier n'aura point clos ma vie;
Mais ma part, je le sais, ne peut m'�tre ravie,
Et je l'emporterai dans l'inconnu divin! �

Elle dit: le reflet des choses �ternelles
L'illumina d'horreur et d'�pouvantement.
Alors elle se tut et pleura longuement:
Une �me flottait vague au fond de ses prunelles.

Or, J�sus, celui-l� qui chassait le D�mon
Et qui, s'�tant assis au bord de la fontaine,
But dans l'urne de gr�s de la Samaritaine,
Soupait ce m�me soir au logis de Simon.

Vers ce foyer, ce toit fumant entre les branches,
Madeleine tendit, humble, ses belles mains;
Et l'on aurait pu voir des pensers plus qu'humains
Rayonner sur son front comme des lueurs blanches.

La tristesse rendait plus belle sa beaut�;
Ses regards au ciel bleu creusaient un clair sillage,
Et ses longs cils mouill�s �taient comme un feuillage
Dans du soleil, apr�s la pluie, un jour d'�t�.

L'enfant de Magdala, la fleur de B�thanie,
S'en alla vers J�sus qu'on a nomm� le Christ,
Et parfuma ses pieds ainsi qu'il est �crit.
Et la terre connut la tendresse infinie.











Denys Tyran De Syracuse. 

LE TYRAN
Je suis roi, fils de Zeus, car Zeus ayant re�u 
Dans sa couche d�airain la Nuit aux sombres voiles, 
En son flanc mit mon germe. Ainsi je fus con�u 
Avant que dans les cieux veillassent les �toiles.
 
LE CHOEUR
Fils auguste de Zeus et de la sombre Nuit, 
Ne pleure point des cieux l�obscurit� premi�re: 
Nos yeux sont si bien clos que le soleil qui luit, 
N�y pourrait pas glisser un trait de sa lumi�re. 

LE TYRAN
Sachez-le bien: je suis entre vous et les cieux, 
Et je viens parmi vous, esclaves aux fronts p�les, 
Afin que vous n�ayez que ma bouche et mes yeux; 
Et moi j�enfanterai seul entre tous vos m�les. 

LE CHOEUR
Et tu nous vois aussi, troupeau morne et tremblant, 
Au poids de ton cothurne accoutumer nos nuques. 
La belle Libert� nous a tendu son flanc, 
Et nous avons counu que nous �tions eunuques.

LE TYRAN
Si certains sont tent�s de r�pandre, imprudents! 
Le miel que sur leur langue a mis l�Abeille antique, 
Qu�ils se coupent plut�t la langue avec leurs dents, 
Pour que vous l�approuviez, voici ma politique. 

LE CHOEUR
Parle, et ne crains plus, roi, l�Abeille et son miel d�or: 
Sur des l�vres sans voix l�Abeille est expir�e; 
Son miel, trop fort pour nous, en paix suinte encor 
Aux fentes des tombeaux sur la route sacr�e. 

LE TYRAN
Or, vous saurez ceci de moi, qu�une cit� 
Ne vaut pas tant par l�or qui sort des l�vres sages, 
Que par le fer aigu que portent au c�t� 
Ceux qui font dans le sang fleurir les nouveaux �ges. 

LE CHOEUR
Je suis de ton avis, � roi, me souvenant 
Que l�an dernier, trois cents bonnes t�tes civiques, 
En v�rit� faisaient un effet surprenant 
Sur les murs ennemis, mornes, au bout des piques. 

LE TYRAN
Et je vous dis ceci: quand sous le h�tre �pais, 
Assis pour vous juger, je tiendrai la balance, 
J�ordonne que vous tous me regardiez en paix 
Au plateau des amis jeter mon fer de lance. 

LE CHOEUR
Devant ta chaise d�or nous nous tiendrons soumis. 
Roi, nous ha�ssons tous les balances �gales,
Mais au plateau penchant, �tant de tes amis, 
Nous mettrons jusqu�aux clous qui tiennent nos sandales. 

LE TYRAN
Or, ceux que d�entre vous le plus j�honorerai, 
Porteront � genoux � mes blanches cavales, 
De l�avoine dor�e, et je leur permettrai 
De prendre les troupeaux des peuplades rivales. 

LE CHOEUR
Nous briguons tous l�honneur d�apporter � genoux 
Une avoine dor�e � tes cavales blanches, 
� roi; puis, pour ton lit, nous engraissons chez nous, 
Nos femmes aux grands yeux, nos soeurs aux belles hanches. 

LE TYRAN
Cest bien, mais pour ran�on, � dormante cit�, 
Du marbre de tes dieux et du sang de tes sages; 
Pour ran�on de ta gloire et de ta libert�, 
Quel est donc le tr�sor que de moi tu pr�sages? 

LE CHOEUR
La volupt� qui donne et parfume la mort, 
Les spasmes �nervants des amours inf�condes; 
Et, pour farder nos fronts que bl�mit le remord, 
La lie �cre du vin et des bouches immondes.












Les L�gions De Varus. 

Auguste regardait pensif couler le Tibre;
Il songeait aux Germains: ce peuple pur et libre
L��tonnait; ces gens-l� lui causaient quelque effroi:
Ils avaient de grands coeurs et n�avaient point de roi.
C�sar trouvait mauvais qu�ils pussent se permettre
D��tre fiers, et de vivre insolemment sans ma�tre.
Puis le bon C�sar prit piti� de leur erreur
Au point de leur vouloir donner un empereur.
Il crut d�un bon effet qu�aussi l�aigle romaine
Se promen�t un peu par la for�t germaine:
Il n�est tel que son vol pour �blouir les sots
Puis, l�or des chefs germains lui viendrait par boisseaux.
On s�ennuyait; la guerre �tait utile en somme
On n�avait pas d�un an illumin� dans Rome.

Auguste se souvint d�un homme de talent;
Varus s��tait montr� proconsul excellent;
Maigre il �tait entr� dans une place grasse,
Et s�en �tait all� gras d�une maigre place.
Donc Varus, que C�sar aimait pour ses travaux,
Ayant trois l�gions, trois ailes de chevaux,
Et pour arri�re-garde ayant quatre cohortes,
De l�Empire romain les troupes les plus fortes,
Mena ces braves gens � travers les for�ts,
Le front dans les taillis, les pieds dans les marais.

Alors la for�t m�re, inviol�e et sainte,
Etreignit les Romains dans son horrible enceinte,
Les fit choir dans des trous, leur d�roba les cieux;
Chaque arbre avait des doigts et leur crevait les yeux.
Les soldats abattaient ces arbres pleins de haines
Et les chevaux, oyant g�mir l��me des ch�nes,
Se jetaient effar�s dans la nuit des halliers,
Et, contre les troncs durs, brisaient leurs cavaliers.
Des fl�ches cependant venaient, inattendues,
Aux arbres �branl�s, clouer les chairs tordues
Et les soldats mouraient la javeline aux mains.

Hermann �tait debout au milieu des Germains
Le chef dormant s��tait relev� pour leur cause,
Hermann, gloire sans nom! Hermann, l�homme, la chose
De l�antique patrie et de la libert�,
Toujours beau, toujours jeune et toujours indompt�!
Le chef blond �tait l�, dans sa force �ternelle
Pieuse, le gardait la for�t maternelle.
Le chef au pavois rouge, autour du bois hurlant,
Serrait un long cordon de Germains au corps blanc;
Et, trois jours et trois nuits, la sainte Walkyrie,
Sur ces bois pleins de sang, fit planer sa furie
Son oeil bleu souriait -et ses neigeuses mains
Tranch�rent le jarret aux enfants des Romains.
Lorsque le courrier vint, poudreux, dire l�arm�e
De l�empire romain dormant sous la ram�e,
L�Empereur en con�ut de si fortes douleurs
Qu�il �ta de son front sa couronne de fleurs,
Et renvoya la foule au milieu d�une f�te;
Aux tapis de son lit il se cogna la t�te,
En s��criant: "Varus, rends-moi mes l�gions!".
Bien quitte alors envers les expiations,
Il allait s�endormir, quand, pleurante et meurtrie,
Devant ses yeux mal clos, se dressa la Patrie.

� C�sar, rends-moi mes fils, lui dit-elle; assassin,
Rends-moi, rends-moi ma chair et le lait de mon sein!
C�sar, trois fois sacr�, toi qui m�as viol�e,
Et qui m�as encha�n�e et qui m�a mutil�e,
Oui, la chair et le sang de mes plus beaux guerriers,
N�est vraiment qu�un fumier � verdir tes lauriers:
A leur cime, une s�ve �pouvantable monte,
H�las! et fait fleurir ma mis�re et ma honte.
Et je n�ai plus mes fils, ceux qui dans mes beaux jours
Me couronnaient d��pis, me couronnaient de tours.
Rends-moi mes l�gions, ma force et ma couronne,
Et dors sous tes lauriers, car leur ombre empoisonne!
Autrefois, quand, aux jours de ma f�condit�,
J�enfantais dans la gloire et dans la libert�,
Je riais � mes fils morts pour la cause sainte,
Tomb�s en appelant ceux dont j��tais enceinte
Leurs fr�res �taient pr�ts, et mon oeil radieux
Les suivait citoyens, les perdait demi-dieux.
Je sentais des guerriers fr�mir dans mes entrailles,
Et mon lait refaisait du sang pour les batailles...
Mais comme la lionne, en sa captivit�,
Je fais tout mon orgueil de ma st�rilit�.
C�sar! vois mes beaut�s maternelles fl�tries;
Vois pendre tristement mes mamelles taries.
Sur les fruits de ton viol mes flancs se sont ferm�s;

Je ne veux pas des fils que ton sang a form�s.
Rends-moi mes l�gions, ces derni�res reliques
De la force romaine et des vertus publiques!
C�sar! rends-moi leur sang pr�cieux et sacr�;
Rends-moi mes l�gions!... mais non, non; je croirai
Le ciel assez cl�ment et toi-m�me assez juste,
Si seulement tu veux, divin C�sar-Auguste,
De tout ce sang glac� que les lunes du nord
Boivent, de tant de chairs que la dent des loups mord,
Me rendre ce qu�il faut de nerfs, de chair et d��me,
Pour tirer de ton cou tordu ton souffle inf�me! �

Ainsi, sur l�empereur roulant ses yeux ardents,
Hurla la Louve, avec des grincements de dents.
Puis Auguste entendit des murmures fun�bres
Tout remplis de son nom monter dans les t�n�bres
Formidables, et vit, par le ciel entr�ouvert,
Des soldats d�filer, blancs sous leur bronze vert;
Et Varus, qui menait la troupe p�le et lente,
Leur montrait le C�sar de sa droite sanglante.
C�sar ferma les yeux et sentit, tout tremblant,
Ses lauriers d�or glacer son front humide et blanc.
Tendant ses maigres bras au ciel de Germanie,
Il cria, bl�me, avec un r�le d�agonie:
�Varus! garde la troupe intr�pide qui dort!
Garde mes l�gions, � ma complice! � Mort! �










 
L'Autographe.

A Etienne Charavay.

Cette feuille soupire une �trange �l�gie, 
Car la reine d��cosse aux l�vres de carmin 
Qui r�citait Ronsard et le Missel romain, 
A mis l� pour jamais un peu de sa magie. 
La Reine blonde avec sa d�bile �nergie 
Signa Marie au bas de ce vieux parchemin, 
Et le feuillet pensif a ti�di sous sa main 
Que bleuissait un sang fier et prompt � l�orgie. 
L� de merveilleux doigts de femme sont pass�s 
Tout empreints du parfum des cheveux caress�s 
Dans le royal orgueil d�un sanglant adult�re. 
J�y retrouve l�odeur et les reflets ros�s 
De ces doigts aujourd�hui muets, d�compos�s, 
Chang�s peut-�tre en fleurs dans un champ solitaire.



Source: http://www.poesies.net
