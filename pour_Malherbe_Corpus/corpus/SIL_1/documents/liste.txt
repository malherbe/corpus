
Poésie

    Rimes neuves et vieilles, avec une préface de George Sand (1866) voir sur Gallica [1] [archive]
    Les Renaissances (1870)
    La Gloire du souvenir, poème d'amour (1872)
    Poésies, 1866-1874. Les Amours. La Vie. L'Amour (1875)
    La Chanson des heures, poésies nouvelles (1874-1878) (1878)
    Le Pays des roses, poésies nouvelles, 1880-1882 (1882)
    Le Chemin des étoiles : les Adorations, la Chanson des jours, Musiques d'amour, Dernières tendresses, Poèmes dialogués, 1882-1885 (1885)
    Le Dessus du panier : Impressions et souvenirs, Soleils toulousains, Propos de saison, Au pays des rêves (1885)
    Poésies, 1872-1878. La Chanson des heures (1887)
    Les Ailes d'or, poésies nouvelles (1890)
    Roses d'octobre, poésies, 1884-1889 (1890)
    Poésies, 1866-1872. Rimes neuves et vieilles. Les Renaissances. La Gloire du souvenir (1892)
    L'Or des couchants, poésies nouvelles, 1889-1892 (1892)
    Trente Sonnets pour Mademoiselle Bartet (1896)
    Les Aurores lointaines, poésies nouvelles, 1892-1895 (1896)
    Les Tendresses, poésies nouvelles, 1895-1898 (1898)
    Les Fleurs d'hiver, poésies nouvelles, 1898-1900 (1900)
