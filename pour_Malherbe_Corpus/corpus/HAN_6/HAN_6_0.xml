<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Smylis</title>
				<title type="sub">BALLET EN UN ACTE</title>
				<title type="medium">Édition électronique</title>
				<author key="HAN">
					<name>
						<forename>Théodore</forename>
						<surname>HANNON</surname>
					</name>
					<date from="1851" to="1916">1851-1916</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">HAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Smylis</title>
						<author>Théodore HANNON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>books.google.com</publisher>
						<idno type="URI">https://books.google.fr/books?id=bNiq8sp4etMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Smylis</title>
								<title>BALLET EN UN ACTE</title>
								<author>Théodore HANNON</author>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimerie A. LEFEVRE</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1891">1891</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-12-18" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2022-12-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="HAN90">
				<head type="main">Smylis</head>
				<p>
					PERSONNAGES <lb/>
					Smylis . . . . . . . . Mmes RICCIO <lb/>
					Myrrha . . . . . . . . STRAMEZZI <lb/>
					Cléon . . . . . . . .  M. F. DUCHAMPS <lb/>
					Prêtresses, Amours, Habitantes de l’île.
				</p>
				<lg n="1">
					<l><space unit="char" quantity="8"/>C’est dans l’Île chère à Catulle</l>
					<l><space unit="char" quantity="8"/>Mendès, et que le mâle auteur</l>
					<l><space unit="char" quantity="8"/>des Fleurs du Mal lui-même adule</l>
					<l><space unit="char" quantity="8"/>en un poème évocateur,</l>
					<l><space unit="char" quantity="8"/>l’Île où la caresse saline</l>
					<l><space unit="char" quantity="8"/>de la brise n’a rien d’amer,</l>
					<l><space unit="char" quantity="8"/>Lesbos - pour qui se fait câline</l>
					<l><space unit="char" quantity="8"/>Sous l’outremer des cieux, la mer !</l>
				</lg>
				<div type="section" n="1">
					<head type="main">Scène Première</head>
					<head type="sub">LES LESBIENNES</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>A l’ombre d’un Temple où s’adore</l>
						<l><space unit="char" quantity="8"/>Vénus vierge éternellement</l>
						<l><space unit="char" quantity="8"/>et dont l’égoïsme se dore</l>
						<l><space unit="char" quantity="8"/>aux feux morts d’un amour qui ment,</l>
						<l><space unit="char" quantity="8"/>une pelouse d’émeraude</l>
						<l><space unit="char" quantity="8"/>s’étale, chaude des parfums</l>
						<l><space unit="char" quantity="8"/>de l’oranger en fleur où rôde</l>
						<l><space unit="char" quantity="8"/>l’âme des purs baisers défunts…</l>
						<l><space unit="char" quantity="8"/>Près d’un bosquet au frais ombrage,</l>
						<l><space unit="char" quantity="8"/>mis paradoxalement là</l>
						<l><space unit="char" quantity="8"/>un Cupidon, que dans sa rage</l>
						<l><space unit="char" quantity="8"/>une prêtresse mutila,</l>
						<l><space unit="char" quantity="8"/>bande encor son arc inutile.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Autour de lui s’en vont nouant</l>
						<l><space unit="char" quantity="8"/>une ronde moqueuse, hostile,</l>
						<l><space unit="char" quantity="8"/>les habitantes se jouant</l>
						<l><space unit="char" quantity="8"/>de ses vaines flèches brisées</l>
						<l><space unit="char" quantity="8"/>loin du sang des cœurs amoureux.</l>
						<l><space unit="char" quantity="8"/>Dans l’herbe aux pointes irisées,</l>
						<l><space unit="char" quantity="8"/>les couples se livrent entre eux</l>
						<l><space unit="char" quantity="8"/>à leur art favori, la danse,</l>
						<l><space unit="char" quantity="8"/>et, souriant à ces ébats,</l>
						<l><space unit="char" quantity="8"/>doucement rythme la cadence</l>
						<l><space unit="char" quantity="8"/>l’océan qui chante là -bas…</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="main">Scène Deuxième</head>
					<head type="sub">LES LESBIENNES. LES PRÊTRESSES, <lb/>puis SMYLIS et MYRRHA.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Soudain, la svelte théorie</l>
						<l><space unit="char" quantity="8"/>s’immobilise au gazon vert,</l>
						<l><space unit="char" quantity="8"/>debout, haletante, attendrie :</l>
						<l><space unit="char" quantity="8"/>du Temple maintenant ouvert</l>
						<l><space unit="char" quantity="8"/>la lente plainte d’un cantique</l>
						<l><space unit="char" quantity="8"/>a fait entendre son accent,</l>
						<l><space unit="char" quantity="8"/>et paraissent sous le portique</l>
						<l><space unit="char" quantity="8"/>de blanches Prêtresses haussant</l>
						<l><space unit="char" quantity="8"/>dans l’air tout embaumé de myrrhe</l>
						<l><space unit="char" quantity="8"/>les souples voiles consacrés</l>
						<l><space unit="char" quantity="8"/>où le ciel rayonnant se mire.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Sous ce dais aux reflets nacrés,</l>
						<l><space unit="char" quantity="8"/>langoureusement enlacées</l>
						<l><space unit="char" quantity="8"/>du rose éclair de leurs bras nus,</l>
						<l><space unit="char" quantity="8"/>s’avancent, neuves fiancées</l>
						<l><space unit="char" quantity="8"/>de la virginale Vénus</l>
						<l><space unit="char" quantity="8"/>qui rend les cœurs invulnérables,</l>
						<l><space unit="char" quantity="8"/>s’avancent Smylis et Myrrha,</l>
						<l><space unit="char" quantity="8"/>ces compagnes inséparables</l>
						<l><space unit="char" quantity="8"/>qu’Amour pourtant désunira.</l>
					</lg>
					<lg n="3">
						<l><space unit="char" quantity="8"/>Elles viennent d’être ordonnées</l>
						<l><space unit="char" quantity="8"/>Prêtresses… faisant le serment</l>
						<l><space unit="char" quantity="8"/>d’être à leur seul culte adonnées :</l>
						<l><space unit="char" quantity="8"/>haine à tout autre sentiment</l>
						<l><space unit="char" quantity="8"/>par qui l’amour se perpétue !</l>
						<l><space unit="char" quantity="8"/>Rigides, regards assombris,</l>
						<l><space unit="char" quantity="8"/>elles marchent vers la statue</l>
						<l><space unit="char" quantity="8"/>et, narguant ses pâles débris,</l>
						<l><space unit="char" quantity="8"/>l’ont ironiquement parée</l>
						<l><space unit="char" quantity="8"/>du voile où leur rancune dort,</l>
						<l><space unit="char" quantity="8"/>puis, en rappel de foi jurée,</l>
						<l><space unit="char" quantity="8"/>échangent leurs bracelets d’or.</l>
					</lg>
					<lg n="4">
						<l><space unit="char" quantity="8"/>Les danses reprennent, joyeuses.</l>
					</lg>
					<lg n="5">
						<l><space unit="char" quantity="8"/>En des doigts souples, tour à tour,</l>
						<l><space unit="char" quantity="8"/>on voit les écharpes soyeuses</l>
						<l><space unit="char" quantity="8"/>changer de ligne et de contour ;</l>
						<l><space unit="char" quantity="8"/>les impalpables banderoles</l>
						<l><space unit="char" quantity="8"/>se métamorphosent en lis</l>
						<l><space unit="char" quantity="8"/>et dans les magiques corolles</l>
						<l><space unit="char" quantity="8"/>Myrrha va poursuivant Smylis.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="main">Scène Troisième</head>
					<head type="sub">LES MÊMES <lb/>un vaisseau passe à l’horizon.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Tout à coup les jeux s’interrompent.</l>
						<l><space unit="char" quantity="8"/>Un navire cingle incliné</l>
						<l><space unit="char" quantity="8"/>Sous sa voilure… des voix rompent</l>
						<l><space unit="char" quantity="8"/>le grand silence profané :</l>
					</lg>
					<lg n="2">
						<head type="main">LES NAUTONIERS.</head>
						<l>Sous ce beau soleil d’or, hardi les matelots !</l>
						<l>l’île infâme, Lesbos, déshonore les flots.</l>
						<l>Nous allons conquérir ses bosquets, ses gazons</l>
						<l>où l’Amour n’a jamais soupiré ses chansons.</l>
						<l>L’île infâme, Lesbos, déshonore les flots,</l>
						<l>sous ce beau soleil d’or, hardi les matelots !</l>
					</lg>
					<lg n="3">
						<head type="main">SMYLIS.</head>
						<l><space unit="char" quantity="8"/>Quel est ce refrain qui blasphème ?</l>
					</lg>
					<lg n="4">
						<head type="main">LES NAUTONIERS.</head>
						<l>L’île infâme, Lesbos, déshonore les flots !</l>
					</lg>
					<lg n="5">
						<head type="main">MYRRHA.</head>
						<l><space unit="char" quantity="12"/>Anathème ! Anathème !</l>
					</lg>
					<lg n="6">
						<head type="main">LES NAUTONIERS.</head>
						<l>Sous ce beau soleil d’or, hardi les matelots !</l>
					</lg>
					<lg n="7">
						<head type="main">SMYLIS et MYRRHA.</head>
						<l><space unit="char" quantity="8"/>O Déesse, puisse ta foudre</l>
						<l><space unit="char" quantity="8"/>réduire ces maudits en poudre !</l>
						<l><space unit="char" quantity="8"/>Tombe et bûcher, que leur navire</l>
						<l><space unit="char" quantity="8"/>sous l’éclair s’embrâse et chavire !</l>
					</lg>
					<lg n="8">
						<head type="main">LES LESBIENNES.</head>
						<l><space unit="char" quantity="8"/>Tombe et bûcher que leur navire</l>
						<l><space unit="char" quantity="8"/>Sous l’éclair s’embrâse et chavire !</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="main">Scène Quatrième</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Le ciel s’est obscurci, l’orage</l>
						<l><space unit="char" quantity="8"/>éclate, l’éclair a brillé</l>
						<l><space unit="char" quantity="8"/>et sous la foudre qui fait rage</l>
						<l><space unit="char" quantity="8"/>le navire en flamme a coulé.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Sur lui se referment les ondes.</l>
						<l><space unit="char" quantity="8"/>L’océan déchaîné bondit,</l>
						<l><space unit="char" quantity="8"/>cabre, et vers ses tombes profondes</l>
						<l><space unit="char" quantity="8"/>roule l’équipage maudit.</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="main">Scène Cinquième</head>
					<head type="sub">SMYLIS, puis CLÉON.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Cléon seul est encore en vie,</l>
						<l><space unit="char" quantity="8"/>il nage vers l’île où le flot</l>
						<l><space unit="char" quantity="8"/>livre à Vénus inassouvie</l>
						<l><space unit="char" quantity="8"/>le sacrilège matelot…</l>
						<l><space unit="char" quantity="8"/>L’azur a rallumé sa flamme.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Au loin les Lesbiennes ont fui</l>
						<l><space unit="char" quantity="8"/>laissant Smylis, le trouble en l’âme,</l>
						<l><space unit="char" quantity="8"/>près de Cléon évanoui.</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="main">Scène Sixième</head>
					<head type="sub">LES MÊMES, MYRRHA.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Un sentiment nouveau s’empare</l>
						<l><space unit="char" quantity="8"/>d’elle son cœur reste interdit</l>
						<l><space unit="char" quantity="8"/>devant ce visage que pare</l>
						<l><space unit="char" quantity="8"/>un charme pour elle inédit.</l>
						<l><space unit="char" quantity="8"/>L’amour victorieux s’éveille</l>
						<l><space unit="char" quantity="8"/>au fond de son être en émoi…</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Jalouse, Myrrha la surveille,</l>
						<l part="I"><space unit="char" quantity="8"/>inquiète elle accourt : </l>
					</lg>
					<lg n="3">
						<head type="main">MYRRHA.</head>
						<l part="F"><space unit="char" quantity="8"/>« Suis-moi !</l>
						<l><space unit="char" quantity="8"/>» Abandonne ce sacrilège.  »</l>
					</lg>
					<lg n="4">
						<head type="main">SMYLIS.</head>
						<l><space unit="char" quantity="8"/>« Je veux l’arracher à la mort. »</l>
					</lg>
					<lg n="5">
						<head type="main">MYRRHA.</head>
						<l><space unit="char" quantity="8"/>« O ma sœur, par quel sortilège</l>
						<l><space unit="char" quantity="8"/>» te fait-il briser sans remord</l>
						<l><space unit="char" quantity="8"/>» et tes serments et ma tendresse ?</l>
						<l><space unit="char" quantity="8"/>» Son trépas soit le bien venu ! »</l>
					</lg>
					<lg n="6">
						<l><space unit="char" quantity="8"/>Entre elles un poignard se dresse,</l>
						<l><space unit="char" quantity="8"/>Myrrha va frapper l’inconnu.</l>
						<l><space unit="char" quantity="8"/>Smylis en un élan superbe</l>
						<l><space unit="char" quantity="8"/>tend sa poitrine au fer qui luit…</l>
						<l><space unit="char" quantity="8"/>Et le poignard tombe dans l’herbe…</l>
					</lg>
					<lg n="7">
						<l><space unit="char" quantity="8"/>Menaçante Myrrha s’enfuit,</l>
					</lg>
				</div>
				<div type="section" n="7">
					<head type="main">Scène Septième</head>
					<head type="sub">SMYLIS, CLÉON, puis MYRRHA.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Enfin seule, la vierge blonde</l>
						<l><space unit="char" quantity="8"/>va puiser au flot assassin</l>
						<l><space unit="char" quantity="8"/>et rend miraculeuse l’onde</l>
						<l><space unit="char" quantity="8"/>qui voulait noyer dans son sein</l>
						<l><space unit="char" quantity="8"/>l’être dont s’émeut tout son être</l>
						<l><space unit="char" quantity="8"/>et dont s’émerveillent ses yeux…</l>
						<l><space unit="char" quantity="8"/>Surpris, Cléon se sent renaître,</l>
						<l><space unit="char" quantity="8"/>éploré d’abord, puis joyeux</l>
						<l><space unit="char" quantity="8"/>en apercevant la prêtresse</l>
						<l><space unit="char" quantity="8"/>dont la radieuse bonté</l>
						<l><space unit="char" quantity="8"/>le secoura dans sa détresse.</l>
						<l><space unit="char" quantity="8"/>Myrrha qui dans l’ombre a guetté</l>
						<l><space unit="char" quantity="8"/>le moment redoutable, vole</l>
						<l part="I"><space unit="char" quantity="8"/>à Smylis, et rude : </l>
						<l part="F"><space unit="char" quantity="8"/>« Je veux</l>
						<l><space unit="char" quantity="8"/>» te voir me suivre, ô sœur frivole,</l>
						<l><space unit="char" quantity="8"/>» en oubli des plus tendres vœux…</l>
						<l part="I"><space unit="char" quantity="8"/>» Souviens-toi ! » </l>
						<l part="F"><space unit="char" quantity="8"/>Smylis qui redoute</l>
						<l><space unit="char" quantity="8"/>sa colère, laisse Cléon</l>
						<l><space unit="char" quantity="8"/>rempli d’angoisse, en proie au doute,</l>
						<l><space unit="char" quantity="8"/>cherchant dans ses yeux un rayon…</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Myrrha l’accable de sa haine</l>
						<l><space unit="char" quantity="8"/>et d’un mouvement emporté</l>
						<l><space unit="char" quantity="8"/>vers les lauriers roses entraîne</l>
						<l><space unit="char" quantity="8"/>sa compagne sans volonté.</l>
						<l><space unit="char" quantity="8"/>Smylis parvient à se reprendre</l>
						<l><space unit="char" quantity="8"/>et, rompant le charme, soudain</l>
						<l><space unit="char" quantity="8"/>à Cléon elle court se rendre.</l>
					</lg>
				</div>
				<div type="section" n="8">
					<head type="main">Scène Huitième</head>
					<head type="sub">LES MÊMES, moins MYRRHA.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Myrrha, qu’affole ce dédain,</l>
						<l><space unit="char" quantity="8"/>s’éloigne, la fureur en elle,</l>
						<l><space unit="char" quantity="8"/>et le vainqueur va soupirant</l>
						<l><space unit="char" quantity="8"/>la douce romance éternelle</l>
						<l><space unit="char" quantity="8"/>à Smylis dont le cœur s’éprend.</l>
					</lg>
				</div>
				<div type="section" n="9">
					<head type="main">Scène Neuvième</head>
					<head type="sub">LES MÊMES, MYRRHA, LES LESBIENNES.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Mais déjà voici reparaître</l>
						<l><space unit="char" quantity="8"/>Myrrha brûlant de se venger</l>
						<l><space unit="char" quantity="8"/>et de la traîtresse et du traître</l>
						<l><space unit="char" quantity="8"/>qu’elle voudrait voir égorger</l>
						<l><space unit="char" quantity="8"/>par les Lesbiennes en furie</l>
						<l><space unit="char" quantity="8"/>dont elle accompagne ses pas.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Au travers de l’Île fleurie</l>
						<l><space unit="char" quantity="8"/>court le frisson noir du trépas…</l>
						<l><space unit="char" quantity="8"/>et la chantante mer s’est tue.</l>
					</lg>
					<lg n="3">
						<l><space unit="char" quantity="8"/>Cependant Smylis et Cléon</l>
						<l><space unit="char" quantity="8"/>Bondissant jusqu’à la statue,</l>
						<l><space unit="char" quantity="8"/>implorent la protection</l>
						<l part="I"><space unit="char" quantity="8"/>de l’enfant dieu… </l>
					</lg>
					<lg n="4">
						<l part="F"><space unit="char" quantity="8"/>La horde hésite,</l>
						<l><space unit="char" quantity="8"/>les Lesbiennes ont reculé.</l>
						<l><space unit="char" quantity="8"/>Myrrha du geste les excite :</l>
						<l><space unit="char" quantity="8"/>« Oui, sans pitié, soit immolé</l>
						<l><space unit="char" quantity="8"/>» à la Déesse qu’il outrage,</l>
						<l><space unit="char" quantity="8"/>» de Lesbos le violateur ! »</l>
					</lg>
				</div>
				<div type="section" n="10">
					<head type="main">Scène Dixième</head>
					<head type="sub">LES MÊMES, LES AMOURS.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Et les vierges ivres de rage</l>
						<l><space unit="char" quantity="8"/>insultant au dieu rédempteur</l>
						<l><space unit="char" quantity="8"/>se ruent… puis restent interdites :</l>
						<l><space unit="char" quantity="8"/>une bande d’Amours armés</l>
						<l><space unit="char" quantity="8"/>des flèches par elles maudites,</l>
						<l><space unit="char" quantity="8"/>au cri de leurs sens alarmés,</l>
						<l><space unit="char" quantity="8"/>du bosquet en riant s’échappe.</l>
					</lg>
					<lg n="2">
						<l><space unit="char" quantity="8"/>Ils viennent protéger, vainqueurs,</l>
						<l part="I"><space unit="char" quantity="8"/>Smylis et Cléon. </l>
					</lg>
					<lg n="3">
						<l part="F"><space unit="char" quantity="8"/>Leur bras frappe</l>
						<l><space unit="char" quantity="8"/>choisissant pour cibles les cœurs</l>
						<l><space unit="char" quantity="8"/>des Lesbiennes épouvantées</l>
						<l><space unit="char" quantity="8"/>et sentant des feux inconnus</l>
						<l><space unit="char" quantity="8"/>brûler leurs chairs déconcertées.</l>
					</lg>
					<lg n="4">
						<l><space unit="char" quantity="8"/>Inconscients, leurs beaux bras nus</l>
						<l><space unit="char" quantity="8"/>qui ne cherchent plus à s’étreindre,</l>
						<l><space unit="char" quantity="8"/>se sont tendus vers les amants…</l>
					</lg>
					<lg n="5">
						<l><space unit="char" quantity="8"/>En vain Myrrha veut-elle éteindre</l>
						<l><space unit="char" quantity="8"/>au souvenir de leurs serments</l>
						<l><space unit="char" quantity="8"/>ces flammes purificatrices,</l>
						<l><space unit="char" quantity="8"/>les Prêtresses à leurs bûchers</l>
						<l><space unit="char" quantity="8"/>courent, tendant provocatrices</l>
						<l><space unit="char" quantity="8"/>la poitrine aux subtils archers.</l>
					</lg>
				</div>
				<div type="section" n="11">
					<head type="main">Scène Onzième</head>
					<head type="sub">LES MÊMES, L’AMOUR.</head>
					<lg n="1">
						<l><space unit="char" quantity="8"/>Myrrha, dans son délire extrême,</l>
						<l><space unit="char" quantity="8"/>stylet au poing va se jeter</l>
						<l><space unit="char" quantity="8"/>sur Cléon que dans sa trirème</l>
						<l><space unit="char" quantity="8"/>avec Smylis veut emporter</l>
						<l part="I"><space unit="char" quantity="8"/>au loin l’Amour : </l>
					</lg>
					<lg n="2">
						<l part="F"><space unit="char" quantity="8"/>" Meurs ! " lui dit- elle…</l>
						<l><space unit="char" quantity="8"/>Mais dans le sein, - vengeur, - Éros</l>
						<l><space unit="char" quantity="8"/>lui plonge une flèche mortelle.</l>
						<ab type="dot">. . . . . . . . . . . . . . . . .</ab>
						<l><space unit="char" quantity="8"/>Et Cythère a conquis Lesbos !</l>
					</lg>
				</div>
			</div>
		</body>
	</text>
</TEI>