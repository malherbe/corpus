<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="sub_2">Compléments</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5748094b</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<author>Jean-Pierre Claris de Florian</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DES BIBLIOPHILES</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1802">1802</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne contient que les fables non incluses dans le premier corpus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="FLO112">
				<head type="number">XI</head>
				<head type="main">LES DEUX SŒURS</head>
				<head type="sub_2">ou</head>
				<head type="sub_1">LA GLOIRE ET LA VERTU</head>
				<p>
					FABLE ADRESSÉE <lb/>
					A Madame la duchesse d’Orléans et à Monseigneur <lb/>
					le prince Henri qui avoient fait à l’auteur <lb/>
					l’honneur de venir déjeuner chez lui.
				</p>
				<lg n="1">
					<l><space unit="char" quantity="8"/>La Gloire, lasse de travaux,</l>
					<l>Se mit à voyager. Sa suite étoit brillante :</l>
					<l><space unit="char" quantity="8"/>C’étoient des guerriers, des héros,</l>
					<l><space unit="char" quantity="8"/>Qui partout semoient l’épouvante ;</l>
					<l>On encensoit la Gloire en mourant de frayeur.</l>
					<l><space unit="char" quantity="8"/>Elle étoit pourtant bonne femme,</l>
					<l><space unit="char" quantity="8"/>Aimable et fière avec douceur.</l>
					<l>Bientôt sur son chemin elle trouve une dame</l>
					<l>Grande, noble, modeste et simple en ses habits ;</l>
					<l>La candeur se peignoit sur son front sans nuage,</l>
					<l><space unit="char" quantity="8"/>L’aménité sur son visage,</l>
					<l><space unit="char" quantity="8"/>Et la bonté dans son souris.</l>
					<l><space unit="char" quantity="8"/>A sa suite quelques amis,</l>
					<l>Peu nombreux, mais bien sûrs, formoient sa cour fidèle ;</l>
					<l>L’air qu’elle respirait en devenoit plus pur.</l>
					<l>A peine de ses yeux la Gloire a vu l’azur</l>
					<l>Qu’elle court à ses pieds. « Je vous cherche, dit-elle ;</l>
					<l><space unit="char" quantity="8"/>De mes jours voici le plus beau.</l>
					<l>Je vous suivrai partout, un sentiment nouveau</l>
					<l>M’avertit que vous seule êtes le bien suprême.</l>
					<l>J’ai triomphé souvent ; c’est un triste plaisir :</l>
					<l><space unit="char" quantity="8"/>Je trouve plus doux de servir</l>
					<l><space unit="char" quantity="8"/>L’objet qu’on révère et qu’on aime. »</l>
					<l>Elle dit. La Vertu la traite comme sœur,</l>
					<l><space unit="char" quantity="8"/>Ensemble elles font le voyage.</l>
					<l>Toutes deux y gagnoient : la Gloire, le bonheur</l>
					<l><space unit="char" quantity="8"/>La Vertu, son plus digne hommage.</l>
				</lg>
				<lg n="2">
					<l><space unit="char" quantity="8"/>Ce matin dans mon ermitage</l>
					<l><space unit="char" quantity="8"/>J’ai reçu ce couple enchanteur.</l>
				</lg>
			</div>
			<div type="poem" key="FLO113">
				<head type="number">XII</head>
				<head type="main">L’AIGLE ET LA FOURMI</head>
				<head type="sub_1">PAR M. DE FLORIAN</head>
				<head type="sub_1">En envoyant ses Fables à M. Hérivaux.</head>
				<lg n="1">
					<l><space unit="char" quantity="8"/>Du dieu qui lance le tonnerre</l>
					<l><space unit="char" quantity="8"/>Un beau jour l’oiseau favori,</l>
					<l><space unit="char" quantity="8"/>Dirigeant son vol sur la terre,</l>
					<l>S’abattit justement tout près d’une fourmi,</l>
					<l><space unit="char" quantity="12"/>Qui parmi la fougère,</l>
					<l><space unit="char" quantity="12"/>Non loin de ses foyers,</l>
					<l><space unit="char" quantity="12"/>En bonne ménagère</l>
					<l><space unit="char" quantity="4"/>Alloit, venoit, pour remplir ses greniers.</l>
					<l>Jugez de sa surprise en voyant si près d’elle</l>
					<l>Le superbe habitant du céleste séjour.</l>
					<l>L’aigle ne la vit point ; sa brillante prunelle</l>
					<l>Ne sut jamais fixer que le flambeau du jour.</l>
					<l>L’insecte veut d’abord regagner sa cellule ;</l>
					<l>Il s’arrête, il hésite, il avance, il recule ;</l>
					<l>Un désir curieux s’oppose à son retour,</l>
					<l>Et bientôt, bannissant un frivole scrupule,</l>
					<l><space unit="char" quantity="4"/>Au roi des airs il veut faire sa cour.</l>
					<l>Méditant sa harangue et composant sa mine,</l>
					<l>Vers l’aigle sur-le-champ la fourmi s’achemine.</l>
					<l>« O vous, dit-elle, ô vous qu’en ces champêtres lieux</l>
					<l>Pour la première fois aperçoivent mes yeux,</l>
					<l>Excusez-moi, Seigneur, si je vous importune,</l>
					<l><space unit="char" quantity="12"/>Et souffrez qu’un moment</l>
					<l>Je goûte auprès de vous le doux contentement</l>
					<l><space unit="char" quantity="8"/>Que m’offre ma bonne fortune.</l>
					<l><space unit="char" quantity="8"/>Par quelques mets dignes de vous</l>
					<l>Je voudrais vous prouver mon respect et mon zèle ;</l>
					<l>Mais une humble fourmi n’a que ses vœux pour elle,</l>
					<l>Et le riche Plutus, de ses trésors jaloux,</l>
					<l>Ne m’en donna jamais la plus simple parcelle.</l>
					<l><space unit="char" quantity="8"/>Pour moi daignez être indulgent,</l>
					<l>Et des grains qu’amassa ma pénible industrie</l>
					<l><space unit="char" quantity="12"/>Que Votre Seigneurie</l>
					<l><space unit="char" quantity="12"/>Accepte le présent.</l>
					<l><space unit="char" quantity="12"/>Si de ma foible offrande</l>
					<l><space unit="char" quantity="12"/>Vous faites quelque cas,</l>
					<l>Du sort j’aurai reçu la faveur la plus grande ;</l>
					<l>De ses longues ligueurs je ne me plaindrai pas. »</l>
					<l><space unit="char" quantity="4"/>L’aigle sourit à notre discoureuse,</l>
					<l><space unit="char" quantity="4"/>Et, déployant son aile vigoureuse,</l>
					<l>Il l’aide à s’y placer ; puis, dans l’air s’élançant,</l>
					<l><space unit="char" quantity="16"/>En un instant</l>
					<l>Il l’emporte au-dessus de la voûte azurée,</l>
					<l>Interdite, confuse, à peine rassurée.</l>
					<l><space unit="char" quantity="8"/>Là, dans un palais enchanté</l>
					<l>Où de tableaux charmants une suite choisie</l>
					<l>Flatte l’esprit, le cœur, par sa variété,</l>
					<l>Il l’accueille, et, d’un air rempli de courtoisie,</l>
					<l>Pour un peu de millet par elle présenté,</l>
					<l><space unit="char" quantity="8"/>Il lui prodigue avec bonté</l>
					<l><space unit="char" quantity="8"/>Et le nectar et l’ambroisie.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>
