<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HAINE AUX BARBARES</title>
				<title type="sub_1">CHANTS PATRIOTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="MDC">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>MÉRI DE LA CANORGUE</surname>
					</name>
					<date from="1805" to="1875">1805-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MDC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
						<author>VICTOR DE MÉRI DE LA CANORGUE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30929677r.public</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
								<author>VICTOR DE MÉRI DE LA CANORGUE</author>
								<imprint>
									<pubPlace>MARSEILLE</pubPlace>
									<publisher>CAMOIN LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<front>
			<p>
				Il faut remonter à quinze cents ans dans l’histoire, pour trouver quelque chose d’analogue à cette nouvelle invasion de barbares, qui vient de porter la désolation, le deuil, le ravage et la mort dans notre belle France, naguère encore si prospère, et qui pleure aujourd’hui désolée sur ses champs dévastés, et sur les ruines fumantes de ses villes, de ses palais, de ses églises et même des asiles sacrés, où la religion et la charité se donnaient rendez-vous pour y soulager et y consoler tous les genres de misères humaines.
				Attila, le fléau de Dieu, ne pouvait certes se reproduire à nos yeux sous des traits plus semblables aux siens que ceux de son féroce imitateur. Toutefois le roi des Huns me parait moins exécrable que son rival en barbarie, En ce sens que, n’étant point chrétien, et que n’ayant point paru sur la terre à une époque de civilisation avancée comme la nôtre, il lui était permis de méconnaitre le droit des gens, les lois de l’humanité, qui sont un frein aux lois de la guerre ; tandis que Guillaume osait s’autoriser du nom de Dieu pour permettre à ses soldats de se livrer à tous les genres de crimes, tels que le meurtre des gens inoffensifs, le pillage, le vol, l’incendie et le viol, et
				c’est ainsi qu’il a prétendu nous moraliser, le féroce hypocrite !
				Tel est l’effrayant tableau de toutes les horreurs que j’ai essayé de représenter dans mes vers, non-seulement pour en flétrir les auteurs, mais encore pour faire ainsi ressortir l’héroïsme de nos soldats dans une lutte désespérée et sublime et rendue si désastreuse, par la félonie, l’incurie et la lâcheté d’un homme à jamais infâme et de quelques-uns de ses généraux.
				Les nations qui nous devaient le plus et qui nous ont abandonné sans nous prêter secours, si ce n’est par de stériles sympathies, ont eu aussi leur part de flétrissure dans mes chants patriotiques.
				Si mon âge avancé ne m’a pas permis de prendre les armes pour la défense de mon malheureux pays, j’ai voulu lui prouver du moins combien je l’aimais, et tout ce que mon cœur de Français a souffert et souffre encore de ses douleurs imméritées, do ses désastres et de ses profondes humiliations.

				AVANT-PROPOS

				LES BARBARES EN FRANCE
			</p>
		</front>
		<body>
			<div type="poem">
				<head type="main">UN CRI VENGEUR</head>
				<head type="form">1er CHANT</head>
				<lg>
					<l>Toi, la mère des Preux, ma noble patrie !</l>
					<l>Sous le poids des revers, vas-tu tomber flétrie ?</l>
					<l>Vas-tu laisser ton sol souillé par l’étranger,</l>
					<l>Sans saisir l’insolent, le vaincre et te venger,</l>
					<l>Sans faire resplendir, sur le champ de bataille,</l>
					<l>L’honneur de ton drapeau, broyé par la mitraille ?</l>
				</lg>
				<lg>
					<l>Les barbares sont là sur tes champs dévastés ;</l>
					<l>Arme-toi de ta faulx, et, d’un élan superbe,</l>
					<l>Les courbant sous tes pieds, fauche-les comme l’herbe</l>
					<l>Tes bataillons surpris n’ont point été domptés :</l>
					<l>Dans les rangs ennemis, ces lions pleins d’audace</l>
					<l>Sont tombés en laissant une sanglante trace.</l>
				</lg>
				<lg>
					<l>Il n’est pas une ville, il n’est pas un hameau</l>
					<l>Qui de nos ennemis ne creuse le tombeau,</l>
					<l>La France provoquée a frémi de colère,</l>
					<l>Et je la vois déjà, dans sa fureur guerrière,</l>
					<l>Coucher dans les sillons de ses champs désolés,</l>
					<l>Ces ravageurs hideux, h sa rage immolés ;</l>
				</lg>
				<lg>
					<l>Faire partout la chasse à ces incendiaires</l>
					<l>Qui brûlent les châteaux, les villes, les chaumières,</l>
					<l>Et laissent sur leurs pas des larmes et du sang.</l>
					<l>O ! France ! étouffe-les dans un effort puissant ;</l>
					<l>Agrandis leurs tombeaux, jettes-y leurs cohortes,</l>
					<l>Et du pays sauvé ferme sur eux les portes !</l>
				</lg>
				<lg>
					<l>L’étranger qui te foule, à chacun de ses pas,</l>
					<l>Fait surgir des héros qui ne reculent pas,</l>
					<l>Qui vengent ton honneur, ô France bien-aimée !</l>
					<l>Dont le cœur héroïque, à son dernier soupir,</l>
					<l>En s’éteignant pour toi palpite de plaisir.</l>
					<l>N’es-tu pas des héros la terre renommée ?</l>
				</lg>
				<lg>
					<l>Qui pourrait t’asservir, mon vaillant pays ?</l>
					<l>Tous les bras sont armés pour venger ta querelle ;</l>
					<l>Quand tu cours des dangers, tous les cœurs sont unis,</l>
					<l>Non, tu ne mourras point, ô patrie immortelle !</l>
					<l>Depuis quinze cents ans aux siècles tu survis,</l>
					<l>Et ton Dieu, dans le Ciel, est le Dieu de Clovis.</l>
				</lg>
			</div>
			<div type="poem">
				<head type="main">DEUX INVASIONS COMPARÉES</head>
				<lg>
					<l>Lorsqu’Attila, jadis, vint fondre sur la Gaule,</l>
					<l>Qu’il traînait après lui le ravage et la mort ;</l>
					<l>Que, tressaillant d’horreur, la terre en était folle,</l>
					<l>Un guerrier de ce temps, Mérovée, au cœur fort,</l>
					<l>Rassembla de ses Francs la valeureuse armée,</l>
					<l>Et, marchant le premier, dédaigneux du trépas,</l>
					<l>Il écrasa les Huns des coups de sa framée</l>
					<l>Il vainquit l’ennemi, mais ne se rendit pas.</l>
				</lg>
				<lg>
					<l>Héritière des Huns et leur barbare émule,</l>
					<l>La Prusse vient chez nous, comme ces ravageurs,</l>
					<l>Nous apporter la guerre et toutes ses horreurs,</l>
					<l>Et, les réunissant, sur nous les accumule.</l>
					<l>Celui qui, malgré nous, nous valut ce fléau,</l>
					<l>A-t-il pu le dompter, ainsi que Mérovée,</l>
					<l>En refoulant des Huns le féroce troupeau,</l>
					<l>En montrant d’un héros la valeur éprouvée ?</l>
				</lg>
				<lg>
					<l>Dans ce champ de carnage où grondait le canon,</l>
					<l>A-t-il cherché la mort pour mériter son nom ?</l>
					<l>Comme à Waterloo, sur son armée en poudre,</l>
					<l>Autre Napoléon, a-t-il bravé la foudre ?</l>
					<l>Et tandis que le fer moissonnait nos guerriers,</l>
					<l>Est-il tombé comme eux, couché sur ses lauriers ?</l>
					<l>Sur son sein tout sanglant, et, dans sa main crispée,</l>
					<l>Comme un noble vaincu pressait-il son épée ?</l>
				</lg>
				<lg>
					<l>Qu’ai-je dit ? son épée !… à son heureux vainqueur,</l>
					<l>Intacte il l’a remise, en dépit de l’honneur.</l>
					<l>Que la honte à jamais à ce traître s’attache ;</l>
					<l>Sans férir un seul coup il s’est rendu, le lâche !</l>
					<l>Il a pu se sauver dans ce fleuve de sang</l>
					<l>Qui déborde aujourd’hui dans notre belle France.</l>
					<l>Dont naguère il était le maître tout-puissant.</l>
					<l>Et qui sur lui, du Ciel, appelle la vengeance.</l>
				</lg>
				<lg>
					<l>Un jour la trahison le mit sur le pavois ;</l>
					<l>Il monta sur le trône l’aide d’un parjure,</l>
					<l>Et depuis dix-huit ans, il nous faisait l’injure,</l>
					<l>Lui qui les méconnut, de nous dicter des lois.</l>
					<l>Le Ciel qui l’a brisé dans un jour de colère,</l>
					<l>A détruit pour toujours sa race tout entière.</l>
					<l>Que notre sol lui soit à jamais interdit ;</l>
					<l>Qu’il soit honni de tous, du monde entier maudit.</l>
				</lg>
				<lg>
					<l>Et quant à ce troupeau, ce vil amas d’esclaves</l>
					<l>Qui voudrait de ses fers nous mettre les entraves,</l>
					<l>Qu’il y renonce enfin, car la France est debout,</l>
					<l>Et son sein se soulève et de colère bout.</l>
					<l>La Prusse de son rang no la fera descendre :</l>
					<l>Semblable à cet oiseau qui sortait de sa cendre,</l>
					<l>Ainsi que Le Phénix la France renaitra,</l>
					<l>Et Dieu qui l’aime encor bientôt la vengera.</l>
				</lg>
			</div>
			<div type="poem">
				<head type="main">A NOS ENVAHISSEURS</head>
				<lg>
					<l>Vous êtes des brigands et non pas des soldats !</l>
					<l>Au sortir des forêts, vous marchez aux combats</l>
					<l>Ainsi, favorisés par l’épaisseur de l’ombre,</l>
					<l>Vous frappez à coups sûrs, en cachant votre nombre ;</l>
					<l>Et puis, sur les vieillards, les femmes, les enfants,</l>
					<l>Vous dirigez l’effort de vos bras triomphants.</l>
					<l>Vous ne respectez rien : dans leurs maisons sacrées,</l>
					<l>Les vierges, sous vos coups, tombent déshonorées.</l>
				</lg>
				<lg>
					<l>Pour signaler partout vos barbares exploits,</l>
					<l>Vous pillez, vous volez, vous tuez à la fois.</l>
					<l>Vous foulez à vos pieds toutes les lois divines ;</l>
					<l>Des temples les plus saints vous faites des ruines.</l>
					<l>C’est Dieu, disent vos chefs, qui combat avec nous :</l>
					<l>Ils mentent c’est l’enfer qui marche devant vous !</l>
					<l>Votre roi porte au Ciel des yeux pleins de tendresse,</l>
					<l>Et, tout en l’insultant, c’est à lui qu’il s’adresse.</l>
				</lg>
				<lg>
					<l>Vous, les dignes soldats d’un roi si bon croyant,</l>
					<l>Vous allez comme lui contre Dieu guerroyant.</l>
					<l>C’est ainsi que, mêlant l’ironie aux blasphèmes,</l>
					<l>Vous frappez sans pitié sur les blessés eux-mêmes :</l>
					<l>Que vos plus gros canons criblent de leurs boulets</l>
					<l>Même les hôpitaux, à l’égal des palais :</l>
					<l>Que, tenant dans les mains la torche incendiaire,</l>
					<l>Vous changez des vivants en ardente poussière.</l>
				</lg>
				<lg>
					<l>Ne vous a-t-on pas vus, aux flammes d’un brasier,</l>
					<l>Jeter les habitants d’un pays tout entier ?</l>
					<l>Barbares, c’est assez ! Rentrez dans vos repaires !</l>
					<l>Débarrassez le sol qu’ont illustré nos pères !</l>
					<l>Barbares, arrêtez le cours de vos forfaits !</l>
					<l>Nous sommes assez forts pour secouer le faix</l>
					<l>Des maux dont vous avez accablé notre France</l>
					<l>Et l’heure sonne enfin de notre délivrance.</l>
				</lg>
				<lg>
					<l>Contre vous rassemblés tous les Francs s’uniront ;</l>
					<l>Les guerriers, les vieillards, les enfants marcheront ;</l>
					<l>Pour venger leurs affronts, les filles et les femmes</l>
					<l>Crieront contre vous : Abattons ces infâmes !</l>
					<l>Et le pays, jetant ses vêtements de deuil,</l>
					<l>Sortira tout vivant du fond de son cercueil</l>
					<l>Le tocsin sonnera de village en village ;</l>
					<l>La foudre grondera comme en un jour d’orage ;</l>
				</lg>
				<lg>
					<l>De votre sang maudit le sol regorgera ;</l>
					<l>Poursuivi sans merci, pas un n’échappera ;</l>
					<l>Le Ciel, enfin, touché des cris de vos victimes,</l>
					<l>Vous anéantira sous le poids de vos crimes.</l>
					<l>Veuves et orphelins, allons ! séchez vos yeux,</l>
					<l>Car s’il n’est plus pour vous de jour qui soit heureux,</l>
					<l>Si vos cœurs désolés sont tout h la souffrance,</l>
					<l>Ah ! souriez du moins au jour de la vengeance !</l>
				</lg>
			</div>
			<div type="poem">
				<head type="main">LES NEUTRES</head>
				<lg>
					<l>Vous n’êtes d’aucun genre, ô nations vieillies,</l>
					<l>Et vous montrez ainsi votre virilité.</l>
					<l>Dans un lâche repos restez ensevelies ;</l>
					<l>Gardez-vous de sortir de la neutralité !</l>
				</lg>
				<lg>
					<l>Après tout, ce n’est rien, c’est la France éplorée</l>
					<l>Qui traine dans le sang sa robe déchirée,</l>
					<l>Qu’on voudrait immoler en la frappant au cœur.</l>
					<l>Et qui, seule, défend sa vie et son honneur.</l>
				</lg>
				<lg>
					<l>A l’abri des obus qui sifflent aux oreilles,</l>
					<l>Vous laissez bombarder la ville des merveilles,</l>
					<l>Sauf venir un jour, dans ce même Paris,</l>
					<l>Avec le monde entier, pleurer sur ses débris.</l>
				</lg>
				<lg>
					<l>Pourquoi vous émouvoir ? Ce sont de pauvres femmes,</l>
					<l>Des enfants, des vieillards, qu’on jette dans les flammes.</l>
					<l>Des pays dévastés, des toits pleins de douleurs,</l>
					<l>Des ruines, du sang, des familles en pleurs ;</l>
				</lg>
				<lg>
					<l>Des blessés achevés par d’affreuses tortures ;</l>
					<l>Des prisonniers sans pain ; de pauvres créatures</l>
					<l>Qui vont errant partout, et qui tendent la main</l>
					<l>Aux passants effarés, et tombent en chemin.</l>
				</lg>
				<lg>
					<l>O neutres impuissants ! de vos secours avares,</l>
					<l>Laissez-nous inonder par ces flots de barbares ;</l>
					<l>Mais s’ils venaient un jour se rejeter sur vous,</l>
					<l>Vous verrait-on encor tourner les yeux sur nous ?</l>
				</lg>
				<lg>
					<l>Supportez tout le poids de votre ingratitude,</l>
					<l>Et n’ayez de nos maux nulle sollicitude !</l>
					<l>La France est généreuse, et souvent ses soldats</l>
					<l>Sont tombés moissonnés, pour vous, dans les combats.</l>
				</lg>
				<lg>
					<l>Admirez aujourd’hui l’élan patriotique</l>
					<l>Qui la fait résister dans sa lutte héroïque !</l>
					<l>Oh ! non, malgré vos rois, elle ne mourra pas,</l>
					<l>Et vous la reverrez prendre partout le pas ;</l>
				</lg>
				<lg>
					<l>Se montrer vos yeux et plus grande et plus fière ;</l>
					<l>Dans nos jours ténébreux épandre sa lumière ;</l>
					<l>Vaincre la barbarie, et de la liberté</l>
					<l>Faire fleurir partout l’empire incontesté ;</l>
				</lg>
				<lg>
					<l>Et secouant enfin une étreinte fatale,</l>
					<l>Substituer le droit la force brutale.</l>
					<l>Qu’importe qu’elle soit envahie h moitié ?</l>
					<l>Elle n’exige rien, pas même la pitié.</l>
				</lg>
				<lg>
					<l>Va donc, roi déloyal, va donc trôner Rome,</l>
					<l>De la vertu des rois si tu n’as plus l’arôme !</l>
					<l>Hâte-toi d’arracher le diadème saint</l>
					<l>Du pontife sacré qui, de par Dieu, l’a ceint</l>
				</lg>
				<lg>
					<l>Souviens-toi cependant que la fortune vole,</l>
					<l>Que le roc Tarpéïen est près du Capitole,</l>
					<l>Et quo tes devanciers de ce haut piédestal,</l>
					<l>Sont tombés foudroyés, dans un moment fatal.</l>
				</lg>
				<lg>
					<l>Allons ! roi galant-homme, attache à ta couronne</l>
					<l>La tiare…, et ce poids entraînera ton trône.</l>
					<l>Et toi, vieille Angleterre, au lion édenté,</l>
					<l>De tes vaisseaux brûlés touche l’indemnité !</l>
				</lg>
				<lg>
					<l>Et toi, pays du Cid, n’ai-je rien à te dire ?</l>
					<l>Incline-toi bien bas devant ton nouveau sire,</l>
					<l>Ce roi que t’a légué ce vaillant hidalgo ;</l>
					<l>Ce Prim qui, châtié, s’écriait : Castigo<ref target="1" type="noteAnchor">1</ref>.</l>
				</lg>
				<lg>
					<l>Peuples dégénérés que la crainte rassemble,</l>
					<l>Aux genoux de Guillaume allez tomber ensemble !</l>
					<l>Et vous, qui nous devez et du sang et de l’or,</l>
					<l>Gardez le tout ensemble, et nos mépris encor.</l>
				</lg>
				<closer>
					<note type="footnote" id="1">(1) Castigo, châtiment en langue espagnole.</note>
				</closer>
			</div>
			<div type="poem">
				<head type="main">A LA FRANCE</head>
				<lg>
					<l>Quel sort sera le tien, ô France bien-aimée !</l>
					<l>Vas-tu servir de proie la horde affamée</l>
					<l>Qui déchire ton sein, s’abreuve de ton sang,</l>
					<l>Et laisse sur ton sol sa souillure, en passant ?</l>
					<l>Ou de Charles-Martel, reprenant la massue,</l>
					<l>Vas-tu broyer encor, comme au jour de Poitiers,</l>
					<l>Une race maudite et dans l’enfer conçue,</l>
					<l>Et convertir ainsi tes cyprès en lauriers ?</l>
				</lg>
				<lg>
					<l>Quoi ! tu ne réponds rien, ô ma chère mourante,</l>
					<l>Et la terre h tes maux demeure indifférente !</l>
					<l>Hélas ! autour de toi quel silence effrayant :</l>
					<l>Tout tremble, tout se tait et l’abîme est béant !</l>
					<l>Pourtant tu n’es point morte, ô France que j’adore !</l>
					<l>Ton cœur, quoique saignant, ton cœur palpite encore.</l>
					<l>Nul peuple, nul héros ne vient te secourir,</l>
					<l>Et, seule, sans secours te faudra-t-il mourir ?</l>
				</lg>
				<lg>
					<l>Ton barbare ennemi, contre toi plein de rage,</l>
					<l>Te foule sous ses pieds, te dépouille et t’outrage :</l>
					<l>Il a juré ta mort, le poignard sur ton sein,</l>
					<l>Et de roi qu’il était il s’est fait assassin !</l>
					<l>Qui donc viendra panser tes blessures profondes ?</l>
					<l>J’interroge le Ciel, et la terre, et les ondes ;</l>
					<l>A défaut des vivants pour chasser ton bourreau,</l>
					<l>Si j’évoquais soudain les morts de leur tombeau !…</l>
				</lg>
				<lg>
					<l>Vous, guerriers d’autrefois, sortez donc de la tombe,</l>
					<l>Et ne permettez pas que la France succombe !</l>
					<l>Vous, Bayard, Duguesclin, et Lahire, et Dunois,</l>
					<l>Armez-vous du drapeau que portaient nos vieux rois ;</l>
					<l>Jeanne d’Arc et Clisson, accourez aux batailles .</l>
					<l>Et chassez l’ennemi debout sur nos murailles ;</l>
					<l>Vous, Turenne et Condé, Catinat et Villars</l>
					<l>Faites fuir sous vos coups ces bandes de pillards.</l>
				</lg>
				<lg>
					<l>Point de Pape Léon, de Geneviève sainte</l>
					<l>Qui de nos murs sacrés sauvegarde l’enceinte,</l>
					<l>Qui fasse reculer ce moderne Attila</l>
					<l>Que Dieu, pour nous punir, de nos jours appela.</l>
					<l>De notre capitale il franchit la barrière,</l>
					<l>Et point de bras vengeur qui le tienne en arrière,</l>
					<l>Qui détourne de nous ce barbare oppresseur</l>
					<l>Qui traîne sur ses pas le carnage et l’horreur !</l>
				</lg>
				<lg>
					<l>Vous, les vaillants, les forts, vous qui vivez encore,</l>
					<l>Vous que la gloire appelle et que l’honneur décore ;</l>
					<l>Et vous, fiers Vendéens, vous, les fils des Croisés,</l>
					<l>Que déjà cent combats ont immortalisés,</l>
					<l>Laisserez-vous ainsi notre vieille patrie</l>
					<l>Pencher vers sa ruine, et s’abimer flétrie,</l>
					<l>Et ne ferez-vous point à ce noble pays</l>
					<l>Quelque nouveau rempart couvert de vos débris ?</l>
				</lg>
				<lg>
					<l>Mais vous, mon Dieu, mais vous, laisserez-vous la France</l>
					<l>S’affaisser sous le poids de sa rude souffrance ?</l>
					<l>N’a-t-elle point encore épuisé devant vous,</l>
					<l>Tout son amer calice, et de votre courroux</l>
					<l>Doit-elle ressentir les suites effroyables</l>
					<l>Sans toucher votre cœur ? Ses accents lamentables</l>
					<l>Sont montés jusqu’au Ciel, ne la repoussez pas,</l>
					<l>Ouvrez-lui votre sein et tendez-lui les bras !</l>
				</lg>
				<lg>
					<l>Pitié, mon Dieu, pitié ! Seigneur, pardon pour elle !</l>
					<l>Rendez-lui, désormais, une vie immortelle.</l>
					<l>Venez à son secours, vous, maître tout-puissant !</l>
					<l>Elle s’est épurée des torrents de sang,</l>
					<l>Et parmi les combats subissant leur martyre,</l>
					<l>Ses héros invoquaient, dans leur pieux délire,</l>
					<l>Le nom de votre Fils ; de leur mourante voix,</l>
					<l>Ils priaient comme lui, tout en baisant sa croix.</l>
				</lg>
				<lg>
					<l>Et je priais ainsi, lorsque du sein des ombres</l>
					<l>Qui couvraient mon pays, couché sur ses décombres ;</l>
					<l>Le soleil s’élança, brillant et radieux,</l>
					<l>Et j’aperçus là-haut, dans la splendeur des cieux,</l>
					<l>Le Christ qui demandait notre grâce à son Père ;</l>
					<l>Et la paix aussitôt, divine messagère,</l>
					<l>Apparut à nos yeux, ravis de ce bonheur,</l>
					<l>Conduisant par la main notre unique Sauveur.</l>
				</lg>
				<lg>
					<l>La France, sous ses lois, et forte, et rajeunie,</l>
					<l>Reprenait, grâce lui, sa force et son génie ;</l>
					<l>Reniant du passé les fatales erreurs,</l>
					<l>Elle ne livrait plus sa vie aux empereurs,</l>
					<l>A ces hideux Césars qui, montés sur le trône,</l>
					<l>Sur leur front sans pudeur attachaient la couronne,</l>
					<l>Dans la boue et le sang se vautraient à plaisir,</l>
					<l>Et, nous faisant tuer, se gardaient de mourir.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>