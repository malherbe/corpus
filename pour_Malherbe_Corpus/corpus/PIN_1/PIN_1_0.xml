<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">VOIX DES SILENCIEUX A LA PATRIE</title>
				<title type="sub_2"/>
				<title type="medium">Édition électronique</title>
				<author key="PIN">
					<name>
						<forename>Albert</forename>
						<surname>PINARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PIN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>VOIX DES SILENCIEUX A LA PATRIE</title>
						<author>ALBERT PINARD</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem">
				<head type="main">VOIX DES SILENCIEUX A LA PATRIE</head>
				<head type="sub_2">
					Strophes dites par Mme Marie Laurent au Théâtre de la Porte Saint-Martin<lb/>
					Au citoyen Dorian Ministre de la République. An 1870.
				</head>
				<div type="section">
					<head type="number">I</head>
					<lg>
						<l>Nous n’avons pas été les clairons de tes fêtes</l>
						<l>Quand, avant le combat, ton peuple, ivre d’orgueuil,</l>
						<l>Chantait ; et quand le poids des immenses défaites,</l>
						<l>France, en courbant la tienne, est tombé sur nos têtes,</l>
						<l>Nous n’avons pas été les chantres de ton deuil.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">II</head>
					<lg>
						<l>Quelle sourde douleur pour une âme française</l>
						<l>Quand on a déclaré la guerre ceux du Rhin,</l>
						<l>Et que tant de Français, « le cœur léger, » pleins d’aise,</l>
						<l>Sur les grands boulevards criaient la Marseillaise,</l>
						<l>Et hurlaient en riant « A Berlin ! à Berlin ! »</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">III</head>
					<lg>
						<l>Car nous ne comprenions ni leur joie imprudente,</l>
						<l>Ni leur fauve désir de voir le sang couler ;</l>
						<l>Et nous restions pensifs et sombres, comme Dante,</l>
						<l>Voyant dans l’avenir cette autre, triomphante,</l>
						<l>Faire sous ses pas lourds ton sol pur s’écrouler !</l>
					</lg>
				</div>
				<div type="section">
				<head type="number">IV</head>
					<lg>
						<l>Avec l’empereur lâche et ses généraux louches,</l>
						<l>Les bataillons trahis et les drapeaux vendus,</l>
						<l>Les bons canons béants sans pâture leurs bouches,</l>
						<l>Et les soldats sans chefs, sons pain et sans cartouches,</l>
						<l>Ce jour vint. — [il manque un mot] bruyants se jugèrent perdus.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">V</head>
					<lg>
						<l>— Deux Décembre chassé, sa meute était en fuite.</l>
						<l>— Lors, pour ne signer pas ta honte, de son nom,</l>
						<l>Quand il vit, sous ses murs, Guillaume — avec la suite.</l>
						<l>Paris grossit sa voix de la voix du canon,</l>
						<l>Et son premier obus à la Prusse dit « Non ! »</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">VI</head>
					<lg>
						<l>Non, nous ne voulons pas la paix qui déshonore ;</l>
						<l>Non, non, nous avons trop du repos acheté</l>
						<l>Au prix du servilisme et de la lâcheté</l>
						<l>O vainqueur insolent, regarde cette aurore,</l>
						<l>Vois, c’est la République avec la Liberté !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">VII</head>
					<lg>
						<l>La morte a soulevé la pierre de sa tombe,</l>
						<l>Et la morte aujourd’hui revient pour secourir</l>
						<l>Ceux qui confusément, sans elle, allaient mourir !</l>
						<l>O Prusse, elle est la foudre ; ô Prusse, elle est la trombe,</l>
						<l>Et dans leur bain de sang tes aigles vont pourrir !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">VIII</head>
					<lg>
						<l>Nous vaincrons. — L’échafaud que ta folie élève</l>
						<l>Pour y faire monter les fortes nations,</l>
						<l>O roi, s’écroulera comme la tour d’un rêve,</l>
						<l>Quand nous ferons bouillir, chez ton peuple, la sève</l>
						<l>Brûlant le tronc séché des dominations.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">IX</head>
					<lg>
						<l>L’échafaud. — Ah ! le sort est sans miséricorde !</l>
						<l>Quand le peuple a couvert un roi de son haro,</l>
						<l>C’est le linceul de chaux après le sac de corde,</l>
						<l>En l’an premier, Capet, place… de la Concorde ;</l>
						<l>Sois pensif, roi Guillaume ; — hier, Queretaro !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">X</head>
					<lg>
						<l>Nous vaincrons, et, vainqueurs : « C’est assez de souffrance,</l>
						<l>« Nos frères, c’est assez d’hommes sacrifiés,</l>
						<l>« De jougs, d’affronts subis, voici la délivrance,</l>
						<l>« Voici la liberté marchant avec la France :</l>
						<l>« Plus de rois ! Tyrannie et tyrans, à leurs piés !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XI</head>
					<lg>
						<l>Nous vaincrons. — C’est alors, ô grandeurs infléchies,</l>
						<l>Qu’on verra, sous les feux de son large flambeau,</l>
						<l>Comme une République, en sortant du tombeau,</l>
						<l>Petit faire son réveil crouler deux monarchies.</l>
						<l>Et plusieurs descendront du trône à l’escabeau.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XII</head>
					<lg>
						<l>Nous vaincrons, et tous, tous, parole, enseigne, armées,</l>
						<l>Diplomates, drapeaux, canons de bronze épais,</l>
						<l>Porteront par le monde avec les Renommées,</l>
						<l>O République, ô France, ô vous nos bien-aimées</l>
						<l>Vos noms, dans le contrat d’une éternelle paix !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XIII</head>
					<lg>
						<l>Ah ! c’est que nous t’aimons, France, avec furie,</l>
						<l>Depuis qu’on reconnait en toi le grand lion ;</l>
						<l>Et qu’on a sur leur coup, leur trône et leur curie,</l>
						<l>Fait passer, tel Hercule en l’antique écurie,</l>
						<l>Le fleuve Révolution !</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XIV</head>
					<lg>
						<l>C’est, que nous souffrions, more, de voir leur nombre</l>
						<l>Conspirer leur richesse et ton abaissement ;</l>
						<l>Acharnés, comme aux flancs d’un grand vaisseau qui sombre,</l>
						<l>Des tarets, sans répit, creusent leurs trous dans l’ombre,</l>
						<l>Pour avancer le jour de l’engloutissement.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XV</head>
					<lg>
						<l>C’est que l’orgueil du lâche et la gloire du crime</l>
						<l>Soulevaient notre cœur pour nous comme pour toi.</l>
						<l>Les fiertés se courbaient sous la terreur intime,</l>
						<l>L’honneur et l’équité descendaient de leur cime ;</l>
						<l>Bonaparte étant prince, et le vice étant roi.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XVI</head>
					<lg>
						<l>Les peuples nous trouvaient indignes de nos pères,</l>
						<l>Chacals fils de lions, et nains fils de Titans.</l>
						<l>Sur nos lâches torpeurs nous fermions nos paupières,</l>
						<l>O France, et nous avions moins de cœur que tes pierres :</l>
						<l>Français insoucieux de sortir de tes flancs.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XVII</head>
					<lg>
						<l>C’est qu’ils t’aiment surtout, ces fils, de l’amour forte,</l>
						<l>Du juge, sans pitié, muet et solennel,</l>
						<l>Et qu’ils voudraient plutôt te voir morte, oui, toi, morte,</l>
						<l>Que de voir le Prussien, ayant forcé ta porte,</l>
						<l>Étaler ses carcans sur ton sein maternel.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XVIII</head>
					<lg>
						<l>Mais ces fils, à l’horreur de la tombe qu’on creuse</l>
						<l>Pour toi, qui t’es refait une virginité,</l>
						<l>Reprenant dans leur deuil un courage irrité,</l>
						<l>T’aiment bien mieux sanglante et t’aiment douloureuse,</l>
						<l>Dans ta tristesse grave et ta virilité.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XIX</head>
					<lg>
						<l>Quand ils ont mis le doigt dans tes larges blessures,</l>
						<l>Sans implorer le ciel et pousser de grands cris,</l>
						<l>Mère, ils ont tous juré de venger tes injures,</l>
						<l>En faisant de leurs corps saignants comme des mûres</l>
						<l>Un chemin triomphal tes pieds blancs meurtris.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XX</head>
					<lg>
						<l>Ils ont fortifié leur âme dans l’épreuve.</l>
						<l>Ils veulent, pour leur mort, qu’en un jour de courroux,</l>
						<l>(De leur farouche amour impérissable preuve)</l>
						<l>Ton coq républicain, comme un vautour, s’abreuve</l>
						<l>Du sang et vermeil des Germains au poil roux.</l>
					</lg>
				</div>
				<div type="section">
					<head type="number">XXI</head>
					<lg>
						<l>Eux morts, si des vivants quelqu’un demandait compte</l>
						<l>Du silence où restaient ces héros soucieux,</l>
						<l>Montrant leurs corps raidis, la face vers les cieux,</l>
						<l>Réponds, France, réponds à cet homme-là : « Compte,</l>
						<l>« Vois comme ils m’ont prouvé leur amour, et fais mieux. »</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1870">30 novembre 1870</date>
					</dateline>
				</closer>
			</div>
		</body>
	</text>
</TEI>