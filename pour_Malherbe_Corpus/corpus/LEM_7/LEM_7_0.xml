<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PETITS POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="LEM">
					<name>
						<forename>Pamphile</forename>
						<surname>LE MAY</surname>
					</name>
					<date from="1837" to="1918">1837-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LEM_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PETITS POÈMES</title>
						<author>LÉON PAMPHILE LEMAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>books.google.fr</publisher>
						<idno type="URL">https://books.google.bj/books?id=dSYOAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PETITS POÈMES</title>
								<author>L. PAMPHILE LEMAY</author>
								<imprint>
									<pubPlace>QUÉBEC</pubPlace>
									<publisher>TYPOGRAPHIE DE C. DARVEAU</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne retient que 2 poèmes : "Le bien pour le mal" et "L’Hiver". Les autres sont publiés dans d’autres éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) et les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="LEM274">
				<head type="main">LE BIEN POUR LE MAL</head>
				<div type="section" n="1">
					<lg n="1">
						<l>Il est des droits sacrés qu’il faut savoir défendre,</l>
						<l>De grands devoirs qu’il faut accomplir. Pour comprendre</l>
						<l>Ce que le ciel commande et ce que l’homme fait,</l>
						<l>Il faut du premier homme admettre le forfait</l>
						<l>Et du Christ incarné le sanglant sacrifice.</l>
						<l>On proclame bien haut l’amour de la justice,</l>
						<l>Mais on oublie, hélas ! de graver en son cœur</l>
						<l>Ce que la bouche loue avec tant de chaleur.</l>
						<l>Dieu ramène pourtant chaque chose à sa gloire :</l>
						<l>C’est ce que je dirai dans une courte histoire.</l>
					</lg>
					<lg n="2">
						<l>Jean Dumas habitait, non loin de la cité,</l>
						<l>Une blanche maison sous les bois. En été,</l>
						<l>Les oiseaux voltigeaient sur les branches des hêtres</l>
						<l>Et venaient, le matin, jusque dans les fenêtres</l>
						<l>Pour chanter au réveil leurs joyeuses chansons ;</l>
						<l>En hiver, le fléau battait dru les moissons,</l>
						<l>Et puis l’on festoyait comme c’est la coutume.</l>
					</lg>
					<lg n="3">
						<l>Or, les coups répétés du marteau sur l’enclume</l>
						<l>Disaient que près de là vivait un forgeron.</l>
						<l>Il se nommait, je crois, Cyprien Bergeron.</l>
						<l>Si je l’appelle ainsi ce n’est pas pour la rime.</l>
						<l>Les deux voisins heureux se montraient de l’estime,</l>
						<l>Mais ils ne marchaient <choice hand="RR" type="false verse" reason="analysis"><sic> </sic><corr source="none">pas</corr></choice> sous la même couleur ;</l>
						<l>L’un était libéral, l’autre, conservateur.</l>
					</lg>
					<lg n="4">
						<l>Ils eurent à la fin une ardente dispute</l>
						<l>Au sujet des héros qui commençaient la lutte,</l>
						<l>Pour un siége d’un jour, dans notre Parlement.</l>
						<l part="I">Jean dit à Bergeron : </l>
						<l part="F">-Tu parles sottement ;</l>
						<l>Ton candidat est fourbe et ta cause est mauvaise.</l>
					</lg>
					<lg n="5">
						<l>Et l’autre répliqua, bondissant sur sa chaise :</l>
					</lg>
					<lg n="6">
						<l>— Ma cause est bonne et mon homme vaut mieux que vous !</l>
					</lg>
					<lg n="7">
						<l>— Tiens ! si je le voulais tu serais avec nous.</l>
						<l part="I">-Comment ? </l>
						<l part="M">-Tu n’est pas libre. </l>
						<l part="F">-Est-ce quelque menace ?</l>
					</lg>
					<lg n="8">
						<l>— Je puis, si je le veux, te chasser de la place.</l>
					</lg>
					<lg n="9">
						<l part="I">-Me chasser ? </l>
						<l part="M">-Te chasser ! </l>
						<l part="F">-Tu ne le feras pas !</l>
						<l>Je le ferai, bien sûr, si, demain, tu ne vas</l>
					</lg>
					<lg n="10">
						<l>Pour l’homme de mon choix enregistrer ton vote.</l>
					</lg>
					<lg n="11">
						<l part="I">-Jamais ! </l>
						<l part="M">-Tu me dois ? </l>
						<l part="M">-Oui. </l>
						<l part="M">-Tu me paieras. </l>
						<l part="F">-Despote !</l>
						<l>— Un grand mot que j’ai lu dans ton petit journal.</l>
						<l>Je ne te ferai pas, moi, de discours banal,</l>
						<l>Mais je te chasserai de ta pauvre boutique !</l>
						<l>— Bah ! j’aurai pour abri mon drapeau politique.</l>
					</lg>
					<lg n="12">
						<l>La querelle dura longtemps et fit du bruit.</l>
						<l>Dumas ne dormit point, rêvant, toute la nuit,</l>
						<l>Aux moyens d’exercer le plus tôt sa vengeance.</l>
						<l>Il fit vendre la forge et rit de l’indigence</l>
						<l>Où tomba tout à coup son malheureux voisin,</l>
						<l>Puis ensuite il noya ses remords dans le vin.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l>Trente ans sont écoulés. Dans les vertes prairies</l>
						<l>Qui s’étendent au Nord, comme des mers fleuries,</l>
						<l>Au bord du lac Saint-Jean, derrière nos grands monts,</l>
						<l>Il s’élève un village où nombre de maisons,</l>
						<l>Pleines de frais enfants, grouillent comme des ruches.</l>
						<l>Dans l’âtre, aux jours de froid, flambent gaîment les bûches</l>
						<l>Lorsque le vent se tait et que les cieux sont clairs,</l>
						<l>On voit de tout côté s’élever dans les airs</l>
						<l>Les colonnes d’argent de la molle fumée.</l>
						<l>Le givre émaille alors la fenêtre fermée.</l>
						<l>Plus tard, la porte s’ouvre et le joyeux soleil</l>
						<l>Jusqu’au cour du foyer plonge un reflet vermeil,</l>
						<l>Et les bois tout en fleurs y versent leurs dictames,</l>
						<l>Doux comme les vertus de ses naïves âmes.</l>
					</lg>
					<lg n="2">
						<l>Dans l’une des maisons, en face du châssis</l>
						<l>Qui donnait sur la route, un homme était assis,</l>
						<l>Un vieillard. Il avait la chevelure blanche,</l>
						<l>Le dos courbé, l’air doux et la figure franche.</l>
						<l>Il fumait en silence, et sou regard rêveur</l>
						<l>Suivait, au bord du lac, une étrange vapeur</l>
						<l>Que le vent déployait comme un voile de soie.</l>
						<l>La maison de cet homme était pleine de joie :</l>
						<l>Le bonheur l’inondait de ses divins rayons.</l>
						<l>On voyait à l’entour onduler les sillons ;</l>
						<l>Les vergers lui donnaient des fruits tout pleins d’arôme,</l>
						<l>Et les pins toujours verts la couvraient de leur dôme.</l>
						<l>Elle était comme un nid enfoui sous les fleurs :</l>
						<l>Le rire éclatait là, là s’essuyaient les pleurs.</l>
					</lg>
					<lg n="3">
						<l>En face s’élevait une forge ; et sans cesse</l>
						<l>Sous l’enclume de fer qui tintait d’allégresse</l>
						<l>On entendait tomber l’implacable marteau.</l>
						<l>Le soufflet, haletant sous son large manteau,</l>
						<l>Attisait le foyer. Se brisant en parcelles,</l>
						<l>Le for rouge battu lançait mille étincelles</l>
						<l>Autour de l’ouvrier content de son labeur.</l>
						<l>Bien souvent le vieillard encor plein de vigueur</l>
						<l>Venait à l’atelier pour reprendre sa tâche.</l>
						<l>Il n’aurait pas voulu s’affaisser comme un lâche,</l>
						<l>Au coin de son foyer, sous le fardeau des ans,</l>
						<l>Comme font de nos jours tant de vieux artisans.</l>
						<l>Mais sou fils, toutefois, le plus souvent peut- être,</l>
						<l>Faisait seul la besogne, et la faisait en maître.</l>
						<l>Il rentrait à son tour les bras noirs de charbon,</l>
						<l>Mais qu’importe ? il avait travaillé, c’était bon</l>
					</lg>
					<lg n="4">
						<l>Pendant que le vieillard fumait, souriant d’aise,</l>
						<l>Assis moelleusement dans une grande chaise,</l>
						<l>Et que Paul, son garçon, était à l’atelier,</l>
						<l>La mère, alerte encor, surveillait le cellier</l>
						<l>Et les filles, chantant quelques chansons nouvelles,</l>
						<l>Cousaient le linge blanc ou nouaient des dentelles.</l>
						<l>Le temps que Dieu donnait ou savait l’employer.</l>
						<l>Un Christ, les bras tendus, protégeait le foyer.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l>Non loin, sur le chemin bordé de bois d’érable,</l>
						<l>Tiré par un cheval poussif et misérable,</l>
						<l>Venait un chariot. Il était encombré :</l>
						<l>Des lits, des bancs, des sacs ! Tout cela délabré,</l>
						<l>Tout cela revêtu de cet air de détresse</l>
						<l>Qui choque le regard et même vous oppresse.</l>
						<l>Ce pénible attelage était, hélas ! guidé</l>
						<l>Par un homme bien vieux. Son front chauve et ridé,</l>
						<l>Penché presque toujours sur la route de sable,</l>
						<l>Gardait d’un long chagrin la trace impérissable.</l>
						<l>Et les essieux criaient, et leurs cris agaçants</l>
						<l>Faisaient, par-ci par-là, sourire les passants.</l>
						<l>Derrière la voiture, un bœuf qui se lamente,</l>
						<l>Un chien la tête basse et que la soif tourmente</l>
						<l>Et deux femmes. La fille, une jeunesse encor,</l>
						<l>Blonde avec un cil tendre, avec des cheveux d’or,</l>
						<l>Belle malgré ses pleurs et sa pâleur extrême ;</l>
						<l>La mère, bien âgée et s’oubliant soi-même</l>
						<l>Pour ne songer toujours qu’à ceux qu’elle chérit.</l>
						<l>Et toutes deux s’en vont songeant dans leur esprit</l>
						<l>Aux beaux jours d’autrefois qui sont passés si vite.</l>
						<l>On dirait que, honteux, le vieillard les évite ;</l>
						<l>Et lorsqu’ils sont ensemble aux heures de repos</l>
						<l>Rarement il se mêle à leurs tristes propos.</l>
					</lg>
					<lg n="2">
						<l>Tout à coup cependant le chariot se brise :</l>
						<l>Une ornière, un caillou, l’on ne sait. La surprise</l>
						<l>Pour les trois voyageurs est grande assurément.</l>
						<l>On regarde, on soupire, on demande comment</l>
						<l>On pourra supporter une pareille épreuve.</l>
						<l>La voiture, c’est vrai, n’était pas toute neuve,</l>
						<l>Mais enfin l’on s’était bien rendu jusqu’ici,</l>
						<l>Pourquoi ne pas aller un peu plus loin aussi ?</l>
						<l>Le forgeron, toujours à sa fenêtre ouverte,</l>
						<l>Regardant le lac bleu dans sa ceinture verte,</l>
						<l>Regardant chaumes, vals et prés d’un mil distrait,</l>
						<l>Aperçut la voiture au moment qu’elle entrait</l>
						<l>Dans le petit village avec sa charge lourde ;</l>
						<l>Il entendit aussi, je crois, la plainte sourde</l>
						<l>Des essieux mal ferrés qui se rompaient soudain</l>
					</lg>
					<lg n="3">
						<l>— Paul, cria-t-il, allons donner un coup de main</l>
						<l>A des colons nouveaux qu’un accident, sans doute,</l>
						<l>Vient d’arrêter là-bas, au milieu de la route."</l>
					</lg>
					<lg n="4">
						<l>Vous le savez déjà, Paul c’était son garçon ;</l>
						<l>Il forgeait en chantant comme un joyeux pinson.</l>
						<l>Il sort, et tous les deux, le fils avec le père</l>
						<l>Vont aider le vieillard qui pleure et désespère.</l>
						<l>On porte à la maison le pauvre mobilier ;</l>
						<l>Le chariot boiteux se traîne à l’atelier,</l>
						<l>Et les deux forgerons se mettent à l’ouvrage.</l>
						<l>Faire la charité leur donne du courage.</l>
						<l>Le soufflet bourdonnant allume un feu d’enfer</l>
						<l>Et les pesants marteaux tombent dru sur le fer</l>
					</lg>
					<lg n="5">
						<l>Quand le travail fut fait il était soir. La grive</l>
						<l>Éparpillait déjà sur la paisible rive,</l>
						<l>Comme des diamants, les notes de sa voix.</l>
						<l>L’ombre s’épaississait sous le dôme des bois.</l>
						<l>L’hôte du forgeron, malgré l’heure avancée,</l>
						<l>Voulut poursuivre alors sa route commencée.</l>
					</lg>
					<lg n="6">
						<l>— Je vais partir, dit- il, mais il faudrait d’abord</l>
						<l part="I">Payer ce que je dois. </l>
					</lg>
					<lg n="7">
						<l part="F">-Pour qu’on reste d’accord</l>
						<l>No m’offrez rien du tout, non ! pas la moindre somme,</l>
						<l>Passez ici la nuit et dormez un bon somme,</l>
						<l>Reprit le forgerou avec un franc souris.</l>
					</lg>
					<lg n="8">
						<l>Les jeunes gens se sont toujours vite compris.</l>
						<l>Un tendre sentiment, une amitié sincère</l>
						<l>Entre Paul et ses sœurs et la jeune étrangère</l>
						<l>Naquit à l’instant même. On descendit gaiement,</l>
						<l>Par un sentier de fleurs, au bord du lac dormant,</l>
						<l>Et, sur un tronc moussu, les pieds tout près de l’onde,</l>
						<l>On alla s’asseoir. Paul, près de la fille blonde</l>
						<l>Se trouva, par hasard ou volontairement.</l>
						<l>Il était tout heureux, parlait joyeusement</l>
						<l>Et regardait beaucoup sa compagne jolie.</l>
					</lg>
					<lg n="9">
						<l>Cependant je ne sais quelle mélancolie</l>
						<l>S’envint clore sa lèvre et noyer son regard.</l>
						<l>Parti d’un mil d’azur, un rayon, comme un dard,</l>
						<l>L’avait touché soudain ; un doux rayon de flamme</l>
						<l>Soudain avait glissé jusqu’au fond de son âme.</l>
					</lg>
					<lg n="10">
						<l>— Jamais, se disait- il, jamais le vent du soir</l>
						<l>Ne s’est levé si pur ! C’est comme un encensoir</l>
						<l>Qui balance dans l’air les parfums de l’aurore.</l>
						<l>Jamais les flots du lac ne sont venus encore</l>
						<l>Murmurer à nos pieds des soupirs si touchants !</l>
						<l>Et jamais les oiseaux n’ont fait de si doux chants !</l>
					</lg>
					<lg n="11">
						<l>C’est son cour qui chantait. Et tout est harmonie,</l>
						<l>Le ciel est près de nous et la terre est bénie</l>
						<l>Lorsque chante le cœur et s’éveille l’amour.</l>
					</lg>
					<lg n="12">
						<l>Il fallut cependant qu’on songeat au retour,</l>
						<l>Car la nuit s’avançait avec son voile d’ombres,</l>
						<l>Et les arbres mêlés formaient des masses sombres</l>
						<l>Où l’on ne distinguait ni feuilles, ni rameaux.</l>
						<l>On fit de longs adieux au lac. Ses fraîches eaux</l>
						<l>Portèrent jusqu’au loin les charmantes paroles,</l>
						<l>Et l’on n’entendit plus, sur les fougères molles,</l>
						<l>Que les pas mesurés des jeunes promeneurs.</l>
						<l>Paul ne marchait pas vite et de nouveaux bonheurs</l>
						<l>Ce soir-là, croyez-le, rayonnaient sur sa vie.</l>
						<l>Sa compagne semblait aussi toute ravie.</l>
						<l>Ils n’avaient pas marché la moitié du chemin</l>
						<l>Qu’ils se parlaient tout bas et la main dans la main.</l>
					</lg>
					<lg n="13">
						<l>Cependant les vieillards assis devant la porte,</l>
						<l>Aspirant cet air pur que le soir nous apporte</l>
						<l>Quand on est dans les champs, sous les bois, près des flots,</l>
						<l>Causaient en attendant le moment du repos.</l>
					</lg>
					<lg n="14">
						<l>— Pour aller, pauvre ami, défricher une terre</l>
						<l>Vous êtes bien trop vieux, je ne saurais le taire,</l>
						<l>Disait le forgeron au colon étranger</l>
						<l>— Je le sais bien, hélas ! nais n’y puis rien changer !</l>
						<l>Je ne demande pas, soyez sûr, l’abondance,</l>
						<l>Mais le pain qu’au travail donne la Providence.</l>
						<l>J’ai connu de beaux jours et je les ai perdus.</l>
						<l>Je possédais des biens ; ils ont été vendus.</l>
						<l>Mes fils se sont enfuis — à vous je le raconte –</l>
						<l>Mes fils ont déserté quand ils ont vu ma honte,</l>
						<l>Quand ils ont vu la faim s’asseoir à notre seuil.</l>
						<l>Où sont-ils maintenant ? où leur coupable orgueil</l>
						<l>Les a- t-il entraînés ? Je ne saurais le dire.</l>
						<l>Je n’ai pas cependant le droit de les maudire</l>
						<l>Parce que je fus lâche et que Dieu me punit.</l>
					</lg>
					<lg n="15">
						<l>Et ce fut en pleurant que le vieillard finit.</l>
					</lg>
					<lg n="16">
						<l>— Quelle était, demanda l’hôte, votre paroisse ?</l>
						<l part="I">Et quel est votre nom ? </l>
					</lg>
					<lg n="17">
						<l part="F">Oppressé par l’angoisse,</l>
						<l>Le malheureux pouvait à peine se tenir.</l>
						<l>Sa femme qu’attristait aussi ce souvenir</l>
						<l>Répondit aussitôt, essuyant sa paupière :</l>
					</lg>
					<lg n="18">
						<l>— Nous avons demeuré bien longtemps à <subst hand="RR" type="phonemization" reason="analysis"><del>St</del><add rend="hidden">Saint</add></subst>-Pierre,</l>
						<l part="I">Saint Pierre d’Orléans. </l>
						<l part="F">Et, parlant presque bas,</l>
						<l part="I">L’homme reprit alors : </l>
						<l part="F">— Mon nom est Jean Dumas.</l>
					</lg>
					<lg n="19">
						<l>— Jean Dumas, dites-vous ? Quoi ! Jean Dumas, de l’Ile ?</l>
						<l>Cria le forgeron : Non ! non ! c’est inutile !</l>
						<l>Tu n’es point Jean Dumas ! je te reconnaîtrais !…</l>
						<l>Approche donc un peu que je lise tes traits !…</l>
						<l>Ah ! sous nos cheveux blancs et sous nos peaux tannées</l>
						<l>On ne retrouve plus nos jeunesses fanées !</l>
					</lg>
					<lg n="20">
						<l>— Quoi ! vous me connaissez ! quoi ! vous m’avez connu !</l>
						<l>Lorsque j’étais heureux êtes-vous donc venu,</l>
						<l>Comme je fais ici, vous asseoir à ma table ?</l>
						<l>Ah ! j’en éprouverais un bonheur véritable !</l>
					</lg>
					<lg n="21">
						<l>— Nous nous sommes connus, mais voilà bien longtemps ;</l>
						<l>Nous sommes à l’hiver, nous étions au printemps.</l>
					</lg>
					<lg n="22">
						<l>– Vraiment, c’est bien heureux ! mais dites-moi, brave homme</l>
						<l>En quel endroit c’était et comment l’on vous nomme.</l>
					</lg>
					<lg n="23">
						<l>— C’était à l’Ile, Jean, reprend le forgeron,</l>
						<l>Et je me nomme, moi, Cyprien Bergeron.</l>
					</lg>
					<lg n="24">
						<l>Dumas reste muet de stupeur ; et sa femme,</l>
						<l>Poussant de ces sanglots qui vous déchirent l’âme</l>
						<l>Et fondant tout à coup en pleurs, s’écrie alors,</l>
						<l>— Vengez-vous, Cyprien, et jetez- nous dehors !</l>
						<l>Et Dumas, demandant le pardon de sa faute,</l>
						<l>Tomba dans la poussière aux genoux de son hôte.</l>
					</lg>
					<lg n="25">
						<l>— Viens, dit le forgeron tout ému ; lève-toi !</l>
						<l>Ne t’agenouille point comme ça devant moi,</l>
						<l>Cela me rend honteux, et je crois qu’on me raille.</l>
						<l part="I">Entrons. </l>
						<l part="F">Le crucifix pendait sur la muraille.</l>
						<l>Il s’en fut à ses pieds se jeter à genoux</l>
						<l part="I">Et dit, levant les mains : </l>
						<l part="F">Mon Dieu, pardonnez-nous</l>
						<l>Comme nous pardonnons à ceux qui nous offensent !</l>
					</lg>
					<lg n="26">
						<l part="I">Puis, quand il fut debout : </l>
					</lg>
					<lg n="27">
						<l part="F">— Jean, les moissons commencent,</l>
						<l>Et je cultive un peu tout en forgeant beaucoup.</l>
						<l>J’ai besoin que l’on m’aide, et je fais un bon coup</l>
						<l>En vous gardant ici, toi, ta femme et ta fille.</l>
						<l>Nous ferons désormais uue seule famille.</l>
					</lg>
					<lg n="28">
						<l>Les jeunes gens rentraient juste à ce moment-là</l>
					</lg>
					<lg n="29">
						<l>— Mon père, ajouta Paul, je songeais à cela</l>
					</lg>
				</div>
			</div>
			<div type="poem" key="LEM275">
				<head type="main">L’HIVER</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l>L’hiver !… voici l’hiver ! Il plane sur nos têtes</l>
						<l><space unit="char" quantity="8"/>Comme un cygne blanc sur les flots.</l>
						<l>L’hiver sous notre ciel, c’est la saison des fêtes,</l>
						<l><space unit="char" quantity="8"/>C’est le signal des longs sanglots ;</l>
						<l>C’est l’époque enivrante où plaisirs et lumières</l>
						<l><space unit="char" quantity="8"/>Inondent les salons dorés ;</l>
						<l>C’est l’heure redoutable où les froides chaumières</l>
						<l><space unit="char" quantity="8"/>Abritent des malheurs sacrés !</l>
					</lg>
					<lg n="2">
						<l>Sur le flanc des coteaux, au milieu des prairies,</l>
						<l><space unit="char" quantity="8"/>La neige étincelle au soleil ;</l>
						<l>On dirait jusqu’au loin d’immenses draperies</l>
						<l><space unit="char" quantity="8"/>Aux fils d’argent et de vermeil.</l>
						<l>Et des troupes d’enfants, au milieu de ces plaines,</l>
						<l><space unit="char" quantity="8"/>Glissent en riant aux éclats……</l>
						<l>Enfants que je chéris, vers la saison des peines</l>
						<l><space unit="char" quantity="8"/>Vous glissez bien plus vite, hélas !</l>
					</lg>
					<lg n="3">
						<l>Quelques flocons de neige aux arbres sans feuillage</l>
						<l><space unit="char" quantity="8"/>Se sont attachés, par hasard,</l>
						<l>Comme les cheveux blancs que vient suspendre l’âge</l>
						<l><space unit="char" quantity="8"/>Sur le front ridé d’un vieillard.</l>
						<l>Le givre s’est collé, comme un rideau de gaze,</l>
						<l><space unit="char" quantity="8"/>Aux vitres de l’humble réduit,</l>
						<l>Et le pauvre ouvrier que le travail écrase</l>
						<l><space unit="char" quantity="8"/>Ne peut voir si le soleil luit.</l>
					</lg>
					<lg n="4">
						<l>Il ne voit pas, non plus, sur la neige éclatante,</l>
						<l><space unit="char" quantity="8"/>Glisser ces superbes traîneaux</l>
						<l>Qu’emportent, frémissant sous la rêne flottante,</l>
						<l><space unit="char" quantity="8"/>Des couples de fougueux chevaux.</l>
						<l>Peut-être un sourd murmure, un blasphème, peut- être,</l>
						<l><space unit="char" quantity="8"/>Monterait du fond de son cœur,</l>
						<l>S’il voyait tant d’heureux passer à sa fenêtre</l>
						<l><space unit="char" quantity="8"/>Comme pour narguer son malheur.</l>
					</lg>
					<lg n="5">
						<l>Promenez votre orgueil sur vos riches voitures,</l>
						<l><space unit="char" quantity="8"/>Vous que le ciel fit naître heureux ;</l>
						<l>Enveloppez-vous bien dans vos chaudes fourrures ;</l>
						<l><space unit="char" quantity="8"/>Fouettez vos coursiers vigoureux ;</l>
						<l>Éblouissez le gueux par votre absurde faste ;</l>
						<l><space unit="char" quantity="8"/>Troublez ses jours si peu sereins….</l>
						<l>Il pourrait oublier qu’il est d’une autre caste,</l>
						<l><space unit="char" quantity="8"/>Que vous êtes ses souverains !</l>
					</lg>
					<lg n="6">
						<l>Quand minuit a sonné, que le bal se repose</l>
						<l><space unit="char" quantity="8"/>Afin de mieux se réveiller ;</l>
						<l>Quand vos petits enfants aux visages de rose</l>
						<l><space unit="char" quantity="8"/>Dorment sur leur tiède oreiller,</l>
						<l>Sous le chaume du pauvre une mère travaille</l>
						<l><space unit="char" quantity="8"/>Depuis le lever du matin ;</l>
						<l>Ses petits, décharnés, grelottent sur la paille</l>
						<l><space unit="char" quantity="8"/>Et demandent un peu de pain.</l>
					</lg>
					<lg n="7">
						<l>Pendant que le vent souffle et que la neige effleure</l>
						<l><space unit="char" quantity="8"/>Vos grands châssis tout radieux,</l>
						<l>Près d’un feu qui s’éteint, l’indigence qui pleure</l>
						<l><space unit="char" quantity="8"/>Accuse peut-être les cieux.</l>
						<l>Elle courbe son front. D’un baiser vient la mordre</l>
						<l><space unit="char" quantity="8"/>Le spectre des mornes hivers ;</l>
						<l>Elle croit qu’au hasard, dans un honteux désordre,</l>
						<l><space unit="char" quantity="8"/>Dieu laisse rouler l’univers.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l>— " Pendant que chez le riche un grand foyer pétille,</l>
						<l>Déployant ses rayons comme un soleil d’été ;</l>
						<l>Pendant que, dans la nuit, la lampe d’or scintille,</l>
						<l>Comme l’œil d’une vierge, au plafond tout sculpté,</l>
					</lg>
					<lg n="2">
						<l>" En vain j’attise, moi, ma froide cheminée,</l>
						<l>Je ne puis réchauffer mes membres engourdis !…</l>
						<l>N’aurai-je donc jamais une autre destinée !…</l>
						<l>Malheur ! je ne crois plus ni Dieu, ni Paradis !…</l>
					</lg>
					<lg n="3">
						<l>" Ou Dieu n’est qu’un tyran. Je travaille sans trève</l>
						<l>Pour un morceau de pain, pour un morceau de bois !…</l>
						<l>Quel bien ai-je aujourd’hui ? Chaque jour qui se lève</l>
						<l>A ma longue misère apporte un nouveau poids !</l>
					</lg>
					<lg n="4">
						<l>" Et, sous ce toit de chaume, une pierre est mon siége :</l>
						<l>Cette paille est mon lit, et ma table est sans pain !</l>
						<l>Je n’ai pour me garder des rigueurs de la neige</l>
						<l>Que ces méchants souliers, que cet habit vilain !</l>
					</lg>
					<lg n="5">
						<l>" Le riche lève-t-il des mains vides de crimes</l>
						<l>Vers ce Dieu sans pitié qui rit de ma douleur ?</l>
						<l>Le riche aide le ciel à faire des victimes,</l>
						<l>Et le ciel, en retour, le garde du malheur.</l>
					</lg>
					<lg n="6">
						<l>Pour prétendre à la paix qu’a-t- il fait dans la vie ?</l>
						<l>A- t-il gravi le roc où monte la vertu ?</l>
						<l>A-t-il prié le Dieu que ma bouche injurie</l>
						<l>Pour qu’il fit reverdir son courage abattu ?</l>
					</lg>
					<lg n="7">
						<l>" Pourquoi n’être point mort dès le sein de ma mère ?</l>
						<l>Pourquoi dans le néant n’être point rejeté ?</l>
						<l>Dieu cruel, le tourment de ma vie éphémère</l>
						<l>Était-il nécessaire à ta félicité ?</l>
					</lg>
					<lg n="8">
						<l>" Je ne crois pas en Dieu, je me plais à le dire ;</l>
						<l>Un Dieu pourrait-il donc avoir un cour de fer ?</l>
						<l>S’il existe qu’il frappe, et qu’à l’instant j’expire…</l>
						<l>Nous nous réchaufferons au feu de son enfer ! "</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<head type="main">LE CANTIQUE DU BON PAUVRE</head>
					<lg n="1">
						<l>Quand la feuille d’ormeau tapisse la vallée,</l>
						<l>Que l’enfant ne suit plus la solitaire allée</l>
						<l><space unit="char" quantity="12"/>Pour prendre un papillon,</l>
						<l>Que les champs, sous la faux, ont vu tomber leurs gerbes,</l>
						<l>Que l’insecte prudent trottine sous les herbes</l>
						<l><space unit="char" quantity="12"/>Et se cache au sillon,</l>
					</lg>
					<lg n="2">
						<l>Seigneur, j’espère en toi ; car l’heure qui s’avance,</l>
						<l>Sur son aile glacée apporte la souffrance</l>
						<l><space unit="char" quantity="12"/>Au seuil de l’indigent ;</l>
						<l>Seigneur, j’espère en toi, car sur l’homme qui pleure</l>
						<l>Tu reposes toujours, de ta Sainte Demeure,</l>
					</lg>
					<lg n="3">
						<l><space unit="char" quantity="12"/>Un regard indulgent.</l>
						<l>Comme un champ que l’automne a noyé dans sa brume</l>
						<l>Mon cœur est, en ces jours, noyé dans l’amertume</l>
						<l><space unit="char" quantity="12"/>Mon cour toujours soumis !</l>
						<l>Après elle traînant sa lamentable escorte,</l>
						<l>La misère, en haillons, s’est assise à ma porte,</l>
						<l><space unit="char" quantity="12"/>Je suis de ses amis !…</l>
					</lg>
					<lg n="4">
						<l>Que le riche demeure à l’abri des orages ;</l>
						<l>Que la froide saison réserve ses outrages</l>
						<l><space unit="char" quantity="12"/>Pour tous ceux qui n’ont rien ;</l>
						<l>Que chaque heure qui vient m’apporte sur son aile</l>
						<l>Un pénible regret, une angoisse nouvelle,</l>
						<l><space unit="char" quantity="12"/>Si Dieu le veut, c’est bien.</l>
					</lg>
					<lg n="5">
						<l>Celui dont le regard veille sur tous les êtres,</l>
						<l>Qui nourrit l’araignée au coin de mes fenêtres,</l>
						<l><space unit="char" quantity="12"/>Le grillon au foyer,</l>
						<l>Pourrait-il, en voyant son enfant sur la terre</l>
						<l>Élever, vers le ciel, un cour pur et sincère,</l>
						<l><space unit="char" quantity="12"/>Ne pas s’apitoyer ?</l>
					</lg>
					<lg n="6">
						<l>Si la vie, à mes yeux, n’offre guère de charmes,</l>
						<l>Si je mange mon pain détrempé de mes larmes,</l>
						<l><space unit="char" quantity="12"/>Mon âme est dans la paix.</l>
						<l>Quand à mon Crucifix mes regards se suspendent,</l>
						<l>Des soucis dévorants, des douleurs qui m’attendent</l>
						<l><space unit="char" quantity="12"/>Je ne crains plus le faix.</l>
					</lg>
					<lg n="7">
						<l>Chaque saison qui fuit, chaque nouvelle année</l>
						<l>Nous disent que bientôt l’on verra terminée</l>
						<l><space unit="char" quantity="12"/>Notre course en ce lieu :</l>
						<l>Et le riche et le pauvre attendront, en poussière,</l>
						<l>Le redoutable jour où luira tout entière</l>
						<l><space unit="char" quantity="12"/>La justice de Dieu.</l>
					</lg>
				</div>
			</div>
		</body>
	</text>
</TEI>