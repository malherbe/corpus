
L’Âme nue


Edmond Haraucourt
L’Âme nue
G. Charpentier et Cie, éditeurs, 1885.
Texte sur une seule page


EDMOND HARAUCOURT



L’ÂME NUE


Regardez en vous comme votre juge vous regarde, et voyez ce qu’il y voit : ce nombre innombrable de péchés.
Bossuet.
ὦ φύσις· ἐκ σοῦ πάντα, ἐν σοὶ πάντα, εἰς σὲ πάντα.
Marcus Aurelius Antoninus.



PARIS
G. CHARPENTIER ET Cie, ÉDITEURS
13, RUE DE GRENELLE, 13
1885




I
LA VIE EXTÉRIEURE
LES LOIS
Le Buste
 3
L’Immuable
 7
Chanson à boire
 10
La Réponse de la Terre
 14
L’Étape
 19
Les Atomes
 22
Clair de Lune
 25
L’Agonie du Soleil
 27
L’Océan
 29


LES CULTES
Le Chant du départ
 33
Conseil du Maître
 36
Les Galoubets
 38
Le Vase
 40
Fuir !
 43
Les Frères
 47
Le Cheval de fiacre
 48
Sur un berceau
 49
Lorsque j’étais enfant
 52
Le Sou
 54
Le peu de foi que j’ai
 55
Arma virumque
 56
L’Insulte
 58
Un Poète
 59
À Alfred de Vigny
 60
Sonnets de sang : — Famille
 62
      Honneur
 63
      Société
 64
      Justice
 65
      Religion
 66
      Patrie
 67
La tête du page
 68
Le Charron
 71
L’île Vierge
 76
Le Beaupré
 78
Les Verges
 80
Fille du Mal
 81
Vierges mortes
 83
Le Cloître
 84
Fille
 85
Premier Orage
 86
Résignation
 90
Le Nazaréen
 93
Magnificat
 96


LES FORMES
Alma parens
 101
La Chanson de la mer
 105
Marée basse
 107
Archipel
 109
Soir d’été
 111
Pleine eau
 113
Soir d’octobre
 115
Avril
 117
Demi-Deuil
 119
La Tet
 121
La mort des Rois
 124
Le Crapaud
 126
Les Éphémères
 130
Les Faibles
 132
Aurore
 133
L’Orage
 136
Le Vieux Christ
 139




II
LA VIE INTÉRIEURE
L’AUBE
Petites Chansons de jadis
 143
À la Désolée
 145
Romance
 147
Romance
 149
Romance
 151
Romance
 153
Romance
 154
Romance
 156
Romance
 158
Le Vent
 160
Dame du Ciel
 163
Renonciation
 166
Toute la vie humaine
 168
Fin du Rêve
 169


MIDI
L’Église
 171
Le Bouclier
 176
Æea
 179
Parisienne
 182
Sonnet à ma mie
 183
En Crète
 184
Calymanthe
 188
Remords futur
 189
Reine du Monde
 191
Adultère
 193
L’Absente
 194
Brune
 196
Chanaan
 199
Vers à Circé
 203
L’Inoubliable
 204
La Sagesse de l’Eunuque
 206
L’Axe
 209
À Gaston Béthune
 211


LE SOIR
La Brute
 215
Ivre
 219
L’Introuvable
 222
Le Vaisseau
 224
Les Bêtes
 226
À quoi bon les baisers
 227
Campo Santo
 228
À Dieu
 229
L’Orfèvre
 230
Cri du Coq
 231
Les Délaissés
 232
La Sirène
 234
La Cité morte
 235
La Lune
 238
Chiens errants
 239
Vraie Mort
 240
Les Gibets
 241
Le Lit
 245
Rêve gris
 249
Chant du Retour
 251
Hyménæé
 252
Le Râle
 256
Temps des Fées
 258
Résipiscence
 260
Le Nénuphar
 263


MER DE GLACE
Mer de Glace
 267







    La dernière modification de cette page a été faite le 21 novembre 2015 à 23:48.
    Les textes sont disponibles sous licence Creative Commons Attribution-partage dans les mêmes conditions ; d’autres conditions peuvent s’appliquer. Voyez les conditions d’utilisation pour plus de détails.

