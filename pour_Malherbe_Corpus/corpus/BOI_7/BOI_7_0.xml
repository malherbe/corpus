<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="BOI86">
				<head type="number">I</head>
				<head type="main">Chanson à boire</head>
				<opener>
					<dateline>
						<date when="1654">(1654)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Soupirez jour et nuit, sans manger et sans boire,</l>
					<l><space unit="char" quantity="12"/>Ne songez qu’à souffrir ;</l>
					<l>Aimez, aimez vos maux, et mettez votre gloire</l>
					<l><space unit="char" quantity="12"/>A n’en jamais guérir.</l>
				</lg>
				<lg n="2">
					<l><space unit="char" quantity="12"/>Cependant nous rirons</l>
					<l><space unit="char" quantity="12"/>Avecque la bouteille,</l>
					<l><space unit="char" quantity="14"/>Et dessous la treille</l>
					<l><space unit="char" quantity="14"/>Nous la chérirons.</l>
				</lg>
				<lg n="3">
					<l>Si, sans vous soulager, une aimable cruelle</l>
					<l><space unit="char" quantity="12"/>Vous retient en prison,</l>
					<l>Allez, aux durs rochers, aussi sensibles qu’elle,</l>
					<l><space unit="char" quantity="12"/>En demander raison.</l>
				</lg>
				<p>
					Cependant nous rirons, etc.
				</p>
			</div>
			<div type="poem" key="BOI87">
				<head type="number">II</head>
				<head type="main">Sonnet sur la mort d’une de mes parentes</head>
				<opener>
					<dateline>
						<date when="1663">(1663)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Parmi les doux transports d’une amitié fidèle,</l>
					<l>Je voyais près d’Iris couler mes heureux jours :</l>
					<l>Iris, que j’aime encore, et que j’aimai toujours,</l>
					<l>Brûlait des mêmes feux dont je brûlais pour elle ;</l>
				</lg>
				<lg n="2">
					<l>Quand, par l’ordre du ciel, une fièvre cruelle</l>
					<l>M’enleva cet objet de mes tendres amours ;</l>
					<l>Et, de tous mes plaisirs interrompant le cours,</l>
					<l>Me laissa de regrets une suite éternelle.</l>
				</lg>
				<lg n="3">
					<l>Ah ! qu’un si rude coup étonna mes esprits !</l>
					<l>Que je versai de pleurs ! que je poussai de cris !</l>
					<l>De combien de douleurs ma douleur fut suivie !</l>
				</lg>
				<lg n="4">
					<l>Iris, tu fus alors moins à plaindre que moi ;</l>
					<l>Et, bien qu’un triste sort t’ait fait perdre la vie,</l>
					<l>Hélas ! en te perdant j’ai perdu plus que toi.</l>
				</lg>
			</div>
			<div type="poem" key="BOI88">
				<head type="number">III</head>
				<head type="main">Fragment de la relation d’un voyage à Saint-Prix</head>
				<lg n="1">
					<l><space unit="char" quantity="8"/>J’ai beau m’en aller à Saint-Prix.</l>
					<l><space unit="char" quantity="8"/>Ce saint, qui de tous maux guérit</l>
					<l>Ne saurait me guérir de mon amour extrême.</l>
					<l><space unit="char" quantity="8"/>Philis, il le faut avouer,</l>
					<l>Si vous ne prenez soin de me guérir vous-même,</l>
					<l>Je ne sais plus du tout à quel saint me vouer.</l>
				</lg>
			</div>
			<div type="poem" key="BOI89">
				<head type="number">IV</head>
				<head type="main">Sur la première représentation <lb/>de l’<hi rend="ital">Agésilas</hi> de Corneille</head>
				<opener>
					<dateline>
						<date when="1666">(1666)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>J’ai vu l’<hi rend="ital">Agésilas</hi>.</l>
					<l><space unit="char" quantity="8"/>Hélas !</l>
				</lg>
			</div>
			<div type="poem" key="BOI90">
				<head type="number">V</head>
				<head type="main">Sur la première représentation <lb/>de l’<hi rend="ital">Attila</hi></head>
				<opener>
					<dateline>
						<date when="1667">(1667)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Après l’<hi rend="ital">Agésilas</hi>,</l>
					<l><space unit="char" quantity="8"/>Hélas !</l>
					<l>Mais après l’<hi rend="ital">Attila</hi>,</l>
					<l><space unit="char" quantity="8"/>Holà !</l>
				</lg>
			</div>
			<div type="poem" key="BOI91">
				<head type="number">VI</head>
				<head type="main">Sur un frère aîné <lb/>que j’avais et avec qui j’étais brouillé</head>
				<opener>
					<dateline>
						<date when="1668">(1668)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>De mon frère, il est vrai, les écrits sont vantés.</l>
					<l><space unit="char" quantity="8"/>Il a cent belles qualités,</l>
					<l>Mais il n’a pas pour moi d’affection sincère.</l>
					<l><space unit="char" quantity="4"/>En lui je trouve un excellent auteur,</l>
					<l>Un poète agréable, un très bon orateur,</l>
					<l><space unit="char" quantity="8"/>Mais je n’y trouve point de frère.</l>
				</lg>
			</div>
			<div type="poem" key="BOI92">
				<head type="number">VII</head>
				<head type="main">Chanson dont les vers sont dans le goût <lb/>de ceux de Chapelain</head>
				<lg n="1">
					<l>Droits et roides rochers, dont peu tendre est la cime,</l>
					<l>De mon flamboyant cœur l’âpre état vous savez.</l>
					<l>Savez aussi, durs bois par les hivers lavés,</l>
					<l>Qu’holocauste est mon cœur pour un front magnanime.</l>
				</lg>
			</div>
			<div type="poem" key="BOI93">
				<head type="number">VIII</head>
				<head type="main">Réponse à des couplets <lb/>satiriques de Linière</head>
				<lg n="1">
					<l>Linière apporte de Senlis</l>
					<l>Tous les mois trois couplets impies.</l>
					<l>A quiconque en veut dans Paris,</l>
					<l>Il en présente des copies.</l>
					<l>Mais ses couplets, tout pleins d’ennui,</l>
					<l>Seront brûlés, même avant lui.</l>
				</lg>
			</div>
			<div type="poem" key="BOI94">
				<head type="number">IX</head>
				<head type="main">Parodie de cinq vers de Chapelle</head>
				<lg n="1">
					<l>Tout grand ivrogne du Marais</l>
					<l>Fait des vers que l’on ne lit guère.</l>
					<l>Il les croit pourtant fort bien faits ;</l>
					<l>Et quand il cherche à les mieux faire,</l>
					<l>Il les fait encor plus mauvais.</l>
				</lg>
			</div>
			<div type="poem" key="BOI95">
				<head type="number">X</head>
				<head type="main">Impromptu,</head>
				<head type="sub_1"> à une dame qui demandait à l’auteur <lb/>un quatrain sur la prise de Mons</head>
				<opener>
					<dateline>
						<date when="1691">(1691)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l><space unit="char" quantity="4"/>Mons était, disait-on, pucelle</l>
					<l>Qu’un roi gardait avec le dernier soin.</l>
					<l><space unit="char" quantity="4"/>Louis le Grand en eut besoin ;</l>
					<l>Mons se rendit : vous auriez fait comme elle.</l>
				</lg>
			</div>
			<div type="poem" key="BOI96">
				<head type="number">XI</head>
				<head type="main">Épigramme contre Perrault et ses partisans</head>
				<opener>
					<dateline>
						<date when="1694">(1694)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Ne blâmez pas Perrault de condamner Homère,</l>
					<l><space unit="char" quantity="8"/> Virgile, Aristote, Platon.</l>
					<l><space unit="char" quantity="8"/>Il a pour lui monsieur son frère,</l>
					<l><subst type="restitution" hand="RR" reason="analysis"><del>G....</del><add rend="hidden">Grammont</add></subst> <subst type="restitution" hand="RR" reason="analysis"><del>N....</del><add rend="hidden">Nevers</add></subst> Lavau, Caligula, Néron,</l>
					<l><space unit="char" quantity="8"/>Et le gros Charpentier, dit-on.</l>
				</lg>
			</div>
			<div type="poem" key="BOI97">
				<head type="number">XII</head>
				<head type="main">Épigramme sur la réconciliation <lb/>de l’auteur et de Perrault</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Tout le trouble poétique,</l>
					<l>A Paris s’en va cesser,</l>
					<l>Perrault l’antipindarique,</l>
					<l>Et Despréaux l’homérique</l>
					<l>Consentent de s’embrasser.</l>
					<l>Quelque aigreur qui les anime,</l>
					<l>Quand, malgré l’emportement</l>
					<l>Comme eux l’un l’autre on s’estime,</l>
					<l>L’accord se fait aisément.</l>
					<l>Mon embarras est comment</l>
					<l>On pourra finir la guerre</l>
					<l>De Pradon et du parterre.</l>
				</lg>
			</div>
			<div type="poem" key="BOI98">
				<head type="number">XIII</head>
				<head type="main">Épitaphe de M. Arnauld, docteur de Sorbonne</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Au pied de cet autel de structure grossière,</l>
					<l>Gît sans pompe, enfermé dans une vile bière,</l>
					<l>Le plus savant mortel qui jamais ait écrit :</l>
					<l>Arnauld, qui sur la grâce instruit par Jésus-Christ,</l>
					<l>Combattant pour l’Église, a, dans l’Église même,</l>
					<l>Souffert plus d’un outrage et plus d’un anathème.</l>
					<l>Plein du feu qu’en son cœur soufflait l’esprit divin,</l>
					<l>Il terrassa Pelage, il foudroya Calvin,</l>
					<l>De tous les faux docteurs confondit la morale,</l>
					<l>Mais, pour prix de son zèle, on l’a vu rebuté,</l>
					<l>En cent lieux opprimé par leur noire cabale,</l>
					<l>Errant, pauvre, banni, proscrit, persécuté ;</l>
					<l>Et, même par sa mort leur fureur mal éteinte</l>
					<l>N’aurait jamais laissé ses cendres en repos,</l>
					<l>Si Dieu lui-même, ici, de son ouaille sainte</l>
					<l>A ces loups dévorants n’avait caché les os.</l>
				</lg>
			</div>
			<div type="poem" key="BOI99">
				<head type="number">XIV</head>
				<head type="main">Épigramme contre l’Académie</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l><space unit="char" quantity="8"/>J’ai traité de Topinamboux</l>
					<l><space unit="char" quantity="8"/>Tous ces beaux censeurs, je l’avoue,</l>
					<l>Qui, de l’antiquité si follement jaloux,</l>
					<l>Aiment tout ce qu’on hait, blâment tout ce qu’on loue ;</l>
					<l><space unit="char" quantity="8"/>Et l’Académie, entre nous,</l>
					<l><space unit="char" quantity="8"/>Souffrant chez soi de si grands fous,</l>
					<l><space unit="char" quantity="8"/>Me semble un peu Topinamboue.</l>
				</lg>
			</div>
			<div type="poem" key="BOI100">
				<head type="number">XV</head>
				<head type="main">Épigramme</head>
				<opener>
					<dateline>
						<date when="1698">(1698)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>J’approuve que chez vous, messieurs, on examine</l>
					<l>Qui, du pompeux Corneille ou du tendre Racine,</l>
					<l>Excita dans Paris plus d’applaudissements.</l>
					<l><space unit="char" quantity="4"/>Mais je voudrais qu’on cherchât tout d’un temps</l>
					<l><space unit="char" quantity="8"/>— La question n’est pas moins belle —</l>
					<l>Qui, du fade Boyer ou du sec La Chapelle,</l>
					<l><space unit="char" quantity="8"/>Excita plus de sifflements.</l>
				</lg>
			</div>
			<div type="poem" key="BOI101">
				<head type="number">XVI</head>
				<head type="main">Sur un portrait de l’auteur</head>
				<opener>
					<dateline>
						<date when="1699">(1699)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l><space unit="char" quantity="8"/>Ne cherchez point comment s’appelle</l>
					<l><space unit="char" quantity="8"/>L’écrivain peint dans ce tableau.</l>
					<l>A l’air dont il regarde et montre la Pucelle,</l>
					<l><space unit="char" quantity="8"/>Qui ne reconnaîtrait Boileau ?</l>
				</lg>
			</div>
			<div type="poem" key="BOI102">
				<head type="number">XVII</head>
				<head type="main">Épitaphe</head>
				<opener>
					<dateline>
						<date when="1703">(1703)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Ci gît, justement regretté,</l>
					<l>Un savant homme sans science,</l>
					<l>Un gentilhomme sans naissance,</l>
					<l>Un très bon homme sans bonté.</l>
				</lg>
			</div>
			<div type="poem" key="BOI103">
				<head type="number">XVIII</head>
				<head type="main">Sur le comte de Gramont</head>
				<opener>
					<dateline>
						<date when="1705">(1705)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l>Fait d’un plus pur limon, Gramont à son printemps</l>
					<l>N’a point vu succéder l’hiver de la vieillesse ;</l>
					<l>La cour le voit encor brillant, plein de noblesse,</l>
					<l><space unit="char" quantity="8"/>Dire les plus fins mots du temps,</l>
					<l>Effacer ses rivaux auprès d’une maîtresse</l>
					<l>Sa course n’est au fond qu’une longue jeunesse,</l>
					<l>Qu’il a déjà poussée à deux fois quarante ans.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>