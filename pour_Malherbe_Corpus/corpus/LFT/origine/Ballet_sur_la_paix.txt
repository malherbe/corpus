Telles étoient jadis ces illustres bergères
Que le Lignon tenoit si chères ;
Tels étoient ces bergers qui, le long de ses eaux,
Menoient leurs paisibles troupeaux,
Et passoient dans les jeux leurs plus belles années.
Parmi ces troupes fortunées,
Les plaisirs de campagne et les plaisirs de cour
Trouvoient leur place tour à tour.
Comme eux, tantôt on nous voit sur l'herbette
Marquer nos pas au son de la musette,
Cueillir et présenter les fleurs,
En y mêlant quelques douceurs :
Tantôt aux bords de nos fontaines
Nous chantons de l'amour les plaisirs et les peines ;
Et le divin. Tircis mêle aussi quelquefois
Son téorbe divin aux accents de nos voix.
Parfois à sa bergère on donne sérénade ;
Avec elle on fait mascarade,
On danse même des ballets.
On fait des -vers galants, on en fait des follets.
Nous lisons de Renaud les douces aventures,
Et les magiques impostures
De la belle qui l'enchanta ;
Tout ce que le Tasse chanta,
Et mille autres récits que la galanterie
Semble avoir inventés pour notre bergerie.
Nous vous dirons aussi que nos brillants guérets
Et nos sombres forêts
Nous fournissent parfois de quoi faire grand'chère ;
Mais cela paraîtrait vulgaire,
Et l'on diroit qu'en discours de berger
On ne parle jamais de boire et de manger.
Ainsi passe le temps, sans tracas, sans cabale ;
Gens d'une humeur assez égale ;
Voilà nos douces libertés :
Qu'ont de mieux vos sociétés ?
