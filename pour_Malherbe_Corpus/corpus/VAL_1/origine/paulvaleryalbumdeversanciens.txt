Album De Vers Anciens. (1920)

Par Paul Val�ry. (1871-1945)









TABLE DES MATIERES
La Fileuse.
H�l�ne.
Orph�e.
Naissance De V�nus.
F�erie.
M�me F�erie.
Baign�e.
Au Bois Dormant.
C�sar.
Le Bois Amical.
Les Vaines Danseuses.
Un Feu Distinct...
Narcisse Parle.
�pisode.
Vue.
Valvins.
�t�.
Profusion Du Soir.
Anne.
Air De S�miramis.
L�Amateur De Po�mes. 












La Fileuse.

Assise, la fileuse au bleu de la crois�e
O� le jardin m�lodieux se dodeline;
Le rouet ancien qui ronfle l�a gris�e.

Lasse, ayant bu l�azur, de filer la c�line
Chevelure, � ses doigts si faibles �vasives,
Elle songe, et sa t�te petite s�incline.

Un arbuste et l�air pur font une source vive
Qui, suspendue au jour, d�licieuse arrose
De ses pertes de fleurs le jardin de l�oisive.

Une tige, o� le vent vagabond se repose,
Courbe le salut vain de sa gr�ce �toil�e,
D�diant magnifique, au vieux rouet, sa rose.

Mais la dormeuse file une laine isol�e;
Myst�rieusement l�ombre fr�le se tresse
Au fil de ses doigts longs et qui dorment, fil�e.

Le songe se d�vide avec une paresse
Ang�lique, et sans cesse, au doux fuseau cr�dule,
La chevelure ondule au gr� de la caresse...

Derri�re tant de fleurs, l�azur se dissimule,
Fileuse de feuillage et de lumi�re ceinte:
Tout le ciel vert se meurt. Le dernier arbre br�le.

Ta soeur, la grande rose o� sourit une sainte,
Parfume ton front vague au vent de son haleine
Innocente, et tu crois languir... Tu es �teinte

Au bleu de la crois�e o� tu filais la laine.







H�l�ne.

Azur! c�est moi... Je viens des grottes de la mort
Entendre l�onde se rompre aux degr�s sonores,
Et je revois les gal�res dans les aurores
Ressuciter de l�ombre au fil des rames d�or.

Mes solitaires mains appellent les monarques
Dont la barbe de sel amusait mes doigts purs;
Je pleurais. Ils chantaient leurs triomphes obscurs
Et les golfes enfuis aux poupes de leurs barques.

J�entends les conques profondes et les clairons
Militaires rythmer le vol des avirons;
Le chant clair des rameurs encha�nes le tumulte,

Et les Dieux, � la proue h�ro�que exalt�s
Dans leur sourire antique et que l��cume insulte,
Tendent vers moi leurs bras indulgents et sculpt�s.











Orph�e.

... Je compose en esprit, sous les myrtes, Orph�e
L�Admirable!... le feu, des cirques purs descend;
Il change le mont chauve en auguste troph�e
D�o� s�exhale d�un dieu l�acte retentissant.

Si le dieu chante, il rompt le site tout-puissant;
Le soleil voit l�horreur du mouvement des pierres;
Une plainte inou�e appelle �blouissants
Les hauts murs d�or harmonieux d�un sanctuaire.

Il chante, assis au bord du ciel splendide, Orph�e!
Le roc marche, et tr�buche; et chaque pierre f�e
Se sent un poids nouveau qui vers l�azur d�lire!

D�un Temple � demi nu le soir baigne l�essor,
Et soi-m�me il s�assemble et s�ordonne dans l�or
� l��me immense du grand hymne sur la lyre!









Naissance De V�nus.

De sa profonde m�re, encor froide et fumante,
Voici qu�au seuil battu de temp�tes, la chair
Am�rement vomie au soleil par la mer,
Se d�livre des diamants de la tourmente.

Son sourire se forme, et suit sur ses bras blancs
Qu��plore l�orient d�une �paule meurtrie,
De l�humide Th�tis la pure pierrerie,
Et sa tresse se fraye un frisson sur ses flancs.

Le frais gravier, qu�arrose et fuit sa course agile,
Croule, creuse rumeur de soif, et le facile
Sable a bu les baisers de ses bonds pu�rils;

Mais de mille regards ou perfides ou vagues,
Son oeil mobile m�le aux �clairs de p�rils
L�eau riante, et la danse infid�le des vagues.










F�erie.

La lune mince verse une lueur sacr�e,
Toute une jupe d�un tissu d�argent l�ger,
Sur les bases de marbre o� vient l�Ombre songer
Que suit d�un char de perle une gaze nacr�e.

Pour les cygnes soyeux qui fr�lent les roseaux
De car�nes de plume � demi lumineuse,
Elle effeuille infinie une rose neigeuse
Dont les p�tales font des cercles sur les eaux...

Est-ce vivre?... � d�sert de volup� pam�e
O� meurt le battement faible de l�eau lam�e,
Usant le seuil secret des �chos de cristal...

La chair confuse des molles roses commence
� fr�mir, si d�un cri le diamant fatal
F�le d�un fil de jour toute la fable immense.










M�me F�erie.

La lune mince verse une lueur sacr�e,
Comme une jupe d�un tissu d�argent l�ger,
Sur les masses de marbre o� marche et croit songer
Quelque vierge de perle et de gaze nacr�e.

Pour les cygnes soyeux qui fr�lent les roseaux
De car�nes de plume � demi lumineuse,
Sa main cueille et dispense une rose neigeuse
Dont les p�tales font des cercles sur les eaux.

D�licieux d�sert, solitude p�m�e,
Quand le remous de l�eau par la lune lam�e
Compte �ternellement ses �chos de cristal,

Quel coeur pourrait souffrir l�inexorable charme
De la nuit �clatante au firmament fatal,
Sans tirer de soi-m�me un cri pur comme une arme?











Baign�e.

Un fruit de chair se baigne en quelque jeune vasque,
(Azur dans les jardins tremblants) mais hors de l�eau,
Isolant la torsade aux puissances de casque,
Luit le chef d�or que tranche � la nuque un tombeau.

�close la beaut� par la rose et l��pingle!
Du miroir m�me issue o� trempent ses bijoux,
Bizarres feux bris�s dont le bouquet dur cingle
L�oreille abandonn�e aux mots nus des flots doux.

Un bras vague inond� dans le n�ant limpide
Pour une ombre de fleur � cueillir vainement
S�effile, ondule, dort par le d�lice vide,

Si l�autre, courb� pur sous le beau firmament,
Parmi la chevelure immense qu�il humecte,
Capture dans l�or simple un vol ivre d�insecte.








Au Bois Dormant.

La princesse, dans un palais de rose pure,
Sous les murmures, sous la mobile ombre dort,
Et de corail �bauche une parole obscure
Quand les oiseaux perdus mordent ses bagues d�or.

Elle n��coute ni les gouttes, dans leurs chutes,
Tinter d�un si�cle vide au lointain le tr�sor,
Ni, sur la for�t vague, un vent fondu de fl�tes
D�chirer la rumeur d�une phrase de cor.

Laisse, longue, l��cho rendormir la diane,
� toujours plus �gale � la molle liane
Qui se balance et bat tes yeux ensevelis.

Si proche de ta joue et si lente la rose
Ne va pas dissiper ce d�lice de plis
Secr�tement sensible au rayon qui s�y pose.










C�sar.

C�sar, calme C�sar, le pied sur toute chose,
Les poings durs dans la barbe, et l�oeil sombre peupl�
D�aigles et des combats du couchant contempl�,
Ton coeur s�enfle, et se sent toute-puissante Cause.

Le lac en vain palpite et l�che son lit rose;
En vain d�or pr�cieux brille le jeune bl�;
Tu durcis dans les noeuds de ton corps rassembl�
L�ordre, qui doit enfin fendre ta bouche close.

L�ample monde, au del� de l�immense horizon,
L�Empire attend l��clair, le d�cret, le tison
Qui changeront le soir en furieuse aurore.

Heureux l�-bas sur l�onde, et berc� du hasard,
Un p�cheur indolent qui flotte et chante, ignore
Quelle foudre s�amasse au centre de C�sar.









Le Bois Amical.

Nous avons pens� des choses pures
C�te � c�te, le long des chemins,
Nous nous sommes tenus par les mains
Sans dire... parmi les fleurs obscures;

Nous marchions comme des fianc�s
Seuls, dans la nuit verte des prairies;
Nous partagions ce fruit de f�eries
La lune amicale aux insens�s

Et puis, nous sommes morts sur la mousse,
Tr�s loin, tout seuls parmi l�ombre douce
De ce bois intime et murmurant;

Et l�-haut, dans la lumi�re immense,
Nous nous sommes trouv�s en pleurant
� mon cher compagnon de silence!









Les Vaines Danseuses.

Celles qui sont des fleurs l�g�res sont venues,
Figurines d�or et beaut�s toutes menues
O� s�irise une faible lune... Les voici
M�lodieuses fuir dans le bois �clairci.
De mauves et d�iris et de nocturnes roses
Sont les gr�ces de nuit sous leurs danses �closes.
Que de parfums voil�s dispensent leurs doigts d�or!
Mais l�azur doux s�effeuille en ce bocage mort
Et de l�eau mince luit � peine, repos�e
Comme un p�le tr�sor d'une antique ros�e
D�o� le silence en fleur monte... Encor les voici
M�lodieuses fuir dans le bois �clairci.
Aux calices aim�s leurs mains sont gracieuses;
Un peu de lune dort sur leurs l�vres pieuses
Et leurs bras merveilleux aux gestes endormis
Aiment � d�nouer sous les myrtes amis
Leurs liens fauves et leurs caresses... Mais certaines,
Moins captives du rythme et des harpes lointaines,
S�en vont d'un pas subtil au lac enseveli
Boire des lys l�eau fr�le o� dort le pur oubli.










Un Feu Distinct...

Un feu distinct m�habite, et je vois froidement
La violente vie illumin�e enti�re...
Je ne puis plus aimer seulement qu�en dormant
Ses actes gracieux m�lang�s de lumi�re.

Mes jours viennent la nuit me rendre des regards,
Apr�s le premier temps de sommeil malheureux;
Quand le malheur lui-m�me est dans le noir �pars
Ils reviennent me vivre et me donner des yeux.

Que si leur joie �clate, un �cho qui m��veille
N�a rejet� qu�un mort sur ma rive de chair,
Et mon rire �tranger suspend � mon oreille,

Comme � la vide conque un murmure de mer,
Le doute -sur le bord d�une extr�me merveille,
Si je suis, si je fus, si je dors ou je veille?








Narcisse Parle.

Narcissiae placandis manibus. 

� fr�res! tristes lys, je languis de beaut�
Pour m��tre d�sir� dans votre nudit�,
Et vers vous, Nymphe, Nymphe, � Nymphe des fontaines,
Je viens au pur silence offrir mes lames vaines.

Un grand calme m��coute, o� j��coute l�espoir.
La voix des sources change et me parle du soir;
J�entends l�herbe d�argent grandir dans l�ombre sainte,
Et la lune perfide �l�ve son miroir
Jusque dans les secrets de la fontaine �teinte.

Et moi! De tout mon coeur dans ces roseaux jet�,
Je languis, � saphir, par ma triste beaut�!
Je ne sais plus aimer que l�eau magicienne
O� j�oubliai le rire et la rose ancienne.

Que je d�plore ton �clat fatal et pur,
Si mollement de moi fontaine environn�e,
O� puis�rent mes yeux dans un mortel azur
Mon image de fleurs humides couronn�e!

H�las! L�image est vaine et les pleurs �ternels!
� travers les bois bleus et les bras fraternels,
Une tendre lueur d�heure ambigu� existe,
Et d�un reste du jour me forme un fianc�
Nu, sur la place p�le o� m�atrire l�eau triste...
D�licieux d�mon, d�sirable et glac�!

Voici dans l�eau ma chair de lune et de ros�e,
� forme ob�issante � mes yeux oppos�e!
Voici mes bras d�argent dont les gestes sont purs!...
Mes lentes mains dans l�or adorable se lassent
D�appeler ce captif que les feuilles enlacent,
Et je crie aux �chos les noms des dieux obscurs!...

Adieu, reflet perdu sur l�onde calme et close,
Narcisse... ce nom m�me est un tendre parfum
Au coeur suave. Effeuille aux m�nes du d�funt
Sur ce vide tombeau la fun�rale rose.

Sois, ma l�vre, la rose effeuillant le baiser
Qui fasse un spectre cher lentement s�apaiser,
Car la nuit parle � demi-voix, proche et lointaine,
Aux calices pleins d�ombre et de sommeils l�gers.
Mais la lune s�amuse aux myrtes allong�s.

Je t�adore, sous ces myrtes, � l�incertaine
Chair pour la solitude �close tristement
Qui se mire dans le miroir au bois dormant.
Je me d�lie en vain de ta pr�sence douce,
L�heure menteuse est molle aux membres sur la mousse
Et d�un sombre d�lice enfle le vent profond.

Adieu, Narcisse... Meurs! Voici le cr�puscule.
Au soupir de mon coeur mon apparence ondule,
La fl�te, par l�azur enseveli module
Des regrets de troupeaux sonores qui s�en vont.
Mais sur le froid mortel o� l��toile s�allume,
Avant qu�un lent tombeau ne se forme de brume,
Tiens ce baiser qui brise un calme d�eau fatal!
L�espoir seul peut suffire � rompre ce cristal.
La ride me ravisse au souffle qui m�exile
Et que mon souffle anime une fl�te gracile
Dont le joueur l�ger me serait indulgent!...

�vanouissez-vous, divinit� troubl�e!
Et, toi, verse � la lune, humble fl�te isol�e,
Une diversit� de nos larmes d�argent.








�pisode.

Un soir favoris� de colombes sublimes,
La pucelle doucement se peigne au soleil.
Aux n�nuphars de l�onde elle donne un orteil
Ultime, et pour ti�dir ses froides mains errantes
Parfois trempe au couchant leurs roses transparentes.
Tant�t, si d�une ond�e innocente, sa peau
Frissonne, c�est le dire absurde d�un pipeau,
Fl�te dont le coupable aux dents de pierrerie
Tire un futile vent d�ombre et de r�verie
Par l�occulte baiser qu�il risque sous les fleurs.
Mais presque indiff�rente aux feintes de ces pleurs,
Ni se se divinisant par aucune parole
De rose, elle d�m�le une lourde aur�ole;
Et tirant de sa nuque un plaisir qui la tord,
Ses poings d�licieux pressent la touffe d�or
Dont la lumi�re coule entre ses doigts limpides!
... Une feuille meurt sur ses �paules humides,
Une goutte tombe de la fl�te sur l�eau,
Et le pied pur s��peure comme un bel oiseau
Ivre d�ombre...











Vue.

Si la plage planche, si
L�ombre sur l�oeil s�use et pleure
Si l�azur est larme, ainsi
Au sel des dents pure affleure

La vierge fum�e ou l�air
Que berce en soi puis expire
Vers l�eau debout d�une mer
Assoupie en son empire

Celle qui sans les ou�r
Si la l�vre au vent remue
Se joue � �vanouir
Mille mots vains o� se mue

Sous l�humide �clair de dents
Le tr�s doux feu du dedans.








Valvins.

Si tu veux d�nouer la for�t qui t�a�re
Heureuse, tu te fonds aux feuilles, si tu es
Dans la fluide yole � jamais litt�raire,
Tra�nant quelques soleils ardemment situ�s

Aux blancheurs de son flanc que la Seine caresse
�mue, ou pressentant l�apr�s-midi chant�,
Selon que le grand bois trempe une longue tresse,
Et m�lange ta voile au meilleur de l��t�.

Mais toujours pr�s de toi que le silence livre
Aux cris multipli�s de tout le brut azur,
L�ombre de quelque page �parse d�aucun livre

Tremble, reflet de voile vagabonde sur
La poudreuse peau de la rivi�re verte
Parmi le long regard de la Seine entr�ouverte.











�t�.

� Francis Viel�-Griffin. 

�t�, roche d�air pur, et toi, ardente ruche,
� mer! �parpill�e en mille mouches sur
Les touffes d�une chair fra�che comme une cruche,
Et jusque dans la bouche o� bourdonne l�azur;

Et toi, maison br�lante, Espace, cher Espace
Tranquille, o� l�arbre fume et perd quelques oiseaux,
O� cr�ve infiniment la rumeur de la masse
De la mer, de la marche et des troupes des eaux,

Tonnes d�odeurs, grands ronds par les races heureuses
Sur le golfe qui mange et qui monte au soleil,
Nids purs, �cluses d�herbe, ombres des vagues creuses,
Bercez l�enfant ravie en un poreux sommeil!

Dont les jambes (mais l�une est fra�che et se d�noue
De la plus rose), les �paules, le sein dur,
Le bras qui se m�lange � l��cumeuse joue
Brillent abandonn�s autour du vase obscur

O� filtrent les grands bruits pleins de b�tes puis�es
Dans les cages de feuille et les mailles de mer
Par les moulins marins et les huttes ros�es
Du jour... Toute la peau dore les treilles d�air.









Profusion Du Soir.
Po�me Abandonn�...

Du soleil soutenant la puissante paresse
Qui plane et s�abandonne � l�oeil contemplateur,
Regard!... Je bois le vin c�leste, et je caresse
Le grain myst�ri-eux de l�extr�me hauteur.

Je porte au sein br�lant ma lucide tendresse,
Je joue avec les feux de l�antique inventeur;
Mais le dieu par degr�s qui se d�sint�resse
Dans la pourpre de l�air s�alt�re avc lenteur.

Laissant dans les champs purs battre toute l�id�e,
Les travaux du couchant dans la sph�re vid�e
Connaissent sans oiseaux leur ancienne grandeur.

L�ange frais de l�oeil nu pressent dans sa pudeur,
Haute nativit� d��toile �lucid�e,
Un diamant agir qui berce la splendeur...

                        *

� soir, tu viens �pandre un d�lice tranquille,
Horizon des sommeils, stupeur des coeurs pieux,
Persuasive approche, insidieux reptile,
Et rose que respire un mortel immobile
Dont l�oeil dore s�engage aux promesses des cieux.

                        *

Sur tes ardents autels son regard favorable
Br�le, l��me distraite, un pass� pr�cieux.
Il adore dans l�or qui se rend adorable
B�tir d�une vapeur un temple m�morable,
Suspendre au sombre �ther son risque et son r�cif,
Et vole, ivre des feux d�un triomphe passif,
Sur l�abime aux ponts d�or rejoindre la Fortune;
-Tandis qu�aux bords lointains du Th��tre pensif,
Sous un masque l�ger glisse la mince lune...

                        *


... Ce vin bu, l�homme b�ille, et brise le flacon.
Aux merveilles du vide il garde une rancune;
Mais le charme du soir fume sur le balcon
Une confusion de femme et de flocon...

                        *

-� Conseil!... Station solennelle!... Balance
D�un doigt dor� pesant les motifs du silence!
� sagesse sensible entre les dieux ardents!
-De l�espace trop beau, pr�serve-moi, balustre!
L�, m�appelle la mer!... L�, se penche l�illustre
V�nus Vertigineuse avec ses bras fondants!

                        *

Mon oeil, quoiqu�il s�attache au sort souple des ondes,
Et boive comme en songe � l��ternel verseau,
Garde une chambre fixe et capable des mondes;
Et ma cupidit� des surprises profondes
Voit � peine au travers du transparent berceau
Cette femme d��cume et d�algue et d�or que roule
Sur le sable et le sel la meule de la houle.

                        *

Pourtant je place aux cieux les �bats d�un esprit;
Je vois dans leurs vapeurs des terres inconnues,
Des deesses de fleurs feindre d��tre des nues,
Des puissances d�orage d�errer a demi nues,
Et sur les roches d�air du soir qui s�assombrit,
Telle divinit� s�accoude. Un ange nage.
Il restaure l�espace � chaque tour de rein.
Moi, qui j�ette ici-bas l�ombre d�un personnage,
Toutefois d�li� dans le plein souverain,
Je me sens qui me trempe, et pur qui me d�daigne!
Vivant au sein futur le souvenir marin,
Tout le corps de mon choix dans mes regards se baigne!

                        *

Une cr�te �cumeuse, �norme et color�e,
Barre, puissamment pure, et plisse le parvis.
Roule jusqu�� mon coeur la distance doree,
Vague!... Croulants soleils aux horizons ravis,
Tu n�iras pas plus loin que la ligne ignor�e
Qui divise les dieux des ombres o� je vis.

                        *

Une volute lente et longue d�une lieue
Semant les charmes lourds de sa blanche torpeur
O� se joue une joie, une soif d��tre bleue,
Tire le noir navire �puis� de vapeur...

                        *

Mais pesants et neigeux les monts du cr�puscule,
Les nuages trop pleins et leurs seins copieux,
Toute la majest� de l�Olympe recule,
Car voici le signal, voici l�or des adieux,
Et l�espace a hum� la barque minuscule...

                        *

Lourds frontons du sommeil toujours inachev�s,
Rideaux bizarrement d�un rubis relev�s
Pour le mauvais regard d�une sombre plan�te,
Les temps sont accomplis, les desirs se sont tus,
Et dans la bouche d�or, b�illements combattus,
S��cart�lent les mots que charmait le po�te...
Les temps sont accomplis, les desirs se sont tus.

                        *

Adieu, Adieu!... Vers vous, � mes belles images,
Mes bras tendent toujours insatiable port!
Venez, effarouch�s, h�rissant vos plumages,
Voiliers aventureux que talonne la mort!
H�tez-vous, h�tez-vous!... La nuit presse!... Tantale
Va p�rir! Et la joie �ph�m�re des cieux!
Une rose nagu�re aux t�n�bres fatale,
Une toute derni�re rose occidentale
P�lit affreusement sur le soir spacieux...
Je ne vois plus fr�mir au m�t du belv�d�re
Ivre de brise un sylphe aux couleurs de drapeau,
Et ce grand port n�est plus qu�un noir d�barcad�re
Couru du vent glac� que sent venir ma peau!

Fermez-vous! Fermez-vous! Fen�tres offens�es!
Grands yeux qui redoutez la v�ritable nuit!
Et toi, de ces hauteurs d�astres ensemenc�es,
Accepte, f�cond� de myst�re et d�ennui,
Une maternit� muette de pens�es...








Anne.

Anne qui se m�lange au drap p�le et d�laisse
Des cheveux endormis sur ses yeux mal ouverts
Mire ses bras lointains tourn�s avec molesse
Sur la peau sans couleur du ventre d�couvert.

Elle vide, elle enfle d�ombre sa gorge lente,
Et comme un souvenir pressant ses propres chairs,
Une bouche bris�e et pleine d�eau br�lante
Roule le go�t immense et le reflet des mers.

Enfin d�sempar�e et libre d��tre fra�che,
La dormeuse d�serte aux touffes de couleur
Flotte sur son lit bl�me, et d�une l�vre s�che,
Tette dans la t�nebre un souffle amer de fleur.

Et sur le linge o� l�aube insensible se plisse,
Tombe, d�un bras de glace effleur� de carmin,
Toute une main d�faite et perdant le d�lice
� travers ses doigts nus denou�s de l�humain.

Au hasard! � jamais, dans le sommeil sans hommes
Pur des tristes �clairs de leurs embrassements,
Elle laisse rouler les grappes et les pommes
Puissantes, qui pendaient aux treilles d�ossements,

Qui riaient, dans leur ambre appelant les vendanges,
Et dont le nombre d�or de riches mouvements
Invoquait la vigueur et les gestes �tranges
Que pour tuer l�amour inventent les amants...

                        *

Sur toi, quand le regard de leurs �mes s��gare,
Leur coeur boulevers� change comme leurs voix,
Car les tendres appr�ts de leur festin barbare
H�tent les chiens ardents qui tremblent dans ces rois...

� peine effleurent-ils de doigts errants ta vie,
Tout leur sang les accable aussi lourd que la mer,
Et quelque violence aux ab�mes ravie
Jette ces blancs nageurs sur tes roches de chair...

R�cifs d�licieux, �le toute prochaine,
Terre tendre, promise aux d�mons apais�s,
L�amour t�aborde, arm� des regards de la haine,
Pour combattre dans l�ombre une hydre de baisers!

                        *

Ah, plus nue et qu�impr�gne une prochaine aurore,
Si l�or triste interroge un ti�de contour,
Rentre au plus pur de l�ombre o� le M�me s�ignore,
Et te fais un vain marbre �bauch� par le jour!

Laisse au p�le rayon ta l�vre viol�e
Mordre dans un sourire un long germe de pleur,
Masque d��me au sommeil � jamais immol�e
Sur qui la paix soudaine a surpris la douleur!

Plus jamais redorant tes ombres satin�es,
La vieille aux doigts de feu qui fendent les volets
Ne viendra t�arracher aux grasses matin�es
Et rendre au doux soleil tes joyeux bracelets...

Mais suave, de l�arbre ext�rieur, la palme
Vaporeuse remue au del� du remords,
Et dans le feu, parmi trois feuilles, l�oiseau calme
Commence le chant seul qui r�prime les morts.







Air De S�miramis.

� Camille Mauclair. 

D�s l�aube, chers rayons, mon front songe � vous ceindre!
� peine il se redresse, il voit d�un oeil qui dort
Sur le marbre absolu, le temps p�le se peindre,
L�heure sur moi descendre et cro�tre jusqu�� l�or...

                        *

... � Existe!... Sois enfin toi-m�me! dit l�Aurore,
� grande �me, il est temps que tu formes un corps!
H�te-toi de choisir un jour digne d��clore,
Parmi tant d�autres feux, les immortels tr�sors!

D�j�, contre la nuit lutte l��pre trompette!
Une l�vre vivante attaque l�air glac�;
L�or pur, de tout en tour, �clate et se r�p�te,
Rappelant tout l�espace aux splendeurs du pass�!

Remonte aux vrais regards! Tire-toi de tes ombres,
Et comme du nageur, dans le plein de la mer,
Le talon tout-puissant l�expulse des eaux sombres,
Toi, frappe au fond de l��tre! Interpelle ta chair,

Traverse sans retard ses invisibles trames,
�puise l�infini de l�effort impuissant,
Et d�barasse-toi d�un d�sordre de drames
Qu�engendrent sur ton lit les monstres de ton sang!

J�accours de l�Orient suffire � ton caprice!
Et je te viens offrir mes plus purs aliments;
Que d�espcae et de vent ta flamme se nourrisse!
Viens te joindee � l��clat de mes pressentiments! �

                        *

-Je r�ponds!... Je surgis de ma profonde absence!
Mon coeur m�arrache aux morts que fr�lait mon sommeil,
Et vers mon but, grand aigle �clatant de puissance,
Il m�emporte!... Je vole au-devant du soleil!

Je ne prends qu�une rose et fuis... La belle fl�che
Au flanc!... Ma t�te enfante une foule de pas...
Ils courent vers ma tour favorite, o� ma fra�che
Altitude m�appelle, et je lui tends les bras!

Monte, � S�miramis, ma�tresse d�une spire
Qui d�un coeur sans amour s��lance au seul honneur!
Ton oeil imp�rial a soif du grand empire
� qui ton spectre dur fait sentir le bonheur...

Ose l�ab�me! Passe un dernier pont de roses!
Je t�approche, p�ril! Orgueil plus irrit�!
Ces fourmis sont � moi! Ces villes sont mes choses,
Ces chemins sont les traits de mon autorit�!

C�est une vaste peau de fauve que mon royaume!
J�ai tu� le lion qui portait cette peau;
Mais encor le fumet du f�roce fant�me
Flotte charg� de mort, et garde mon troupeau!

Enfin, j�offre au soleil le secret de mes charmes!
Jamais il n�a dor� de seuil si gracieux!
De ma fragilit� je go�te les alarmes
Entre le double appel de la terre et des cieux.

Repas de ma puissance, intelligible orgie,
Quel parvis vaporeux de toits et de for�ts
Place aux pieds de la pure et divine vigie,
Ce calme �loignement d��v�nements secrets!

L��me enfin sur ce fa�te a trouv� ses demeures!
� de quelle grandeur, elle tient sa grandeur
Quand mon coeur soulev� d�ailes int�rieurs
Ouvre au ciel en moi-m�me une autre profondeur!

Anxieuse d�azur, de gloire consum�e,
Poitrine, gouffre d�ombre aux narines de chair,
Aspire cet encens d��mes et de fum�e
Qui monte d�une ville analogue � la mer!

Soleil, soleil, regarde en toi rire mes ruches!
L�intense et sans repos Babylone bruit,
Toute rumeurs de chars, clairons, cha�nes de cruches
Et plaintes de la pierre au mortel qui construit.

Qu�ils flattent mon d�sir de temples implacables,
Les sons aigus de scie et les cris des ciseaux,
Et ces g�missements de marbres et de c�bles
Qui peuplent l�air vivant de structure et d�oiseaux!

Je vois mon temple neuf na�tre parmi les mondes,
Et mon voeu prendre place au s�jour des destins;
Il semble de soi-m�me au ciel monter par ondes
Sous le bouillonnement des actes indistincts.

Peuple stupide, � qui ma puissance m�encha�ne,
H�las! mon orgueil m�me a besoin de tes bras!
Et que ferait mon coeur s�il n�aimait cette haine
Dont l�innombrable t�te est si douce � mes pas?

Plate, elle me murmure une musique telle
Que le calme de l�onde en fait de sa fureur,
Quand elle se rapaise aux pieds d�une mortelle
Mais qu�elle se r�serve un retour de terreur.

En vain j�entends monter contre ma face auguste
Ce murmure de crainte et de f�rocit�:
� l�image des dieux la grande �me est injuste
Tant elle s�appareille � la n�cessit�!

Des douceurs de l�amour quoique parfois touch�e,
Pourtant nulle tendresse et nuls renoncements
Ne me laissent captive et victime couch�e
Dans les puissants liens du sommeil des amants!

Baisers, baves d�amour, basses b�atitudes,
� mouvements marins des amants confondus,
Mon coeur m�a conseill� de telles solitudes,
Et j�ai plac� si haut mes jardins suspendus

Que mes supr�mes fleurs n�attendent que la foudre
Et qu�en d�pit des pleurs des amants les plus beaux,
� mes roses, la main qui touche tombe en poudre:
Mes plus doux souvenirs b�tissent des tombeaux!

Qu�ils sont doux � mon coeur les temples qu�il enfante
Quand tir� lentement du songe de mes seins,
Je vois un monument de masse triomphante
Joindre dans mes regards l�ombre de mes desseins!

Battez, cymbales d�or, mamelles cadenc�es,
Et roses palpitant sur ma pure paroi!
Que je m��vanouisse en mes vastes pens�es,
Sage S�miramis, enchanteresse et roi!









L�Amateur de po�mes.


Si je regarde tout � coup n�a v�ritable pens�e, je ne me console pas de devoir 
subir cette parole int�rieure sans personne et sans origine; ces figures 
�ph�m�res; et cette infinit� d�entreprises interrompues par leur propre 
facilit�, qui se transforment l�une dans l�autre, sans que rien ne change avec 
elles. Incoh�rente sans le para�tre, nulle instantan�ment comme elle est 
spontan�e, la pens�e, par sa nature, manque de style.

Mais je n�ai pas tous les jours la puissance de proposer � mon attention 
quelques �tres n�cessaires, ni de feindre les obstacles spirituels qui 
formeraient une apparence de commencement, de pl�nitude et de fin, au lieu de 
mon insupportable fuite.

Un po�me est une dur�e, pendant laquelle, lecteur, je respire une loi qui fut 
pr�par�e; je donne mon souffle et les machines de ma voix; ou seulement leur 
pouvoir, qui se concilie avec le silence.

Je m�abandonne � l�adorable allure: lire, vivre o� m�nent les mots. Leur 
apparition est �crite. Leurs sonorit�s concert�es. Leur �branlement se compose, 
d�apr�s une m�ditation ant�rieure, et ils se pr�cipiteront en groupes 
magnifiques ou purs, dans la r�sonance. M�me mes �tonnements sont assur�s: ils 
sont cach�s d�avance, et font partie du nombre.

Mu par l��criture fatale, et ai le m�tre toujours futur encha�ne sans retour ma 
m�moire, je ressens chaque parole dans toute sa force, pour l�avoir ind�finiment 
attendue. Cette mesure qui me transporte et que je colore, me garde du vrai et 
du faux. Ni le doute ne me divise, ni la raison ne me travaille. Nul hasard, 
mais une chance extraordinaire se fortifie. Je trouve sans effort le langage de 
ce bonheur; et je pense par artifice, une pens�e toute certaine, 
merveilleusement pr�voyante, -aux lacunes calcul�es, sans t�n�bres 
involontaires, dont le mouvement me commande et la quantit� me comble: une 
pens�e singuli�rement achev�e.




Source: http://www.poesies.net





