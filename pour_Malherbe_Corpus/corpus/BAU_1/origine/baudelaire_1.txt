Reminder of your request:

Downloading format: : Text

View 1 to 42 on 42

Number of pages: 42

Full notice

Title : Complément aux "Fleurs du mal" de Charles Baudelaire (Édition Michel Lévy, 1869)

Author : Baudelaire, Charles (1821-1867). Auteur du texte

Publisher : chez tous les libraires (Bruxelles)

Publication date : 1869

Type : text

Type : monographie imprimée

Language : french

Language : language.label.français

Format : 1 vol. (36 p.) ; 20 cm

Format : Nombre total de vues : 42

Description : Contient une table des matières

Description : Avec mode texte

Rights : public domain

Identifier : ark:/12148/bpt6k58314273

Source : Médiathèque de la communauté urbaine d'Alençon, 2010-43243

Relationship : http://catalogue.bnf.fr/ark:/12148/cb30066465r

Provenance : Bibliothèque nationale de France

Date of online availability : 11/05/2010

The text displayed may contain some errors. The text of this document has been generated automatically by an optical character recognition (OCR) program. The estimated recognition rate for this document is 98 %.
For more information on OCR

COMPLEMENT

AUX

FLEURS DU MAL

DE

CHARLES BAUDELAIRE

(ÉDITION MICHEL I.ÉVY, 186!))

BRUXELLES

CHEZ TOUS LES LIBRAIRES

1869

COMPLEMENT

AUX

FLEURS DU MAL

Bruxelles. —Imprimerie de J. H. BRIARD, rue des Minimes, 51.

COMPLÉMENT

AUX

FLEURS DU MAL

DE

CHARLES BAUDELAIRE

(ÉDITION MICHEL LÉVY, 1869)

BRUXELLES

CHEZ TOUS LES LIBRAIRES 1869

PIÈCES RETIRÉES

DE LA PREMIÈRE ÉDITION

DES

FLEURS.DU MAL

i LESBOS (1)

Mère des jeux latins et des voluptés grecques, Lesbos, où les baisers, languissants ou joyeux, Chauds comme les soleils, frais comme les pastèques, Font l'ornement des nuits et des jours glorieux; Mère des jeux latins et des voluptés grecques !

(1) Cette pièce et les cinq suivantes, condamnées en 1857 par le tribunal correctionnel, ne peuvent pas être reproduites dans le recueil des Fleurs du Mal.

(Noie de l'éditeur.) .1

LES FLEURS DU MAL

Lesbos, où les baisers sont comme les cascades Qui se jettent sans peur dans les gouffres sans fonds, Et courent, sanglotant et gloussant par saccades, Orageux et secrets, fourmillants et profonds; Lesbos, où les baisers sont comme les cascades !

Lesbos, où les Phrynés l'une l'autre s'attirent, Où jamais un soupir ne resta sans écho, A l'égal de Paphos les étoiles t'admirent, Et Vénus à bon droit peut jalouser Sapho! Lesbos, où les Phrynés l'une l'autre s'attirent!

Lesbos, terre des nuits chaudes et langoureuses, Qui font qu'à leurs miroirs, stérile volupté ! Les filles aux yeux creux, de leurs corps amoureuses, Caressent les fruits mûrs de leur nubilité; Lesbos, terre des nuits chaudes et langoureuses !

Laisse du vieux Platon se froncer l'oeil austère; Tu tires ton pardon de l'excès des baisers, Reine du doux empire, aimable et noble terre, Et des raffinements toujours inépuisés. Laisse du vieux Platon se froncer l'oeil austère.

Tu tires ton pardon de l'éternel martyre Infligé sans relâche aux coeurs ambitieux, Qu'attire loin de nous le radieux sourire Entrevu vaguement au bord des autres cieux! Tu tires ton pardon de l'éternel martyre !

LESBOS

Qui des dieux osera, Lesbos, être ton juge Et condamner ton front pâli dans les travaux, Si ses balances d'or n'ont pesé le déluge De larmes, qu'à la mer ont versé tes ruisseaux? Qui des dieux osera, Lesbos, être ton juge!

Que nous veulent les lois du juste et de l'injuste? Vierges au coeur sublime, honneur de l'Archipel, Votre religion comme une autre est auguste, Et l'Amour se rira de l'Enfer et du Ciel ! Que nous veulent les lois du juste et de l'injuste !..

Car Lesbos entre tous m'a choisi sur la terre Pour chanter le secret de ses vierges en fleurs, Et je fus dès l'enfance admis au noir mystère Des rires effrénés mêlés aux sombres pleurs; Car Lesbos entre tous m'a choisi sur la terre.

Et depuis lors je veille au sommet de Leucate, Comme une sentinelle à l'oeil perçant et sûr, Qui guette nuit et jour brick, tartane ou frégate, Dont les formes au loin frissonnent dans l'azur ; Et depuis lors je veille au sommet de Leucate,

Pour savoir si la mer est indulgente et bonne ; Et parmi les sanglots dont le roc retentit, Un soir ramènera vers Lesbos qui pardonne Le cadavre adoré de Sapho qui partit Pour savoir si la mer est indulgente et bonne !

LES FLEURS DU MAL

De la mâle Sapho, l'amante et le poète,

Plus belle que Vénus par ses mornes pâleurs!

— L'oeil d'azur est vaincu par l'oeil noir que tacheté Le cercle ténébreux tracé par les douleurs

De la mâle Sapho, l'amante et le poète !

— Plus belle que Vénus se dressant sur le monde Et versant les trésors de sa sérénité

Et le rayonnement de sa jeunesse blonde

Sur le vieil Océan de sa fille enchanté ;

Plus belle que Vénus se dressant sur le monde !

— De Sapho qui mourut le jour de son blasphème, Quand, insultant le culte et le rite inventé,

Elle fit son beau corps la pâture suprême

D'un brutal dont l'orgueil punit l'impiété

De Sapho qui mourut le jour de son blasphème.

Et c'est depuis ce temps que Lesbos se lamente, Et malgré les honneurs que lui rend l'univers, S'enivre chaque nuit du cri de la tourmente Que poussent vers les cieux ses rivages déserts ! Et c'est depuis ce temps que Lesbos se lamente !

FEMMES DAMNEES

II

FEMMES DAMNÉES

DELPHINE ET H1PP0LYTE

A la pâle clarté des lampes languissantes, Sur de profonds coussins tout imprégnés d'odeur, Hippolyte rêvait aux caresses puissantes Qui levaient le rideau de sa jeune candeur.

Elle cherchait, d'un oeil troublé par la tempête, De sa naïveté le ciel déjà lointain, Ainsi qu'un voyageur qui retourne la tête Vers les horizons bleus dépassés le matin.

De ses yeux, amortis les paresseuses larmes, L'air brisé, la stupeur, la morne volupté, Ses bras vaincus, jetés comme de vaines armes, Tout servait, tout parait sa fragile beauté.

6 LES FLEURS DU MAL

Étendue à ses pieds, calme et pleine de joie, Delphine la couvait avec des yeux ardents, Comme un animal fort qui surveille une proie, Après l'avoir d'abord marquée avec les dents.

Beauté forte à genoux devant la beauté frêle, Superbe, elle humait voluptueusement Le vin de son triomphe, et s'allongeait vers elle, Comme pour recueillir un doux remercîment.

Elle cherchait dans l'oeil de sa pâle victime

Le cantique muet que chante le plaisir,

Et cette gratitude infinie et sublime

Qui sort de la paupière ainsi qu'un long soupir.

— « Hippolyte, cher coeur, que dis-tu de ces choses? Comprends-tu maintenant qu'il ne faut pas offrir L'holocauste sacré de tes premières roses Aux souffles violents qui pourraient les flétrir ?

Mes baisers sont légers comme ces éphémères Qui caressent le soir les grands lacs transparents, Et ceux de ton amant creuseront leurs ornières Comme des chariots ou des socs déchirants ;

Ils passeront sur toi comme un lourd attelage De chevaux et de boeufs aux sabots sans pitié... Hippolyte, ô ma soeur! tourne donc ton visage, Toi, mon âme et mon coeur, mon tout et ma moitié,

FEMMES DAMNEES 7

Tourne vers moi tes yeux pleins d'azur et d'étoiles ! Pour un de ces regards charmants, baume divin, Des plaisirs plus obscurs je lèverai les voiles, Et je t'endormirai dans un rêve sans fin ! »

Mais Hippolyte alors, levant sa jeune tête :

— « Je ne suis point ingrate et ne me repens pas, Ma Delphine, je souffre et je suis inquiète, Comme après un nocturne et terrible repas.

Je sens fondre sur moi de lourdes épouvantes Et de noirs bataillons de fantômes épars, Qui veulent me conduire en des routes mouvantes Qu'un horizon sanglant ferme de toutes parts.

Avons-nous donc commis une action étrange? Explique, si tu peux, mon trouble et mon effroi : Je frissonne de peur quand tu me dis: « Mon ange! » Et cependant je sens ma bouche aller vers toi.

Ne me regarde pas ainsi, toi, ma pensée, Toi que j'aime à jamais, ma soeur d'élection, Quand même tu serais une embûche dressée, Et le commencement de ma perdition !»

Delphine secouant sa crinière tragique, Et comme trépignant sur le trépied de fer, L'oeil fatal, répondit d'une voix despotique:

— « Qui donc devant l'amour ose parler d'enfer?

8 LES FLEURS DU MAL

Maudit soit à jamais le rêveur inutile Qui voulut le premier, dans sa stupidité, S'éprenant d'un problème insoluble et stérile, ' Aux choses de l'amour mêler l'honnêteté !

Celui qui veut unir dans un accord mystique L'ombre avec la chaleur, la nuit avec le jour, Ne chauffera jamais son corps paralytique A ce rouge soleil que l'on nomme l'amour!

Va, si tu veux, chercher un fiancé stupide; Cours offrir un corps vierge à ses cruels baisers ; Et, pleine de remords et d'horreur, et livide, Tu me rapporteras tes seins stigmatisés...

On ne peut ici-bas contenter qu'un seul maître ! » Mais l'enfant, épanchant une immense douleur, Cria soudain : — « Je sens s'élargir dans mon être Un abîme béant; cet abîme est mon coeur!

Brûlant comme un volcan, profond comme le vide,

Rien ne rassasiera ce monstre gémissant,

Et ne rafraîchira la soif de l'Euménide

Qui, la torche à la main, le brûle "jusqu'au sang!

Que nos rideaux fermés nous séparent du monde, , Et que la lassitude amène le repos :

Je veux m'anéantir dans ta gorge profonde,

Et trouver sur ton sein la fraîcheur des tombeaux ! »

FEMMES DAMNÉES 9

— Descendez, descendez, lamentables victimes, Descendez le chemin de l'enfer éternel ! Plongez au plus profond du gouffre, où tous les crimes, Flagellés par un vent qui ne vient pas du ciel,

Bouillonnent pêle-mêle avec un bruit d'orage. Ombres folles ! courez au but de vos désirs ; Jamais vous ne pourrez assouvir votre rage, Et votre châtiment naîtra de vos plaisirs.

Jamais un rayon frais n'éclaira vos cavernes ; Par les fentes des murs des miasmes fiévreux Filtrent en s'enflammant ainsi que des lanternes, Et pénètrent vos corps de leurs parfums affreux.

L'âpre stérilité de votre jouissance

Altère votre soif et roidit votre peau,

Et lèvent furibond de la concupiscence

Fait claquer votre chair ainsi qu'un vieux drapeau.

Loin des peuples vivants, errantes, condamnées, A travers les déserts courez- comme les loups ; Faites votre destin, âmes désordonnées, Et fuyez l'infini que vous portez en vous !

10 LES FLEURS DU MAL

III

LE LETHE

Viens sur mon coeur, âme cruelle et sourde, Tigre adoré, monstre aux airs.indolents; Je veux longtemps plonger mes doigts tremblants Dans l'épaisseur de ta crinière lourde;

Dans tes jupons remplis de ton parfum Ensevelir ma tête endolorie, Et respirer, comme une fleur flétrie, Le doux relant de mon amour défunt.

Je veux dormir ! dormir plutôt que vivre ! Dans un sommeil aussi doux que la mort, J'étalerai mes baisers sans remord Sur ton beau corps poli comme le cuivre.

LE LÉTHÉ 11

Pour engloutir mes sanglots apaisés Rien ne me vaut l'abîme de ta couche ; L'oubli puissant habite sur ta bouche, Et le Léthé coule dans tes baisers.

A mon destin, désormais mon délice, J'obéirai comme un prédestiné ; Martyr docile, innocent condamné, Dont sa ferveur attise le supplice.

Je sucerai, pour noyer ma rancoeur, Le népenthès et la bonne ciguë Aux bouts charmants de cette gorge aiguë Qui n'a jamais emprisonné de coeur.

12 LES FLEURS DU MAL

IV

A CELLE QUI EST TROP GAIE

Ta tête, ton geste, ton air

Sont beaux comme un beau paysage;

Le rire joue en ton visage

Comme un vent frais dans un ciel clair.

Le passant chagrin que tu frôles Est ébloui par la santé Qui jaillit comme une clarté De tes bras et de tes épaules.

Les retentissantes couleurs Dont tu parsèmes tes toilettes Jettent dans l'esprit des poètes L'image d'un ballet de.fleurs.

Ces robes folles sont l'emblème

De ton esprit bariolé ;

Folle dont je suis affolé,

Je te hais autant que je t'aime !

A CELLE QUI EST TROP GAIE 13

Quelquefois dans un beau jardin Où je traînais mon atonie, J'ai senti, comme une ironie, Le soleil déchirer mon sein ;

Et le printemps et la verdure Ont tant humilié mon coeur, Que j'ai puni sur une fleur L'insolence de la Nature.

Ainsi je voudrais, une nuit, Quand l'heure des voluptés sonne, Vers les trésors de ta personne, Comme un lâche, ramper sans bruit ;

Pour châtier ta chair joyeuse, Pour meurtrir ton sein pardonné, Et faire à ton flanc étonné Une blessure large et creuse,

Et, vertigineuse douceur! A travers ces lèvres nouvelles Plus éclatantes et plus belles, T'infuser mon venin, ma soeur !

14 LES FLEURS DU MAL

V

LES BIJOUX

La très-chère était nue, et, connaissant mon coeur, Elle n'avait gardé que ses bijoux sonores, Dont le riche attirail lui donnait l'air vainqueur Qu'ont dans leurs jours heureux les esclaves des Mores.

Quand il jette en dansant son bruit vif et moqueur, Ce monde rayonnant de métal et de pierre Me ravit en extase, et j'aime avec fureur Les choses où le son se mêle à la lumière.

Elle était donc couchée et se laissait aimer, Et du haut du divan elle souriait d'aise A mon amour profond et doux comme la mer, Qui vers elle montait comme vers sa falaise.

- LES BIJOUX ' 15

Les yeux fixés sur moi, comme un tigre dompté, D'un air vague et rêveur elle essayait des poses, Et la candeur unie à la lubricité Donnait un charme neuf à ses métamorphoses.

Et son bras et sa jambe, et sa cuisse et ses reins, Polis comme de l'huile, onduleux comme un cygne, Passaient devant mes yeux clairvoyants et sereins; Et son ventre et ses seins, ces grappes de ma vigne,

S'avançaient, plus câlins que les anges du mal, Pour troubler le repos où mon âme était mise, Et pour la déranger du rocher de cristal Où, calme et solitaire, elle s'était assise.

Je croyais voir unis par un nouveau dessin

Les hanches de l'Antiope au buste d'un imberbe,

Tant sa taille faisait ressortir son bassin !

Sur ce teint fauve et brun le fard était superbe !

— Et la lampe s'étant résignée à mourir, Comme le foyer seul illuminait la chambre, Chaque fois qu'il poussait un flamboyant soupir, Il inondait de sang cette peau couleur d'ambre.

16 LES FLEURS DU MAL

VI

LES MÉTAMORPHOSES DU VAMPIRE

La femme cependant, de sa bouche de fraise, En se tordant ainsi qu'un serpent sur la braise, Et pétrissant ses seins sur le fer de son buse, Laissait couler ces mots tout imprégnés de musc :

— « Moi, j'ai la lèvre humide, et je sais la science De perdre au fond d'un lit l'antique conscience ; Je sèche tous les pleurs sur mes seins triomphants, Et fais rire les vieux du rire des enfants.

Je remplace, pour qui me voit nue et sans voiles, La lune, le soleil, le ciel et "les étoiles! Je suis, mon cher savant, si docte aux voluptés, Lorsque j'étouffe un homme en mes bras redoutés,

LES MÉTAMORPHOSES DU VAMPIRE 17

Ou lorsque j'abandonne aux morsures mon buste, Timide et libertine, et fragile et robuste, Que sur ces matelas qui se pâment d'émoi, Les anges impuissants se damneraient pour moi ! ^

— Quand elle eut de mes os sucé toute la moelle, Et que languissamment je me tournai vers elle Pour lui rendre un baiser d'amour, je ne vis plus Qu'une outre aux flancs gluants, toute pleine de pus !

Je fermai les deux yeux, dans ma froide épouvante, Et quand je les rouvris à la clarté vivante, A mes côtés, au lieu du mannequin puissant Qui semblait avoir fait provision de sang,

Tremblaient confusément des débris de squelette, Qui d'eux-mêmes rendaient le cri d'une girouette Ou d'une enseigne, au bout d'une tringle de fer, Que balance le vent pendant les nuits d'hiver.

GALANTERIES

VII

LES PROMESSES D'UN VISAGE

J'aime, ô pâle beauté, tes sourcils surbaissés,

D'où semblent couler des ténèbres ; Tes yeux, quoique très-noirs, m'inspirent des pensers . Qui ne sont pas du tout funèbres ;

Tes yeux, qui sont d'accord avec tes noirs cheveux,

Avec ta crinière élastique. Tes yeux, languissamment, me disent : « Si tu veux,

Amant de la muse plastique,

20 LES FLEURS DU MAL

Suivre l'espoir qu'en toi nous avons excité

Et tous les goûts que tu professes, Tu pourras constater notre véracité

Depuis le nombril jusqu'aux fesses.

Tu trouveras au bout de deux beaux seins bien lourds,

Deux larges médailles de bronze, Et sous un ventre uni, doux comme du velours,

Bistré comme la peau d'un bonze,

Une riche toison qui, vraiment, est la soeur

De cette énorme chevelure Souple et frisée, et qui t'égale en épaisseur,

Nuit sans étoiles, nuit obscure! »

LE MONSTRE 21

VIII

LE MONSTRE

ou

LE PARANYMPHE D UNE NYMPHE MACABRE

I

Tu n'es certes pas, ma très-chère, Ce que Veuillot nomme un tendron. Le jeu, l'amour, la bonne chère Bouillonnent en toi, vieux chaudron ! Tu n'es plus fraîche, ma très-chère,

Ma vieille infante ! Et cependant Tes caravanes insensées T'ont donné ce lustre abondant Des choses qui sont très-usées, Mais qui séduisent cependant.

22 LES FLEURS DU MAL

Je ne trouve pas monotone La verdeur de tes quarante ans ; Je préfère tes fruits, automne, Aux fleurs banales du printemps ! Non, tu n'es jamais monotone.

Ta carcasse a des agréments Et des grâces particulières ; Je trouve d'étranges piments Dans le creux de tes deux salières ; Ta carcasse a des agréments !

Nargue des amants ridicules Du melon et du giraûmont! Je préfères tes clavicules A celles du roi Salomon, Et je plains ces gens ridicules !

Tes cheveux, comme un casque bleu, Ombragent ton front de guerrière, Qui ne pense et rougit que peu, Et puis se sauvent par derrière, Comme les crins d'un casque bleu.

Tes yeux qui semblent de la boue Où scintille quelque fanal, Ravivés au fard de ta joue, Lancent un éclair infernal ! Tes yeux sont noirs comme la boue !

LE MONSTRE 23

Par sa luxure et son dédain

Ta lèvre amère nous provoque ;

Cette lèvre, c'est un Éden

Qui nous attire et qui nous choque.

Quelle luxure ! et quel dédain !

Ta jambe musculeuse et sèche Sait gravir au haut des volcans, Et malgré la neige et la dèche, Danser les plus fougueux cancans; Ta jambe est musculeuse et sèche.

Ta peau brûlante et sans douceur, Comme celle des vieux gendarmes, Ne connaît pas plus la sueur Que ton oeil ne connaît les larmes, (Et pourtant elle a sa douceur!)

II

Sotte, tu t'en vas droit au diable !

Volontiers j'irais avec toi,

Si cette vitesse effroyable

Ne me causait pas quelque émoi.

Va-t-en donc, toute seule, au diable !

24 LES FLEURS DU MAL

Mon rein, mon poumon, mon jarret Ne me laissent plus rendre hommage A ce seigneur, comme il faudrait: « Hélas ! c'est vraiment bien dommage ! » Disent mon rein et mon jarret.

Oh! très-sincèrement je souffre De ne pas aller aux sabbats, Pour voir, quand il pète du souffre, Comment tu lui baises son cas ! Oh! très-sincèrement je souffre.

Je suis diablement affligé De ne pas être ta torchère, Et de te demander congé, Flambeau d'enfer ! Juge, ma chère, Combien je dois être affligé,

Puisque depuis longtemps je t'aime, Étant très-logique ! En effet, Voulant du mal chercher la crème Et n'aimer qu'un monstre parfait, Vraiment oui! vieux monstre, je t'aime!

BOUFFONNERIES

IX SUR LES DÉBUTS DE MIIe AMINA BOSCHETTI

AU THEATRE DE LA MONNAIE, A BRUXELLES

1864

Amina bondit, — fuit, puis voltige et sourit ;

Le Welche dit: « Tout ça, pour moi, c'est du prâcrit;

Je ne connais, en fait de nymphes bocagères,

Que celles de Mont,agne-aux-Herbes-Potagères. »

Du bout de son pied fin et de son oeil qui rit, Amina verse à flots le délire et l'esprit; Le Welche dit : « Fuyez, délices mensongères ! Mon épouse n'a pas ces allures légères. »

26 LES FLEURS DU MAL

Vous ignorez, sylphide au regard triomphant, Qui voulez enseigner la walse à l'éléphant, Au hibou la gaîté, le rire à la cigogne,

Que sur la grâce en feu le Welche dit: « Haro ! » Et que le doux Bacchus lui versant le bourgogne, Le monstre répondrait : « J'aime mieux le faro ! »

A PROPOS D'UN IMPORTUN 27

A M. EUGENE FROMENTIN

A PROPOS D'UN IMPORTUN

QUI SE DISAIT SON AMI

Il me dit qu'il était très-riche, Mais qu'il craignait le choléra ;

— Que de son or il était chiche, Mais qu'il goûtait fort l'Opéra;

— Qu'il raffolait de la nature, Ayant connu monsieur Corot;

— Qu'il n'avait pas encor voiture, Mais que cela viendrait bientôt;

28 LES FLEURS DU MAL

— Qu'il aimait le marbre et la brique, Les bois noirs et les bois dorés ;

— Qu'il possédait dans sa fabrique Trois contre-maîtres décorés ;

— Qu'il avait, sans compter le reste, Vingt mille actions sur le Nord;

— Qu'il avait trouvé, pour un zeste, Des encadrements d'Oppenord ;

— Qu'il donnerait (fût-ce à Luzarches!) Dans le bric-à-brac jusqu'au cou,

Et qu'au Marché des Patriarches Il avait fait plus d'un bon coup ;

— Qu'il n'aimait pas beaucoup sa femme, Ni sa mère; — mais qu'il croyait

A l'immortalité de l'âme, Et qu'il avait lu Niboyet !

— Qu'il penchait pour l'amour physique, Et qu'à Rome, séjour d'ennui,

Une femme, d'ailleurs phtisique, Était morte d'amour pour lui.

— Pendant trois heures et demie, Ce bavard, venu de Tournai,

M'a dégoisé toute sa vie : J'en ai le cerveau consterné.

A PROPOS D'UN IMPORTUN 29

S'il fallait décrire ma peine,

Ce serait à n'en plus finir ;

Je me disais, domptant ma haine:

« Au moins, si je pouvais dormir ! »

Comme un qui n'est pas à son aise, Et qui n'ose pas s'en aller, Je frottais de mon cul ma chaise, Rêvant de me faire empaler.

Ce monstre se nomme Bastogne ; Il fuyait devant le fléau. Moi, je fuirai jusqu'en Gascogne, Ou j'irai me jeter à l'eau,

Si dans ce Paris qu'il redoute,

Quand chacun sera retourné, "■

Je trouve encore sur ma route

Ce fléau natif de Tournai !

Bruxelles, 1865.

30 LES FLEURS DU MAL

X UN CABARET FOLATRE

SUR LA ROUTE DE BRUXELLES A UCCLE

Vous qui raffolez des squelettes Et des emblèmes détestés, Pour épicer les voluptés, « Fût-ce de simples omelettes ! »

Vieux Pharaon, ô Monselet ! Devant cette enseigne imprévue, J'ai rêvé de vous : A la vue Du cimetière, estaminet!

FIN

TABLE

PIECES CONDAMNEES

Lesbos 1

Femmes damnées 5

Le Lethé 40

A celle qui est trop gaie 12

Les bijoux 14

Les métamorphoses du vampire 16

GALANTERIES

Les promesses d'un visage 19

Le Monstre 21

BOUFFONNERIES

Sur les débuts de M1Ie Boschetti 25

A propos d'un importun 27

Un cabaret folâtre 30
