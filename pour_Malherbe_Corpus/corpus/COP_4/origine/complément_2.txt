<l>Il s'est laissé nommer, dans sa ville, chanoine. </l>
<l>Là, depuis son retour, vite le bon abbé </l>
<l>Dans l'ancienne habitude est de nouveau tombé </l>
<l>Et d'un logis bien cher a retrouvé la route. </l>
<l>Certes, quand il y vient lentement, il se doute </l>
<l>Qu'on entend de très loin son pas sur le pavé </l>
<l>Et que, près du rideau foiblement soulevé, </l>
<l>Un regard amical le voit venir et guette. </l>
<l>Mais il n'i pas encore osé lever la tête </l>
<l>Depuis quatre ans qu'il fait tous les jours ce chemin; </l>
<l>Et quand il est entré, son missel à la main, </l>
<l>Dans le salon étroit et suranné de celle </l>
<l>A qui, par vieil usage, il dit « la demoiselle, » </l>
<l>Toutes les fois, il feint de croire à l'air surpris </l>
<l>Qu'à son aspect, soudain, la douce fille a pris, </l>
<l>Et qui la trouble au point que sa voix en hésite </l>
<l>Dans son remercîment de la bonne visite. </l>
<l>En deuil, ayant gardé ses beaux yeux clairs et doux, </l>
<l>Et délicatement flattant, sur ses genoux. </l>
<l>Le pelage soyeux de sa chatte endormie, </l>
<l>Telle, chaque matin, il voit sa vieille amie </l>
<l>Devant laquelle il reste une grande heure assis. </l>
<l>Lui faisant, d'un ton bas, quelques simples récits, </l>
<l>Sans que jamais en eux un geste, un rien dénote </l>
<l>Plus qu'une affection de vieux prêtre à dévote; </l>
<l>Et lorsque du sujet honnête et puéril </l>
<l>L'entretien a suivi tout doucement le fil, </l>
<l>Sans un mot qui s'émeut, sans cordiale étreinte. </l>
<l>Comme si la mémoire en eux était éteinte </l>
<l>Du sacrifice fait jadis à leur devoir. </l>
<l>Ils échangent enfin un très faible : « Au revoir. »</l>
<l>— Pourtant il faut qu'il lutte et qu'elle se contienne, </l>
<l>Car, même redoutant l'effusion chrétienne </l>
<l>Où l'on doit se nommer un instant frère et sœur, </l>
<l>Elle n'a jamais pris l'abbé pour confesseur. </l>



