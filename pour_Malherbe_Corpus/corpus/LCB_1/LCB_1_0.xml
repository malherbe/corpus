<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="LCB">
					<name>
						<forename>Jean-Baptiste</forename>
						<surname>LACOMBE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LCB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
						<author>JEAN-BAPTISTE LACOMBE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6506752d.r=J.%20B.%20LACOMBE%2C%20%20%20JE%20VOUS%20SALUE%20GUILLAUME%20LE%20VAINQUEUR%20PARIS?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
								<author>JEAN-BAPTISTE LACOMBE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem">
				<head type="main">JE VOUS SALUE GUILLAUME LE VAINQUEUR PARIS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>AveCesar.</quote>
							<bibl>
								Cette facétie fut prononcée par le grand-duc de Bade,<lb/>
								gendre de Guillaume de Prusse, au palais de Versailles,<lb/>
								le Ier janvier 1871.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg>
					<l>Lorsque, tout frissonnant des sueurs de la honte,</l>
					<l>Loin des bruits, anxieux, sans témoins, on affronte</l>
					<l>Le grand livre où Tacite enferma les Césars… ;</l>
					<l>Lorsqu’on a sous les yeux tous les lambeaux épars</l>
					<l>Qui forment la légende, et que, l’âme froissée,</l>
					<l>On peut, résolûment, la manche retroussée,</l>
					<l>Fouiller l’entassement des noms accumulés</l>
					<l>De tous les malfaiteurs des siècles écoulés… ;</l>
					<l>Quand on peut soupeser les feuillets de l’histoire,</l>
					<l>Pour savoir de quels maux se compose la gloire…</l>
					<l>Guillaume, on cherche en vain, parmi tous ces héros</l>
					<l>Dont la lime du temps n’a pu mordre les os,</l>
					<l>Celui qui, plus que toi, mettant crime sur crimes,</l>
					<l>Dans nos champs désolés entassa de victimes…</l>
				</lg>
				<lg>
					<l>Vainement j’interroge, allant jusqu’à Néron,</l>
					<l>Pour voir s’il en est un. – L’histoire me dit : « Non ! »</l>
					<l>De Cartouche à Mandrin, jusques à Charlemagne,</l>
					<l>Sur le velours du trône et sur les bancs du bagne,</l>
					<l>Depuis Caligula, Winceslas, Sigismond,</l>
					<l>D’autres que l’on retrouve en creusant plus profond,</l>
					<l>Il n’est pas un bandit, pas un porte-ferraille,</l>
					<l>Qui, comme criminel, se mesure à ta taille !</l>
				</lg>
				<lg>
					<l>Et c’est toi, tigre-roi, sinistre maraudeur,</l>
					<l>Qu’on ose proclamer Guillaume le Vainqueur ?…</l>
					<l>C’est à devenir fou ! – Que ton gendre de Bade,</l>
					<l>L’égorgeur de Strasbourg, te donnant l’accolade</l>
					<l>Au nom des souverains, princes et hauts barons</l>
					<l>Que déjà nos enfants appellent hauts larrons,</l>
					<l>S’en vienne, maîtrisant un éclat de fou rire,</l>
					<l>Te taper sur le ventre et te donner du « Sire »,</l>
					<l>Ces choses-là se font peut-être en carnaval ;</l>
					<l>Mais n’être que Guillaume et se croire Annibal,</l>
					<l>S’amuser au César ! essayer la couronne</l>
					<l>Que porta Charles-Quint !… Sur l’honneur, je m’étonne</l>
					<l>Que ton bouffon Bismarck, ce valet sans pudeur,</l>
					<l>Ne t’ait pas dit, ô roi ! qu’un sceptre d’empereur</l>
					<l>Est un roseau fragile en le siècle où nous sommes…</l>
					<l>Siècle où Dieu n’est plus là pour signer vos diplômes… ;</l>
					<l>Qu’il ne t’ait pas montré l’homme de Friedland,</l>
					<l>Et celui qu’on a pris sous les murs de Sedan :</l>
					<l>L’un, couvert du manteau que brode la victoire ;</l>
					<l>L’autre sifflé, hué, conspué par l’histoire…</l>
					<l>L’un, du bruit de ses pas étonnant l’univers,</l>
					<l>Pendant vingt ans luttant, faisant face aux revers,</l>
					<l>A la tête des siens défiant la mitraille,</l>
					<l>Se couchant tout botté sur les champs de bataille,</l>
					<l>N’eût jamais attendu, les pieds sur les chenets,</l>
					<l>Buvant du champenois, qu’un de Moltke, un von Reigtz,</l>
					<l>Lâchement abrités derrière une fascine,</l>
					<l>Lui livrassent Berlin réduit par la famine !…</l>
				</lg>
				<lg>
					<l>C’est l’épée à la main qu’il allait aux combats…</l>
					<l>Et l’on peut excuser l’ivresse des soldats,</l>
					<l>Éblouis des rayons de ce grand météore,</l>
					<l>Répondant toujours : « Oui ! » quand il disait : « Encore ! »</l>
					<l>D’avoir permis, un jour que l’Europe avait peur,</l>
					<l>Où tous les souverains, affolés de stupeur,</l>
					<l>Se tenaient cois, tapis sous le dais de leur trône,</l>
					<l>Que ce victorieux saisisse une couronne,</l>
					<l>Et que, posant son pied sur leurs fronts consternés,</l>
					<l>Il dise à ce troupeau de princes détrônés :</l>
					<l>« Élus du droit divin, je suis le droit du glaive !…</l>
					<l>Vous êtes ce qui tombe, et moi ce qui s’élève ;</l>
					<l>Vous êtes le passé, vous êtes souvenir !…</l>
					<l>Moi, je suis l’inconnu qui s’appelle avenir !… »</l>
				</lg>
				<lg>
					<l>Avenir ! mot sonore avec lequel on grise</l>
					<l>L’humanité qui marche à la terre promise.</l>
					<l>Mot terrible et profond ! phare mystérieux</l>
					<l>Allumé par la main du jeune ambitieux</l>
					<l>Qui, bravant le remous de la marée humaine,</l>
					<l>Vit sombrer son esquif au cap de Sainte-Hélène !…</l>
				</lg>
				<lg>
					<l>Guillaume, le soldat dont j’épelle le nom</l>
					<l>S’appela Bonaparte, et puis Napoléon.</l>
					<l>Il avait, à vingt ans, conquis assez de gloire</l>
					<l>Pour rester sous sa tente et lasser la victoire</l>
					<l>A suivre pas à pas ce jeune conquérant,</l>
					<l>Qui, des bords de l’Adige, impétueux torrent,</l>
					<l>Poursuivant sans repos sa marche triomphale,</l>
					<l>Entra victorieux dans votre capitale !…</l>
					<l>Il avait abattu l’Autriche à Marengo</l>
					<l>Et dicté le traité de Campo-Formio… ;</l>
					<l>Il avait le Tyrol, toute la Lombardie !</l>
					<l>Il voulut l’univers ! Et, de sa main hardie</l>
					<l>Souffletant le vieux monde, il courut,, comme un fou,</l>
					<l>Se briser impuissant sous les murs de Moscou !…</l>
				</lg>
				<lg>
					<l>Sire, c’est là le sort que Dieu, dans sa justice,</l>
					<l>Garde aux vainqueurs qui vont, au gré de leur caprice,</l>
					<l>Jeter aux nations des défis outrageants…</l>
					<l>Cirons, que le flatteur compare à des géants,</l>
					<l>Rome vous réservait sa roche Tarpéienne !…</l>
					<l>Nous avons le bâton… et nous avons Cayenne !…</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>
