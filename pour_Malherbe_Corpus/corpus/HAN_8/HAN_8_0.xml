<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LE MIRLITON PRIAPIQUE</title>
				<title type="sub">69 QUATRAINS CONTRE LE SPLEEN</title>
				<title type="medium">Édition électronique</title>
				<author key="HAN">
					<name>
						<forename>Théodore</forename>
						<surname>HANNON</surname>
					</name>
					<date from="1851" to="1916">1851-1916</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>édition numérique du texte initial (gutenberg.org)</resp>
					<name>
						<forename>Eevee, Claudine</forename>
						<surname>Claudine Corbasson</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">HAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MIRLITON PRIAPIQUE</title>
						<author>Théodore HANNON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>hathitrust.org</publisher>
						<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=osu.32435015589302</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LE MIRLITON PRIAPIQUE</title>
								<title>69 QUATRAINS CONTRE LE SPLEEN</title>
								<author>FRÈRE CULPIDON</author>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Au Mont Carmel, En la Sacrée confiserie (Henry KISTEMAECKERS, Editeur)</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-12-18" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2022-12-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="HAN132">
				<head type="main">SONNET <lb/>DE JOYEUSE ENTRÉE</head>
				<lg n="1">
					<l>Pour faire un Mirliton, Madame,</l>
					<l>Ma plume vaut mieux qu’un roseau :</l>
					<l>Le roseau plie aux vents… moi, dame !</l>
					<l>Je réponds de mon arbrisseau.</l>
				</lg>
				<lg n="2">
					<l>Plein de sève, vivace et souple,</l>
					<l>Il peut rompre : plier, jamais !</l>
					<l>Les bonnes choses vont par couple…</l>
					<l>Mon instrument est simple, mais</l>
				</lg>
				<lg n="3">
					<l>J’enguirlande autour de sa tige</l>
					<l>Ces Quatrains sur lesquels voltige</l>
					<l>Très peu de l’esprit de Rotrou.</l>
				</lg>
				<lg n="4">
					<l>Pressez le Mirliton des fièvres,</l>
					<l>O Madame, contre vos lèvres</l>
					<l>Sans souffler trop fort dans le trou !</l>
				</lg>
			</div>
			<div type="poem" key="HAN133">
				<head type="number">1</head>
				<head type="main">QUATRAIN HIPPIQUE</head>
				<lg n="1">
					<l>CINQ jours par mois la femme peut</l>
					<l>Le disputer aux plus ingambes,</l>
					<l>Car elle a — excusez du peu !</l>
					<l>Un pur sang entre les jambes.</l>
				</lg>
			</div>
			<div type="poem" key="HAN134">
				<head type="number">2</head>
				<lg n="1">
					<l>UNE puce me mord, s’écriait mon amie,</l>
					<l><space unit="char" quantity="8"/>» Et se livre à maint entrechat,</l>
					<l>» Cherche donc !… » Je cherchai, mais ne la trouvant mie,</l>
					<l><space unit="char" quantity="8"/>J’ai jeté ma langue au chat !</l>
				</lg>
			</div>
			<div type="poem" key="HAN135">
				<head type="number">3</head>
				<lg n="1">
					<l>IL est des vérités parfois inattendues,</l>
					<l>Me disait un bonhomme en ces choses expert</l>
					<l><space unit="char" quantity="10"/>Avec les femmes perdues</l>
					<l>On se retrouve ; avec les autres, on se perd !</l>
				</lg>
			</div>
			<div type="poem" key="HAN136">
				<head type="number">4</head>
				<lg n="1">
					<l>LES guides, prétend-on, loin d’être cannibales,</l>
					<l><space unit="char" quantity="4"/>S’aiment entre eux peu platoniquement.</l>
					<l>Pourquoi les en blâmer ? Ils font logiquement :</l>
					<l><space unit="char" quantity="4"/>Le soldat doit aimer les trous de balles.</l>
				</lg>
			</div>
			<div type="poem" key="HAN137">
				<head type="number">5</head>
				<lg n="1">
					<l>MOI, qu’aucun fait ne va troublant,</l>
					<l>Une chose pourtant me frappe :</l>
					<l><space unit="char" quantity="4"/>Quand l’hiver met la nappe,</l>
					<l>Y en a qu’ pour les mangeurs de blanc !</l>
				</lg>
			</div>
			<div type="poem" key="HAN138">
				<head type="number">6</head>
				<lg n="1">
					<l>N’ALLEZ point me crier Shocking ! comme une Anglaise…</l>
					<l>‒ La sculpture est un art tout d’hygiène au fond.</l>
					<l>Vivent les sculpteurs ! ces gens à maquettes font</l>
					<l><space unit="char" quantity="12"/>Des capotes en glaise.</l>
				</lg>
			</div>
			<div type="poem" key="HAN139">
				<head type="number">7</head>
				<lg n="1">
					<l>GERMINY paya cher l’octroi</l>
					<l>De ses amours confiturières,</l>
					<l>Te le général Villeroy</l>
					<l>Sans cesse pris par ses derrières.</l>
				</lg>
			</div>
			<div type="poem" key="HAN140">
				<head type="number">8</head>
				<lg n="1">
					<l>LE Baron Seutin ſut un docteur plein de charme,</l>
					<l>Mais c’était avant tout un chirurgien vanté.</l>
					<l>Les bandages iront à la postérité :</l>
					<l><space unit="char" quantity="12"/>Il bandait comme un carme.</l>
				</lg>
			</div>
			<div type="poem" key="HAN141">
				<head type="number">9</head>
				<lg n="1">
					<l>Ta plume crache ? eh ! bien, fais-la tailler, ganache ! »</l>
					<l>Voilà ce que jadis, dans mon humble canton,</l>
					<l>Disait mon magister… Aujourd’hui c’est quand on</l>
					<l><space unit="char" quantity="8"/>Taille ma plume qu’elle crache !</l>
				</lg>
			</div>
			<div type="poem" key="HAN142">
				<head type="number">10</head>
				<lg n="1">
					<l>ÉTANT frileuse au lit, Charlotte fit l’achat,</l>
					<l>Pour réchauffer ses nuits, d’un griffon jeune et tendre…</l>
					<l>Grands dieux ! démolit-il ce proverbe : « S’entendre</l>
					<l><space unit="char" quantity="14"/>Comme chien et chat ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN143">
				<head type="number">11</head>
				<head type="main">A UNE ACTRICE</head>
				<lg n="1">
					<l>OR, durant bien des soirs, du chignon aux semelles,</l>
					<l>Je t’ai tenue au bout… de ma lorgnette, hélas !</l>
					<l>Gaiment, je donnerais ces soirs dont je suis las</l>
					<l>Pour te tenir une heure au bout de mes jumelles.</l>
				</lg>
			</div>
			<div type="poem" key="HAN144">
				<head type="number">12</head>
				<lg n="1">
					<l>JEANNE est gourmande encore et son vieux se rebelle :</l>
					<l>Le grog est bu, les ans ont soufflé le réchaud…</l>
					<l>Pourtant elle ne veut pas s’endormir, la belle,</l>
					<l><space unit="char" quantity="4"/>Sans avoir pris quelque chose de chaud.</l>
				</lg>
			</div>
			<div type="poem" key="HAN145">
				<head type="number">13</head>
				<lg n="1">
					<l>LE sceptre d’ici-bas, l’universel remède,</l>
					<l>» Le magique levier rêvé par Archimède,</l>
					<l>» C’est… <hi rend="ital">ceci</hi>… » Mais Irma : « Tu te vantes, mon cher,</l>
					<l><space unit="char" quantity="8"/>Cela n’est qu’une chose en l’air ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN146">
				<head type="number">14</head>
				<head type="main">BOTANIQUE APPLIQUÉE</head>
				<lg n="1">
					<l>LES Capucines sont les fleurs de la banlieue,</l>
					<l>Leur calice est rempli d’un suc délicieux,</l>
					<l>Madame, et pour goûter leur nectar précieux,</l>
					<l><space unit="char" quantity="8"/>Il suffit de sucer la queue.</l>
				</lg>
			</div>
			<div type="poem" key="HAN147">
				<head type="number">15</head>
				<lg n="1">
					<l>UNE femme d’esprit, mais d’un esprit factice,</l>
					<l>Encornant son époux, un grave magistrat,</l>
					<l>Disait à chaque coup de canif au contrat :</l>
					<l><space unit="char" quantity="8"/>« Je dresse mes bois de justice ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN148">
				<head type="number">16</head>
				<lg n="1">
					<l>L’IGNORANCE est un vice aimé du genre humain</l>
					<l>Que je ne puis sentir même en photographie.</l>
					<l>Au réveil, j’ai toujours mon histoire à la main,</l>
					<l>Et puis je fais des cartes de géographie.</l>
				</lg>
			</div>
			<div type="poem" key="HAN149">
				<head type="number">17</head>
				<lg n="1">
					<l>AUX champs, au lit, aux bois, en ville ou dans les seigles,</l>
					<l><space unit="char" quantity="8"/>L’amour, d’après Tanaquil, </l>
					<l><space unit="char" quantity="8"/>Est une de ces choses qu’il</l>
					<l><space unit="char" quantity="8"/>Ne faut pas faire dans les règles.</l>
				</lg>
			</div>
			<div type="poem" key="HAN150">
				<head type="number">18</head>
				<lg n="1">
					<l>LA Fontaine, un beau jour, ô chatte, en femme honnête</l>
					<l><space unit="char" quantity="8"/>Voulut vous métamorphoser.</l>
					<l>C’était imprudent… Moi, sans plus vous exposer,</l>
					<l><space unit="char" quantity="8"/>Je vous aurais refait minette.</l>
				</lg>
			</div>
			<div type="poem" key="HAN151">
				<head type="number">19</head>
				<head type="main">A UNE REINE DE THÉÂTRE</head>
				<lg n="1">
					<l>DU blanc de perle au front, du rouge à la pommette,</l>
					<l>Comme un astre tu luis à nos regards joyeux,</l>
					<l>Mais tu ne peux rester qu’une étoile à nos yeux…</l>
					<l>Si tu veux, dès ce soir, je te ferai comète !</l>
				</lg>
			</div>
			<div type="poem" key="HAN152">
				<head type="number">20</head>
				<head type="main">QUATRAIN NATURALISTE</head>
				<lg n="1">
					<l>L’ARAIGNÉE est un monstre horrible qui m’épate,</l>
					<l><space unit="char" quantity="4"/>» O mon amant, son aspect me fait froid</l>
					<l>» Dans les lombes. Son corps sans tête est mon effroi</l>
					<l><space unit="char" quantity="4"/>» Mais où tu sais j’en adore la patte. »</l>
				</lg>
			</div>
			<div type="poem" key="HAN153">
				<head type="number">21</head>
				<head type="main">QUATRAIN PHARMACEUTIQUE</head>
				<lg n="1">
					<l>LE copahu sonne son âpre glas</l>
					<l>Pour mon cœur qui verse des pleurs et flanche.</l>
					<l>Avec la femme avariée, hélas !</l>
					<l>L’amour est un combat à larme blanche.</l>
				</lg>
			</div>
			<div type="poem" key="HAN154">
				<head type="number">22</head>
				<head type="main">CONTRE LES AVOCATS</head>
				<lg n="1">
					<l>POUR plaire aux dames point n’est besoin de harangue,</l>
					<l><space unit="char" quantity="8"/>Préférant, en réalité,</l>
					<l><space unit="char" quantity="8"/>Dans le bleu de l’intimité,</l>
					<l>Aux mots superficiels le moindre coup de langue.</l>
				</lg>
			</div>
			<div type="poem" key="HAN155">
				<head type="number">23</head>
				<lg n="1">
					<l>NOUS ne sommes souvent que l’amant honoraire</l>
					<l>D’un cœur qui vous remet sans cesse au lendemain,</l>
					<l><space unit="char" quantity="8"/>Avec la veuve Poignet, frère,</l>
					<l>On s’offre toujours tout de première main.</l>
				</lg>
			</div>
			<div type="poem" key="HAN156">
				<head type="number">24</head>
				<lg n="1">
					<l>CERTAIN époux à jeûn dont la jeune accouchée</l>
					<l>Ne pouvait apaiser la faim, — prêt à râler,</l>
					<l>Renversant sa nounou n’en fit qu’une bouchée :</l>
					<l><space unit="char" quantity="8"/>La nourrice est un pis-aller.</l>
				</lg>
			</div>
			<div type="poem" key="HAN157">
				<head type="number">25</head>
				<head type="main">A UNE DAME BIEN</head>
				<lg n="1">
					<l>MES veux et leur Terre-Promise,</l>
					<l>O madame, et mes rêves fous</l>
					<l>Seraient de manger avec vous</l>
					<l>Des pommes de terre… en chemise.</l>
				</lg>
			</div>
			<div type="poem" key="HAN158">
				<head type="number">26</head>
				<lg n="1">
					<l>ÔTEZ vos doigts de là, Monsieur ! n’espérez rien</l>
					<l>» En me prenant ainsi sur vos genoux… l’entrée… »</l>
					<l>« Madame, ce que j’en fais c’est pour votre bien.»</l>
					<l><space unit="char" quantity="8"/>« Ah ! j’en suis toute pénétrée ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN159">
				<head type="number">27</head>
				<lg n="1">
					<l>EN cette nuit de noce où mon cœur t’appela,</l>
					<l><space unit="char" quantity="8"/>Mère, un détail m’a semblé louche :</l>
					<l>Mon mari m’a surtout mis ses moustaches… là…</l>
					<l><space unit="char" quantity="8"/>Serait-il porté sur sa bouche ?</l>
				</lg>
			</div>
			<div type="poem" key="HAN160">
				<head type="number">28</head>
				<lg n="1">
					<l>ROUGE et noire souvent nous en font voir de vertes !</l>
					<l>Il faut au jeu savoir perdre des monceaux d’or</l>
					<l>Sans pâlir… Mais il n’est que les femmes encor</l>
					<l><space unit="char" quantity="8"/>Pour savoir essuyer des pertes.</l>
				</lg>
			</div>
			<div type="poem" key="HAN161">
				<head type="number">29</head>
				<head type="main">QUATRAIN ASTRONOMIQUE</head>
				<lg n="1">
					<l>COMBIEN doucement brille en l’azur qui pâlit</l>
					<l>L’astre des soirs, topaze au front de la nuit brune !</l>
					<l><space unit="char" quantity="8"/>Mais c’est surtout au ciel de lit</l>
					<l><space unit="char" quantity="8"/>Que j’aime à voir lever la lune.</l>
				</lg>
			</div>
			<div type="poem" key="HAN162">
				<head type="number">30</head>
				<lg n="1">
					<l>AVEC les femmes j’ai toujours été très large. »</l>
					<l>Disait un vieux viveur, le faisant à la charge.</l>
					<l><space unit="char" quantity="8"/>Aussi lui fut-il répondu :</l>
					<l>« Très large ?… Elles te l’ont, pauvre ami, bien rendu ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN163">
				<head type="number">31</head>
				<lg n="1">
					<l>QUE cherches-tu, mon cher, où fourres-tu la tête,</l>
					<l>» Et que veulent tes doigts aux ongles diligents ? »</l>
					<l>« Ne te grattais-tu point, mignonne ? » — Il est des gens</l>
					<l><space unit="char" quantity="4"/>Qui cherchent toujours la petite bête.</l>
				</lg>
			</div>
			<div type="poem" key="HAN164">
				<head type="number">32</head>
				<head type="main">OBÉISSANCE ACTIVE</head>
				<lg n="1">
					<l>AU camp de Beverloo, las de recherche vaine,</l>
					<l>Un soldat fit un trou par terre et s’y mit… Quand</l>
					<l>Son chef, passant, lui dit : « Vas-tu foutre le camp ! »</l>
					<l>Mais l’autre, sans bouger : « C’est fait, mon capitaine ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN165">
				<head type="number">33</head>
				<head type="main">VÉRITÉ</head>
				<lg n="1">
					<l><space unit="char" quantity="4"/>SUIVRE la file au bois, aux gares ou</l>
					<l>Aux théâtres me met dans une rage bleue,</l>
					<l>Mais les femmes toujours s’en donnent pour un sou :</l>
					<l>Ce n’est pas ce qui les gêne faire la queue !</l>
				</lg>
			</div>
			<div type="poem" key="HAN166">
				<head type="number">34</head>
				<head type="main">A UNE MISS DÉCENTE</head>
				<lg n="1">
					<l>SI je possédais, chère enfant,</l>
					<l>Le don de la métamorphose,</l>
					<l>Je vous ferais, vœu triomphant,</l>
					<l>Je vous ferais… feuille de rose.</l>
				</lg>
			</div>
			<div type="poem" key="HAN167">
				<head type="number">35</head>
				<lg n="1">
					<l>TA femme, tu la bats donc qu’elle crie à l’aide ?</l>
					<l>» Calme-toi, mon cher, rien ne passe la douceur :</l>
					<l>» Le mari qui rudoie est un lâche… » « Et ta sœur !</l>
					<l>» Avec la femme il faut toujours être très raide. »</l>
				</lg>
			</div>
			<div type="poem" key="HAN168">
				<head type="number">36</head>
				<lg n="1">
					<l>JE sors d’un dîner chic où la dent put agir</l>
					<l><space unit="char" quantity="4"/>Sans que la bonne humeur s’enfuisse ;</l>
					<l>Ces dames, au dessert, m’ont servi sans rougir</l>
					<l><space unit="char" quantity="4"/>Des Pets de nonne et des Couilles de Suisse !</l>
				</lg>
			</div>
			<div type="poem" key="HAN169">
				<head type="number">37</head>
				<lg n="1">
					<l>IL est vieux, mais pour lui le monde est toujours neuf :</l>
					<l><space unit="char" quantity="4"/>En vain Quatre-vingt succède à Septante,</l>
					<l><space unit="char" quantity="8"/>Près de la beauté qui le tente</l>
					<l><space unit="char" quantity="4"/>Notre homme est toujours en Soixante-neuf.</l>
				</lg>
			</div>
			<div type="poem" key="HAN170">
				<head type="number">38</head>
				<lg n="1">
					<l>LA gente Françoise a de nombreux professeurs</l>
					<l>Grâce auxquels son esprit sortira des gangues.</l>
					<l>Or, j’en envie un seul parmi tous ces faiseurs :</l>
					<l><space unit="char" quantity="12"/>Le professeur de langues.</l>
				</lg>
			</div>
			<div type="poem" key="HAN171">
				<head type="number">39</head>
				<lg n="1">
					<l><space unit="char" quantity="8"/>LA femme n’est pas tant friande,</l>
					<l>Mais son instinct la pousse aux reliefs plantureux ;</l>
					<l><space unit="char" quantity="8"/>Elle a toujours le ventre creux</l>
					<l>Et veut, pour le remplir, un bon morceau de viande.</l>
				</lg>
			</div>
			<div type="poem" key="HAN172">
				<head type="number">40</head>
				<lg n="1">
					<l>S’IL faut en croire les pamphlets</l>
					<l>Écrits aux époques vieillottes,</l>
					<l>Jésus-Christ reçut des soufflets…</l>
					<l>Mon petit-frère <hi rend="ital">des calottes</hi>.</l>
				</lg>
			</div>
			<div type="poem" key="HAN173">
				<head type="number">41</head>
				<head type="main">QUARTIER A LOUER</head>
				<head type="sub_1">PRÉSENTEMENT</head>
				<lg n="1">
					<l>CERTAINE veuve avait une chambre à louer.</l>
					<l>Je cours me présenter, et mon bonheur éclate</l>
					<l>En entendant l’enfant crier à s’enrouer :</l>
					<l><space unit="char" quantity="4"/>« Entrée en jouissance immédiate ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN174">
				<head type="number">42</head>
				<head type="main">GUITARE MILITARISTE</head>
				<lg n="1">
					<l>CRAINS fort la cantinière et reste réfractaire</l>
					<l>Aux propos qu’à ton cœur elle vient fredonner,</l>
					<l>Car ne met-elle pas sa joie à te donner</l>
					<l><space unit="char" quantity="12"/>La goutte — militaire ?</l>
				</lg>
			</div>
			<div type="poem" key="HAN175">
				<head type="number">43</head>
				<lg n="1">
					<l>QU’Y a-t-il de meilleur dans l’homme ?</l>
					<l>« C’est le chien ! » a dit un goujat…</l>
					<l>Parfait ! mais dans la femme en somme,</l>
					<l>Le meilleur aussi, c’est le chat.</l>
				</lg>
			</div>
			<div type="poem" key="HAN176">
				<head type="number">44</head>
				<head type="main">IMPROMPTU</head>
				<lg n="1">
					<l>EN visite, trouvant Madame seule, store</l>
					<l>Baissé, je lui troussai son pantalon brodé…</l>
					<l>Elle pâma, criant : « Quel incident !… Encore ! ! »</l>
					<l>Fit-elle ensuite. · Or moi : « L’incident est vidé ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN177">
				<head type="number">45</head>
				<lg n="1">
					<l>OSTENDE me charme beaucoup,</l>
					<l>Des plaisirs c’est la grande coupe,</l>
					<l>Car on y peut tirer sa coupe</l>
					<l><space unit="char" quantity="10"/>Et son coup</l>
				</lg>
			</div>
			<div type="poem" key="HAN178">
				<head type="number">46</head>
				<lg n="1">
					<l>CONTRE les sculpteurs je m’indigne,</l>
					<l>Car ces botanistes bâtés</l>
					<l>Mettent toujours, les entêtés,</l>
					<l>Aux glands des feuilles de vigne !</l>
				</lg>
			</div>
			<div type="poem" key="HAN179">
				<head type="number">47</head>
				<lg n="1">
					<l>COUSIN, faisons des jeux. » « Soit, cousine, à quoi ? » « Dame !</l>
					<l><space unit="char" quantity="8"/>» Jouons au trou-madame… »</l>
					<l>« Non, je voudrais un jeu innocent moins rouillé. »</l>
					<l><space unit="char" quantity="8"/>« Alors jouons au doigt-mouillé. »</l>
				</lg>
			</div>
			<div type="poem" key="HAN180">
				<head type="number">48</head>
				<lg n="1">
					<l>LES pédérastes n’ont aucune</l>
					<l>Des vertus qu’on rêve aux banquiers,</l>
					<l>Car ce sont des gens coutumiers</l>
					<l>De faire des trous à la lune.</l>
				</lg>
			</div>
			<div type="poem" key="HAN181">
				<head type="number">49</head>
				<head type="main">QUATRAIN BIBLIQUE</head>
				<lg n="1">
					<l>LA Pomme d’Adam n’est qu’un rêve</l>
					<l>De la Bible, bouquin scieur :</l>
					<l>Le fruit qui tenta Madame Ève,</l>
					<l>Ce fut… les Prunes de Monsieur.</l>
				</lg>
			</div>
			<div type="poem" key="HAN182">
				<head type="number">50</head>
				<lg n="1">
					<l>VOTRE bouche est mignonne et peut</l>
					<l>Dans nos cœurs allumer les fièvres.</l>
					<l>En le disant je songe un peu,</l>
					<l>Madame, à vos petites lèvres !</l>
				</lg>
			</div>
			<div type="poem" key="HAN183">
				<head type="number">51</head>
				<head type="main">LA FEUILLE A L’ENDROIT</head>
				<lg n="1">
					<l>JALOUSANT des fraters les excès féodaux,</l>
					<l>Certain soir, à rebours, je caressai la belle :</l>
					<l>« L’aimes-tu point ? » Fis-je à l’enfant qui se rebelle ?…</l>
					<l><space unit="char" quantity="4"/>« Oh ! que non, Monsieur, j’en ai plein le dos ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN184">
				<head type="number">52</head>
				<lg n="1">
					<l>ENFIN, soyons catégorique,</l>
					<l>» Madame, comment entre-t-on</l>
					<l>» Dans votre cœur, cœur électrique ? »</l>
					<l>« Il faut pousser sur le bouton. »</l>
				</lg>
			</div>
			<div type="poem" key="HAN185">
				<head type="number">53</head>
				<lg n="1">
					<l>MONSIEUR de Germiny raffolait des beaux noirs ;</l>
					<l>Mieux eût valu pour lui courir autour d’un arbre.</l>
					<l>Les siècles graveront ses hauts faits sur le marbre</l>
					<l><space unit="char" quantity="16"/>Des urinoirs !</l>
				</lg>
			</div>
			<div type="poem" key="HAN186">
				<head type="number">54</head>
				<lg n="1">
					<l>PAUVRE amant, quel remords soudainement me ronge ?</l>
					<l>» J’ai trompé mon époux, le meilleur des amis… »</l>
					<l>« Ma chère, calme-toi, lorsque l’acte est commis,</l>
					<l><space unit="char" quantity="8"/>» Il faut dessus’passer l’éponge ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN187">
				<head type="number">55</head>
				<lg n="1">
					<l>MINEURES ! à ce mot négatif chacun pâme,</l>
					<l>Rêvant de l’enclos neuf qu’il voudrait bien herser…</l>
					<l>Pour moi, je les dédaigne, et de toute mon âme,</l>
					<l>Ces clous pas assez mûrs pour se faire percer !</l>
				</lg>
			</div>
			<div type="poem" key="HAN188">
				<head type="number">56</head>
				<lg n="1">
					<l>LA veille, Anna pleurait d’être aux autels menée.</l>
					<l><space unit="char" quantity="8"/>Voulant-mourir fille. Or, le lendemain</l>
					<l>Il eût fallu la voir serrer à pleine main</l>
					<l><space unit="char" quantity="8"/>Le nœud de l’hyménée !</l>
				</lg>
			</div>
			<div type="poem" key="HAN189">
				<head type="number">57</head>
				<lg n="1">
					<l>BRUNE, blonde, châtaine, rousse,</l>
					<l>Devant l’ombre est égale en prix</l>
					<l>Toute femme que l’on retrousse :</l>
					<l><space unit="char" quantity="2"/>La nuit tous les chats sont gris.</l>
				</lg>
			</div>
			<div type="poem" key="HAN190">
				<head type="number">58</head>
				<head type="main">ÉVANGILE SELON…</head>
				<lg n="1">
					<l>SUIVANT une dame en robe à traîne, je lui</l>
					<l>Marchai sur la queue… à peine ! Elle se tourna, blême,</l>
					<l>Me criant sans rougir : « Pourquoi faire à autrui </l>
					<l>Ce qui vous déplairait qu’on vous fit à vous-même ? »</l>
				</lg>
			</div>
			<div type="poem" key="HAN191">
				<head type="number">59</head>
				<head type="main">ALLER ET RETOUR</head>
				<lg n="1">
					<l>EN amour l’aller est parfait,</l>
					<l>Mais la retraite, bien amère :</l>
					<l>On part chevauchant la chimère</l>
					<l>Et l’on revient… sur un bidet !</l>
				</lg>
			</div>
			<div type="poem" key="HAN192">
				<head type="number">60</head>
				<lg n="1">
					<l>UN soir flirtant en vain près de certaine vierge,</l>
					<l>« Roc à qui je me plains, » criai-je — et je rêvais :</l>
					<l>Comme Moïse encore, hélas ! si je pouvais,</l>
					<l><space unit="char" quantity="8"/>Rocher, te frapper de ma verge !</l>
				</lg>
			</div>
			<div type="poem" key="HAN193">
				<head type="number">61</head>
				<head type="main">MARINE</head>
				<lg n="1">
					<l>PRENDRE la lune dédaigneuse</l>
					<l>Là-haut est malaisé, mais dans</l>
					<l>La mer je veux prendre, ô baigneuse,</l>
					<l><space unit="char" quantity="4"/>La lune avec mes dents !</l>
				</lg>
			</div>
			<div type="poem" key="HAN194">
				<head type="number">62</head>
				<head type="main">UN PRÉJUGÉ</head>
				<lg n="1">
					<l>LES pendus, nous dit-on, traversent une phase</l>
					<l>Drôle… quand se roidit leur pauvre corps ballant.</l>
					<l>Erreur ! Pour moi, j’affirme impossible l’extase</l>
					<l><space unit="char" quantity="12"/>Avec un nœud coulant !</l>
				</lg>
			</div>
			<div type="poem" key="HAN195">
				<head type="number">63</head>
				<lg n="1">
					<l>Il n’y a plus d’enfants ! » disait après un mot</l>
					<l>Terrible, une bégueule à l’esprit somnifère.</l>
					<l>« Eh ! parbleu, chère dame, interrompt le marmot,</l>
					<l><space unit="char" quantity="8"/>» C’est pour cela qu’il faut en faire ! »</l>
				</lg>
			</div>
			<div type="poem" key="HAN196">
				<head type="number">64</head>
				<lg n="1">
					<l>LA meilleure façon d’enchaîner les amours</l>
					<l>En ménage, n’est pas d’être un mari goguette,</l>
					<l><space unit="char" quantity="8"/>Bien au contraire, il faut toujours</l>
					<l><space unit="char" quantity="8"/>Mener sa femme à la braguette.</l>
				</lg>
			</div>
			<div type="poem" key="HAN197">
				<head type="number">65</head>
				<lg n="1">
					<l>MOUCHES-TU la chandelle, tiens !</l>
					<l><space unit="char" quantity="6"/>C’est qu’il y a mèche,</l>
					<l>Et, pauvre ami, si tu la tiens</l>
					<l><space unit="char" quantity="6"/>C’est qu’y a pas mèche !</l>
				</lg>
			</div>
			<div type="poem" key="HAN198">
				<head type="number">66</head>
				<lg n="1">
					<l>EN amour, nul besoin d’interprètes exsangues :</l>
					<l><space unit="char" quantity="8"/>Qu’ils gardent leur science pour eux,</l>
					<l><space unit="char" quantity="14"/>Car les amoureux</l>
					<l><space unit="char" quantity="14"/>Se passent des langues.</l>
				</lg>
			</div>
			<div type="poem" key="HAN199">
				<head type="number">67</head>
				<lg n="1">
					<l><space unit="char" quantity="4"/>RACE de malédiction,</l>
					<l><space unit="char" quantity="4"/>Aux doigts crochus, à la main preste,</l>
					<l>Les Juifs, de par leur circoncision,</l>
					<l>Sont des gens qui jouissent de leur reste.</l>
				</lg>
			</div>
			<div type="poem" key="HAN200">
				<head type="number">68</head>
				<lg n="1">
					<l>EN son art Meyerbeer bandait toujours son arc,</l>
					<l>Cependant il montra sa discrète nature</l>
					<l><space unit="char" quantity="8"/>En ne faisant à <hi rend="ital">Jeanne d’Arc</hi></l>
					<l><space unit="char" quantity="8"/>Qu’une petite Ouverture.</l>
				</lg>
			</div>
			<div type="poem" key="HAN201">
				<head type="number">69</head>
				<head type="main">ARGUMENT <hi rend="ital">AD FEMINEM</hi></head>
				<lg n="1">
					<l>EH ! bien, cher avocat, et mon pauvre mari ? »</l>
					<l>« Son affaire est pendante… Oh ! j’en suis bien marri. »</l>
					<l><space unit="char" quantity="12"/>« Pendante ?… C’est infâme !</l>
					<l>» Il fallait la remettre aux mains de votre femme. »</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>
