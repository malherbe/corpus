<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">SIÈGE DE PARIS — ENTRE ABSENTS</title>
				<title type="sub">RÉPONSE A LA LETTRE D’UN MOBILE BRETON</title>
				<title type="medium">Édition électronique</title>
				<author key="FLR">
					<name>
						<forename>Zenaïde</forename>
						<surname>FLEURIOT</surname>
					</name>
					<date from="1829" to="1890">1829-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>SIÈGE DE PARIS. ENTRE ABSENTS — RÉPONSE A LA LETTRE D’UN MOBILE BRETON</title>
						<author>ZENAÏDE FLEURIOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6121248t.r=ZENA%C3%8FDE%20FLEURIOT%20SI%C3%88GE%20DE%20PARIS?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>SIÈGE DE PARIS. ENTRE ABSENTS — RÉPONSE A LA LETTRE D’UN MOBILE BRETON</title>
								<author>ZENAÏDE FLEURIOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LECOFFRE LIBRAIRE-ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem">
				<head type="main">RÉPONSE A LA LETTRE D’UN MOBILE BRETON</head>
				<head type="sub">MARC’HARID A ERWAN</head>
				<opener>
					<dateline>
						<date when="1870">Kermor, le 27 novembre 1870.</date>
					</dateline>
				</opener>
				<lg>
					<l>Mon frère, ce matin nous recevons ta lettre ;</l>
					<l>Et ce soir, pas plus tard, sur papier je veux mettre</l>
					<l>Nos souvenirs à tous et les bruits du pays.</l>
					<l>Je revenais du four par le grand bois taillis ;</l>
					<l>Tu sais, le bois touffu qui nous cache la lande,</l>
					<l>Où tu m’as tant cueilli de bouquets de lavande.</l>
					<l>Mon cœur était bien triste, et mon pas était lourd :</l>
					<l>On avait si souvent dit ton nom par le bourg !</l>
					<l>« Vous n’avez plus Erwan et sa jument pécharde, »</l>
					<l>M’avait crié Kernacʼh, le joueur de bombarde,</l>
					<l>Qui noyait le chagrin chez les nouveaux conscrits,</l>
					<l>Dont les yeux sont mouillés et les chapeaux fleuris ;</l>
					<l>Et qui, tout désolés de quitter leur Bretagne,</l>
					<l>Bravement, en dansant, se mettent en campagne.</l>
					<l>Ce mot, dit en riant, me pesait sur le cœur,</l>
					<l>Et je songeais à toi, quand je vis le facteur</l>
					<l>Qui grimpait le sentier. Je ne pouvais comprendre</l>
					<l>Pourquoi le brave gars me criait de l’attendre,</l>
					<l>La poste, et c’était là le gros de nos soucis,</l>
					<l>Ne marchant plus chez nous, du moins jusqu’à Paris.</l>
					<l>J’attendis cependant ; et quelle fut ma joie</l>
					<l>Quand sortit de son sac un fin papier de soie,</l>
					<l>Où je lus notre nom et celui de Kernor.</l>
					<l>Dans ma jatte de bois je plaçai mon trésor ;</l>
					<l>Et, tirant mes sabots pour arriver plus vite,</l>
					<l>Je courus comme un lièvre éperdu vers mon gîte.</l>
					<l>Le chemin raboteux à mes pieds semblait doux ;</l>
					<l>Et je ne sentais plus ni sable ni cailloux.</l>
					<l>Dans la cour, je passai derrière le grand hêtre ;</l>
					<l>Et, sans bruit, j’avançai la tête à la fenêtre.</l>
					<l>Tadcoz<ref target="1" type="noteAnchor">1</ref> était assis sur le banc de noyer</l>
					<l>Qui touche à ton lit clos, à gauche du foyer.</l>
					<l>Depuis que son Erwan est parti pour la guerre,</l>
					<l>Le pauvre cher Tadcoz ne nous parle plus guère ;</l>
					<l>Il marmotte tout bas en regardant le feu,</l>
					<l>Et conte, à sa façon, sa lourde peine à Dieu.</l>
					<l>Barbaïk, la pauvresse, à genoux devant l’âtre,</l>
					<l>Chauffait ses vieilles mains ; et notre petit pâtre,</l>
					<l>Qui voudrait s’engager, coupait des navets blancs,</l>
					<l>De ceux que tu semas dans le pré des Étangs.</l>
					<l>Ma mère, que vraiment je trouve un peu changée,</l>
					<l>Taillait du pain de seigle ; et la nappe frangée</l>
					<l>Recevait à la fois entre ses larges plis</l>
					<l>Et le pain et les pleurs dont ses yeux sont remplis.</l>
					<l>Devant Père fumaient nos crêpes de dentelle…</l>
					<l>En manges-tu là-bas ? Dans une grande écuelle</l>
					<l>Vois-tu mousser le lait du matin baratté ?</l>
					<l>Il se signait disant son bénédicité,</l>
					<l>Quand je criai : « D’Erwan j’apporte des nouvelles !… »</l>
					<l>Il fallait voir les yeux lancer des étincelles :</l>
					<l>Père restait debout, son chapeau dans la main,</l>
					<l>Mammik<ref target="2" type="noteAnchor">2</ref> tombait assise en renversant le pain ;</l>
					<l>Ton chien, ton pauvre Mapp, qui dormait dans la cendre,</l>
					<l>Jappait, le nez en l’air, d’un ton plaintif et tendre.</l>
					<l>Barbaïk et Tadcoz avaient dressé leurs corps :</l>
					<l>Ton nom ressuscitait même ces deux cœurs morts.</l>
					<l>J’entrai… Comme les vieux ont l’oreille un peu dure,</l>
					<l>J’allai près du foyer commencer ma lecture.</l>
					<l>Nos parents écoutaient sans oser respirer,</l>
					<l>Et Mère ne pouvait s’empêcher de pleurer.</l>
					<l>Quand je finis, je vis qu’elle baisait ta lettre.</l>
					<l>Mon père a dit : « Le gars au métier va se mettre</l>
					<l>Et sera bon soldat pour faire son devoir. »</l>
					<l>Le pâtre s’étonnait de tout ce qu’un point noir</l>
					<l>Me disait en français ; Barbaïk, la pauvresse,</l>
					<l>Que tout événement militaire intéresse,</l>
					<l>Causait avec Tadcoz des guerres d’autrefois.</l>
					<l>Pour moi, j’ai dû laisser Tadcoz à ses exploits,</l>
					<l>Et tous à leur bonheur. Me faisant ta courrière,</l>
					<l>J’ai, sur mes souliers fins, couru vers la rivière.</l>
					<l>D’Yvonne j’entendais le grand coup de battoir,</l>
					<l>Et je ne voulais pas attendre jusqu’au soir</l>
					<l>Pour porter à son cœur, qui t’est resté fidèle,</l>
					<l>De ton bon souvenir l’agréable nouvelle.</l>
					<l>Ta promise n’a plus sa gaîté de pinson ;</l>
					<l>Ses lèvres, d’où sortaient le rire ou la chanson,</l>
					<l>Sont closes maintenant. Près d’elle je m’arrête,</l>
					<l>Je l’appelle bien haut ; elle lève la tête,</l>
					<l>Aperçoit le papier que je tiens à la main,</l>
					<l>Se dresse sur ses pieds, bondit dans le chemin.</l>
					<l>Pour lire, je m’étais assise au pied d’un saule,</l>
					<l>Du vieux saule entr’ouvert. Par-dessus mon épaule,</l>
					<l>Yvonnette lisait ; et de ses yeux baissés</l>
					<l>Tombaient des pleurs d’amour, qu’ici je t’ai glissés.</l>
					<l>Elle te fait, par moi, ses compliments sincères ;</l>
					<l>Elle dit que ton nom revient dans ses prières ;</l>
					<l>Et qu’à Sainte-Anne, un jour, nous irons les pieds nus</l>
					<l>Avec le cierge blanc, qui coûte deux écus.</l>
					<l>Yvonne consolée a repris son ouvrage ;</l>
					<l>Et j’ai repris gaîment mon doux pèlerinage.</l>
					<l>Aux amis, aux parents, à monsieur le recteur,</l>
					<l>J’ai porté ton billet en fidèle facteur.</l>
					<l>Cela soulève un peu notre poids de tristesse</l>
					<l>De penser qu’on reçoit un bon mot de tendresse,</l>
					<l>Grâce à cette machine étrange, à ce ballon</l>
					<l>Qui traverse le ciel comme un oiseau, dit-on.</l>
					<l>Il est doux de savoir au fond de sa montagne</l>
					<l>Qu’on est content, là-bas, des soldats de Bretagne.</l>
					<l>Donc, on est un peu moins désolé par le bourg…</l>
					<l>Le bon Dieu, disons-nous, n’est pas devenu sourd.</l>
					<l>Les gars ont leur fusil, les femmes leur rosaire ;</l>
					<l>Avec cela, chez nous, on supporte la guerre.</l>
					<l>Dis à monsieur René qu’on est bien au château.</l>
					<l>Le vieux Comte a planté sur sa porte un drapeau.</l>
					<l>Et prépare au combat, avant qu’il ne s’engage,</l>
					<l>Les vieux et les petits qui montrent du courage.</l>
					<l>Ils en ont tous, Erwan, à preuve le tailleur,</l>
					<l>Qui, tout bossu qu’il est, s’habille en artilleur.</l>
					<l>Ce matin, mamelle Anne a cueilli dans notre aire</l>
					<l>Des fleurs de centaurée et de pariétaire ;</l>
					<l>La Comtesse et Mammik, assises sur les puits,</l>
					<l>Ensemble sanglotaient, en parlant de leurs fils.</l>
					<l>Chacun à son chagrin en ce temps de misère,</l>
					<l>Riche et pauvre ont prêté du sang pour cette guerre,</l>
					<l>Vous vous battez là-bas, et nous souffrons ici ;</l>
					<l>S’ils viennent à Quimper, nous nous battrons aussi.</l>
					<l>Mais il faut te quitter ; je n’ai plus rien à mettre.</l>
					<l>Au chant du rossignol je ferme cette lettre.</l>
					<l>Kenavos<ref target="3" type="noteAnchor">3</ref>, mon Erwan, je t’embrasse pour tous.</l>
					<l>Kenavos, le bon Dieu te ramène chez nous !</l>
				</lg>
				<closer>
					<note type="footnote" id="1">Grand-père</note>
					<note type="footnote" id="2">Petite mère.</note>
					<note type="footnote" id="3">Adieu</note>
				</closer>
			</div>


		</body>
	</text>
</TEI>