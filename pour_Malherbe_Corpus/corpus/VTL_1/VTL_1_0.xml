<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.7.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES IMPRIMÉES À PARIS ENTRE 1848 ET 1851 </title>
				<title type="main">DE TRENTE-ET-UN CHANSONNIERS DITS « POPULAIRES »</title>
				<title type="corpus">corpus Romain Benini</title>
				<title type="medium">Édition électronique</title>
				<author key="VTL">
					<name>
						<forename>louis</forename>
						<surname>VOITELAIN</surname>
					</name>
					<date from="1798" to="1852">1798-1852</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent/>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2024</date>
				<idno type="local">VTL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Filles du peuple ?</title>
						<title>Pour une stylistique de la chanson au XIXe siècle</title>
						<author>Romain Benini</author>
						<idno type="URI">https://books.openedition.org/enseditions/17162</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ENS Éditions</publisher>
							<date when="2021">2021</date>
						</imprint>
					</monogr>
					<note>Corpus de textes fourni par l’auteur</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<p>Références des textes imprimés. Extrait du document "Ref_biblio_corpus_Benini.pdf (Romain Benini)"</p>
				<p>
					Le Vieux Prolétaire, air de Léonide, s. d., BnF Ye 54216.
					À David d’Angers, air de Muse des bois, etc., s. d., BnF Ye 54216.
					Louis-Napoléon, air du Forçat libéré ou de La Fête des Martyrs, juillet 1848, BnF
					Ye 7185 (318), repris en décembre 1848, BnF IFN 53017438.
					Adieux au village, air de Allez cueillir des bluets dans les blés, dans Le Républicain
					lyrique, no 6, Paris, Durand, décembre 1848, BnF Nump 4424.
					La Grand-Mère, air de V’li, v’lan, dans Le Républicain lyrique, no 6, Paris, Durand,
					février 1849, BnF Nump 4424.
					À Raspail, air de Retour en France, ou de Vive Paris, juin 1849, BnF Ye 7185 (382).
					Le Père Jérôme, air de Ma marmotte a mal au pied, dans Le Favori des dames, Paris,
					Durand, 1849, BnF Ye 7180 (554).
				</p>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1849">1848-1849</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="VTL1">
				<head type="main">LE VIEUX PROLÉTAIRE.</head>
				<head type="tune"><hi rend="smallcap">Air </hi> :<hi rend="ital">De Léonide</hi>.</head>
				<lg n="1">
					<l>Soixante hivers ont affaibli ma vue,</l>
					<l>Soixante hivers ont énervé mon bras ;</l>
					<l>Ah ! c’en est fait : ma vieillesse éperdue</l>
					<l>N’espère plus en des maîtres ingrats.</l>
					<l>Las ! du côteau qui me servait de couche</l>
					<l>Les vents du nord ont flêtri le gazon ;</l>
					<l>Hommes du roi, si le malheur vous touche,</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="2">
					<l>J’ai vu là-bas le clocher de Bicêtre,</l>
					<l>Hier vers lui je crus prendre l’essor ;</l>
					<l>D’un faux espoir j’aimais à me repaître :</l>
					<l>Quoique bien vieux, je suis trop jeune encor,</l>
					<l>En attendant que vos lois bienveillantes</l>
					<l>D’un ciel plus doux me montrent l’horison,</l>
					<l>Pour ranimer mes forces défaillantes,</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="3">
					<l>Sur ma sueur d’opulentes familles</l>
					<l>Impudemment ont levé du butin ;</l>
					<l>Par mon travail j’ai vu doter leurs filles,</l>
					<l>De leurs garçons j’ai payé le latin ;</l>
					<l>J’ai vu briller mainte ignoble maîtresse,</l>
					<l>Des faux plaisirs j’ai fourni le poison ;</l>
					<l>Je ne puis plus engraisser la paresse,</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="4">
					<l>J’avais un fils ; il était à l’armée</l>
					<l>Quand, vers la Rhône, en des jours de terreur,</l>
					<l>Des ouvriers le menace affamée</l>
					<l>De leurs patrons provoqua la fureur ;</l>
					<l>Contre le plomb, le fer et le salpêtre,</l>
					<l>Le pauvre agneau défendait sa toison !…</l>
					<l>Mon fils est mort en protégeant son maître ;</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="5">
					<l>Que n’ai-je, hélas ! sans honte et sans courage,</l>
					<l>D’un grand seigneur augmenté le bétail ;</l>
					<l>Oui, j’aurais pu me soustraire au naufrage</l>
					<l>En préférant la livrée au travail.</l>
					<l>D’un plat valet le langage servile</l>
					<l>Peut obtenir des secours du blason ;</l>
					<l>Il sait ramper, je ne sus qu’être utile ;</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="6">
					<l>Pour me fermer cet abri redoutable</l>
					<l>Vous m’opposez un obstacle imprévu :</l>
					<l>Ah ! d’un méfait s’il faut être coupable,</l>
					<l>J’ai mendié ; vous ne m’avez pas vu.</l>
					<l>Moi, mendier !… Non, non, ce mot m’outrage ;</l>
					<l>Mais des beaux jours j’ai vu fuir la saison ;</l>
					<l>On ne veut plus me donner de l’ouvrage,</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
				<lg n="7">
					<l>Mais, dites-vous, votre faible nature</l>
					<l>Au temps heureux n’a pas prévu la faim.</l>
					<l>– Est-ce en trouvant à peine sa pâture</l>
					<l>Que la fourmi se crée un lendemain ?</l>
					<l>Vous le savez, l’abeille, en sa sagesse,</l>
					<l>N’admit jamais l’oisif à sa moisson.</l>
					<l>Moi, j’ai donné mon suc à la mollesse :</l>
					<l>Accordez-moi le pain de la prison.</l>
				</lg>
			</div>
			<div type="poem" key="VTL2">
				<head type="main">A DAVID D’ANGERS.</head>
				<head type="tune"><hi rend="smallcap">Air </hi> :<hi rend="ital">Muse des Bois, etc.</hi></head>
				<lg n="1">
					<l>Si Phidias émerveilla l’Attique,</l>
					<l>Quand son ciseau se vouait à ses dieux,</l>
					<l>De Praxitèle héritier poétique,</l>
					<l>Reprends, reprends ton vol audacieux,</l>
					<l>Suis ton essor, laisse tracer à d’autres</l>
					<l>Trop de héros fameux par leurs forfaits,</l>
					<l>L’humanité possède ses apôtres :</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits <del hand="RR" reason="analysis" type="repetition">(bis)</del>.</l>
				</lg>
				<lg n="2">
					<l>Sous ses liens la plaintive Italie</l>
					<l>Murmure encore un chant de liberté,</l>
					<l>Au souvenir des fils de Cornélie</l>
					<l>Son front flétri rêve l’égalité.</l>
					<l>Frères martyrs, leur héroïque exemple</l>
					<l>Peut des tyrans ébranler les palais ;</l>
					<l>En attendant qu’on leur élève un temple,</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits.</l>
				</lg>
				<lg n="3">
					<l>Sur les Français, quand un lâche monarque</l>
					<l>Laissait régner Albion et le deuil,</l>
					<l>Vint une femme, et le bras de la Parque</l>
					<l>Sur les Bretons étendit son lincueil.</l>
					<l>La hache en main, d’une horde en furie</l>
					<l>Une autre Jeanne a préservé Beauvais ;</l>
					<l>Plus d’une femme a sauvé la patrie.</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits.</l>
				</lg>
				<lg n="4">
					<l>Quatre-vingt-neuf, sur la grande famille</l>
					<l>Vit un soleil briller, puis se ternir,</l>
					<l>Mais le boulet qui sapa la Bastille</l>
					<l>A défriché les champs de l’avenir.</l>
					<l>Des nains sans cœur, las ! aux temps où nous sommes</l>
					<l>Vers de beaux noms en vain lancent leurs traits.</l>
					<l>La grande époque enfanta de grands hommes :</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits.</l>
				</lg>
				<lg n="5">
					<l>Avaient-ils peur ces tribuns énergiques</l>
					<l>Qui sous la foudre improvisaient la loi ?</l>
					<l>Avaient-ils peur, quand leurs bras athlétiques</l>
					<l>Jetaient aux rois la poussière d’un roi ?</l>
					<l>Leurs corps sanglants ont roulé sur l’arène,</l>
					<l>Mais leur grand cœur ne leur mentit jamais ;</l>
					<l>Pour les venger de l’oubli, de la haine,</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits.</l>
				</lg>
				<lg n="6">
					<l>Fille du Temps, mère de la Justice,</l>
					<l>La Vérité marche, et, malgré l’erreur,</l>
					<l>De la Raison l’éloquente milice</l>
					<l>Chez les méchants a porté la terreur.</l>
					<l>Aux plébéiens le doux chantre d’Émile,</l>
					<l>Aux travailleurs, Saint-Simon, Lamennais !</l>
					<l>Du genre humain ont préché l’évangile.</l>
					<l>Ah ! donne-nous ! donne-nous leurs portraits.</l>
				</lg>
				<closer>
					<signed><hi rend="smallcap">Louis VOITELAIN.</hi></signed>
					<p>
						Imp. de Pollet et Comp., rue Saint-Denis, 380.
					</p>
				</closer>
			</div>
			<div type="poem" key="VTL3">
				<head type="main">LOUIS NAPOLÉON.</head>
				<head type="sub">Paroles du citoyen Louis VOITELAIN.</head>
				<head type="tune">
					Air du Forçat libéré (<hi rend="smallcap">G. Verry</hi>), <lb/>
					ou de la Fête des Martyrs (<hi rend="smallcap">G. Leroy</hi>).</head>
				<lg n="1">
					<l>Toi qui naquis au soleil de l’Empire,</l>
					<l>Toi, qu’on bercé des chants victorieux,</l>
					<l>Enfant des camps, quoi ! ton orgueil aspire</l>
					<l>A nous courber sous un joug glorieux !</l>
					<l>Pauvre insensé ! las ! tu te crois encore</l>
					<l>Aux jours où Mars ornait ton lit de fleurs,</l>
					<l>Ce n’est pas toi qui dois sécher nos pleurs,</l>
					<l>Ce n’est pas toi que la Patrie implore.</l>
				</lg>
				<lg n="2">
					<l>Fuis, faible aiglon, la sainte humanité</l>
					<l>Ne veut s’unir qu’avec la Liberté !</l>
				</lg>
				<lg n="3">
					<l>D’un nom fameux, vomi par la tempête,</l>
					<l>Tu veux, en vain, te parer aujourd’hui ;</l>
					<l>Quand jusqu’aux cieux l’aigle élevait la tête,</l>
					<l>Du passereau se montra-t-il l’appui ?</l>
					<l>Non, sous son vol créant de nouveaux maîtres,</l>
					<l>Des opprimés il méconnut la voix,</l>
					<l>A prix de sang achetant ses exploits,</l>
					<l>Il enfanta des ingrats et des traîtres.</l>
				</lg>
				<lg n="4">
					<l>Fuis,<subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">faible aiglon, la sainte humanité</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Ne veut s’unir qu’avec la Liberté !</add></subst></l>
				</lg>
				<lg n="5">
					<l>Oui, sur un nom ton fol espoir se fonde,</l>
					<l>Mais, pourrait-il seconder ton élan ?</l>
					<l>Va, ce grand nom qui fit trembler le monde,</l>
					<l>Sur un rocher perdit son talisman ;</l>
					<l>Vois l’Univers saluer en silence</l>
					<l>Cet avenir qui vient malgré les rois ;</l>
					<l>Cet avenir, si riche de nos droits,</l>
					<l>Parle à nos cœurs plus haut que ta naissance.</l>
				</lg>
				<lg n="6">
					<l>Fuis,<subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">faible aiglon, la sainte humanité</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Ne veut s’unir qu’avec la Liberté !</add></subst></l>
				</lg>
				<lg n="7">
					<l>Vois, devant toi ce Peuple qui s’écoule</l>
					<l>N’espère plus t’élever au pavois ;</l>
					<l>En vain, tes cris se perdent dans la foule,</l>
					<l>Aucun écho ne répond à ta voix,</l>
					<l>Oui, c’en est fait du sceptre des conquêtes,</l>
					<l>Et, quand tu viens pour alourdir nos fers,</l>
					<l>L’égalité prépare ses concerts,</l>
					<l>Et la nature attend ses jours de fêtes.</l>
				</lg>
				<lg n="8">
					<l>Fuis,<subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">faible aiglon, la sainte humanité</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Ne veut s’unir qu’avec la Liberté !</add></subst></l>
				</lg>
				<lg n="9">
					<l>Un calme affreux a glacé tes séïdes ;</l>
					<l>La pitié seule ose prier pour toi,</l>
					<l>Maudis, maudis des conseillers avides,</l>
					<l>Bénis le ciel, tu ne seras pas Roi.</l>
					<l>Des potentats les flatteurs éphémères</l>
					<l>Sur ton butin jamais ne s’ébattront,</l>
					<l>Jamais le temps ne ridera ton front</l>
					<l>Sous un bandeau teint du sang de tes frères.</l>
				</lg>
				<lg n="10">
					<l>Fuis,<subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">faible aiglon, la sainte humanité</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Ne veut s’unir qu’avec la Liberté !</add></subst></l>
				</lg>
				<lg n="11">
					<l>Fuis, pauvre aiglon, retourne vers ton aire,</l>
					<l>N’évoque plus un grand nom du tombeau.</l>
					<l>Las ! tu l’as vu, ton essor téméraire</l>
					<l>Ne pourrait rien sur un peuple nouveau.</l>
					<l>Du conquérant qui maîtrisa la France,</l>
					<l>Va, pour jamais l’autel est renversé ;</l>
					<l>Décore-toi des lauriers du passé,</l>
					<l>Mais laisse-nous les fleurs de l’Espérance !</l>
				</lg>
				<lg n="12">
					<l>Fuis, faible aiglon, la sainte humanité</l>
					<l>Ne veut s’unir qu’avec la Liberté !</l>
				</lg>
				<closer>
					<p>
						────────────────────────────────── <lb/>
						PARIS. – IMPRIMERIE DE BEAULÉ ET MAIGNAND, <lb/>
						rue Jacques de Brosse, 8.<lb/>
					</p>
				</closer>
			</div>
			<div type="poem" key="VTL4">
				<head type="sub_2">Librairie chansonnière de DURAND, éditeur, rue Rambuteau, 3</head>
				<head type="main">A RASPAIL.</head>
				<head type="sub_1">Paroles de Louis VOITELAIN.</head>
				<head type="tune">Air du <hi rend="ital">Retour en France</hi>, ou de <hi rend="ital">Vive Paris.</hi></head>
				<lg n="1">
					<l>Ami du pauvre, étoile humanitaire,</l>
					<l>Dont la lumière a raffermi mes sens,</l>
					<l>En souriant, ma muse prolétaire</l>
					<l>A ton savoir consacre son encens ;</l>
					<l>Je me sens mieux, l’espoir est dans mon âme,</l>
					<l>J’en reviendrai malgré la Faculté.</l>
					<l>De tes fourneaux attise encor la flamme,</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<lg n="2">
					<l>Un Esculape, en me montrant Bicêtre,</l>
					<l>M’a dit : ce lieu calmera vos ébats ;</l>
					<l>Là, de bons soins vous sauveront peut-être,</l>
					<l>Vous êtes fou… Moi, j’ai souri tout bas,</l>
					<l>A me venger déjà mon luth s’apprête,</l>
					<l>Mais la souffrance a miné ma gaîté.</l>
					<l>J’ai vingt refrains amassés dans la tête…</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<lg n="3">
					<l>Non, ma raison ne s’est pas émoussée,</l>
					<l>Aux souvenirs mon cœur n’est point fermé,</l>
					<l>Dans le chaos où s’éteint la pensée,</l>
					<l>Non, mon esprit ne s’est point abimé ;</l>
					<l>Je hais toujours ces fripons qu’on décore,</l>
					<l>Mes vœux toujours sont à l’humanité ;</l>
					<l>Quoiqu’appauvri, mon sang bouillonne encore…</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<lg n="4">
					<l>Mais, chut ! dis-tu, maint argus me surveille ;</l>
					<l>Dame Thémis qu’effraya ma couleur,</l>
					<l>A mes rivaux, un jour, prêtant l’oreille,</l>
					<l>M’a défendu d’être utile au malheur.</l>
					<l>Va, pour les bons il est un saint refuge,</l>
					<l>Va, ton triomphe est dans l’éternité,</l>
					<l>Tes juges là rencontreront un juge…</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<lg n="5">
					<l>Tu les verrais, nos oppresseurs avides</l>
					<l>A ta couronne ajouter des fleurons ;</l>
					<l>Si tu pouvais déraciner les rides</l>
					<l>Par la débauche empreinte sur leurs fronts.</l>
					<l>Eux te bénir ? Non, dans la haute sphère,</l>
					<l>L’ingratitude a le droit de cité ;</l>
					<l>Des pauvres seuls pleuraient sur le Calvaire.</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<lg n="6">
					<l>Oui, je suis mieux ; oui, j’aspire au vieil âge ;</l>
					<l>J’ai vu s’enfuir la moitié de mes maux.</l>
					<l>Mais la tempête a causé du ravage,</l>
					<l>L’arbre a ployé, redresse ses rameaux.</l>
					<l>Qu’à ses banquets l’avenir me convie !</l>
					<l>Loin du rescif où je fus arrêté,</l>
					<l>Je veux sombrer en regrettant la vie…</l>
					<l>Fais un effort ! et rends-moi la santé.</l>
				</lg>
				<p>
					<hi rend="bold">Moyens préservatifs contre le Choléra.</hi><lb/>
					Évitez de manger tout ce qui occasionne un dérangement de 	corps,
					ne buvez jamais d’eau pure ni aucune boisson rafraîchissante,
					telle que bière, limonade gazeuse, orgeat, 	groseille. Buvez tous
					les soirs, avant de vous coucher, une bonne tasse de thé avec de
					la camomille bien sucrée, en y ajoutant un verre de rhum.<lb/>
					Nourriture épicée et aromatique ; vin généreux.<lb/>
					Trois fois par jour croquer gros comme un pois de camphre et l’avaler
					au moyen d’un quart de verre d’eau salée (une poignée de sel gris
					dans un litre d’eau).<lb/>
					Tous les quatre jours prendre avant dîner 10 centigrammes d’aloès
					(deux grumeaux).
					Soir et matin se frictionner le corps avec de l’alcool camphré et se
					frictionner pendant cinq minutes à la pommade camphrée.
					<lb/><lb/>
					<hi rend="bold">Moyens curatifs.</hi><lb/>
					Dès que les premiers symptômes se déclarent, avaler un petit verre
					d’eau-de-vie camphrée, ou bien un petit verre de la liqueur suivante :
					Eau-de-vie, 1 litre ; Camphre, 20 centigrammes ; Aloès, 10 centigrammes ;
					Canelle, 1 gramme ; Safran, 10 centigrammes ; Muscade, 20 centigrammes.<lb/>
					On peut mêler la liqueur à une livre de sucre fondu au feu, avec un
					demi-litre d’eau.<lb/>
					Lavement à la graine de lin, avec une pincée de sel gris, un gramme
					d’aloès et un dé à coudre d’huile camphrée.<lb/>
					Lotions incessantes à l’eau sédative, frictions incessantes, tantôt
					à l’alcool camphré, tantôt à la pommade camphrée, surtout sur le ventre.
				</p>
				<closer>
					<p>
						───────────────────────────────────── <lb/>
						Imp. de Beaulé et Maignand, rue Jacques de Brosse,
					</p>
				</closer>
			</div>
			<div type="poem" key="VTL5">
				<head type="main"><hi rend="smallcap">Adieux au Village</hi></head>
				<head type="tune">Air : <hi rend="ital">Allez cueillir des bluets dans les blés.</hi></head>
				<lg n="1">
					<l>Ah ! si jadis Virgile, en son délire,</l>
					<l>Fêta les champs dans ses vers pleins de feu,</l>
					<l>C’est que l’ennui ne glaçait point sa lyre ;</l>
					<l>C’est qu’il chantait sous un ciel toujours bleu.</l>
					<l>Mais moi, pauvret, devant l’âtre qui brille,</l>
					<l>Je rêve, hélas ! à des foyers chéris.</l>
					<l>L’air est plus doux où l’âme est en famille,</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<lg n="2">
					<l>On m’avait dit : ta santé chancelante</l>
					<l>Dans un air pur bientôt va refleurir.</l>
					<l>Je n’en crois rien : l’arbuste qu’on transplante,</l>
					<l>Au moindre choc doit s’attendre à périr.</l>
					<l>Pauvre docteur, ton espoir fut un rêve,</l>
					<l>L’ennui toujours décima les proscrits ;</l>
					<l>Où l’on naquit la vie a plus de sève,</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<lg n="3">
					<l>En m’éloignant de la moderne Athène,</l>
					<l>Je crus aux champs trouver la charité ;</l>
					<l>Mais qu’ai-je vu ? l’opulence hautaine</l>
					<l>Plus dure encor qu’au sein de la cité.</l>
					<l>J’ai des heureux vu la phalange avide</l>
					<l>De la moisson ramasser les débris ;</l>
					<l>J’ai des glaneurs vu la corbeille vide.</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<lg n="4">
					<l>Ne croyant point aux dévoûments sublimes,</l>
					<l>L’erreur, ici, soulève des tombeaux,</l>
					<l>Pour blasphémer d’héroïques victimes</l>
					<l>Qui du servage ont brisé les anneaux.</l>
					<l>Va, peuple ingrat ! attente à la mémoire</l>
					<l>De fiers tribuns que tu n’as pas compris.</l>
					<l>Moi, pour chanter les géants de l’histoire,</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<lg n="5">
					<l>En vain, ici, j’accorderais ma lyre</l>
					<l>Pour activer la chute des méchants,</l>
					<l>A mes refrains nul n’oserait souscrire,</l>
					<l>Pas un écho ne redirait mes chants.</l>
					<l>Peut-être même un cri liberticide</l>
					<l>Accueillerait mes plébéïens écrits.</l>
					<l>Ah ! pour semer sur un sol moins aride,</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<lg n="6">
					<l>Fille des cieux, amitié fraternelle,</l>
					<l>Qui sur mes maux verse des flots d’amour,</l>
					<l>J’entends au loin ta voix qui me rappelle,</l>
					<l>La coupe en main, tu guettes mon retour.</l>
					<l>Sur mon passé pour voir tomber un voile ;</l>
					<l>Dans l’avenir pour bercer mes esprits ;</l>
					<l>Pour raviver ma pâlissante étoile,</l>
					<l>Village, adieu ! je retourne à Paris.</l>
				</lg>
				<closer>
					<signed><hi rend="smallcap">Voitelain.</hi></signed>
					<p>
						[Dans <hi rend="ital">Le Républicain lyrique</hi> n° 6, <lb/>chez Durand / Beaulé et Maignand.]
					</p>
				</closer>
			</div>
			<div type="poem" key="VTL6">
				<head type="main">La Grand’-Mère.</head>
				<head type="tune">Air : <hi rend="ital">v’li</hi>, <hi rend="ital">v’lan</hi>.</head>
				<lg n="1">
					<l>Un jour, quand j’étais tout petit,</l>
					<l>Que je pleurais à perdre haleine,</l>
					<l>Ma grand’mère à bas de son lit</l>
					<l>Sauta, malgré sa soixantaine ;</l>
					<l>Puis, me prenant entre ses bras,</l>
					<l>Pour m’apaiser m’a dit tout bas :</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Tu n’es pas au bout de ta peine,</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Garde tes larmes pour tantôt.</l>
				</lg>
				<lg n="2">
					<l>Ton père, mon pauvre chéri,</l>
					<l>Quoique maintenant il t’embrasse,</l>
					<l>Quand tu poussas ton premier cri,</l>
					<l>Fit une piteuse grimace ;</l>
					<l>Il avait raison, sur ma foi !</l>
					<l>On se serait passé de toi,</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Dieu n’a rien mis dans ta besace,</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="3">
					<l>Hier, d’un fléau destructeur</l>
					<l>La violence fut extrême ;</l>
					<l>Tout est gelé… le percepteur</l>
					<l>Assure qu’on paîra quand même.</l>
					<l>De par le ciel, de par l’impôt,</l>
					<l>Rien dans la huche et rien au pot.</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Nous allons faire un long carême,</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="4">
					<l>Tu jeûneras plus d’une fois…</l>
					<l>Lorsque tu reçus l’existence</l>
					<l>Tes cinq aînés, qui sont au bois,</l>
					<l>Avaient déjà maigre pitance ;</l>
					<l>Un eût suffi, mais six… hélas !</l>
					<l>Guillot-le-Riche n’en a pas.</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Ta mère en a fait pénitence.</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="5">
					<l>Hâte-toi de grandir pourtant,</l>
					<l>Ton appétit fait du ravage ;</l>
					<l>Une bêche est là qui t’attend,</l>
					<l>Pour apprivoiser ton courage ;</l>
					<l>L’école ! il n’y faut pas songer,</l>
					<l>Avant de lire il faut manger.</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Pas de hochets pour ton jeune âge !</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="6">
					<l>Le bon Dieu qu’il faut adorer,</l>
					<l>Quoique parfois sa foudre gronde,</l>
					<l>Créa le soleil pour dorer</l>
					<l>Les grains que la terre féconde :</l>
					<l>De grands mangeurs, pour leurs sillons,</l>
					<l>Ont accaparé ses rayons.</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Il ne luit pas pour tout le monde,</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="7">
					<l>Plus maltraité que la fourmi,</l>
					<l>Qui, l’hiver, se repaît au gîte,</l>
					<l>Le pauvre, par l’âge blémi,</l>
					<l>N’est plus qu’une plante maudite.</l>
					<l>L’arbre sans fruit qu’on met à bas,</l>
					<l>Meurt, mais du moins ne languit pas.</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Les malheureux vieillissent vite,</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<lg n="8">
					<l>Quand tu seras père à ton tour,</l>
					<l>Au lieu d’écouter ta colère ;</l>
					<l>Lorsque du fruit de ton amour</l>
					<l>La plainte sera trop amère ;</l>
					<l>A sa clameur pour mettre un frein,</l>
					<l>Rappelle-toi de mon refrain :</l>
					<l><space unit="char" quantity="6"/>Dodo, mon petiot,</l>
					<l>Je le tiens de feu mon grand-père ;</l>
					<l><space unit="char" quantity="6"/>Dodo, <subst hand="RR" reason="analysis" type="repetition"><del hand="RR" type="repetition" reason="analysis">etc.</del><add rend="hidden">mon petiot,</add></subst></l>
					<l><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Garde tes larmes pour tantôt.</add></subst></l>
				</lg>
				<closer>
					<signed>Louis<hi rend="smallcap"> Voitelain.</hi></signed>
					<p>[Dans <hi rend="ital">Le Républicain lyrique n°8,</hi> <lb/>chez Durand / Beaulé et Maignand.]</p>
				</closer>
			</div>
			<div type="poem" key="VTL7">
				<head type="main">Le Père Jérôme</head>
				<head type="tune">Air : <hi rend="ital">Ma marmotte a mal au pied.</hi></head>
				<lg n="1">
					<l>Je me souviens qu’à dix-huit ans</l>
					<l><space unit="char" quantity="4"/>(Age que je regrette),</l>
					<l>Pour tirer parti des instants,</l>
					<l><space unit="char" quantity="4"/>Je fuyais l’étiquette</l>
					<l><space unit="char" quantity="8"/>A cache-pot,</l>
					<l><space unit="char" quantity="8"/>Bravant l’impôt,</l>
					<l><space unit="char" quantity="4"/>Sous un abri de chaume,</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu</l>
					<l><space unit="char" quantity="8"/>Du vin du crû</l>
					<l><space unit="char" quantity="4"/>Chez le père Jérôme !</l>
				</lg>
				<lg n="2">
					<l>Jérôme était un vieux troupier ;</l>
					<l><space unit="char" quantity="4"/>Son vin était potable :</l>
					<l>Tel qu’il entrait dans son cellier,</l>
					<l><space unit="char" quantity="4"/>Il le servait à table.</l>
					<l><space unit="char" quantity="8"/>Là, l’âge d’or</l>
					<l><space unit="char" quantity="8"/>Avait encor</l>
					<l><space unit="char" quantity="4"/>Son bachique fantôme.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu, <del hand="RR" type="repetition" reason="analysis">etc.</del></l>
					<l><space unit="char" quantity="8"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Du vin du crû</add></subst></l>
					<l><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Chez le père Jérôme !</add></subst></l>
				</lg>
				<lg n="3">
					<l>Aux souvenirs de maint Bayard,</l>
					<l><space unit="char" quantity="4"/>J’ouvrais des yeux humides :</l>
					<l>L’un nous parlait de Saint-Bernard,</l>
					<l><space unit="char" quantity="4"/>L’autre des Pyramides ;</l>
					<l><space unit="char" quantity="8"/>Puis, du Kremlin</l>
					<l><space unit="char" quantity="8"/>Jérôme enfin</l>
					<l><space unit="char" quantity="4"/>Nous charbonnait le dôme.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu, <del hand="RR" type="repetition" reason="analysis">etc.</del></l>
					<l><space unit="char" quantity="8"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Du vin du crû</add></subst></l>
					<l><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Chez le père Jérôme !</add></subst></l>
				</lg>
				<lg n="4">
					<l>En maudissant les chefs flétris</l>
					<l><space unit="char" quantity="4"/>Dans la lutte dernière,</l>
					<l>Nous entonnions les chants proscrits</l>
					<l><space unit="char" quantity="4"/>Par la blanche bannière :</l>
					<l><space unit="char" quantity="8"/>Là, de Capet</l>
					<l><space unit="char" quantity="8"/>Un plat valet</l>
					<l><space unit="char" quantity="4"/>N’eût été qu’un atôme.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu, <del hand="RR" type="repetition" reason="analysis">etc.</del></l>
					<l><space unit="char" quantity="8"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Du vin du crû</add></subst></l>
					<l><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Chez le père Jérôme !</add></subst></l>
				</lg>
				<lg n="5">
					<l>Quand Érigone à nos cerveaux</l>
					<l><space unit="char" quantity="4"/>Inspirait son délire,</l>
					<l>Nous remettions à nos chapeaux</l>
					<l><space unit="char" quantity="4"/>Les couleurs de l’empire ;</l>
					<l><space unit="char" quantity="8"/>Puis, d’Austerlitz</l>
					<l><space unit="char" quantity="8"/>Mes vieux amis</l>
					<l><space unit="char" quantity="4"/>Rêvaient le second tome.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu, <del hand="RR" type="repetition" reason="analysis">etc.</del></l>
					<l><space unit="char" quantity="8"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Du vin du crû</add></subst></l>
					<l><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Chez le père Jérôme !</add></subst></l>
				</lg>
				<lg n="6">
					<l>D’une mâle et franche amitié</l>
					<l><space unit="char" quantity="4"/>Chacun était l’apôtre.</l>
					<l>Par routine et non par pitié,</l>
					<l><space unit="char" quantity="4"/>On payait l’un pour l’autre :</l>
					<l><space unit="char" quantity="8"/>On n’eut pas fait</l>
					<l><space unit="char" quantity="8"/>De l’intérêt</l>
					<l><space unit="char" quantity="4"/>Pour tout l’or du royaume.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu, <del hand="RR" type="repetition" reason="analysis">etc.</del></l>
					<l><space unit="char" quantity="8"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Du vin du crû</add></subst></l>
					<l><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add rend="hidden">Chez le père Jérôme !</add></subst></l>
				</lg>
				<lg n="7">
					<l>Mais, hélas ! la mort à ses lois</l>
					<l><space unit="char" quantity="4"/>A soumis ce vieux brave…</l>
					<l>Partout où je trinque, je vois</l>
					<l><space unit="char" quantity="4"/>Rôder des rats de cave :</l>
					<l><space unit="char" quantity="8"/>L’égalité</l>
					<l><space unit="char" quantity="8"/>A déserté</l>
					<l><space unit="char" quantity="4"/>La cabane de chaume.</l>
					<l><space unit="char" quantity="8"/>Ah ! que j’ai bu</l>
					<l><space unit="char" quantity="8"/>Du vin du crû</l>
					<l><space unit="char" quantity="4"/>Chez le père Jérôme !</l>
				</lg>
				<closer>
					<signed><hi rend="smallcap">Louis Voitelain.</hi></signed>
					<p>[Dans <hi rend="ital">Le Favori des Dames n°3,</hi> <lb/>chez Durand / Beaulé et Maignand.]</p>
				</closer>
			</div>
		</body>
	</text>
</TEI>
