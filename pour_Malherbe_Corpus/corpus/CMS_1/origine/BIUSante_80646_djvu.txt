
Skip to main content
Full text of "Les sonnets du docteur -- [avec un front. de G. Clairin et 1 eau-forte de Félicien Rops ; dédicace de Charles Monselet]"
See other formats














, 



Les Sonnets 

du Docteur 


^ ^ 



I 








8ü6 4 6 








é/f c 


Les Sonnets 

du Docteur 



80u4ô 

PARIS 

chez la plupart des libraires 
1884 




# # 

•t # # 



LES 

SONNETS DU DOCTEUR 

t t 
t 



JUSTIFICATION DES TIRAGES . 


350 exemplaires sur papier de Hollande. 
125 . — sur simili-Japon. 

25 — sur Japon impérial. 






/ ‘ytn^ tte- 

'O^ ; -t^i^ ■if'^y f*^/^‘^^/"' 

^ 

-<W^ i^'iuyi /‘^ '''^^ 

COA 1^ A-t-nA- cyi'‘<AAnAn^i ^ 

^ /-CAA. 





X .>.ï\ N riVbinvuV 1:-»^'-si<>S^ 

•\w^ 'vs^:i s<^ ■■vs>,'-\ ^ st\ ^ 

si«;'VviijiNvtv*» ..■f<;T\ku\ s'i ) 

'■'«■JV '\ 1;JV> \'.!t\I'ï*S^ >V^.V\^'^ ^■ 

“^'X 's'''''i^^ A<l;.vss^s^ 






V i»t^\ i 


î'»\-''^'<t *•». |\^ »i^-I^'V5>^' , 

5 ». .V«^ .>>,r\-M< J 

' è»-4 ^«^V>. * ^''■*''\'î-<'.^tv,'''^' \4<rvt''Xî .- 

i ;!« ■>'«^>.'1 } 


















LES- 

Sonnets du Docteur 

AVANT-PROPOS 

L ORsauE j’étais impatient 

La Muse m’a dit : « Je suis tendre. 

« Je n’amène pas le client.... 

« Mais je console de l’attendre. » 





LE CATAPLASME 


F laccidité, tiédeur, mollesse humide et douce! 

Cataplasme douillet, topique velouté, 

Trésor de bonhomie et de sincérité. 

Tu caresses encor la main qui te repousse! 

Que tu sois de fécule ou de graine de lin. 

Que l'opium (arrose ou que le chloroforme 
Apporte dans tes plis l’apaisement énorme, 

Tu t’appliques toujours consolant et câlin. 

La batiste (abrite en sa trame serrée. 

En dépit du tissu, ton cœur médicinal 
S’imprègne avidement de sanie enfiévrée. 

A travers le rideau du confessionnal 
Ainsi le prêtre vient, onctueux et banal. 

Éponger les aigreurs de notre âme ulcérée. 




ECCHYMOSES 


M élie a fini d’être sage 

Et s’en mord les doigts maintenant. 
Des taches d’un bleu chagrinant 
Marbrent sa nuque et son corsage. 

Ses compagnes d’apprentissage 
Hochent la tête en la menant 
Près d’un herboriste éminent^ 

Oracle attitré du passage. 

Et la nigaude d’exposer 
Un vallon noir, des sommets roses, 

Où l’autre pour herboriser 

Trouve un parterre d’ecchymoses. 

Livides fieurs d’alcôve écloses 
Sous la ventouse du baiser. 


& 


- 9 — 




CALVITIE 


C oiffeur! Tu me trompais quand par tes artifices 
Tu disais raffermir mes cheveux défaillants. 
Ceux qu’avaient épargnés tes fers aux mors brûlants. 
Tu les assassinais d’eaux régénératrices! 

Tu m’as causé, coiffeur, de si grands préjudices 
Que je te voudrais voir, ayant perdu le sens. 

Sur toi-même épuiser tes drogues corruptrices 
Et tourner contre toi tes engins malfaisans. 

Ainsi, quand l’ouragan s’abat sur la futaie. 

D’un souffle destructeur il arrache et balaie 
La verte frondaison qui jonche le chemin. 

Au bocage pareil, mon front est sans mystère. 

Il ne me reste plus un cheveu sur la terre. 

Et je gémis, songeant au crâne de Robin! 

f 




BANDAGES & APPAREILS 


D ans la vitrine, où l’œil jette un regard oblique, 
Apollon et Vénus prêtent leurs nudités 
A des enlacements d’appareils brevetés. 

Ils servent, dieux captifs, d’enseigne à la boutique.. 

Un bandage inguinal à pelote élastique 
Etreint Cypris la blonde et masque ses beautés. 
L’acier flexible et fort, en détours éhontés. 

Suit amoureusement la courbe hypogastrique. 

Sur la gorge et les flancs divins, je vois encor. 
Bannissant la chlamyde et la ceinture d’or. 

Des ressorts médaillés à Paris, Vienne et Londre. 

O crime 1 — Et cependant Eros, confus et las. 

Levant un lourd faisceau de sondes en ses bras. 
Semble implorer le ciel pour l’homme qui s’effondre. 

S 




LE COR AUX PIEDS 


J E suis le cor aux pieds, et c’est moi qui proteste 
Contre le cordonnier et son cuir oppresseur. 
L’élégance m’impose un joug que je déteste. 

Je veux que tu sois libre, ô phalange, ma sœur ! 

En vain le pédicure, arrondissant le geste, 

D'un scalpel magistral me sculpte en professeur. 
Son triomphe est d’un jour, car le terrain me reste 
Et j’y renais plus fort sous le fer agresseur. 

Insensé! Tu voudrais, comprimant la nature, 

Faire admirer un pied trop grand pour ta chaussure. 
Le bottier, ton complice, est aussi ton bourreau. 

Qu’un aveugle instrument nous taille et nous harcèle, 
La persécution redouble notre ^èle. 

Oignons, durillons, cors, nous narguons Galopeau. 





APPÉTIT 


U N concours imprévu d’affaires ennuyeuses 

« Me prive, Cher Monsieur, du plaisir d’assister 
« A ce dîner où vous vouliez bien m’inviter. 

« Mille regrets. Navré. Circonstances fâcheuses... » 

Ceite lettre était courte et les autres verbeuses ; 

Mais sur un fond commun tous ils avaient brodé. 

Ils me lâchaient, devant un repas commandé 
Où se multipliaient les sauces onctueuses. 

Quel guignon 1— Quand soudain, au détour du Rat Mort, 
J’aperçois Béchamel. Je l’attache à mon sort. 

Cet homme est, à lui seul, un essaim de convives. 

Je ne me lassais pas de le voir s’occuper. 

Tout autre m’eût donné les craintes les plus vives; 
Mais lui, dès le dessert:—« Où pourrions-nous souper?^ 






LE VER SOLITAIRE ‘ 

B ien avant que Fourier rêvât le Phalanstère, 

Bien avant Saint-Simon et le Père Enfantin, 
Dans les retraits ombreux du petit intestin 
Le Solium déjà pratiquait leur chimère. 

Un cestdide obscur, un simple entoqoaire 
Avait constitué l’État républicain. 

Martyr voué d’avance au remède africain. 

Salut, fils du Scole.v, pâle et doux Solitaire! 

Tes anneaux, dont chacun forme un ménage uni. 
Sur un boyau commun prospèrent à l’envi. 

L’un à l’autre attachés, pas plus sujets que maîtres. 

Oui, c’est un beau spectacle, et l’on doit respecter 
Le sentiment profond qui me pousse à chanter 
En vers de douqe pieds le ver de doutée mètres ! 


- 14 — 





BLENNORRHAGIE 


D iEpx! Qu’ila l'airfaroucheet qu’ilfaitmalàvoir! 

Écumantet meurtri comme un loup pris au piège. 
En ses flancs déchirés grince un fer de rasoir. 

Qui l’abreuve? Chopart. Et qui le nourrit? Mège. 

Eux cependant, blottis au fond du suspensoir 
Dont le souple réseau les berce et les protège, 
Pareils à deux oiseaux frileux, fuyant la neige, 

Ils reposent, et rien n’émeut leur nonchaloir. 

Ne rappellent-ils pas, tant leur retraite est douce, 

Acis et Galatée endormis sur la mousse 

Dans la grotte qui vit leurs amours; et, sur eux, 

La main crispée au sol, le Cyclope hideux 
Penchant son œil unique, où la rage impuissante 
Lentement fait couler une larme brûlante? 


[5 — 



LES ENGELURES 


L 'affreux petit collège où l’on dut m’interner 
Ressemblait, en hiver, à ce cercle du Dante 
Où dans la glace on voit les gens se démener. 
L’économe était d’une avarice impudente. 

Près d’un poêle mourant, la classe grelottante 
Se morfondait, tuant le temps à griffonner. 

Et quatre fois par jour descendait piétiner 
Un préau ténébreux, lac de neige fondante. 

Sur nos doigts crevassés, sur nos tnentons bleuis 
L'engelure empourprée incrustait ses rubis. 

Et nos pieds enrageaient, dévorés de brûlures. 

C’était dur. Et pourtant, j’aime ce souvenir. 
Enfant, f ignorais tout des soucis à venir. 

O jeunesse, reviens ! Revenez, engelures! 



- i6 - 




MASSAGE 


D ans les nuits sans sommeil l’amour vous a blêmie 
Et vos chairs ont perdu leur tonus, 6 ma sœur! 
Maintenant il vous faut confier au masseur 
Les trésors alanguis de votre anatomie. 

Ointes d’une huile ambrée, effort de la chimie. 

Ses mains, en qui la force épouse la douceur. 
Pressent le grand-dorsal, malaxent l’extenseur. 

Pour des combats nouveaux vous voilà raffermie. 

Jadis votre docteur, plein de calme aujourd’hui. 

Massait fougueusement sur des lits de pervenches . 

Il opère à présent pour le compte d’autrui. 

Tel, plongeant ses bras nus au sein des pâtes blanches. 
Le gindre enfariné, dévêtu jusqu’aux hanches. 

Pétrit des petits pains — qui ne sont pas pour lui. 



- 17 -■ 




LE RHUME DE CERVEAU 


O u donc t’ai-je pincée, absurde phlegmasie, 
Stupide cory:{a, catarrhe insidieux? 

Mon pouls est enfiévré, ma pensée obscurcie. 

Coule![, ma pituitaire, et vous, pleure^, mes yeux ! 

L’éternuement secoue en vain mon inertie. 

Pidoux avec Trousseau, docteurs judicieux. 
N’opposant qu’un mouchoir au mal capricieux. 
Croient qu’il faut le traiter par la diplomatie. 

Eh bien! Je resterai farouche en mon fauteuil. 

Les pieds sur les chenets et condamnant mon seuil ; 
L’isolement convient à ma face piteuse. 

Et f aurai des mouchoirs en nombre indéfini. 

J’en veux mouiller autant qu’un évêque en bénit. 

Car je n’ai plus d’espoir qu’en vous, ma blanchisseuse! 



- i8 - 



DERMATOLOGIE 


S ous les rideaux discrets^ au fond du vieil hospice, 
Les sylphes du Midi, chantés par Fracastor, 
Donnent à leurs amants qui sommeillent encor 
Des baisers dont la trace est une cicatrice. 

La rougissante Acné, l’agaçante Ecqéma, 

Chéloïs au front pur. Syphilis au cœur tendre, 

Purpura, Sycosis, Éphélis, Ecthyma 

Sur la peau des mortels préférés vont s’étendre. 

Le jour luit. Une horde envahit les dortoirs. 

Portant tabliers blancs avec paletots noirs. 

Ce sont les ennemis des virus et des lymphes. 

Ils vont, et devant eux marche le professeur. 

Comme un faune jaloux qui s’avance, grondeur. 

Pour troubler vos ébats amoureux, belles nymphes. 


- 19 - 



DICHOTOMIE 


D ix-huit cents médecins sous le ciel de Paris 

Parmi les maux humains répandent des formules; 
Les uns, cœurs généreux ou martyrs ridicules 
Du dévouement sans borne et du labeur sans prix; 

Les autres, professant un élégant mépris 
Pour le client naïf qu’ils gorgent de granules; 

En haut quelques savants, princes, principicules ; 

En bas quelques rêveurs, des sots, des incompris. 

Mais les plus étonnants dans la docte cohorte 
Sont ces courtiers qui vont quêtant de porte en porte 
Le cas chirurgical et rémunérateur; 

Puis, quand ils ont semblé partager sa besogne, 
Confraternellement partagent, sans vergogne. 

L’or sanglant mis aux pieds du Grand Opérateur. 




AUSCULTATION 


C omment! C’est toi, belle Margot? 

— a Mais oui, m’sieu Paul, et f m’épouvante. 
« Quel malheur pour un’ pauv’ servante! 

« Mais quoi qu’j’ai donc ben dans 1’jabot? 

« Pourvu qu’ça s’rait pas quéqu’ pierrot! 

« Ça m’porte au cœur, ça m’grouilV dans l’vent’e! 
« Pas comm’ vous, moi; j’suis pas savante. 

(I P’t-êf ben qu’vous m’en dire{ l’fin mot, » 

— « ....Là donc! Baisse encor ta chemise!.... » 
Complaisamment l’oreille est mise 
Sur deux seins plus durs qu’inhumains; 

Et, dans des gestes téméraires, 

L’Étudiant à pleines mains 
Palpe ses premiers honoraires. 



STRABISME 

A M'i» C.... artiste dramatique. 

J ’ai toujours fortement goûté la beauté louche. 

Des axes visuels l’imperturbable écart 
Met un pouvoir étrange en son vague regard: 

Même en s’humanisant il reste encor farouche. 

Comme pour démentir les aveux de la bouche, 

L’œil boudeur se détourne et, nous poussant à bout, 
Semble tout refuser quand l’autre accorde tout. 
Inquiet, l’amant cherche un accent qui le touche. 

Danaé, ton coup d’œil va troubler à la fois 
Ceux du parterre et ceux du paradis. — Je crois 
Que je vais formuler un vœu très égoïste. 

Je voudrais — cache au moins ce sourire moqueur — 

Être galant autant que je suis oculiste 

Pour fixer, à moi seul, ton regard et ton cœur. 




MALADIES SECRÈTES 


M arquis de Rambuteau, f aime ces labyrinthes 
Dont ta main paternelle a.semé nos trottoirs. 
Leur front lumineux porte au sein des brouillards noirs 
Le nom des Bodegas et des Eucalypsinthes. 

Leurs murs sont diaprés du faîte jusqu’aux plinthes 
D’avis offerts gratis à d’amers désespoirs ; 

Et c’est pourquoi f entends, le long des réservoirs, 
Dans le gazouillement des eaux, monter des plaintes. 


O l’anxieux regard du malade éperdu 

Quand il franchit ton seuil, temple du copahul 

Moi, f en sors souriant, car j’eus des mœurs austères. 

Mes organes sont purs comme ceux des agneaux. 
L’âge les rend peut-être un peu moins génitaux. 

Mais ils sont demeurés largement urinaires. 


- 23 - 





MAIGREUR 

A S. B., de la Comédie-Française. 

Z EUS, qui te façonna dans un roseau flexible, 

Le cueillit sur les bords où disparut Syrinx ; 
Puis il s’arrêta court, ayant fait ton larynx, 

Luth vivant, qu’il dota d’une gaine impossible. 

Il économisa la matière tangible, 

Et les chastes panneaux signés Pérugin pinx., 

Et la scène où l’on voit agoniser Le Sphinx 
N’exhibèrent jamais corps plus irréductible. 

Arrêtant la jumelle au cran qui fait voir gros. 

Mon œil inquisiteur évoque le mirage 
D’un embonpoint fictif étranger à tes os. 

Et cherche à pallier l’erreur de son ouvrage. 

Mais que de charme encor dans cet étui tout secl 
Pourquoi n’avoir pas mis un peu de chair avec 1 



- 24 - 




DIGESTION 


A petits coups j’achève un excellent café, 

Et, d’un doigt de cognac détergéant l’œsophage, 
Je digère, plongé dans l’odorant nuage 
Qui s’exhale des plis d’un havane étojfé. 

Décidément le chef a partout triomphé. 

Des hors-d’œuvre au rôti, du poisson au fromage. 

Pas un seul plat qui n’ait reçu mon double hommage; 
Toi surtout, sein fécond du dindonneau truffé 1 

Dans le fauteuil berceur où mes vertus chancellent. 
Des hoquets innocents tour à tour me rappellent 
Tantôt la bisque rose et tantôt les foies gras. 

Les yeux mi-clos, f entame un rêve bucolique. 

Mais quel est ce parfum soudain et magnifique? 

La truffe a murmuré : « C’est moi!.... Ne le dis pas! » 

— 25 — 





— 26 — 




LE SPÉCULUM 


C ATINETTE, CM quelquB aventure 
S’étant éraillé le satin, 

Va consulter un beau matin. 

On la hisse. Elle est en posture. 

Un tube d’étroite ouverture, 

Dans un pâle reflet d’étain 
Guide le regard incertain 
Au sein de sa riche nature. 

Voilà le bobo découvert. 

A nous la flamme, à nous le fer! 
Mais — ô faiblesse de la bête! — 

Son cautère à peine soufflé. 
L’opérateur, courbant la tête, 
Adore ce qu’il a brûlé. 

f 


— 27 — 



PHTHIRIASE 


R ome va s’endormir aux pieds d’un nouveau maître. 

En ce jour, aux sons clairs envolés de l’airain, 
Le pape Sixte a mis sur son front souverain 
La couronne du roi, du guerrier et du prêtre. 

Pensif il est assis à la haute fenêtre 
Et goûte la fraîcheur du soir dans l’air serein. 

Or, la mystique voix d’un Phthirius pèlerin, 

Dans un prurit dont la caresse le pénètre. 

Monte, reconnaissante, et dit: « O mon appui! 

« Te souvient-il des temps lointains où, pauvres hères, 
n: Nous gardions les troupeaux en traînant nos misères, 

« Nous, que le monde acclame et révère aujourd’hui ? 
« Ah! fût-il mille fois plus qu’Hercule robuste, 

« Nul ne m’arrachera de ta personne auguste! » 



- 28 — 



ÉPIDÉMIES, ENDÉMIES 


G ange, 6 Père des eaux^soUs tes bambous trompeurs 
Où la tanigartchie emplit ses urnes blanches, 
Exhalé des limons impurs court dans les branches 
Un frisson pestilent fait de deuils et de peurs. 

Des humus violés s’élèvent en vapeurs 
Le vomito, la fièvre, implacables revanches; 

Et les fléaux, roulant comme des avalanches, 

Fraient leur route à travers l’angoisse et les stupeurs. 

Ainsi, pour accomplir ses vengeances hautaines, 

La Nature asservie en soulevant ses chaînes 
Frappe ironiquement son maître souffreteux. 

Partout quelque endémie à l’homme fait cortège. 

Et l’Alpe même abrite en ses girons de neige 
Le crétin puéril et myxœdémateux. 



-29- 



CHLOROSE 


J E ne veux pas savoir le nombre d’hématies 
Que la chlorose avare a laissé dans ton sang. 

Je ne veux pas compter sur ton front languissant 
Les pétales restés à tes roses transies. 

Pauvre enfant! le nerf vague, aux mille fantaisies, 
Donne seul à ton cœur son rhythme bondissant; 
Seul il rougit parfois ton visage innocent 
De l’éclat sans chaleur des pudeurs cramoisies. 

Pour le dompter veux-tu connaître un moyen sûr? 
N’épuise plus en vain les sources martiales, 

Mais laisse-toi conduire aux choses nuptiales. 

. Au soleil de l’amour ouvre tes yeux d’azur. 

Suis la loi, deviens femme, et qu'en ton sein expire 
■ Dans les blancheurs du lait la pâleur de la cire. 



- 3o - 




MÉDECINE LÉGALE 

« Casse-poitrim appellantur. » 
(Professeur Tardieu ) 

C OURBÉ SOUS le fardeau de son désir difforme. 

Sinistre, l’œil au guet, plus craintif que le faon. 
Le soir il va le long des berges. — C’est Alphand 
Qui sur leurs bords déserts a fait verdoyer l’orme. 

Là rôde encor cet être hybride dont la forme 
A des rondeurs de femme et des maigreurs d’enfant; 
Dont le col découvert et le veston bouffant 
Trahissent un organe infundibuliforme. 

Ils se sont devinés et rejoints. Le danger 

Les harcelant, ils vont — couple affreux héberger 

Sous la voûte aux trous noirs leur rut démoniaque. 

Enfin l’homme, assouvi, sort d’un pas vacillant 
Et fuit, rasant les murs, grisé d’ammoniaque. 

Son ambre, à lui, son musc et son ylang-ylang. 



- 3i - 





BONBON LAXATIF 


J E suis un aimable hypocrite^ 

Car je mens pour faire le bien. 
Je n’ai qu’un but et qu’un moyen : 
Plaire d’abord, guérir ensuite. 

Blanche comme une stalactite, 

Ma robe en sucre dit combien 
Je séduis le petit chrétien 
Pris par la gourme ou l’entérite. 

Craintive à l’ombre du danger, 

La maman court me mélanger 
A d’autres bonbons plus sincères. 

Mais Dieu guide le cher enfant. 

Il me choisit, m’avale et rend 
Le calme à ses petits viscères. 







CONGESTION CÉRÉBRALE 


U N soir qu’il se sentait la visière moins nette. 

Mon grand-oncle Bernard, vert encor, mais très 
S'inspirant d’un menu savant et copieux, [vieux. 
Fit largement honneur aux talents de Jeannette. 

Puis son menton pesa plus lourd sur la serviette; 

Un chœur de feux-follets dansa devant ses yeux. 

Et, son âme quittant la table pour les deux. 

Il mourut doucement, le nes[ sur son assiette. 

Seigneur, Seigneur mon Dieu, je suis à vos genoux! 
Écouteq un pécheur qui tremble devant vous. 

Et vous redoute autant qu’il craint l’anorexie. 

Quand je serai plus vieux que mon oncle, et plus bas. 
Comme dernier dessqrt de mon dernier repas, 
Accorde^-moi, Seigneur, la douce apoplexie! 



- 33 - 





‘i? 4 


CONSTIPATION 


R ien ne venait. Huit jours d’un régime torride 
L’avaient comme encloué. Sa plume retenait 
La copie implorée en vain ; rien ne venait. 

Le cæcum restait sourd et le cerveau stupide. 

Contracté par l’effort, le chroniqueur turgide, 
Qu’une oscillation stérile promenait 
Du cabinet d’étude à l’autre cabinet, 

Froissait avec fureur un papier toujours vide. 

Ohl'demeurer ainsi vissé dans l’acajou 
Sur le trône où périt Cæsar Elagabale ! 

Son cœur se brise; il prie et pleure. Tout à coup 


L'appareil a vibré sous un choc de scybale. 

La débâcle est immense et la plume s’emballe : 
Tabatabataba... agaga... otiloulou... 


- 35 - 



HOMARD NATURE 


L e homard est enfin sorti du court-bouillon. 

Au sein de la mixture épicée et brûlante 
Il vient de revêtir son harnais vermillon 
Qu’il étale, couché dans l’herbe verdoyante. 

Piquant comme un cactus, dur comme un mirmillon. 
Il oppose au couteau son armure savante; 

Vain refuge, où ma main ferme et persévérante 
Creuse, d’un bout à l’autre, un énorme sillon. 

O chair incarnadine et pâle de la queue! 

Pinces, qui me faisieq naguère une peur bleue! 
Anfractuosités que f adore fouiller! 

Votre alléchant fumet trouble les plus bégueules, 

Et mon cœur bat plus fort lorsque le sommelier 
Met le Sauternes d’or près du Homard de gueules. 


- 36 - 







LES GAUDES 


A ux sommets du Jura le ciel pâlit à peine. 

L’oiseau n’a pas encor quitté l’abri des bois, 

Et déjà, s’échappant du front des humbles toits, 

La fumée en flots gris se répand sur la plaine. 

Levée avant le jour et tournant le fuseau, 

La mère est là, veillant, près du feu qui pétille. 

Sur la marmite où chante et s’épaissit dans l’eau 
Le maïs blond, régal de la jeune famille. 

A son appel, voilà les enfants réunis; 

Et c’est plaisir de voir leurs museaux réjouis 
Baignés dans la vapeur de leurs assiettes chaudes. 

Puis dans les prés, où l’aube éclaire leur chemin, 

S’en vont, poussant les bœufs, une perche à la main, 
Les petits Francs-Comtois tout barbouillés de gaudes. 


-37- 





DIFFAH 


A u seuil de la maison, dont la blancheur éclate 
Dans l’a:{ur transparent du ciel algérien. 
Parmi les arbres verts au bigarre maintien. 

Où le fruit d’or se mêle à la fleur écarlate. 

Le maître-rôtisseur de Ben-Ali-Chérif 
Promène un goupillon plein de graisse brûlante 
Sur les flancs d'un agneau qui cuit, mets primitif, 
Percé de part en part d’une perche sanglante. 

Treille, dont le rideau nous cachait au soleil. 

Tu nous vis attaquer un festin nonpareil 

Près du bassin de marbre où l’eau rit et s’élance; 

Et, joyeux mécréants, nés pour scandaliser 
Le Prophète et sa loi, tu nous vis arroser 
Le rôti du désert des meilleurs vins de France. 




- 38 - 




LE HOMARD A LA COPPÉE 


C ’ÉTAIT un tout petit homard de Batignolle. 

Nous l’avions acheté trois francs, place Bréda. 
En vain, pour le payer moins cher, on marchanda; 
Le fruitier, cœur loyal, n’avait qu’une parole. 

Nous portions le cabas tous deux, à tour de rôle. 
Comme nous arrivions aux remparts, Amanda 
Entra dans un débit de vins et demanda 
Deux setiers. — Le soleil dorait sa tete folle I 

Puis, ce furent des cris, des rires enfantins. 

Elle avait un effroi naïf des intestins 
Dont, je dois l’avouer, l’odeur était amère... 

Nous revînmes le soir, peu nourris, mais joyeux. 

Et d’un petit homard nous fîmes trois heureux. 

Car elle avait gardé les pattes -pour sa mère! 



- 39 — 





LANGUE FUMÉE 

A M...., par colis postal. 

P AS d'aube. Le soleil surgit. Il illumine 

Les grèves de Mannâr et les flots attiédis. 
Noirs démons oubliés dans ces verts paradis., 

Les Cinghalais, gagnant quelque roche marine, 

S’élancent dans l’abîme où dort là pintadine. 

Sous le faix du butin ils nagent, alourdis ; 

Puis, dans la chair nacrée ouvrant leurs doigts hardis. 
De son écrin vivant tirent la perle fine. 

Je suis, par le courage, à ces pêcheurs pareil. 

Des hauteurs .de Paris plongeant dans la province. 

Où je ne puis revoir, hélas I qu’en mon sommeil, 

Monselet pourléchant sa double badigoince. 

J’arrache au goufre amer un trésor sans rival ; 

Cette langue signée Aubelle-Méneval. 

(DIJON) 


? ? 
? 


— 40 - 



PONDÉRATION 


N euf mois juste après les derniers épithalames 
Il naquit, pesant trois kilos exactement ; 

Et dès lors, chaque jour, par un allaitement 
Méthodique, il s’accrut d’un nombre entier de grammes. 

Existence rivée aux chiffres, aux programmes. 

Il contrôla des Poids pour le Gouvernement. 

Quand il avait du vague à l'âme, — rarement — 

Il lisait les Tarifs douaniers, ces dictâmes! 

Son repas lui coûta toujours trente-deux sous. 

Sobre avec les buveurs, correct avec les fous. 

Il ne connut jamais les débauches exquises; 

Mais subissant l’attrait fatal des numéros. 

Tous les samedis soir il allait aux plus gros 
A prix-fixe acheter des caresses précises. 


— 41 — 





DISCOURS DE RÉCEPTION 

à la Société « la Cigale » 


M essieurs, je ne suis pas cigalier de naissance; 

Les Francs-Comtois n' ont pas cet avantage. Aussi 
La Camargue m’ignore, et c’est tout au plus si 
Le long du P.-L.-M. j’entrevis la Provence. ■ 

Entre nos arts divers un trait de ressemblance 
M’aura valu l’accueil que je rencontre ici. 

Nos arts sont libéraux. Mot pompeux t Mais, voici. 
Leur libéralité n’est pas ce que l’on pense. 

Poètes, médecins, peintres, étudiants. 

Les affres des débuts nous trouvent souriants 
Et plus gais, sous nos toits, que Gobseck dans son antre. 

Pour donner un symbole aux serviteurs du beau, 

La noble Insouciance attaché à leur drapeau 
Un insecte qui chante en se brossant le ventre. 




TRANSFORMISME 




S ous les Océans noirs à peine refroidis, 

Spongiaire naissant bercé dans le blastème, 

Je pris des bras, je fus le ^oophyte abstême ; 

Un test, je me nommai mollusque, et j’attendis. 

Cent mille ans je vécus poisson. Instants maudits ! 

Les schistes m’écrasaient 1 Soudain jusqu’au ciel même 
L’aile jn’emporte, oiseau. Je marche, je grandis; 

Me voilà cétacé, ruminant, monotrême. 

Le Pôle me surprend mammouth. Au creux des rocs 
J’égrène çà et là mes ossements d’aurochs. 

Je n’avais pas encor la malice du singe I 

Mais un jour je deviens /'Ancêtre vénéré, 

■Le père de Darwin, l’oncle du bon Littré. 

A pressent je suis /'HOMME, et je porte du linge. 



— 43 — 







I 



Table 


Avant-propos. 7 

Le Cataplasme. 8 

Ecchymoses. 9 

Calvitie .. lo 


Le professeur Charles Robin, membre de l’Institut, etc. 

Bandages et appareils. 11 

Le Cor aüx pieds. 

Appétit.. ■ ' ^ 

Le Ver solitaire. H 

Le remède africain, le iausso d’Abyssinie, pays où le ver solitaire 
est très commun. — Scoîex, embryon du tænia. 

Blennorrhagie. 

Potion Chopari, capsules thCègt. 

Les Engelures. 

Massage. '7 

Le Rhume de cerveau. 

Fiàoux et Trmisseau, auteurs d’un célèbre Traité de thérapeutique. 

Dermatologie . ^9 

Le Midi, hôpital spécial. 

Frdcasior, syphiliographe italien du xvi<s siècle. — Et poète ! 


- 45 
















Dichotomie ... !.. 20 

De §\y^(X, en deux parties, et T0|!jtvî, partage. — Branche très 
florissante de la chirurgie contemporaine. 

Auscultation. 21 

Strabisme. 22 

Maladies secrètes. 2 3 

Les Bodegas sont des établissements de mastroquets exotiques où les 
vins de Cette se vendent en espagnol [aqui se habla), — Et 
VEucalypsinthe ! Rôve d’un liquoriste marseillais qui prétendait 
avec l’essence d’eucalyp-ius imiter l’ab-sinthe ! 

Maigreur . . . . ■. 24 

Digestion. 25 

Préservatifs. ' 26 

Millant, frère du gros Milknt, philanthrope bien connu dans le 
quartier du Palais-Royal, 

Le Spéculum . . . i. 27 

Phthiriase. 28 

Epidémies, endémies. 29 

Tamgartchiey porteuse d’eau dans l’Hindoustan.— !Kyxœâème, dégé- 
nérescençe des tissus particulière aux crétins. 

Chlorose.'. 3 o 

Hématies, globules rouges du sang. — Le nerf vague, aussi peu 
connu sous le nom de pneimogasirique. — Sources martiales, fer¬ 
rugineuses. 

médecine légale. 3 i 


On n’a pas oublié les aventures postéro-judiciaires de M. de G., 
et du capitaine V. .. 

Du culte secret tous deux prêtres, 

Obscuri per syîvas îbant; 

Et, comme nos pieux ancêtres, 

Fidem reciumque colehanl. 


Bonbon laxatif. 32 

Congestion cérébrale.. 33 


'L'anorexie est la perte de l’appétit. Qjaelle perte ! 


— ^6 — 
































’Vo.i-LEl 


U N gros événement agite le ménage. 

Madame a mis au monde hier — une souris! 
Les époux consternés contemplent, ahuris, 

Ce produit exigu d/un très long mariage. . 

Mais, la mère un instant songe et reprend courage. 
Au maillot, où l’enfant jette ses premiers cris. 

Elle coud des rubans, des dentelles de prix. 

Si bien que le foetus devient un personnage. 

Ainsi j’ai fait pour toi, mon petit souriceau. 

Elzévir a brodé les langes du trousseau ; 

La Hollande en tissa la toile tout entière; 

Des burins glorieux ont paré ton berceau; 

Enfin, pour t’amener de l’ombré à la lumière, 
Guttenberg étant mort, j’ai choisi Darantière. 



- 44 - 















«mvsæ noster 






f 











