Les Syrtes


Jean Mor�as



SYRTES - REMEMBRANCES


p7

Dans l' �tre br�lent les tisons,
les tisons noirs aux flammes roses ;
dehors hurlent les vents moroses,
les vents des vilaines saisons.
Contre les chenets roux de rouille,
mon chat frotte son maigre dos.
En les ramages des rideaux,
on dirait un essaim qui grouille :

p8

c' est le pass�, c' est le pass�
qui pleure la tendresse morte ;
c' est le bonheur que l' heure emporte
qui chante sur un ton lass�.


SYRTES - REMEMBRANCES, 1


p9

L�-bas, o�, sous les ciels attiques,
les cr�puscules radieux
teignent d' am�thyste les dieux
sculpt�s aux frises des portiques ;
o�, dans le feuillage argent�
des peupliers aux torses maigres,
cr�pitent les cigales aigres
ivres des coupes de l' �t� ;

p10

l�-bas, o� d' or fin sont les sables
et d' azur rythmique les mers,
o� pendent les citrons amers
dans les bosquets imp�rissables,
la vierge aux seins inapais�s
plus belle que la Tyndaride,
fit couler sur ma l�vre aride
le dictame de ses baisers.


SYRTES - REMEMBRANCES, II


p11

D' o� vient cette aubade c�line
chant�e-on e�t dit-en bateau,
o� se m�le un pizzicato
de guitare et de mandoline ?
Pourquoi cette chaleur de plomb
o� passent des senteurs d' orange,
et pourquoi la s�quelle �trange
de ces p�lerins � froc blond ?

p12

Et cette dame quelle est-elle,
cette dame que l' on dirait
peinte par le vieux Tintoret
dans sa robe de brocatelle ?
Je me souviens, je me souviens :
ce sont des d�funtes ann�es,
ce sont des guirlandes fan�es
et ce sont des r�ves anciens !


SYRTES - REMEMBRANCES, III


p13

Parmi des ch�nes, accoud�e
sur la colline au vert gazon,
se dresse la blanche maison,
de ch�vrefeuille enguirland�e.
� la fen�tre, o� dans des pots,
fleurit la p�le marguerite,
soupire une autre Marguerite :
mon coeur a perdu son repos... 

p14

le lin moule sa gorge plate
riche de candides aveux,
et la splendeur de ses cheveux
ainsi qu' un orbe d' or �clate.
Va-t-elle murmurer mon nom ?
Irons-nous encor sous les graves
porches du vieux burg des burgraves ?
Songe �teint, rena�tras-tu ? -non !


SYRTES - REMEMBRANCES, IV


p15

Hautes sierras aux gorges nues,
lacs d' �meraude, lacs glac�s,
isards sur les cr�tes dress�s,
aigles qui planez par les nues ;
sapins sombres aux larges troncs,
fondri�res de l' Ent�cade
o� chante la fra�che cascade
derri�re les rhododendrons ;

p16

et vous, talus plant�s d' yeuses,
irai-je encor par les sentiers
m�lant les rouges �glantiers
� la p�leur des scabieuses ?
Dans les massifs emplis de geais
m�nerai-je encore � la brune
la jeune belle � la peau brune,
au pied mignon, � l' oeil de jais ?


SYRTES - REMEMBRANCES, V


p17

En jupe de peluche noire,
avec des chapeaux tout fleuris,
mes folles amours de Paris
chantent autour de ma m�moire.
Elles ont des cheveux d' or pur,
et, sous les blanches cascatelles
des guipures et des dentelles,
des seins de lis vein�s d' azur.

p18

Avec une audace espagnole,
ma gourmande caresse n' a-
t-elle aux genoux de Rosina
moqu� les verrous de Barthole ?
N' ai-je pas promen� ma main,
avec des luxures d' artiste,
sous des chemises de batiste
embaumant l' ambre et le jasmin ?


SYRTES - REMEMBRANCES


p19

Contre les chenets roux de rouille
le chat ne frotte plus son dos.
En les ramages des rideaux
on n' entend plus d' essaim qui grouille.
Dans l' �tre pleins de noirs tisons,
eteintes sont les flammes roses ;
et seuls hurlent les vents moroses,
les vents des vilaines saisons.


SYRTES - BOUQUET A LA GRAEFIN


p21

Parc ducal. Le ciel fige en du smalt les branches.
Dans les nids, gazouillis d' oisels et d' oiselles.
Seigneurs tr�s chamarr�s, gentes damoiselles.
Des fleurs rouges, des fleurs jaunes, des fleurs
blanches.
Cheveux longs � la brise �pars, courbes hanches.
Vos l�vres s' irisaient de vin de Moselle.
J' ai hum� longuement vos yeux de gazelle,
derri�re les buissons piqu�s de pervenches.

p22

Vieux chambellan g�teux en culotte courte,
vous offrit, sur un plat d' argent, de la tourte,
avec un madrigal surann�, Graefin ;
il vous baisa le bout de votre main lisse ;
vous lui f�tes, je crois, des yeux en coulisse
et vous ne s�tes point que j' avais le spleen.


SYRTES - OTTILIE


p23

Des l�vres de bacchide et des yeux de madone,
des sourcils bifurqu�s o� le diable a son pleige ;
ses cheveux vaporeux que le peigne abandonne
sont couronn�s de fleurs plus froides que la neige.
Vient-elle de l' alc�ve ou bien de l' ossuaire,
lorsque ses mules d' or fr�lent les dalles grises ?
Est-ce voile d' hymen ou fun�bre suaire,
la gaze qui palpite aux vesp�rales brises ?

p24

Autour du burg, la lune, aux n�cromants fid�le,
filtre en gouttes d' argent � travers les ramures.
Et l' on entend fr�mir, ainsi que des coups d' aile,
des harpes, dans la salle o� r�vent les armures.


SYRTES - ODE - I


p25

Seins des femmes ! � seins de lis ! � seins de nacre !
Vos rythmes indolents dorlotent nos blessures.
Leurs l�vres ! Vous gardez, en vos calices l' �cre
saveur des bigarreaux et des grenades sures.
-mais, aux bords fabuleux des fleuves du Levant,
j' eus mes r�ves berc�s aux ghazels des P�ris ;
et, dans l' antre fatal, la dame de Mervent
scella mes yeux pensifs de ses baisers fleuris.


SYRTES - ODE - II


p26

Sur la nappe ouvrag�e o� le festin s' exalte,
la venaison royale alterne aux fruits des �les ;
dans les chypres et les muscats de Rivesalte,
endormeur des soucis, � L�th�, tu t' exiles.
-mais l' antique hippogriffe au vol jamais fourbu,
m' a port� sur son aile � la table des dieux ;
et l�, dans la clart� sid�rale, j' ai bu,
� pleine urne, les flots du nectar radieux.


SYRTES - ODE III

En ces �ges maudits insultant aux chim�res,
pareils aux hurlements impurs des filles so�les,
jusqu' � vos pieds d' argile, � gloires �ph�m�res,
montent les hosannas sacril�ges des foules.
-mais, sous les myrtes blancs de la sainte D�los
que baigne l' archipel de ses flux et reflux,
je crois ou�r mon nom �clatant dans les los
chant�s, en le futur, aux po�tes �lus. -


SYRT., MYSTIQ. SONT, LA-BAS, I


p27

mystiques sont, l�-bas, les clairs de lune bleus :
� votre front poli nimb� de clair de lune !
Berceuse est la chanson des archipels houleux :
� vos cheveux errants aux brises de la dune !


SYRT., MYSTIQ. ST LA-BAS, II


p28

Sous votre pied d' airain, Astart�, foulez-nous :
voici le Koh-innor, les jades de Palmyre !
�tes-vous la Madone ador�e � genoux ?
Mon �me montera comme un parfum de myrrhe !


SYRTES - TES MAINS


p29

Tes mains semblant sortir d' une tapisserie
tr�s ancienne o� l' argent � l' or brun se marie,
o� parmi les fouillis bizarres des ramages
se bossue en relief le contour des images,
me parlent de beaux rapts et de royale orgie,
et de tournois de preux, dont j' ai la nostalgie.

p30

Tes mains � l' ongle rose et tranchant comme un bec
durent pincer jadis la harpe et le rebec,
sous le dais incrust� du portique ogival
ouvrant ses treillis d' or � la fra�cheur du val,
et, pleines d' onction, rougir leurs fins anneaux
de chrysoprase, dans le sang des huguenots.
Tes mains aux doigts p�lis semblent des mains
de sainte
par Giotto r�v�e et pieusement peinte
en un coin tr�s obscur de quelque basilique
pleine de chapes d' or, de cierges, de reliques,
o� je voudrais dormir tel qu' un �v�que mort,
dans un tombeau sculpt�, sans crainte et sans remord.


SYRTES - ARIETTE


p31

Tu me lias de tes mains blanches,
tu me lias de tes mains fines,
avec des cha�nes de pervenches,
et des cordes de capucines.
Laisse tes mains blanches,
tes mains fines,
m' encha�ner avec des pervenches
et des capucines.


SYRTES - SENSUALITE


p33

N' �coute plus l' archet plaintif qui se lamente
comme un ramier mourant le long des boulingrins ;
ne tente plus l' essor des r�ves p�r�grins
tra�nant des ailes d' or dans l' argile infamante.
Viens par ici : voici les f�eriques d�cors,
dans du S�vres les mets exquis dont tu te s�vres,
les coupes de Samos pour y tremper tes l�vres,
et les divans profonds pour reposer ton corps.

p34

Viens par ici : voici l' ardente �rubescence
des cheveux roux piqu�s de fleurs et de b�ryls,
les �tangs des yeux pers, et les roses avrils
des croupes, et les lis des seins frott�s d' essence
viens humer le fumet-et mordre � pleines dents
� la banalit� suave de la vie,
et dormir le sommeil de la b�te assouvie,
d�daigneux des splendeurs des songes transcendants.


SYRTES, ASSEZ D' ABSTINENCES, I


p35

Assez d' abstinences moroses :
de Schiraz effeuillons les roses
au bord du lac sacr�,
et que pour moi l' amour ruisselle
de sa l�vre d' alme pucelle,
plus doux qu' un vin sucr�.


SYRTES, ASSEZ D' ABSTINENC., II

Assez de chrysolithe terne :
que l' on me montre la caverne
des kohinors-soleils,
et des saphirs plus bleus que l' onde,
et des clairs rubis de Golconde
au sang des dieux pareils.


SYRTES, ASSEZ D' ABSTINENC., III


p36

Assez d' existence servile :
que l' on m' emporte dans la ville
o� je serai le Khan,
infaillible comme un proph�te
et dont la justice parfaite
prodigue le carcan.


SYRTES, CONTE D' AMOUR, I


p37

La lune se mirait dans le lac taciturne,
p�le comme un grand lis, pleine de nonchaloirs.
-quel lutin nous versait les philtres de son urne ? -
la brise sanglotait parmi les arbres noirs...
baiser spirituel, son baiser, sois b�ni !
Dans mon coeur plein d' horreur tu ravivas la
flamme,
dans mon coeur plein d' horreur, mon pauvre coeur
terni.
-ai-je effleur� sa l�vre ? Ai-je hum� son �me ?


SYRTES, CONTE D' AMOUR, II


p38

Je veux un amour plein de sanglots et de pleurs,
un amour au front p�le orn� d' une couronne
de roses dont la pluie a terni les couleurs,
je veux un amour plein de sanglots et de pleurs.
Je veux un amour triste ainsi qu' un ciel
d' automne,
un amour qui serait comme un bois plant� d' ifs
o� dans la nuit le cor m�lancolique sonne ;
je veux un amour triste ainsi qu' un ciel
d' automne,
fait de remords tr�s lents et de baisers furtifs,


SYRTES, CONTE D' AMOUR, III


p39

mon coeur est un cercueil vide dans une tombe ;
mon �me est un manoir hant� par les corbeaux.
-ton coeur est un jardin plein des lis les plus
beaux ;
ton �me est blanche ainsi que la blanche colombe.
Mon r�ve est un ciel bas o� sanglote le vent ;
mon avenir, un tertre en friche sur la lande.
-ton r�ve est pur ainsi que la plus pure offrande,
ton avenir sourit comme un soleil levant.

p40

Ma bouche a les venins des fauves belladones ;
mes sombres yeux sont pleins des haines des maudits.
-ta bouche est une fleur �close au paradis,
tes chastes yeux sont bons comme ceux des madones.


SYRTES, CONTE D' AMOUR, IV


p41

Dans les jardins mouill�s, parmi les vertes branches,
scintille la splendeur des belles roses blanches.
La chenille stri�e et les noirs moucherons
insultent vainement la neige de leurs fronts :
car, lorsque vient la nuit tra�nant de larges voiles,
que s' allument au ciel les premi�res �toiles,
dans les berceaux fleuris, les larmes des lutins
lavent toute souillure, et l' �clat des matins
fait miroiter encor parmi les vertes branches
le peplum virginal des belles roses blanches.

p42

Ainsi, ma belle, bien qu' entre tes bras mutins
je sente s' �veiller des d�sirs clandestins,
bien que vienne parfois la sorci�re hyst�rie
me verser les poisons de sa bouche fl�trie,
quand j' ai lav� mes sens en tes yeux obsesseurs,
j' aime mieux de tes yeux les mystiques douceurs
que l' irritant contour de tes fringantes hanches,
et mon amour, absous de ses d�sirs pervers,
en moi s' �panouit comme les roses blanches
qui s' ouvrent au matin parmi les arbres verts.


SYRTES, CONTE D' AMOUR, V


p43

Bient�t viendra la neige au blanc manteau d' hermine ;
dans les parcs d�feuill�s, sous le ciel morne
et gris,
sur leurs socles, parmi les boulingrins fl�tris,
les priapes frileux feront bien triste mine.
Alors, ma toute belle, assis au coin du feu,
aux rouges flamboiements des b�ches cr�pitantes,
nous reverrons, au fond des visions latentes,
le paysage vert, le paysage bleu,

p44

le paysage vert et rose et jaune et mauve
o� murmure l' eau claire en les fouillis des joncs,
o� se dresse au-dessus des fourr�s sauvageons
le c�ne mena�ant de la montagne chauve.
Nous reverrons les boeufs, les grands boeufs
blancs et roux,
tra�nant des chariots sous l' ardeur tropicale,
et sur le pont tr�s vieux la tr�s vieille bancale
et le jeune cr�tin au ricanement doux.
Ainsi nous revivrons nos extases �teintes
et nous ranimerons nos bonheurs saccag�s
et nous ressentirons nos baisers �chang�s
dans les campagnes d' or et d' �meraude teintes.
H�las ! N' �coutant pas la voix des sorts moqueurs
et laissant mon esprit s' enivrer de chim�res,
je ne veux pas penser que les ondes am�res
vont se mettre bient�t au travers de nos coeurs.


SYRTES, CONTE D' AMOUR, VI


p45

Rouges comme un fer de forge
ou le taureau qu' on �gorge,
sous les regrets assassins
nos coeurs saignent dans nos seins.
Viennent donc des sorts propices
nous garer des pr�cipices !
Que nous nous serrions la main
sans souci du lendemain ;

p46

qu' enfin nous puissions sans tr�ve,
sans redouter l' heure br�ve,
sous les ciels profonds des lits
tordre nos corps affaiblis !


SYRTES, CONTE D' AMOUR, VII


p47

Hiver : la bise se lamente,
la neige couvre le verger.
Dans nos coeurs aussi, pauvre amante,
il va neiger, il va neiger.
Hier : c' �tait les soleils jaunes.
Hier, c' �tait encor l' �t�.
C' �tait l' eau courant sous les aulnes
dans le val de ma�s plant�.

p48

Hier, c' �tait les blancs, les roses
lis, les lis d' or �rubescent-
et demain : c' est les passeroses,
c' est les ifs plaintifs, balan�ant,
balan�ant leur verdure dense,
sur nos bonheurs ensevelis ;
demain, c' est la macabre danse
des souvenirs aux fronts p�lis ;
demain, c' est les doutes, les craintes,
c' est les d�sirs martyris�s,
c' est le coucher sans tes �treintes,
c' est le lever sans tes baisers.


SYRTES, CONTE D' AMOUR, VIII


p49

Ne ternis pas de pleurs les mystiques prunelles
de tes grands yeux navr�s, stri�s d' or et d' agate ;
laisse-la t' emporter, la berceuse fr�gate,
par les immensit�s des vagues solennelles.
Triste, je r�verai, pendant mes nuits moroses,
de baisers alanguis et de caresses brusques,
de nids capitonn�s o� des coupes �trusques
s' exhalent les ennuis des chlorotiques roses.

p50

Et l' absence irritant le d�sir qu' elle rive,
ma passion tenace o� le souvenir veille
montera dans mon coeur, d�bordante et pareille
aux fluviales eaux qui grondent sur la rive.


SYRTES, CONTE D' AMOUR, IX


p51

Nous marchions nous tenant par la main, dans la rue
o� sous les becs de gaz se heurte la cohue.
Sous les jasmins en fleur qui bordent le chemin,
� l' ombre nous marchions, nous tenant par la main.
Et ma joie est fan�e avec le blanc jasmin.
Sa voix, perlant tout bas ses notes argentines,
ber�ait mon coeur, ainsi qu' un psaume des matines.

p52

Son baiser acharn�, grisant comme les nuits,
faisait sourire encor mon front charg� d' ennuis.
Et mes bras veufs en vain la cherchent dans les
nuits.


SYRTES, CONTE D' AMOUR, X


p53

Ce jour-l�, les flots bleus susurreront plus bleus
le long des c�tes blanches,
et du soleil frileux, les rayons plus frileux
se joueront dans les branches.
Malgr� le rude hiver, les fleurs de l' �glantier
souriront grand' ouvertes,
et l' on verra changer les cailloux du sentier
en �meraudes vertes.

p54

Les loups pour les agneaux auront des soins exquis,
et sous l' oeil bon des aigles,
les grands vautours feront la cour, en fins marquis,
aux colombes espi�gles.
Les dames, aux propos galants des s�ducteurs,
ne seront pas rebelles,
et les Almavivas, malgr� les vieux tuteurs,
enl�veront leurs belles.
Car ce jour-l�, jour saint, vaillamment attendu,
dans tes chastes prunelles,
mes yeux retrouveront le paradis perdu
des amours �ternelles.
Car ce jour-l�, les coeurs par le bonheur bris�s,
mes l�vres dans les tiennes,
nous nous rappellerons en de nouveaux baisers
nos caresses anciennes.


SYRTES, CONTE D' AMOUR, XI


p55

La feuille des for�ts
qui tourne dans la bise
l�-bas, par les gu�rets,
la feuille des for�ts
qui tourne dans la bise,
va-t-elle revenir
verdir-la m�me tige ?
L' eau claire des ruisseaux
qui passe claire et vive

p56

� l' ombre des berceaux,
l' eau claire des ruisseaux
qui passe claire et vive,
va-t-elle retourner
baigner-la m�me rive ?


SYRTES, LES BONNES SOUVENANCES


p57

Irisant le ciel gris de nos mornes pens�es,
ravivant les soleils �teints des renouveaux,
elles passent toujours au fond de nos cerveaux,
un bon souris sur des l�vres jamais pliss�es.
Leur prunelle est l' aurore, et leur natte tress�e
est fulgurante ainsi que l' �clat des flambeaux.
Leur prunelle est la nuit, et, sur le cou mass�e,
leur chevelure est bleue ainsi que les corbeaux.

p58

Aux accords p�n�trants d' anciennes ritournelles,
elles bercent nos coeurs pleins d' ennui ; ce sont
elles
qui pansent doucement nos blessures mortelles,
elles qui, sur nos cils, viendront s�cher nos pleurs.
-et le temps, �mondeur de beaut�s et de fleurs,
met sur leur front vieilli de plus fra�ches
couleurs.


SYRTES, PARMI LES MARRONNIERS


p59

Parmi les marronniers, parmi les
lilas blancs, les lilas violets,
la villa de houblon s' enguirlande,
de houblon et de lierre rampant.
La glycine, des vases bleus pend ;
des gla�euls, des tilleuls de Hollande.
Ch�re main aux longs doigts d�licats,
nous versant l' or du sang des muscats,
dans la bonne fra�cheur des tonnelles,

p60

dans la bonne senteur des moissons,
dans le soir, o� languissent les sons
des violons et des ritournelles.
Aux plaintifs tintements des bassins
sur les nattes et sur les coussins,
les paresses en les flots des tresses.
Dans la bonne senteur des lilas
les soucis adoucis, les coeurs las
dans la lente langueur des caresses.


SYRTES, LA CARMENCITA


p61

Pauvre enfant, tes prunelles vierges,
malgr� leur feu diamant�,
dans mon coeur, temple d�vast�,
ne rallumeraient pas les cierges.
Pauvre enfant, les sons de ta voix
-telles les harpes s�raphiques-
de mes souvenirs mal�fiques
ne couvriraient pas les abois.

p62

Pauvre enfant, de tes l�vres vaines,
la miraculeuse liqueur
n' adoucirait pas la rancoeur
qui tarit la vie en mes veines.
Pareil au climat meurtrier
d�sert� de toute colombe,
et pareil � la triste tombe,
o� l' on ne vient jamais prier,
-� la trop tard-au cours du fleuve
in�luctable, je m' en vais,
ayant au gr� des vents mauvais
effeuill� ma couronne neuve.


SYRTES, DANS LA BASILIQUE, I


p63

Dans la basilique o� les p�les cierges
font briller les ors du grand ostensoir,
sur les feuillets des missels � fermoir
courent les doigts fins des pudiques vierges.
Elle t' attendait, la vierge aux yeux bleus,
mais tu n' as pas su lire dans ses yeux-
dans la basilique, aux clart�s des cierges.


SYRTES, DANS LA BASILIQUE, II


p64

Dans la chambre rose o� les lilas blancs
m�laient leurs parfums aux ti�deurs des b�ches,
cette pr�sidente en peignoir � ruches,
quand elle jouait avec ses perruches,
sangdieu ! Qu' elle avait des regards troublants !
Tu n' as pas cueilli les beaux lilas blancs,
tu n' as pas cherch� les secrets troublants
du peignoir � tra�ne avecque des ruches,
dans la chambre rose o� les lilas blancs
m�laient leurs parfums aux ti�deurs des b�ches.


SYRTES, OISILLON BLEU


p65

Oisillon bleu couleur-du-temps,
tes chants, tes chants
dorlotent doucement les coeurs
meurtris par les destins moqueurs.
Oisillon bleu couleur-du-temps,
tes chants, tes chants
donnent de nouvelles vigueurs
aux corps min�s par les langueurs.

p66

Oisillon bleu couleur-du-temps,
tes chants, tes chants
font revivre les espoirs morts
et terrassent les vieux remords.
Oisillon bleu couleur-du-temps,
je t' ai cherch� longtemps, longtemps,
par mont, par val et par ravin
en vain, en vain !


SYRTES, CHIMAERA


p67

J' allumai la clart� mortuaire des lustres
au fond de la crypte o� se r�vulse ton oeil,
et mon r�ve cueillit les fleuraisons palustres
pour ennoblir ta chair de p�leur et de deuil.
Je prof�rai les sons d' �tranges palatales,
selon les rites des tr�pass�s n�cromants,
et sur ta l�vre teinte au sang des digitales
ferment�rent soudain des philtres endormants.

p68

Ainsi je t' ai cr�� de la supr�me essence,
fant�me immarcessible au front d' astres nimb�,
pour me purifier de la concupiscence,
pour consoler mon coeur dans l' opprobre tomb�.


SYRTES, LES ROSES JAUNES


p69

Les roses jaunes ceignent les troncs
des grands platanes, dans le jardin
o� c' est comme un tintement soudain
d' eau qui s' �goutte en les bassins ronds.
Nul battement d' ailes, au matin ;
au soir, nul souffle couchant les fronts
des lis p�lis, et des liserons
p�lis au clair de lune incertain.

p70

Et dans ce calme o� la fra�cheur tombe,
c' est comme un apaisement de tombe,
comme une mort qui lente viendrait
sceller nos yeux de sa main cl�mente,
dans ce calme o� rien ne se lamente
ou par l' espace, ou par la for�t.


SYRTES, LE DEMONIAQUE


p71

Ai-je suc� les sucs d' innomm�s magist�res ?
Quel succube au pied bot m' a-t-il donc envo�t� ?
Oh ! Ne l' �tre plus, oh ! Ne l' avoir pas �t� !
Suc mal�fique, � magist�res d�l�t�res !
Point d' holocauste offert sur les autels des Tyrs,
point d' �pres cauchemars, d' affres �pileptiques !
Seuls les r�ves pareils aux ciels clairs des
tryptiques,
seuls les d�sirs nimb�s du halo des martyrs !

p72

Qui me rendra jamais l' hermine primitive,
et le lis virginal, et la sainte for�t
o�, dans le chant des luths, Viviane appara�t
versant les philtres de sa l�vre fugitive !
H�las ! H�las ! Au fond de l' Er�be �paissi,
j' entends r�ler mon coeur cribl� comme une cible.
-viendra-t-on te briser, sortil�ge invincible ? -
h�te-toi, h�te-toi, bon Devin, car voici
que l' automne se met � secouer les roses,
et que les jours rieurs s' effacent au lointain,
et qu' il va s' �teignant le suave matin :
-et demain, c' est trop tard pour les m�tamorphoses !


SYRTES, LES BRAS QUI SE NOUENT


p73

Les bras qui se nouent en caresses p�m�es,
le cordial bu du baiser animal,
les cheveux qu' on tord, les haleines hum�es,
des nerfs �nerv�s apaisent-ils le mal ?
� nos visions les toujours affam�es !
� les voeux sonnant ainsi qu' un faux m�tal !
En nos �mes, in�luctables N�m�es,
qui viendra terrasser le monstre fatal ?

p74

Et puisqu' il faut que toutes coupes soient br�ves,
puisqu' il faut en vain sur d' impossibles gr�ves
chercher le n�penth�s et le lotus d' or ;
ne vaudrait-il mieux le d�sir qu' on triture :
ne vaudrait-il mieux te voler ta p�ture,
d�go�t carnassier, � fun�bre condor !


SYRTES, ACCALMIE, I


p75

Lorsque sous la rafale et dans la brume dense,
autour d' un fr�le esquif sans voile et sans rameurs,
on a senti monter les flots pleins de rumeurs
et subi des ressacs l' �tourdissante danse,
il fait bon sur le sable et le varech amer
s' endormir doucement au pied des roches creuses,
berc� par les chansons plaintives des macreuses,
� l' heure o� le soleil se couche dans la mer.


SYRTES, ACCALMIE, II


p76

Que l' on jette ces lis, ces roses �clatantes,
que l' on fasse cesser les fl�tes et les chants
qui viennent raviver les luxures flottantes
� l' horizon vermeil de mes d�sirs couchants.
Oh ! Ne me soufflez plus le musc de votre haleine,
oh ! Ne me fixez pas de vos yeux fulgurants,
car je me sens br�ler, ainsi qu' une phal�ne,
� l' azur �toil� de ces flambeaux errants.

p77

Oh ! Ne me tente plus de ta caresse avide,
oh ! Ne me verse plus l' enivrante liqueur
qui coule de ta bouche-amphore jamais vide-
laisse dormir mon coeur, laisse mourir mon coeur.
Mon coeur repose, ainsi qu' en un cercueil d' �rable,
dans la s�r�nit� de sa conversion ;
avec les regrets vains d' un bonheur mis�rable,
ne trouble pas la paix de l' absolution.


SYRTES, ACCALMIE, III


p78

Feux libertins flambant dans l' auberge fatale
o� se vautre l' imp�nitence des d�go�ts,
o� mon �me a br�l� sa robe de vestale,
-eteignez-vous !
Par les malsaines nuits de crimes travers�es,
hippogriffes du mal, femelles des hiboux,
qui pr�tiez votre essor � mes l�ches pens�es,
-envolez-vous !

p79

Salamandres-d�sirs, sorci�res-convoitises
qui hurliez dans mon coeur avec des cris de loups
la persuasion de toutes les feintises,
-ah ! Taisez-vous !


SYRTES, ACCALMIE, IV


p80

J' ai trouv� jusqu' au fond des cavernes alpines
l' antique ennui nich�,
et j' ai meurtri mon coeur pantelant, aux �pines
de l' �ternel p�ch�.
� sagesse cl�mente, � d�esse aux yeux calmes,
viens visiter mon sein,
que je m' endorme un peu dans la fra�cheur des palmes,
loin du d�sir malsain.


SYRTES, ACCALMIE, V


p81

Mon coeur, mon coeur fut la lanterne
eclairant le lupanar terne ;
mon coeur, mon coeur, fut un rosier,
rosier pouss� sur le fumier.
Mon coeur, mon coeur est le blanc cierge
br�lant sur un cercueil de vierge ;
mon coeur, mon coeur est sur l' �tang
un chaste n�nuphar flottant.


SYRTES, ACCALMIE, VI


p82

� mer immense, mer aux rumeurs monotones,
tu ber�as doucement mes r�ves printaniers ;
� mer immense, mer perfide aux mariniers,
sois cl�mente aux douleurs sages de mes automnes.
Vague qui viens avec des murmures c�lins
te coucher sur la dune o� pousse l' herbe am�re,
berce, berce mon coeur comme un enfant sa m�re,
fais-le repu d' azur et d' effluves salins.

p83

Loin des villes, je veux sur les falaises mornes
secouer la torpeur de mes obsessions,
-et mes pensers, pareils aux calmes alcyons,
monteront � travers l' immensit� sans bornes.


SYRTES, MUSIQUE LOINTAINE


p85

La voix, songeuse voix de l�vres devin�es,
�parse dans les sons aigus de l' instrument,
� travers les murs sourds filtre implacablement,
irritant des d�sirs et des langueurs fan�es.
Alors, comme sous la baguette d' un sorcier,
dans mon esprit flottant la vision se calque :
blanche avec des cheveux plus noirs qu' un
catafalque,
fr�le avec des rondeurs plus lisses que l' acier.

p86

dans le jade se meurt la branche de verveine.
les tapis sont profonds et le vitrail profond.
les coussins sont profonds et profond le plafond.
nul baiser attristant, nulle caresse vaine. 
la voix, songeuse voix de l�vres devin�es,
�parse dans les sons aigus de l' instrument,
� travers les murs sourds filtre implacablement,
irritant des d�sirs et des langueurs fan�es.


SYRTES, ETRE SEREIN...


p87

�tre serein ainsi qu' un roc inaccessible,
sans souci de chercher l' oubli de ses pens�es ;
l' �me close aux sanglots des lyres cadenc�es,
aux r�ves hasardeux ne pas servir de cible.
Aux ors incandescents des tr�sors des Palmyres,
aux perles des Ophirs-aveugles ses prunelles ;
la vert�bre r�tive aux visions charnelles
�parses dans l' odeur �nervante des myrrhes.

p88

Le temps p�trifi� sur les feuillets du livre ;
le ciel du coeur uni comme un m�tal ; sans rides,
� sensibilit�, tes surfaces virides ;
l' aube pareille au cr�puscule : � ne pas vivre !


SYRTES, HOMO, FUGE, I


p89

Sur l' arbre et la b�te de somme,
sur le fauve altier, et sur l' homme
inutilement r�volt�,
monstre de pleurs et de sang ivre,
d�sir formidable de vivre,
tu fais peser ta volont�.


SYRTES, HOMO, FUGE, II


p90

Pour vaincre l' aust�re non-�tre
tu dis aux succubes de na�tre,
et de ta main tu prodiguas
les joyaux aux prostitu�es,
et les couronnes pollu�es
autour du front des ren�gats.


SYRTES, HOMO, FUGE, III

Expert en les dialectiques,
tu parles et tu sophistiques
avec ta voix de clair m�tal :
et les tentations pullulent,
et les tentations ululent
dans l' ombre du ravin fatal.


SYRTES, HOMO, FUGE, IV


p91

Car tu sais pour damner notre �me
faire jaillir la pure-flamme
dans l' oeil des hiboux et des freux ;
tu connais les accoutumances
des devins, et les nigromances
et les hocuspocus affreux.


SYRTES, HOMO, FUGE, V

Sous la com�te et sous la lune,
en tunique de pourpre brune,
tr�s blanche avec des cheveux blonds,
pr�s du lac o� nagent les cygnes,
ta feinte candeur a des signes
qui parlent des sentiers oblongs.


SYRTES, HOMO, FUGE, VI


p92

� travers les chaudes haleines
des tabacs et des marjolaines,
de nos voeux tu guides l' essor
o�, dans sa fi�re nonchalance,
la fleur-charnelle se balance
pareille au grand lis nimb� d' or.


SYRTES, HOMO, FUGE, VII

Mais ta promesse n' est que leurre !
Bient�t, bient�t sonnera l' heure
du chevalier au pied fourch�,
et nous savons bien que tu caches
sous les velours et les panaches,
toute la hideur du p�ch�.


SYRTES, HOMO, FUGE, VIII


p93

Oh ! Qu' il vienne un autre messie
secouer l' antique inertie,
qu' il vienne en ses r�demptions
d�truire l' oeuvre de la femme 
et te faucher, d�sir inf�me
des neuves g�n�rations.



Source: http://www.poesies.net