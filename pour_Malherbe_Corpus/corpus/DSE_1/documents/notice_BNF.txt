Type(s) de contenu et mode(s) de consultation : Texte noté : sans médiation

Auteur(s) : Duvert, Félix-Auguste (1795-1876) 
Voir les notices liées en tant qu'auteur
Saintine, X.-B. (1798-1865) 
Voir les notices liées en tant qu'auteur
Arago, Étienne (1802-1892) 
Voir les notices liées en tant qu'auteur

Titre(s) : Cagotisme et liberté, ou Les deux semestres [Texte imprimé], revue de l'année 1830, en deux parties, par MM. Duvert, Ernest (Boniface) et Étienne

Publication : Paris : J.-N. Barba, 1830

Description matérielle : 46 p. ; in-16

Note(s) : Paris, Vaudeville, 31 décembre 1830

Création : (France) Paris, Théâtre du Vaudeville, 1830-1
