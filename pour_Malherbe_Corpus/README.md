<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Corpus XML pour l'analyse

##### Ensemble du corpus des textes hors droits


▫ Les répertoires (voir le document **liste_des_recueils.txt** pour le code des auteurs et des recueils) :

* Code à 3 lettres pour les pièces de théâtre d'un auteur
	(exemple : **MOL** pour les pièces de théâtre de *Molière*)

* Code à 3 lettres+soulignement+numéro d'ordre pour les recueils de poésies d'un auteur
	(exemple : **BAU_1** pour le premier recueil de poésie de *Baudelaire*).

▫ Chaque répertoire contient :

* 3 sous-répertoires :
	+ **documents** : contient le ou les fichiers des éditions de référence (pdf, djvu...).
	+ **notes** : contient le fichier de prise de notes lors de la préparation et de l'analyse du texte
(Ce répertoire est vide dans la version mise en dépôt).
	+ **origine** : contient le fichier ou les fichiers à l'origine du texte numérisé.

* Un fichier **numéro.txt** qui contient un nombre incrémentiel pour la numérotation automatique des poèmes du recueil.
Ce numéro permet d'avoir une numérotation continue des poèmes d'un auteur.

* Le fichier xml du corpus initial : nom du répertoire suivi de _0.xml
	(exemple : **BAU_1_0.xml**). C'est ce fichier qu'il faut éventuellement modifier si une correction s'impose.

Le répertoire **corpus** est à copier dans le dossier **Malherbe_Corpus** du répertoire des programmes d'analyse (voir le dépôt **Programmes** du projet **Malherbe**).

Les  fichiers XML de ce répertoire se distinguent de ceux du répertoire 
**Corpus_Malherbe** dans la mesure où ils peuvent contenir des attributs relatifs
à des instructions pour l'analyse automatique ; par exemple, présence d'un attribut rhyme="none
pour des vers sans rimes. 

Les textes du répertoire **Corpus_Malherbe** sont créés à partir de ceux du présent répertoire par application d'une feuille de
transformation XSL qui supprime les éléments et attributs propres à l'analyse (liste_éléments_analyse.xsl et suppression_éléments_analyse.xsl).












