#!

# Validation XML d'un texte avec Xerces
# Exemple de ligne de commande : ./validation_xerces.sh BAU BAU_1

# Richard Renault, 2017


echo -e " \033[47m\033[30m - Validation du fichier : \033[0m\033[44m\033[1;37m $1 \033[0m\033[47m\033[1;30m $2 \033[0m"; tput sgr0

# création du répertoire LOG pour les fichiers de log (liste des erreurs)
if [ ! -d "../LOG" ] ; then mkdir  ../LOG ; fi

fichier_xsd=$(ls ../XSD/*.xsd)
schema_xsd=${fichier_xsd##*/}

# teste la présence de la première variable : nom de l'auteur
if [ -n "$1" ]
	then
		if [ -d "../Textes/$1" ]
			then
				# teste la présence de la seconde variable : nom du recueil
				if [ -n "$2" ]
					then
						if [ -d "../Textes/$1/$2" ]
							then

								java -jar ../XSD/xerces/xsd11-validator.jar -sf ../XSD/$schema_xsd -if ../Textes/$1/$2/$2.xml >/dev/null 2> ../LOG/${1%_0.xml}.log

								# -s = le fichier a une taille supérieure à zéro
								if [ -s ../LOG/${1%_0.xml}.log ]
									then
										echo -e "\033[33m  $n- Fichier $rep non valide ! (voir le fichier de log)\033[0m"; tput sgr0
									else
										echo -e "  $n- Fichier $rep validé !"
										rm ../LOG/${1%_0.xml}.log
								fi

							else
								echo -e " \033[40m\033[31m- $2 n'est pas un nom de recueil valide ! \033[0m"; tput sgr0
						fi
					else
						echo -e " \033[40m\033[31m- Il manque le nom du recueil (code du recueil) ! \033[0m"; tput sgr0
						echo -e " \033[40m\033[31m- exemple : ./validation_xml.sh BAU BAU_1  \033[0m"; tput sgr0
				fi
			else
				echo -e " \033[40m\033[31m- $1 n'est pas un nom de répertoire valide ! \033[0m"; tput sgr0
				echo -e " \033[40m\033[31m- exemple : ./validation_xml.sh BAU BAU_1  \033[0m"; tput sgr0
		fi
	else
		echo -e " \033[40m\033[31m- Il manque le nom du répertoire (code de l'auteur) ! \033[0m"; tput sgr0
		echo -e " \033[40m\033[31m- exemple : ./validation_xml.sh BAU BAU_1  \033[0m"; tput sgr0
fi

