#!

# Création de la liste des recueils et des pièces de théâtre par auteur
# Le fichier de sortie, au format XML, est ensuite transformé en HTML puis en PDF

# fichiers créés :
# Documents/Corpus_Malherbe_liste_des_textes.pdf
# Documents/Corpus_Malherbe_liste_des_textes_A5.pdf

# programme Python : Scripts/Python/liste_textes.py
# feuille de transformation xsl : Scripts/XSL/liste_textes_html.xsl
# feuille de style css : HTML/css/liste_textes.css

# Richard Renault, mai 2020

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────


echo -e " \033[44m\033[37m  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ \033[0m"; tput sgr0
echo -e " \033[44m\033[37m  ◼ Corpus Malherbe                                         \033[0m"; tput sgr0
echo -e " \033[47m\033[34m  ◼ Création du fichier XML de la liste des textes          \033[0m"; tput sgr0


# création du répertoire tmp 
if [ ! -d "../tmp" ];then mkdir ../tmp ;  fi

cd Python
# lancement du programme python 
echo -e "\033[40m\033[1;34m    ▪ Écriture du fichier XML \033[0m"; tput sgr0
python3 liste_textes.py ../../BDD/CSV/corpus_auteurs.csv ../../BDD/CSV/corpus_coauteurs.csv ../../BDD/CSV/corpus_recueils.csv ../../BDD/CSV/corpus_état.csv ../../BDD/CSV/corpus_licences.csv ../../tmp/liste_textes.xml

# transformation XML -> HTML
echo -e "\033[40m\033[1;34m    ▪ Transformation XML > HTML \033[0m"; tput sgr0
cd ..
saxonb-xslt -ext:on -s ../tmp/liste_textes.xml -o ../HTML/liste_textes.html -xsl XSL/liste_textes_html.xsl

# conversion HTML -> PDF
echo -e "\033[40m\033[1;34m    ▪ Conversion PDF \033[0m"; tput sgr0
wkhtmltopdf -q -T 20 -B 20 --enable-local-file-access ../HTML/liste_textes.html ../Documents/Corpus_Malherbe_liste_des_textes.pdf 2>/dev/null

# version A5 du document PDF
echo -e "\033[40m\033[1;34m    ▪ Version A5 \033[0m"; tput sgr0

pdftops ../Documents/Corpus_Malherbe_liste_des_textes.pdf ../tmp/Corpus_Malherbe_liste_des_textes.ps
psnup -2 -pa4 ../tmp/Corpus_Malherbe_liste_des_textes.ps ../tmp/Corpus_Malherbe_liste_des_textes_A5.ps 2>/dev/null
ps2pdf ../tmp/Corpus_Malherbe_liste_des_textes_A5.ps ../tmp/Corpus_Malherbe_liste_des_textes_A5.pdf

# mise en forme horizontal
pdftk ../tmp/Corpus_Malherbe_liste_des_textes_A5.pdf cat 1-endeast output ../Documents/Corpus_Malherbe_liste_des_textes_A5.pdf

rm -rf ../tmp
