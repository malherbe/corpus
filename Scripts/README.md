<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Scripts et Programmes 

▫ Répertoire **Python** : Programmes et modules Python

▫ Répertoire **XSL** : Feuilles de transformation XSLT

▫ Répertoire **Fontes** : fontes utilisées pour le nuage de mots au format SVG

* **faire_index.sh** : Script Bash pour créer la table des matières d'un recueil du répertoire **Textes**.

* **faire_texte_brut.sh** : Script Bash pour créer une version TXT d'un texte à partir d'un fichier XML du répertoire **Textes**.

* **faire_treemap.sh** : Script Bash pour inclure dans une page HTML les données statistiques relatives à la création de la carte proportionnelle interactive **treemap_corpus.html** du répertoire HTML. Les données sont générées par le programme Python **statistiques_auteurs.py**, lancé à partir du script Bash **statistiques_auteurs.sh**.

* **liste_participants.sh** : Script Bash pour l'extraction des informations contenues dans l'entête des fichiers XML et relatives aux différentes responsabilités (participations et tâches effectuées). Le document TXT créé est dans le répertoire Documents : **Liste des participants.txt**.

* **liste_textes.sh** : Script Bash pour établir la liste des textes. Le document PDF créé est dans le répertoire Documents : **Corpus_Malherbe_liste_des_textes.pdf**. Une version HTML est dans le répertoire HTML.

* **statistiques_auteurs.sh** : Script Bash pour lancer le programme Python d'analyse statistique du corpus (**statistiques_auteurs.py**).
Documents produits :
	* Graphique de la répartition du corpus selon les siècles, dans le répertoire BDD/Graphiques : **répartition_siècles.svg** et **répartition_siècles.png**. Graphique intégré dans le document **Répartition_siècles_[date].pdf**.

	* Carte proportionnelle interactive du corpus au format HTML (Google treemap) dans le répertoire HTML : **treemap_corpus.html** 

* **validation_xml.sh** : Script Bash pour la validation d'un fichier XML du répertoire Textes ; validation au moyen du schéma de validation du projet dans le répertoire XSD : **TEI_Corpus_Malherbe_1.2.xsd**.

* **nuage_mots.sh** : Script Bash pour lancer le programme Python de création du nuage de mots des auteurs du corpus (image au format SVG : **Images/nuage_corpus.svg**).

* **liste_programmes.txt** : Liste des programmes et bibliothèques nécessaires au fonctionnement des présents programmes dans un environnement Linux Ubuntu.
