<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
version="2.0"
exclude-result-prefixes="tei"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:tei="http://www.tei-c.org/ns/1.0">


<!--
	TRANSFORMATION XML > TXT (CSV)
	Table des matières au format TXT
-->

<!--
	Feuille de transformation sous licence libre
	Licence CECILL version Version 2.1
	(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
	© Richard Renault, CRISCO, Université de Caen - Normandie
-->

<xsl:output method="text" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>


<xsl:template match="/">
	<xsl:apply-templates select="tei:TEI/tei:teiHeader"/>
	<xsl:apply-templates select="//tei:div[@type='part' or @type='subpart' or @type='poem' or @type='drama']"/>
</xsl:template>


<xsl:template match="tei:TEI/tei:teiHeader">

	<!-- code de l'auteur -->
	<xsl:for-each select="//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author">
		<xsl:value-of select="@key" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:for-each>

	<xsl:text disable-output-escaping="yes">———</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

	<!-- code du recueil -->
	<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

	<!-- nom ou noms -->
	<!-- RR octobre 2020 : refonte du traitement des noms -->

	<xsl:choose>
		<!-- auteur anonyme -->
		<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name[@type='anonymous']">
			<xsl:text disable-output-escaping="yes">(auteur anonyme)</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
			<!-- il y a plusieurs auteurs  -->
				<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort]">
					<xsl:for-each select="//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort]">
						<!-- RR octobre 2020 : refonte du traitement des noms -->
						<xsl:call-template name="plusieurs_auteurs" />
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!-- nom de l'auteur -->
					<!-- RR octobre 2020 : refonte du traitement des noms -->
					<xsl:call-template name="nom_auteur" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

					<!-- dates de l'auteur -->
					<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:date/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>



	<!-- titre du recueil-->
	<xsl:text disable-output-escaping="yes">════════════════════════════════════════════</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

	<!-- octobre 2024 -->
	<xsl:for-each select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='main']">
		<xsl:value-of select="text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:for-each>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub']">
		<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub']/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:if>
	<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub_2']">
		<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub_2']/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:if>
	<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub_1']">
	<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='sub_1']/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:if>

	<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='part']">
	<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='part']/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	</xsl:if>


	<!-- date de création -->
	<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

	<!-- nombre de vers -->
	<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:extent/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

</xsl:template>

<!-- RR octobre 2020 : refonte du traitement des noms -->
<xsl:template name="nom_auteur">
	<xsl:choose>
		<!-- nom de plume -->
		<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:addName[@type='pen_name']">
			<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:addName/text()" />

			<xsl:text disable-output-escaping="yes"> (</xsl:text>

			<!-- (prénom et nom) -->
			<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:forename/text()"/>
			<xsl:text disable-output-escaping="yes"> </xsl:text>

			<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink">
				<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink/text()"/>
				<xsl:text disable-output-escaping="yes"> </xsl:text>
			</xsl:if>
			<xsl:value-of select="upper-case(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:surname/text())"/>

			<xsl:text disable-output-escaping="yes">)</xsl:text>
		</xsl:when>

		<!-- prénom et nom -->
		<xsl:otherwise>
			<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:forename/text()"/>
			<xsl:text disable-output-escaping="yes"> </xsl:text>
			<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink">
				<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink/text()"/>
				<xsl:text disable-output-escaping="yes"> </xsl:text>
			</xsl:if>
			<xsl:value-of select="upper-case(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:surname/text())"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- RR octobre 2020 : refonte du traitement des noms -->
<xsl:template name="plusieurs_auteurs">
	<xsl:choose>
		<!-- nom de plume -->
		<xsl:when test="tei:name/tei:addName[@type='pen_name']">
			<xsl:value-of select="tei:name/tei:addName/text()" />
		</xsl:when>
		<!-- prénom et nom -->
		<xsl:otherwise>
			<xsl:value-of select="tei:name/tei:forename/text()"/>
			<xsl:text disable-output-escaping="yes"> </xsl:text>
			<xsl:if test="tei:name/tei:nameLink">
				<xsl:value-of select="tei:name/tei:nameLink/text()"/>
				<xsl:text disable-output-escaping="yes"> </xsl:text>
			</xsl:if>
			<xsl:value-of select="tei:name/tei:surname/text()"/>
		</xsl:otherwise>
	</xsl:choose>
	<!-- dates de l'auteur -->
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:value-of select="tei:date/text()" />
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
</xsl:template>



<!-- Traitement des parties  -->
<xsl:template match="tei:div[@type='part']">
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">▪ </xsl:text>

	<xsl:if test="tei:head[@type='number']">
		<xsl:value-of select="tei:head[@type='number']/text()" />
		<xsl:text disable-output-escaping="yes"> </xsl:text>
	</xsl:if>

	<!-- janvier 2020 : prise en compte des appels de note dans le titre  -->
	<xsl:if test="tei:head[@type='main']">
		<xsl:choose>
			<xsl:when test="child::tei:ref">
				<xsl:value-of select="tei:head[@type='main']/text()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="tei:head[@type='main']//text()" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>

	<xsl:if test="tei:head[@type='sub']">
		<xsl:text disable-output-escaping="yes"> - </xsl:text>
		<xsl:value-of select="tei:head[@type='sub']//text()" />
	</xsl:if>

	<xsl:choose>
		<!-- la partie contient une sous-partie -->
		<xsl:when test="tei:div[@type='subpart']">
			<xsl:apply-templates select="tei:div[@type='part']/tei:div[@type='subpart']" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:when>

		<!-- la partie contient des poèmes -->
		<xsl:otherwise>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:apply-templates select="tei:div[@type='part']/tei:div[@type='poem'or @type='drama']" />
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>


<!-- Traitement des sous-parties  -->
<xsl:template match="tei:div[@type='subpart']">
	<xsl:text disable-output-escaping="yes">&#9;</xsl:text>
	<xsl:text disable-output-escaping="yes">─────────────────────────────────────────────────────</xsl:text>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#9;</xsl:text>
	<xsl:text disable-output-escaping="yes">▫ </xsl:text>

	<xsl:if test="tei:head[@type='number']">
		<xsl:value-of select="tei:head[@type='number']/text()" />
		<xsl:text disable-output-escaping="yes"> </xsl:text>
	</xsl:if>

	<xsl:value-of select="tei:head[@type='main']/text()" />

	<xsl:if test="tei:head[@type='sub']">
		<xsl:text disable-output-escaping="yes"> - </xsl:text>
		<xsl:value-of select="tei:head[@type='sub']/text()" />
	</xsl:if>

	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
	<xsl:apply-templates select="tei:div[@type='subpart']/tei:div[@type='poem'or @type='drama']" />


</xsl:template>

<!-- Traitement des poèmes inclus dans un "front"  -->
<xsl:template match="tei:body/tei:div[@type='front']/tei:div[@type='poem'or @type='drama']">
	<xsl:call-template name="titres" />
</xsl:template>

<!-- Traitement des poèmes inclus dans une partie  -->
<xsl:template match="tei:div[@type='part']/tei:div[@type='poem'or @type='drama']">
	<xsl:text disable-output-escaping="yes">&#9;</xsl:text>
	<xsl:text disable-output-escaping="yes">─ </xsl:text>
	<xsl:call-template name="titres" />
</xsl:template>

<!-- Traitement des poèmes inclus dans une sous-partie  -->
<xsl:template match="tei:div[@type='subpart']/tei:div[@type='poem'or @type='drama']">
	<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
	<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
		<xsl:text disable-output-escaping="yes">─ </xsl:text>
	<xsl:call-template name="titres" />
</xsl:template>

<!-- Traitement des poèmes à la racine -->
<xsl:template match="tei:body/tei:div[@type='poem'or @type='drama']">
	<xsl:text disable-output-escaping="yes">─ </xsl:text>
	<xsl:call-template name="titres" />
</xsl:template>

<!-- Traitement des poèmes insérés -->
<xsl:template match="tei:div[@insert]">
</xsl:template>



<!-- Traitement des titres  -->
<xsl:template name="titres">

	<!-- type de texte -->
	<xsl:choose>
		<!--
		<xsl:when test="@insert">
			<xsl:text disable-output-escaping="yes">poème inséré</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="@type='poem'">
			<xsl:text disable-output-escaping="yes">poème</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">pièce</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

	<!-- code du poème-->
	<xsl:value-of select="@key" />
	<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

	<!-- titre ou incipit du poème-->
	<xsl:choose>

		<!-- le poème est reconnu comme manquant -->
		<xsl:when test="tei:gap[@reason='missing']">
				<!-- <xsl:text disable-output-escaping="yes">(poème manquant)</xsl:text> -->

				<!-- si le titre est number -->
				<xsl:choose>
					<xsl:when test="child::tei:head[@type='main']">
						<xsl:value-of select="tei:head[@type='main']/text()" />
					</xsl:when>
					<xsl:when test="child::tei:head[@type='number']">
						<xsl:value-of select="tei:head[@type='number']/text()" />
					</xsl:when>
				</xsl:choose>
				<xsl:if test="tei:head">
					<xsl:text disable-output-escaping="yes"> : </xsl:text>
				</xsl:if>
				<xsl:value-of select="descendant::tei:desc/text()"/>
		</xsl:when>

		<!-- le poème ne doit pas être inclus dans la liste -->
		<xsl:when test="tei:gap[@reason='redundant']">
			<xsl:text disable-output-escaping="yes"></xsl:text>
		</xsl:when>

		<!-- le poème est en prose -->
		<xsl:when test="tei:gap[@reason='unanalysable']">
				<xsl:text disable-output-escaping="yes">(poème en prose)</xsl:text>
		</xsl:when>

		<!-- si le titre est number -->
		<xsl:when test="tei:head[@type='number']">
			<xsl:choose>

				<!-- décembre 2019 -->
				<!-- le numéro est suivi d'un titre avec un appel de note -->
				<xsl:when test="child::tei:head[@type='main']/tei:ref">
					<xsl:value-of select="tei:head[@type='number']/text()" />
					<xsl:text disable-output-escaping="yes">. </xsl:text>
					<xsl:value-of select="child::tei:head[@type='main']/text()" />
					<xsl:if test="tei:head[@type='sub']">
						<xsl:text disable-output-escaping="yes"> - </xsl:text>
						<xsl:value-of select="tei:head[@type='sub']//text()" />
					</xsl:if>
				</xsl:when>

				<!-- le numéro est suivi d'un titre  -->
				<xsl:when test="child::tei:head[@type='main']">
					<xsl:value-of select="tei:head[@type='number']/text()" />
					<xsl:text disable-output-escaping="yes">. </xsl:text>
					<xsl:value-of select="child::tei:head[@type='main']//text()" />
					<xsl:if test="tei:head[@type='sub']">
						<xsl:text disable-output-escaping="yes"> - </xsl:text>
						<xsl:value-of select="tei:head[@type='sub']//text()" />
					</xsl:if>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="tei:head[@type='number']/text()" />
					<xsl:text disable-output-escaping="yes">. </xsl:text>
					<!-- R.R. avril 2018 : ne pas prendre le premier vers si celui-ci contient un attribut ana -->
					<!-- <xsl:value-of select="descendant::tei:l[2]//text()" /> -->
					<!-- R.R. septembre 2022 : ne pas prendre le texte des éléments add et sic -->
					<!-- R.R. décembre 2022 : ne pas prendre les vers avec ana="mising" -->
					<xsl:text disable-output-escaping="yes">"</xsl:text>
					<xsl:choose>
						<xsl:when test="descendant::tei:l[position() = 1 and not(@ana='missing')]">
							<xsl:value-of select="descendant::tei:l[1]//text()[not(ancestor::tei:add or ancestor::tei:sic)]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="descendant::tei:l[not(@ana='missing')][1]//text()[not(ancestor::tei:add or ancestor::tei:sic)]"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text disable-output-escaping="yes">"</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>

		<!-- si le titre est main et contient un appel de note-->
		<xsl:when test="tei:head[@type='main' and child::tei:ref]">
			<xsl:value-of select="tei:head[child::tei:ref]/text()" />
			<xsl:if test="tei:head[child::tei:ref]/following-sibling::tei:head[@type='sub']">
				<xsl:text disable-output-escaping="yes"> - </xsl:text>
				<xsl:value-of select="tei:head[child::tei:ref]/following-sibling::tei:head[@type='sub']//text()" />
			</xsl:if>
		</xsl:when>

		<!-- s'il n'y a pas de titre, on prend l'incipit -->
		<xsl:when test="not(tei:head[@type='main']) and not(tei:head[@type='number'] )">
					<!-- R.R. avril 2018 : ne pas prendre le premier vers si celui-ci contient un attribut ana -->
					<!-- <xsl:value-of select="descendant::tei:l[2]//text()" /> -->
					<!-- R.R. septembre 2022 : ne pas prendre le texte des éléments add et sic -->
					<!-- R.R. décembre 2022 : ne pas prendre les vers avec ana="mising" -->
					<xsl:text disable-output-escaping="yes">"</xsl:text>
					<xsl:choose>
						<xsl:when test="descendant::tei:l[position() = 1 and not(@ana='missing')]">
							<xsl:value-of select="descendant::tei:l[1]//text()[not(ancestor::tei:add or ancestor::tei:sic)]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="descendant::tei:l[not(@ana='missing')][1]//text()[not(ancestor::tei:add or ancestor::tei:sic)]"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text disable-output-escaping="yes">"</xsl:text>
		</xsl:when>

		<!-- si le titre est main -->
		<xsl:when test="tei:head/@type='main'">
			<xsl:choose>
				<xsl:when test="tei:head[@type='sub']">
					<xsl:value-of select="tei:head[@type='main']//text()" />
					<xsl:if test="tei:head[@type='sub']">
						<xsl:text disable-output-escaping="yes"> - </xsl:text>
						<xsl:value-of select="tei:head[@type='sub']//text()" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="child::tei:ref">
					<xsl:value-of select="tei:head[@type='main']/text()" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="tei:head[@type='main']//text()" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>

	</xsl:choose>
	<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
</xsl:template>


<xsl:template match="/tei:lb">
	<xsl:text disable-output-escaping="yes"> - - </xsl:text>
</xsl:template>


<!-- traitement de l'élément <subst> et <add> -->
<xsl:template match="tei:add">
</xsl:template>

<!-- juillet 2022 : traitement de l'élément <choice> -->
<xsl:template match="tei:sic">
</xsl:template>


<xsl:template match="/tei:hi[@rend='smallcap']">
</xsl:template>


</xsl:stylesheet>
