<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
version="2.0"
exclude-result-prefixes="tei"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:tei="http://www.tei-c.org/ns/1.0">

<!--
	TRANSFORMATION XML > TXT (CSV)
	Texte du recueil au format TXT
-->

<!--
	Feuille de transformation sous licence libre
	Licence CECILL version Version 2.1
	(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
	© Richard Renault, CRISCO, Université de Caen - Normandie
-->

	<xsl:output method="text" indent="no" encoding="utf-8"/>

	<xsl:variable name="retour_ligne">
	<xsl:text>
	</xsl:text>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:apply-templates select="/tei:TEI/tei:teiHeader"/>
		<xsl:apply-templates select="/tei:TEI/tei:text"/>
	</xsl:template>

	<xsl:template match="tei:teiHeader">

		<xsl:text disable-output-escaping="yes">Version TXT du texte obtenue par extraction à partir du fichier XML.</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:text disable-output-escaping="yes">┌─────────────────────┐</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">│Entête du fichier XML│</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">└─────────────────────┘</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:choose>
			<!-- Il y a plusieurs auteurs  -->
			<xsl:when test="count(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author) != 1">
				<xsl:for-each select = "/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author">
					<xsl:choose>
						<!-- c'est le dernier  -->
						<xsl:when test="position()=last()">
							<xsl:value-of select="@key" />
						</xsl:when>
						<xsl:otherwise>
								<xsl:value-of select="@key" />
								<xsl:text disable-output-escaping="yes"> - </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:when>
			<!-- Il y a un seul auteurs  -->
			<xsl:otherwise>
				<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:author/@key" />
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>


		<xsl:value-of select="tei:fileDesc/tei:publicationStmt/tei:idno/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<!-- auteur(s)  -->
		<xsl:choose>
			<!-- auteur anonyme -->
			<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name[@type='anonymous']">
				<xsl:text disable-output-escaping="yes">(auteur anonyme)</xsl:text>
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			</xsl:when>
			<xsl:otherwise>

				<xsl:choose>
				<!-- il y a plusieurs auteurs   -->
					<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort]">
						<xsl:choose>
							<!-- il y a 2 auteurs   -->
							<xsl:when test="count(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort]) = 2">
								<xsl:choose>
									<!-- nom de plume -->
									<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:addName[@type='pen_name']">
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:addName/text()" />
									</xsl:when>
									<!-- prénon et nom -->
									<xsl:otherwise>
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:forename/text()"/>
										<xsl:text disable-output-escaping="yes"> </xsl:text>
										<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:nameLink">
											<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:nameLink/text()"/>
											<xsl:text disable-output-escaping="yes"> </xsl:text>
										</xsl:if>
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:name/tei:surname/text()"/>
									</xsl:otherwise>
								</xsl:choose>

								<!-- dates de l'auteur -->
								<xsl:text disable-output-escaping="yes"> (</xsl:text>
								<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='1']/tei:date/text()" />
								<xsl:text disable-output-escaping="yes">)</xsl:text>

								<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
								<xsl:text disable-output-escaping="yes">&#38; </xsl:text>
								<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

								<xsl:choose>
									<!-- nom de plume -->
									<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:addName[@type='pen_name']">
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:addName/text()" />
									</xsl:when>
									<!-- prénon et nom -->
									<xsl:otherwise>
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:forename/text()"/>
										<xsl:text disable-output-escaping="yes"> </xsl:text>
										<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:nameLink">
											<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:nameLink/text()"/>
											<xsl:text disable-output-escaping="yes"> </xsl:text>
										</xsl:if>
										<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:name/tei:surname/text()"/>
									</xsl:otherwise>
								</xsl:choose>

								<!-- dates de l'auteur -->
								<xsl:text disable-output-escaping="yes"> (</xsl:text>
								<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@sort='2']/tei:date/text()" />
								<xsl:text disable-output-escaping="yes">) </xsl:text>
							</xsl:when>

							<!-- il y a plus de 2 auteurs   -->
							<xsl:otherwise>
								<xsl:for-each select = "/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author">
									<xsl:choose>
										<!-- ce n'est pas le dernier -->
										<xsl:when test="position() != last()">
											<xsl:choose>
												<!-- c'est l'avant-dernier -->
												<xsl:when test="position()=last()-1">
													<xsl:choose>
														<!-- nom de plume -->
														<xsl:when test="tei:name/tei:addName[@type='pen_name']">
															<xsl:value-of select="tei:name/tei:addName/text()" />
														</xsl:when>
														<!-- prénon et nom -->
														<xsl:otherwise>
															<xsl:value-of select="tei:name/tei:forename/text()"/>
															<xsl:text disable-output-escaping="yes"> </xsl:text>
															<xsl:if test="tei:name/tei:nameLink">
																<xsl:value-of select="tei:name/tei:nameLink/text()"/>
																<xsl:text disable-output-escaping="yes"> </xsl:text>
															</xsl:if>
															<xsl:value-of select="tei:name/tei:surname/text()"/>
														</xsl:otherwise>
													</xsl:choose>
													<!-- dates de l'auteur -->
													<xsl:text disable-output-escaping="yes"> (</xsl:text>
													<xsl:value-of select="tei:date/text()" />
													<xsl:text disable-output-escaping="yes">)</xsl:text>
													<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<!-- nom de plume -->
														<xsl:when test="tei:name/tei:addName[@type='pen_name']">
															<xsl:value-of select="tei:name/tei:addName/text()" />
														</xsl:when>
														<!-- prénon et nom -->
														<xsl:otherwise>
															<xsl:value-of select="tei:name/tei:forename/text()"/>
															<xsl:text disable-output-escaping="yes"> </xsl:text>
															<xsl:if test="tei:name/tei:nameLink">
																<xsl:value-of select="tei:name/tei:nameLink/text()"/>
																<xsl:text disable-output-escaping="yes"> </xsl:text>
															</xsl:if>
															<xsl:value-of select="tei:name/tei:surname/text()"/>
														</xsl:otherwise>
													</xsl:choose>
													<!-- dates de l'auteur -->
													<xsl:text disable-output-escaping="yes"> (</xsl:text>
													<xsl:value-of select="tei:date/text()" />
													<xsl:text disable-output-escaping="yes">)</xsl:text>
													<xsl:text disable-output-escaping="yes">, </xsl:text>
													<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<!-- c'est le dernier -->
										<xsl:otherwise>
											<xsl:text disable-output-escaping="yes">&#38; </xsl:text>
											<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
											<xsl:choose>
												<!-- nom de plume -->
												<xsl:when test="tei:name/tei:addName[@type='pen_name']">
													<xsl:value-of select="tei:name/tei:addName/text()" />
												</xsl:when>
												<!-- prénon et nom -->
												<xsl:otherwise>
													<xsl:value-of select="tei:name/tei:forename/text()"/>
													<xsl:text disable-output-escaping="yes"> </xsl:text>
													<xsl:if test="tei:name/tei:nameLink">
														<xsl:value-of select="tei:name/tei:nameLink/text()"/>
														<xsl:text disable-output-escaping="yes"> </xsl:text>
													</xsl:if>
													<xsl:value-of select="tei:name/tei:surname/text()"/>
												</xsl:otherwise>
											</xsl:choose>
											<!-- dates de l'auteur -->
											<xsl:text disable-output-escaping="yes"> (</xsl:text>
											<xsl:value-of select="tei:date/text()" />
											<xsl:text disable-output-escaping="yes">)</xsl:text>
											<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					</xsl:when>

					<xsl:otherwise>
						<!-- nom de l'auteur -->
						<xsl:call-template name="nom_auteur" />
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

						<!-- dates de l'auteur -->
						<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:date/text()" />
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>

		<!-- titre du recueil-->
		<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:title[@type='main']/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:if test="tei:fileDesc/tei:titleStmt/tei:title[@type='sub']">
			<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:title[@type='sub']/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<xsl:if test="tei:fileDesc/tei:titleStmt/tei:title[@type='sub_2']">
			<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:title[@type='sub_2']/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<xsl:if test="tei:fileDesc/tei:titleStmt/tei:title[@type='sub_1']">
		<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:title[@type='sub_1']/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<xsl:if test="tei:fileDesc/tei:titleStmt/tei:title[@type='part']">
		<xsl:value-of select="tei:fileDesc/tei:titleStmt/tei:title[@type='part']/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<!-- date de création -->
		<xsl:value-of select="tei:profileDesc/tei:creation/tei:date/text()" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">_________________________________________________________________</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Édition électronique</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Corpus Malherbə</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Laboratoire CRISCO, Université de Caen</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Contact : crisco.incipit@unicaen.fr</xsl:text>

		<!-- préparateurs -->
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">_________________________________________________________________</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Préparation du texte :</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:for-each select="//tei:fileDesc/tei:titleStmt/tei:respStmt">
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">▫ </xsl:text>
			<xsl:value-of select="tei:name/tei:forename/text()" />
			<xsl:text disable-output-escaping="yes"> </xsl:text>
			<xsl:value-of select="tei:name/tei:surname/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">  (</xsl:text>
			<xsl:value-of select="tei:resp/text()" />
			<xsl:text disable-output-escaping="yes">)</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:for-each>

		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<!-- origine de la source électronique -->
		<xsl:text disable-output-escaping="yes">_________________________________________________________________</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">Origine de la source électronique :</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:if test="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull">
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:titleStmt/tei:title/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:titleStmt/tei:author/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:titleStmt/tei:editor/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:publicationStmt/tei:publisher//descendant::node()/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:publicationStmt/tei:idno/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblFull/tei:sourceDesc//descendant::node()/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<xsl:if test="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct">
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:monogr/tei:title/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:monogr/tei:author/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:monogr/tei:editor/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:monogr/tei:imprint/tei:publisher/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:monogr/tei:imprint/tei:date/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:value-of select="tei:fileDesc/tei:sourceDesc[position()=1]/tei:biblStruct/tei:note/text()" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		</xsl:if>

		<!-- édition de référence pour les corrections métriques -->
		
		<xsl:if test="tei:fileDesc/tei:sourceDesc[position()=2]">

			<xsl:text disable-output-escaping="yes">_________________________________________________________________</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">Édition de référence pour les corrections métriques :</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

			<xsl:for-each select="//tei:fileDesc/tei:sourceDesc[position()&gt;1]">

				<xsl:if test="tei:biblFull">
					<xsl:value-of select="tei:biblFull/tei:titleStmt/tei:title/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblFull/tei:titleStmt/tei:author/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblFull/tei:titleStmt/tei:editor/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblFull/tei:publicationStmt/tei:publisher/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblFull/tei:publicationStmt/tei:idno/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblFull/tei:sourceDesc//descendant::node()/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				</xsl:if>

				<xsl:if test="tei:biblStruct">
					<xsl:value-of select="tei:biblStruct/tei:monogr/tei:title/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblStruct/tei:monogr/tei:author/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblStruct/tei:monogr/tei:editor/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblStruct/tei:monogr/tei:imprint/tei:publisher/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="tei:biblStruct/tei:monogr/tei:imprint/tei:date/text()" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				</xsl:if>
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

			</xsl:for-each>

		</xsl:if>

		<!-- licence de diffusion -->

		<xsl:if test="tei:fileDesc/tei:publicationStmt/tei:availability">
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:text disable-output-escaping="yes">┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉</xsl:text>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:choose>
					<!-- texte sous droits -->
					<xsl:when test="tei:fileDesc/tei:publicationStmt/tei:availability[@status='restricted']">
						<xsl:text disable-output-escaping="yes">texte sous droits à ne pas diffuser</xsl:text>
					</xsl:when>
					<!-- texte hors droits -->
					<xsl:otherwise>
						<xsl:text disable-output-escaping="yes">texte hors droits distribué sous licence libre</xsl:text>
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
						<xsl:value-of select="tei:fileDesc/tei:publicationStmt/tei:availability/tei:licence/text()" />
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
						<xsl:value-of select="tei:fileDesc/tei:publicationStmt/tei:availability/tei:licence/@target" />
						<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		<xsl:text disable-output-escaping="yes">┌─────┐</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">│TEXTE│</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:text disable-output-escaping="yes">└─────┘</xsl:text>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

	</xsl:template>

	<!-- RR octobre 2020 : refonte du traitement des noms -->
	<xsl:template name="nom_auteur">
		<xsl:choose>
			<!-- nom de plume -->
			<xsl:when test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:addName[@type='pen_name']">
				<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:addName/text()" />

				<xsl:text disable-output-escaping="yes"> (</xsl:text>

				<!-- (prénom et nom) -->
				<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:forename/text()"/>
				<xsl:text disable-output-escaping="yes"> </xsl:text>

				<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink">
					<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink/text()"/>
					<xsl:text disable-output-escaping="yes"> </xsl:text>
				</xsl:if>
				<xsl:value-of select="upper-case(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:surname/text())"/>

				<xsl:text disable-output-escaping="yes">)</xsl:text>

			</xsl:when>

			<!-- prénom et nom -->
			<xsl:otherwise>
				<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:forename/text()"/>
				<xsl:text disable-output-escaping="yes"> </xsl:text>
				<xsl:if test="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink">
					<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:nameLink/text()"/>
					<xsl:text disable-output-escaping="yes"> </xsl:text>
				</xsl:if>
				<xsl:value-of select="upper-case(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:name/tei:surname/text())"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- traitement de l'élément <subst> et <add> -->
	<xsl:template match="tei:add">
	</xsl:template>

	<!-- juillet 2022 : traitement de l'élément <choice> -->
	<xsl:template match="tei:sic">
	</xsl:template>

	<xsl:template match="tei:lb">
		<xsl:value-of select="$retour_ligne" />
	</xsl:template>

</xsl:stylesheet>
