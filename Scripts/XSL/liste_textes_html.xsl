<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/TR/REC-html40" exclude-result-prefixes="xsl">

<xsl:output
	method="html"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	encoding="UTF-8"
	indent="yes" />

	<xsl:template match="/CORPUS">

		<html xml:lang="fr">
		  <head>
			<title>MÉTRIQUE EN LIGNE</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta name="description" content="traitement automatique de textes versifiés" />
			<meta name="keywords" content="métrique poésie versification" />
			<meta name="author" content="Richard Renault - CRISCO Université de Caen Normandie" />
			<link type="text/css" rel="stylesheet" href="css/liste_textes.css" />
		  </head>
			<body>
				<div class="page">

				<table class="table_image">
					<tbody>
					<tr>
						<td class="image1">
							<img src="images/corpus_malherbe.svg" alt="corpus_malherbe.svg" height="50px"/>
						</td>
						<td class="image2">
							<img src="images/CRISCO_UNICAEN.svg" alt="CRISCO_UNICAEN.svg" height="50px"/>
						</td>
					</tr>
					</tbody>
				</table>
	
					<xsl:apply-templates />
	
				</div>
			</body>
		</html>
	
	</xsl:template>
	
	
<xsl:template match="version">
	<div class="version">
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="titre_liste_recueils">
	<hr/>
	<div class="titre_liste_recueils">
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="auteur">
	<ul class="auteur">
		<li>
			<xsl:apply-templates />
		</li>
	</ul>
</xsl:template>


<xsl:template match="recueils">
	<ul class="recueils">
		<xsl:apply-templates />
	</ul>
</xsl:template>

<xsl:template match="recueil">
		<li class="recueil">
			<xsl:apply-templates />
		</li>
</xsl:template>

<xsl:template match="puce_auteur">
	<spam class="puce_auteur">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>

<xsl:template match="puce_recueil">
	<spam class="puce_recueil">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>

<xsl:template match="nom_auteur">
	<spam class="nom_auteur">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>


<xsl:template match="code_auteur">
	<spam class="code_auteur">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>

<xsl:template match="code_recueil">
	<spam class="code_recueil">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>

<xsl:template match="date_traitement">
	<div class="date_traitement">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="effectifs">
	<div class="effectifs">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="effectifs_titre">
	<div class="effectifs_titre">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="nbr_corpus">
	<spam class="nbr_corpus">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</spam>
</xsl:template>

<xsl:template match="ligne_effectifs">
	<div class="ligne_effectifs">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</div>
</xsl:template>

<xsl:template match="site_git">
	<div class="site_git">
		<xsl:value-of select="/text()" />
		<xsl:apply-templates />
	</div>
</xsl:template>


<xsl:template match="pb">
 <div style="page-break-before: always;"> </div>
</xsl:template>

<xsl:template match="lb">
<xsl:text disable-output-escaping="yes">&#060;br /&#062;</xsl:text>
</xsl:template>



</xsl:stylesheet>
