<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
version="2.0" 
exclude-result-prefixes="tei" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:functx="http://www.functx.com"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:tei="http://www.tei-c.org/ns/1.0">

<!--
	TRANSFORMATION XML > XML
	Suppression des attributs d'analyse de l'élément <l> :
	rhyme="none" 
	ana="unanalyzable"
	ana="incomplete" where="(I|M|F)"
	ana="prose"

	suppression de l'élément <add> = ajout d'un contenu pour l'analyse
	Suppression de la balise <del> = contenu non pris en compte dans l'analyse
	Suppression de l'élément <subts> = contient <del> et <add> : remplace un contenu
	qui ne peut pas être analysé (visible) par un contenu analysable (invisible).
	Exemple : remplace "18" par "dix-huit".

	juillet 2022

	-->

<!--
	Feuille de transformation sous licence libre
	Licence CECILL version Version 2.1
	(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
	© Richard Renault, CRISCO, Université de Caen - Normandie
-->

	<xsl:output method="xml" indent="no" encoding="utf-8"/>

	<xsl:template match="@*|node()">
	  <xsl:copy>
		<xsl:apply-templates select="@*|node()"/>
	  </xsl:copy>
	</xsl:template>


	<xsl:template match="@rhyme" />

	<xsl:template match="@ana" />

	<xsl:template match="@where" />

	<xsl:template match="tei:add[parent::tei:l]">
	</xsl:template>

	<xsl:template match="tei:add[parent::tei:hi]">
	</xsl:template>

	<xsl:template match="tei:l[child::tei:subst[@type='repetition']/tei:del/not(text())]">
		<xsl:text disable-output-escaping="yes"></xsl:text>
	</xsl:template>

	<xsl:template match="tei:del[parent::tei:l]">
		<xsl:copy-of select="text()" />
	</xsl:template>

	<xsl:template match="tei:del[parent::tei:hi]">
		<xsl:copy-of select="text()" />
	</xsl:template>

	<xsl:template match="tei:del[parent::tei:foreign]">
		<xsl:copy-of select="text()" />
	</xsl:template>

	<xsl:template match="tei:subst">
		<xsl:copy-of select="tei:del/text()" />
	</xsl:template>


</xsl:stylesheet>
