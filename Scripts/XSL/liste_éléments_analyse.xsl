<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
version="2.0"
exclude-result-prefixes="tei"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:tei="http://www.tei-c.org/ns/1.0">

<!--
	TRANSFORMATION XML > TXT (CSV)

	Création d'un fichier CSV contenant tous les éléments et attributs d'analyse :
	rhyme="none" 
	ana="unanalyzable"
	ana="incomplete" where="I"
	ana="prose"

	élément <add>, <del> et <subst>

	Cette feuille de transformation est utilisée notamment pour vérifier si le fichier XML
	contient encore des balises d'analyse après application de la feuille de transformation :
	suppression_éléments_analyse.xsl

	juillet 2022
-->

<!--
	Feuille de transformation sous licence libre
	Licence CECILL version Version 2.1
	(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
	© Richard Renault, CRISCO, Université de Caen - Normandie
-->

<xsl:output method="text" indent="no" encoding="utf-8" omit-xml-declaration="yes"/>


<xsl:template match="/">
	<xsl:apply-templates select="/tei:TEI/tei:text/tei:body"/>
</xsl:template>


	<xsl:template match="/tei:TEI/tei:text/tei:body">
		<xsl:for-each select="//tei:subst|//tei:add[not(ancestor::tei:subst)]|//tei:del[not(ancestor::tei:subst)]|//tei:l[@ana]|//tei:l[@rhyme]">

			<!-- CHAMP 1 - code du recueil-->
			<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno/text()" />
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 2 - code du poème-->
			<xsl:value-of select="ancestor::tei:div[@type='poem' or @type='drama']/@key" />
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 3 - numéro du vers  -->
			<xsl:value-of select="ancestor::tei:l/@n|@n"/>
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 4 - nature de la balise-->
			<xsl:choose>
				<xsl:when test="local-name(.)='subst'">
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@reason"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@type"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:call-template name="vers" />
				</xsl:when>
				<xsl:when test="local-name(.)='add'">
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@reason"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@type"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:call-template name="vers" />
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='del'">
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@reason"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@type"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:call-template name="vers" />
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:when>
				<xsl:when test="@ana">
					<xsl:text disable-output-escaping="yes">ana</xsl:text>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@ana"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:call-template name="vers" />
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:when>
				<xsl:when test="@rhyme">
					<xsl:text disable-output-escaping="yes">rhyme</xsl:text>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@rhyme"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:call-template name="vers" />
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="ancestor::tei:l/@n"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		</xsl:for-each>



		<xsl:for-each select="//tei:div[@rhyme]">

			<!-- CHAMP 1 - code du recueil-->
			<xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno/text()" />
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 2 - code du poème-->
			<xsl:value-of select="ancestor::tei:div[@type='poem' or @type='drama']/@key" />
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 3 - numéro du vers  -->
			<xsl:value-of select="ancestor::tei:div/@n|@n"/>
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>

			<!-- CHAMP 4 - nature de la balise-->
			<xsl:choose>
				<xsl:when test="@rhyme">
					<xsl:text disable-output-escaping="yes">rhyme</xsl:text>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="name()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="@rhyme"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
					<xsl:value-of select="tei:head/text()"/>
					<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>

		</xsl:for-each>

	</xsl:template>


<xsl:template name="vers">
	<xsl:choose>
		<xsl:when test="local-name(.)='l'">
			<xsl:value-of select="descendant::text()"/>
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
		</xsl:when>

		<xsl:when test="local-name(.)='gap'">
			<xsl:value-of select="text()"/>
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
		</xsl:when>

		<xsl:otherwise>
			<xsl:value-of select="ancestor::tei:l//text()"/>
			<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="del">
	<xsl:choose>
		<xsl:when test="local-name(.)='del'">
			<xsl:value-of select="descendant::tei.del//text()"/>
		</xsl:when>

		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
</xsl:template>

<xsl:template name="add">
	<xsl:choose>
	<xsl:when test="tei.add">
		<xsl:value-of select="descendant::tei.add//text()"/>
		<xsl:text disable-output-escaping="yes">&#x09;</xsl:text>
		
	</xsl:when>
</xsl:choose>
</xsl:template>


</xsl:stylesheet>
