#!

# Lancement du programme Python d'extraction des informations sur les données
# statistiques auteur par auteur

# Richard Renault, juin 2020


# fichiers en entrée :
#	../version.txt
#	../BDD/CSV/corpus_nombres.csv
#	../BDD/CSV/corpus_auteurs.csv
#	../BDD/CSV/corpus_coauteurs.csv

# fichiers en sortie :
#	../BDD/CSV/corpus_statistiques.csv 
#	../BDD/TXT/corpus_treemap.txt

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────


date_ref=$(date +%Y-%m-%d)

echo -e " \033[44m\033[37m  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ \033[0m"; tput sgr0
echo -e " \033[44m\033[37m  ◼ Corpus Malherbe                                         \033[0m"; tput sgr0
echo -e " \033[47m\033[34m  ◼ Traitement statistique du corpus                        \033[0m"; tput sgr0

cd Python
python3 statistiques_auteurs.py ../version.txt ../../BDD/CSV/corpus_nombres.csv ../../BDD/CSV/corpus_auteurs.csv ../../BDD/CSV/corpus_coauteurs.csv ../../BDD/CSV/corpus_statistiques.csv ../../BDD/TXT/corpus_treemap.txt ../../BDD/TXT/corpus_nuage.txt

# suppresion des doublons pour le treemap
sort ../../BDD/TXT/corpus_treemap.txt > ../../BDD/TXT/corpus_treemap1.txt
uniq ../../BDD/TXT/corpus_treemap1.txt > ../../BDD/TXT/corpus_treemap2.txt
cp ../../BDD/TXT/corpus_treemap2.txt ../../BDD/TXT/corpus_treemap.txt
rm ../../BDD/TXT/corpus_treemap1.txt
rm ../../BDD/TXT/corpus_treemap2.txt

