#!

# Production du fichier d'index d'un recueil de poésies
# = table des matières avec un identifiant unique pour chaque poème
# Exemple de ligne de commande : ./faire index.sh BAU BAU_1

# Richard Renault, 2020

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────


echo -e " \033[47m\033[30m - Création du fichier d'index : \033[0m\033[44m\033[1;37m $1 \033[0m\033[47m\033[1;30m $2 \033[0m"; tput sgr0

# teste la présence de la première variable : nom de l'auteur
if [ -n "$1" ]
	then
		if [ -d "../Corpus_Malherbe/Textes/$1" ]
			then
				# teste la présence de la seconde variable : nom du recueil
				if [ -n "$2" ]
					then
						if [ -d "../Corpus_Malherbe/Textes/$1/$2" ]
							then

								saxonb-xslt -ext:on -s ../Corpus_Malherbe/Textes/$1/$2/$2.xml -o ../Corpus_Malherbe/Textes/$1/$2/${2%_0.xml}_index.txt -xsl XSL/txt_index.xsl
								echo "  - C'est fait !"

							else
								echo -e " \033[40m\033[31m- $2 n'est pas un nom de recueil valide ! \033[0m"; tput sgr0
						fi
					else
						echo -e " \033[40m\033[31m- Il manque le nom du recueil (code du recueil) ! \033[0m"; tput sgr0
						echo -e " \033[40m\033[31m- exemple : $0 BAU BAU_1  \033[0m"; tput sgr0
				fi
			else
				echo -e " \033[40m\033[31m- $1 n'est pas un nom de répertoire valide ! \033[0m"; tput sgr0
				echo -e " \033[40m\033[31m- exemple : $0 BAU BAU_1  \033[0m"; tput sgr0
		fi
	else
		echo -e " \033[40m\033[31m- Il manque le nom du répertoire (code de l'auteur) ! \033[0m"; tput sgr0
		echo -e " \033[40m\033[31m- exemple : $0 BAU BAU_1  \033[0m"; tput sgr0
fi
