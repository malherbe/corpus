#!

# Lancement du programme Python de création d'un nuage de mots avec le module WordCloud
# Nuage des auteurs du corpus relativement au nombre de ver

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────

echo -e " \033[44m\033[37m  ◼ Création d'un nuage de mots en SVG   \033[0m"; tput sgr0

./Python/nuage_mots.py ../BDD/TXT/corpus_nuage.txt Fontes/texgyreschola-bold.otf ../Documents/nuage_corpus.svg

echo -e " \033[44m\033[37m  ◼ C'est fait !                         \033[0m"; tput sgr0
