#!

# Création du document : liste_des_participants.txt (liste des personnes ayant participé à la préparation d'un corpus)
# Lancement du programme Python d'extraction des informations sur la préparation des textes
# 
# Richard Renault, juin 2020

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────


date_ref=$(date +%Y-%m-%d)



echo -e " \033[44m\033[37m  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ \033[0m"; tput sgr0
echo -e " \033[44m\033[37m  ◼ Corpus Malherbe                                         \033[0m"; tput sgr0
echo -e " \033[47m\033[34m  ◼ Création de la liste des intervenants et des tâches     \033[0m"; tput sgr0

python3 Python/liste_participants.py ../BDD/CSV/corpus_préparation.csv ../Documents/Liste_des_participants.txt
