#!/usr/bin/python3
# encodage: UTF-8

# Richard Renault - juin 2020

# Différentes classes et méthodes utilisées par les programmes.
# Certaines classes ou méthodes proviennent des modules utilisés
# par les programmes de l'analyse métrique.

"""
─────────────────────────────────────────────────────────────────────────
Programme sous licence libre

Licence CECILL version Version 2.1
(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)

© Richard Renault, CRISCO, Université de Caen - Normandie

Ce programme informatique a été réalisé dans le cadre d'un projet
de traitement automatique de textes versifiés.

Ce programme est régi par la licence CeCILL soumise au droit français
et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
conditions de la licence CeCILL telle que diffusée par le CEA, le CNRS
et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
─────────────────────────────────────────────────────────────────────────
"""


import re
import csv

def affiche_traitement(ligne, couleur='blanc') :
	if couleur == "vert" :
		code = '\033[32m '
	elif couleur == "rouge" :
		code = '\033[31m '
	elif couleur == "jaune" :
		code = '\033[93m '
	elif couleur == "cyan" :
		code = '\033[36m '
	elif couleur == "bleu-clair" :
		code = '\033[94m '
	elif couleur == "magenta" :
		code = '\033[35m '
	elif couleur == "magenta-clair" :
		code = '\033[95m '
	elif couleur == "bleu-blanc" :
		code = '\033[47m\033[34m '
	elif couleur == "magenta-blanc" :
		code = '\033[47m\033[35m '
	elif couleur == "blanc" :
		code = '\033[39m '
	elif couleur == "vert-clair" :
		code = '\033[92m '
	elif couleur == "gris-clair" :
		code = '\033[37m '
	elif couleur == "fond_jaune" :
		code = '\t\033[43;1m'
	else :
		code = '\033[39m '
	print(code+ligne+" \033[0m")


def affiche_contrôle(ligne, couleur='blanc', vue=True) :
	if vue == True :
		affiche_traitement(ligne, couleur)
	else :
		None


class Dialect( csv.Dialect ):
	"""Dialecte représentant la convention d'écriture des fichiers CSV au sein du projet:
	séparation avec des tabulations et retours à la ligne."""
	def __init__( self ):
		self.delimiter = "\t"
		self.lineterminator = '\n'
		self._valid = True
		self._name = 'anam'
		self.quoting = csv.QUOTE_NONE


class Csv:
	"""Classe permettant d'écrire dans un CSV pendant le traitement."""

	def __init__( self, csvFileName ):
		self.csvFile = open( csvFileName, 'w' )

	def close( self ):
		"""Ferme le fichier de CSV. Doit être invoqué à la fin d'un traitement utilisant un CSV."""
		self.csvFile.close()

	def ecrire( self, *texte ):
		"""Ecrit dans le fichier CSV le texte en argument.
		@param *texte: plusieures string"""
		for i in range(len(texte)):
			if i == range(len(texte))[-1] :
				self.csvFile.write( texte[i] )
			else :
				self.csvFile.write( texte[i] )
				self.csvFile.write( "\t" )

	def retour_ligne( self ):
		"""Ecrit un retour à la ligne dans le fichier CSV."""
		self.csvFile.write( '\n' )

	def tab ( self ):
		"""Introduit une tabulations	dans le fichier CSV."""
		self.csvFile.write( "\t" )


class CSV_lecture_auteurs( list ):
	"""
	Lecture du fichier CSV des auteurs
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )
		l_data = []
		l_auteurs = []
		l_recueils = []

		#ne prend pas en compe la première ligne du fichier csv
		ligne = next(tmp_csv)

		d_auteurs = {}

		for ligne in tmp_csv:

			code = ""
			nom = ""
			date = ""
			nom_tri = ""

			code = ligne[0]

			# nom de plume
			if ligne[3] != '' :
				nom = ligne[3]
				nom_tri = ligne[3]

				if ligne[5] != "" :
					date = "("+ligne[5]+")"
				else :
					date = ""

			else :
				# avec particule
				if ligne[4] != "" :
					nom = ligne[1]+" "+ligne[4]+" "+ligne[2]
					nom_tri = ligne[2]

					if ligne[5] != "" :
						if ligne[5] != "" :
							date = "("+ligne[5]+")"
						else :
							date = ""

				else :
					#print(ligne)
					nom = ligne[1]+" "+ligne[2]
					nom_tri = ligne[2]

					if ligne[5] != "" :
						if ligne[5] != "" :
							date = "("+ligne[5]+")"
						else :
							date = ""

			d_auteurs[code] = (nom_tri, nom, date)

		return d_auteurs


class CSV_lecture_coauteurs( list ):
	"""
	Lecture du fichier CSV des coauteurs
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )
		l_data = []
		l_auteurs = []
		l_recueils = []

		#ne prend pas en compe la première ligne du fichier csv
		ligne = next(tmp_csv)

		code_recueil = ""
		coauteurs = ()

		d_coauteurs = {}

		for ligne in tmp_csv :

			code_recueil = ligne[0]

			l_co = ligne[1].split(' ')
			t_co = tuple(l_co)

			d_coauteurs[code_recueil] = t_co

		return d_coauteurs


class Txt:
	"""Classe permettant d'écrire dans un fichier txt."""

	def __init__( self, txtFileName ):
		self.txtFile = open( txtFileName, 'w' )

	def fermer( self ):
		"""Ferme le fichier de txt."""
		self.txtFile.close()

	def écrire( self, texte ):
		"""Ecrit dans le fichier."""
		self.txtFile.write( texte )

	def retour_ligne( self ):
		"""Ecrit un retour à la ligne."""
		self.txtFile.write( '\n' )

	def tab ( self ):
		"""Introduit une tabulations."""
		self.txtFile.write( "\t" )

	def ajuster (self, texte, longueur, retrait) :
		"""Écrit dans le fichier avec des retour à la ligne
		selon une longueur voulue et un retrait du texte
		longueur = longueur du texte
		retrait = retrait avec tabulation
		"""
		texte_modif = ""
		retrait_tab = ''.join(('\t') for i in range(retrait))

		if len(texte) > longueur :

			l_texte = list(texte)
			index = 0
			empan = 0

			for c in l_texte :

				index += 1
				empan += 1

				if len(l_texte) <= longueur :
					l_texte_modif.append(texte)
					break

				else :
					if c == " "  and empan >= longueur :
						l_texte[index-1] = str("\n"+retrait_tab+"  ")
						empan = 0
					None

			texte_modif = ''.join(l_texte)
			self.txtFile.write( texte_modif )

		else :
			self.txtFile.write( texte )

