#!/usr/bin/python3
# encodage : UTF-8

# Richard Renault - juillet 2020

# Statistiques par siècle, par auteur et par recueil

# fichiers en entrée :
#	../BDD/CSV/corpus_nombres.csv
#	../BDD/CSV/corpus_auteurs.csv
#	../BDD/CSV/corpus_coauteurs.csv

# fichiers en sortie :
#	../BDD/CSV/corpus_statistiques.csv 
#	../BDD/TXT/corpus_treemap.txt
#	../BDD/TXT/corpus_nuage.txt

"""
─────────────────────────────────────────────────────────────────────────
Programme sous licence libre

Licence CECILL version Version 2.1
(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)

© Richard Renault, CRISCO, Université de Caen - Normandie

Ce programme informatique a été réalisé dans le cadre d'un projet
de traitement automatique de textes versifiés.

Ce programme est régi par la licence CeCILL soumise au droit français
et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
conditions de la licence CeCILL telle que diffusée par le CEA, le CNRS
et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
─────────────────────────────────────────────────────────────────────────
"""

# mars 2022
# correction intégrant l'alerte :
# FutureWarning: Indexing with multiple keys (implicitly converted to a tuple of keys) will be deprecated, use a list instead.


import sys
import os
import csv
import time
import re

import locale
locale.setlocale(locale.LC_ALL, '')

import numpy as np
import pandas as pd

from matplotlib import pyplot
from matplotlib import patches

from pprint import pprint
from modules_python import affiche_contrôle
from modules_python import affiche_traitement
from modules_python import Dialect
from modules_python import Txt
from modules_python import Csv
from modules_python import CSV_lecture_coauteurs


class lecture_corpus_nombres( object ):
	"""
	Lecture du fichier csv des données numériques recueil par recueil.
	Les oeuvres écrites en collaboration sont traitées à part.
	La première ligne du fichier CSV (description des champs) n'est pas prise
	en compte.
	"""
	def __new__( self, fichier_in1):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( fichier_in1 ), 'anam' )

		# ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		l_data = []

		for ligne in tmp_csv:
			if ligne[6].startswith('16') :
				siècle = '17'
			elif ligne[6].startswith('17') :
				siècle = '18'
			elif ligne[6].startswith('18') :
				siècle = '19'
			elif ligne[6].startswith('19') :
				siècle = '20'
			else :
				siècle = "??"

			l_data.append([ligne[1], ligne[2], ligne[3], ligne[4], ligne[6], siècle, int(ligne[7]), int(ligne[8]), int(ligne[9]), int(ligne[10]), int(ligne[11])])

		return l_data


class lecture_corpus_auteurs( object ):
	"""
	Lecture du fichier csv des auteurs du corpus.
	La première ligne du fichier CSV (description des champs) n'est pas prise
	en compte.
	"""
	def __new__( self, csv_file) :

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )

		# ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		d_auteurs = {}

		for ligne in tmp_csv:

			# août 2024
			# ignore les auteurs sans dates et les auteurs dont une date manque

			if re.match(r"\d{4}-\d{4}", ligne[5]) :

				# présence d'un nom de plume
				if ligne[3] != '' :
					d_auteurs[ligne[0]] = (ligne[3], self.quel_siècle_auteur(ligne[0], ligne[5]))
				else :
					# présence d'une particule
					if ligne[4] != '' :
						d_auteurs[ligne[0]] = (ligne[1]+" "+ligne[4]+" "+ligne[2].upper(), self.quel_siècle_auteur(ligne[0], ligne[5]))
					else :
						d_auteurs[ligne[0]] = (ligne[1]+" "+ligne[2].upper(), self.quel_siècle_auteur(ligne[0], ligne[5]))

		return d_auteurs

	def quel_siècle_auteur( code, dates) :
		"""
		Détermine l'appartenance à un siècle à partir de la date de naissance.
		"""

		date_naissance = dates.split('-')[0]
		année_référence = int(date_naissance)+30
		siècle = str(int(str(année_référence)[0:2])+1)

		return siècle


class Traitement_données( object ) :
	'''
	Analyse des données : siècles, auteurs et recueils.
	'''
	def __init__ ( self, lecture_CSV, d_auteurs, d_coauteurs, fichier_out1, fichier_out2, fichier_out3, version ) :

		# lecture du numéro de version du corpus
		f_version = open(version, 'r')
		lecture_version = f_version.read()
		n_version = lecture_version
		f_version.close()

		d_csv = {}
		l_csv = []

		données_CSV = lecture_CSV

		self.date_traitement = time.strftime( "%Y-%m-%d" )
		df_données = pd.DataFrame(données_CSV, columns = [	'code_auteur',
															'auteur',
															'code_recueil',
															'titre_recueil',
															'date',
															'siècle',
															'nbr_poèmes',
															'nbr_pièces',
															'nbr_vers_poèmes',
															'nbr_vers_pièces',
															'nbr_vers_total' ])

		###### par recueils (recopie des données initiales)
		for l in données_CSV :
			l_csv.append([l[0],l[1],l[2],l[3],l[4],l[5],'',l[6],l[7],l[8],l[9],l[10]])

		###### par siècle
		# dictionnaire du nombre de recueils par siècle
		d_nbr_recueils_par_siècle = df_données.groupby(['siècle'])['code_recueil'].count().to_dict()

		# liste des données (nombre de vers) par siècle
		
		# mars 2022 : modification prenant en compte le message d'alerte (Pandas) :
		# FutureWarning: Indexing with multiple keys (implicitly converted to a tuple of keys) will be deprecated, use a list instead.
		#l_nbr_vers_recueil = df_données.groupby(['siècle'])['nbr_poèmes','nbr_pièces','nbr_vers_poèmes', 'nbr_vers_pièces', 'nbr_vers_total'].sum().reset_index(drop=False).values.tolist()

		l_nbr_vers_recueil = df_données.groupby(['siècle'])[['nbr_poèmes','nbr_pièces','nbr_vers_poèmes', 'nbr_vers_pièces', 'nbr_vers_total']].sum().reset_index(drop=False).values.tolist()


		for l in l_nbr_vers_recueil :
			# ajout du nombre de recueils
			for r in d_nbr_recueils_par_siècle.keys() :
				if l[0] == r :
					l_csv.append(['','','','','',l[0],d_nbr_recueils_par_siècle[r],l[1],l[2],l[3],l[4],l[5]])

		###### par auteur
		# dictionnaire du nombre de recueils par auteur
		d_nbr_recueils_par_auteur = df_données.groupby(['code_auteur'])['code_recueil'].count().to_dict()

		# liste des données (nombre de vers) par auteur
		
		# mars 2022 : modification prenant en compte le message d'alerte (Pandas) :
		# FutureWarning: Indexing with multiple keys (implicitly converted to a tuple of keys) will be deprecated, use a list instead.
		#l_nbr_vers_auteur = df_données.groupby(['code_auteur', 'auteur'])['nbr_poèmes','nbr_pièces','nbr_vers_poèmes', 'nbr_vers_pièces', 'nbr_vers_total'].sum().reset_index(drop=False).values.tolist()

		l_nbr_vers_auteur = df_données.groupby(['code_auteur', 'auteur'])[['nbr_poèmes','nbr_pièces','nbr_vers_poèmes', 'nbr_vers_pièces', 'nbr_vers_total']].sum().reset_index(drop=False).values.tolist()


		for l in l_nbr_vers_auteur :
			# ajout du nombre de recueils
			for r in d_nbr_recueils_par_auteur.keys() :
				if l[0] == r :
					l_csv.append([l[0],l[1],'','','','',d_nbr_recueils_par_auteur[r],l[2],l[3],l[4],l[5],l[6]])


		####### nombre de vers par année (année de création)
		l_nbr_vers_an = df_données.groupby(['date'])['nbr_vers_total'].sum().reset_index(drop=False).values.tolist()

		d_nbr_vers_siècle = {}

		s17 = 0
		s18 = 0
		s19 = 0
		s20 = 0
		total = 0

		for an in l_nbr_vers_an :

			if an[0].startswith('16') :
				s17 += an[1]
			elif an[0].startswith('17') :
				s18 += an[1]
			elif an[0].startswith('18') :
				s19 += an[1]
			elif an[0].startswith('19') :
				s20 += an[1]

		d_nbr_vers_siècle['17'] = s17
		d_nbr_vers_siècle['18'] = s18
		d_nbr_vers_siècle['19'] = s19
		d_nbr_vers_siècle['20'] = s20

		total_vers_siècle =s17+s18+s19+s20

		self.graphique_partition_siècle(d_nbr_vers_siècle, total_vers_siècle, n_version)


		# pour treemap Google

		d_coauteurs_inv = dict(map(reversed, d_coauteurs.items()))

		l_treemap = []

		l_treemap.append(['Corpus Malherbe', None, 0])
		l_treemap.append(['17', 'Corpus Malherbe', None])
		l_treemap.append(['18', 'Corpus Malherbe', None])
		l_treemap.append(['19', 'Corpus Malherbe', None])
		l_treemap.append(['20', 'Corpus Malherbe', None])

		for auteur in d_auteurs.keys() :
			l_treemap.append([d_auteurs[auteur][0], d_auteurs[auteur][1], None])

		for l in l_csv :
			
			if l[3] != '' :

				
				# recueils écrits en collaboration
				if l[2] in d_coauteurs.keys() :

					# pas de & dans le HTML
					noms_auteurs = l[1].replace('&', '&amp;')

					if [l[3]+' ('+l[2]+')', noms_auteurs, l[11]] not in l_treemap :
						l_treemap.append([noms_auteurs, l[5], None])
						l_treemap.append([l[3]+' ('+l[2]+')', noms_auteurs, l[11]])
				else :

					if l[0] in d_auteurs.keys() :
						l_treemap.append([l[3]+' ('+l[2]+')', l[1], l[11]])

		# écriture des données pour la carte propotionnelle
		self.ecriture_txt_carte(l_treemap)

		# écriture des données pour le nuage de mots
		self.ecriture_txt_nuage(l_nbr_vers_auteur)

		# écriture des données dans le fichier csv des données statistiques
		self.ecriture_csv(l_csv)


	def graphique_partition_siècle(self, valeurs, total, n_version) :
		"""
		Graphique en camenbert de la répartition du corpus selon les siècles.
		"""
		#print(valeurs)
		#print(total)
		#print(n_version)

		total_espace = "{:,d}".format(total).replace(',', ' ')

		l_valeurs = []

		for s in sorted(valeurs.keys()) :
			l_valeurs.append(valeurs[s])

		pyplot.rcParams.update({'mathtext.default': 'regular' }) 
		pyplot.rcParams['font.size'] = 14
		pyplot.rcParams['patch.edgecolor'] = 'white'

		# Pie chart
		l_siècles = ['$XVII^{e}$', '$XVIII^{e}$', '$XIX^{e}$', '$XX^{e}$']
		couleurs = ['#165044', '#761723', '#2A4965', '#BDA83B']

		fig1, ax1 = pyplot.subplots(figsize=(8, 6))

		patches, texts, autotexts = ax1.pie(
			l_valeurs,
			colors = couleurs,
			labels=l_siècles,
			autopct='%1.1f%%',
			startangle=90,
			counterclock=False,
			wedgeprops={'linewidth': 0, 'linestyle': 'solid', 'antialiased': True}
			)

		for text in texts:
			text.set_color('black')

		for autotext in autotexts:
			autotext.set_color('white')

		pyplot.subplots_adjust(top = 0.74, bottom = 0.02, right = 0.95, left = 0.29, hspace = 0, wspace = 0)

		ax1.axis('equal')  

		pyplot.title("RÉPARTITION DU CORPUS SELON LES SIÈCLES\n(date d'édition du recueil)\n\npour un total de "+str(total_espace)+" vers\n\n", fontsize=14)

		texte_17 = '$XVII^{e}$ : '+"{:,d}".format(l_valeurs[0]).replace(',', ' ')+" vers"
		texte_18 = '$XVIII^{e}$ : '+"{:,d}".format(l_valeurs[1]).replace(',', ' ')+" vers"
		texte_19 = '$XIX^{e}$ : '+"{:,d}".format(l_valeurs[2]).replace(',', ' ')+" vers"
		texte_20 = '$XX^{e}$ : '+"{:,d}".format(l_valeurs[3]).replace(',', ' ')+" vers"

		ax1.legend(patches, [texte_17, texte_18, texte_19, texte_20],
			frameon = False,
			fontsize = 14,
			loc="center right",
			bbox_to_anchor=(0, 0, 0.1, 1))

		pyplot.figtext(0.02, 0.2, "\n\nversion : "+n_version+"\n"+time.strftime('%B %Y'))

		#pyplot.show()

		chemin = "../../BDD/Graphiques/" 

		if os.path.exists(chemin) == True :
			
			pyplot.savefig('../../BDD/Graphiques/SVG/partition_corpus.svg')
			pyplot.savefig('../../BDD/Graphiques/PNG/partition_corpus.png')
		else :
			print ("Le répertoire : "+chemin+" n'existe pas !")
			sys.exit(-1)

		pyplot.close()


	def ecriture_txt_carte(self, l_treemap) :
		"""
		Écriture des données pour le fichier TXT de la carte proportionnelle.
		"""

		fichier_txt = fichier_out2
		txt_stats = Txt(fichier_txt)

		for l in l_treemap :
			l_modif = str(l).replace('None', 'null')
			txt_stats.écrire (l_modif)
			txt_stats.écrire (',')
			txt_stats.retour_ligne()


	def ecriture_txt_nuage(self, l_nbr_vers_auteur) :
		"""
		Écriture des données pour le fichier TXT de la carte proportionnelle.
		"""

		fichier_txt_nuage = fichier_out3
		txt_stats_nuage = Txt(fichier_txt_nuage)

		for l in l_nbr_vers_auteur :

			txt_stats_nuage.écrire (l[1])
			txt_stats_nuage.écrire (',')
			txt_stats_nuage.écrire (str(l[6]))
			txt_stats_nuage.retour_ligne()


	def ecriture_csv(self, l_csv) :
		"""
		Écriture des données pour le fichier CSV des données statistiques.
		"""

		fichier_csv = fichier_out1

		csv_stats = Csv(fichier_csv)

		csv_stats.ecrire ("ID_AUTEUR\tAUTEUR\tID_RECUEIL\tTITRE_RECUEIL\tDATE_CREATION\tSIÈCLE\tNBR_RECUEILS\tNBR_POEMES\tNBR_PIECES\tNBR_VERS_POEMES\tNBR_VERS_PIECES\tNBR_VERS_TOTAL\tDATE")
		csv_stats.retour_ligne()

		for ligne in l_csv :
			csv_stats.ecrire \
			( \
				str(ligne[0]), \
				str(ligne[1]), \
				str(ligne[2]), \
				str(ligne[3]), \
				str(ligne[4]), \
				str(ligne[5]), \
				str(ligne[6]), \
				str(ligne[7]), \
				str(ligne[8]), \
				str(ligne[9]), \
				str(ligne[10]),\
				str(ligne[11]),\
				self.date_traitement \
			)
			csv_stats.retour_ligne()


if __name__ == '__main__':

	args = sys.argv[1:]
	if len(args) != 7 :
		print ("Il faut que le programme python ait une ligne de commande comme :\n")
		print ("statistiques_auteurs.py version.txt corpus_nombres.csv .corpus_auteurs.csv corpus_coauteurs.csv corpus_statistiques.csv corpus_treemap.txt corpus_nuage.txt")
		sys.exit(-1)
	fichier_in0 = args[0]
	fichier_in1 = args[1]
	fichier_in2 = args[2]
	fichier_in3 = args[3]

	fichier_out1 = args[4]
	fichier_out2 = args[5]
	fichier_out3 = args[6]

	if not os.path.isfile(fichier_in0):
		print ('fichier: ' + fichier_in0 + " n'existe pas")
		sys.exit(-1)

	if not os.path.isfile(fichier_in1):
		print ('fichier: ' + fichier_in1 + " n'existe pas")
		sys.exit(-1)

	if not os.path.isfile(fichier_in2):
		print ('fichier: ' + fichier_in2 + " n'existe pas")
		sys.exit(-1)

	if not os.path.isfile(fichier_in3):
		print ('fichier: ' + fichier_in3 + " n'existe pas")
		sys.exit(-1)

lire_csv_d_coauteurs = CSV_lecture_coauteurs(fichier_in3)
lire_csv_auteurs = lecture_corpus_auteurs(fichier_in2)
lire_csv_nombres = lecture_corpus_nombres(fichier_in1)

traitement = Traitement_données(lire_csv_nombres, lire_csv_auteurs, lire_csv_d_coauteurs, fichier_out1, fichier_out2, fichier_out3, fichier_in0)

