#!/usr/bin/python3
# encodage : UTF-8

#Richard Renault - janvier 2022
#création d'un nuage de mots au format SVG

# ligne de commande : nuage_mots.py liste.csv police.ttf graphique.svg

# Le fichier SVG généré contient une licence CC BY-NC-SA ; 
# voir le contenu de la variable metadata_svg qui introduit l'entête XML.
# Modifier le texte des éléments correspondant au nom du créateur, nom du fichier
# et description du graphique. 
# Modifier éventuellement le choix de la licence ou retirer cet élément.

"""
─────────────────────────────────────────────────────────────────────────
Programme sous licence libre

Licence CECILL version Version 2.1
(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)

© Richard Renault, CRISCO, Université de Caen - Normandie

Ce programme informatique a été réalisé dans le cadre d'un projet
de traitement automatique de textes versifiés.

Ce programme est régi par la licence CeCILL soumise au droit français
et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
conditions de la licence CeCILL telle que diffusée par le CEA, le CNRS
et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
─────────────────────────────────────────────────────────────────────────
"""

import sys
import os
import csv
import random
import multidict as multidict
from wordcloud import WordCloud
from os import path
from PIL import Image


class Traitement( object ) :
	'''
	classe principale
	'''
	def __init__ ( self, liste, police, graphique ):

		# chemin pour utiliser la police ttf locale
		d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

		fontFILE = d + police

		fontFamily = 'Bodoni'
		fontWeight = 'Bold'

		# métadonnées pour le fichier SVG
		metadata_svg = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?> \n\
<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"1000\" height=\"1000\">\n\
<title>Nuage Corpus Malherbə</title>\n\
<desc>Nuage de mots sur le nombre de vers par auteur</desc>\n\
<metadata id=\"license\" \n\t\
xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n\t\
xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n\t\
xmlns:cc=\"http://creativecommons.org/ns#\">\n\t\
<rdf:RDF>\n\t\t\
<cc:Work rdf:about=\"\">\n\t\t\t\
<dc:format>image/svg+xml</dc:format>\n\t\t\t\
<dc:type rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n\t\t\t\
<cc:license rdf:resource=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\" />\n\t\t\
</cc:Work>\n\t\t\
<cc:License rdf:about=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\">\n\t\t\t\
<cc:permits rdf:resource=\"http://creativecommons.org/ns#Reproduction\" />\n\t\t\t\
<cc:permits rdf:resource=\"http://creativecommons.org/ns#Distribution\" />\n\t\t\t\
<cc:requires rdf:resource=\"http://creativecommons.org/ns#Notice\" />\n\t\t\t\
<cc:requires rdf:resource=\"http://creativecommons.org/ns#Attribution\" />\n\t\t\t\
<cc:permits rdf:resource=\"http://creativecommons.org/ns#DerivativeWorks\" />\n\t\t\t\
<cc:requires rdf:resource=\"http://creativecommons.org/ns#ShareAlike\" />\n\t\t\
</cc:License>\n\t\
</rdf:RDF>\n\
</metadata>"

		# création du dictionnaire des données
		dict_mots = {}
		mon_dict = self.faire_dict(liste)

		# conversion du dictionnaire ordinaire en multidict
		dict_pour_nuage = self.conversion_multidict(mon_dict)

		#print(dict_pour_nuage)
		self.faire_graphique_SVG(dict_pour_nuage, police, graphique, metadata_svg)

	def faire_dict (self, liste) :
		"""
		lecture du fichier CSV des données pour un dictionnaire
		"""
		mon_dict = {}
		lire = csv.reader(open(liste))
		for ligne in lire:
			mon_dict[ligne[0]] = int(ligne[1])
		return mon_dict


	def conversion_multidict(self, mon_dict) :
		"""
		conversion d'un dictionnaire ordinaire en multidict
		"""
		dict_pour_nuage = multidict.MultiDict()
		for k in mon_dict.keys() :
			dict_pour_nuage.add(k, mon_dict[k])
		return dict_pour_nuage


	def random_color_func(self, word=None, font_size=None, position=None, font_path=None, orientation=None,  random_state=None):
		"""
		Contrôle de la couleur du texte
		origine : https://amueller.github.io/word_cloud/generated/wordcloud.random_color_func.html
		"""
		h = int(0.0 * 21.0 / 255.0)
		s = int(0.0 * 255.0 / 255.0)
		l = int(90.0 * float(random_state.randint(10, 80)) / 255.0)

		return "hsl({}, {}%, {}%)".format(h, s, l)


	def faire_graphique_SVG(self, dict_pour_nuage, fontFILE, graphique, metadata_svg):
		
		"""
		background_color=None pour un fond transparent
		"""

		wc = WordCloud(font_path=fontFILE, background_color=None, max_words=1000, width=1000, height=1000, margin=2)
		wc.generate_from_frequencies(dict_pour_nuage)
		wc.recolor(color_func=self.random_color_func, random_state=3)
		wc_svg = wc.to_svg(embed_font=True, optimize_embedded_font=True)


		wc_svg_modif = wc_svg.replace('<svg xmlns="http://www.w3.org/2000/svg" width="1000" height="1000">', metadata_svg)

		#print(wc_svg_modif)
		sortie = open(graphique,"w")
		sortie.write(wc_svg_modif)
		
		sortie.close()


if __name__ == '__main__':

	args = sys.argv[1:]
	if len(args) != 3 :
		print ("Il faut que le programme ait une ligne de commande comme :\n")
		print ("nuage_mots.py liste.csv font.ttf graphique.svg")
		sys.exit(-1)

	inFileName_1 = args[0]
	inFileName_2 = args[1]

	outFileName_3 = args[2]

	if not os.path.isfile(inFileName_1):
		print ('fichier: ' + inFileName_1 + " n'existe pas")
		sys.exit(-1)

	if not os.path.isfile(inFileName_2):
		print ('fichier: ' + inFileName_2 + " n'existe pas")
		sys.exit(-1)


faire_nuage = Traitement(inFileName_1, inFileName_2, outFileName_3)

