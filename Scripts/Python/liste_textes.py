#!/usr/bin/python3
# -*- coding: UTF-8 -*-

# Ce programme permet de créer la liste des recueils et des pièces de théâtre par auteur
# Le fichier de sortie, au format XML

# Richard Renault, mai 2020


import sys
import os
import re
import csv
import time

from operator import itemgetter, attrgetter

import locale
locale.setlocale(locale.LC_ALL, '')



from pprint import pprint

def affiche_traitement(ligne, couleur='blanc') :
	if couleur == "vert" :
		code = '\033[32m '
	elif couleur == "rouge" :
		code = '\033[31m '
	elif couleur == "jaune" :
		code = '\033[93m '
	elif couleur == "cyan" :
		code = '\033[36m '
	elif couleur == "bleu-clair" :
		code = '\033[94m '
	elif couleur == "magenta" :
		code = '\033[35m '
	elif couleur == "magenta-clair" :
		code = '\033[95m '
	elif couleur == "bleu-blanc" :
		code = '\033[47m\033[34m '
	elif couleur == "magenta-blanc" :
		code = '\033[47m\033[35m '
	elif couleur == "blanc" :
		code = '\033[39m '
	elif couleur == "vert-clair" :
		code = '\033[92m '
	elif couleur == "gris-clair" :
		code = '\033[37m '
	elif couleur == "fond_jaune" :
		code = '\t\033[43;1m'
	else :
		code = '\033[39m '
	print(code+ligne+" \033[0m")


# remplace print
# tellement plus pratique !!
def affiche(texte) :
	print(texte)


#csv.field_size_limit(sys.maxsize)
maxInt = sys.maxsize

class Dialect( csv.Dialect ):
	"""Dialecte représentant la convention d'écriture des fichiers CSV au sein du projet:
	séparation avec des tabulations et retours à la ligne."""
	def __init__( self ):
		self.delimiter = "\t"
		#R.R fin de ligne Unix et non Windows
		self.lineterminator = '\n'
		self._valid = True
		self._name = 'anam'
		self.quoting = csv.QUOTE_NONE


class Csv:
	"""Classe permettant d'écrire dans un CSV pendant le traitement."""

	def __init__( self, csvFileName ):
		self.csvFile = open( csvFileName, 'w' )

	def close( self ):
		"""Ferme le fichier de CSV. Doit être invoqué à la fin d'un traitement utilisant un CSV."""
		self.csvFile.close()

	def ecrire( self, *texte ):
		"""Ecrit dans le fichier CSV le texte en argument.
		@param *texte: plusieures string"""
		# R.R. modification pour un traitement correcte des champs vides
		# parcours sur les indices de la liste et non sur les éléments
		for i in range(len(texte)):
			if i == range(len(texte))[-1] :
				# R.R. pas de tabulation si l'élement est le dernier
				##print(texte[i])
				self.csvFile.write( texte[i] )
			else :
				self.csvFile.write( texte[i] )
				self.csvFile.write( "\t" )

	def retour_ligne( self ):
		"""Ecrit un retour à la ligne dans le fichier CSV."""
		self.csvFile.write( '\n' )

	def tab ( self ):
		"""Introduit une tabulations	dans le fichier CSV."""
		self.csvFile.write( "\t" )


class CSV_lecture_auteurs( list ):
	"""
	Lecture du fichier CSV des auteurs
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )
		#l_data = []
		#l_auteurs = []
		#l_recueils = []

		d_auteurs = {}

		#ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv:

			code = ""
			nom = ""
			date = ""
			nom_tri = ""

			code = ligne[0]

			# nom de plume
			if ligne[3] != '' :
				nom = ligne[3]
				nom_tri = ligne[3]

				if ligne[5] != "" :
					date = "("+ligne[5]+")"
				else :
					date = ""

			else :
				# avec particule
				if ligne[4] != "" :
					nom = ligne[1]+" "+ligne[4]+" "+ligne[2]
					nom_tri = ligne[2]

					if ligne[5] != "" :
						if ligne[5] != "" :
							date = "("+ligne[5]+")"
						else :
							date = ""

				else :
					#print(ligne)
					nom = ligne[1]+" "+ligne[2]
					nom_tri = ligne[2]

					if ligne[5] != "" :
						if ligne[5] != "" :
							date = "("+ligne[5]+")"
						else :
							date = ""

			#print(nom_tri)
			#print(nom)
			#print(date)
			d_auteurs[code] = (nom_tri, nom, date)

		return d_auteurs


class CSV_lecture_coauteurs( list ):
	"""
	Lecture du fichier CSV des coauteurs
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )
		#l_data = []
		#l_auteurs = []
		#l_recueils = []

		code_recueil = ""

		d_coauteurs = {}

		#ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv :
			coauteurs = []
			if ligne != [] :
				code_recueil = ligne[0]
				l_co = ligne[1].split(" ")
				for c in l_co :
					if c != "" :
						coauteurs.append(c)
				d_coauteurs[code_recueil] = coauteurs

		##print(d_coauteurs)
		return d_coauteurs


class CSV_lecture_recueils( list ):
	"""
	Lecture du fichier CSV des recueils
	"""

	def __new__( self, csv_file, d_licences):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )
		#l_data = []
		#l_auteurs = []
		#l_recueils = []

		code_recueil = ""

		d_recueils = {}

		#ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv :

			#print(ligne)
			code_recueil = ligne[2]
			genre = ligne[6]
			titre = ligne[3]
			date = ligne[4]
			date_tri = ligne[5]
			nbr_p = ligne[7]
			nbr_d = ligne[8]

			#print(code_recueil)
			#print(d_licences[code_recueil])

			licence = d_licences.get(code_recueil, "free")

			d_recueils[code_recueil] = (genre, titre, date, date_tri, nbr_p, nbr_d, licence)

		##pprint(d_recueils)

		return d_recueils


class CSV_lecture_effectifs( list ):
	"""
	Lecture du fichier CSV des effectifs du corpus
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )

		d_effectifs = {}

		#ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv :

			if ligne != [] :

				d_effectifs["nbr_auteurs"] = ligne[0]
				d_effectifs["nbr_recueils"] = ligne[1]
				d_effectifs["nbr_poèmes"] = ligne[2]
				d_effectifs["nbr_pièces"] = ligne[3]
				d_effectifs["nbr_vers"] = ligne[4]
				d_effectifs["date_effectifs"] = ligne[5]

		return d_effectifs


class CSV_lecture_licences( list ):
	"""
	Lecture du fichier CSV des licences
	"""

	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )

		code_recueil = ""
		d_licences = {}

		#ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv :

			#print(ligne)
			code_recueil = ligne[0]
			licence = ligne[2]

			d_licences[code_recueil] = licence

		##pprint(d_licences)

		return d_licences


class Faire_liste( object ):
	"""
	classe principale
	"""

	def __init__ ( self, d_auteurs, d_coauteurs, d_licences, d_effectifs, fichier_xml) :

		d_auteurs_tri = {}

		#date_traitement = time.strftime( "%Y-%m-%d" )
		date_traitement = time.strftime( "%d-%m-%Y" )

		# liste ordonnée des auteurs
		for coauteur in d_coauteurs :

			l_noms = []
			l_ref_aut = []

			for aut in d_coauteurs[coauteur] :
				nom = d_auteurs[aut][0]
				nom_complet = d_auteurs[aut][1]
				date = d_auteurs[aut][2]
				code = aut

				ref_aut = (nom_complet, date, "["+code+"]")

				l_noms.append(nom)
				l_ref_aut.append(ref_aut)

			t_ref_aut = tuple(l_ref_aut)
			s_noms = " ".join(l_noms)

			d_auteurs_tri[s_noms] = (coauteur[0:3], t_ref_aut)

		for auteur in d_auteurs.keys() :

			# mai 2022
			# la clé du dictionnaire de tri est un tuple : nom de l'auteur + prénom, nom
			# afin avoir les homonymes : (BARBIER, Jules BARBIER) et (BARBIER, Auguste BARBIER)
			# erreur signalée par F. Demay

			#print(d_auteurs[auteur])

			if d_auteurs[auteur][0] == '(anonyme)' :
				d_auteurs_tri[('','')] = (auteur, (("(anonyme)", d_auteurs[auteur][2], "["+auteur+"]"),))

			else :
				d_auteurs_tri[(d_auteurs[auteur][0], d_auteurs[auteur][1])] = (auteur, ((d_auteurs[auteur][1], d_auteurs[auteur][2], "["+auteur+"]"),))


		#pprint(d_auteurs_tri)
		#pprint(d_recueils)

		# dictionnaire des recueils de poèmes
		d_recueils_p = {}
		l_auteurs_p = []

		# dictionnaire des pièces de théâtre
		d_recueils_d = {}
		l_auteurs_d = []

		for k in d_recueils.keys() :

			if d_recueils[k][0] == 'pièce de théâtre' :
				d_recueils_d[k] = d_recueils[k]
				l_auteurs_d.append(k[0:3])
			else :
				
				# mai 2022 : les recueils qui n'ont qu'une seule pièce de théâtre sont mis avec les pièces
				if d_recueils[k][4] == '0' and d_recueils[k][5] != '0':
					d_recueils_d[k] = d_recueils[k]
					l_auteurs_d.append(k[0:3])
				else :
					d_recueils_p[k] = d_recueils[k]
					l_auteurs_p.append(k[0:3])

		s_auteurs_p = set(l_auteurs_p)
		s_auteurs_d = set(l_auteurs_d)

		##pprint(s_auteurs_p)

		# écriture du fichier xml

		fichier_xml = open(outFileName_1, "a")
		fichier_xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')
		fichier_xml.write("\t<CORPUS>\n")
		fichier_xml.write("\t\t<titre></titre>\n")
		fichier_xml.write("\t\t<version>Corpus Malherbe, version 3.7</version>\n")

		# faire la liste des recueils de poèmes
		Liste_textes(d_auteurs_tri, d_recueils_p, fichier_xml, s_auteurs_p, "Recueils de poèmes")

		fichier_xml.write("\t\t<pb />\n")

		# faire la liste des recueils des pièces de théâtre
		Liste_textes(d_auteurs_tri, d_recueils_d, fichier_xml, s_auteurs_d, "Pièces de théâtre")

		fichier_xml.write("\t\t<date_traitement>"+date_traitement+"</date_traitement>\n")

		# écriture des données numériques

		# le nombre de recueil : calcul interne
		# Un recueil de poésies constitué uniquement d'une pièce de théâtre n'est pas comptabilisé comme recueil de poésies (ex: HUG_19, Le Pape)
		nbr_recueils = len(d_recueils_p.keys())

		fichier_xml.write(
			"\t\t<effectifs>\n"
			+"\t\t\t<effectifs_titre>Extension du corpus :</effectifs_titre>\n"
			+"\t\t\t<ligne_effectifs><puce_auteur>■ </puce_auteur><nbr_corpus>"+d_effectifs["nbr_auteurs"]+" auteurs</nbr_corpus></ligne_effectifs>\n"
			+"\t\t\t<ligne_effectifs><puce_auteur>■ </puce_auteur><nbr_corpus>"+str(nbr_recueils)+" recueils de poésies</nbr_corpus></ligne_effectifs>\n"
			+"\t\t\t<ligne_effectifs><puce_auteur>■ </puce_auteur><nbr_corpus>"+d_effectifs["nbr_pièces"]+" pièces de théâtre</nbr_corpus></ligne_effectifs>\n"
			+"\t\t\t<ligne_effectifs><puce_auteur>■ </puce_auteur><nbr_corpus>"+d_effectifs["nbr_poèmes"]+" poèmes</nbr_corpus></ligne_effectifs>\n"
			+"\t\t\t<ligne_effectifs><puce_auteur>■ </puce_auteur><nbr_corpus>"+d_effectifs["nbr_vers"]+" vers</nbr_corpus></ligne_effectifs>\n"
			+"\t\t</effectifs>\n"
			+"<lb/>\n"
			+"<lb/>\n"
			+"<lb/>\n"
			+"<lb/>\n"
			+"<f_note>* Texte sous droits. Le texte est analysé mais n'est pas mis à disposition.</f_note>\n"
			+"<site_git>https://git.unicaen.fr/malherbe/corpus</site_git>\n"
			+"\t</CORPUS>\n"
			)

		fichier_xml.close()

def Liste_textes (d_auteurs_tri, d_recueils_genre, fichier_xml, s_auteurs, titre) :
	"""
	Méthode pour écrire les données auteur par auteur
	"""
	#print(d_recueils_genre.keys())

	fichier_xml.write("\t\t<liste_recueils>\n")
	fichier_xml.write("\t\t\t<titre_liste_recueils>▶ "+titre+" :</titre_liste_recueils> \n")

	# mai 2022 : tri alphébétique optimisé
	for c in sorted(d_auteurs_tri.keys(), key=lambda e: (locale.strxfrm(e[0]), locale.strxfrm(e[1]))):

		code_recueil = d_auteurs_tri[c][0]

		if code_recueil in s_auteurs :

			# plusieurs auteurs
			if len(d_auteurs_tri[c][1]) == 2 :

				fichier_xml.write(
					"\t\t\t<auteur>\n\t\t\t\t<ref_auteur><puce_auteur>■ </puce_auteur>"
					+"<nom_auteur>"
					+d_auteurs_tri[c][1][0][0]
					+"</nom_auteur> "
					+d_auteurs_tri[c][1][0][1]
					+" "
					+d_auteurs_tri[c][1][0][2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
					+ " et "
					+"<nom_auteur>"
					+d_auteurs_tri[c][1][1][0]
					+"</nom_auteur> "
					+d_auteurs_tri[c][1][1][1]
					+" "
					+d_auteurs_tri[c][1][1][2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
					+"</ref_auteur>\n"
					)

			elif len(d_auteurs_tri[c][1]) > 2 :

				fichier_xml.write(
					"\t\t\t<auteur>\n\t\t\t\t<ref_auteur><puce_auteur>■ </puce_auteur>"
					)

				for co in d_auteurs_tri[c][1] :

					if co != d_auteurs_tri[c][1][-1] :

						if co != d_auteurs_tri[c][1][-2] :

							fichier_xml.write(
								"<nom_auteur>"
								+co[0]
								+"</nom_auteur> "
								+co[1]
								+" "
								+co[2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
								+ " , "
								)

						else :

							fichier_xml.write(
								"<nom_auteur>"
								+co[0]
								+"</nom_auteur> "
								+co[1]
								+" "
								+co[2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
								+ " et "
								)

					else :

						fichier_xml.write(
							"<nom_auteur>"
							+co[0]
							+"</nom_auteur> "
							+co[1]
							+" "
							+co[2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
							)

				fichier_xml.write(
					"</ref_auteur>\n"
					)

			else :
				fichier_xml.write(
					"\t\t\t<auteur>\n\t\t\t\t<ref_auteur><puce_auteur>■ </puce_auteur>"
					+"<nom_auteur>"
					+d_auteurs_tri[c][1][0][0]
					+"</nom_auteur> "
					+d_auteurs_tri[c][1][0][1]
					+" "
					+d_auteurs_tri[c][1][0][2].replace("[",'<code_auteur>').replace("]","</code_auteur>")
					+"</ref_auteur>\n"
					)

			# dictionnaire des recueils de l'auteur
			d_recueils_genre_auteur = {}
			
			for cr in d_recueils_genre.keys() :
				
				if cr.startswith(code_recueil) :
					#print("\t"+str(d_recueils_genre[cr]))
					d_recueils_genre_auteur[cr] = d_recueils_genre[cr]

			#pprint(d_recueils_genre_auteur)

			fichier_xml.write(
			"\t\t\t\t<recueils>\n"
			)

			for cr_tri in sorted(d_recueils_genre_auteur.items(), key=lambda t: t[1][3]) :
				#print(cr_tri[1][3])

				#if d_auteurs_tri[c][0] == 'MUS' :
					#print(cr_tri[1][4])

				fichier_xml.write(
				"\t\t\t\t\t<recueil><puce_recueil>■</puce_recueil> "
				+cr_tri[1][1]+", "
				+cr_tri[1][2]
				)

				if titre == 'Recueils de poèmes' :

					if cr_tri[1][4] != '0' and cr_tri[1][5] != '0' :
						if cr_tri[1][4] == '1' :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poème, "
							)
						else :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poèmes, "
							)

						if cr_tri[1][5] == '1' :
							fichier_xml.write(
							cr_tri[1][5]+" pièce de théâtre) "
							)
						else :
							fichier_xml.write(
							cr_tri[1][5]+" pièces de théâtre) "
							)

					elif cr_tri[1][4] != '0' and cr_tri[1][5] == '0' :
						if cr_tri[1][4] == '1' :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poème) "
							)
						else :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poèmes) "
							)

				else :
					if cr_tri[1][4] != '0' :
						if cr_tri[1][4] == '1' :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poème inséré)"
							)
						else :
							fichier_xml.write(
							" ("
							+cr_tri[1][4]+" poèmes insérés)"
							)

				fichier_xml.write(
				"<code_recueil>"
				+cr_tri[0]
				+"</code_recueil>\n"
				)

				if cr_tri[1][6] == 'restricted' :
					fichier_xml.write(
					"*"
					)

				fichier_xml.write(
				"</recueil>\n"
				)

			fichier_xml.write(
			"\t\t\t\t</recueils>\n"
			)

			fichier_xml.write(
				"\t\t\t</auteur>\n"
				)


		else :

			#print(d_auteurs_tri[c])
			None


	fichier_xml.write(
		"\t\t</liste_recueils>\n"
		)


if __name__ == '__main__':

	args = sys.argv[1:]
	if len(args) != 6 :
		print ("Il faut que le programme python ait une ligne de commande comme :\n")
		print (" ")
		sys.exit(-1)

	inFileName_1 = args[0]
	inFileName_2 = args[1]
	inFileName_3 = args[2]
	inFileName_4 = args[3]
	inFileName_5 = args[4]

	outFileName_1 = args[5]


	if not os.path.isfile(inFileName_1):
		print ('fichier: ' + inFileName_1 + " n'existe pas")
		sys.exit(-1)


# traitement de la liste csv des auteurs
#affiche('\033[47m\033[34m■ Création du fichier XML de la liste des recueils'+' \033[0m')
d_auteurs = CSV_lecture_auteurs(inFileName_1)
#pprint(d_auteurs)

# traitement de la liste csv des coauteurs
d_coauteurs = CSV_lecture_coauteurs(inFileName_2)
#print(d_coauteurs)

# traitement de la liste csv des licences
d_licences = CSV_lecture_licences(inFileName_5)
#print(d_licences)

# traitement de la liste csv des recueils
d_recueils = CSV_lecture_recueils(inFileName_3, d_licences)
#print(d_recueils)

# traitement de la liste csv des effectifs
d_effectifs = CSV_lecture_effectifs(inFileName_4)
#print(d_effectifs)



# faire la liste des recueils par auteur
faire = Faire_liste(d_auteurs, d_coauteurs, d_licences, d_effectifs, outFileName_1)



