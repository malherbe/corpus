#!/usr/bin/python3
# encodage: UTF-8

# Richard Renault - juin 2020
# Liste des participants et des tâches effectuées

# fichiers en entrée : ../BDD/CSV/corpus_préparation.csv
# les données sont extraites de l'entête XML-TEI de chaque fichier du corpus Malherbe

# fichiers en sortie :../Documents/Liste_des_participants.txt

"""
─────────────────────────────────────────────────────────────────────────
Programme sous licence libre

Licence CECILL version Version 2.1
(https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)

© Richard Renault, CRISCO, Université de Caen - Normandie

Ce programme informatique a été réalisé dans le cadre d'un projet
de traitement automatique de textes versifiés.

Ce programme est régi par la licence CeCILL soumise au droit français
et respectant les principes de diffusion des logiciels libres.
Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
conditions de la licence CeCILL telle que diffusée par le CEA, le CNRS
et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
─────────────────────────────────────────────────────────────────────────
"""

import sys
import os
import csv
import time

from operator import itemgetter, attrgetter
from pprint import pprint
from modules_python import affiche_contrôle
from modules_python import affiche_traitement
from modules_python import Dialect
from modules_python import Txt

def affiche(texte) :
	print(texte)

class CSV_lecture_intervenants():
	"""
	R.R.
	méthode pour lire le fichier csv
	- ne prend pas en compte la première ligne (description des champs)
	"""
	def __new__( self, csv_file):

		dialect = Dialect()
		csv.register_dialect( 'anam', dialect )
		tmp_csv = csv.reader( open( csv_file ), 'anam' )

		d_intervenants = {}
		l_data = []

		# ne prend pas en compe la première ligne du fichier csv ()
		ligne = next(tmp_csv)

		for ligne in tmp_csv:
			if ligne != [] :

				l_nom = ligne[4].split(" ")
				nom = l_nom[-1]
				prénom = l_nom[0]
				t_nom = (nom,prénom)

				if t_nom not in d_intervenants.keys() :
					d_intervenants[t_nom] = [ [ligne[6], ligne[0], ligne[1], ligne[2], ligne[5] ] ]

				else :
					d_intervenants[t_nom].append([ligne[6], ligne[0], ligne[1], ligne[2], ligne[5] ])

				l_data.append([ligne[0], ligne[1], ligne[2], ligne[4], ligne[5], ligne[6]])

		# tri des données : année, code du recueil
		for intervenant in d_intervenants.keys() :

			#pprint(d_intervenants[intervenant])
			sorted(d_intervenants[intervenant], key=itemgetter(0,1))

		#pprint(d_intervenants)

		return d_intervenants, l_data



class Traitement_Liste( object ) :
	'''
	Analyse des données
	'''
	def __init__ ( self, lecture_CSV, inFileName_2) :

		self.date_traitement = time.strftime( "%d-%m-%Y" )
		d_intervenants = lecture_CSV[0]
		self.ecriture_txt(d_intervenants, inFileName_2)



	def ecriture_txt(self, d_intervenants, inFileName_2) :
		"""
		écriture des données dans un fichier txt
		"""

		txt_intervenants = Txt(inFileName_2)

		txt_intervenants.retour_ligne()
		
		txt_intervenants.écrire("┌───────────────┐")
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("│Corpus Malherbə│")
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("└───────────────┘")
		txt_intervenants.retour_ligne()
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("LISTE DES PARTICIPANTS ET DES TÂCHES EFFECTUÉES")
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("───────────────────────────────────────────────")
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("Les informations sont extraites de l'entête XML-TEI des fichiers du corpus.")
		txt_intervenants.retour_ligne()
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("Les dates données pour chaque participant correspondent aux dates de la première")
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire("version du texte au format XML-TEI du corpus Malherbə.")
		txt_intervenants.retour_ligne()
		txt_intervenants.retour_ligne()

		for nom in sorted(d_intervenants.keys(), key=itemgetter(0)):

			#print(nom)

			txt_intervenants.retour_ligne()
			txt_intervenants.tab()
			txt_intervenants.écrire('▪ '+nom[1]+' '+nom[0])
			txt_intervenants.retour_ligne()

			d_tmp = {}

			for i in d_intervenants[nom] :

				#print(i[0])

				if i[0] not in d_tmp.keys() :
					d_tmp[i[0]] = [i[1:]]

				else :
					d_tmp[i[0]].append(i[1:])

			#pprint(d_tmp)

			for an in sorted(d_tmp.keys()) :

				txt_intervenants.tab()
				txt_intervenants.tab()
				txt_intervenants.écrire('▫ '+an)
				txt_intervenants.retour_ligne()

				for recueil in sorted(d_tmp[an], key=itemgetter(0)) :

					txt_intervenants.tab()
					txt_intervenants.tab()
					txt_intervenants.tab()
					txt_intervenants.ajuster('▸ '+recueil[0]+' : '+recueil[1]+', '+recueil[2], 60, 3)
					txt_intervenants.retour_ligne()
					txt_intervenants.tab()
					txt_intervenants.tab()
					txt_intervenants.tab()
					txt_intervenants.tab()

					txt_intervenants.ajuster('▹ '+recueil[3], 60, 4)
					txt_intervenants.retour_ligne()

		txt_intervenants.retour_ligne()
		txt_intervenants.retour_ligne()
		txt_intervenants.écrire(self.date_traitement)


if __name__ == '__main__':

	args = sys.argv[1:]
	if len(args) != 2 :
		print ("Il faut que le programme python ait une ligne de commande comme :\n")
		print (" ")
		sys.exit(-1)
	inFileName_1 = args[0]
	inFileName_2 = args[1]


	if not os.path.isfile(inFileName_1):
		print ('fichier: ' + inFileName_1 + " n'existe pas")
		sys.exit(-1)


lecture_CSV = CSV_lecture_intervenants(inFileName_1)

traitement = Traitement_Liste(lecture_CSV, inFileName_2)

