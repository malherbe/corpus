#!

# Création d'une carte proportionnelle du corpus par siècles, auteurs, et recueils

# Les données sont incluses à partir du fichier /BDD/TXT/corpus_treemap.txt
# généré par le programme statistiques_auteurs.sh et et statistiques_auteurs.py

# fichier au format HTML : /HTML/treemap_corpus.html
# feuille de style : /HTML/css/treemap_google.css
# programme de création de la carte en web service : 

# ─────────────────────────────────────────────────────────────────────────
# Programme sous licence libre
# Licence CECILL version Version 2.1
# (https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)
# © Richard Renault, CRISCO, Université de Caen - Normandie
# ─────────────────────────────────────────────────────────────────────────



echo -e " \033[44m\033[37m  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ \033[0m"; tput sgr0
echo -e " \033[44m\033[37m  ◼ Corpus Malherbe                                         \033[0m"; tput sgr0
echo -e " \033[47m\033[34m  ◼ Création de la carte proportionnelle du corpus          \033[0m"; tput sgr0


read version < version.txt


echo -e "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" >
   <head>
	<title>MÉTRIQUE EN LIGNE</title>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>
	<meta name=\"description\" content=\"traitement automatique de textes versifiés\"></meta>
	<meta name=\"keywords\" content=\"métrique poésie versification\"></meta>
	<meta name=\"author\" content=\"Richard renault - CRISCO Université de Caen Normandie\"></meta>
	<link type=\"text/css\" rel=\"stylesheet\" href=\"css/treemap_google.css\"></link>
	<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>
	<script type=\"text/javascript\">
	google.charts.load('current', {'packages':['treemap']});
	google.charts.setOnLoadCallback(drawChart);
	 function drawChart() {
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'ID');
	data.addColumn('string', 'Parent');
	data.addColumn('number', 'Number Of Lines');
	data.addRows(
	[
	" > ../HTML/treemap_corpus.html



	cat ../BDD/TXT/corpus_treemap.txt >> ../HTML/treemap_corpus.html

echo -e "
]
	);

	var tree = new google.visualization.TreeMap(document.getElementById('chart_div'));

	var options = {
		highlightOnMouseOver: true,
		maxDepth: 1,
		maxPostDepth: 2,

		
		minHighlightColor: '#006661',
		midHighlightColor: '#fff',
		maxHighlightColor: '#FDFEFF',

		minColor: '#003E89',
		midColor: '#fff',
		maxColor: '#D5E9FF',
		
		headerHeight: 25,
		headerColor: 'D8D8D8',
		
		showScale: true,
		height: 800,
		useWeightedAverageForAggregation: true
	};
		tree.draw(data, options);
	}
	</script>


   </head>
   <body>
	<div class=\"page\">
		<div class=\"titre\">
			<h2>Corpus Malherb&#601;, version : 3.7</h2>
			<h2>Carte proportionnelle du corpus par siècles, auteurs et recueils</h2>
			<p>La taille des cellules est proportionnelle au nombre de vers.</p>
			<div>
				<p><span class=clic> &lt;clic droit&gt;</span> pour zoomer sur une zone</p>
				<p><span class=clic> &lt;clic gauche&gt;</span> pour revenir en arrière</p>
			</div>
		</div>
		<div id=\"chart_div\" style=\"width: 800px; height: 800px;\"></div>
	</div>
   </body>
</html>
	" >> ../HTML/treemap_corpus.html
