<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LA BONNE CHANSON</title>
				<title type="medium">Une édition électronique</title>
				<author key="VER">
					<name>
						<forename>Paul</forename>
						<surname>VERLAINE</surname>
					</name>
					<date from="1844" to="1896">1844-1896</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML, Application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>394 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">VER_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Paul Verlaine</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Œuvres_complètes_de_Paul_Verlaine</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres complètes</title>
								<author>Paul Verlaine</author>
								<edition>3ème édition</edition>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2029102</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Léon Vanier</publisher>
									<date when="1902">1902</date>
								</imprint>
								<biblScope unit="tome">1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres posthumes</title>
						<author>Paul Verlaine</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Page:Verlaine_-_%C5%92uvres_posthumes,_Messein,_II.djvu/241</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres posthumes,</title>
								<author>Paul Verlaine</author>
								<idno type="URL">https://archive.org/details/oeuvresposthumes02verluoft</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Messein</publisher>
									<date when="1913">1913</date>
								</imprint>
								<biblScope unit="tome">2</biblScope>
							</monogr>
							<note>source du poème "DÉDICACE MANUSCRITE DE LA BONNE CHANSON"</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
		<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2011-10-12" who="RR">Quelques corrections typographiques ; Ajout des balises lg dans les poèmes qui n’en avaient pas et ajout du poème "à ma bien aimée".</change>
			<change when="2012-02-21" who="RR">Ajout d’un identifiant pour le recueil.</change>
			<change when="2012-06-04" who="RR">Ajout d’un vers manquant dans VER95 : De par la grâce, le sourire et la bonté.</change>
			<change when="2016-03-04" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			<change when="2017-07-26" who="RR">Ajout du poème "DÉDICACE MANUSCRITE DE LA BONNE CHANSON".</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="VER91">
				<head type="main">DÉDICACE MANUSCRITE DE LA BONNE CHANSON</head>
				<opener>
					<salute>à Mme M. M. de F.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Faut-il donc que ce petit livre</l>
					<l n="2" num="1.2">Où plein d’espoir chante l’Amour,</l>
					<l n="3" num="1.3">Te trouve souffrante en ce jour,</l>
					<l n="4" num="1.4">Toi, pour qui seule je veux vivre ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Faut il qu’au moment tant béni</l>
					<l n="6" num="2.2">Ce mal affreux t’ait disputée</l>
					<l n="7" num="2.3">A ma tendresse épouvantée</l>
					<l n="8" num="2.4">Et de ton chevet m’ait banni ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— Mais puisque enfin sourit encore</l>
					<l n="10" num="3.2">Après l’orage terminé</l>
					<l n="11" num="3.3">L’avenir, le front couronné</l>
					<l n="12" num="3.4">De fleurs qu’un joyeux soleil dore,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Espérons, ma mie, espérons !</l>
					<l n="14" num="4.2">Va ! les heureux de cette vie</l>
					<l n="15" num="4.3">Bientôt nous porteront envie,</l>
					<l n="16" num="4.4">Tellement nous nous aimerons !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">5 juillet 1870.</date>
					</dateline>
				</closer>
			</div>
			<div type="poem" key="VER92">
				<head type="number">I</head>
				<lg n="1">
					<l n="1" num="1.1">Le soleil du matin doucement chauffe et dore</l>
					<l n="2" num="1.2">Les seigles et les blés tout humides encore,</l>
					<l n="3" num="1.3">Et l’azur a gardé sa fraîcheur de la nuit.</l>
					<l n="4" num="1.4">L’on sort sans autre but que de sortir ; on suit,</l>
					<l n="5" num="1.5">Le long de la rivière aux vagues herbes jaunes,</l>
					<l n="6" num="1.6">Un chemin de gazon que bordent de vieux aunes.</l>
					<l n="7" num="1.7">L’air est vif. Par moment un oiseau vole avec</l>
					<l n="8" num="1.8">Quelque fruit de la haie ou quelque paille au bec,</l>
					<l n="9" num="1.9">Et son reflet dans l’eau survit à son passage.</l>
					<l part="I" n="10" num="1.10">C’est tout.</l>
					<l part="F" n="10" num="1.10">Mais le songeur aime ce paysage</l>
					<l n="11" num="1.11">Dont la claire douceur a soudain caressé</l>
					<l n="12" num="1.12">Son rêve de bonheur adorable, et bercé</l>
					<l n="13" num="1.13">Le souvenir charmant de cette jeune fille,</l>
					<l n="14" num="1.14">Blanche apparition qui chante et qui scintille,</l>
					<l n="15" num="1.15">Dont rêve le poète et que l’homme chérit,</l>
					<l n="16" num="1.16">Évoquant en ses vœux dont peut-être on sourit</l>
					<l n="17" num="1.17">La Compagne qu’enfin il a trouvée, et l’âme</l>
					<l n="18" num="1.18">Que son âme depuis toujours pleure et réclame.</l>
				</lg>
			</div>
			<div type="poem" key="VER93">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1">Toute grâce et toutes nuances</l>
					<l n="2" num="1.2">Dans l’éclat doux de ses seize ans,</l>
					<l n="3" num="1.3">Elle a la candeur des enfances</l>
					<l n="4" num="1.4">Et les manèges innocents.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ses yeux, qui sont les yeux d’un ange,</l>
					<l n="6" num="2.2">Savent pourtant, sans y penser,</l>
					<l n="7" num="2.3">Éveiller le désir étrange</l>
					<l n="8" num="2.4">D’un immatériel baiser.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et sa main, à ce point petite</l>
					<l n="10" num="3.2">Qu’un oiseau-mouche n’y tiendrait,</l>
					<l n="11" num="3.3">Captive sans espoir de fuite,</l>
					<l n="12" num="3.4">Le cœur pris par elle en secret.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">L’intelligence vient chez elle</l>
					<l n="14" num="4.2">En aide à l’âme noble ; elle est</l>
					<l n="15" num="4.3">Pure autant que spirituelle :</l>
					<l n="16" num="4.4">Ce qu’elle a dit, il le fallait</l>
				</lg>

				<lg n="5">
					<l n="17" num="5.1">Et si la sottise l’amuse</l>
					<l n="18" num="5.2">Et la fait rire sans pitié,</l>
					<l n="19" num="5.3">Elle serait, étant la muse,</l>
					<l n="20" num="5.4">Clémente jusqu’à l’amitié,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Jusqu’à l’amour — qui sait ? peut-être,</l>
					<l n="22" num="6.2">A l’égard d’un poète épris</l>
					<l n="23" num="6.3">Qui mendierait sous sa fenêtre,</l>
					<l n="24" num="6.4">L’audacieux ! un digne prix</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">De sa chanson bonne ou mauvaise !</l>
					<l n="26" num="7.2">Mais témoignant sincèrement,</l>
					<l n="27" num="7.3">Sans fausse note et sans fadaise,</l>
					<l n="28" num="7.4">Du doux mal qu’on souffre en aimant.</l>
				</lg>
			</div>
			<div type="poem" key="VER94">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1">En robe grise et verte avec des ruches,</l>
					<l n="2" num="1.2">Un jour de juin que j’étais soucieux,</l>
					<l n="3" num="1.3">Elle apparut souriante à mes yeux</l>
					<l n="4" num="1.4">Qui l’admiraient sans redouter d’embûches ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elle alla, vint, revint, s’assit, parla,</l>
					<l n="6" num="2.2">Légère et grave, ironique, attendrie :</l>
					<l n="7" num="2.3">Et je sentais en mon âme assombrie</l>
					<l n="8" num="2.4">Comme un joyeux reflet de tout cela ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Sa voix, étant de la musique fine,</l>
					<l n="10" num="3.2">Accompagnait délicieusement</l>
					<l n="11" num="3.3">L’esprit sans fiel de son babil charmant</l>
					<l n="12" num="3.4">Où la gaîté d’un bon cœur se devine.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Aussi soudain fus-je, après le semblant</l>
					<l n="14" num="4.2">D’une révolte aussitôt étouffée,</l>
					<l n="15" num="4.3">Au plein pouvoir de la petite Fée</l>
					<l n="16" num="4.4">Que depuis lors je supplie en tremblant.</l>
				</lg>
			</div>
			<div type="poem" key="VER95">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1">Puisque l’aube grandit, puisque voici l’aurore,</l>
					<l n="2" num="1.2">Puisque, après m’avoir fui longtemps, l’espoir veut bien</l>
					<l n="3" num="1.3">Revoler devers moi qui l’appelle et l’implore,</l>
					<l n="4" num="1.4">Puisque tout ce bonheur veut bien être le mien,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’en est fait à présent des funestes pensées,</l>
					<l n="6" num="2.2">C’en est fait des mauvais rêves, ah ! c’en est fait</l>
					<l n="7" num="2.3">Surtout de l’ironie et des lèvres pincées</l>
					<l n="8" num="2.4">Et des mots où l’esprit sans l’âme triomphait.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Arrière aussi les poings crispés et la colère</l>
					<l n="10" num="3.2">A propos des méchants et des sots rencontrés ;</l>
					<l n="11" num="3.3">Arrière la rancune abominable ! arrière</l>
					<l n="12" num="3.4">L’oubli qu’on cherche en des breuvages exécrés !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Car je veux, maintenant qu’un Être de lumière</l>
					<l n="14" num="4.2">A dans ma nuit profonde émis cette clarté</l>
					<l n="15" num="4.3">D’une amour à la fois immortelle et première,</l>
					<l n="16" num="4.4">De par la grâce, le sourire et la bonté,</l>
				</lg>

				<lg n="5">
					<l n="17" num="5.1">Je veux, guidé par vous, beaux yeux aux flammes douces,</l>
					<l n="18" num="5.2">Par toi conduit, ô main où tremblera ma main,</l>
					<l n="19" num="5.3">Marcher droit, que ce soit par des sentiers de mousses</l>
					<l n="20" num="5.4">Ou que rocs et cailloux encombrent le chemin ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Oui, je veux marcher droit et calme dans la Vie,</l>
					<l n="22" num="6.2">Vers le but où le sort dirigera mes pas,</l>
					<l n="23" num="6.3">Sans violence, sans remords et sans envie :</l>
					<l n="24" num="6.4">Ce sera le devoir heureux aux gais combats.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Et comme, pour bercer les lenteurs de la route,</l>
					<l n="26" num="7.2">Je chanterai des airs ingénus, je me dis</l>
					<l n="27" num="7.3">Qu’elle m’écoutera sans déplaisir sans doute ;</l>
					<l n="28" num="7.4">Et vraiment je ne veux pas d’autre Paradis.</l>
				</lg>
			</div>
			<div type="poem" key="VER96">
				<head type="number">V</head>
				<lg n="1">
					<l n="1" num="1.1">Avant que tu ne t’en ailles,</l>
					<l n="2" num="1.2">Pâle étoile du matin,</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"/>— Mille cailles</l>
					<l n="4" num="1.4">Chantent, chantent dans le thym. —</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tourne devers le poète,</l>
					<l n="6" num="2.2">Dont les yeux sont pleins d’amour;</l>
					<l n="7" num="2.3"><space quantity="8" unit="char"/>— L’alouette</l>
					<l n="8" num="2.4">Monte au ciel avec le jour. —</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Tourne ton regard que noie</l>
					<l n="10" num="3.2">L’aurore dans son azur ;</l>
					<l n="11" num="3.3"><space quantity="8" unit="char"/>— Quelle joie</l>
					<l n="12" num="3.4">Parmi les champs de blé mûr ! —</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Puis fais luire ma pensée</l>
					<l n="14" num="4.2">Là-bas — bien loin, oh, bien loin !</l>
					<l n="15" num="4.3"><space quantity="8" unit="char"/>— La rosée</l>
					<l n="16" num="4.4">Gaîment brille sur le foin. —</l>
				</lg>

				<lg n="5">
					<l n="17" num="5.1">Dans le doux rêve où s’agite</l>
					<l n="18" num="5.2">Ma mie endormie encor…</l>
					<l n="19" num="5.3"><space quantity="8" unit="char"/>— Vite, vite,</l>
					<l n="20" num="5.4">Car voici le soleil d’or. —</l>
				</lg>
			</div>
			<div type="poem" key="VER97">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1">La lune blanche</l>
					<l n="2" num="1.2">Luit dans les bois;</l>
					<l n="3" num="1.3">De chaque branche</l>
					<l n="4" num="1.4">Part une voix</l>
					<l n="5" num="1.5">Sous la ramée…</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Ô bien-aimée.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">L’étang reflète,</l>
					<l n="8" num="3.2">Profond miroir,</l>
					<l n="9" num="3.3">La silhouette</l>
					<l n="10" num="3.4">Du saule noir</l>
					<l n="11" num="3.5">Où le vent pleure…</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1">Rêvons, c’est l’heure.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Un vaste et tendre</l>
					<l n="14" num="5.2">Apaisement</l>
					<l n="15" num="5.3">Semble descendre</l>
					<l n="16" num="5.4">Du firmament</l>
					<l n="17" num="5.5">Que l’astre irise…</l>
				</lg>
				<lg n="6">
					<l n="18" num="6.1">C’est l’heure exquise.</l>
				</lg>
			</div>
			<div type="poem" key="VER98">
				<head type="number">VII</head>
				<lg n="1">
					<l n="1" num="1.1">Le paysage dans le cadre des portières</l>
					<l n="2" num="1.2">Court furieusement, et des plaines entières</l>
					<l n="3" num="1.3">Avec de l’eau, des blés, des arbres et du ciel</l>
					<l n="4" num="1.4">Vont s’engouffrant parmi le tourbillon cruel</l>
					<l n="5" num="1.5">Où tombent les poteaux minces du télégraphe</l>
					<l n="6" num="1.6">Dont les fils ont l’allure étrange d’un paraphe.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Une odeur de charbon qui brûle et d’eau qui bout,</l>
					<l n="8" num="2.2">Tout le bruit que feraient mille chaînes au bout</l>
					<l n="9" num="2.3">Desquelles hurleraient mille géants qu’on fouette ;</l>
					<l n="10" num="2.4">Et tout à coup des cris prolongés de chouette. —</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">— Que me fait tout cela, puisque j’ai dans les yeux</l>
					<l n="12" num="3.2">La blanche vision qui fait mon cœur joyeux,</l>
					<l n="13" num="3.3">Puisque la douce voix pour moi murmure encore,</l>
					<l n="14" num="3.4">Puisque le Nom si beau, si noble et si sonore</l>
					<l n="15" num="3.5">Se mêle, pur pivot de tout ce tournoiement,</l>
					<l n="16" num="3.6">Au rythme du wagon brutal, suavement.</l>
				</lg>
			</div>
			<div type="poem" key="VER99">
				<head type="number">VIII</head>
				<lg n="1">
					<l n="1" num="1.1">Une Sainte en son auréole,</l>
					<l n="2" num="1.2">Une Châtelaine en sa tour,</l>
					<l n="3" num="1.3">Tout ce que contient la parole</l>
					<l n="4" num="1.4">Humaine de grâce et d’amour ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">La note d’or que fait entendre</l>
					<l n="6" num="2.2">Un cor dans le lointain des bois,</l>
					<l n="7" num="2.3">Mariée à la fierté tendre</l>
					<l n="8" num="2.4">Des nobles Dames d’autrefois !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Avec cela le charme insigne</l>
					<l n="10" num="3.2">D’un frais sourire triomphant</l>
					<l n="11" num="3.3">Éclos dans des candeurs de cygne</l>
					<l n="12" num="3.4">Et des rougeurs de femme-enfant ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Des aspects nacrés, blancs et roses,</l>
					<l n="14" num="4.2">Un doux accord patricien.</l>
					<l n="15" num="4.3">Je vois, j’entends toutes ces choses</l>
					<l n="16" num="4.4">Dans son nom Carlovingien.</l>
				</lg>
			</div>
			<div type="poem" key="VER100">
				<head type="number">IX</head>
				<lg n="1">
					<l n="1" num="1.1">Son bras droit, dans un geste aimable de douceur,</l>
					<l n="2" num="1.2">Repose autour du cou de la petite sœur,</l>
					<l n="3" num="1.3">Et son bras gauche suit le rhythme de la jupe.</l>
					<l n="4" num="1.4">A coup sûr une idée agréable l’occupe,</l>
					<l n="5" num="1.5">Car ses yeux si francs, car sa bouche qui sourit,</l>
					<l n="6" num="1.6">Témoignent d’une joie intime avec esprit.</l>
					<l n="7" num="1.7">Oh ! sa pensée exquise et fine, quelle est-elle ?</l>
					<l n="8" num="1.8">Toute mignonne, tout aimable, et toute belle,</l>
					<l n="9" num="1.9">Pour ce portrait, son goût infaillible a choisi</l>
					<l n="10" num="1.10">La pose la plus simple et la meilleure aussi :</l>
					<l n="11" num="1.11">Debout, le regard droit, en cheveux ; et sa robe</l>
					<l n="12" num="1.12">Est longue juste assez pour qu’elle ne dérobe</l>
					<l n="13" num="1.13">Qu’à moitié sous ses plis jaloux le bout charmant</l>
					<l n="14" num="1.14">D’un pied malicieux imperceptiblement.</l>
				</lg>
			</div>
			<div type="poem" key="VER101">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1">Quinze longs jours encore et plus de six semaines</l>
					<l n="2" num="1.2">Déjà ! Certes, parmi les angoisses humaines</l>
					<l n="3" num="1.3">La plus dolente angoisse est celle d’être loin.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">On s’écrit, on se dit que l’on s’aime, on a soin</l>
					<l n="5" num="2.2">D’évoquer chaque jour la voix, les yeux, le geste</l>
					<l n="6" num="2.3">De l’être en qui l’on met son bonheur, et l’on reste</l>
					<l n="7" num="2.4">Des heures à causer tout seul avec l’absent.</l>
					<l n="8" num="2.5">Mais tout ce que l’on pense et tout ce que l’on sent</l>
					<l n="9" num="2.6">Et tout ce dont on parle avec l’absent, persiste</l>
					<l n="10" num="2.7">À demeurer blafard et fidèlement triste.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Oh ! l’absence ! le moins clément de tous les maux !</l>
					<l n="12" num="3.2">Se consoler avec des phrases et des mots,</l>
					<l n="13" num="3.3">Puiser dans l’infini morose des pensées</l>
					<l n="14" num="3.4">De quoi vous rafraîchir, espérances lassées,</l>
					<l n="15" num="3.5">Et n’en rien remonter que de fade et d’amer !</l>
					<l n="16" num="3.6">Puis voici, pénétrant et froid comme le fer,</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1">Plus rapide que les oiseaux et que les balles</l>
					<l n="18" num="4.2">Et que le vent du sud en mer et ses rafales</l>
					<l n="19" num="4.3">Et portant sur sa pointe aiguë un fin poison,</l>
					<l n="20" num="4.4">Voici venir, pareil aux flèches, le soupçon</l>
					<l n="21" num="4.5">Décoché par le Doute impur et lamentable.</l>
				</lg>
				<lg n="5">
					<l n="22" num="5.1">Est-ce bien vrai ? Tandis qu’accoudé sur ma table</l>
					<l n="23" num="5.2">Je lis sa lettre avec des larmes dans les yeux,</l>
					<l n="24" num="5.3">Sa lettre, où s’étale un aveu délicieux,</l>
					<l n="25" num="5.4">N’est-elle pas alors distraite en d’autres choses ?</l>
					<l n="26" num="5.5">Qui sait ? Pendant qu’ici pour moi lents et moroses</l>
					<l n="27" num="5.6">Coulent les jours, ainsi qu’un fleuve au bord flétri,</l>
					<l n="28" num="5.7">Peut-être que sa lèvre innocente a souri ?</l>
					<l n="29" num="5.8">Peut-être qu’elle est très joyeuse et qu’elle oublie ?</l>
				</lg>
				<lg n="6">
					<l n="30" num="6.1">Et je relis sa lettre avec mélancolie.</l>
				</lg>
			</div>
			<div type="poem" key="VER102">
				<head type="number">XI</head>
				<lg n="1">
					<l n="1" num="1.1">La dure épreuve va finir :</l>
					<l n="2" num="1.2">Mon cœur, souris à l’avenir.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">Ils sont passés les jours d’alarmes</l>
					<l n="4" num="2.2">Où j’étais triste jusqu’aux larmes.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1">Ne suppute plus les instants,</l>
					<l n="6" num="3.2">Mon âme, encore un peu de temps.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1">J’ai tu les paroles amères</l>
					<l n="8" num="4.2">Et banni les sombres chimères.</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1">Mes yeux exilés de la voir</l>
					<l n="10" num="5.2">De par un douloureux devoir,</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1">Mon oreille avide d’entendre</l>
					<l n="12" num="6.2">Les notes d’or de sa voix tendre</l>
				</lg>

				<lg n="7">
					<l n="13" num="7.1">Tout mon être et tout mon amour</l>
					<l n="14" num="7.2">Acclament le bienheureux jour</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1">Où, seul rêve et seule pensée,</l>
					<l n="16" num="8.2">Me reviendra la fiancée !</l>
				</lg>
			</div>
			<div type="poem" key="VER103">
				<head type="number">XII</head>
				<lg n="1">
					<l n="1" num="1.1">Va, chanson, à tire-d’aile</l>
					<l n="2" num="1.2">Au-devant d’elle, et dis lui</l>
					<l n="3" num="1.3">Bien que dans mon cœur fidèle</l>
					<l n="4" num="1.4">Un rayon joyeux a lui,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dissipant, lumière sainte,</l>
					<l n="6" num="2.2">Les ténèbres de l’amour :</l>
					<l n="7" num="2.3">Méfiance, doute, crainte,</l>
					<l n="8" num="2.4">Et que voici le grand jour !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Longtemps craintive et muette,</l>
					<l n="10" num="3.2">Entendez-vous ? la gaîté,</l>
					<l n="11" num="3.3">Comme une vive alouette</l>
					<l n="12" num="3.4">Dans le ciel clair a chanté.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Va donc, chanson ingénue,</l>
					<l n="14" num="4.2">Et que, sans nul regret vain,</l>
					<l n="15" num="4.3">Elle soit la bienvenue</l>
					<l n="16" num="4.4">Celle qui revient enfin.</l>
				</lg>
			</div>
			<div type="poem" key="VER104">
				<head type="number">XIII</head>
				<lg n="1">
					<l n="1" num="1.1">Hier, on parlait de choses et d’autres,</l>
					<l n="2" num="1.2">Et mes yeux allaient recherchant les vôtres ;</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">Et votre regard recherchait le mien</l>
					<l n="4" num="2.2">Tandis que courait toujours l’entretien.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1">Sous le sens banal des phrases pesées</l>
					<l n="6" num="3.2">Mon amour errait après vos pensées ;</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1">Et quand vous parliez, à dessein distrait</l>
					<l n="8" num="4.2">Je prêtais l’oreille à votre secret :</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1">Car la voix, ainsi que les yeux de Celle</l>
					<l n="10" num="5.2">Qui vous fait joyeux et triste décèle,</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1">Malgré tout effort morose et rieur,</l>
					<l n="12" num="6.2">Et met au plein jour l’être intérieur.</l>
				</lg>

				<lg n="7">
					<l n="13" num="7.1">Or, hier je suis parti plein d’ivresse :</l>
					<l n="14" num="7.2">Est-ce un espoir vain que mon cœur caresse,</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1">Un vain espoir, faux et doux compagnon ?</l>
					<l n="16" num="8.2">Oh ! non ! n’est-ce pas ? n’est-ce pas que non ?</l>
				</lg>
			</div>
			<div type="poem" key="VER105">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1">Le foyer, la lueur étroite de la lampe :</l>
					<l n="2" num="1.2">La rêverie avec le doigt contre la tempe</l>
					<l n="3" num="1.3">Et les yeux se perdant parmi les yeux aimés ;</l>
					<l n="4" num="1.4">L’heure du thé fumant et des livres fermés ;</l>
					<l n="5" num="1.5">La douceur de sentir la fin de la soirée ;</l>
					<l n="6" num="1.6">La fatigue charmante et l’attente adorée</l>
					<l n="7" num="1.7">De l’ombre nuptiale et de la douce nuit,</l>
					<l n="8" num="1.8">Oh ! tout cela, mon rêve attendri le poursuit</l>
					<l n="9" num="1.9">Sans relâche, à travers toutes remises vaines,</l>
					<l n="10" num="1.10">Impatient des mois, furieux des semaines !</l>
				</lg>
			</div>
			<div type="poem" key="VER106">
				<head type="number">XV</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai presque peur, en vérité,</l>
					<l n="2" num="1.2">Tant je sens ma vie enlacée</l>
					<l n="3" num="1.3">À la radieuse pensée</l>
					<l n="4" num="1.4">Qui m’a pris l’âme l’autre été,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tant votre image, à jamais chère,</l>
					<l n="6" num="2.2">Habite en ce cœur tout à vous,</l>
					<l n="7" num="2.3">Mon cœur uniquement jaloux</l>
					<l n="8" num="2.4">De vous aimer et de vous plaire ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et je tremble, pardonnez-moi</l>
					<l n="10" num="3.2">D’aussi franchement vous le dire,</l>
					<l n="11" num="3.3">À penser qu’un mot, un sourire</l>
					<l n="12" num="3.4">De vous est désormais ma loi,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et qu’il vous suffirait d’un geste,</l>
					<l n="14" num="4.2">D’une parole ou d’un clin d’œil,</l>
					<l n="15" num="4.3">Pour mettre tout mon être en deuil</l>
					<l n="16" num="4.4">De son illusion céleste.</l>
				</lg>

				<lg n="5">
					<l n="17" num="5.1">Mais plutôt je ne veux vous voir,</l>
					<l n="18" num="5.2">L’avenir dut-il m’être sombre</l>
					<l n="19" num="5.3">Et fécond en peines sans nombre,</l>
					<l n="20" num="5.4">Qu’à travers un immense espoir,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Plongé dans ce bonheur suprême</l>
					<l n="22" num="6.2">De me dire encore et toujours,</l>
					<l n="23" num="6.3">En dépit des mornes retours,</l>
					<l n="24" num="6.4">Que je vous aime, que je t’aime !</l>
				</lg>
			</div>
			<div type="poem" key="VER107">
				<head type="number">XVI</head>
				<lg n="1">
					<l n="1" num="1.1">Le bruit des cabarets, la fange des trottoirs,</l>
					<l n="2" num="1.2">Les platanes déchus s’effeuillant dans l’air noir,</l>
					<l n="3" num="1.3">L’omnibus, ouragan de ferraille et de boues,</l>
					<l n="4" num="1.4">Qui grince, mal assis entre ses quatre roues,</l>
					<l n="5" num="1.5">Et roule ses yeux verts et rouges lentement,</l>
					<l n="6" num="1.6">Les ouvriers allant au club, tout en fumant</l>
					<l n="7" num="1.7">Leur brûle-gueule au nez des agents de police,</l>
					<l n="8" num="1.8">Toits qui dégouttent, murs suintants, pavé qui glisse,</l>
					<l n="9" num="1.9">Bitume défoncé, ruisseaux comblant l’égout,</l>
					<l n="10" num="1.10">Voilà ma route — avec le paradis au bout.</l>
				</lg>
			</div>
			<div type="poem" key="VER108">
				<head type="number">XVII</head>
				<lg n="1">
					<l n="1" num="1.1">N’est-ce pas ? en dépit des sots et des méchants</l>
					<l n="2" num="1.2">Qui ne manqueront pas d’envier notre joie,</l>
					<l n="3" num="1.3">Nous serons fiers parfois et toujours indulgents</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">N’est-ce pas ? nous irons, gais et lents, dans la voie</l>
					<l n="5" num="2.2">Modeste que nous montre en souriant l’Espoir,</l>
					<l n="6" num="2.3">Peu soucieux qu’on nous ignore ou qu’on nous voie.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Isolés dans l’amour ainsi qu’en un bois noir,</l>
					<l n="8" num="3.2">Nos deux cœurs, exhalant leur tendresse paisible,</l>
					<l n="9" num="3.3">Seront deux rossignols qui chantent dans le soir.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Quant au Monde, qu’il soit envers nous irascible</l>
					<l n="11" num="4.2">Ou doux, que nous feront ses gestes ? Il peut bien,</l>
					<l n="12" num="4.3">S’il veut, nous caresser ou nous prendre pour cible.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Unis par le plus fort et le plus cher lien,</l>
					<l n="14" num="5.2">Et d’ailleurs, possédant l’armure adamantine,</l>
					<l n="15" num="5.3">Nous sourirons à tous et n’aurons peur de rien.</l>
				</lg>

				<lg n="6">
					<l n="16" num="6.1">Sans nous préoccuper de ce que nous destine</l>
					<l n="17" num="6.2">Le Sort, nous marcherons pourtant du même pas,</l>
					<l n="18" num="6.3">Et la main dans la main, avec l’âme enfantine</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">De ceux qui s’aiment sans mélange, n’est-ce pas ?</l>
				</lg>
			</div>

			<div type="poem" key="VER109">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1">Nous sommes en des temps infâmes</l>
					<l n="2" num="1.2">Où le mariage des âmes</l>
					<l n="3" num="1.3">Doit sceller l’union des cœurs</l>
					<l n="4" num="1.4">À cette heure d’affreux orages,</l>
					<l n="5" num="1.5">Ce n’est pas trop de deux courages</l>
					<l n="6" num="1.6">Pour vivre sous de tels vainqueurs.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">En face de ce que l’on ose</l>
					<l n="8" num="2.2">Il nous siérait, sur toute chose,</l>
					<l n="9" num="2.3">De nous dresser, couple ravi</l>
					<l n="10" num="2.4">Dans l’extase austère du juste,</l>
					<l n="11" num="2.5">Et proclamant, d’un geste auguste,</l>
					<l n="12" num="2.6">Notre amour fier, comme un défi !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Mais quel besoin de te le dire ?</l>
					<l n="14" num="3.2">Toi la bonté, toi le sourire,</l>
					<l n="15" num="3.3">N’es-tu pas le conseil aussi,</l>
					<l n="16" num="3.4">Le bon conseil loyal et brave,</l>
					<l n="17" num="3.5">Enfant rieuse au penser grave,</l>
					<l n="18" num="3.6">À qui tout mon cœur dit : merci !</l>
				</lg>
			</div>
			<div type="poem" key="VER110">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1">Donc, ce sera par un clair jour d’été :</l>
					<l n="2" num="1.2">Le grand soleil, complice de ma joie,</l>
					<l n="3" num="1.3">Fera, parmi le satin et la soie,</l>
					<l n="4" num="1.4">Plus belle encor votre chère beauté ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Le ciel tout bleu, comme une haute tente,</l>
					<l n="6" num="2.2">Frissonnera somptueux à longs plis</l>
					<l n="7" num="2.3">Sur nos deux fronts heureux qu’auront pâlis</l>
					<l n="8" num="2.4">L’émotion du bonheur et l’attente ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et quand le soir viendra, l’air sera doux</l>
					<l n="10" num="3.2">Qui se jouera, caressant, dans vos voiles,</l>
					<l n="11" num="3.3">Et les regards paisibles des étoiles</l>
					<l n="12" num="3.4">Bienveillamment souriront aux époux.</l>
				</lg>
			</div>
			<div type="poem" key="VER111">
				<head type="number">XX</head>
				<lg n="1">
					<l n="1" num="1.1">J’allais par des chemins perfides,</l>
					<l n="2" num="1.2">Douloureusement incertain.</l>
					<l n="3" num="1.3">Vos chères mains furent mes guides.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">Si pâle à l’horizon lointain</l>
					<l n="5" num="2.2">Luisait un faible espoir d’aurore ;</l>
					<l n="6" num="2.3">Votre regard fut le matin.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Nul bruit, sinon son pas sonore,</l>
					<l n="8" num="3.2">N’encourageait le voyageur.</l>
					<l n="9" num="3.3">Votre voix me dit : « Marche encore ! »</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Mon cœur craintif, mon sombre cœur</l>
					<l n="11" num="4.2">Pleurait, seul, sur la triste voie ;</l>
					<l n="12" num="4.3">L’amour, délicieux vainqueur,</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Nous a réunis dans la joie.</l>
				</lg>
			</div>
			<div type="poem" key="VER112">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1">L’hiver a cessé : la lumière est tiède</l>
					<l n="2" num="1.2">Et danse, du sol au firmament clair.</l>
					<l n="3" num="1.3">Il faut que le cœur le plus triste cède</l>
					<l n="4" num="1.4">À l’immense joie éparse dans l’air.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Même ce Paris maussade et malade</l>
					<l n="6" num="2.2">Semble faire accueil aux jeunes soleils</l>
					<l n="7" num="2.3">Et, comme pour une immense accolade,</l>
					<l n="8" num="2.4">Tend les mille bras de ses toits vermeils.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">J’ai depuis un an le printemps dans l’âme</l>
					<l n="10" num="3.2">Et le vert retour du doux floréal,</l>
					<l n="11" num="3.3">Ainsi qu’une flamme entoure une flamme,</l>
					<l n="12" num="3.4">Met de l’idéal sur mon idéal,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Le ciel bleu prolonge, exhausse et couronne</l>
					<l n="14" num="4.2">L’immuable azur où rit mon amour.</l>
				</lg>

				<lg n="5">
					<l n="15" num="5.1">La saison est belle et ma part est bonne</l>
					<l n="16" num="5.2">Et tous mes espoirs ont enfin leur tour.</l>
				</lg>
				<lg n="6">
					<l n="17" num="6.1">Que vienne l’été ! que viennent encore</l>
					<l n="18" num="6.2">L’automne et l’hiver ! Et chaque saison</l>
					<l n="19" num="6.3">Me sera charmante, ô Toi que décore</l>
					<l n="20" num="6.4">Cette fantaisie et cette raison !</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>