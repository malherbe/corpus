<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES CONTEMPORAINS DE "PARALLÈLEMENT"</title>
				<title type="medium">Édition électronique</title>
				<author key="VER">
					<name>
						<forename>Paul</forename>
						<surname>VERLAINE</surname>
					</name>
					<date from="1844" to="1896">1844-1896</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="SN">
						<forename>Sabine</forename>
						<surname>Nguyen</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique, vérification, correction des données analysées et actualisation de l’entête</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Préparation du texte et vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>58 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">VER_26</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres poétiques complètes</title>
						<author>Paul Verlaine</author>
						<edition>Texte établi et annoté par Y.-G. Le Dantec, édition révisée, complétée et présentée Jacques Borel</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque de la Pléiade, Gallimard</publisher>
							<date when="1962">1962</date>
						</imprint>
					</monogr>
					<note>Numérisation du texte correspondant à ce recueil.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1889">1889</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>En l’absence de version électronique, le texte des poèmes a été numérisé à partir de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-04" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			<change when="2017-07-30" who="RR">Révision de l’entête concernant l’origine des textes.</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
<text>
	<body>
		<div type="poem" key="VER384">
			<head type="main">EN 17…</head>
			<lg n="1">
				<l n="1" num="1.1">Le parc rit de rayons ramisés,</l>
				<l n="2" num="1.2">De baisers, d’éclats de voix de femmes…</l>
				<l n="3" num="1.3">L’air sent bon, il est tout feux tout flammes</l>
				<l n="4" num="1.4">Et les cœurs, aussi, vont embrasés.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Une flûte au loin sonne la charge</l>
				<l n="6" num="2.2">Des amours altières et frivoles,</l>
				<l n="7" num="2.3">Des amours sincères et des folles,</l>
				<l n="8" num="2.4">Et de l’Amour multiforme et large.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Décor charmant, peuple aimable et fier ;</l>
				<l n="10" num="3.2">Tout n’est là que jeunesse et que joie,</l>
				<l n="11" num="3.3">On perçoit des frôlements de soie,</l>
				<l n="12" num="3.4">On entend des croisements de fer.</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1">Maintes guitares bourdonnent, guêpes</l>
				<l n="14" num="4.2">Du désir élégant et farouche :</l>
				<l n="15" num="4.3">— « Beau masque, on sait tes yeux et ta bouche. »</l>
				<l n="16" num="4.4">Des mots lents flottent comme des crêpes.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1">Pourtant, c’est trop beau, pour dire franc…</l>
				<l n="18" num="5.2">Un pressentiment fait comme une ombre</l>
				<l n="19" num="5.3">À ce tableau d’extases sans nombre,</l>
				<l n="20" num="5.4">Et du noir rampe au nuage blanc !</l>
			</lg>
			<lg n="6">
				<l n="21" num="6.1">Ô l’incroyable mélancolie</l>
				<l n="22" num="6.2">Tombant soudain sur la noble fête !</l>
				<l n="23" num="6.3">De l’orage ? ô non, c’est la tempête !</l>
				<l n="24" num="6.4">L’ennui, le souci ? — C’est la folie !</l>
			</lg>
		<closer>
			<dateline>
				<date when="1889">15 janvier 1889</date>.
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER385">
			<head type="main">ÉCRIT ENTRE CHAMBÉRY ET AIX</head>
			<head type="sub">AU CHER HÔTE, LE RÉVÉREND DOCTEUR H. CAZALIS <lb/>ET À SON CHER VICAIRE</head>
			<lg n="1">
				<l n="1" num="1.1">Deux minutes plus tard</l>
				<l n="2" num="1.2">Je restais à Saint-Pierre,</l>
				<l n="3" num="1.3">Amis, chez Blanc-bolbard</l>
				<l n="4" num="1.4">En vidant la soupière ;</l>
				<l n="5" num="1.5">Pour votre gai pochard</l>
				<l n="6" num="1.6">Dites une prière !</l>
			</lg>
			<lg n="2">
				<l n="7" num="2.1">Ave ! le voilà loin,</l>
				<l n="8" num="2.2">Ô Monsieur le Vicaire</l>
				<l n="9" num="2.3">Pour le soulô du coin</l>
				<l n="10" num="2.4">Lisez votre bréviaire ! !</l>
			</lg>
		<closer>
			<dateline>
				<date when="1889">[1889.]</date>
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER386">
			<head type="main">QUATRAIN</head>
			<lg n="1">
				<l n="1" num="1.1">Ça, c’est un richard qu’on emporte</l>
				<l n="2" num="1.2">À l’établissement thermal.</l>
				<l n="3" num="1.3">J’y fus à pattes — c’est, n’importe !</l>
				<l n="4" num="1.4">Tout ce que je lui veux de mal.</l>
			</lg>
		</div>
		<div type="poem" key="VER387">
			<head type="main">QUATRAIN</head>
			<lg n="1">
				<l n="1" num="1.1">On m’a massé comme un jeune homme</l>
				<l n="2" num="1.2">Ut, ré, mi, fa sol, la si, ut,</l>
				<l n="3" num="1.3">Et douché, fallait voir ! mais comme</l>
				<l n="4" num="1.4">Cela ne m’a pas guéri, zut !</l>
			</lg>
		</div>
		<div type="poem" key="VER388">
			<head type="main">SUR RAOUL PONCHON</head>
			<lg n="1">
				<l n="1" num="1.1">Zut, merde, nom de Dieu ! Ça me rend tout ronchon</l>
				<l n="2" num="1.2">D’fair’ quand j’croyais tout fait, cor’ un’ blagu’ pour Ponchon.</l>
			</lg>
		<closer>
			<dateline>
				<date when="1889">[1889.]</date>
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER389">
			<head type="main">ÉCRIT EN MARGE DE « WILHEILM MEISTER »</head>
			<lg n="1">
				<l n="1" num="1.1">En dépit des clichés et des poëtes blonds</l>
				<l n="2" num="1.2">Et des bas bleus, jetant, par des chemins de Tendre,</l>
				<l n="3" num="1.3">Leur style et leur bonnet à qui les veut entendre,</l>
			</lg>
			<lg n="2">
				<l n="4" num="2.1">À la Mignon pâlotte, œil noir sous des cils longs,</l>
				<l n="5" num="2.2">Je préfère cent fois, cent mille fois Philine,</l>
				<l n="6" num="2.3">Avec son rire franc et sa grâce féline.</l>
			</lg>
			<lg n="3">
				<l n="7" num="3.1">Elle est châtaine, elle est petite, et porte au mieux</l>
				<l n="8" num="3.2">Son babil de perruche et sa rondeur de caille.</l>
				<l n="9" num="3.3">Aussi, comme menant par le nez jeune et vieux,</l>
				<l n="10" num="3.4">Elle fait des amants qu’elle trompe, et s’en raille.</l>
			</lg>
			<lg n="4">
				<l n="11" num="4.1">Elle va, vient, revient, et son chapeau de paille</l>
				<l n="12" num="4.2">S’envole, et le soleil empourpre ses cheveux,</l>
				<l n="13" num="4.3">Et, rythmant son allure et les bonds de sa taille,</l>
				<l n="14" num="4.4">Ses mules à talons font un clic-clac joyeux.</l>
			</lg>
		<closer>
			<dateline>
				<date when="1889">21 novembre 1889</date>.
			</dateline>
		</closer>
		</div>
	</body>
</text>
</TEI>