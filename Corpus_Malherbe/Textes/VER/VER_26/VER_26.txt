Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
VER
VER_26

Paul VERLAINE
1844-1896

POÈMES CONTEMPORAINS DE "PARALLÈLEMENT"
1889
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Sabine Nguyen
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

▫ Richard Renault
  (Application des programmes de traitement automatique, vérification, correction des données analysées et actualisation de l’entête)

▫ Éliane Delente
  (Préparation du texte et vérification des données analysées)

_________________________________________________________________
Origine de la source électronique :

Œuvres poétiques complètes
Paul Verlaine

Bibliothèque de la Pléiade, Gallimard
1962
Numérisation du texte correspondant à ce recueil.

┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



EN 17…

Le parc rit de rayons ramisés,
De baisers, d’éclats de voix de femmes…
L’air sent bon, il est tout feux tout flammes
Et les cœurs, aussi, vont embrasés.


Une flûte au loin sonne la charge
Des amours altières et frivoles,
Des amours sincères et des folles,
Et de l’Amour multiforme et large.


Décor charmant, peuple aimable et fier ;
Tout n’est là que jeunesse et que joie,
On perçoit des frôlements de soie,
On entend des croisements de fer.


Maintes guitares bourdonnent, guêpes
Du désir élégant et farouche :
— « Beau masque, on sait tes yeux et ta bouche. »
Des mots lents flottent comme des crêpes.


Pourtant, c’est trop beau, pour dire franc…
Un pressentiment fait comme une ombre
À ce tableau d’extases sans nombre,
Et du noir rampe au nuage blanc !


Ô l’incroyable mélancolie
Tombant soudain sur la noble fête !
De l’orage ? ô non, c’est la tempête !
L’ennui, le souci ? — C’est la folie !



15 janvier 1889.




ÉCRIT ENTRE CHAMBÉRY ET AIX
AU CHER HÔTE, LE RÉVÉREND DOCTEUR H. CAZALIS 
ET À SON CHER VICAIRE

Deux minutes plus tard
Je restais à Saint-Pierre,
Amis, chez Blanc-bolbard
En vidant la soupière ;
Pour votre gai pochard
Dites une prière !


Ave ! le voilà loin,
Ô Monsieur le Vicaire
Pour le soulô du coin
Lisez votre bréviaire ! !



[1889.]




QUATRAIN

Ça, c’est un richard qu’on emporte
À l’établissement thermal.
J’y fus à pattes — c’est, n’importe !
Tout ce que je lui veux de mal.



QUATRAIN

On m’a massé comme un jeune homme
Ut, ré, mi, fa sol, la si, ut,
Et douché, fallait voir ! mais comme
Cela ne m’a pas guéri, zut !



SUR RAOUL PONCHON

Zut, merde, nom de Dieu ! Ça me rend tout ronchon
D’fair’ quand j’croyais tout fait, cor’ un’ blagu’ pour Ponchon.



[1889.]




ÉCRIT EN MARGE DE « WILHEILM MEISTER »

En dépit des clichés et des poëtes blonds
Et des bas bleus, jetant, par des chemins de Tendre,
Leur style et leur bonnet à qui les veut entendre,


À la Mignon pâlotte, œil noir sous des cils longs,
Je préfère cent fois, cent mille fois Philine,
Avec son rire franc et sa grâce féline.


Elle est châtaine, elle est petite, et porte au mieux
Son babil de perruche et sa rondeur de caille.
Aussi, comme menant par le nez jeune et vieux,
Elle fait des amants qu’elle trompe, et s’en raille.


Elle va, vient, revient, et son chapeau de paille
S’envole, et le soleil empourpre ses cheveux,
Et, rythmant son allure et les bonds de sa taille,
Ses mules à talons font un clic-clac joyeux.



21 novembre 1889.




