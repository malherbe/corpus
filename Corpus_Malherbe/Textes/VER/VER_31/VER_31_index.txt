VER
———
VER_31

Paul VERLAINE
1844-1896

════════════════════════════════════════════
FISCH-TON-KAN

1873

237 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ FISCH-TON-KAN
	─ poème	VER909	PETIT MORCEAU DE SCÈNE
	─ poème	VER910	AIR DE POUSSAH
	─ poème	VER911	TRIO GOULGOULY, FISCH-TON-KAN, KAKAO
	─ poème	VER912	AIR ET DUO
	─ poème	VER913	AIR ET DUO
