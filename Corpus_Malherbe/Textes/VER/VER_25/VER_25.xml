<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">RELIQUAT DE "CELLULAIREMENT" ET POÈMES CONTEMPORAINS DE "SAGESSE"</title>
				<title type="medium">Édition électronique</title>
				<author key="VER">
					<name>
						<forename>Paul</forename>
						<surname>VERLAINE</surname>
					</name>
					<date from="1844" to="1896">1844-1896</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="SN">
						<forename>Sabine</forename>
						<surname>Nguyen</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique, vérification, correction des données analysées et actualisation de l’entête</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Préparation du texte et vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>208 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">VER_25</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres poétiques complètes</title>
						<author>Paul Verlaine</author>
						<edition>Texte établi et annoté par Y.-G. Le Dantec, édition révisée, complétée et présentée Jacques Borel</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque de la Pléiade, Gallimard</publisher>
							<date when="1962">1962</date>
						</imprint>
					</monogr>
					<note>Numérisation du texte correspondant à ce recueil.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1873-1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>En l’absence de version électronique, le texte des poèmes a été numérisé à partir de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-04" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			<change when="2017-07-30" who="RR">Révision de l’entête concernant l’origine des textes.</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
<text>
	<body>
		<div type="poem" key="VER209">
			<head type="main">ΙΗΣΟΥΣ ΧΡΙΣΤΟΣ ΘΕΟΥ ΥΙΟΣ ΣΩΤΗΡ</head>
			<lg n="1">
				<l n="1" num="1.1">Tu ne parles pas, ton sang n’est pas chaud,</l>
				<l n="2" num="1.2">Ton amour fécond reste solitaire.</l>
				<l n="3" num="1.3">L’abîme où tu vis libre est le cachot</l>
				<l n="4" num="1.4">Où se meurt depuis six mille ans la Terre.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Ton œil sans paupière et ton corps sans bras</l>
				<l n="6" num="2.2">Prêche vigilance et dit abstinence.</l>
				<l n="7" num="2.3">Tu planas jadis que les Ararats,</l>
				<l n="8" num="2.4">Confident serein du Déluge immense !</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Tout puissant, tout fort, tout juste et tout saint,</l>
				<l n="10" num="3.2">Tu sauvas Jonas, tu sauvas Tobie,</l>
				<l n="11" num="3.3">Sauve notre cœur que le mal enceint,</l>
				<l n="12" num="3.4">Sauve-nous, Seigneur, et confonds l’Impie !</l>
			</lg>
		<closer>
			<dateline>
				<date when="1873">[1873.]</date>
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER210">
			<head type="main">FAUT HURLER AVEC LES LOUPS !!</head>
			<head type="sub_1">Théâtre des Folies-Hainaut.</head>
			<head type="sub_1">Chansonnette par M. Pablo de Herlanes, <lb/>chantée par Edmond Lepelletier.</head>
			<div type="section" n="1">
				<head type="number">I<hi rend="sup">er</hi> couplet</head>
				<lg n="1">
					<l n="1" num="1.1">Je m’suis marié le cinq ou l’six</l>
					<l n="2" num="1.2">D’Avril ou d’Mai d’l’annéʼ dergnière,</l>
					<l n="3" num="1.3">Je devins veuf le neuf ou l’dix</l>
					<l n="4" num="1.4">D’Juin ou d’Juillet, j’m’en souviens guère…</l>
					<l n="5" num="1.5">— Ah ! mon bonhomm’, me direz-vous,</l>
					<l n="6" num="1.6">Quel malheur ! que j’te trouve à plaindre !…</l>
					<l n="7" num="1.7">— Il faut hurler avec les loups !</l>
					<l n="8" num="1.8"><space quantity="20" unit="char"/>J’vas geindre !</l>
				</lg>
			</div>
			<div type="section" n="2">
				<head type="number">2<hi rend="sup">e</hi> couplet</head>
				<lg n="1">
					<l n="9" num="1.1">Bien que la pertʼ de ma moitié</l>
					<l n="10" num="1.2">Fût pour mon âme un coup bien rude,</l>
					<l n="11" num="1.3">Quéqu’temps après j’me suis r’marié,</l>
					<l n="12" num="1.4">Histoirʼ d’en pas perdrʼ l’habitude…</l>
					<l n="13" num="1.5">— Ah ! mon bonhomm’, me direz-vous,</l>
					<l n="14" num="1.6">C’te fois-ci, ton étoilʼ va r’luire…</l>
					<l n="15" num="1.7">— Il faut hurler avec les loups ! !</l>
					<l n="16" num="1.8"><space quantity="20" unit="char"/>J’vas rire ! !</l>
				</lg>
			</div>
			<div type="section" n="3">
				<head type="number">3<hi rend="sup">e</hi> couplet</head>
				<lg n="1">
					<l n="17" num="1.1">Mais à part qu’elle est chauvʼ tandis</l>
					<l n="18" num="1.2">Qu’l’autʼ s’contentait d’un g’nou modeste,</l>
					<l n="19" num="1.3">Joséphinʼ c’est, quand je vous l’dis,</l>
					<l n="20" num="1.4">L’mêmʼ caractèrʼ que feu Céleste…</l>
					<l n="21" num="1.5">— Ah ! mon bonhomm’, me direz-vous,</l>
					<l n="22" num="1.6">Pour le coup t’as d’la veine à r’vendre,</l>
					<l n="23" num="1.7">— J’veux plus hurler avec les loups !</l>
					<l n="24" num="1.8"><space quantity="20" unit="char"/>J’vas m’pendre !</l>
				</lg>
			</div>
		<closer>
			<dateline>
				<date when="1873">[1873.]</date>
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER211">
			<head type="main">[VIEUX COPPÉE]</head>
			<lg n="1">
				<l n="1" num="1.1">Les écrevisses ont mangé mon cœur qui saigne,</l>
				<l n="2" num="1.2">Et me voici logé maintenant à l’enseigne</l>
				<l n="3" num="1.3">De ceux dont Carjat dit « C’était un beau talent,</l>
				<l n="4" num="1.4">Mais pas de caractère », et je vais, bras ballants,</l>
				<l n="5" num="1.5">Sans limite et sans but, ainsi qu’un fiacre à l’heure,</l>
				<l n="6" num="1.6">Pâle, à jeun, et trouvé trop c. par Gill qui pleure.</l>
				<l n="7" num="1.7">« Mourir, — dormir ! » a dit Shakspeare ; si ce n’est</l>
				<l n="8" num="1.8">Que ça, je cours vers la forêt que l’on connaît,</l>
				<l n="9" num="1.9">Et puisque c’est fictif, j’y vais prendre à mon aise</l>
				<l n="10" num="1.10">Ton beau poëte blond, faune barbizonnaise !</l>
			</lg>
		<closer>
			<dateline>
				<date when="1873">[Été 1873.]</date>
			</dateline>
		</closer>
		</div>
		<div type="poem" key="VER212">
			<head type="main">SUR JULES CLARETIE</head>
			<lg n="1">
				<l n="1" num="1.1">Jules…, non, au fait, ne nommons personne</l>
				<l n="2" num="1.2">(Je le redis aux peuples étonnés)</l>
				<l n="3" num="1.3">N’a pas de cartilages dans le nez ;</l>
				<l n="4" num="1.4">Comment voulez-vous que sa trompe sonne ?</l>
			</lg>
		<closer>
			<dateline>
				<date when="1874">[1874.]</date>
			</dateline>
		</closer>
		</div>
		<div type="part" n="1">
			<head type="main">VIEUX COPPÉES</head>
			<div type="poem" key="VER213">
				<head type="number">I</head>
				<lg n="1">
					<l n="1" num="1.1">Dites, n’avez-vous pas, lecteurs, l’âme attendrie,</l>
					<l n="2" num="1.2">Contemplé quelquefois son image chérie ?</l>
					<l n="3" num="1.3">Tête pale appuyée au revers de sa main,</l>
					<l n="4" num="1.4">César rêve d’hier et pense au lendemain.</l>
					<l n="5" num="1.5">Il évoque les jours de gloire et d’ordre, et songe</l>
					<l n="6" num="1.6">Aux jours où le crédit n’était pas un mensonge.</l>
					<l n="7" num="1.7">Au moins, il s’attendrit sur les chemins de fer</l>
					<l n="8" num="1.8">Très-mous et sur l’emprunt inférieur au pair,</l>
					<l n="9" num="1.9">Puis, triste, il rêve cœur qu’on navre et qui s’effrite,</l>
					<l n="10" num="1.10">À sa si blanche, à sa si pâle Marguerite.</l>
				</lg>
			</div>
			<div type="poem" key="VER214">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1">Pour charmer tes ennuis, ô temps qui nous dévastes,</l>
					<l n="2" num="1.2">Je veux, durant cent vers coupés en dizains chastes</l>
					<l n="3" num="1.3">Comme les ronds égaux d’un même saucisson,</l>
					<l n="4" num="1.4">Servir aux amateurs un plat de ma façon.</l>
					<l n="5" num="1.5">Tout désir un peu sot, toute idée un peu bête</l>
					<l n="6" num="1.6">Et tout ressouvenir stupide mais honnête</l>
					<l n="7" num="1.7">Composeront le fier menu qu’on va licher.</l>
					<l n="8" num="1.8">Muse, accours, donne-moi ton ut le plus léger,</l>
					<l n="9" num="1.9">Et chantons notre gamme en notes bien égales,</l>
					<l n="10" num="1.10">À l’instar de Monsieur Coppée et des cigales.</l>
				</lg>
			</div>
			<div type="poem" key="VER215">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1">Endiguons les ruisseaux : les prés burent assez.</l>
					<l n="2" num="1.2">Bonsoir lecteur, et vous lectrice qui pensez</l>
					<l n="3" num="1.3">D’ailleurs bien plus à Worth qu’aux sons de ma guimbarde</l>
					<l n="4" num="1.4">Agréez le salut respectueux du barde</l>
					<l n="5" num="1.5">Indigne de vos yeux abaissés un instant</l>
					<l n="6" num="1.6">Sur ces cent vers que scande un rythme équilistant ;</l>
					<l n="7" num="1.7">Et vous, protes, n’allez pas rendre encore pire</l>
					<l n="8" num="1.8">Qu’il ne l’est, ce pastiche infâme d’une lyre</l>
					<l n="9" num="1.9">Dûment appréciée entre tous gens de goût</l>
					<l n="10" num="1.10">Par des coquilles trop navrantes. — Et c’est tout ! —</l>
				</lg>
		<closer>
			<dateline>
				<date when="1874">[1874.]</date>
			</dateline>
		</closer>
			</div>
		</div>
		<div type="part" n="2">
			<head type="main">AUTRES VIEUX COPPÉES</head>
			<div type="poem" key="VER216">
				<head type="number">IV</head>
				<head type="main"><hi rend="ital">Ultissima Verba.</hi></head>
				<lg n="1">
					<l n="1" num="1.1">Épris d’absinthe pure et de philomathie</l>
					<l n="2" num="1.2">Je m’emmerde et pourtant au besoin j’apprécie</l>
					<l n="3" num="1.3">Les théâtres qu’on peut avoir à la Gatti.</l>
					<l n="4" num="1.4"><hi rend="ital">Quatre-vingt-treize</hi> a des beautés et c’est senti</l>
					<l n="5" num="1.5">Comme une merde, quoi qu’en disent Gros et tronche</l>
					<l n="6" num="1.6">Et l’Acadême où les Murgers boivent du ponche.</l>
					<l n="7" num="1.7">Mais plus de bleus et la daromphe m’a chié.</l>
					<l n="8" num="1.8">C’est triste et merde alors et que foutre ? J’y ai</l>
					<l n="9" num="1.9">Pensé beaucoup. Carlisse ? Ah ! non, c’est rien qui vaille</l>
					<l n="10" num="1.10">À cause de l’emmerdement de la mitraille !</l>
				</lg>
		<closer>
			<dateline>
				<date when="1875">[24 août 1875.]</date>
			</dateline>
		</closer>
			</div>
			<div type="poem" key="VER217">
				<head type="number">V</head>
				<lg n="1">
					<l n="1" num="1.1">La sale bête ! (En général). Et je m’emmerde !</l>
					<l n="2" num="1.2">Malheur ! Faut-il qu’un temps si précieux se perde ?</l>
					<l n="3" num="1.3">Le russe est sans l’arabe appliqué, j’ai cent mots</l>
					<l n="4" num="1.4">D’Aztec, mais quand viendront ces cent balles ! Chameaux</l>
					<l n="5" num="1.5">Va donc ! Et me voici truffard pour un semesse</l>
					<l n="6" num="1.6">Et c’est Pipo qu’il faut quoiqu’au fond je m’en fesse</l>
					<l n="7" num="1.7">Éclater la sous-ventrière ! Merde à chien !</l>
					<l n="8" num="1.8">Ingénieur à l’étranger ça fait très bien,</l>
					<l n="9" num="1.9">Mais la braise ! Faut-il que tout ce temps se perde ?</l>
					<l n="10" num="1.10">Mon pauvre cœur bave à la quoi ! Bave à la merde !</l>
				</lg>
		<closer>
			<dateline>
				<date when="1876">[Hiver 1875-1876.]</date>
			</dateline>
		</closer>
			</div>
			<div type="poem" key="VER218">
				<head type="number">VI</head>
				<head type="main"><hi rend="ital">Dargnières Nouvelles.</hi></head>
				<lg n="1">
					<l n="1" num="1.1">C’est pas injussʼ de s’voir dans un pareillʼ situate !…</l>
					<l n="2" num="1.2">Et pas la queuʼ d’un pauvrʼ keretzer sous la patte !…</l>
					<l n="3" num="1.3">J’arrive à Vienne avec les meyeurs intentions,</l>
					<l n="4" num="1.4">— Sans compter que j’comptʼ sur des brevets d’inventions —</l>
					<l n="5" num="1.5">En arrêvant je m’collʼ quéqʼ Fanta commʼ de jusse,</l>
					<l n="6" num="1.6">Bon ! V’là qu’un cocher d’fiacʼ m’volʼ tout !… C’est pas injusse ?…</l>
					<l n="7" num="1.7">Voui, m’fait tout jusqu’à ma limace et mon grimpant</l>
					<l n="8" num="1.8">Et m’plant’là dans la strass par un froid. Pas foutant ?</l>
					<l n="9" num="1.9">Non ! Vrai, pour le début en v’là-t-y un triomphe :</l>
					<l n="10" num="1.10">Ah ! La salʼ bête ! Encor plus pirʼ que la daromphe.</l>
				</lg>
		<closer>
			<dateline>
				<date when="1876">[1876.]</date>
			</dateline>
		</closer>
			</div>
			<div type="poem" key="VER219">
				<head type="number">VII</head>
				<lg n="1">
					<l n="1" num="1.1">Je renonce à Satan, à ses pomp’, à ses œuffs !</l>
					<l n="2" num="1.2">Je vous gobe, ô profonds mugissements des bœufs,</l>
					<l n="3" num="1.3">J’fonde eunʼ nouvelle école, et sans coll’, j’agricole.</l>
					<l n="4" num="1.4">Coll’-toi ça dans l’fusil, mondʼ frivole, et racole-</l>
					<l n="5" num="1.5">Z-en d’autres. Désormais j’dis merdʼ à les Gatti,</l>
					<l n="6" num="1.6">À les Russ’, à les Vienne et aux Scaferlati</l>
					<l n="7" num="1.7">D’contrebande, et j’vas faire un très chouettʼ mariache.</l>
					<l n="8" num="1.8">J’me cramponne à toi, Roche, et j’défends qu’on marrache</l>
					<l n="9" num="1.9">Eud’toi… Vivʼ le lard dans la soupe — et soillions</l>
					<l n="10" num="1.10">Sérillieux, — et qu’noutʼ sueur alle abreuffʼ nos sillions !</l>
				</lg>
			</div>
			<div type="poem" key="VER220">
				<head type="number">VIII</head>
				<lg n="1">
					<l n="1" num="1.1">N. de D. ! J’ai rien voyagé d’puis mon dergnier</l>
					<l n="2" num="1.2">Coppée ! Il est vrai qu’j’en d’viens chauvʼ comme un pagnier</l>
					<l n="3" num="1.3">Percé, qu’j’ai là quéqu’chosʼ dans l’gosier qui m’ratisse,</l>
					<l n="4" num="1.4">Et que j’sens commʼ les avant-goûts d’un rhumatisse,</l>
					<l n="5" num="1.5">Et que j’m’emmerdʼ plussʼ euqʼ jamais ; mais c’est-n-égal :</l>
					<l n="6" num="1.6">J’ai promené ma gueule infecte au Sénégal,</l>
					<l n="7" num="1.7">Et vu Cinq-Hélènʼ (merde à Badingue !) unʼ rudʼ noce !…</l>
					<l n="8" num="1.8">Mais tout ça c’est pas sérillieux : j’rêve eudʼ négoce,</l>
					<l n="9" num="1.9">À c’t’heure, et, plein d’astuc’, j’baluchonnʼ des vieillʼ plaqʼs</l>
					<l n="10" num="1.10">D’assuranc’, pour revend’, contʼ du rhum, aux Canaqʼs.</l>
				</lg>
		<closer>
			<dateline>
				<date when="1876">[Fin 1876.]</date>
			</dateline>
		</closer>
			</div>
			<div type="poem" key="VER221">
				<head type="number">IX</head>
				<lg n="1">
					<l n="1" num="1.1">Ah merde alors, j’aim’ mieux l’café d’Suèdʼ que la Suède</l>
					<l n="2" num="1.2">Ell’mêmʼ oûsque c’est la mêmʼ chose — un peu plus raide</l>
					<l n="3" num="1.3">Peut-êtʼ qu’en hiver dans c’te francʼ (que j’chie un peu</l>
					<l n="4" num="1.4">Mon n’veu d’ailleurs). Et pis, des conseils, cré vingt nieu,</l>
					<l n="5" num="1.5">Comme s’il en pleuvait dans ce pays de neige !</l>
					<l n="6" num="1.6">Alors quoi ? Jusqu’à nouvel ordʼ j’flâne en Norvège !</l>
					<l n="7" num="1.7">Où ça n’ensuite aller ? Ça m’la coupe à la fin</l>
					<l n="8" num="1.8">Tous ces bâtons merdeux dans les rouʼs d’mon destin…</l>
				</lg>
					<p><hi rend="ital">(Rêveur)</hi></p>
				<lg n="2">
					<l n="9" num="2.1">Si j’rappliquais pour un trimessʼ à Charlepompe</l>
					<l n="10" num="2.2">(À merde) ? Histoire eud faire un peu suer la darompe ?</l>
				</lg>
		<closer>
			<dateline>
				<date when="1877">[1877.]</date>
			</dateline>
		</closer>
			</div>
		</div>
		<div type="poem" key="VER222">
			<head type="main">SUR RIMBAUD</head>
			<lg n="1">
				<l n="1" num="1.1">Malgré moi je reviens, et ma lettʼ s’y résigne,</l>
				<l n="2" num="1.2">À cet homm… ais qui fut si philomathe, hélas !</l>
				<l n="3" num="1.3">Et dont Noël et Chapsal, chez les pions qui s’indigne,</l>
				<l n="4" num="1.4"><space quantity="12" unit="char"/>Parle à feu Vaugelas…</l>
			</lg>
		</div>
		<div type="poem" key="VER223">
			<head type="main">LONDRES</head>
			<opener>
				<epigraph>
					<cit>
						<quote>… un grave Anglais correct, bien mis, beau linge.</quote>
						<bibl>
							<name>[Victor Hugo.]</name>
						</bibl>
					</cit>
				</epigraph>
			</opener>
			<lg n="1">
				<l n="1" num="1.1">Un dimanche d’été, quand le soleil s’en mêle,</l>
				<l n="2" num="1.2">Londres forme un régal offert aux délicats :</l>
				<l n="3" num="1.3">Les arbres forts et ronds sur la verdure frêle,</l>
				<l n="4" num="1.4">Vert tendre, ont l’air bien loin des brumes et des gaz,</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Tant ils semblent plantés en terre paysanne.</l>
				<l n="6" num="2.2">Un soleil clair, léger dans le ciel fin, bleuté</l>
				<l n="7" num="2.3">À peine. On est comme en un bain où se pavane</l>
				<l n="8" num="2.4">Le parfum d’une lente infusion de thé.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Dix heures et demie, heure des longs services</l>
				<l n="10" num="3.2">Divins. Les cloches par milliers chantent dans l’air</l>
				<l n="11" num="3.3">Sonore et volatil sur d’étranges caprices,</l>
				<l n="12" num="3.4">Les psaumes de David s’ébrouent en brouillard clair.</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1">Argentine comme on n’en entend pas en France,</l>
				<l n="14" num="4.2">Pays de sonnerie intense, bronze amer,</l>
				<l n="15" num="4.3">Font un concert très doux de joie et d’espérance,</l>
				<l n="16" num="4.4">Trop doux peut-être, il faut la crainte de l’Enfer.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1">L’après-midi, cloches encor. Des files d’hommes ;</l>
				<l n="18" num="5.2">De femmes et d’enfants bien mis glissent plutôt</l>
				<l n="19" num="5.3">Qu’ils ne marchent, muets, on dirait économes</l>
				<l n="20" num="5.4">De leur voix réservée aux amen de tantôt.</l>
			</lg>
			<lg n="6">
				<l n="21" num="6.1">Tout ce monde est plaisant dans sa raide attitude</l>
				<l n="22" num="6.2">Gardant, bien qu’erroné, le geste de la foi</l>
				<l n="23" num="6.3">Et son protestantisme à la fois veule et rude</l>
				<l n="24" num="6.4">Met quelqu’un tout de même au-dessus de la loi.</l>
			</lg>
			<lg n="7">
				<l n="25" num="7.1">Espoir du vrai chrétien, riche vivier de Pierre,</l>
				<l n="26" num="7.2">Poisson prêt au pêcheur qui peut compter dessus,</l>
				<l n="27" num="7.3">Sait-Esprit, Dieu puissant, versez-leur la lumière</l>
				<l n="28" num="7.4">Pour qu’ils apprennent à comprendre enfin Jésus.</l>
			</lg>
			<lg n="8">
				<l n="29" num="8.1">Six heures. Les buveurs regagnent leur buvette,</l>
				<l n="30" num="8.2">La famille son home et la rue est à Dieu :</l>
				<l n="31" num="8.3">Et dans le ciel sali quelque étoile seulette</l>
				<l n="32" num="8.4">Pronostique la pluie aux gueux sans feu ni lieu.</l>
			</lg>
		<closer>
			<dateline>
				<date when="1876">[1876 ?]</date>
			</dateline>
		</closer>
		</div>
		<div type="drama" key="VER224">
			<head type="main">LA TENTATION DE SAINT ANTOINE</head>
			<div type="body">
				<div type="scene" n="1">
					<head type="number">SCÈNE I</head>
					<stage type="cast">
						La Thébaïde. La pente d’un colline. Un ermitage. <lb/>(Fausse sécurité.) <lb/>
						<name>ANTOINE</name><hi rend="ital">seul</hi>
					</stage>
					<sp n="1">
						<speaker>ANTOINE</speaker><stage><hi rend="ital">arpentant la scène :</hi></stage>
						<l n="1">Je ne suis plus tenté ! Je ne le serai pas !</l>
						<stage><hi rend="ital">(Il s’agenouille.)</hi></stage>
						<stage><hi rend="ital">(La prière du Pharisien.)</hi></stage>
						<l n="2">À vous gloire, Seigneur, qui m’avez pas à pas</l>
						<l n="3">Amené sur le haut de la colline sainte</l>
						<l n="4">Où le juste s’assure et vous aime sans crainte,</l>
						<l n="5">À vous gloire ! Et pitié pour mes frères encor</l>
						<l n="6"><space quantity="12" unit="char"/>Au pied noir du Thabor !</l>
						<stage><hi rend="ital">(Il se relève et se souvient présomptueusement.)</hi></stage>
						<l n="7"><space quantity="8" unit="char"/>Naguère, que c’était atroce !</l>
						<l n="8"><space quantity="8" unit="char"/>L’enfer avait pouvoir sur moi</l>
						<l n="9"><space quantity="8" unit="char"/>Belzébuth nain, Satan colosse,</l>
						<l n="10"><space quantity="8" unit="char"/>Le Bélial, serpent et roi,</l>
						<l n="11"><space quantity="8" unit="char"/>La petite Vénus féroce,</l>
						<l n="12"><space quantity="8" unit="char"/>Chémos-Péor, tigre et lion,</l>
						<l n="13"><space quantity="8" unit="char"/>Et ce lion, Appolyon,</l>
						<l n="14"><space quantity="8" unit="char"/>Tous les sons et toutes les formes,</l>
						<l n="15"><space quantity="8" unit="char"/>Toutes les bêtes de limons</l>
						<l n="16"><space quantity="8" unit="char"/>Et toutes les ombres des monts,</l>
						<l n="17"><space quantity="8" unit="char"/>Toute l’eau morte aux froids énormes,</l>
						<l n="18"><space quantity="8" unit="char"/>Le tournoi de tout le torrent</l>
						<l n="19">Et tout le feu des cieux, des soufres et des pierres,</l>
						<l n="20">Tout me tombait dessus, de partout, déchirant,</l>
						<l n="21">Lacérant, torturant, perforant, écœurant</l>
						<l n="22">Mon cœur, mes os, mon sang, mes pieds et mes paupières,</l>
						<l n="23">Faisant une bouillie avec mes chairs entières</l>
						<l n="24">Et broyant tout de moi, tout, — hormis mes prières !</l>
						<stage><hi rend="ital">(Il s’agenouille de nouveau.)</hi></stage>
						<stage><hi rend="ital">(Suite et fin de la prière du Pharisien.)</hi></stage>
						<l n="25">L’honneur à vous, Seigneur, qui m’avez préservé,</l>
						<l n="26">Pour l’exemple des saints en vos mains élevé</l>
						<l n="27">Comme un autre serpent d’airain contre la peste</l>
						<l n="28">De l’hérésie affreuse et de la chair funeste !</l>
						<l n="29">Mais, encore un coup, dieu bon, illuminez-les,</l>
						<l n="30"><space quantity="12" unit="char"/>Mes frères aveuglés !</l>
						<stage><hi rend="ital">Bruit militaire. Antoine se redresse et assiste au défilé du fantôme de l’armée des anti-hérésiarques qui va chantant :</hi></stage>
					</sp>
				</div>
				<div type="scene" n="2">
					<head type="number"><hi rend="ital">SCÈNE II</hi></head>
					<stage type="cast">
						<name>ANTOINE</name>,
						<name>L’ARMÉE FANTÔME</name>
					</stage>
					<sp n="2">
						<speaker/>
						<l n="31"><space quantity="12" unit="char"/>Christ est notre polémarque,</l>
						<l n="32"><space quantity="12" unit="char"/>Vive, vive l’empereur !</l>
					</sp>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</div>
			</div>
			<div type="back">
				<closer>
					<dateline>
						<date when="1878">[1878.]</date>
					</dateline>
				</closer>
			</div>
		</div>
	</body>
</text>
</TEI>