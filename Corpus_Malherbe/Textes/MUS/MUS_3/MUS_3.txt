Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
MUS
MUS_3

Alfred de MUSSET
1810-1857

POÉSIES COMPLÉMENTAIRES
1828-1855
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Poésies Posthumes.
Alfred De Musset


http://www.poesies.net/demussetpoesiespostumes.txt
L’édition électronique ne mentionne pas l’édition imprimée d’origine.
_________________________________________________________________
Édition de référence pour les corrections métriques :

Poésies complètes
Alfred De Musset

Bibliothèque de la Pléiade, Gallimard,Paris
1951


┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



Un Rêve
BALLADE

La corde nue et maigre,
Grelottant sous le froid
Beffroi,
Criait d’une voix aigre
Qu’on oublie au couvent
L’Avent.


Moines autour d’un cierge,
Le front sur le pavé
Lavé,
Par décence, à la Vierge
Tenaient leurs gros péchés
Cachés ;


Et moi, dans mon alcôve,
Je ne songeais à rien
De bien ;
La lune ronde et chauve
M’observait avec soin
De loin ;


Et ma pensée agile,
S’en allant par degré,
Au gré
De mon cerveau fragile,
Autour de mon chevet
Rêvait.


— Ma marquise au pied leste !
Qui ses yeux noirs verra,
Dira
Qu’un ange, ombre céleste,
Des chœurs de Jéhova
S’en va !


Quand la harpe plaintive
Meurt en airs languissants,
Je sens,
De ma marquise vive,
Le lointain souvenir
Venir !


Marquise, une merveille,
C’est de te voir valser,
Passer,
Courir comme une abeille
Qui va cherchant les pleurs
Des fleurs !


Ô souris-moi, marquise !
Car je vais, à te voir,
Savoir
Si l’amour t’a conquise,
Au signal que me doit
Ton doigt.


Dieu ! si ton œil complice
S’était de mon côté
Jeté !
S’il tombait au calice
Une goutte de miel
Du ciel !


Viens, faisons une histoire
De ce triste roman
Qui ment !
Laisse, en tes bras d’ivoire,
Mon âme te chérir,
Mourir !


Et que, l’aube venue,
Troublant notre sommeil
Vermeil,
Sur ton épaule nue
Se trouve encor demain
Ma main !


Et ma pensée agile,
S’en allant par degré
Au gré
De mon cerveau fragile,
Autour de mon chevet
Rêvait !


— Vois-tu, vois-tu, mon ange,
Ce nain qui sur mon pied
S’assied !
Sa bouche (oh ! c’est étrange !)
A chaque mot qu’il dit
Grandit.


Vois-tu ces scarabées
Qui tournent en croissant,
Froissant
Leurs ailes recourbées
Aux ailes d’or des longs
Frelons ?


— Non, rien ; non, c’est une ombre
Qui de mon fol esprit
Se rit,
C’est le feuillage sombre,
Sur le coin du mur blanc
Tremblant.


— Vois-tu ce moine triste,
Là, tout près de mon lit,
Qui lit ?
Il dit : « Dieu vous assiste ! »
A quelque condamné
Damné !


— Moi, trois fois sur la roue
M’a, le bourreau masqué,
Marqué,
Et j’eus l’os de la joue
Par un coup mal visé
Brisé.


— Non, non, ce sont les nonnes
Se parlant au matin
Latin ;
Priez pour moi, mignonnes,
Qui mon rêve trouvais
Mauvais.


— Reviens, oh ! qui t’empêche,
Toi, que le soir, longtemps,
J’attends !
Oh ! ta tête se sèche,
Ton col s’allonge, étroit
Et froid !


Otez-moi de ma couche
Ce cadavre qui sent
Le sang !
Otez-moi cette bouche
Et ce baiser de mort,
Qui mord !


− Mes amis, j’ai la fièvre,
Et minuit, dans les noirs
Manoirs,
Bêlant comme une chèvre,
Chasse les hiboux roux
Des trous.



A une Muse
ou
UNE VALSEUSE DANS LE CÉNACLE ROMANTIQUE
STANCES

Quand Madame W…aldor à P…aul F…oucher s’accroche
Montrant le tartre de ses dents,
Et dans la valse en feu, comme l’huître à la roche,
S’incruste à ses muscles ardents ;


Quand, de ses longs cheveux flagellant sa pommette,
De son épine osseuse elle crispe les nœuds,
Coudoyant les valseurs, ainsi qu’une comète
Heurte les astres dans les cieux ;


Quand, d’un sourire affreux glaçant la contredanse,
Suspendue au collet du hanneton crépu,
Comme un squelette à la potence
Elle agite son corps pointu ;


Quand la molle sueur qui de son sein ruisselle
Comme l’huile d’un vieux quinquet,
Sur ses pieds avachis tombant de son aisselle
Fait des dessins sur le parquet ;


Et quand, brisée enfin par la valse rapide,
Nonchalante et fermant les yeux,
Elle laisse flotter sa mamelle livide,
Et darde un regard fauve au Werther pustuleux,


Alors, le ciel pâlit, la chouette siffle et crie,
Les morts dans leurs tombeaux se retournent d’horreur,
La lune disparaît, la rivière charrie,
Et Drouineau devient rêveur.



La Loi sur la Presse

I
Je ne fais pas grand cas des hommes politiques ;
Je ne suis pas l’amant de nos places publiques,
On n’y fait que brailler et tourner à tous vents.
Ce n’est pas moi qui cherche, aux vitres des boutiques,
Ces placards éhontés, débaucheurs de passants,
Qui tuaient la pudeur dans les yeux des enfants.


II
Que les hommes entre eux soient égaux sur la terre,
Je n’ai jamais compris que cela pût se faire,
Et je ne suis pas né de sang républicain ;
Je n’ai jamais été, Dieu merci, pamphlétaire :
Je ne suis pas de ceux qui font mentir leur faim,
Et dans tous les égouts vont s’enfournant du pain.


III
Pour être d’un parti j’aime trop la paresse,
Et dans aucun haras je ne suis étalon.
Ma Muse, vierge encor, n’a rien d’écrit au front.
Je n’ai servi que Dieu, ma mère et ma maîtresse,
Et par quelque sentier qu’ait passé ma jeunesse,
Aucun gravier fangeux ne lui traîne au talon.


IV
J’ai fléchi le genou sur la dalle sanglante,
Chaude et tremblante encor d’un meurtre surhumain,
Quand de joie et d’horreur la France palpitante
Vit un père et ses fils, se tenant par la main,
A travers les éclairs d’une muraille ardente,
Passer en souriant, conduits par le Destin.


V
J’ai prié, j’ai pleuré, moi, fils d’un siècle impie,
Le jour qu’à Notre-Dame, aux pieds du Dieu sauveur,
Une reine, une mère, ô fatale grandeur !
Vint, la tête baissée, et par les pleurs maigrie,
Prier pour ses enfants l’ange de la patrie,
Et rendre grâce à Dieu, pâle encor de terreur.


VI
Que la liberté sainte engendre la licence,
C’est un mal, je le sais ; et de tous les fléaux
Le pire est qu’un bandit soit bâtard d’un héros.
C’est un ardent soleil que celui de la France,
Son immense clarté projette une ombre immense :
Dieu voulut qu’un grand bien fît toujours de grands maux.


VII
Oui, c’est la vérité, le théâtre et la presse
Étalent aujourd’hui des spectacles hideux,
Et c’est en pleine rue à se boucher les yeux.
Un vil mépris de tout nous travaille sans cesse ;
La muse, de nos temps, ne se fait plus prêtresse,
Mais bacchante ; et le monde a dégradé ses dieux.


VIII
Oui, c’est la vérité qu’à peine émancipée,
L’intelligence humaine, hier esclave encor,
A pris à tire-d’aile un monstrueux essor.
Nos hommes ont souillé leur plus vaillante épée,
La parole, cette arme au sein de Dieu trempée,
Dont notre siècle au flanc porte la lame d’or.


IX
Oui, c’est la vérité, la France déraisonne ;
Elle donne aux badauds, comme à Lacédémone,
Le spectacle effrayant d’un esclave enivré.
C’est que nous avons bu d’un vin pur et sacré,
Et, joyeux vigneron qu’un pampre vert couronne,
Nous vendangeons encor d’un pas mal assuré.


X
Mais morbleu ! c’est un sourd ou c’est une statue,
Celui qui ne dit rien de la loi qu’on nous fait !
Messieurs les députés ne visent qu’à l’effet.
Eh ! pour l’amour de Dieu, si votre âme est émue,
Soyez donc trivial, comme on l’est dans la rue,
La Bruyère l’a dit ; celui-là s’y connaît.


XI
Une loi sur la presse ! ô peuple gobe-mouche !
La loi, pas vrai ? quel mot ! comme il emplit la bouche !
Une loi maternelle et qui vous tend les bras !
Une loi, notez bien, qui ne réprime pas,
Qui supprime ! Une loi, comme Sainte-n’y-touche,
Une petite loi qui marche à petits pas !


XII
Une charmante loi, pleine de convenance,
Qui couvre tous les seins que l’on ne saurait voir !
Vous pouvez tout écrire en toute confiance ;
Votre intention seule est ce qu’on veut savoir.
Rien que l’intention ! Voyez quelle indulgence !
La loi flaire un écrit ; s’il sent mauvais, bonsoir !


XIII
Avez-vous insulté par quelque raillerie
Les hauts représentants de la société ?
Médîtes-vous d’un pair, ou bien d’un député ?
L’offense la plus grave a droit de seigneurie ;
Les pairs vous jugeront, s’il plaît à la pairie ;
Sinon, c’est le pays, refait et recompté.


XIV
Avez-vous comparé dans quelque théorie
L’état de république avec la royauté ?
Avez-vous fait un rêve, et dit à la patrie
Ce que pour elle un jour vous auriez souhaité ?
Les pairs vous jugeront, s’il plaît à la pairie ;
Sinon, c’est le pays, refait et recompté.


XV
Avez-vous quelque place, ou bien quelque industrie,
Dont les jours de juillet vous aient déshérité
D’un vieux maître banni serviteur regretté,
Osez-vous à l’exil faire une flatterie ?
Les pairs vous jugeront, s’il plaît à la pairie ;
Sinon, c’est le pays, refait et recompté.


XVI
N’auriez-vous pas construit, pour quelque espièglerie,
Au fond d’une campagne ou d’une métairie,
Un théâtre forain sur deux tréteaux planté ?
Les pairs vous jugeront s’il plaît à la pairie,
Sinon, c’est le pays, refait et recompté ;
Et vous verrez le bât dont vous serez bâté !


XVII
Mais monsieur le ministre a dit à la tribune
Que l’art était perdu, que le goût s’en allait ;
Que la loi, pour la scène, était ce qu’il fallait ;
Qu’autrefois l’éloquence était chose commune,
Mais qu’en France, aujourd’hui, l’on n’en voyait aucune,
Et la chose, à l’ouïr, parut claire en effet.


XVIII
Je voudrais bien savoir, pour la rendre plus claire,
Ce que c’est que ce goût dont on nous parle tant.
Le goût ! toujours le goût ! — Lorsque j’étais enfant,
J’avais un précepteur qui m’en disait autant.
Je vois bien trois mille ans depuis la mort d’Homère ;
Mais depuis trois mille ans je ne vois sur la terre


XIX
Qu’un seul siècle de goût qu’on appelle le grand.
C’est celui de Boileau, c’est celui de Corneille.
Mais enfin, monsieur Thiers, cette terre est bien vieille ;
Que ce siècle soit beau, soit grand, c’est à merveille,
Et je n’en dirai pas de mal assurément ;
Quand le diable y serait, ce n’en est qu’un, pourtant.


XX
Est-ce une loi pour tous qu’un siècle dans l’histoire ?
Parce que trois pédants m’ont farci la mémoire
De je ne sais quels vers, à contre-cœur appris,
N’est-il pour moi qu’un siècle, et pour moi qu’un pays ?
Eh ! s’il est glorieux, qu’il dorme dans sa gloire,
Ce siècle de malheur ! c’est du mien que je suis.


XXI
Dans quel temps vivons-nous, voyons, je vous en prie ?
Vivons-nous sous Louis quatorzième du nom ?
Alors portons perruque, allons à Trianon,
Soyons des fleurs d’amour et de galanterie ;
Enfin, décidez-vous, monsieur Thiers, ou sinon,
Laissez-nous être au monde et vivre notre vie.


XXII
Serait-ce par hasard que ce goût si vanté
Passerait à vos yeux pour quelque vieil usage ?
Ne le croiriez-vous pas de la Grèce apporté ?
Cela pourrait bien être, et vous pensez, je gage,
Que ce goût merveilleux, dont vous faites tapage,
Vient de la vénérable et sainte antiquité.


XXIII
L’an de la quatre-vingt-cinquième olympiade
(C’était, vous le savez, le temps d’Alcibiade,
Celui de Périclès, et celui de Platon),
Certain vieillard vivait, vieillard assez maussade…
Mais vous le connaissez, et vous savez son nom :
C’était Aristophane, ennemi de Cléon.


XXIV
Lisez-le, monsieur Thiers, c’est un rude génie ;
Il avait peu de grâce, et de goût nullement.
On le voyait le soir, devant l’Académie,
Poser sa large main sur sa tempe blanchie,
A l’ombre du smilax et du peuplier blanc.
Le siècle qui l’a vu s’en est appelé grand.


XXV
Quand son regard perçant fixait la face humaine,
Pour fouiller la pensée, il allait droit au cœur ;
Mais il n’en montrait rien qu’un sourire moqueur,
Jusqu’au jour où lui-même à la face d’Athène,
Tout barbouillé de lie, il montait sur la scène,
Attaquait un archonte, et revenait vainqueur.


XXVI
Il nommait par leur nom les choses et les hommes.
Ni le bien, ni le mal, pour lui n’était voilé ;
Ses vers, au peuple même au théâtre assemblé,
De dures vérités n’étaient point économes,
Et s’il avait vécu dans le temps où nous sommes,
A propos de la loi peut-être eût-il parlé.


XXVII
« Étourdis habitants de la vieille Lutèce,
Dirait-il, qu’avez-vous, et quelle étrange ivresse
Vous fait dormir debout ? Faut-il prendre un bâton ?
Si vous êtes vivants, à quoi pensez-vous donc ?
Pendant que vous donnez, on bâillonne la presse,
Et la chambre en travail enfante une prison. »


XXVIII
« On bannissait jadis, au temps de barbarie ;
Si l’exil était pire ou mieux que l’échafaud,
Je ne sais ; mais, du moins, sur les mers de la vie
On laissait l’exilé devenir matelot.
Cela semblait assez de perdre sa patrie.
Maintenant avec l’homme on bannit le cachot


XXIX
« Dieu juste ! nos prisons s’en vont en colonie !
Je ne m’étonne pas qu’on civilise Alger.
Les pauvres musulmans ne savaient qu’égorger ;
Mais nous, notre océan porte à Philadelphie
Une rare merveille, une plante inouïe,
Que nous ferons germer sur le sol étranger.


XXX
« Regardez, regardez, peuples du nouveau monde !
N’apercevez-vous rien sur votre mer profonde ?
Ne vient-il pas à vous, du bout de l’horizon,
Un cétacée énorme, au triple pavillon ?
Vous ne devinez pas ce qui se meut sur Tonde,
C’est la première fois qu’on lance une prison.


XXXI
« Enfants de l’Amérique, accourez au rivage !
Venez voir débarquer, superbe et pavoisé,
Un supplice nouveau par la mer baptisé.
Vos monstres quelquefois nous arrivent en cage ;
Venez, c’est votre tour, et que l’homme sauvage
Fixe ses yeux ardents sur l’homme apprivoisé.


XXXII
« Voyez-vous ces forçats que de cette machine
On tire deux à deux pour les descendre à bord ?
Les voyez-vous fiévreux et le fouet sur l’échine,
Glisser sur leur boulet dans les sables du port ?
Suivez-les, suivez-les, le monde est en ruine ;
Car le génie humain a fait pis que la mort.


XXXIII
« Qu’ont-ils fait, direz-vous, pour un pareil supplice ?
Ont-ils tué leurs rois, ou renversé leurs dieux ?
Non. Ils ont comparé deux esclaves entre eux ;
Ils ont dit que Solon comprenait la justice
Autrement qu’à Paris les préfets de police,
Et qu’autrefois en Grèce il fut un peuple heureux.


XXXIV
« Pauvres gens ! c’est leur crime ; ils aiment leur pensée,
Tous ces pâles rêveurs au langage inconstant.
On ne fera d’eux tous qu’un cadavre vivant.
Passez, Américains, passez, tête baissée ;
Et que la liberté, leur triste fiancée,
Chez vous, du moins, au front les baise en arrivant.»



Août 1835.




Sur une Morte

Elle était belle, si la Nuit
Qui dort dans la sombre chapelle
Où Michel-Ange a fait son lit,
Immobile, peut être belle.


Elle était bonne, s’il suffit
Qu’en passant la main s’ouvre et donne,
Sans que Dieu n’ait rien vu, rien dit,
Si l’or sans pitié fait l’aumône.


Elle pensait, si le vain bruit
D’une voix douce et cadencée,
Comme le ruisseau qui gémit,
Peut faire croire à la pensée.


Elle priait, si deux beaux yeux,
Tantôt s’attachant à la terre,
Tantôt se levant vers les cieux,
Peuvent s’appeler la Prière.


Elle aurait souri, si la fleur
Qui ne s’est point épanouie
Pouvait s’ouvrir à la fraîcheur
Du vent qui passe et qui l’oublie.


Elle aurait pleuré, si sa main
Sur son cœur froidement posée
Eût jamais, dans l’argile humain,
Senti la céleste rosée.


Elle aurait aimé, si l’orgueil,
Pareil à la lampe inutile
Qu’on allume près d’un cercueil,
N’eût veillé sur son cœur stérile.


Elle est morte, et n’a point vécu.
Elle faisait semblant de vivre.
De ses mains est tombé le livre
Dans lequel elle n’a rien lu.



Dans la Prison de la Garde nationale
VERS ÉCRITS AU-DESSOUS 
D’UNE TÊTE DE FEMME DESSINÉE SUR LE MUR

Qui que tu sois, je t’en conjure,
Mets ton lit de l’autre côté.
Ne traîne pas ta couverture
Sur le sein déjà maltraité
De cette douce créature.
Un crayon plein d’habileté
Créa son aimable figure,
Qui respire la volupté.
Elle est belle, laisse-la pure.



1843.




Vers inscrits dans la cellule n°14
DE LA MAISON D’ARRÊT 
DE LA GARDE NATIONALE

Dans cette petite chapelle
L’ennui ne vient qu’aux ennuyeux.
Pense un instant et pars joyeux,
Ta maîtresse en sera plus belle.



A Mademoiselle Anaïs
RONDEAU

Que rien ne puisse en liberté
Passer sous le sacré portique
Sans être quelque peu heurté
Par les bornes de la critique,
C’est un axiome authentique.


Pourquoi tant de sévérité ?
Grétry disait avec gaîté :
« J’aime mieux un peu de musique
Que rien. »


A ma Louison ce mot s’applique.
Sur le théâtre elle a jeté
Son petit bouquet poétique.
Pourvu que vous l’ayez porté,
Le reste est moins, en vérité,
Que rien.



1849.




Cantate de Bettine

Nina, ton sourire,
Ta voix qui soupire,
Tes yeux qui font dire
Qu’on croit au bonheur,


Ces belles années,
Ces douces journées,
Ces roses fanées,
Mortes sur ton cœur…


Nina, ma charmante,
Pendant la tourmente,
La mer écumante
Grondait à nos yeux ;


Riante et fertile,
La plage tranquille
Nous montrait l’asile
Qu’appelaient nos vœux !


Aimable Italie,
Sagesse ou folie,
Jamais, jamais ne t’oublie
Qui t’a vue un jour !


Toujours plus chérie,
Ta rive fleurie
Toujours sera la patrie
Que cherche l’amour.



Complainte de Minuccio

Va dire, Amour, ce qui cause ma peine,
A mon seigneur, que je m’en vais mourir,
Et, par pitié, venant me secourir,
Qu’il m’eût rendu la Mort moins inhumaine.


A deux genoux je demande merci.
Par grâce, Amour, va-t’en vers sa demeure.
Dis-lui comment je prie et pleure ici,
Tant et si bien qu’il faudra que je meure
Tout enflammée, et ne sachant point l’heure
Où finira mon adoré souci.
La Mort m’attend, et s’il ne me relève
De ce tombeau prêt à me recevoir,
J’y vais dormir, emportant mon doux rêve ;
Hélas ! Amour, fais-lui mon mal savoir.


Depuis le jour où, le voyant vainqueur,
D’être amoureuse, Amour, tu m’as forcée,
Fût-ce un instant, je n’ai pas eu le cœur
De lui montrer ma craintive pensée,
Dont je me sens à tel point oppressée,
Mourant ainsi, que la Mort me fait peur.
Qui sait pourtant, sur mon pâle visage,
Si ma douleur lui déplairait à voir ?
De l’avouer je n’ai pas le courage.
Hélas ! Amour, fais-lui mon mal savoir.


Puis donc, Amour, que tu n’as pas voulu
A ma tristesse accorder cette joie
Que dans mon cœur mon doux seigneur ait lu,
Ni vu les pleurs où mon chagrin se noie,
Dis-lui du moins, et tâche qu’il le croie,
Que je vivrais, si je ne l’avais vu.
Dis-lui qu’un jour, une Sicilienne
Le vit combattre et faire son devoir.
Dans son pays, dis-lui qu’il s’en souvienne,
Et que j’en meurs, faisant mon mal savoir.



Au bas d’un portrait 
de Mlle Augustine Brohan

J’ai vu ton sourire et tes larmes,
J’ai vu ton cœur triste et joyeux :
Qui des deux a le plus de charmes ?
Dis-moi ce que j’aime le mieux :
Les perles de ta bouche ou celles de tes yeux ?



Le Chant des Amis

De ta source pure et limpide
Réveille-toi, fleuve argenté ;
Porte trois mots, coursier rapide :
Amour, patrie et liberté !


Quelle voile, au vent déployée,
Trace dans l’onde un vert sillon ?
Qui t’a jusqu’à nous envoyée ?
Quel est ton nom, ton pavillon ?


— J’ai porté la céleste flamme
En tous lieux où Dieu l’a permis.
Mon pavillon, c’est l’oriflamme ;
Mon nom, c’est celui des amis.


Fils des Saxons, fils de la France,
Vous souvient-il du sang versé ?
Près du soleil de l’Espérance
Voyez-vous l’ombre du passé ? »


Le Rhin n’est plus une frontière ;
Amis, c’est notre grand chemin,
Et, maintenant, l’Europe entière
Sur les deux bords se tend la main.


De ta source pure et limpide
Retrempe-toi, fleuve argenté ;
Redis toujours, coursier rapide !
Amour, patrie et liberté.



