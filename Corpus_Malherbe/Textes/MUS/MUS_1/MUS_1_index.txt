MUS
———
MUS_1

Alfred de MUSSET
1810-1857

════════════════════════════════════════════
PREMIÈRES POÉSIES

1829-1835

7710 vers

─ poème	MUS1	Au lecteur
─ poème	MUS2	Don Paez
─ pièce	MUS3	Les marrons du feu
─ poème	MUS4	Portia

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ CHANSONS À METTRE EN MUSIQUE - ET FRAGMENTS
	─ poème	MUS5	L’andalouse
	─ poème	MUS6	Le lever
	─ poème	MUS7	Madrid
	─ poème	MUS8	Madame La Marquise
	─ poème	MUS9	"Quand je t’aimais, pour toi j’aurais donné ma vie,"
	─ poème	MUS10	Au Yung-Frau
	─ poème	MUS11	A Ulric G.
	─ poème	MUS12	Venise
	─ poème	MUS13	Stances
	─ poème	MUS14	Sonnet
	─ poème	MUS15	Ballade à la lune
─ poème	MUS16	Mardoche
─ poème	MUS17	Suzon
─ poème	MUS18	Les vœux stériles
─ poème	MUS19	Octave
─ poème	MUS20	Les secrètes pensées de Rafael
─ poème	MUS21	Chanson
─ poème	MUS22	À Pépa
─ poème	MUS23	À Juana
─ poème	MUS24	À Julie
─ poème	MUS25	À Laure
─ poème	MUS26	À mon ami Édouard B.
─ poème	MUS27	À mon ami Alfred T.
─ poème	MUS28	À Madame N. Menessier
─ poème	MUS29	À madame M***
─ poème	MUS30	Le Saule
─ poème	MUS31	Au lecteur
─ pièce	MUS32	La coupe et les lèvres
─ pièce	MUS33	À quoi rêvent les jeunes filles
─ poème	MUS34	Namouna
