Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
RIV
RIV_2

André RIVOIRE
1872-1930

BERTHE AUX GRANDS PIEDS
1899
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Berthe aux grands pieds
André Rivoire


https://fr.wikisource.org/wiki/Berthe_aux_grands_pieds


ŒUVRES DE ANDRÉ RIVOIRE
André Rivoire

Paris
ALPHONSE LEMERRE, ÉDITEUR
1899




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



PROLOGUE

Reines au corps mignon, dames du temps jadis
Dont l’âme est envolée en de bleus paradis,
Capricieuse et vague ainsi qu’une fumée,
Mais dont toute légende est un peu parfumée,
Spectres inoubliés, vous qui venez le soir,
Invisibles, pourtant présentes, vous asseoir
Près des rêveurs et des poètes sans maîtresses,
Et répandre sur eux l’or de vos longues tresses,
Et les aimer dans l’ombre, et leur chanter tout bas
Les si vieilles chansons qu’ils n’inventeraient pas,
Il nous plaît d’évoquer, fragile, en un poème,
Berthe aux grands pieds, fleur de Hongrie ou de Bohême,
Qui sut rester chaste et fidèle avec douceur,
Qui fut presque une sainte, et qui fut votre sœur ;
Il nous plaît pour charmer une heure passagère
D’avoir là, sous nos yeux, sa présence légère,
De plaindre sans amour, sans couronne et sans pain,
Celle qui fut promise au lit du roi Pépin,
De suivre pas à pas ses fortunes diverses,
De respirer, printemps noyé sous les averses,
Le parfum de son âme éclose un soir de mai,
Qui s’ouvre enfin, comme un beau lis longtemps fermé ;
Il nous plaît de montrer dans l’ombre qui s’éclaire,
Portraicturés en noir sur fond crépusculaire,
Les chevaliers portant le casque et le haubert,
Pépin, la serve Alix et son cousin Tybert,
Et la vieille aux yeux secs qui trama l’imposture,
Et Dieu même qui, pour sauver sa créature,
Au cœur des trois sergents soudoyés pour sa mort
Fit sourdre, à temps, la providence d’un remord,
Et la forêt du Mans sans ciel et sans limite,
Et le bon paysan après le bon ermite,
Et là-bas, seuls et vieux sous leur nom tout en fleur,
Le roi Flores avec la reine Blanchefleur.



I

LA TOUR DE STRIGON

Une plaine. Du soir. Du silence. Une tour :
Au pied, un lac tranquille, et des bois alentour.
C’est l’heure des langueurs et des monotonies.
La nuit masse déjà, plus sombre au bord des cieux,
L’accroupissement noir des monts silencieux.
Berthe avec Blanchefleur rêvent, les mains unies.


Et Berthe dit : « Ma mère, on croirait que le soir
Est triste d’une absence et vide d’un espoir ;
En vain, mon œil s’attache aux plis de la colline ;
Personne ne viendra, personne n’est venu ;
Ma jeunesse est stérile et mon cœur méconnu ;
Je vous aime, et pourtant je me sens orpheline.


« J’espère encore, et me sens triste d’espérer ;
Je voudrais être seule et je voudrais pleurer :
Quelque chose en mon cœur se lamente et s’étonne.
Qu’ai-je fait de ce jour qui penche à son déclin ?
J’ai brodé de la toile et j’ai filé du lin.
Mon printemps est pensif et las, comme un automne.


« Pourquoi ne suis-je pas fille de pauvres gens ?
Légère sous le poids des labeurs diligents,
L’hiver au coin de l’âtre où pétillent les bûches,
Et l’été dans la plaine où flambent les midis,
Ma beauté serait fraîche et mes gestes hardis ;
J’aurais vécu des jours sans rêve et sans embûches.


« Au lieu de tout cela, je suis seule et j’attends.
L’appel du vent s’attriste en mes voiles flottants ;
L’ombre aux plis de ma robe éteint mes splendeurs vaines,
Et le monde, et l’amour, tout est si loin de nous,
Quand je m’accoude au bord des soirs calmes et doux,
Le cœur vide et le front couronné de verveines !


« Voilà trop de longs mois qu’aux pays inconnus
Les messagers partis ne sont pas revenus.
Faudra-t-il donc vieillir monotone et jalouse ?
Faudra-t-il donc ainsi traîner au long des jours
L’angoisse et le regret qu’il m’ignore, toujours,
Le fiancé lointain dont ma vie est l’épouse ?


« Jeune encore, je sens en mon âme, déjà,
Descendre un peu de la vieillesse qui neigea
Mystérieuse et lente aux cheveux de mon père.
Mon cœur va se fermer, d’avoir trop attendu… »
Et Blanchefleur, penchant la tête, a répondu,
Sans y croire et les yeux pleins de larmes : « Espère ! »




II

LE BON ROI PÉPIN

Or, dans les temps que Berthe espérait en Hongrie,
Autour de la Saint-Jean, quand la rose est fleurie,
Et que la mousse abonde aux flancs verts du coteau,
Le roi Pépin le Bref, fils de Charles Marteau,
Un soir qu’il était seul assis devant sa porte,
Songeait, bien tristement, que sa femme était morte.
Il fit mander à lui ses comtes, ses barons,
Qui vinrent casqués d’or, étoiles d’éperons,
Et leur dit : « Donne-moi ta main, que je la serre !…
Ensuite, j’ai besoin d’un avis très sincère.
Je suis presque très jeune encore, et suis très seul.
Je me couche, le soir, comme un mort au linceul,
Et mon alcôve auguste est vide de caresses ;
Mon baiser tout-puissant s’écarte des maîtresses :
Car, si j’avais un fils, j’entends qu’il fût de moi,
Ou du moins tel selon l’Église et par la loi.
Voyez, réfléchissez et décidez ; que faire ?
— Sire, Berthe aux grands pieds serait bien votre affaire,
Dit quelqu’un (or, c’était Enguerrand de Montcler) :
Elle est fille de roi comme il convient, c’est clair !
— Berthe aux grands pieds, dit l’autre en frisant ses moustaches,
Ce nom nous garantit de solides attaches.
Mais c’est perdre du temps que de parler pour rien.
Qu’on demande sa main à son père. C’est bien.
Je fais duc le premier de vous qui me l’amène.
J’attendrai, s’il le faut, la fin de la semaine,
Mais pas plus. Par les monts, par les bois, par les vaux,
Courez, volez, crevez chacun quatre chevaux.
Sachez-le tous avant de vous mettre en campagne,
J’aurai d’elle un enfant qui sera Charlemagne.
Nous serons gouvernés, messieurs, soyez contents.
Dispersez-vous. J’ai dit. Je suis veuf, et j’attends. »




III

LA CHEVAUCHÉE DES BARONS

Sur des chevaux noirs, sur des chevaux blancs,
Les hardis barons se sont mis en selle.
De tout son désir Pépin les harcèle ;
Barons et coursiers ont l’écume aux flancs.


Épris d’horizons et goulus d’espace,
Ils vont, écrasant, dans les champs herbeux,
Troupeaux de moutons et troupeaux de bœufs,
La moisson qui pousse et l’enfant qui passe.


Ils vont devant eux, sans trop savoir où ;
Leur galop d’enfer luit dans la ténèbre ;
Un qui tombe pousse un grand cri funèbre,
Sa bête ayant mis les pieds dans un trou.


Et toujours, au loin, le reste s’élance
Des crêtes des monts aux crêtes des monts,
Criant et hurlant comme des démons,
Hâtant leurs chevaux à grands coups de lance.


Oh ! les râteliers d’où pendait le foin !
Épuisés, meurtris, des coups qu’ils leur flanquent,
Les pauvres bidets s’usent et s’efflanquent…
Pourtant la Hongrie est encor bien loin.




IV

L’ARRIVÉE

Cependant ont passé les nuits après les jours,
Et Berthe à son balcon, pensive, attend toujours,
Les lèvres et les mains vaines, mais sans reproches.
Or voici que les temps, et les barons, sont proches,
Et la vierge bientôt ne doit plus ignorer
Que Pépin se languit de toujours espérer.
Ils viennent. Les voilà, fourbus et hors d’haleine ;
Leurs ombres devant eux s’allongent sur la plaine,
Et leur cuirasse luit dans le soleil couchant.
Le galop des chevaux se hâte en trébuchant.
Mais là-bas, comme Elsa fit plus tard vers le cygne,
Berthe aux grands pieds leur tend les bras et leur fait signe,
Puis, se ressouvenant qu’elle est fille de roi,
Elle prend un air doux, mélancolique et froid.




V

LA DEMANDE

Les barons ont fait la demande.
Tout s’est passé selon leurs vœux.
Le père a dit : « Ma joie est grande. »
Et Berthe : « Si l’on veut, je veux. »


Elle rougit, suivant l’usage.
Et tous les barons d’admirer,
Au nom de Pépin, ce visage
Si clair qu’on s’y pouvait mirer.


Alors, le roi Flores propose :
« Messieurs, vous devez avoir faim.
Venez donc prendre quelque chose
De réconfortant et de fin.


« Vous avez la joue et l’œil caves,
Et vos chevaux n’en veulent plus.
Videz mes celliers et mes caves,
Soyez friands, soyez goulus.


« Tout est conclu, rien ne vous presse.
Restez un mois, restez-en deux.
Reposez-vous avec ivresse
Des nuits et des jours hasardeux. »


Mais eux que Pépin seul anime,
Plus forts que la tentation,
Déclinent d’un geste unanime
Cette aimable invitation :


« Notre prince est en peine, sire,
Laissez-nous partir à l’instant.
On se consume comme cire,
Quand on ignore et qu’on attend.


— J’aime, dit le roi, votre zèle ;
Mais je ne puis, à mon regret,
Vous octroyer la demoiselle,
Sans son trousseau qui n’est pas prêt.


« Si vous avez trop souciance
Du roi qui languit de savoir,
Portez à son impatience
L’allégement d’un sûr espoir.


« Volez à lui, prompts comme flèche,
Vous lui direz en arrivant
Que Berthe vous suit en calèche,
Et que son cœur marche devant. »


Il dit. Chacun bondit en selle.
Et c’est ainsi, de huit à neuf,
Que Berthe aux grands pieds, très pucelle,
Fut promise à Pépin, très veuf.




VI

LE DÉPART POUR LA FRANCE

Tout est prêt. Sous les doigts des brodeuses agiles,
Les robes de velours, de soie et de satin,
Belles de leurs couleurs pimpantes et fragiles,
Sont écloses, comme des fleurs dans le matin.


On a mis aux chevaux des guirlandes de roses ;
La cour d’honneur a pris un air d’enchantement.
Des poètes joufflus ont récité des proses
Au nom de la famille et du gouvernement.


Voici l’heure des vrais adieux et des tendresses,
Et du dernier baiser, qu’un autre toujours suit.
Le roi Flores se sent le cœur plein de détresses,
Des pleurs tremblent au bord de ses yeux pleins de nuit.


« Adieu, puisque je suis trop vieux pour vous conduire,
Ma fille, trop d’hivers ont alourdi mes pas ;
Mais si dans mon palais l’âge m’a su réduire,
Mes suprêmes conseils ne vous quitteront pas.


« Aux pauvres, avant tout, ne soyez point amère.
Il sied mal au bonheur d’être orgueil et dédain.
Soyez fleur, blanche fleur, comme était votre mère,
Ouvrez votre âme à tous, ainsi qu’un clair jardin. »


Il dit, et se détourne, et Berthe sans pensée
Sanglote éperdument, sachant qu’il est trop tard.
Tout au fond de son cœur d’heureuse fiancée
S’éplore la tristesse amère du départ.


Déjà, pour son passage, on fait ranger la foule ;
Au galop des chevaux, Berthe va s’éloigner.
Cependant, les yeux lourds de pleurs qu’elle refoule,
La reine Blanchefleur ne peut se résigner.


Elle est là, sans espoir dont son deuil se soulage,
Près de Berthe, impuissante à lui quitter la main.
« Je t’accompagnerai jusqu’au prochain village,
Dit-elle, je serai plus vaillante… demain. »




VII

LE DERNIER ADIEU

Et toujours Blanchefleur a suivi son enfant.
De soir en soir, de ville en ville, elle diffère
L’inévitable adieu qu’il faudra bientôt faire.
Le cœur lui sanglote et lui fend.


Voici que les chemins vont quitter la Hongrie.
Maintenant, c’est la Saxe, avec ses rochers nus,
L’effroi de traverser des pays inconnus,
Sans qu’un visage vous sourie.


La reine Blanchefleur se sent plus triste encor,
Songeant qu’il va falloir chevaucher en arrière.
Le cortège a fait halte au bord d’une clairière
Dans un silencieux décor.


Et toutes deux, la mère et la fille, s’étreignent ;
Elles voudraient sourire et leurs yeux sont en pleurs ;
Des nids d’oiseaux heureux chantent parmi les fleurs ;
Tout bas, sans savoir, elles craignent.


« Donnez-moi votre anneau, ma fille, en souvenir
Des jours où votre main m’était proche et câline,
Et pour garder de vous, pauvre mère orpheline,
Un don que je puisse bénir.


« Et maintenant, puisqu’il le faut, Dieu vous conserve !
Alix vous accompagne où l’amour vous conduit.
Aimez-la. Qu’elle soit pour vous, dès aujourd’hui,
Votre sœur et non votre serve.


« Je l’ai voulu choisir, vierge de tout soupçon,
Parce que vous l’aimez et qu’elle vous ressemble
Au point qu’on vous prendrait, quand vous êtes ensemble,
Pour deux fleurs du même buisson.


« Sa présence sera douce à votre souffrance.
Elle, du moins, elle est heureuse et rien ne perd :
Car sa mère Margiste et son cousin Tybert
Vous suivent avec elle en France ;


« Ils veilleront sur vous, ma fille, tous les trois… »
Elle dit, et l’embrasse, et le navrant cortège
S’éloigne ; et Blanchefleur, pour que Dieu les protège,
Fait dans l’ombre un signe de croix.




VIII

L’ENTRÉE À PARIS

Paris ! Des fleurs et des drapeaux !
La ville entière est pavoisée,
Les balcons et l’humble croisée,
Les monuments et les chapeaux.


Des gens sont grimpés aux toitures.
Risquant de se rompre le cou,
Pour voir, — et d’ailleurs pas beaucoup, —
Passer au galop trois voitures.


Car chacun sait que, ce jour-là,
— Le matin ou dans la soirée —
Berthe va faire son entrée
Dans son carrosse de gala.


On hurle, on s’étouffe, on s’égorge…
Pépin, dès la pointe du jour,
A dû rencontrer son amour
Près de Villeneuve-Saint-George.


On se renseigne éperdument :
« On dit qu’elle est jeune et gentille…
— Monsieur, vous écrasez ma fille…
— Où donc est passé mon amant ?… »


On se bouscule, on se querelle.
Soudain, rumeur. C’est Berthe enfin,
Mignonne comme un séraphin
Et saluant de son ombrelle.


Elle passe au milieu des cris
Dont on accueille les carosses.
Tous ces gens lui semblent féroces.
Pépin prend des airs attendris.


Elle a passé. Grave et sereine,
La foule au hasard se répand,
Et tous — bourgeois ou sacripant —
Sont très fiers d’avoir vu la reine.




IX

LE COMPLOT

Dans tout ce qui précède on n’a point vu de traître.
Patience ! un instant : les nôtres vont paraître,
Dans cette chambre obscure et propre aux noirs desseins.
Tous les trois ont quitté leurs airs de petits saints ;
Ils trament un complot : Margiste en sera l’âme,
Alix en est la fleur, Tybert en est la lame…
Ah ! pauvre Blanchefleur, si tu pouvais les voir.
Sinistres, parlant bas dans l’ombre, trio noir,
Sans scrupule ourdissant leurs toiles d’araignée
Dont ta Berthe est déjà la mouche désignée,
Comme tu maudirais d’un cœur épouvanté
Ta crédulité borgne et ta naïveté !




X

LE REPAS DE NOCE

Je ne vous dirai rien de la cérémonie
Par qui la pauvre Berthe à Pépin fut unie ;
Je ne vous dirai pas
Les gestes onctueux de l’évêque aux doigts souples,
Le discours de l’adjoint, le nom des plus beaux couples,
Le menu du repas.


Vous saurez seulement qu’on a fait chère lie.
La voisine au dessert est toujours plus jolie
De tout ce qu’on a bu.
Jeunes gens et vieillards, chacun fut bon convive,
Et voici qu’une flamme indiscrète s’avive
Aux yeux du plus fourbu.


C’est l’heure où la plus froide a le cœur charitable.
Les genoux, amoureux des pauvres pieds de table
Qui n’y comprennent rien,
Se rapprochent ; les mains se cherchent et consentent,
Et les plus bedonnants sur leur chaise se sentent
Le corps aérien.


Berthe aux grands pieds, selon l’antique Protocole
Qui sévissait, branlant, comme un maître d’école,
Son front déjà caduc,
— Berthe aux grands pieds, craintive et tendre, a dû se mettre
En face de Pépin, son époux et son maître,
Près d’un vieil archiduc.


Elle trouve que c’est bien loin, et puis qu’en somme
Tout cet amas de gens dont la gaîté l’assomme
N’est pas d’un grand régal.
Et voici qu’à présent c’est le tour de l’orchestre.
Pauvres chers épousés ! leur grandeur les séquestre
Loin du lit conjugal.


Mais tandis qu’aux doux sons des harpes et des flûtes
Les ménestrels chantaient les amoureuses luttes
Des chevaliers d’antan,
Pépin cligne de l’œil vers Berthe très émue,
Et la vierge comprend que cet œil qui remue
Ça veut dire : « Viens-t’en ! »




XI

LES CONSEILS DE MARGISTE

Berthe s’est retirée en ses appartements.
Elle est là, toute seule, assez mal rassurée,
Toute pleine de trouble et de frissonnements,
Tournant au moindre bruit dont elle est effleurée
Son profil gracieux vers la porte d’entrée
En des gestes d’effroi pudiques et charmants.


On entre. C’est Margiste. Elle approche, discrète,
À pas mystérieux qu’on n’entend point venir.
Elle dit : « Trois abbés sont en train de bénir
Le lit tendre et moelleux que l’amour vous apprête.
Quittez-moi ce beau voile et cette gorgerette…
Madame votre mère a dû vous prévenir.


« Je songe en vous voyant comme je fus heureuse,
Tel soir, aux bras subtils de feu mon cher époux.
Il était fort, il était tendre, il était doux.
Quand j’y songe, un grand vide en mon âme se creuse.
Mais sachez… En trois mots, prenez bien garde à vous :
Le roi Pépin le Bref a la main douloureuse.


« Le soin que j’ai de vous m’a su faire informer,
Discrètement, de la façon qu’il a d’aimer.
Je tiens tous ces détails de son premier concierge ;
Vous voyez, j’en suis pâle encore comme un cierge…
C’est un homme effrayant, surtout pour une vierge,
Un mari dangereux qu’il faudrait réformer. »


Berthe s’écrie en pleurs : « Retournons chez ma mère !
— À quoi bon ? dit Margiste, hé ! vous n’y pensez pas,
On saurait nous rejoindre au bout de quatre pas.
Croyez-moi, chère enfant, toute fuite est chimère ;
Mais, dût ma pauvre Alix y trouver le trépas !
J’entends que cette nuit ne vous soit point amère.


« Le ciel même a voulu, de la tête aux talons,
Qu’elle vous ressemblât de visage et d’allure,
— Hormis les pieds qu’elle a peut-être un peu moins longs ; —
Elle a mêmes yeux bleus et mêmes cheveux blonds.
Moi-même, je m’y trompe, et, si j’osais conclure,
Vous êtes le manteau dont elle est la doublure.


« S’il vous plaisait, madame, elle pourrait, ce soir,
D’un cœur triste et soumis tenter pour vous l’épreuve.
C’est elle qu’en son lit Pépin ferait asseoir.
Aux choses de l’amour, comme vous, elle est neuve,
Et, quand elle devrait, sans anneau, rester veuve,
En tel péril, c’est tout gagner qu’un peu surseoir. »


Elle dit, et déjà Berthe la suit, navrée.
Même aux soupçons du mal son cœur naïf est clos,
Car toujours l’innocence ignore les complots…
Mais Alix et Pépin vont faire leur entrée,
Et, comme la pudeur nous fut toujours sacrée,
Nous allons supprimer trois ou quatre tableaux.




XII

LE RÉVEIL DE PÉPIN

Brisé d’amour, près d’Alix un peu lasse,
Le roi Pépin rêve et dort lourdement.
Déjà le ciel pâlit ; c’est le moment :
Berthe aux grands pieds va réclamer sa place.


Trop tard, hélas ! Le destin s’accomplit.
Alix dira : « Que veut cette importune ? »
Et Berthe enfin n’aura d’autre fortune
Que d’insister, pleurante, au pied du lit.


La pauvre enfant n’y sera point admise.
Même Pépin, s’il s’éveille en sursaut,
Pour n’avoir pas tout à fait l’air d’un sot,
Doit croire Alix, puisqu’elle est en chemise.


Et cependant que la serve au cœur vil
Songe aux détails que sa fraude comporte,
La douce Berthe ouvre déjà la porte,
Sans se douter qu’elle court grand péril…




XIII

LA FORÊT DU MANS

Un coin de bois perdu dans la forêt du Mans.
Des arbustes épars alentour d’un vieux chêne,
Si touffu que la nuit semble toujours prochaine :
La chouette l’emplit de ses hululements.


C’est là que, sans scrupule, ayant juré sa perte,
Les poches tintant clair de l’or qu’ils ont reçu,
Valets du noir complot que Margiste a conçu,
Trois sergents — rengagés — traînent la pauvre Berthe.


Dieu même en sa faveur ne s’est point déclaré,
Pour d’autres criminels réservant son tonnerre,
Et voici qu’à présent Berthe la débonnaire
Va périr sans absoute et sans miséréré.


Déjà les trois sergents ont tiré leur épée :
Berthe attend d’un cœur ferme et d’un corps anxieux,
Et, pour ne pas se voir mourir, ferme les yeux…
— Mourir, oh ! n’être plus qu’une tête coupée !


S’en aller d’ici-bas, sans avoir eu sa part !…
Faudrait-il croire, enfin, ce qu’ont dit les sceptiques,
Que le vrai Dieu n’est pas le bon Dieu des cantiques,
Et qu’il est trop partout pour être quelque part !


Mais, dans l’instant précis que Berthe désespère,
Sur un nuage d’or au céleste reflet,
— Et ce nuage, c’est sa barbe s’il vous plaît ! —
Vint à passer par là, brave homme, Dieu le père.


Son clair regard qui met en fuite les péchés
Se pose doucement sur ces hommes de crimes,
Et tous trois, au moment de frapper leur victime,
Par un trouble inconnu se sentent empêchés.


« À quoi bon nous charger d’un meurtre ? Pourquoi faire ?
Dit l’un d’eux. Reprenons, frères, notre chemin.
Nous soutiendrons qu’elle a péri de notre main,
Et les bêtes du bois en feront leur affaire. »


Sitôt dit, sitôt fait. Ils s’éloignent. Leurs pas
Traînent sinistrement sur les feuilles durcies ;
Et bientôt la nuit tombe aux branches obscurcies.
Berthe est seule, et voudrait crier, et n’ose pas.




XIV

À TRAVERS BOIS

Elle a marché toute la nuit dans les ténèbres
Que les loups emplissaient de hurlements funèbres,
Et la ronce méchante a déchiré ses mains,
Et seule, à travers bois, sans lune et sans chemins,
Elle va gémissante, et meurtrie, et brisée,
Dans sa robe fragile et blanche d’épousée.
Son corps fiévreux grelotte, elle a peur, elle a froid,
Et s’arrête, et gémit. Pauvre fille de roi !
Jusqu’à ce jour, hélas ! depuis qu’elle était née,
De parfums, de tiédeurs toujours environnée,
Elle ignorait l’horreur des nuits sous le ciel noir
Et l’angoisse d’errer sans gîte et sans espoir,
À l’heure où, remuant les branches endormies,
Rôdent confusément des bêtes ennemies.
Elle n’a même plus la force de s’enfuir,
Et son corps sans courage est près de la trahir.
Épuisée et dolente, au bord d’une fontaine,
Elle pleure, elle songe à sa mère lointaine,
La reine Blanchefleur qui venait doucement,
Penchée à son chevet, baiser son front dormant.
Elle songe à Margiste et tout bas lui pardonne :
Elle a recommandé son âme à la Madone
Et fait de longs adieux à tout ce qu’elle aimait.
Maintenant, elle attend la mort et se soumet,
— Quand voici tout à coup, dans l’ombre toute proche,
Tinter pieusement l’appel clair d’une cloche.




XV

LE BON ERMITE

La cloche a guidé Berthe à travers la forêt,
Puis s’est tue ; elle arrive auprès d’une clairière.
Au seuil de sa cabane un ermite en prière,
Des deux mains se frappant la poitrine, adorait.


— Il voit Berthe, et d’abord, craignant un de ces pièges
Que les démons subtils tendent à la vertu,
Redoutable, il s’écrie : « Arrière, que veux-tu ?
Je veille, et c’est en vain, Satan, que tu m’assièges. »


Mais Berthe se rapproche et lui parle humblement :
« Ne me repoussez pas. Je suis lasse et perdue ;
J’ai froid, j’ai faim, j’ai soif ; votre pitié m’est due,
Mon père, et je l’implore au nom de saint Clément. »


L’ermite se rassure, et, songeant que le diable
Ne se présente pas au nom d’un saint, voici,
D’un visage plus calme et d’un ton radouci,
Qu’il ose sans remords se montrer pitoyable.


Il interroge Berthe, et Berthe sans détour
Lui dit son nom royal, son cœur, son espérance,
Son départ de Hongrie et son entrée en France,
L’amour du roi Pépin promis à son amour.


Elle dit le filet de maligne imposture
Dont la serve Margiste a su l’envelopper,
Le remords des sergents au moment de frapper,
Et sa nuit dans les bois, fuyante à l’aventure.


Le bon vieillard l’écoute avec recueillement ;
Et quand elle a fini cette histoire touchante,
Le matin rit au ciel, le bois s’éveille et chante,
Et les fleurs des buissons s’entr’ouvrent doucement.


« Votre temps reviendra d’aimer et d’être aimée,
Dit l’ermite, et, tout bas, dût-il être lointain,
Gardez au fond du cœur l’espoir d’un clair matin,
Vibrant de soleil souple et de brise embaumée.


« Dieu même punira vos lâches assassins :
Attendez les retours que sa bonté vous garde ;
Priez, résignez-vous, sachant qu’il vous regarde,
Et qu’il vous a sauvée, et qu’il a ses desseins.


« D’ici là, je connais une sainte famille ;
Le père est sabotier ; dans cette humble maison,
Vous vivrez doucement, sans peur de trahison ;
Vous serez leur bon ange et vous serez leur fille.


« À votre sort ancien dites un long adieu.
Que nul, excepté moi, ne sache qui vous êtes…
Et maintenant voici des fruits et des noisettes.
Reposez-vous, ma sœur, et rendez grâce à Dieu ! »




XVI

CHEZ LES HUMBLES

Calmes et continus, comme une eau sans brisants,
Les jours ont passé vite, et les mois et les ans.
Berthe au milieu des bois a vécu dédaignée,
D’une vie humble et douce et presque résignée,
Debout dès l’aube, et, tant que chantent les oiseaux,
Toujours poussant l’aiguille ou tournant ses fuseaux.
Le sabotier Simon et sa femme Constance
L’ont jadis accueillie en leur simple existence,
Et tous deux se sont mis bien vite à l’adorer.
L’ermite vient parfois lui dire d’espérer,
Et les gens qui, le soir, passent sans la connaître
L’écoutent un instant chanter à sa fenêtre :


Aux plis frileux
Des coteaux bleus
L’adieu du jour mourant se dissémine.
À petits pas,
Qu’on n’entend pas,
Sous les branches, la nuit chemine.


L’ombre des bois gagne les cieux…
Brodez, mes doigts ! Rêvez, mes yeux !


Des fleurs aux mains,
Par les chemins
Les bûcherons s’en viennent des clairières,
Et du clocher
Va s’épancher
L’appel des cloches en prières.


À l’heure où s’endorment les bois.
Rêvez, mes yeux ! Brodez, mes doigts !


Telle au hasard des mots, sur des airs d’autrefois,
Berthe égrène en brodant ses rêves et sa voix.
Le silence du soir tendrement l’environne,
Et son front qui devait porter une couronne
S’incline un peu, pour suivre aux plis des linges blancs
L’aiguille jamais lasse en ses doigts vigilants.
Elle n’espère plus que Pépin lui sourie,
Mais elle pense à lui, de loin comme en Hongrie,
Sur la tour de Strigon, les soirs qu’elle rêvait
D’un prince en manteau d’or assis à son chevet.
Dix ans n’ont point flétri sa grâce d’exilée,
— Se résigner tout bas, c’est être consolée, —
Et Berthe se résigne, et toute sa douleur,
C’est d’évoquer parfois la reine Blanchefleur,
Sans enfants, et bien seule, et bien lasse, et bien triste,
Et le roi Flores, vieux à douter qu’il existe.
Tous deux ont pu mourir. Au fond de sa forêt,
Si loin d’eux et de tout, Berthe ne le saurait.
Mais elle a cet espoir en son âme ingénue
Que Dieu veille sur elle, et l’aurait prévenue.




XVII

LE MÉNAGE ROYAL

À Paris, même jour, même heure. Un boudoir tendre.
Miroirs et bibelots de toilette. Un divan.
Enfin, tout ce qu’il faut pour s’aimer ou s’attendre.
Un nid d’amant coupable ou de mari fervent.


C’est là que chaque soir, aux genoux de la reine,
Dolent et résigné,
Depuis dix ans, Pépin servilement se traîne,
Et se sent dédaigné.


Car la reine est méchante et sa beauté funeste
À ce charme obsesseur dont nul ne peut guérir.
Pépin tout à la fois l’adore et la déteste
D’un amour douloureux qui s’obstine à souffrir.


Lui qui l’espérait douce, et bonne, et maternelle,
Elle l’a tant déçu
Que parfois, comme en rêve, il songe : « Est-ce bien elle ? »
Et doute à son insu.


Est-ce triste ? être un roi puissant que l’on ménage,
Dont le fils portera la couronne de fer !
Être maître en Europe et serf en son ménage,
Et cuire en son amour comme dans un enfer !


Triste, oh ! triste ! — Et malgré le désir qui l’affame,
Un autre ennui le poinct,
C’est que les deux enfants qu’il a de cette femme
Ne lui ressemblent point.


Tel, il songe. — Et dans l’ombre, âme éprise de lucres,
La reine avec Tybert s’épanche en doux propos :
« Qu’est-ce que tu dirais d’un impôt sur les sucres,
D’une aide sur les blés et d’un droit sur les peaux ? »




XVIII

FLORES ET BLANCHEFLEUR

Toujours le même soir. En Hongrie. Un jardin
En qui l’automne éclôt de l’été qui se fane.
Tout s’apaise et s’endort sous un ciel diaphane.
L’heure est douce et les fleurs s’inclinent sans dédain.


Sur un banc que la pluie et la mousse ont verdi,
Perdus dans l’ombre comme un nid dans l’herbe haute,
Flores et Blanchefleur sont assis côte à côte,
La pensée indolente et le front alourdi.


Ils rêvent doucement à l’absente. Leurs yeux
Tristes évoquent d’elle un geste, une attitude.
Ayant mêmes regrets et même solitude,
Ils parlent d’elle encor, bien que silencieux.


Dix ans qu’elle est partie aux pays étrangers !
Et depuis, sans espoir et presque sans nouvelle,
Ils savent seulement le peu que leur révèle
Le va-et-vient tardif de rares messagers.


Elle, qu’ils ont bercée au creux de leurs genoux,
Elle, jadis si frêle, elle est peut-être grasse !
Ses deux maternités ont pu faner sa grâce,
Cerner d’un bleu meurtri ses yeux calmes et doux.


Tels ils rêvent, le soir, et pleins des jours anciens,
Ils s’évadent tous deux hors du présent sans joie,
Et Blanchefleur soupire : « Il faut que je la voie !
J’ai rêvé l’autre nuit qu’elle me disait : Viens !


« Laissez-moi m’en aller vers elle qui m’attend !
Et quand je reviendrai bientôt, grave et lassée,
Pleine encor du bonheur de l’avoir embrassée,
Nous pourrons tous les deux mourir d’un cœur content. »


Un peu de brise pleure aux branches en émoi,
Et Flores qui, depuis longtemps, toujours refuse,
À voix basse, attendri d’espérance confuse,
Murmure : « Allez, princesse, et parlez-lui de moi ! »




XIX

BLANCHEFLEUR EN FRANCE

La reine Blanchefleur a traversé la France.
Les champs étaient déserts, les bois étaient flétris,
Et les villes, où les enfants jouaient sans cris,
Sombres, avaient un air de deuil et de souffrance.


Tout le long des chemins elle regardait fuir
Une succession de plaines dévastées,
Terres à l’abandon, maisons inhabitées.
Son bonheur maternel n’osait s’épanouir.


Elle avait dans le cœur un besoin de sourire.
Partout sur son passage elle voyait les gens
Détourner leur regard de ses yeux indulgents,
Et des poings se crispaient qui semblaient la maudire.


Mais voici qu’elle est presque au bout de son chemin,
Cette nuit sous la tente est enfin la dernière.
Demain elle tiendra sa fille prisonnière,
Dans ses bras, sur son cœur, sous ses lèvres… Demain !


Et, ne pouvant dormir, Blanchefleur s’est assise
À quelques pas du camp, pour rêver sans témoin…
Pleine de sa tristesse, elle regarde, au loin,
Les étoiles trembler dans la brume indécise.


Aux lisières du bois le silence frémit.
Mais on parle dans l’ombre, et Blanchefleur écoute.
Un bruit confus de gens en marche sur la route
Se rapproche. Une voix d’enfant pleure et gémit.


La reine Blanchefleur les appelle, et demande :
« Où vous en allez-vous si tard, mes pauvres gens ? »
Et l’homme a répondu : « Madame, les sergents
Ont saisi ma maison pour acquitter l’amende.


« Nous allons devant nous sans travail et sans pain.
Comme nous, la moitié de la France mendie.
Avarice de reine est dure maladie :
Cette Berthe a changé notre bon roi Pépin.


« Autrefois, cette plaine était fertile et riche.
Aux étables, le soir, rentraient de longs troupeaux.
Mais on a tant sué d’argent pour les impôts
Qu’on se croise les bras au bord des champs en friche.


« Puis, à la porte, un jour, l’huissier heurte du poing :
— Paie ou pars ! — On s’en va, la tête et les mains vides,
Tandis que les petits enfants, bouches avides,
Pleurent de faim dans l’ombre et ne comprennent point.


Et Blanchefleur songeait : « Seigneur, est-il possible !
Non, non, ce n’est pas vrai. Cet homme souffre et ment.
Ma fille était trop pure et trop tendre. Comment
Serait-elle à ce point devenue insensible ? »


Elle tire sa bourse où de l’or tinte et luit.
Elle arrache à ses doigts ses bagues, et s’écrie :
« Tenez, prenez, je suis Blanchefleur de Hongrie ;
Si ma fille vous a fait tort, pardonnez-lui ! »




XX

LE CHÂTIMENT

Or Blanchefleur est au palais depuis hier.
Alix est très émue, et Tybert n’est pas fier,
Et d’un pli soucieux leur front pâle se creuse,
Songeant qu’il va falloir payer la douloureuse.
N’importe. Ils lutteront jusqu’au bout. Pour l’instant,
Gagner du temps le plus possible est l’important.
Pépin seul tient en bas compagnie à la reine.
Alix a déclaré qu’elle avait la migraine.
Elle garde la chambre, et ceinte de bandeaux
Salutaires, dans la pénombre des rideaux
Qui tamisent le jour trop cru de la fenêtre,
Elle pense : « Après tout, que peut-elle y connaître ?
Je ressemblais à Berthe, on n’y voit pas très clair
Dans cette chambre ; et puis, il faudrait bien du flair
Pour se douter après dix ans de réussite
Qu’au lit du roi Pépin ma place est illicite. »
Mais voici qu’on entend venir un bruit de pas.
La reine Blanchefleur paraît : « N’approchez pas,
Ô ma mère, soupire Alix d’une voix fausse.
Car je me sens déjà comme un mort dans sa fosse.
Mère, n’approchez pas ! Le médecin défend
Qu’on m’embrasse. » Et Blanchefleur dit : « Ma pauvre enfant !
N’ai-je fait un si long voyage, ma chérie,
Par tous les longs chemins qui viennent de Hongrie
Que pour te voir mourir ? — Mère, n’approchez pas,
Dit la serve. Parlez de loin, et parlez bas.
Votre présence me fait mal et me torture. »
Mais Blanchefleur déjà devine l’imposture.
Sa fille, en la voyant, n’aurait pas tel dédain.
Tout son cœur maternel s’illumine soudain.
Prompte comme un joueur qui fait sauter la carte,
D’un bond elle s’élance aux rideaux qu’elle écarte.
Et de tous les côtés, dans leurs habits de cour,
S’empressent les barons. Pépin lui-même accourt.
Blanchefleur soulevant la jupe de dentelle :
« Ce ne sont pas les pieds de ma fille, dit-elle.
Roi, vous avez été trompé. N’en doutez point.
— Je le savais, dit-il, mais non pas à ce point… »
La serve à deux genoux se prosterne et se traîne.
Et Blanchefleur reprend : « Qu’as-tu fait de ta reine ?
Parler te vaudrait mieux que tels gémissements.
Rends-moi ma fille ! — Elle est dans la forêt du Mans,
Dit alors une voix. Pardonnez-nous, messire !
Je fus un de ces trois qu’on chargea de l’occire ;
Nous l’avons seulement perdue en la forêt :
En bien cherchant, peut-être on la retrouverait. »
Pépin s’écrie alors : « Que personne ne sorte !
Arrêtez cette femme ! et, si la reine est morte,
Sachez que je prétends, moi-même, d’un seul coup,
Madame, vous trancher la tête au ras du cou !
Qu’on arrête avec vous Tybert, votre complice :
Il sied qu’il ait aussi part en votre supplice…
Maintenant, mettons-nous en route sans retard.
Vous, sergent, guidez-nous. Et, comme il est très tard,
N’oublions pas, messieurs, de prendre des lanternes,
Pour ne pas choir dans les étangs ou les citernes ! »




XXI

BERTHE ET PÉPIN

Le sergent qui les guide a reconnu la place :
Le vieil arbre, le chemin creux, c’est bien cela.
Un grand besoin d’agir soutient leur force lasse.
Mais, naturellement, Berthe n’était plus là.


Déjà le ciel brunit. Le cœur du roi tressaille,
Et son dernier espoir s’éloigne avec le jour.
Mais voici que, dans l’ombre, écartant la broussaille,
Cette femme qui vient, c’est Berthe, son amour.


Tandis qu’elle brodait, rêveuse, en sa chaumière,
Le cœur plein d’un mystérieux et tendre émoi,
Il lui sembla qu’un ange en robe de lumière
De loin lui faisait signe et lui disait : « Suis-moi ! »


Sans surprise, docilement, elle est venue.
Elle est là maintenant, grave et craintive un peu.
Le roi Pépin la prend par sa blanche main nue,
Et les parfums du soir embaument leur aveu.


« C’est donc vous, cette fois, vous que j’ai tant aimée,
Vous que je pressentais absente obscurément !
Je vous ai. Votre main tremble en ma main fermée…
Ce n’était donc pas vous qui faisiez mon tourment.


« Je regarde et je vois. Quand je maudissais l’autre.
Et que je détestais son âme en son beau corps.
Le corps seul, que j’aimais, était un peu le vôtre,
Et je ne pouvais pas adorer sans remords.


« Je comprends maintenant pour quel espoir tenace
Malgré moi, mon amour ne voulait pas mourir.
L’avenir était sombre en moi. Vaine menace !
Mon cœur ne s’est pas clos, puisqu’il devait s’ouvrir.


« Et toi, que faisais-tu ? Moi ce n’est pas ma faute
Si l’autre avait sa place en mon lit, sous mon dais.
Toujours, assis, couchés, nous vivions côte à côte. »
Et Berthe, en rougissant, lui répond : « J’attendais. »


Blanchefleur à son tour s’approche et l’on s’embrasse.
On pleure et l’on sourit. Tableau simple et charmant :
Mère et fille, femme et mari. Je vous fais grâce,
Et ne décrirai point leur attendrissement.


Vous saurez seulement que, dans le crépuscule,
Dont la pénombre tendre emplit au loin les bois,
Pépin parlait d’amour sans être ridicule,
— Et vraiment c’était bien pour la première fois…

⁂ 

Maintenant le public aime qu’on le renseigne
Sur les héros qu’on laisse en des cas hasardeux.
C’est un vœu trop connu pour que je le dédaigne.
Sachez donc en trois mots tout ce qu’il advint d’eux :
Je ne vous ai pas dit que Margiste était morte,
— C’est un oubli ; — Margiste est morte, — ayant pris froid…
D’Alix et de Tybert, l’histoire nous rapporte
Que Berthe intercéda pour eux auprès du roi.
Blanchefleur retourna très heureuse en Hongrie.
Maître Simon devint sabotier de la cour,
— Emploi que l’on créa tout exprès, je vous prie ; —
Quant à Berthe et Pépin, ils s’aimèrent d’amour…
Vous voyez, tout cela finit par un sourire,
L’histoire est toute simple, et je n’ai qu’un regret,
C’est que pour la conter il ait fallu l’écrire,
Et puis conter en vers, c’est peut-être indiscret.




