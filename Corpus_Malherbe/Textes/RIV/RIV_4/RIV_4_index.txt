RIV
———
RIV_4

André RIVOIRE
1872-1930

════════════════════════════════════════════
LE CHEMIN DE L’OUBLI

1904

1573 vers

─ poème	RIV125	Réclusion
─ poème	RIV126	Matin
─ poème	RIV127	Bonheur tendre
─ poème	RIV128	Possession
─ poème	RIV129	Chambre d’Auberge
─ poème	RIV130	Sourires
─ poème	RIV131	Le Fardeau
─ poème	RIV132	Le Désir veille
─ poème	RIV133	Certitude
─ poème	RIV134	Ennui
─ poème	RIV135	Regards au loin
─ poème	RIV136	Refrain
─ poème	RIV137	Soir tombant
─ poème	RIV138	Visites
─ poème	RIV139	Dernier Départ
─ poème	RIV140	Reliques
─ poème	RIV141	Il était une fois…
─ poème	RIV142	Passé lointain
─ poème	RIV143	La Payse
─ poème	RIV144	La Chanson de l’Amour
─ poème	RIV145	Tristesse
─ poème	RIV146	Malentendu
─ poème	RIV147	Sommeil
─ poème	RIV148	Premiers Baisers
─ poème	RIV149	Déclin
─ poème	RIV150	Désir muet
─ poème	RIV151	Prudence
─ poème	RIV152	Trop tard
─ poème	RIV153	Été
─ poème	RIV154	Délivrance
─ poème	RIV155	Aveu
─ poème	RIV156	Recommencements
─ poème	RIV157	Nocturne
─ poème	RIV158	Fin d’Amour
─ poème	RIV159	Complainte
─ poème	RIV160	Espoir secret
─ poème	RIV161	Baisers
─ poème	RIV162	Rancune
─ poème	RIV163	Le Nom
─ poème	RIV164	Larmes
─ poème	RIV165	Tendresse
─ poème	RIV166	Clairvoyance
─ poème	RIV167	Rajeunissement
─ poème	RIV168	Expérience
─ poème	RIV169	Orgueil triste
─ poème	RIV170	Jour perdu
─ poème	RIV171	Éveil
─ poème	RIV172	Petite Amie
─ poème	RIV173	Là-bas
─ poème	RIV174	Retour
─ poème	RIV175	Intérieur
─ poème	RIV176	Réveil
─ poème	RIV177	La vieille Maison
─ poème	RIV178	Soir d’Automne
─ poème	RIV179	Le Refuge
