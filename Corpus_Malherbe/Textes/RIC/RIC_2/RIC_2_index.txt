RIC
———
RIC_2

Jean RICHEPIN
1849-1926

════════════════════════════════════════════
LES CARESSES

1877

3358 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ FLORÉAL
	─ poème	RIC112	I. DÉCLARATION
	─ poème	RIC113	II. "Le jour où je vous vis pour la première fois,"
	─ poème	RIC114	III. RONDEAU
	─ poème	RIC115	IV. SONNET-MADRIGAL
	─ poème	RIC116	V. SÉRÉNADE
	─ poème	RIC117	VI. "A quoi bon des serments ?"
	─ poème	RIC118	VII. UN CADEAU. — SONNET D’ENVOI
	─ poème	RIC119	VIII. SONNET GREC
	─ poème	RIC120	IX. SONNET ROMAIN
	─ poème	RIC121	X. SONNET MOYEN-AGE
	─ poème	RIC122	XI. SONNET RENAISSANCE
	─ poème	RIC123	XII. SONNET WATTEAU
	─ poème	RIC124	XIII. SONNET ROMANTIQUE
	─ poème	RIC125	XIV. SONNET MODERNE
	─ poème	RIC126	XV. "Ne sois pas jalouse, va !"
	─ poème	RIC127	XVI. AU JARDIN DE MON CŒUR
	─ poème	RIC128	XVII. ÉTOILES FILANTES
	─ poème	RIC129	XVIII. UN MIRACLE
	─ poème	RIC130	XIX. LA NOCE FÉERIQUE
	─ poème	RIC131	XX. "Si tu veux, m’amour, ce soir"
	─ poème	RIC132	XXI. LA CHANSON DES CHANSONS
	─ poème	RIC133	XXII. LE SOLEIL RICHE
	─ poème	RIC134	XXIII. LE SOLEIL PAUVRE
	─ poème	RIC135	XXIV. "Tu me demandes, rieuse"
	─ poème	RIC136	XXV. "C’est le malin. A la fenêtre grande ouverte"
	─ poème	RIC137	XXVI. "Eh ! oui, c’est toi la plus forte !"
	─ poème	RIC138	XXVII. LA VOIX DES CHOSES
	─ poème	RIC139	XXVIII. DANS LES FLEURS
	─ poème	RIC140	XXIX. L’ENSORCELÉ
	─ poème	RIC141	XXX. "Crois-tu que mon cœur amer"
	─ poème	RIC142	XXXI. LE BATEAU ROSE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ THERMIDOR
	─ poème	RIC143	I. LE PENDU JOYEUX
	─ poème	RIC144	II. VIEILLES AMOURETTES
	─ poème	RIC145	III. L’IDÉAL
	─ poème	RIC146	IV. "Puisqu’à mon fauve amour tu voulus te soumettre,"
	─ poème	RIC147	V. REPAS CHAMPÊTRE
	─ poème	RIC148	VI. RONDEAUX MIGNONS
	─ poème	RIC149	VII. "Pourquoi donc t’habiller si matin, ma chérie ?"
	─ poème	RIC150	VIII. LE GALANT JARDINIER
	─ poème	RIC151	IX. "La salive de tes baisers sent la dragée"
	─ poème	RIC152	X. "Comment, mignonne, j’ai fait souffrir votre orgueil ?"
	─ poème	RIC153	XI. "Quand je vous ai mise en colère,"
	─ poème	RIC154	XII. RÉVEIL
	─ poème	RIC155	XIII. "Tu dors ? Ce n’est pas vrai, folle, tu fais semblant."
	─ poème	RIC156	XIV. "Bien avant d’avoir pu contempler à mon gré"
	─ poème	RIC157	XV. "Depuis lors je t’ai tenue"
	─ poème	RIC158	XVI. "Son corps est d’un blanc monotone"
	─ poème	RIC159	XVII. BEAUTÉ MODERNE
	─ poème	RIC160	XVIII. AU THÉÂTRE
	─ poème	RIC161	XIX. UNE FANTAISIE
	─ poème	RIC162	XX. "Tes paroles ont des musiques cristallines."
	─ poème	RIC163	XXI. "Mes désirs ne sont point lassés."
	─ poème	RIC164	XXII. "La possession dégoûte !"
	─ poème	RIC165	XXIII. "Encore et toujours, te dis-je !"
	─ poème	RIC166	XXIV. LE TRÉSOR
	─ poème	RIC167	XXV. LE GOINFRE D’AMOUR
	─ poème	RIC168	XXVI. "Sous tes lèvres de miel quand tu fermes mes yeux,"
	─ poème	RIC169	XXVII. INSATIABLEMENT
	─ poème	RIC170	XXVIII. UN PEU DE REPOS
	─ poème	RIC171	XXIX. LENDEMAIN DE FÊTE
	─ poème	RIC172	XXX. "O maîtresse, ta bouche exécrable et charmante"
	─ poème	RIC173	XXXI. ESCLAVAGE
	─ poème	RIC174	XXXII. ABDICATION
	─ poème	RIC175	XXXIII. "Dis-moi n’importe quoi ! porte-moi n’importe où"
	─ poème	RIC176	XXXIV. A CORPS PERDU
	─ poème	RIC177	XXXV. L’AMOUR MALSAIN

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ BRUMAIRE
	─ poème	RIC178	I. SONNET D’AUTOMNE
	─ poème	RIC179	II. SES YEUX
	─ poème	RIC180	III. "Ne sois donc pas méchante, ô ma petite fille !"
	─ poème	RIC181	IV. LA FORGE
	─ poème	RIC182	V. "Ses cheveux formant sa coiffure lumineuse,"
	─ poème	RIC183	VI. FAÇON DE MADRIGAL
	─ poème	RIC184	VII. LES DEUX LITS
	─ poème	RIC185	VIII. LE DERNIER CADEAU
	─ poème	RIC186	IX. JOURNÉE FAITE
	─ poème	RIC187	X. BILLET DE FAIRE PART
	─ poème	RIC188	XI. L’HERBE SANS NOM
	─ poème	RIC189	XII. "Quand je suis loin, je suis cependant près de toi,"
	─ poème	RIC190	XIII. AIR RETROUVÉ
	─ poème	RIC191	XIV. REGAINS
	─ poème	RIC192	XV. LE VIOLON
	─ poème	RIC193	XVI. RÉVOLTE
	─ poème	RIC194	XVII. LES POISONS INUTILES
	─ poème	RIC195	XVIII. "Sur mon beau jasmin d’Espagne"
	─ poème	RIC196	XIX. "Et pourtant la marguerite"
	─ poème	RIC197	XX. JALOUSIE
	─ poème	RIC198	XXI. LE CARNET
	─ poème	RIC199	XXII. LE BOUQUET
	─ poème	RIC200	XXIII. "Sous son joug las de ployer,"
	─ poème	RIC201	XXIV. NUIT D’ADIEU
	─ poème	RIC202	XXV. INDIFFÉRENCE
	─ poème	RIC203	XXVI. INSOMNIE
	─ poème	RIC204	XXVII. LES SORCIÈRES
	─ poème	RIC205	XXVIII. SOURIRE POLI
	─ poème	RIC206	XXIX. "Mon cœur fut un fruit dans une haie,"
	─ poème	RIC207	XXX. LA MORT DE L’AUTOMNE
	─ poème	RIC208	XXXI. "Le cadavre est lourd"
	─ poème	RIC209	XXXII. "Ah ! c’est en vain que je m’en vais !"
	─ poème	RIC210	XXXIII. LE BATEAU NOIR

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ NIVÔSE
	─ poème	RIC211	I. "Le ciel est transi."
	─ poème	RIC212	II. LE PLAT DE FAÏENCE
	─ poème	RIC213	III. LES SOMNAMBULES
	─ poème	RIC214	IV. PLONGEON
	─ poème	RIC215	V. "Du pic de la cime haute"
	─ poème	RIC216	VI. LE DOMPTEUR
	─ poème	RIC217	VII. "Après tout, est-ce tant ma faute ? Elle savait"
	─ poème	RIC218	VIII. L’ARMADA
	─ poème	RIC219	IX. PEINES PERDUES
	─ poème	RIC220	X. "« Homme aux yeux cruels, prends garde !"
	─ poème	RIC221	XI. "J’ai rencontré le coucou"
	─ poème	RIC222	XII. A MAURICE BOUCHOR
	─ poème	RIC223	XIII. PLAINTES COMIQUES
	─ poème	RIC224	XIV. BALLADE DE BONNE RÉCOMPENSE
	─ poème	RIC225	XV. "Je veux chanter ma folie"
	─ poème	RIC226	XVI. LES CRUCIFIES
	─ poème	RIC227	XVII. L’HÔTE
	─ poème	RIC228	XVIII. "Où vivre ? Dans quelle ombre"
	─ poème	RIC229	XIX. LE MAUDIT
	─ poème	RIC230	XX. L’OUBLI IMPOSSIBLE
	─ poème	RIC231	XXI. L’INCONSOLABLE
	─ poème	RIC232	XXII. SOMBRES PLAISIRS
	─ poème	RIC233	XXIII. AU BORD DE LA MER
	─ poème	RIC234	XXIV. LES NAUFRAGÉS
	─ poème	RIC235	XXV. DEUX LIARDS DE SAGESSE
	─ poème	RIC236	XXVI. VAINES PAROLES
	─ poème	RIC237	XXVII. "Te souviens-tu du baiser,"
	─ poème	RIC238	XXVIII. "Bien souvent je ne pense à rien, comme une bête."
	─ poème	RIC239	XXIX. "Te souviens-tu d’une étoile"
	─ poème	RIC240	XXX. AU COIN DU FEU
	─ poème	RIC241	XXXI. LA BERCEUSE
	─ poème	RIC242	XXXII. NOCTURNE
	─ poème	RIC243	XXXIII. LE BON SOUVENIR
	─ poème	RIC244	XXXIV. PARIS
	─ poème	RIC245	XXXV. PARFUM SUPRÊME
