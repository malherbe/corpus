BCH
———
BCH_1

Maurice BOUCHOR
1855-1929

════════════════════════════════════════════
LES POËMES DE L’AMOUR ET DE LA MER

1876

3472 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I LA FLEUR DES EAUX
	─ poème	BCH1	ENVOI
	─ poème	BCH2	I. "L’air est plein d’une odeur exquise de lilas"
	─ poème	BCH3	II. "Je m’étais enivré d’espace et de ciel bleu ;"
	─ poème	BCH4	III. "Les feuilles dans les bois commencent à roussir,"
	─ poème	BCH5	IV. "Le vent dans les rochers sifflait et mugissait ;"
	─ poème	BCH6	V. TES YEUX
	─ poème	BCH7	VI. "J’ai rencontré mon idéal"
	─ poème	BCH8	VII. ÉLÉGIE
	─ poème	BCH9	VIII. "Je mettrai sur ta bouche entr’ouverte et fleurie"
	─ poème	BCH10	IX. "La mer tranquille et grande, reine"
	─ poème	BCH11	X. "Je t’aime comme la santé,"
	─ poème	BCH12	XI. "La nuit était tranquille et ténébreuse ; à peine"
	─ poème	BCH13	XII. MADRIGAL
	─ poème	BCH14	XIII. "Nous nous aimerons au bord d’un sentier"
	─ poème	BCH15	XIV. "Amour, pensers d’amour, rêves subtils et doux,"
	─ poème	BCH16	XV. "Mignonne, es-tu dévote et fais-tu ta prière ?"
	─ poème	BCH17	XVI. "Ce jour-là, vous songiez. Qui pouvait remplir, chère,"
	─ poème	BCH18	XVII. "O ma chère, qu’il t’en souvienne ! Et qu’il te plaise,"
	─ poème	BCH19	XVIII. "Je meurs d’amour, je suis amoureux comme un chien"
	─ poème	BCH20	XIX. "S’il me fallait mourir le premier soir d’amour"
	─ poème	BCH21	XX. LA NUIT BIENHEUREUSE
	─ poème	BCH22	XXI. "En revenant, je regardais"
	─ poème	BCH23	XXII. LE DUO DES AMOUREUX
	─ poème	BCH24	XXIII. L’AUTRE NUIT
	─ poème	BCH25	XXIV. "Mignonne, il est des hypocrites"
	─ poème	BCH26	XXV. TES CHEVEUX
	─ poème	BCH27	XXVI. "« Vous m’avez appelée, et moi j’ai répondu ;"
	─ poème	BCH28	XXVII. "Malgré tant de chansons sur tes yeux et ta bouche,"
	─ poème	BCH29	XXVIII. CHANSON
	─ poème	BCH30	XXIX. "On entendait encore au loin, dans l’air du soir,"
	─ poème	BCH31	XXX. "Pourquoi regardes-tu toujours l’horizon triste"
	─ poème	BCH32	XXXI. "Mais si cette nature est triste, que t’importe,"
	─ poème	BCH33	XXXII. "M’aimes-tu ? le caprice ou le besoin d’aimer"
	─ poème	BCH34	XXXIII. "J’ai pendant longtemps caressé ce rêve"
	─ poème	BCH35	XXXIV. "Et nous coucher ensemble, immobiles et froids,"
	─ poème	BCH36	XXXV. "N’as-tu pas des frissons parfois ?"
	─ poème	BCH37	XXXVI. "Non, les baisers d’amour n’éveillent point les morts !"
	─ poème	BCH38	XXXVII. "Je regardais la mer où venait se mirer"
	─ poème	BCH39	XXXVIII. "L’automne est passé, l’hiver est venu,"
	─ poème	BCH40	XXXIX. SÉRÉNADE EN HIVER
	─ poème	BCH41	XL. "Après avoir marché sur la route durcie"
	─ poème	BCH42	XLI. "Réveille la vigueur de tes sens épuisés"
	─ poème	BCH43	XLII. AND GOOD NIGHT INDEED
	─ poème	BCH44	XLIII. "C’est une belle nuit glacée,"
	─ poème	BCH45	XLIV. "Le dernier oiseau de l’année"
	─ poème	BCH46	XLV. "Le stupide hasard qui gouverne le monde"
	─ poème	BCH47	XLVI. "Car toi seule es pour moi la jeunesse du monde ;"
	─ poème	BCH48	XLVII. "Quel son lamentable et sauvage"
	─ poème	BCH49	XLVIII. "Le ciel tranquille sur nos têtes"
	─ poème	BCH50	XLIX. "Elle devait partir au point du jour. Mes yeux"
	─ poème	BCH51	L. "Oh ! par le ciel qui fut si tranquille et si bleu,"

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II LA MORT DE L’AMOUR
	─ poème	BCH52	I. "J’étais l’enfant sacré de la grande Nature,"
	─ poème	BCH53	II. "Paris, terrible et grand — aussi grand que la mer,"
	─ poème	BCH54	III. CHANT D’AMOUR
	─ poème	BCH55	IV. "Nos souvenirs, toutes ces choses"
	─ poème	BCH56	V. "C’est novembre. C’est le mois"
	─ poème	BCH57	VI. LE SOUVENIR
	─ poème	BCH58	VII. "Moi, par la neige et par la bise,"
	─ poème	BCH59	VIII. "Mon amour d’antan, vous souvenez-vous ?"
	─ poème	BCH60	IX. "Ses cheveux avaient les parfums étranges"
	─ poème	BCH61	X. "Les souvenirs les plus lointains"
	─ poème	BCH62	XI. "Las ! où sont les neiges d’antan ?"
	─ poème	BCH63	XII. "Me rappelant, l’âme charmée,"
	─ poème	BCH64	XIII. "Tu t’en venais à moi par les longs soirs d’hiver"
	─ poème	BCH65	XIV. "Aimée, aux jours lointains où nous nous reverrons,"
	─ poème	BCH66	XV. "Quand verrons-nous comme autrefois"
	─ poème	BCH67	XVI. "Souviens-toi ! c’était un matin d’automne,"
	─ poème	BCH68	XVII. "Par les larmes que j’ai versées"
	─ poème	BCH69	XVIII. "Tout m’obsède. Le bruit incessant des voitures,"
	─ poème	BCH70	XIX. EN MER
	─ poème	BCH71	XX. "J’ai revu le jardin où nous avions aimé ;"
	─ poème	BCH72	XXI. "Nos sentiers aimés s’en vont refleurir"
	─────────────────────────────────────────────────────
	▫ XXII 
		─ poème	BCH73	I. "Tu reviendras un jour, et nous nous aimerons"
		─ poème	BCH74	II. "Mais sans toi, rien n’est doux. Une plainte étouffée"
		─ poème	BCH75	III. "J’ai vécu nuit et jour près d’elle, et j’ai sondé"
		─ poème	BCH76	IV. "Oh ! sois bénie et sois encor bénie, ô toi"
	─ poème	BCH77	XXIII. "Dans votre solitude, ô bois sombres et doux,"
	─ poème	BCH78	XXIV. "Que le vaisseau léger, que la lune propice,"
	─ poème	BCH79	XXV. PRINTEMPS TRISTE
	─ poème	BCH80	XXVI. "Non, ce n’est pas l’hiver, le printemps ni l’automne"
	─ poème	BCH81	XXVII. "Si quand je te contemple, ô reine de folie,"
	─ poème	BCH82	XXVIII. "Le vent roulait les feuilles mortes ; mes pensées"
	─ poème	BCH83	XXIX. "Bonsoir ! Et pourquoi donc me regarder ainsi ?"
	─ poème	BCH84	XXX. "Est-ce donc qu’il est vrai, dans cette âpre vallée"
	─ poème	BCH85	XXXI. "C’est bien fini, le temps de compter jusqu’à trois,"
	─ poème	BCH86	XXXII. "Il faisait une nuit merveilleusement belle ;"
	─ poème	BCH87	XXXIII. LA SYMPHONIE DES SANGLOTS
	─ poème	BCH88	XXXIV. "Le vent était bien doux, la lune était bien fine,"
	─ poème	BCH89	XXXV. "Il ne nous reste donc, après tant de nuits folles,"
	─ poème	BCH90	XXXVI. "Nous voguions en mer sous les étoiles ;"
	─ poème	BCH91	XXXVII. "Mon âme quelquefois me semble triompher"
	─ poème	BCH92	XXXVIII. "Le temps des lilas et le temps des roses"
	─ poème	BCH93	XXXIX. "C’est le vent qui m’a fait pleurer,"
	─ poème	BCH94	XL. "L’année est morte, ding dong !"
	─ poème	BCH95	XLI. "De quoi pouvions-nous bien parler, un soir de mai,"
	─ poème	BCH96	XLII. "Une nuit orageuse et toute sombre. A peine"
	─ poème	BCH97	XLIII. "A présent, sur la route où je marche éperdu,"
	─ poème	BCH98	XLIV. LA VENGEANCE DES ÉTOILES

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III L’AMOUR DIVIN
	─ poème	BCH99	I. "Qu’ai-je donc ? le printemps me trouble et me soulève."
	─ poème	BCH100	II. "Comme des cavaliers innombrables, les flots"
	─ poème	BCH101	III. "Entre le ciel et l’eau j’ai cheminé longtemps,"
	─ poème	BCH102	IV. LA MER AMOUREUSE
	─ poème	BCH103	V. "L’air était doux. C’était l’heure où le jour décline ;"
	─ poème	BCH104	VI. AUTREFOIS
	─ poème	BCH105	VII. AUJOURD’HUI
	─ poème	BCH106	VIII. MATIN
	─ poème	BCH107	IX. LES FÉERIES DE LA MER
	─ poème	BCH108	X. "O bon soleil, par qui tout se métamorphose,"
	─ poème	BCH109	XI. "Que la brise du ciel est légère et joyeuse,"
	─ poème	BCH110	XII. "L’air m ?enveloppe et me caresse ;"
	─ poème	BCH111	XIII. "Mon cœur saigne en voyant passer les belles filles,"
	─ poème	BCH112	XIV. "Sans but, j’ai devant moi cheminé nuit et jour"
	─ poème	BCH113	XV. "N’est-il pas un remède, et ne guérit-on pas ?"
	─ poème	BCH114	XVI. "Pourquoi tenter d’aimer ? Solitaire et farouche,"
	─ poème	BCH115	XVII. "Une nuit je marchais dans la campagne obscure ;"
	─ poème	BCH116	XVIII. "Tu m’as tendu les bras, ô puissante déesse,"
	─ poème	BCH117	XIX. "Dans les splendeurs orientales"
	─ poème	BCH118	XX. "Éteignant ses pâles étoiles,"
	─ poème	BCH119	XXI. "Allons, la mer est belle et la brise se lève,"
	─ poème	BCH120	XXII. A LORD BYRON
	─ poème	BCH121	XXIII. "Par une nuit d’été délicieuse et triste"
	─ poème	BCH122	XXIV. "L’océan qui roule sa plainte"
	─ poème	BCH123	XXV. L’ART
	─ poème	BCH124	XXVI. "Nous portons les flambeaux qui doivent luire au monde,"
	─ poème	BCH125	XXVII. POUR LE JOUR DES MORTS
	─ poème	BCH126	XXVIII. "Hélas ! il est trop vrai, le temps seul est vainqueur"
	─ poème	BCH127	XXIX. "Les mendiants sans pain qui vont vendant des fleurs"
	─ poème	BCH128	XXX. "Vin de topaze, d’or, de rubis, d’améthyste,"
	─ poème	BCH129	XXXI. "Je suis donc enfermé dans cette étroite chambre."
	─ poème	BCH130	XXXII. "Adieu la mer ! je suis repris"
	─ poème	BCH131	XXXIII. "Oui, pour Dieu. Je ferai palpiter dans les cieux"
	─ poème	BCH132	XXXIV. ÉPILOGUE
