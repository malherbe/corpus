<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">L’ADIEU</title>
				<title type="medium">Une édition électronique</title>
				<author key="MRT">
					<name>
						<forename>Albert</forename>
						<surname>MÉRAT</surname>
					</name>
					<date from="1840" to="1909">1840-1909</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>408 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2015">2015</date>
				<idno type="local">MRT_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">l’Adieu</title>
						<author>Albert Mérat</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/L%E2%80%99Adieu_%28Albert_M%C3%A9rat%29/Texte_entier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>l’Adieu</title>
								<author>Albert Mérat</author>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-28" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="MRT167">
				<head type="number">I</head>
				<lg n="1">
					<l n="1" num="1.1">Oh ! pourquoi partir sans adieux ?</l>
					<l n="2" num="1.2">Pourquoi m’ôter ton doux visage,</l>
					<l n="3" num="1.3">Tes lèvres chères et tes yeux</l>
					<l n="4" num="1.4">Où je n’ai pas lu ce présage ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pourquoi sans un mot de regret ?</l>
					<l n="6" num="2.2">Est-ce que l’heure était venue ?</l>
					<l n="7" num="2.3">Si ton cœur, hélas ! était prêt,</l>
					<l n="8" num="2.4">Je ne t’aurais pas retenue.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Pourquoi t’oublîrais-je ? La main</l>
					<l n="10" num="3.2">De qui me vint cette blessure</l>
					<l n="11" num="3.3">Eut ce cher caprice inhumain,</l>
					<l n="12" num="3.4">Et pour me frapper fut peu sûre</l>
				</lg>
			</div>
			<div type="poem" key="MRT168">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai là, devant moi, son portrait.</l>
					<l n="2" num="1.2">Regardez : la tête est jolie</l>
					<l n="3" num="1.3">Délicatement, sans apprêt…</l>
					<l n="4" num="1.4">Je ne l’avais pas embellie.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ce qu’on vantait, c’étaient ses yeux,</l>
					<l n="6" num="2.2">Beaux parfois d’un éclair farouche.</l>
					<l n="7" num="2.3">Qu’importe ! je n’aimais pas mieux</l>
					<l n="8" num="2.4">Baiser ses chers yeux que sa bouche.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Les ciels charmants me sont fermés.</l>
					<l n="10" num="3.2">Était-elle pire ou meilleure</l>
					<l n="11" num="3.3">Que la femme que vous aimez ?</l>
					<l n="12" num="3.4">Je ne le sais pas, mais je pleure.</l>
				</lg>
			</div>
			<div type="poem" key="MRT169">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1">Ne leur en veuillons pas :</l>
					<l n="2" num="1.2">Nos pauvres amoureuses</l>
					<l n="3" num="1.3">Suivent à petits pas</l>
					<l n="4" num="1.4">Des routes plus heureuses.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Paris ne leur vaut rien :</l>
					<l n="6" num="2.2">On y fait des folies.</l>
					<l n="7" num="2.3">Elles nous aiment bien</l>
					<l n="8" num="2.4">Tant qu’elles sont jolies.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Que faire ? Les laisser</l>
					<l n="10" num="3.2">S’enfuir à tire-d’ailes,</l>
					<l n="11" num="3.3">Et puis ne pas cesser</l>
					<l n="12" num="3.4">De nous souvenir d’elles.</l>
				</lg>
			</div>
			<div type="poem" key="MRT170">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1">Il reste la mélancolie</l>
					<l n="2" num="1.2">Quand le bonheur s’en est allé.</l>
					<l n="3" num="1.3">Alors il faut bien qu’on oublie</l>
					<l n="4" num="1.4">Ou qu’on ne soit pu consolé,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Mais, comme l’oubli serait pire,</l>
					<l n="6" num="2.2">Sans le vouloir on se souvient,</l>
					<l n="7" num="2.3">Et la lèvre essaye un sourire</l>
					<l n="8" num="2.4">Qu’on effort cruel y retient.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Souvenir, oubli, même chose,</l>
					<l n="10" num="3.2">Faite de douceur et de fiel !</l>
					<l n="11" num="3.3">— Porte d’amour ouverte et close,</l>
					<l n="12" num="3.4">Azur et tristesse du ciel !</l>
				</lg>
			</div>
			<div type="poem" key="MRT171">
				<head type="number">V</head>
				<lg n="1">
					<l n="1" num="1.1">Non, tu ne m’as rien emporté!</l>
					<l n="2" num="1.2">C’est encor moi qui te possède;</l>
					<l n="3" num="1.3">J’ai gardé toute ta beauté;</l>
					<l n="4" num="1.4">A nul autre je ne te cède!</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Écoute ! L’homme à qui tes bras</l>
					<l n="6" num="2.2">Ouvrent le ciel de tes caresses,</l>
					<l n="7" num="2.3">Quoi qu’il fasse, ne t’aura pas,</l>
					<l n="8" num="2.4">O la plus belle des maîtresses !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">J’ai mis à l’abri mes trésors</l>
					<l n="10" num="3.2">Comme un avare statuaire;</l>
					<l n="11" num="3.3">Et la merveille de ton corps</l>
					<l n="12" num="3.4">A mon âme pour sanctuaire.</l>
				</lg>
			</div>
			<div type="poem" key="MRT172">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1">C’était le bruit de sa bottine</l>
					<l n="2" num="1.2">A travers ce que je rêvais,</l>
					<l n="3" num="1.3">Ou sa tête penchée et fine</l>
					<l n="4" num="1.4">Près de mon front, quand j’écrivais.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elle fit ce manège d’ange</l>
					<l n="6" num="2.2">Deux beaux étés et deux hivers.</l>
					<l n="7" num="2.3">Je disais : « Cela me dérange,</l>
					<l n="8" num="2.4">« Et je ne ferai pas de vers. »</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Elle remuait tout de même;</l>
					<l n="10" num="3.2">La plume me tombait des doigts.</l>
					<l n="11" num="3.3">— Parfums légers de ce qu’on aime,</l>
					<l n="12" num="3.4">Musique éteinte de sa voix !</l>
				</lg>
			</div>
			<div type="poem" key="MRT173">
				<head type="number">VII</head>
				<lg n="1">
					<l n="1" num="1.1">Un jour nous étions en bateau :</l>
					<l n="2" num="1.2">Elle voulut manger des mûres.</l>
					<l n="3" num="1.3">— Le bord, c’est presque le coteau,</l>
					<l n="4" num="1.4">Avec les bois pleins de murmures.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Vous savez quels soleils charmants</l>
					<l n="6" num="2.2">Tombent à midi sur nos plaines.</l>
					<l n="7" num="2.3">— Penchée en de fins mouvements.</l>
					<l n="8" num="2.4">Toute rouge, les deux mains pleines,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Parmi les feuillages brisés</l>
					<l n="10" num="3.2">Où quelque merle s’effarouche,</l>
					<l n="11" num="3.3">Elle noircit de ses baisers</l>
					<l n="12" num="3.4">Mes paupières et puis ma bouche.</l>
				</lg>
			</div>
			<div type="poem" key="MRT174">
				<head type="number">VIII</head>
				<lg n="1">
					<l n="1" num="1.1">L’eau coulait au bord des prés,</l>
					<l n="2" num="1.2">Loin de nos mélancolies.</l>
					<l n="3" num="1.3">— Les gazons sont diaprés.</l>
					<l n="4" num="1.4">Toutes les fleurs sont jolies.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tu te souviens, les oiseaux</l>
					<l n="6" num="2.2">Chantaient des épithalames,</l>
					<l n="7" num="2.3">Et les tiges des roseaux</l>
					<l n="8" num="2.4">Frissonnaient comme nos âmes.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Nous fûmes de longs instants</l>
					<l n="10" num="3.2">Sans parole et sans sourire…</l>
					<l n="11" num="3.3">Point de baisers ! — Le printemps</l>
					<l n="12" num="3.4">N’eut cette fois rien à dire.</l>
				</lg>
			</div>
			<div type="poem" key="MRT175">
				<head type="number">IX</head>
				<lg n="1">
					<l n="1" num="1.1">Les caresses, ailes de l’âme,</l>
					<l n="2" num="1.2">Par le chemin du souvenir,</l>
					<l n="3" num="1.3">S’en vont, tremblantes, vers la femme</l>
					<l n="4" num="1.4">Que l’on n’a pas su retenir.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">O caresses ! choses légères,</l>
					<l n="6" num="2.2">Au vol fidèle, au rhythme sûr,</l>
					<l n="7" num="2.3">Suivant les chères passagères</l>
					<l n="8" num="2.4">Près de la fange ou dans l’azur;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Il se peut que je la revoie</l>
					<l n="10" num="3.2">Ou que vienne l’oubli vainqueur,</l>
					<l n="11" num="3.3">Mais vers elle je vous envoie,</l>
					<l n="12" num="3.4">Lourdes des chaînes de mon cœur.</l>
				</lg>
			</div>
			<div type="poem" key="MRT176">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1">Je n’aimerai jamais que toi…</l>
					<l n="2" num="1.2">A moins qu’une femme ne m’aime,</l>
					<l n="3" num="1.3">Et ne me donne aussi sa foi</l>
					<l n="4" num="1.4">Pour me la reprendre de même.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Car, vois-tu, nous ne pouvons pas,</l>
					<l n="6" num="2.2">Si forte qu’en soit notre envie,</l>
					<l n="7" num="2.3">Aux liens frêles de vos bras</l>
					<l n="8" num="2.4">Dérober jamais notre vie.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Nous prenons nos amours brisés</l>
					<l n="10" num="3.2">Pour nous en forger d’autres chaînes.</l>
					<l n="11" num="3.3">— O contagion des baisers,</l>
					<l n="12" num="3.4">Défaillances toujours prochaines !</l>
				</lg>
			</div>
			<div type="poem" key="MRT177">
				<head type="number">XI</head>
				<lg n="1">
					<l n="1" num="1.1">Du temps que tu fus tout mon bien,</l>
					<l n="2" num="1.2">O tête bien-aimée et folle,</l>
					<l n="3" num="1.3">Par caprice tu voulais bien</l>
					<l n="4" num="1.4">Voir à mon front une auréole.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dans les tableaux, un nimbe d’or</l>
					<l n="6" num="2.2">Luit sur la tête des apôtres;</l>
					<l n="7" num="2.3">Nous n’avons pas ce beau décor :</l>
					<l n="8" num="2.4">Nous sommes faits comme les autres.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Les sonnets les plus triomphants</l>
					<l n="10" num="3.2">Se font très-simplement en somme.</l>
					<l n="11" num="3.3">Si les femmes sont des enfants,</l>
					<l n="12" num="3.4">Un poëte n’est rien qu’un homme.</l>
				</lg>
			</div>
			<div type="poem" key="MRT178">
				<head type="number">XII</head>
				<lg n="1">
					<l n="1" num="1.1">« Toujours l’extase des baisers !</l>
					<l n="2" num="1.2">« Ne boire que la fleur des choses !</l>
					<l n="3" num="1.3">« Les printemps sont malavisés ;</l>
					<l n="4" num="1.4">« Les roses ont tort d’être roses.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">« Avoir toujours un oiseau bleu</l>
					<l n="6" num="2.2">« Qui vous sautille dans la tête !</l>
					<l n="7" num="2.3">« Il vaut bien mieux nous dire adieu,</l>
					<l n="8" num="2.4">« C’est gentil et c’est très-honnête.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">« Ton cœur n’aura qu’à se fermer;</l>
					<l n="10" num="3.2">« Et puis, vois-tu, l’ai cette envie;</l>
					<l n="11" num="3.3">« Être heureuse, ne pas aimer,</l>
					<l n="12" num="3.4">« N’avoir plus cela dans ma vie ! »</l>
				</lg>
			</div>
			<div type="poem" key="MRT179">
				<head type="number">XIII</head>
				<lg n="1">
					<l n="1" num="1.1">Nous nous rencontrerons</l>
					<l n="2" num="1.2">Quelquefois par la ville,</l>
					<l n="3" num="1.3">Et nous nous salûrons</l>
					<l n="4" num="1.4">D’une façon civile.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Un souvenir tout bas</l>
					<l n="6" num="2.2">Nous parlera peut-être,</l>
					<l n="7" num="2.3">Ou bien nous n’aurons pas</l>
					<l n="8" num="2.4">L’air de nous reconnaître;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Chacun de son côté,</l>
					<l n="10" num="3.2">Sans que l’autre s’étonne…</l>
					<l n="11" num="3.3">— Les fleurs naissent l’été</l>
					<l n="12" num="3.4">Et meurent à l’automne.</l>
				</lg>
			</div>
			<div type="poem" key="MRT180">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai fait ce rêve bien souvent,</l>
					<l n="2" num="1.2">Qui mettait mon cœur en détresse :</l>
					<l n="3" num="1.3">L’amour, soufflant comme le vent,</l>
					<l n="4" num="1.4">Avait emporté ma maîtresse.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Mais au matin quel beau réveil !</l>
					<l n="6" num="2.2">A mes yeux et dans mes oreilles,</l>
					<l n="7" num="2.3">C’étaient ses yeux comme un soleil</l>
					<l n="8" num="2.4">Et des paroles sans pareilles;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Maintenant presque chaque nuit</l>
					<l n="10" num="3.2">Je fais encor ce mauvais rêve :</l>
					<l n="11" num="3.3">C’est le regret qui le conduit</l>
					<l n="12" num="3.4">Et l’amertume qui l’achève.</l>
				</lg>
			</div>
			<div type="poem" key="MRT181">
				<head type="number">XV</head>
				<lg n="1">
					<l n="1" num="1.1">Son désordre était charmant :</l>
					<l n="2" num="1.2">On eût dit beaucoup de fées</l>
					<l n="3" num="1.3">Dans un tourbillonnement</l>
					<l n="4" num="1.4">Légères et décoiffées.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Seule, elle, faisait cela ;</l>
					<l n="6" num="2.2">Je riais de la voir rire.</l>
					<l n="7" num="2.3">— Un jour elle s’envola :</l>
					<l n="8" num="2.4">Puisse l’air bleu la conduire !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Bien souvent j’ai découvert,</l>
					<l n="10" num="3.2">Tout en cherchant autre chose,</l>
					<l n="11" num="3.3">Du fil dans un livre ouvert</l>
					<l n="12" num="3.4">Et, dans mes vers, un nœud rose.</l>
				</lg>
			</div>
			<div type="poem" key="MRT182">
				<head type="number">XVI</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai mêlé ma vie à la tienne,</l>
					<l n="2" num="1.2">Toutes mes nuits et tous mes jours,</l>
					<l n="3" num="1.3">Sans que la crainte me retienne</l>
					<l n="4" num="1.4">D’être enfin seul et sans recours.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Lorsque j’ai voulu la reprendre,</l>
					<l n="6" num="2.2">Je me suis, hélas ! aperçu</l>
					<l n="7" num="2.3">Que, dans ce rêve long et tendre,</l>
					<l n="8" num="2.4">J’ai beaucoup donné, peu reçu.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je gardais de l’heure passée</l>
					<l n="10" num="3.2">Des chaînes blanches à mon cou :</l>
					<l n="11" num="3.3">Mais mon esprit et ma pensée</l>
					<l n="12" num="3.4">Étaient allés je ne sais où.</l>
				</lg>
			</div>
			<div type="poem" key="MRT183">
				<head type="number">XVII</head>
				<lg n="1">
					<l n="1" num="1.1">Les étoiles ne me sont rien,</l>
					<l n="2" num="1.2">Et je ne saurais rien leur dire.</l>
					<l n="3" num="1.3">Un même éclat qui les vaut bien</l>
					<l n="4" num="1.4">Fait ton regard et ton sourire.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ceux qui, niant un bien réel,</l>
					<l n="6" num="2.2">Cherchent les astres sous leurs voiles,</l>
					<l n="7" num="2.3">Se trompent : ce n’est pas au ciel</l>
					<l n="8" num="2.4">Que sont les plus douces étoiles.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">L’éclat des yeux, bien plus certain,</l>
					<l n="10" num="3.2">Est meilleur parce qu’il est nôtre.</l>
					<l n="11" num="3.3">Il se lève soir et matin;</l>
					<l n="12" num="3.4">C’est la nuit seule qui fait l’autre.</l>
				</lg>
			</div>
			<div type="poem" key="MRT184">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1">Oh ! les mauvais dîners charmants</l>
					<l n="2" num="1.2">Couvrant un seul bout de la table,</l>
					<l n="3" num="1.3">Les faciles raffinements,</l>
					<l n="4" num="1.4">Et le bonheur inévitable !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’était trop chaud, citait trop froid,</l>
					<l n="6" num="2.2">Selon le hasard ou ta guise ;</l>
					<l n="7" num="2.3">Ton sourire m’ôtait le droit</l>
					<l n="8" num="2.4">De nier cette chère exquise.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Nous ferons des repas meilleurs</l>
					<l n="10" num="3.2">Certainement un jour ou l’autre,</l>
					<l n="11" num="3.3">Mais toi, tu les feras ailleurs,</l>
					<l n="12" num="3.4">Et ma table n’est plus la nôtre.</l>
				</lg>
			</div>
			<div type="poem" key="MRT185">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1">Les poëtes sont des rois</l>
					<l n="2" num="1.2">En effet très-ridicules.</l>
					<l n="3" num="1.3">Ils ont peut-être des droits</l>
					<l n="4" num="1.4">Sur les vagues crépuscules,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Sur les nuits, sur les soleils,</l>
					<l n="6" num="2.2">Sur les choses ténébreuses,</l>
					<l n="7" num="2.3">Et sur les baisers vermeils</l>
					<l n="8" num="2.4">De leurs belles amoureuses.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ils ne peuvent, tout est là !</l>
					<l n="10" num="3.2">A la femme brune ou blonde,</l>
					<l n="11" num="3.3">Sauf leurs rimes de gala,</l>
					<l n="12" num="3.4">Donner les biens de ce monde.</l>
				</lg>
			</div>
			<div type="poem" key="MRT186">
				<head type="number">XX</head>
				<lg n="1">
					<l n="1" num="1.1">Quand on est heureux, on n’a pas d’histoire.</l>
					<l n="2" num="1.2">On se cache, on s’aime à l’ombre, tout bas;</l>
					<l n="3" num="1.3">Rien de glorieux, pas de fait notoire;</l>
					<l n="4" num="1.4">Le monde oublié ne vous connaît pas.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Si quelqu’un pourtant, avec un sourire.</l>
					<l n="6" num="2.2">Dit, en vous voyant fuir l’éclat du jour :</l>
					<l n="7" num="2.3">«Ce sont des hiboux !» eh bien, laissez dire…</l>
					<l n="8" num="2.4">Ce sont des oiseaux éblouis d’amour.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Quand le baiser fait la parole vaine,</l>
					<l n="10" num="3.2">On s’en va, muets, dans les grands prés verts.</l>
					<l n="11" num="3.3">— Loin de mon bonheur, je fixe ma peine</l>
					<l n="12" num="3.4">Sur l’émail fragile et bleu de mes vers.</l>
				</lg>
			</div>
			<div type="poem" key="MRT187">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1">Comment aurait-elle pu,</l>
					<l n="2" num="1.2">Quand je le pouvais à peine,</l>
					<l n="3" num="1.3">Renouer le fil rompu</l>
					<l n="4" num="1.4">De notre vie incertaine ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Les baisers sont des baisers.</l>
					<l n="6" num="2.2">Les caresses, des caresses.</l>
					<l n="7" num="2.3">Les bonheurs sont malaisés</l>
					<l n="8" num="2.4">Quand on n’a que ces richesses.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le soir même, doux et clair,</l>
					<l n="10" num="3.2">Conspire à donner la fièvre.</l>
					<l n="11" num="3.3">Plein d’étoiles, il a l’air</l>
					<l n="12" num="3.4">D’une vitrine d’orfèvre.</l>
				</lg>
			</div>
			<div type="poem" key="MRT188">
				<head type="number">XXII</head>
				<lg n="1">
					<l n="1" num="1.1">Ce qui m’arrive est affreux :</l>
					<l n="2" num="1.2">Elle est morte, je l’enterre.</l>
					<l n="3" num="1.3">L’adieu fut très-douloureux ;</l>
					<l n="4" num="1.4">Mais je commence à me taire.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">J’ai, comme on jette des fleurs</l>
					<l n="6" num="2.2">Sur les blancs cercueils des mortes,</l>
					<l n="7" num="2.3">Versé sur elle des pleurs</l>
					<l n="8" num="2.4">Et des fleurs de toutes sortes.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je demeure seul, hélas !</l>
					<l n="10" num="3.2">Avec ma mélancolie.</l>
					<l n="11" num="3.3">— Voici venir les lilas</l>
					<l n="12" num="3.4">Dont le parfum dit : oublie.</l>
				</lg>
			</div>
			<div type="poem" key="MRT189">
				<head type="number">XXIII</head>
				<lg n="1">
					<l n="1" num="1.1">Quand les malheureux ont l’été</l>
					<l n="2" num="1.2">Et le soleil pour leur sourire,</l>
					<l n="3" num="1.3">Il semble qu’un peu de gaîté</l>
					<l n="4" num="1.4">Vienne atténuer leur martyre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Mais l’hiver, quand il fait si froid,</l>
					<l n="6" num="2.2">Malgré la force coutumière,</l>
					<l n="7" num="2.3">L’espérance cède et décroît</l>
					<l n="8" num="2.4">Ainsi que la douce lumière.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Avant que le ciel ne soit bleu,</l>
					<l n="10" num="3.2">L’amant triste, la lèvre aride,</l>
					<l n="11" num="3.3">N’a plus même le coin du feu,</l>
					<l n="12" num="3.4">Où la place laissée est vide.</l>
				</lg>
			</div>
			<div type="poem" key="MRT190">
				<head type="number">XXIV</head>
				<lg n="1">
					<l n="1" num="1.1">Si je n’étais pas assez bon,</l>
					<l n="2" num="1.2">Vois-tu, tu devais me le dire.</l>
					<l n="3" num="1.3">J’ai l’habitude du pardon</l>
					<l n="4" num="1.4">Comme toi celle du sourire.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">L’amant a dans son cœur le ciel :</l>
					<l n="6" num="2.2">Mais, s’il y passe des nuées,</l>
					<l n="7" num="2.3">Les heures d’amour éternel</l>
					<l n="8" num="2.4">En sont parfois diminuées.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">J’aurais tâché d’être meilleur,</l>
					<l n="10" num="3.2">Et, sans en rien faire paraître,</l>
					<l n="11" num="3.3">J’aurais prolongé mon bonheur</l>
					<l n="12" num="3.4">Et ton bonheur aussi, peut-être.</l>
				</lg>
			</div>
			<div type="poem" key="MRT191">
				<head type="number">XXV</head>
				<lg n="1">
					<l n="1" num="1.1">Dans la forêt mouillée et verte,</l>
					<l n="2" num="1.2">Comme deux rudes compagnons,</l>
					<l n="3" num="1.3">Nous allions à la découverte</l>
					<l n="4" num="1.4">Cueillir au loin des champignons.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Nous n’y connaissions pas grand’chose;</l>
					<l n="6" num="2.2">Comme des enfants, tous les deux,</l>
					<l n="7" num="2.3">Nous goûtions leur chair blanche et rose;</l>
					<l n="8" num="2.4">C’était peut-être hasardeux.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Mais nous ne nous empoisonnâmes</l>
					<l n="10" num="3.2">Pas même un peu, pas même un jour.</l>
					<l n="11" num="3.3">Nos estomacs valaient nos âmes,</l>
					<l n="12" num="3.4">Et nous avions beaucoup d’amour.</l>
				</lg>
			</div>
			<div type="poem" key="MRT192">
				<head type="number">XXVI</head>
				<lg n="1">
					<l n="1" num="1.1">Te souviens-tu de ce matin d’hiver,</l>
					<l n="2" num="1.2">De la dernière et chère promenade ?</l>
					<l n="3" num="1.3">Il faisait beau, le soleil était clair :</l>
					<l n="4" num="1.4">C’était un temps d’heureux ou de malade.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’était aussi notre pays charmant,</l>
					<l n="6" num="2.2">Le fleuve lent et sa rive un peu plate;</l>
					<l n="7" num="2.3">Et les coteaux qui dressent finement</l>
					<l n="8" num="2.4">Au bord du ciel leur forme délicate;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et je pensais : les pentes de velours</l>
					<l n="10" num="3.2">Verront encor la belle promeneuse.</l>
					<l n="11" num="3.3">Aux mois si doux où l’été fait les jours</l>
					<l n="12" num="3.4">Longs et pareils à l’âme lumineuse.</l>
				</lg>
			</div>
			<div type="poem" key="MRT193">
				<head type="number">XXVII</head>
				<lg n="1">
					<l n="1" num="1.1">En vain ma force se roidit.</l>
					<l n="2" num="1.2">C’est bien fini : je l’ai revue.</l>
					<l n="3" num="1.3">Elle était gaie. On aurait dit</l>
					<l n="4" num="1.4">Que je ne l’avais pas connue.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Quel changement subit et grand</l>
					<l n="6" num="2.2">Pourquoi suis-je resté le même ?</l>
					<l n="7" num="2.3">Son beau visage indifférent</l>
					<l n="8" num="2.4">Est à peine celui que j’aime,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Puisse l’oubli venir pour moi</l>
					<l n="10" num="3.2">De la douce vie ancienne !</l>
					<l n="11" num="3.3">— Ou bien, par une juste loi,</l>
					<l n="12" num="3.4">Qu’elle, un jour aussi, se souvienne !</l>
				</lg>
			</div>
			<div type="poem" key="MRT194">
				<head type="number">XXVIII</head>
				<lg n="1">
					<l n="1" num="1.1">Il ne faut pas les appeler cruelles :</l>
					<l n="2" num="1.2">Elles le sont tout naturellement,</l>
					<l n="3" num="1.3">Comme les fleurs, quelquefois les plus belles,</l>
					<l n="4" num="1.4">Dont le parfum fait qu’on meurt en dormant.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Quand la fraîcheur pure de leur haleine</l>
					<l n="6" num="2.2">Embaume l’air et flotte autour de nous.</l>
					<l n="7" num="2.3">C’est un vertige, et la chambre en est pleine,</l>
					<l n="8" num="2.4">Et des langueurs fléchissent nos genoux.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">C’est leur vertu, ce n’est pas leur envie</l>
					<l n="10" num="3.2">D’être un péril : tout poison est normal.</l>
					<l n="11" num="3.3">Ô douces fleurs, ô roses de la vie,</l>
					<l n="12" num="3.4">Vous exhalez le bien comme le mal !</l>
				</lg>
			</div>
			<div type="poem" key="MRT195">
				<head type="number">XXIX</head>
				<lg n="1">
					<l n="1" num="1.1">Non, je ne te réclame rien ;</l>
					<l n="2" num="1.2">Conserve de l’heure passée</l>
					<l n="3" num="1.3">Tout ce que tu pris de mon bien :</l>
					<l n="4" num="1.4">Mon cœur, hélas! et ma pensée.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tu pourras en avoir besoin</l>
					<l n="6" num="2.2">En ces tristes nuits sans délire</l>
					<l n="7" num="2.3">Où l’on pleurerait dans un coin,</l>
					<l n="8" num="2.4">Si l’on pouvait, au lieu de rire.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Dans ton cœur à peine fermé</l>
					<l n="10" num="3.2">Souffre que le regret s’attarde.</l>
					<l n="11" num="3.3">— Le souvenir d’avoir aimé</l>
					<l n="12" num="3.4">Te suive longtemps, et te garde !</l>
				</lg>
			</div>
			<div type="poem" key="MRT196">
				<head type="number">XXX</head>
				<lg n="1">
					<l n="1" num="1.1">Ce n’est pas moi qui dois pleurer,</l>
					<l n="2" num="1.2">Et ce n’est pas moi qu’il faut plaindre :</l>
					<l n="3" num="1.3">Je puis encore t’adorer ;</l>
					<l n="4" num="1.4">L’oubli ne saurait pas m’atteindre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’est toi, bientôt, qui t’en iras,</l>
					<l n="6" num="2.2">Ne sachant plus comment on aime,</l>
					<l n="7" num="2.3">Jeter, hélas ! en d’autres bras</l>
					<l n="8" num="2.4">Le blanc fantôme de toi-même.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Dans ton cœur dévasté l’oubli</l>
					<l n="10" num="3.2">Sèche les fleurs à peine écloses.</l>
					<l n="11" num="3.3">— Sur mon amour enseveli</l>
					<l n="12" num="3.4">Le temps fera fleurir des roses.</l>
				</lg>
			</div>
			<div type="poem" key="MRT197">
				<head type="number">XXXI</head>
				<lg n="1">
					<l n="1" num="1.1">Quand tu n’auras plus ton beau sein,</l>
					<l n="2" num="1.2">Ni la douceur de ton haleine,</l>
					<l n="3" num="1.3">Ni l’éclat rose et le dessin</l>
					<l n="4" num="1.4">De ta joue adorable et pleine,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Alors je serai presque vieux :</l>
					<l n="6" num="2.2">Mon heure aussi sera passée,</l>
					<l n="7" num="2.3">Mais l’âge aura mis dans mes yeux</l>
					<l n="8" num="2.4">Et sur mon front plus de pensée</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ton cœur sera triste et déçu</l>
					<l n="10" num="3.2">Et tu songeras : « Lui, peut-être,</l>
					<l n="11" num="3.3">« Ne se serait pas aperçu,</l>
					<l n="12" num="3.4">Ou ne l’eût pas laissé paraître. »</l>
				</lg>
			</div>
			<div type="poem" key="MRT198">
				<head type="number">XXXII</head>
				<lg n="1">
					<l n="1" num="1.1">A m’avouer pour son amant</l>
					<l n="2" num="1.2">Il faudra bien qu’on s’habitue.</l>
					<l n="3" num="1.3">— Du marbre pur, rose et charmant.</l>
					<l n="4" num="1.4">J’ai fait jaillir une statue.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">J’ai taillé le bloc de façon</l>
					<l n="6" num="2.2">Que ma main s’y puisse connaître ;</l>
					<l n="7" num="2.3">Et l’on doit garder le soupçon</l>
					<l n="8" num="2.4">Que je demeurerai son maître.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Des bras pourront la posséder</l>
					<l n="10" num="3.2">Et fléchir sous sa blanche étreinte ;</l>
					<l n="11" num="3.3">Nul œil jaloux la regarder,</l>
					<l n="12" num="3.4">Sans qu’il y trouve mon empreinte !</l>
				</lg>
			</div>
			<div type="poem" key="MRT199">
				<head type="number">XXXIII</head>
				<lg n="1">
					<l n="1" num="1.1">Tu peux bien ne pas revenir</l>
					<l n="2" num="1.2">Si c’est à présent ton envie ;</l>
					<l n="3" num="1.3">Mais redoute mon souvenir,</l>
					<l n="4" num="1.4">Qui, malgré toi, t’aura suivie</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dans les songes des nuits d’été</l>
					<l n="6" num="2.2">Des étoiles étaient écloses.</l>
					<l n="7" num="2.3">Ton pied cher, sans but arrêté.</l>
					<l n="8" num="2.4">A perdu le chemin des roses</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Il n’est de loin pas de retour.</l>
					<l n="10" num="3.2">Les sources claires sont taries</l>
					<l n="11" num="3.3">Où tu mirais ton pauvre amour…</l>
					<l n="12" num="3.4">Les petites fleurs sont flétries !</l>
				</lg>
			</div>
			<div type="poem" key="MRT200">
				<head type="number">XXXIV</head>
				<lg n="1">
					<l n="1" num="1.1">Pourquoi la renier ?</l>
					<l n="2" num="1.2">Je n’ai pas de colère.</l>
					<l n="3" num="1.3">Ô mon amour dernier,</l>
					<l n="4" num="1.4">Ô chose bleue et claire !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pourquoi me souvenir</l>
					<l n="6" num="2.2">Qu’elle me fût amère ?</l>
					<l n="7" num="2.3">J’aime mieux retenir</l>
					<l n="8" num="2.4">Par l’aile ma chimère.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le pardon est plus doux.</l>
					<l n="10" num="3.2">Mon adieu se colore</l>
					<l n="11" num="3.3">D’un regret sans courroux,</l>
					<l n="12" num="3.4">D’avoir perdu l’aurore.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>