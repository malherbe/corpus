ROS
———
ROS_1

Edmond ROSTAND
1868-1918

════════════════════════════════════════════
LES MUSARDISES

1887-1893

4812 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I LA CHAMBRE D’ÉTUDIANT
	─ poème	ROS1	I. DÉDICACE
	─ poème	ROS2	II. LA CHAMBRE
	─ poème	ROS3	III. À MA LAMPE
	─ poème	ROS4	IV. À LA MÊME - EN LA COIFFANT DE SON ABAT-JOUR
	─ poème	ROS5	V. LE DIVAN
	─ poème	ROS6	VI. LA FENÊTRE
	─ poème	ROS7	VII. CHARIVARI À LA LUNE
	─ poème	ROS8	VIII. LE VIEUX PION
	─ poème	ROS9	IX. LES SONGE-CREUX
	─ poème	ROS10	X. LA FORÊT
	─ poème	ROS11	XI. OÙ L’ON RETROUVE PIF-LUISANT
	─ poème	ROS12	XII. OÙ L’ON PERD PIF-LUISANT
	─ poème	ROS13	XIII. SOUVENIRS DE VACANCES
	─ poème	ROS14	XIV. LA PREMIÈRE
	─ poème	ROS15	XV. "Oh ! les yeux, les beaux yeux des femmes !"
	─ poème	ROS16	XVI. LES TZIGANES
	─ poème	ROS17	XVII. BALLADE DE LA NOUVELLE ANNÉE
	─ poème	ROS18	XVIII. DEUX MAGASINS
	─ poème	ROS19	XIX. L’ALBUM DE PHOTOGRAPHIES
	─ poème	ROS20	XX. AU CIEL
	─ poème	ROS21	XXI. BALLADE - DES VERS QU’ON NE FINIT JAMAIS
	─ poème	ROS22	XXII. SUR UN EXEMPLAIRE - DE LA PREMIÈRE ÉDITION DE CE LIVRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II INCERTITUDES
	─ poème	ROS23	I. CHANSON DANS LE SOIR
	─ poème	ROS24	II. EXERCICES
	─ poème	ROS25	III. LES BARQUES ATTACHÉES
	─ poème	ROS26	IV. MATIN
	─ poème	ROS27	V. SILENCE
	─ poème	ROS28	VI. BILLET DE REMERCIEMENT
	─ poème	ROS29	VII. "N’obligez pas le poème"
	─ poème	ROS30	VIII. LE SOUVENIR VAGUE
	─ poème	ROS31	IX. "Oui, sans doute, et tant pis pour ceux que l’aveu choque"
	─ poème	ROS32	X. NOS RIRES
	─ poème	ROS33	XI. LES DEUX CAVALIERS
	─ poème	ROS34	XII. L’HEURE CHARMANTE
	─ poème	ROS35	XIII. LE CAUCHEMAR

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III LA MAISON DES PYRÉNÉES
	─ poème	ROS36	I. LA MAISON
	─ poème	ROS37	II. LES PYRÉNÉES
	─ poème	ROS38	III. L’EAU
	─ poème	ROS39	IV. LA BRANCHE
	─ poème	ROS40	V. LA FONTAINE DE CARAOUET
	─ poème	ROS41	VI. LA GLYCINE
	─ poème	ROS42	VII. LE CARILLON DE SAINT-MAMET
	─ poème	ROS43	VIII. PRIÈRE D’UN MATIN BLEU
	─ poème	ROS44	IX. OMBRES ET FUMÉES
	─ poème	ROS45	X. LA FLEUR
	─ poème	ROS46	XI. L’IF
	─ poème	ROS47	XII. LA BROUETTE
	─ poème	ROS48	XIII. L’AMOUREUX DE MARGARIDON
	─ poème	ROS49	XIV. LES BŒUFS
	─ poème	ROS50	XV. LES GENETS
	─ poème	ROS51	XVI. "Derniers petits chants et derniers ébats"
	─ poème	ROS52	XVII. L’OURS
	─ poème	ROS53	XVIII. TOUT D’UN COUP
	─ poème	ROS54	XIX. LE MENDIANT FLEURI
	─ poème	ROS55	XX. LE CONTREBANDIER
