VOI
———
VOI_1

Vincent VOITURE
1597-1648

════════════════════════════════════════════
Quelques vers de Monsieur de Voiture

1650

440 vers

─ poème	VOI1	STANCES
─ poème	VOI2	STANCES
─ poème	VOI3	STANCES SUR SA MAISTRESSE
─ poème	VOI4	POUR MINERVE EN UN BALET
─ poème	VOI5	STANCES SUR UNE DAME
─ poème	VOI6	SONNET
─ poème	VOI7	AUTRE
─ poème	VOI8	RONDEAU
─ poème	VOI9	AUTRE
─ poème	VOI10	AUTRE
─ poème	VOI11	AUTRE
─ poème	VOI12	STANCES A LA LOUANGE DU SOULIER
