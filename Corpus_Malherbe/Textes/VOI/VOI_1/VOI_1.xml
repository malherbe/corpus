<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Quelques vers de Monsieur de Voiture</title>
				<title type="medium">Une édition électronique</title>
				<author key="VOI">
					<name>
						<forename>Vincent</forename>
						<surname>VOITURE</surname>
					</name>
					<date from="1597" to="1648">1597-1648</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>Aurélie</forename>
						<surname>Duval</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>440 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">VOI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Quelques vers de Monsieur de Voiture</title>
						<author>Vincent Voiture</author>
					</titleStmt>
						<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/curiosa/voiture.htm</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES MUSES OUBLIÉES</title>
								<title>QUELQUES VERS DE MONSIEUR DE VOITURE</title>
								<author>Vincent Voiture</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Editions de la Sirène</publisher>
									<date when="1922">1921-1922</date>
								</imprint>
								<biblScope unit="vol">volume 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1650">1650</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="VOI1">
				<head type="main">STANCES</head>
				<head type="sub_1">ESCRITES SUR DES TABLETTES</head>
				<lg n="1">
					<l n="1" num="1.1">Voicy mon amour sur la touche :</l>
					<l n="2" num="1.2">Jugez s’il marque nettement,</l>
					<l n="3" num="1.3">Et si sa pointe se rebouche,</l>
					<l n="4" num="1.4">Dans la peine et dans le tourment.</l>
					<l n="5" num="1.5">Mais en l’estat où je me treuve,</l>
					<l n="6" num="1.6">Qu’est-il besoin de cette preuve,</l>
					<l n="7" num="1.7">Pour vous montrer que ma langueur</l>
					<l n="8" num="1.8">Et que ma constance est extréme ?</l>
					<l n="9" num="1.9">Ne le sçavez-vous pas vous-mesme</l>
					<l n="10" num="1.10">Si vous m’avez touché le cœur ?</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Je croirois avoir trop d’amour,</l>
					<l n="12" num="2.2">Et de vous estre trop fidelle,</l>
					<l n="13" num="2.3">Si vous n’estiez qu’un peu plus belle,</l>
					<l n="14" num="2.4">Que l’Astre qui donne le jour.</l>
					<l n="15" num="2.5">Mais puisque le reste du monde,</l>
					<l n="16" num="2.6">N’a rien de beau qui vous seconde ;</l>
					<l n="17" num="2.7">Et que tout cede au Dieu vainqueur</l>
					<l n="18" num="2.8">Que vostre bel œil emprisonne,</l>
					<l n="19" num="2.9">Il ne faut pas que je m’estonne,</l>
					<l n="20" num="2.10">Si vous m’avez touché le cœur.</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">Vous ne sçauriez douter de moy,</l>
					<l n="22" num="3.2">Ni de la peine que j’endure,</l>
					<l n="23" num="3.3">Pour servir une ame trop dure :</l>
					<l n="24" num="3.4">Car la touche vous en fait foy.</l>
					<l n="25" num="3.5">Sans estre donc plus recherchée,</l>
					<l n="26" num="3.6">Souffrez aussi d’estre touchée,</l>
					<l n="27" num="3.7">Et despoüillez cette rigueur,</l>
					<l n="28" num="3.8">Qui rend vostre beauté farouche.</l>
					<l n="29" num="3.9">Je vous puis bien toucher la bouche,</l>
					<l n="30" num="3.10">Si vous m’avez touché le cœur.</l>
				</lg>
			</div>
			<div type="poem" key="VOI2">
				<head type="main">STANCES</head>
				<head type="sub_1">ESCRITES DE LA MAIN GAUCHE</head>
				<head type="sub_2">sur un feüillet des mesmes Tablettes, <lb/>qui regardoit un miroir mis au <lb/>dedans de la couverture</head>
				<lg n="1">
					<l n="1" num="1.1">Quand je me plaindrois nuit et jour</l>
					<l n="2" num="1.2">De la cruauté de mes peines,</l>
					<l n="3" num="1.3">Et quand du pur sang de mes veines</l>
					<l n="4" num="1.4">Je vous escrirois mon amour :</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Si vous ne voyez, à l’instant,</l>
					<l n="6" num="2.2">Le bel objet qui l’a fait naistre,</l>
					<l n="7" num="2.3">Vous ne le pourrez reconnoistre,</l>
					<l n="8" num="2.4">Ni croire que je souffre tant.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">En vos yeux, mieux qu’en mes escris</l>
					<l n="10" num="3.2">Vous verrez l’ardeur de mon ame,</l>
					<l n="11" num="3.3">Et les rayons de cette flame</l>
					<l n="12" num="3.4">Dont pour vous je me trouve espris.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Vos beautez vous le feront voir,</l>
					<l n="14" num="4.2">Bien mieux que je ne le puis dire :</l>
					<l n="15" num="4.3">Et vous ne le sçauriez bien lire,</l>
					<l n="16" num="4.4">Que dans la glace d’un miroir.</l>
				</lg>
			</div>
			<div type="poem" key="VOI3">
				<head type="main">STANCES SUR SA MAISTRESSE</head>
				<head type="sub_2">rencontrée en habit de garçon <lb/>un soir du Carnaval</head>
				<lg n="1">
					<l n="1" num="1.1">Je sens au profond de mon ame,</l>
					<l n="2" num="1.2">Brusler une nouvelle flame :</l>
					<l n="3" num="1.3">Et laissant les autres amours,</l>
					<l n="4" num="1.4">Qui tenoient mon ame en altere,</l>
					<l n="5" num="1.5">J’ayme un garçon depuis trois jours,</l>
					<l n="6" num="1.6">Plus beau que celuy de Cythere.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Si le but de cette pensée,</l>
					<l n="8" num="2.2">A ma conscience offensée,</l>
					<l n="9" num="2.3">J’en ay defia le chastiment.</l>
					<l n="10" num="2.4">Car le feu qui brusla Gomore,</l>
					<l n="11" num="2.5">Ne fut jamais si vehement,</l>
					<l n="12" num="2.6">Que celuy-là qui me devore.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Mais je ne croy pas que l’on blâme</l>
					<l n="14" num="3.2">L’amoureuse ardeur dont m’enflame</l>
					<l n="15" num="3.3">Le bel œil de ce jouvenceau ;</l>
					<l n="16" num="3.4">Ni qu’aymer d’un amour extréme</l>
					<l n="17" num="3.5">Ce que Nature a fait de beau,</l>
					<l n="18" num="3.6">Soit un peché contre elle-mesme.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Un soir que j’attendois la Belle,</l>
					<l n="20" num="4.2">Qui depuis deux ans m’ensorcelle ;</l>
					<l n="21" num="4.3">Je vis comme tombé des Cieux,</l>
					<l n="22" num="4.4">Ce Narcisse objet de ma flame :</l>
					<l n="23" num="4.5">Et dés qu’il fut devant mes yeux,</l>
					<l n="24" num="4.6">Je le sentis dedans mon ame.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Sa face riante et naïve,</l>
					<l n="26" num="5.2">Jettoit une flame si vive,</l>
					<l n="27" num="5.3">Et tant de rayons alentour,</l>
					<l n="28" num="5.4">Qu’à l’esclat de cette lumiere</l>
					<l n="29" num="5.5">Je doutay que ce fust l’Amour,</l>
					<l n="30" num="5.6">Avecque les yeux de sa mere.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Mille fleurs fraichement écloses,</l>
					<l n="32" num="6.2">Les lys, les œillets et les roses</l>
					<l n="33" num="6.3">Couvroient la neige de son teint.</l>
					<l n="34" num="6.4">Mais dessous ces fleurs entassées,</l>
					<l n="35" num="6.5">Le serpent dont je fus atteint,</l>
					<l n="36" num="6.6">Avoit ses embûches dressées.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Sur un front blanc comme l’yvoire,</l>
					<l n="38" num="7.2">Deux petits arcs de couleur noire,</l>
					<l n="39" num="7.3">Estoient mignardement voûtez :</l>
					<l n="40" num="7.4">D’où ce Dieu qui me fait la guerre,</l>
					<l n="41" num="7.5">Foulant aux pieds nos libertez,</l>
					<l n="42" num="7.6">Triomphoit de toute la terre.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Ses yeux, le Paradis des ames,</l>
					<l n="44" num="8.2">Pleins de ris, d’attraits, et de flames,</l>
					<l n="45" num="8.3">Faisoient de la nuit un beau jour :</l>
					<l n="46" num="8.4">Astres de divines puissances,</l>
					<l n="47" num="8.5">De qui l’Empire de l’Amour</l>
					<l n="48" num="8.6">Prend ses meilleures influences.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Sur tout, il avoit une grace,</l>
					<l n="50" num="9.2">Un je ne sçay quoy qui surpasse</l>
					<l n="51" num="9.3">De l’Amour les plus doux appas,</l>
					<l n="52" num="9.4">Un ris qui ne se peut descrire,</l>
					<l n="53" num="9.5">Un air que les autres n’ont pas,</l>
					<l n="54" num="9.6">Que l’on voit, et qu’on ne peut dire.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Parmy tant d’ennemis renduë.</l>
					<l n="56" num="10.2">Ma liberté mal defenduë,</l>
					<l n="57" num="10.3">Fut sous le joug d’un Estranger ;</l>
					<l n="58" num="10.4">Mon Cœur se rendit à sa suite,</l>
					<l n="59" num="10.5">Et dans le fort de ce danger</l>
					<l n="60" num="10.6">Ma Raison se mit à la fuite.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Sans le connoistre davantage,</l>
					<l n="62" num="11.2">Ma volonté luy fit hommage</l>
					<l n="63" num="11.3">De tout ce qu’elle avoit en main ;</l>
					<l n="64" num="11.4">Mais du meschant l’ame inconstante,</l>
					<l n="65" num="11.5">Me trompa dés le lendemain,</l>
					<l n="66" num="11.6">Et me frustra de mon attente.</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">Plein de dépit et de colère.</l>
					<l n="68" num="12.2">Soudain je m’en devois défaire :</l>
					<l n="69" num="12.3">Apprenant par cette leçon,</l>
					<l n="70" num="12.4">Qu’il n’avoit point d’arrest en l’ame,</l>
					<l n="71" num="12.5">Et que sous l’habit d’un garcon,</l>
					<l n="72" num="12.6">Il portoit le cœur d’une femme.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">Toutefois, malgré cette injure,</l>
					<l n="74" num="13.2">J’en pris un plus heureux augure :</l>
					<l n="75" num="13.3">Et je n’eusse pû croire alors,</l>
					<l n="76" num="13.4">Que le Ciel, dont il fut l’ouvrage,</l>
					<l n="77" num="13.5">Sous le voile d’un si beau corps,</l>
					<l n="78" num="13.6">Eust mis un si mauvais courage.</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1">Mais sa malice découverte,</l>
					<l n="80" num="14.2">S’est reconnuë avec ma perte,</l>
					<l n="81" num="14.3">Car depuis on ne l’a pû voir :</l>
					<l n="82" num="14.4">Le perfide a gagné la fuite,</l>
					<l n="83" num="14.5">Tenant mon cœur en son pouvoir,</l>
					<l n="84" num="14.6">Avec ma liberté seduite.</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1">Gagné d’une sorciere flame,</l>
					<l n="86" num="15.2">J’avois mis les clefs de mon ame</l>
					<l n="87" num="15.3">En la garde de ce voleur :</l>
					<l n="88" num="15.4">Mais d’une malice funeste,</l>
					<l n="89" num="15.5">M’en ayant rauy le meilleur,</l>
					<l n="90" num="15.6">Il mit le feu dedans le reste.</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1">Mais je l’ayme, et quoy qu’il me face,</l>
					<l n="92" num="16.2">Je voudrois revoir cette face,</l>
					<l n="93" num="16.3">Ce chef-d’œuvre tant estimé,</l>
					<l n="94" num="16.4">Où le Ciel tout son mieux assemble :</l>
					<l n="95" num="16.5">Et depuis j’ay tousjours aymé</l>
					<l n="96" num="16.6">Une fille qui luy ressemble.</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1">Avec les traits de son visage,</l>
					<l n="98" num="17.2">Elle a sa taille et son corsage,</l>
					<l n="99" num="17.3">Sa voix, son port, et sa façon,</l>
					<l n="100" num="17.4">Son doux ris, son adresse extréme.</l>
					<l n="101" num="17.5">Enfin, sous l’habit d’un garcon,</l>
					<l n="102" num="17.6">Je l’aurois prise pour luy-mesme.</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1">Ses yeux sçavent les mesmes charmes,</l>
					<l n="104" num="18.2">Elle vse de pareilles armes,</l>
					<l n="105" num="18.3">Avec tous les mesmes attraits :</l>
					<l n="106" num="18.4">Et croy, tant elle luy ressemble,</l>
					<l n="107" num="18.5">Qu’elle luy touche de bien prés,</l>
					<l n="108" num="18.6">Et qu’ils sont alliez ensemble.</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1">Elle connoist bien, la meschante,</l>
					<l n="110" num="19.2">La cause du mal qui m’enchante,</l>
					<l n="111" num="19.3">Et qui me retient en langueur :</l>
					<l n="112" num="19.4">Et, sans doute, elle pourroit dire</l>
					<l n="113" num="19.5">Quelque nouvelle de mon cœur,</l>
					<l n="114" num="19.6">Et de celuy qui le retire.</l>
				</lg>
				<lg n="20">
					<l n="115" num="20.1">Car, sans en voir d’autre apparence,</l>
					<l n="116" num="20.2">Je jurerois en asseurance,</l>
					<l n="117" num="20.3">A voir son visage assassin,</l>
					<l n="118" num="20.4">Et son œillade cauteleuse,</l>
					<l n="119" num="20.5">Qu’elle a sa part à ce larcin,</l>
					<l n="120" num="20.6">Et qu’elle en est la receleuse.</l>
				</lg>
				<lg n="21">
					<l n="121" num="21.1">Amour, petit Dieu qui disposes</l>
					<l n="122" num="21.2">Du reglement de toutes choses ;</l>
					<l n="123" num="21.3">Et qui fais entendre tes loix</l>
					<l n="124" num="21.4">Par toute la machine ronde :</l>
					<l n="125" num="21.5">Fay-moy justice à cette fois,</l>
					<l n="126" num="21.6">Toy qui fais droit à tout le monde.</l>
				</lg>
				<lg n="22">
					<l n="127" num="22.1">Fay-moy raison de l’inhumaine,</l>
					<l n="128" num="22.2">Qui retient mon cœur à la gehesne,</l>
					<l n="129" num="22.3">Sans esperance d’avoir mieux ;</l>
					<l n="130" num="22.4">Mais, sur tout, ne voy pas la belle :</l>
					<l n="131" num="22.5">Car si tu regardes ses yeux,</l>
					<l n="132" num="22.6">Je sçay que tu seras pour elle.</l>
				</lg>
				<lg n="23">
					<l n="133" num="23.1">La mauvaise me tient ravie</l>
					<l n="134" num="23.2">Mon ame, mon cœur, et ma vie,</l>
					<l n="135" num="23.3">Car chez elle se vient sauver</l>
					<l n="136" num="23.4">Le voleur de cette depoüille.</l>
					<l n="137" num="23.5">Mais j’espere tout retrouver,</l>
					<l n="138" num="23.6">Si tu permets que je la foüille.</l>
				</lg>
			</div>
			<div type="poem" key="VOI4">
				<head type="main">POUR MINERVE EN UN BALET</head>
				<lg n="1">
					<l n="1" num="1.1">Vovs qui chassiez de vostre Cour</l>
					<l n="2" num="1.2">Toutes les mollesses d’Amour,</l>
					<l n="3" num="1.3">Et les feux dont il se conserve :</l>
					<l n="4" num="1.4">D’où vous sont ces attraits venus ?</l>
					<l n="5" num="1.5">Et depuis quand, belle Minerve,</l>
					<l n="6" num="1.6">Avez-vous les yeux de Venus ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Les Graces qui suivent tousjours</l>
					<l n="8" num="2.2">La douce Mere des Amours,</l>
					<l n="9" num="2.3">Vont à vous comme à la plus belle :</l>
					<l n="10" num="2.4">Mesme ce Dieu qui sçait voler,</l>
					<l n="11" num="2.5">S’il vous voyoit mise auprés d’elle,</l>
					<l n="12" num="2.6">Ne sçauroit à laquelle aller.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Si vous eussiez eu ces appas,</l>
					<l n="14" num="3.2">Lors que vous vinstes icy bas,</l>
					<l n="15" num="3.3">Vous faire voir aux yeux d’un homme :</l>
					<l n="16" num="3.4">Sans quitter le sejour des Cieux,</l>
					<l n="17" num="3.5">Vous eussiez remporté la pomme,</l>
					<l n="18" num="3.6">Au jugement de tous les Dieux.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Vos charmes ont plus de pouvoir,</l>
					<l n="20" num="4.2">Que ceux que nous venons de voir</l>
					<l n="21" num="4.3">Dans l’enchantement d’une couppe,</l>
					<l n="22" num="4.4">Ils sont bien plus forts et plus doux :</l>
					<l n="23" num="4.5">Et je ne sçache en cette trouppe,</l>
					<l n="24" num="4.6">D’autre enchanteresse que vous.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Cette Circé, dont les Demons</l>
					<l n="26" num="5.2">Applaudissent l’orgueil des monts,</l>
					<l n="27" num="5.3">Qui remplit la Terre d’allarmes,</l>
					<l n="28" num="5.4">Et renverse l’ordre des Cieux,</l>
					<l n="29" num="5.5">A dans ses livres moins de charmes,</l>
					<l n="30" num="5.6">Que vous n’en avez dans vos yeux.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Elle peut le monde troubler,</l>
					<l n="32" num="6.2">Elle fait les Astres trembler,</l>
					<l n="33" num="6.3">Et bride le cours de la Lune :</l>
					<l n="34" num="6.4">Mais vous, d’un pouvoir sans pareil,</l>
					<l n="35" num="6.5">Dans le milieu de la nuit brune,</l>
					<l n="36" num="6.6">Vous nous faites voir un Soleil.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Mille rayons ensorcelez,</l>
					<l n="38" num="7.2">Sortent de vos yeux estoillez,</l>
					<l n="39" num="7.3">Qui percent sans faire ouverture :</l>
					<l n="40" num="7.4">Et redoutée en toutes pars,</l>
					<l n="41" num="7.5">Vous faites bransler la Nature,</l>
					<l n="42" num="7.6">Par le moyen de vos regars.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Aussi faudra-t’il desormais</l>
					<l n="44" num="8.2">Qu’elle vous cede pour jamais.</l>
					<l n="45" num="8.3">Car plus docte Magicienne,</l>
					<l n="46" num="8.4">Vous meritez le maniment</l>
					<l n="47" num="8.5">D’une autre verge que la sienne,</l>
					<l n="48" num="8.6">Et qui charme plus puissamment.</l>
				</lg>
			</div>
			<div type="poem" key="VOI5">
				<head type="main">STANCES SUR UNE DAME</head>
				<head type="sub_1">DONT LA JUPPE</head>
				<head type="sub_2">fut retroussée en versant dans un <lb/>carrosse, à la campagne</head>
				<lg n="1">
					<l n="1" num="1.1">Philis je suis dessous vos loix :</l>
					<l n="2" num="1.2">Et sans remede à cette fois</l>
					<l n="3" num="1.3">Mon ame est vostre prisonniere.</l>
					<l n="4" num="1.4">Mais sans justice et sans raison,</l>
					<l n="5" num="1.5">Vous m’avez pris par le derriere :</l>
					<l n="6" num="1.6">N’est-ce pas une trahison ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Je m’estois gardé de vos yeux ;</l>
					<l n="8" num="2.2">Et ce visage gracieux,</l>
					<l n="9" num="2.3">Qui peut faire paslir le nostre,</l>
					<l n="10" num="2.4">Contre moy n’ayant point d’appas,</l>
					<l n="11" num="2.5">Vous m’en avez fait voir un autre,</l>
					<l n="12" num="2.6">Dequoy je ne me gardois pas.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">D’abord il se fit mon vainqueur :</l>
					<l n="14" num="3.2">Ses attraits percerent mon cœur,</l>
					<l n="15" num="3.3">Ma liberté se vit ravie :</l>
					<l n="16" num="3.4">Et le meschant, en cét estat,</l>
					<l n="17" num="3.5">S’estoit caché toute sa vie,</l>
					<l n="18" num="3.6">Pour faire cét assassinat.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Il est vray que je fus surpris.</l>
					<l n="20" num="4.2">Le feu passa dans mes esprits :</l>
					<l n="21" num="4.3">Et mon cœur autrefois superbe,</l>
					<l n="22" num="4.4">Humble, se rendit à l’Amour,</l>
					<l n="23" num="4.5">Quand il vit vostre cu sur l’herbe,</l>
					<l n="24" num="4.6">Faire honte aux rayons du jour.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Le Soleil confus dans les Cieux,</l>
					<l n="26" num="5.2">En le voyant si radieux,</l>
					<l n="27" num="5.3">Pensa retourner en arriere,</l>
					<l n="28" num="5.4">Son feu ne servant plus de rien,</l>
					<l n="29" num="5.5">Mais ayant veû vostre derriere,</l>
					<l n="30" num="5.6">Il n’osa plus montrer le sien.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">En decouvrant tant de beautez,</l>
					<l n="32" num="6.2">Les Sylvains furent enchantez,</l>
					<l n="33" num="6.3">Et Zephyre voyant encore</l>
					<l n="34" num="6.4">D’autres appas que vous avez :</l>
					<l n="35" num="6.5">Mesme en la presence de Flore,</l>
					<l n="36" num="6.6">Vous baisa ce que vous sçavez.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">La Rose la Reyne des fleurs,</l>
					<l n="38" num="7.2">Perdit ses plus vives couleurs ;</l>
					<l n="39" num="7.3">De crainte l’œillet devint blesme :</l>
					<l n="40" num="7.4">Et Narcisse alors convaincu,</l>
					<l n="41" num="7.5">Oublia l’amour de soy-mesme,</l>
					<l n="42" num="7.6">Pour se mirer en vostre cu.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Aussi rien n’est si precieux,</l>
					<l n="44" num="8.2">Et la clarté de vos beaux yeux,</l>
					<l n="45" num="8.3">Vostre teint qui jamais ne change,</l>
					<l n="46" num="8.4">Et le reste de vos appas,</l>
					<l n="47" num="8.5">Ne meritent point de louange,</l>
					<l n="48" num="8.6">Qu’alors qu’il ne se montre pas.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">On m’a dit qu’il a des defaux</l>
					<l n="50" num="9.2">Qui me causeront mille maux ;</l>
					<l n="51" num="9.3">Car il est farouche à merveilles :</l>
					<l n="52" num="9.4">Il est dur comme un diamant,</l>
					<l n="53" num="9.5">Il est sans yeux et sans oreilles,</l>
					<l n="54" num="9.6">Et ne parle que rarement.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Mais je l’ayme, et veux que mes vers</l>
					<l n="56" num="10.2">Par tous les coins de l’Univers</l>
					<l n="57" num="10.3">En fassent vivre la memoire :</l>
					<l n="58" num="10.4">Et ne veux penser desormais</l>
					<l n="59" num="10.5">Qu’à chanter dignement la gloire</l>
					<l n="60" num="10.6">Du plus beau cu qui fut jamais.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Philis, cachez bien ses appas,</l>
					<l n="62" num="11.2">Les mortels ne dureroient pas,</l>
					<l n="63" num="11.3">Si ces beautez estoient sans voiles.</l>
					<l n="64" num="11.4">Les Dieux qui regnent dessus nous,</l>
					<l n="65" num="11.5">Assis la-haut sur les Estoilles,</l>
					<l n="66" num="11.6">Ont un moins beau siege que vous.</l>
				</lg>
			</div>
			<div type="poem" key="VOI6">
				<head type="main">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1">Il faut finir mes jours en l’amour d’Uranie,</l>
					<l n="2" num="1.2">L’absence ni le temps ne m’en sçauroient guerir</l>
					<l n="3" num="1.3">Et je ne voy plus rien qui me pût secourir,</l>
					<l n="4" num="1.4">Ni qui sceust r’appeller ma liberté bannie.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dés long-temps je connois sa rigueur infinie,</l>
					<l n="6" num="2.2">Mais pensant aux beautez pour qui je dois perir :</l>
					<l n="7" num="2.3">Je benis mon martyre, et content de mourir,</l>
					<l n="8" num="2.4">Je n’ose murmurer contre sa tyrannie.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Quelquefois ma raison, par de foibles discours,</l>
					<l n="10" num="3.2">M’incite à la revolte, et me promet secours.</l>
					<l n="11" num="3.3">Mais lors qu’à mon besoin je me veux servir d’elle ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1">Apres beaucoup de peine et d’efforts impuissans</l>
					<l n="13" num="4.2">Elle dit qu’Uranie est seule aymable et belle,</l>
					<l n="14" num="4.3">Et m’y rengage plus que ne font tous mes sens.</l>
				</lg>
			</div>
			<div type="poem" key="VOI7">
				<head type="main">AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Des portes du matin l’Amante de Cephale,</l>
					<l n="2" num="1.2">Ses roses espandoit dans le milieu des airs,</l>
					<l n="3" num="1.3">Et jettoit sur les Cieux nouvellement ouvers,</l>
					<l n="4" num="1.4">Ces traits d’or et d’azur, qu’en naissant elle estale.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Quand la Nymphe divine, à mon repos fatale,</l>
					<l n="6" num="2.2">Apparut, et brilla de tant d’attraits divers,</l>
					<l n="7" num="2.3">Qu’il sembloit qu’elle seule esclairoit l’Univers,</l>
					<l n="8" num="2.4">Et remplissoit de feux la rive Orientale.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le Soleil se hastant pour la gloire des Cieux,</l>
					<l n="10" num="3.2">Vint opposer sa flame à l’éclat de ses yeux,</l>
					<l n="11" num="3.3">Et prit tous les rayons dont l’Olympe se dore.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1">L’onde, la terre, et l’air s’allumoient à l’entour :</l>
					<l n="13" num="4.2">Mais aupres de Philis on le prit pour l’Aurore,</l>
					<l n="14" num="4.3">Et l’on creut que Philis estoit l’Astre du jour.</l>
				</lg>
			</div>
			<div type="poem" key="VOI8">
				<head type="main">RONDEAU</head>
				<lg n="1">
					<l n="1" num="1.1">Ma foy, c’est fait de moy, car Isabeau</l>
					<l n="2" num="1.2">M’a conjuré de luy faire un Rondeau.</l>
					<l n="3" num="1.3">Cela me met en une peine extréme.</l>
					<l n="4" num="1.4">Quoy treize vers, huit en eau, cinq en eme !</l>
					<l n="5" num="1.5">Je luy ferois aussi-tost un bateau !</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">En voila cinq pourtant en un monceau.</l>
					<l n="7" num="2.2">Faisons-en huict, en invoquant Brodeau,</l>
					<l n="8" num="2.3">Et puis mettons, par quelque stratagéme,</l>
					<l n="9" num="2.4"><space unit="char" quantity="12"/>Ma foy, c’est fait.</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1">Si je pouvois encor de mon cerveau</l>
					<l n="11" num="3.2">Tirer cinq vers, l’ouvrage seroit beau.</l>
					<l n="12" num="3.3">Mais cependant, je suis dedans l’onziéme,</l>
					<l n="13" num="3.4">Et si je croy que je fais le douziéme ;</l>
					<l n="14" num="3.5">En voila treize ajustez au niveau.</l>
					<l n="15" num="3.6"><space unit="char" quantity="12"/>Ma foy, c’est fait.</l>
				</lg>
			</div>
			<div type="poem" key="VOI9">
				<head type="main">AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Un beuveur d’eau, pour aux Dames complaire</l>
					<l n="2" num="1.2">Suivant l’Amour dont le seul feu l’éclaire,</l>
					<l n="3" num="1.3">Se voit tousjours sobre, courtois et doux :</l>
					<l n="4" num="1.4">Et ne sçauriez si tost boire dix coups</l>
					<l n="5" num="1.5">Qu’encor plustot il ne le puisse faire.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Venus d’Amour la gracieuse mere</l>
					<l n="7" num="2.2">Nasquit de l’eau sur les bords de Cythere,</l>
					<l n="8" num="2.3">Aussi son fils favorise sur tous,</l>
					<l n="9" num="2.4"><space unit="char" quantity="12"/>Un beuveur d’eau.</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1">Il entend mieux ses loix et son mistere.</l>
					<l n="11" num="3.2">Il sçait joüir, et discret sçait se taire,</l>
					<l n="12" num="3.3">A le rein ferme, et fermes les genoux.</l>
					<l n="13" num="3.4">Et trente six yurognes comme vous,</l>
					<l n="14" num="3.5">Ne valent pas, en l’amoureuse affaire,</l>
					<l n="15" num="3.6"><space unit="char" quantity="12"/>Un beuveur d’eau.</l>
				</lg>
			</div>
			<div type="poem" key="VOI10">
				<head type="main">AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Tout beau corps, toute belle image,</l>
					<l n="2" num="1.2">Sont grossiers aupres du visage</l>
					<l n="3" num="1.3">Que Philis a receu des Cieux.</l>
					<l n="4" num="1.4">Sa bouche, son ris, et ses yeux,</l>
					<l n="5" num="1.5">Mettent tous les cœurs au pillage.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Sa gorge est un divin ouvrage,</l>
					<l n="7" num="2.2">Rien n’est si droit que son corsage</l>
					<l n="8" num="2.3">Enfin elle a, pour dire mieux,</l>
					<l n="9" num="2.4"><space unit="char" quantity="12"/>Tout beau.</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1">Parmy tout ce qui plus m’engage,</l>
					<l n="11" num="3.2">Est un certain petit passage,</l>
					<l n="12" num="3.3">Qui vermeil et delicieux :</l>
					<l n="13" num="3.4">Mais ce secret est pour les Dieux,</l>
					<l n="14" num="3.5">Ma plume, changeons de langage :</l>
					<l n="15" num="3.6"><space unit="char" quantity="12"/>Tout beau.</l>
				</lg>
			</div>
			<div type="poem" key="VOI11">
				<head type="main">AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Si haut je veux loüer Sylvie,</l>
					<l n="2" num="1.2">Que toute autre en meure d’envie.</l>
					<l n="3" num="1.3">Sa personne est pleine d’appas.</l>
					<l n="4" num="1.4">Les Amours naissent sous ses pas :</l>
					<l n="5" num="1.5">Et c’est par eux qu’elle est servie.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">De cent vertus elle est suivie.</l>
					<l n="7" num="2.2">Son cœur tient mon ame ravie :</l>
					<l n="8" num="2.3">Et les Conquerans ne l’ont pas</l>
					<l n="9" num="2.4"><space unit="char" quantity="12"/>Si haut.</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1">Quoy que mon amour m’y convie,</l>
					<l n="11" num="3.2">Ma langue au secret asservie</l>
					<l n="12" num="3.3">N’ose parler d’un certain cas.</l>
					<l n="13" num="3.4">Je diray seulement tout bas,</l>
					<l n="14" num="3.5">Que je n’en vis un de ma vie</l>
					<l n="15" num="3.6"><space unit="char" quantity="12"/>Si haut.</l>
				</lg>
			</div>
			<div type="poem" key="VOI12">
				<head type="main">STANCES A LA LOUANGE DU SOULIER</head>
				<head type="sub_1">d’une Dame</head>
				<lg n="1">
					<l n="1" num="1.1">Moy qui fut pris ce Caresme,</l>
					<l n="2" num="1.2">Et qui me vis au pouvoir</l>
					<l n="3" num="1.3">D’un beau Soulier jaune, et noir</l>
					<l n="4" num="1.4">Que j’aymois plus que moy-mesme :</l>
					<l n="5" num="1.5">Je suis maintenant en feu,</l>
					<l n="6" num="1.6">Pour un Soulier noir, et bleu.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Comme un criminel qu’on mene</l>
					<l n="8" num="2.2">Où son Destin l’a reduit,</l>
					<l n="9" num="2.3">A la Bastille est conduit,</l>
					<l n="10" num="2.4">Sortant du bois de Vincenne :</l>
					<l n="11" num="2.5">Ainsi mon cœur prisonnier</l>
					<l n="12" num="2.6">Va de soulier en soulier.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Le pied qui cause ma peine,</l>
					<l n="14" num="3.2">Et qui me tient sous sa loy :</l>
					<l n="15" num="3.3">Ce n’est pas un pied de Roy ;</l>
					<l n="16" num="3.4">Mais plustost un pied de Reyne,</l>
					<l n="17" num="3.5">Car je voy dans l’avenir,</l>
					<l n="18" num="3.6">Qu’il le pourra devenir.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Sur ce beau pied la Nature</l>
					<l n="20" num="4.2">Admirable en ses effets,</l>
					<l n="21" num="4.3">A sceu bastir un Palais</l>
					<l n="22" num="4.4">De divine Architecture ;</l>
					<l n="23" num="4.5">Où se trouvent tous les Dieux</l>
					<l n="24" num="4.6">Mieux logez que dans les Cieux.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">C’est un grand Temple d’yvoire,</l>
					<l n="26" num="5.2">Plein de grace et de beauté,</l>
					<l n="27" num="5.3">En quelques lieux marqueté</l>
					<l n="28" num="5.4">D’une Ebene douce et noire</l>
					<l n="29" num="5.5">Qui sert en ce lieu si beau</l>
					<l n="30" num="5.6">Comme d’ombre en un tableau.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Deux flambeaux incomparables,</l>
					<l n="32" num="6.2">Plus brillans que le Soleil,</l>
					<l n="33" num="6.3">Par un éclat sans pareil,</l>
					<l n="34" num="6.4">Et des rayons favorables,</l>
					<l n="35" num="6.5">Rendent les lieux d’alentour</l>
					<l n="36" num="6.6">Pleins de lumiere et d’Amour.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">La nef de cét edifice</l>
					<l n="38" num="7.2">Est pleine d’un jour tres-pur,</l>
					<l n="39" num="7.3">Mais le cœur en est obscur,</l>
					<l n="40" num="7.4">Et fait par tel artifice,</l>
					<l n="41" num="7.5">Que les yeux les plus perçans</l>
					<l n="42" num="7.6">Ne penetrent point dedans.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Tout ce que la Terre et l’Onde</l>
					<l n="44" num="8.2">Produisent de precieux :</l>
					<l n="45" num="8.3">Tout ce qu’on voit dans les Cieux,</l>
					<l n="46" num="8.4">Et qui paroist dans le monde,</l>
					<l n="47" num="8.5">Est fait imparfaitement,</l>
					<l n="48" num="8.6">Au prix de ce bastiment.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Mais un personnage antique,</l>
					<l n="50" num="9.2">Parent de Nostradamus,</l>
					<l n="51" num="9.3">M’a dit en termes confus ;</l>
					<l n="52" num="9.4">Que ce Temple magnifique,</l>
					<l n="53" num="9.5">Pour estre plus exaucé,</l>
					<l n="54" num="9.6">Sera bien-tost renversé.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>