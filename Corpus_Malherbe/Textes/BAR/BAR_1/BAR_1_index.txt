BAR
———
BAR_1

Jules BARBEY D’AUREVILLY
1808-1889

════════════════════════════════════════════
Poussières

1854

1129 vers

─ poème	BAR1	À Valognes
─ poème	BAR2	"Débouclez-les, vos longs cheveux de soie,"
─ poème	BAR3	"Je vivais sans cœur, tu vivais sans flamme,"
─ poème	BAR4	L’Échanson
─ poème	BAR5	La Beauté
─ poème	BAR6	La Haine Du Soleil
─ poème	BAR7	Le Cid
─ poème	BAR8	La Maîtresse Rousse
─ poème	BAR9	Les Nénuphars
─ poème	BAR10	"Oh ! les yeux adorés ne sont pas ceux qui virent"
─ poème	BAR11	"Oh ! pourquoi voyager ? as-tu dit. C’est que l’âme"
─ poème	BAR12	"Te souviens-tu du soir, où près de la fenêtre"
─ poème	BAR13	Treize Ans
─ poème	BAR14	Le Buste Jaune
─ poème	BAR15	Le Vieux Goëland
─ poème	BAR16	"À qui rêves-tu si tu rêve,"
─ poème	BAR17	"Si tu pleures jamais, que ce soit en silence ;"
─ poème	BAR18	"Oh ! comme tu vieillis ! tu n’en es pas moins belle ;"
─ poème	BAR19	Sur Son Album
─ poème	BAR20	"Si j’avais, sous ma mantille,"
─ poème	BAR21	À Roger de Beauvoir
─ poème	BAR22	"Saigne, saigne, mon cœur… saigne ! je veux sourire."
─ poème	BAR23	Un Amour De Jupe
─ poème	BAR24	Les Spectres
─ poème	BAR25	Chanson
─ poème	BAR26	"Tu t’en vas, — ce n’est pas ta faute."
─ poème	BAR27	"Tête pâle de ma Chimère"
