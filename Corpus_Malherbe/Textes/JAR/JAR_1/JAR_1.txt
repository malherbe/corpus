Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
JAR
JAR_1

Alfred JARRY
1873-1907

LES MINUTES DE SABLE MÉMORIAL
1894
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

LES MINUTES DE SABLE MÉMORIAL ; SUIVIES DE CÉSAR-ANTÉCHRIST
Alfred Jarry


https://gallica.bnf.fr/ark:/12148/bpt6k6288784g?rk=193134;0


LES MINUTES DE SABLE MÉMORIAL ; SUIVIES DE CÉSAR-ANTÉCHRIST
Alfred Jarry

Paris
FASQUELLE  ÉDITEURS
1932




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



LES TROIS MEUBLES DU MAGE
SURANNÉS

I
MINÉRAL

Vase olivâtre et vain d’où l’âme est envolée,
Crâne, tu tournes un bon visage indulgent
Vers nous, et souris de ta bouche crénelée.
Mais tu regrettes ton corps, tes cheveux d’argent,


Tes lèvres qui s’ouvraient à la parole ailée.
Et l’orbite creuse où mon regard va plongeant,
Bâille à l’ombre et soupire et s’ennuie esseulée,
Très nette, vide box d’un cheval voyageant.


Tu n’es plus qu’argile et mort. Tes blanches molaires
Sur les tons mats de l’os brillent de flammes claires,
Tels les cuivres fourbis par un larbin soigneux.


Et, presse-papier lourd, sur le haut d’une armoire
Serrant de l’occiput les feuillets du grimoire,
Contre le vent rôdeur tu rechignes, hargneux.



II
VÉGÉTAL

Le vélin écrit rit et grimace, livide.
Les signes sont dansants et fous. Les uns, flambeaux,
Pétillent radieux dans une page vide.
D’autres en rangs pressés, acrobates corbeaux,


Dans la neige épandue ouvrent leur bec avide.
Le livre est un grand arbre émergé des tombeaux.
Et ses feuilles, ainsi que d’un sac qui se vide,
Volent au vent vorace et partent par lambeaux.


Et son tronc est humain comme la mandragore ;
Ses fruits vivants sont les fèves de Pythagore ;
Des feuillets verdoyants lui poussent en avril.


Et les prédictions d’or qu’il emmagasine,
Seul le nécromant peut les lire sans péril,
La nuit, à la lueur des torches de résine.



III
ANIMAL

Tout vêtu de drap d’or frisé, contemplatif,
Besicles d’or armant son nez bourbon, il trône.
A l’entour se presse un cortège admiratif
Que fait trembler le feu soudain de son œil jaune.


Il est très sage, et rend justice sous un aulne
(Jadis Pallas en fit son conseil privatif) ;
Il a pour méditer l’arrêt, esprit actif,
Et pour l’exécuter griffes longues d’une aune.


Doux, poli, le hibou viendra vous prévenir
Quand l’heure sonnera que la Mort vous emporte ;
Et criera trois fois son nom à travers la porte.


Car il déchiffre sur les tombes l’avenir,
Rêvant la nuit devant les X philosophales
Des longs fémurs croisés en siestes triomphales.




BERCEUSE
DU MORT POUR S’ENDORMIR

Le grand portrait pendu au mur,
solaire sous sa tente obscure,


dans les plis du fantôme blanc
qui me couve hausse son front lent.


O que pâle est mon front lunaire
sous les étoiles septénaires !


Le portrait de mon front mural
a sucé tout mon sang qui râle.


Le vampire hume dans mon cou
et mes artères des airs fous,


cependant que les araignées
trottent de mes mains décharnées


avec leurs toiles de velours,
bagues où s’empêtrent mes doigts lourds.


Qui donc a caché sous ma glotte
un pipeau moisi de hulotte,


m’empêchant d’ouïr les navettes
tisser de mes cierges squelettes ?


A leur pointe des papillons
ont des élytres de grillons


et s’en vont voler sur les fleurs
de la tenture de pâleurs.


Leurs ailes jaunes sont des tuiles
dont on bat les cartes mobiles ;


et du plafond qui dort très calme,
du plafond plat tombent des lames…


Puissent mes os rester intacts
dans leur fourreau de chair compacte,


rester intacts jusques à l’heure
où se débat le corps qui meurt,


où la peau fait comme une vitre
transparente à l’âme, et se vitre


l’œil de méduse à tentacules…
Des poulpes noirs autour circulent,


faisant des ronds avec leurs mains
pour figurer les lendemains.


Le cierge hausse son cœur qui pleure
de clepsydre me comptant l’heure ;


cependant que des Absalons
indifférents des rideaux longs


larmoient les pieds mous dans le vague.
Voici qu’une petite vague


mousseuse aux oreilles de lièvre
ou d’escargot vient sur mes lèvres,


et que mes narines de vœux
ont respiré des pastels bleus.


De mes genoux que le poids gonfle
se dégrafent mes pesants ongles :


très doucement je me déplie
comme un habit dans mon grand lit,


dont on verrait flotter les manches
au vent des cloches des dimanches.


Les sonneurs de leurs bras très las
abattront des cloches des glas…


Je vois leurs cloches sous les nues
bâiller des langues inconnues…


Dans le ciel où le jour s’efface
Splendit voilée la Sainte-Face…



LA RÉGULARITÉ DE LA CHÂSSE

I

Châsse claire où s’endort mon amour chaste et cher,
Je m’abrite en ton ombre infinie et charmante,
Sur le sol des tombeaux où la terre est la chair…
Mais sur ton corps frileux tu ramènes ta mante.


Rêve ! rêve et repose ! Écoute, bruit berceur,
Voler vers le ciel vain les voix vagues des vierges.
Elles n’ont point filé le linceul de leur soeur.
Croissez, ô doigts de cire et blêmissants des cierges,


Main maigrie et maudite où menace la mort !
O Temps n’épanche plus l’urne des campanules
En gouttes lourdes. Hors de la flamme qui mord
Nait une nef noyée en des nuits noires, nulles ;


Puis les piliers polis poussent comme des pins,
Et les torchères sont des poings de parricides.
Et la flamme peureuse oscille aux vitraux peints
Qui lancent à la nuit leurs lames translucides…


L’orgue soupire et gronde en sa trompe d’airain
Des sons sinistres et sourds, des voix comme celles
Des morts roulés sans trêve au courant souterrain…
Des sylphes font chanter les clairs violoncelles.


C’est le bal de l’abîme où l’amour est sans fin ;
Et la danse vous noie en sa houleuse alcôve.
La bouche de la tombe encore ouverte a faim ;
Mais ma main mince mord la mer de moire mauve…


Puis l’engourdissement délicieux des soirs
Vient poser sur mon cou son bras fort ; et m’effleurent
Les lents vols sur les murs lourds des longs voiles noirs…
Seules les lampes d’or ouvrent leurs yeux qui pleurent.



II

Pris
dans l’eau calme de granit gris,
nous voguons sur la lagune dolente.
Notre gondole et ses feux d’or
dort
lente.


Dais
d’un ciel de cendre finlandais
où vont se perdant loin les mornes berges,
n’obscurcis plus, blêmes fanaux,
nos
cierges.


Nef
dont l’avant tombe à pic et bref,
abats tes mâts, tes voiles, noires trames ;
glisse sur les flots marcescents
sans
rames.


Puis
dans l’air froid comme un fond de puits
l’orgue nous berçant ouate sa fanfare.
Le vitrail nous montre, écusson,
son
phare.


Clair,
un vol d’esprits flotte dans l’air :
corps aériens transparents, blancs linges,
inquiétants regards dardés
des
sphinges.


Et
le criblant d’un jeu de palet,
fins disques, brillez au toit gris des limbes
mornes et des souvenirs feus,
bleus
nimbes…


La
gondole spectre que hala
la mort sous les ponts de pierre en ogive,
illuminant son bord brodé
dé-
rive.


Mis
tout droits dans le fond, endormis,
nous levons nos yeux morts aux architraves,
d’où les cloches nous versent leurs
pleurs
graves.




TAPISSERIES
D’après et pour Munthe.

I
La Peur

Roses de feu, blanches d’effroi,
Les trois Filles sur le mur froid
Regardent luire les grimoires ;
Et les spectres de leurs mémoires
Sont évoqués sur les parquets,
Avec l’ombre de doigts marqués
Aux murs de leurs chemises blanches,
Et de griffes comme des branches.


Le poêle noir frémit et mord
Des dents de sa tête de mort
Le silence qui rampe autour.
Le poêle noir, comme une tour
Prêtant secours à trois guerrières
Ouvre ses yeux de meurtrières.


Roses de feu, blanches d’effroi,
En longues chemises de cygnes,
Les trois Filles, sur le mur froid
Regardant grimacer les signes
Ouvrent, les bras d’effroi liés,
Leurs yeux comme des boucliers.



II
La Princesse Mandragore

De sa baguette d’or, la Fée
Parmi la forêt étouffée
Sous les plis des ombrages lourds
A conduit la Princesse pâle.
Et par son ordre, le velours
De la mousse à ses pieds d’opale
A mis des mules de carcans


Et sur sa robe des clinquants
Stillent des gouttes de rosée.
Et les champignons à ses pieds
Prosternent leur tête rasée.
Les lapins hors de leurs clapiers,
Les limaces, cendre d’un âtre
Pétri de boue et de limons,
Ont levés leurs fronts de démons
Vers la triomphante marâtre.


La Princesse reste debout
Comme un arbre où la sève bout,
La Princesse reste rigide ;
Et, passant sur son front algide,
Tous les ouragans des effrois
Lancent au ciel ses cheveux droits.



III
Au repaire des Géants

J’en ai vu trois, j’en ai vu six,
Des Géants monstrueux assis
Sur les talus et les glacis
Et sur les piédestaux de marbres,
Avec leurs gros bras raccourcis,
Et leurs barbes comme des arbres,
Et leurs cheveux flambant au vent
Sur l’immobile paravent
Des murailles monumentales. —
J’ai vu six Géants dans leurs stalles.


Et sous leurs sourcils broussailleux,
J’ai vu — j’ai vu luire leurs yeux
D’or comme l’or de deux essieux
Tournant sous un char funéraire.
Ce sont six vaches qu’on va traire,
Rocs au lac de leur lait passant,
Les six Géants, pieds dans le sang.
Leurs doigts maigres, comme des torches,
Brassent le sang qui les éteint ;
De leur sang noir leur corps se teint,
Et leurs jambes comme des porches.


Et sur le cou du Roi Géant
Grimace un crâne de néant.
Pas de tête sur ses épaules.
Ses poings, branchus comme des saules,
Sont bénissants et triomphants,
Cierges clairs au repaire sombre
Deux grandes ailes de Harfangs
Sur son cou cisaillent dans l’ombre.


Le Géant a planté son doigt
Dans un grand navire qui doit
Passer le lac de son empire.
Son doigt est le mât du navire.
Et des ours bruns courbent leurs dos
Sous leurs fourrures pour fardeaux,
Courbent leur échine de flamme.
La tempête en fait une lame
De scie ou des murs à créneaux
Ou des follets sur des fourneaux.
Ils rament sur l’eau bouillonnante,
Rythmant la danse frissonnante
Des bruns frisons de leurs toisons
Aux coups de fouet des horizons.


La Princesse pâle à la proue,
Les yeux aux dos de ses rameurs,
Voit tournoyer comme une roue
Un grand oiseau dans les rumeurs
Et les tonnerres du repaire.
Le grand oiseau vert au long cou
Tord ses ailes fortes, espère
Voler contre l’ouragan fou.
Dans le repaire un oiseau rôde,
Un grand pélican d’émeraude,
Toujours avec des efforts neufs.
Les vents mouvants en font des nœuds.


Impassibles parmi, très lentes,
Reines des épouvantements,
Voici ramper aux murs dormants
De grandes monères sanglantes.




L’HOMME A LA HACHE
D’après et pour P. Gauguin.

A l’horizon, par les brouillards,
Les tintamarres des hasards,
Vagues, nous armons nos démons
dans l’entre-deux sournois des monts.


Au rivage que nous fermons
Dome un géant sur les limons.
Nous rampons à ses pieds, lézards.
Lui, sur son char tel un César


Ou sur un piédestal de marbre,
Taille une barque en un tronc d’arbre
Pour debout dessus nous poursuivre


Jusqu’à la fin verte des lieues.
Du rivage ses bras de cuivre
Lèvent au ciel la hache bleue.



[poèmes contenus dans la pièce]
HALDERNABLOU

PROLOGUE
Avant l’aurore, dans la forêt triangulaire.
Le CHŒUR, dont la voix s’éloigne.

Sur la plainte des mandragores
Et la pitié des passiflores
Le lombric blanc des enterrements rentre en ses tanières.


Le sérail des faces de sable
Soumis au bois de nos sandales
Luit de l’or de toutes ses croix à nos paupières.


Le cuivre roux des feuilles mortes
Et la force des vieilles écorces
Sonne et bénit le glas très doux de nos retraites.


Rentrons : le jour bientôt se lève.
La cendre de la nuit achève
De fuir avec le sang coulant des sabliers.


Les cœurs perdent leur sang qui coule.
Le cerf-volant de nos cagoules
Suspend son spectre aux lointains comme des masques jaunes d’effraies.


Que le mort dorme avant l’aurore.
Que le mort dorme avant le premier pleur de la lumière.
Sur la plainte des mandragores
Et la pitié des passiflores
Le lombric blanc des enterrements rentre en ses tanières.



LE PASTEUR DES HIBOUX

Strophe Ire (Pavot)

La volute
Des incantations
S’exhale en fumée et fuit hors des sept trous de ma flûte.
Or frisé des hiboux ocellés, nations
Des solitaires roux méditant sur les troncs
Des ormes difformes et le cuivre lunaire des pierres,
A mon souffle fermez les cymbales de vos paupières
Et les bagues aux doigts de la nuit de l’or de vos yeux de tromblons.



Antistrophe 1re (Passiflore).

Double
A l’horizon la vision trouble
Des rideaux mous s’ouvrant des ailes des hiboux.
Cymbales
Aux trous ou aux clous des doigts de gloire,
Les tromblons de leurs yeux sur nous
Dans l’or ocellé de leur tête de ciboire.



Epode Ire (Drosera)

Il ocellera, le hibou,
Son biniou
Des éventails de pleurs mordorés de son cou.



Strophe II (Fougère).

La suédoise ouate à ses doigts bouche et lute
Les polyèdres des orbites de ma flûte.



Antistrophe II (Agaric).

La volute
Du cou du hibou
Blute
L’essaim
Du van des étincelles
Des yeux nyctalopes de ses ailes
Lourd et si bruissant de malchus d’assassins.



Epode II (Mandragore).

Il ocellera, le hibou,
Son biniou
Des éventails de pleurs mordorés de son cou.
Il ocellera, le hibou,
Son biniou
Aux volutes
Des polyèdres des orbites de ma flûte.





LA VIEILLE gardienne d’un Water-closet chante
d’une voix grinçante de cigale prisonnière :


La belle dit à l’amant :
Entrez, entrez, bergerette ;
Noire la langue muette,
Baiser de bouche qui ment ;
Et des morts dans la brouette.



SCÈNE III
LE CHŒUR

L’éclair allume sa lampe et l’éteint pour rire
Et l’enveloppe de son manteau de souris ;
Car devant Balthazar l’éclair fier vient d’écrire
En lettres de bave aux murailles du ciel gris !



SCÈNE VI
LE CHŒUR

Strophe :

La lune ombre de sang l’acier de son croissant.
Le stupre aux ongles tous deux nous marchons chassant
Devant nous les lampadaires en vol de grues
Par l’horizon tendu de noir des mortes rues.


Images de Saintes, vos paupières férues
Dans la chambre, de l’Acte à taire, applaudissant
Ironiques en clins éternels, noircissant
L’œil par l’étendue des rues parcourues…


Otez de devant notre ombre vos yeux de mur,
Comme d’un qu’on va piétiner rampant mobile
Le cheval des tramways révulse un nez obscur…


Les lampadaires luisent en angle passant
La lune ombre de sang l’acier de son croissant…
Stupre aux ongles, tous deux nous marchons par la ville.


Le Livre de l’Acte passé
Sur les rails de fer roule et râle.
Dormez indéfiniment, ô mains trépassées ;
Vous ne refermerez plus vos dents sépulcrales.



Antistrophe :

Là-bas fuit le regard des vieux crabes tourteaux,
Sur les ponts, sur le glas des cloches des bateaux.
Sur les toits perchent des oiseaux monumentaux.
Dos en angle des cercueils, mettez vos manteaux.


Mettez vos manteaux bleus et gris, toits centenaires.
Rhinolophes, au nez ferré d’argent, lunaires,
Voletez en signes de croix, noires monères,
Vol erratique des planètes septénaires.


Le livre m’a serré de ses pinces de fer
Mieux que les mortes mains n’avaient mordu ma chair.
O les lourds patins sur la glace vert enfer !


Il avait dit : Toujours ! — Jamais plus ! lui réponds-je.
— Et j’écrase la cervelle comme une éponge
Et la mémoire, dit le corbeau, bec de songe.


Sur les toits perchent des corbeaux monumentaux ;
Les toits sont des cercueils qu’ont cloués des marteaux
Au ciel lunaire.
Vent,
Ne va pas soulevant
Le toit violet, sur le mur blanc au couvent :
Amour défunt, béni par le héron missionnaire !



Épode :

Le Temps sous les pandanus sonne son cor.
Le petit vieillard rit et grimace encor,
Tombant sous l’hallali torve des cuivrares.
Les caméléons dans leurs glauques simarres
Sont des vrilles de vigne au-dessus des mares
Et du tombeau vert des amours trépassés.
Sabbatiques rosses,
Évêques renversés chevauchant leurs crosses,
Les caméléons volent aux cieux lassés.


Or flambe et luit et chevronne au ciel d’opale
Entre ses longs doigts d’épervier de mains pâles
Aux cieux lassés
Le Livre au vol de corbeaux de ses signes trépassés.




ÉPILOGUE
Dans la forêt triangulaire, après le crépuscule.
LE CHŒUR

(Sa voix, d’abord morte presque encore et qui mur-
mure, de plus en plus tonne éclatante.)


Les hauts chapeaux des noirs Yankees
Confèrent au ciel oublié
Les trois piliers du Sablier.


La sieste des longs fémurs croise
Ses blanches X philosophales.
La pointe de nos barbes s’effiloque en la rafale.


Que la boule de nos cagoules,
Rose reflet au sang qui coule
Cherche le mort, momie en l’or du crépuscule ;


Et les sabliers retournés
Sable en haut donnent au damné
La nuit entière avant les Juifs Errants par la nuit nulle.


Rempli le sablier d’albâtre,
Le cœur qui pleure ne peut battre.
Comme lui sous les ifs nos pieds d’ibis sur les marais.


Pleuvra la future lumière
Aux plombs de vitraux des forêts
Sur notre tâche de nécrophores coutumière.


Sur la plainte des mandragores
Et la pitié des passiflores
Le lombric blanc des enterrements sort de ses tanières.


(Le Chœur, QU’ON N’A JAMAIS VU, blanchit
le fond de son aube soufrée à ogives. Paraissant :)


Le lombric blanc des enterrements sort de ses tanières !




LES PARALIPOMÈNES

I

Pèlerin aux chemins célèbres
Dont des corps morts gardent les bords
Pour égrener de leurs doigts forts
Le chapelet de mes vertèbres,


Le ventouse bourdon de ma main de vélin
S’est fait chauve de ses racines de gorgone
Aux rocs roulés chus de mâchoire qui marmonne
De géant trucidé pour mon tapis félin.


Quintuple chapelet ma main de saule sonne
Damnation de ses feuilles d’espoir, couronne
De lépreux cliquetant du droit serpent câlin
Dormant au déroulement des routes de lin.


Le sable du sérail soumis à mes sandales
Tourne à mes yeux ses yeux de croix gyrant aux dalles.
Le cadran s’est fêlé de l’église au frappant


Double regard à la lune, en la florescence
Du halo de brouillard ainsi que l’on encense
Des lampadaires hauts, plates plumes de paon.


Chute des dés de fer au long des toits pliés
Des cloches bavant leur glas de mort pour la mienne.
Devantures des marchands de vin : oubliés
La conférence des falots rabelaisienne.


Le grand papillon noir afin qu’il n’appartienne
Aux masses de monnayeurs des chevaux liés
Ni de la corne des sabots en lourds piliers
Ricoche des pavés en glace aérienne.


Le vol s’est arrêté droit de la matité
De la noire cheminée au ciel ouaté.
Mais il me faut laisser des traces sur la terre


De la veuve sandale enchaînée à mon pied.
Des lampes, du ciel et du temps m’ont épié
Les inflexibles yeux rond nimbe au solitaire.


Je marche à l’horizon risiblement opaque
Au ricanement des cadrans. Et les bourdons
Ombres de pèlerins en file au ciel de laque
Frappent les gonds de l’horizon gardant ses dons.


La pluie est monotone en l’heure tombant : craque
Au plomb lourd de la pluie, ô Sablier qui vaque
Toujours, gonflant les épines des diodons.
Quand s’ouvrira le Jour qui s’épand en pardons


Irradiés au fond de mer ou de ciguës
Vers qui tournant au vent je vire mes mains nues
Priez : déjà la pluie et l’heure avec son pleur


M’engrillent pour la nuit et le sommeil sans rêve.
Priez que mes désirs dorment : et j’aurai l’heur
Que mon âme qui meurt veuille me faire trêve.


Versé le plat reflet des barbes dans l’eau moire
Des ifs vitraux au ciel s’intersèquent les plombs.
O visage si rond de la ville, les fonds
Qui dédaignent les bras plongeurs ont ta mémoire.


Ramant rapide sous les durs remous, la gloire
Se dessine fuyant des falots aux talons
Remontés du liquide à l’air les échelons,
Voici sur l’horizon se dresser la Tour noire.


Tombés plongent les clairs carreaux de deux prunelles,
Les doigts de la fenêtre oculaire infernaux
De l’orbite ont jeté deux larmes parallèles,


Et de douleur la Tour huhule en ses créneaux,
Cependant qu’à son front les aigrettes jumelles
Raides au ciel de laque arment deux sentinelles.




LES PROLÉGOMÈNES
DE CÉSAR-ANTECHRIST

I
Prose (Saint Pierre parle.)

Comme deux amants
La nuit bouche à bouche
Dispersent leur couche
De baisers déments ;
Tête du Ciboire,
Épanche en mon sein
Ton amour malsain.
Nomme-t-on ça croire ?
De mon Dieu jaloux,
Il n’est pas pour vous.


L’un me dit qu’il l’aime :
Âne du latin,
Il me cite même
Du saint Augustin ;
Plus ou moins notables
Épluchant des faits
Grattés sur les tables
Froides des cafés.
L’un me dit qu’il l’aime,
L’autre qu’il blasphème.


L’amant de son Dieu
A son nom qu’il jure
Dans sa bouche impure
En tout temps et lieu.
Dans un petit groupe
Le blasphémateur
Cite son auteur
Aux pages qu’il coupe.
Les blasphémateurs
Sont littérateurs.


Il me plaît répandre
Dans un lieu fermé
Comme au vent la cendre
Le sang de l’aimé.
Et j’aime qu’il rampe
Devant mon courroux ;
Sa langue de Lampe
Lèche mes genoux.
Dieu permet encore
Que je Le dévore.


Mais il ne veut pas
Que l’on s’évertue
En d’oisifs combats ;
Que l’on prostitue
L’amour éprouvé
A l’âme banale
Qui n’a même pas le
Chic du réprouvé.
Il s’offre à ma fête —
Pour que je Le prête ?


De mon Dieu jaloux
Dont l’on fait un thème,
Il n’est pas pour vous.
La mode est qu’on l’aime ;
On en fait un sport.
On le prend peut-être
Pour un beau décor…
Comme une fenêtre
Fermons sur ma croix
Sa porte de bois.




Prologue de Conclusion.

Du mur
Obscur exutoire
Des revenants des victoires
La mygale s’écrase aux faces soleils des tambours
Par la gloire et la mort de ses doigts noirs battoirs !


Le quadruple coin de la cloche s’accroche aux lointains
Tintant le glas lourd, gourd et sourd des prières d’étain.


L’enfant drapé de la pourpre et du sang du Christ mourant
Sur son front a les fleurs de la vierge couronne écran
Et la croix sur 1 épaule en militaire dans le rang.
Et Jean-Baptiste enfant va rose et nu sous le ciel bleu
Avec à ses pieds blancs des sandales couleur de feu ;
La peau du mouton bêlant vêt le prophète de Dieu.


On égorgea les fleurs sur la route des innocents.
Le barrissement des tambours fait envoler le sang
Que brouta la biche de Geneviève de Brabant.


Marchez aux reposoirs vers le calvaire et l’abattoir !
L’hermine rouge a brodé la peau de la terre noire,
Les hoquets des tambours tremblent sur le sable mouvant ;
Sous son armure de pavés, l’enfer guette rêvant.


Les Suisses diables chamarrés fourchus sous leurs habits
Lèvent le couperet de leur grand chapeau de rubis.


Les vents de mort tirent aux dés tous les décès de l’an
Par les cloches tric-trac au son du batail roulant,
Et le portail bénit de ses doigts unis les allants.


On a tendu toute la rue avec des linceuls blancs,
L’escarpolette des guirlandes haut s’en va volant.


Paix ! le sonneur avec ses deux cloches sonne le glas
Êgouttant les deux verres sur la terre à chaque pas,
Et sous son crâne rit l’heure qui a fui du cadran.
Il s’en va sonnant et tintant par le blanc de la place ;
Dans les deux mortiers du vieux voleur les pilons se glacent.


Malgré le nombril de midi où dort le coq sur le clocher
Sous le cristal de l’œil de l’oiseau couronné perché
Éditant ses pas à rebours furtif il les efface.


Du mur
Obscur exutoire
Du silence à rebours sort des revenants des victoires…
Par le tic-tac de gloire et de mort de ses doigts noirs battoirs
La mygale s’écrase aux faces des Tambours.



LE SABLIER

Suspends ton cœur aux trois piliers,
Suspends ton cœur les bras liés,
Suspends ton cœur, ton cœur qui pleure
Et qui se vide au cours de l’heure
Dans son reflet sur un marais.
Pends ton cœur aux piliers de grès.


Verse ton sang, cœur qui t’accointes
A ton reflet par vos deux pointes.


Les piliers noirs, les piliers froids
Serrent ton cœur de leurs trois doigts.
Pends ton cœur aux piliers de bois
Secs, durs, inflexibles tous trois.


Dans ton anneau noir, clair Saturne,
Verse la cendre de ton urne.


Pends ton cœur, aérostat, aux
Triples poteaux monumentaux.
Que tout ton lest vidé ruisselle :
Ton lourd fantôme est ta nacelle,
Ancrant ses doigts estropiés
Aux ongles nacrés de tes pieds.


VERSE TON AME QU’ON ÉTRANGLE
AUX TROIS VENTS FOUS DE TON TRIANGLE


Montre ton cœur au pilori
D’où s’épand sans trêve ton cri,
Ton pleur et ton cri solitaire
En fleuve éternel sur la terre.


Hausse tes bras noirs calcinés
Pour trop compter l’heure aux damnés.
Sur ton front transparent de corne
Satan a posé son tricorne.
Hausse tes bras infatigués
Comme des troncs d’arbre élagués.
Verse la sueur de ta face
Dans ton ombre où le temps s’efface ;


Verse la sueur de ton front
Qui sait l’heure où les corps mourront !


Et sur leur sang ineffaçable
Verse ton sable intarissable.
Ton corselet de guêpe fin
Sur leur sépulcre erre sans fin,
Sur leur blanc sépulcre que lave
La bave de ta froide lave.


Plante un gibet en trois endroits,
Un gibet aux piliers étroits,
Où l’on va pendre un cœur à vendre.
De ton cœur on jette la cendre,
De ton cœur qui verse la mort.


Le triple pal noirci le mord ;
Il mord ton cœur, ton cœur qui pleure
Et qui se vide au cours de l’heure
Au van des vents longtemps errés
Dans son reflet sur un marais.



