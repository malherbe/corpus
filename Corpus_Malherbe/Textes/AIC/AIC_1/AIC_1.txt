Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
AIC
AIC_1

Jean AICARD
1848-1921

Jeanne Darc
Le Rachat de la Tour
1866
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes d’analyse, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Jeanne Darc
Jean Aicard


https://fr.wikisource.org/wiki/Jeanne_d%E2%80%99Arc_:_le_rachat_de_la_tour


Jeanne Darc
Jean Aicard
http://gallica.bnf.fr/ark:/12148/bpt6k54710859/f5.image

Toulon
mpr. de E. Aurel
1866




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



LE MARTYRE

À M. J. MICHELET


I

Par de cruels enfants une femme suivie
Se traînait. En ses yeux la mort avec la vie,
Hagardes, s’éteignaient, se rallumaient toujours.
Les enfants la heurtaient au choc de leurs batailles ;
Elle, pour se venger, déchirait ses entrailles,
Folle, qui s’arrachait à son propre secours.


Or, la France a jadis souffert cette souffrance ;
L’Anglais la dépeçait longuement. Pauvre France !
Elle-même mordait ses bras, crevait son sein,
Et tandis que, tombée, elle rendait son râle,
Le roi dansait au bruit d’une chanson banale,
Froid complice de l’assassin.


Et parmi les enfants de la mère-patrie
Pas un qui se levât, fier, et l’âme attendrie,
Chassant le léopard vil qui la dévorait.
Sentant plus de pitié que ses frères dans l’âme,
Ne voyant que des cœurs efféminés, la femme
Se fait homme — et soudain Jeanne Darc apparaît.


Vous la connaissez tous, cette figure étrange,
Cette vierge domptant les vieux guerriers, — cet ange,
Qui, l’auréole au front, traverse, tout-puissant,
Les livides lueurs de l’orage des armes,
Et, faible enfant, parfois se prend à fondre en larmes
Devant tant d’horreur et de sang !


De la France vaincue elle est le bon génie,
Et quand, vainqueur lassé dont la tâche est finie,
Elle voudrait revoir sa chaumière et ses bois,
On la jette aux bourreaux en repoussant sa mère !…
Oh ! tandis qu’on la brûle aux longs cris de : sorcière !
Grand Dieu, que faites-vous ? et toi, peuple ? et vous, rois ?



II

Dans ces jours en deuil où la France
Courbait son front pâle, abattu,
Ils murmuraient : « Pleins d’espérance,
Longtemps nous avons combattu !
Toi, quel est ton espoir, ô femme
Dont un souffle briserait l’âme ? »
Elle dit : « Je veux un drapeau !
Je veux t’aimer, France, ma mère,
Et dans la mêlée en colère,
Que mon glaive dorme au fourreau ! »


Jeanne, merci ! — Comme une Idée,
Glaive au repos, bannière au vent,
Luis sur la France fécondée
Où n’est plus un anglais vivant !
Pour tant de victoires divines,
Que veux-tu ? — « Revoir mes collines ! »
Ô Jeanne, suprême soutien,
Ton peuple, formidable armée
À ta vue enthousiasmée,
Si tu disparais n’est plus rien !


Elle resta, tuant en elle
Les jeunes songes du bonheur.
France ! qu’elle était grande et belle,
L’enfant sans reproche et sans peur !
Le Seigneur jettera sans doute
Tous les paradis en sa route ?
Non, mais l’horreur, la trahison ;
Et sur la vierge qu’on insulte,
Après la guerre et son tumulte,
La solitude et la prison !


Le roi dort dans sa nonchalance ;
Tes chevaliers vont accourir ;
Tu ne peux, sous tant de souffrance,
Fille de Dieu, vivre et périr !
Ah ! ton peuple grandi se lève ;
Il va broyer ces murs !… vain rêve !
Roi, Chevaliers, Peuple, — tout dort.
À quoi bon te fier aux hommes ?
Tu ne sais quels ingrats nous sommes !
Ta délivrance, c’est la mort !


Un conseil de prêtres s’assemble ;
Les Anglais tiennent leur vainqueur.
La Pucelle s’avance et tremble
Timide, la main sur le cœur.
Meurtrissant son âme meurtrie :
« Il faut renier ta patrie,
Ton roi, criaient-ils, et ton Dieu ! ».
« Non ! » répond-elle, faible et forte,
Et du cachot passant la porte,
Sublime, elle se livre au feu !


Avec tous ses rayons, ta gloire
Ici nous apparaît, enfant !
Ce n’est point ta longue victoire,
Reims, ni le sacre triomphant ;
C’est de faire pâlir ces traîtres,
D’effrayer ces bourreaux, tes maîtres ;
D’avilir leur orgueil brutal ;
Par ta mort ta vie est complète !
C’est un triomphe, ta défaite !
Ton bûcher, c’est un piédestal !


Tout est consommé : le supplice
L’a prise à la face des cieux ;
Son grandiose sacrifice
S’efface des cœurs oublieux.
Avec son échafaud s’écroule
Le souvenir, — et de la foule
S’éteint la honte et le remord ;
Sur ces cendres, nulle statue,
Magnifique, ne perpétue
Cette existence et cette mort !


Mais Quelqu’un a veillé, qui laisse,
Lorsque le temps écrase tout,
Telle qu’une ombre vengeresse,
La Prison de Jeanne debout.
Voilà le monument, ô France !
Voilà le piédestal immense !
Et quand tu l’auras acheté,
Consolidant cette ruine,
Rouen, tombeau de l’héroïne,
Sera son immortalité !



III
LE RACHAT

À M. ERNEST MORIN.


Ô Maître, vous avez peint les époques sombres ;
Vous avez évoqué ces solennelles ombres,
Les Xaintrailles et les Dunois,
Et le réveil soudain de la France opprimée ;
Alors, grands et petits, foule enthousiasmée,
Frémissaient, comme votre voix.


Non ! tout n’est pas si mort en nos frêles poitrines,
Que rien n’y batte plus ; — sur les tristes ruines
Naissent des arbustes souvent ;
L’incendie est éteint ; le feu vit sous la cendre ;
Formidable, il surgit, lorsque Dieu fait descendre
Du haut du ciel un coup de vent !


Or ce vent a soufflé de votre âme profonde ;
Votre fécond labeur rajeunit un vieux monde !
Pour vaincre les efforts du temps,
Sans armes, vous avez levé votre bannière :
Du bûcher noir jaillit une blanche lumière ;
Un jour refait quatre cents ans.


Et maintenant, esprits qu’illumine l’Histoire
Nous n’avons pas le droit d’oublier cette gloire,
Indifférents à tant d’amour ;
Chacun doit expier la faute universelle ;
Pacifiques soldats de l’Idée immortelle,
À la France rendons sa Tour !


Domremy jette un cri grand de reconnaissance ;
La pensée, aigle altier, de l’humble bourg s’élance,
Comme autrefois l’ange sauveur,
Et Rouen l’applaudit ; puis ma ville natale ;
Reçois donc, ô Toulon, la strophe filiale
Que mon cœur dédie à ton cœur !


Les Tisseurs de Lyon brodent une oriflamme ;
Travaillons tous, allons ! femmes, pour une femme !
Jeunesse, pour la liberté !
Debout ! Donnons avec nos cœurs notre parole ;
Tressons pour la martyre une blanche auréole
Éclatante de vérité !


Un jour, — quand régnera la Paix sainte et propice,
Nous serons acclamés les fils de la justice,
Acclamés d’une seule voix !
Car les Peuples voudront abdiquer leurs misères
Devant tous les Sauveurs saignants sur les calvaires,
Sur les bûchers et sur les croix !



Toulon,
27 avril 1866.





