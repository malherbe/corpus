Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
VAL
VAL_6

Paul VALÉRY
1871-1945

PIÈCES DIVERSES
contenues dans le recueil : POÉSIES (1942)
1942
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

POÉSIES ET MÉLANGES
Paul Valéry


https://ebooks-bnr.com/valery-paul-poesie-et-melange/

Ce livre numérique est réalisé principalement d’après :
Valéry, Paul, Œuvres, Paris, NRF (Sagittaire), 1931. D’autres
éditions ont été consultées en vue de l’établissement du présent
texte.
(extrait du document édité par la bibliothèque numérique romande, 2016)

_________________________________________________________________
Édition de référence pour les corrections métriques :

POÉSIES
Paul Valéry

Gallimard
1942


┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



NEIGE

A Madame Jacqueline Pasteur Vallery-Radot


Quel silence, battu d’un simple bruit de bêche !…


Je m’éveille, attendu par cette neige fraîche
Qui me saisit au creux de ma chère chaleur.
Mes yeux trouvent un jour d’une dure pâleur
Et ma chair langoureuse a peur de l’innocence.
Oh ! combien de flocons, pendant ma douce absence,
Durent les sombres cieux perdre toute la nuit !
Quel pur désert tombé des ténèbres sans bruit
Vint effacer les traits de la terre enchantée
Sous cette ample candeur sourdement augmentée
Et la fondre en un lieu sans visage et sans voix,
Où le regard perdu relève quelques toits
Qui cachent leur trésor de vie accoutumée
À peine offrant le vœu d’une vague fumée.



SINISTRE

QUELLE heure cogne aux membres de la coque
Ce grand coup d’ombre où craque notre sort ?
Quelle puissance impalpable entre-choque
Dans nos agrès des ossements de mort ?


Sur l’avant nu, l’écroulement des trombes
Lave l’odeur de la vie et du vin :
La mer élève et recreuse des tombes,
La même eau creuse et comble le ravin.


Homme hideux, en qui le cœur chavire,
Ivrogne étrange égaré sur la mer
Dont la nausée attachée au navire
Arrache à l’âme un désir de l’enfer,


Homme total, je tremble et je calcule,
Cerveau trop clair, capable du moment
Où, dans un phénomène minuscule,
Le temps se brise ainsi qu’un instrument…


Maudit soit-il le porc qui t’a gréée,
Arche pourrie en qui grouille le lest !
Dans tes fonds noirs, toute chose créée
Bat ton bois mort en dérive vers l’Est…


L’abîme et moi formons une machine
Qui jongle avec des souvenirs épars :
Je vois ma mère et mes tasses de Chine,
La putain grasse au seuil fauve des bars ;


Je vois le Christ amarré sur la vergue !…
Il danse à mort, sombrant avec les siens ;
Son œil sanglant m’éclaire cet exergue :
UN GRAND NAVIRE A PÉRI CORPS ET BIENS !…



COLLOQUE
(pour deux flûtes)

A Francis Poulenc, qui a fait chanter ce colloque.


A

D’une Rose mourante
L’ennui penche vers nous ;
Tu n’es pas différente
Dans ton silence doux
De cette fleur mourante ;
Elle se meurt pour nous…
Tu me sembles pareille
À celle dont l’oreille
Était sur mes genoux…
À celle dont l’oreille
Ne m’écoutait jamais
Tu me sembles pareille
À l’autre que j’aimais :
Mais de celle ancienne,
Sa bouche était la mienne.



B

Que me compares-tu
Quelque rose fanée ?
L’amour n’a de vertu
Que fraîche et spontanée…
Mon regard dans le tien
Ne trouve que son bien :
Je m’y vois toute nue :
Mes yeux effaceront
Tes larmes qui seront
D’un souvenir venues…
Si ton désir naquit
Qu’il meure sur ma couche
Et sur mes lèvres qui
T’emporteront la bouche…




LA DISTRAITE

Daigne , Laure, au retour de la saison des pluies,
Présence parfumée, épaule qui t’appuies
Sur ma tendresse lente attentive à tes pas,
Laure, très beau regard qui ne regarde pas,
Daigne, tête aux grands yeux qui dans les cieux t’égares,
– 169 –Tandis qu’à pas rêveurs, tes pieds voués aux mares
Trempent aux clairs miroirs dans la boue arrondis,
Daigne, chère, écouter les choses que tu dis…



INSINUANT II

FOLLE et mauvaise
Comme une abeille
Ma lèvre baise
L’ardente oreille.


J’aime ton frêle
Étonnement
Où je ne mêle
Qu’un rien d’amant.


Quelle surprise…
Ton sang bourdonne.
C’est moi qui donne
Vie à la brise…


Dans tes cheveux
Tendre et méchante
Mon âme hante
Ce que je veux.



HEURE

L’HEURE me vient sourire et se faire sirène :
Tout s’éclaire d’un jour que jamais je ne vis :
Danseras-tu longtemps, Rayon, sur le parvis
De l’âme sombre et souveraine ?


Voici L’HEURE , la soif, la source et la sirène.


Pour toi, le passé brûle, HEURE qui m’assouvis ;
Enfin, splendeur du seul, ô biens que j’ai ravis,
J’aime ce que je suis : ma solitude est reine !
Mes plus secrets démons, librement asservis
Accomplissent dans l’or de l’air même où je vis
Une sagesse pure aux lucides avis :
Ma présence est toute sereine.
Voici l’HEURE , la soif, la source et la sirène,


Danseras-tu longtemps, rayon, sur le parvis
Du soir, devant l’œil noir de ma nuit souveraine ?



L’OISEAU CRUEL…

L’OISEAU cruel toute la nuit me tint
Au point aigu du délice d’entendre
Sa voix qu’adresse une fureur si tendre
Au ciel brûlant d’astres jusqu’au matin.


Tu perces l’âme et fixes le destin
De tel regard qui ne peut se reprendre ;
Tout ce qui fut tu le changes en cendre,
Ô voix trop haute, extase de l’instinct…


L’aube dans l’ombre ébauche le visage
D’un jour très beau qui déjà ne m’est rien :
Un jour de plus n’est qu’un vain paysage,


Qu’est-ce qu’un jour sans le visage tien ?
Non !… Vers la nuit mon âme retournée
Refuse l’aube et la jeune journée.



À L’AURORE…

À l’aurore, avant la chaleur,
La tendresse de la couleur
À peine éparse sur le monde,
Étonne et blesse la douleur.


Ô Nuit, que j’ai toute soufferte,
Souffrez ce sourire des cieux
Et cette immense fleur offerte
Sur le front d’un jour gracieux.


Grande offrande de tant de roses,
Le mal vous peut-il soutenir
Et voir rougissantes les choses
À leurs promesses revenir ?


J’ai vu se feindre tant de songes
Sur mes ténèbres sans sommeil
Que je range entre les mensonges
Même la force du soleil,


Et que je doute si j’accueille
Par le dégoût, par le désir,
Ce jour très jeune sur la feuille
Dont l’or vierge se peut saisir.



ÉQUINOXE
ÉLÉGIE



To look…




Je change… Qui me fuit ?… Ses feuilles immobiles
Accablent l’arbre que je vois…
Ses bras épais sont las de bercer mes sibylles :
Mon silence a perdu ses voix.


Mon âme, si son hymne était une fontaine
Qui chantait de toutes ses eaux,
N’est plus qu’une eau profonde où la pierre lointaine
Marque la tombe des oiseaux.


Au lit simple d’un sable aussi fin que la cendre
Dorment les pas que j’ai perdus,
Et je me sens vivant sous les ombres descendre
Par leurs vestiges confondus.


Je perds distinctement Psyché la somnambule
Dans les voiles trop purs de l’eau
Dont le calme et le temps se troublent d’une bulle
Qui se défait de ce tombeau.


À soi-même, peut-être, Elle parle et pardonne,
Mais cédant à ses yeux fermés,
Elle me fuit fidèle, et, tendre, m’abandonne
À mes destins inanimés.


Elle me laisse au cœur sa perte inexpliquée,
Et ce cœur qui bat sans espoir
Dispute à Perséphone Eurydice piquée
Au sein pur par le serpent noir…


Sombre et mourant témoin de nos tendres annales,
Ô soleil, comme notre amour,
L’invincible douceur des plages infernales
T’appelle aux rives sans retour.


Automne, transparence ! ô solitude accrue
De tristesse et de liberté !
Toute chose m’est claire à peine disparue ;
Ce qui n’est plus se fait clarté.


Tandis que je m’attache à mon regard de pierre
Dans le fixe et le dur « Pourquoi ? »,
Un noir frémissement, l’ombre d’une paupière
Palpite entre moi-même et moi…


Ô quelle éternité d’absence spontanée
Vient tout à coup de s’abréger ?…
Une feuille qui tombe a divisé l’année
De son événement léger.


Vers moi, restes ardents, feuilles faibles et sèches,
Roulez votre frêle rumeur,
Et toi, pâle Soleil, de tes dernières flèches,
Perce-moi ce temps qui se meurt…


Oui, je m’éveille enfin, saisi d’un vent d’automne
Qui soulève un vol rouge et triste ;
Tant de pourpre panique aux trombes d’or m’étonne
Que je m’irrite et que j’existe !



LA CARESSE

Mes chaudes mains, baigne-les
Dans les tiennes… Rien ne calme
Comme d’amour ondulés
Les passages d’une palme.


Tout familiers qu’ils me sont,
Tes anneaux à longues pierres
Se fondent dans le frisson
Qui fait clore les paupières


Et le mal s’étale, tant,
Comme une dalle est polie,
Une caresse l’étend
Jusqu’à la mélancolie.



CHANSON À PART

Q UE fais-tu ? De tout.
Que vaux-tu ? Ne sais,
Présages, essais,
Puissance et dégoût…
Que vaux-tu ? Ne sais…
Que veux-tu ? Rien, mais tout.


Que sais-tu ? L’ennui.
Que peux-tu ? Songer.
Songer pour changer
Chaque jour en nuit.
Que sais-tu ? Songer
Pour changer d’ennui.


Que veux-tu ? Mon bien.
Que dois-tu ? Savoir,
Prévoir et pouvoir
Qui ne sert de rien.
Que crains-tu ? Vouloir.
Qui es-tu ? Mais rien !


Où vas-tu ? À mort.
Qu’y faire ? Finir,
Ne plus revenir
Au coquin de sort.
Où vas-tu ? Finir.
Que faire ? Le mort.



LE PHILOSOPHE ET LA JEUNE PARQUE

Cette sorte de Fable fut écrite en guise de Préface 

au Commentaire de la "Jeune Parque" par Alain.


La Jeune Parque, un jour, trouva son Philosophe :


« Ah, dit-elle, de quelle étoffe
Je saurai donc mon être fait…
À plus d’un je produis l’effet
D’une personne tout obscure ;
Chaque mortel qui n’a point cure
De songer ni d’approfondir,
Au seul nom que je porte a tôt fait de bondir.
Quand ce n’est la pitié, j’excite la colère,
Et parmi les meilleurs esprits,
S’il est quelqu’un qui me tolère,
Le reste tient qu’il s’est mépris.
Ces gens disent qu’il faut qu’une muse ne cause
Non plus de peines qu’une rose !
Qui la respire a purement plaisir.
Mais les amours sont les plus précieuses
Qu’un long labeur de l’âme et du désir
Mène à leurs fins délicieuses.
Aux cœurs profonds ne suffit point
D’un regard, qu’un baiser rejoint,
Pour qu’on vole au plus vif d’une brève aventure…
Non !… L’objet vraiment cher s’orne de vos tourments,
Vos yeux en pleurs lui voient des diamants,
L’amère nuit en fait la plus tendre peinture.
C’est pourquoi je me garde et mes secrets charmants.
Mon cœur veut qu’on me force, et vous refuse, Amants
Que rebutent les nœuds de ma belle ceinture.
Mon Père l’a prescrit : j’appartiens à l’effort.
Mes ténèbres me font maîtresse de mon sort,
Et ne livrent enfin qu’à l’heureux petit nombre
Cette innocente MOI que fait frémir son ombre
Cependant que l’Amour ébranle ses genoux.
CERTES, d’un grand désir je fus l’œuvre anxieuse…
Mais je ne suis en moi pas plus mystérieuse
Que le plus simple d’entre vous…
Mortels, vous êtes chair, souvenance, présage ;
Vous fûtes ; vous serez ; vous portez tel visage :
Vous êtes tout ; vous n’êtes rien,
Supports du monde et roseaux que l’air brise,
Vous VIVEZ… Quelle surprise !…
Un mystère est tout votre bien,
Et cet arcane en vous s’étonnerait du mien ?


Que seriez-vous, si vous n’étiez mystère ?
Un peu de songe sur la terre,
Un peu d’amour, de faim, de soif, qui font des pas
Dont aucun ne fuit le trépas,
Et vous partageriez le pur destin des bêtes
Si les Dieux n’eussent mis, comme un puissant ressort,
Au plus intime de vos têtes,
Le grand don de ne rien comprendre à votre sort.
« Qui suis-je ? » dit au jour le vivant qui s’éveille
Et que redresse le soleil.
« Où vais-je ? » fait l’esprit qu’immole le sommeil,
Quand la nuit le recueille en sa propre merveille.
Le plus habile est piqué de l’abeille,
Dans l’âme du moindre homme un serpent se remord ;
Un sot même est orné d’énigmes par la mort
Qui le pare et le drape en personnage grave,
Glacé d’un tel secret qu’il en demeure esclave.
ALLEZ !… Que tout fût clair, tout vous semblerait vain !
Votre ennui peuplerait un univers sans ombre
D’une impassible vie aux âmes sans levain.
Mais quelque inquiétude est un présent divin.
L’espoir qui dans vos yeux brille sur un seuil sombre
Ne se repose pas sur un monde trop sûr ;
De toutes vos grandeurs le principe est obscur.
Les plus profonds humains, incompris de soi-mêmes,
D’une certaine nuit tirent des biens suprêmes
Et les très purs objets de leurs nobles amours.
Un trésor ténébreux fait l’éclat de vos jours :
Un silence est la source étrange des poèmes.
Connaissez donc en vous le fond de mon discours :
C’est de vous que j’ai pris l’ombre qui vous éprouve.
Qui s’égare en soi-même aussitôt me retrouve.
Dans l’obscur de la vie où se perd le regard,
Le temps travaille, la mort couve,
Une Parque y songe à l’écart.
C’est MOI… Tentez d’aimer cette jeune rebelle :
« Je suis noire, mais je suis belle »
Comme chante l’Amante, au Cantique du Roi,
Et si j’inspire quelque effroi,
Poème que je suis, à qui ne peut me suivre,
Quoi de plus prompt que de fermer un livre ?


C’est ainsi que l’on se délivre
De ces écrits si clairs qu’on n’y trouve que soi. »



