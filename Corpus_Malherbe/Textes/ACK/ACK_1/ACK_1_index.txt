ACK
———
ACK_1

Louise-Victorine ACKERMANN
1813-1890

════════════════════════════════════════════
PREMIÈRES POÉSIES

1862

588 vers

─ poème	ACK1	I. Adieux à la Poésie
─ poème	ACK2	II. Élan mystique
─ poème	ACK3	III. Aux Femmes
─ poème	ACK4	IV. Le Départ
─ poème	ACK5	V. À une Artiste
─ poème	ACK6	VI. Renoncement
─ poème	ACK7	VII. La Belle au Bois dormant
─ poème	ACK8	VIII. La Jeunesse
─ poème	ACK9	IX. In Memoriam
─ poème	ACK10	X. Le Fantôme
─ poème	ACK11	XI. La Lyre d’Orphée
─ poème	ACK12	XII. A Alfred de Musset
─ poème	ACK13	XIII. Deux vers D’Alcée
─ poème	ACK14	XIV. La Lampe D’Héro
─ poème	ACK15	XV. Pygmalion
─ poème	ACK16	XVI. L’Hyménée et l’Amour
─ poème	ACK17	XVII. Endymion
─ poème	ACK18	XVIII. Hébé
─ poème	ACK19	XIX. Un autre Cœur
─ poème	ACK20	XX. La Coupe du Roi de Thulé
─ poème	ACK21	XXI. Daphné
─ poème	ACK22	XXII. L’Abeille
