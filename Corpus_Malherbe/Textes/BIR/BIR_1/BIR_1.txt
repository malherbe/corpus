Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
BIR
BIR_1

Hercule BIRAT
1796-1872

CHANT COMMUNISTE
PAR UN HOMME QUI NE L’EST GUÈRE
1849
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

CHANT COMMUNISTE PAR UN HOMME QUI NE L’EST GUÈRE
Hercule BIRAT


https://gallica.bnf.fr/ark:/12148/bpt6k5726482b?rk=21459;2


CHANT COMMUNISTE PAR UN HOMME QUI NE L’EST GUÈRE

Narbonne
IMPRIMERIE DE CAILLARD
1849




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



CHANT COMMUNISTE

I

Celui dont le précurseur
Fut Saint Jean-Baptiste,
Notre divin Rédempteur,
Était communiste.
Qui l’a dit ?… Son testament.
Ce livre est le rudiment
D’une république
Bien démocratique.



II

Que répondit Samuel
( La Bible l’atteste )
Au sot peuple d’Israël ?
» Un roi c’est la peste.
» Vous voulez avoir des rois,
» Vous vous en mordrez les doigts
» Une république
» C’est la forme unique.



III

Dans nos dangereux hasards
Saint Paul, je m’en flatte,
Bénira nos étendards
Couleur écarlate :
Aux turcarets de son temps
Il montrait de longues dents ;
A la république
Fera-t-il la nique ?



IV

Où sont les gens comme il faut,
Parmi les Apôtres ?
Saint Joseph tint le rabot,
Il sera des nôtres.
Je vois au rôle des Saints
Un noble pour cent vilains ;
Notre république
Aura leur pratique.



V

Au courroux de l’Éternel
Serions-nous en butte ?
Le poète aimé du Ciel,
Qui chanta la chute
De nos coupables aïeux,
Fut l’avocat chaleureux
D’une république
Fort démagogique.



VI

Des plats zélateurs des rois.
La huaille impie
A beau crier sur les toits :
Chimère, utopie ! ! !
Quand on a pour soi Platon,
Ergoteur du premier bon,
Une république
Peut sembler logique.



VII

Tite-Live, auteur latin,
Est sobre de craques ;
Eh bien, dans son vieux bouquin,
Je lis que les Gracques
Voulaient que tout plébéien
Eût sa case et son lopin
Dans la république.
O vertu civique !



VIII

Voltaire mit aux abois
La gent à soutane ;
Mais il fut l’ami des rois,
Et je le condamne.
Nous joûrons ses deux Brutus,
Son Tancrède, et rien de plus.
Pour la république
Le reste est morbique.



IX

Jean-Jacques le Genevois
Commit bien des fautes
Soit, mais il portait aux rois
De fameuses bottes
Et ses immortels écrits
Préparèrent les esprits
A la république,
Sur le sol gallique.



X

A la selle il accoucha
De feu Robespierre.
Nul autant n’effaroucha
Les grands de la terre ;
Doux chez lui comme un mouton,
Il luttait en vrai démon
Pour la république
La plus excentrique.



XI

Figurons sur ce tableau,
Ceint d’une auréole,
Le pourvoyeur du bourreau,
Du peuple l’idole.
Pouvant tout, ce bon Marat,
Creva sans un assignat
De la république ;
Fait très-authentique.



XII

Quand on vante Washington,
Voyez-vous, j’enrage !
Il ferma les clubs, dit-on,
Ce fut grand dommage.
Américains opulents,
De sordides débitants,
Votre république
N’est qu’une boutique !



XIII

Notre programme échoûra !
Disait Lafayette ;
Bientôt il tapissera
La sale lunette.
Louis-Philippe est bien fin ;
Il nous fait, le Pèlerin !
Une république
Toute monarchique.



XIV

Le suffrage universel
Est notre conquête.
Au peuple faisons appel,
Liberté complète.
Son vœu sera notre loi
S’il adopte, au lieu d’un roi,
Notre république ;
Autrement, bernique !



XV

De par Louis Blanc, Raspail,
Huber, Caussidière,
Nous aurons droit au travail.
Nargue la misère !
Aux riches, frères, amis,
Nous ferons voir du pays.
Notre république
Leur vaut la colique.



XVI

On nous promit un milliard,
Et, tout nous l’assure.
Il viendra quoique un peu tard,
Mais avec usure.
Ôter aux pécunieux
Pour doter les paresseux,
De la république
C’est là la tactique.



XVII

Que Thémis, pleine d’égards
Pour ceux de la veille,
Aux méfaits des montagnards
Ferme son oreille ;
Je l’admets, je le comprends :
Mais ne pas sangler les blancs,
Sous la république,
Est-ce politique ?



XVIII

Voici venir le bon temps,
Forçats, très-chers frères !
Vous aurez des remplaçants,
Quittez les galères !
Quel est donc le vrai voleur,
Si ce n’est le possesseur ?
Sous la république,
Avoir est inique.



XIX

Dans vos infects cabanons
Étiez-vous aux noces ?
Moi, dans d’humides donjons
J’ai pourri mes chausses.
Les forts succèdent aux fins ;
Bombance aux républicains !
Sous la république
N’est-ce pas topique ?



XX

Comme à des Sancho-Panças
Que la faim dévore,
On veut nous souffler les plats…
Point de Tarti-fuore ! !
Lamartine est ce docteur,
J’ai sa baguette en horreur-,
Notre république
Deviendrait étique.



XXI

Dans le déduit amoureux,
Moi, je suis bon drille ;
Et j’ai, par-dessus les yeux,
Du pâté d’anguille ;
Si j’avais quelque crédit,
Au code il serait écrit :
Sous la république
La femme est publique.



XXII

Vous aurez affaire à nous,
Durs célibataires !
Vous, dont Jésus est l’époux,
Vous deviendrez mères !
Des pensions, des rubans
A qui pondra plus d’enfants !
Quelle république
Ample et prolifique !



XXIII

Qui veut la fin, aux moyens
Serait-il contraire ?
Vous, bâtards-adultérins,
Et vous, filles-mères,
L’État vous adoptera,
L’État vous honorera :
Sous la république
La morale abdique.



XXIV

La science est un fatras,
Vidons les écoles !
Et des blancs, ces scélérats,
Remplissons les geôles !
Le bonnet est de rigueur ;
A la carmagnole, honneur !
Sous la république
Ni frac, ni tunique…



XXV

De stabals et de noëls,
De longues antiennes,
De versets sempiternels
Les heures sont pleines
Cela bientôt finira ;
Du sublime ça-ira,
A la république,
Suffit le cantique.



XXVI

Mon fils a prénom Paulet ;
Ma fille, Charlotte.
L’un s’appellera navet.
Et l’autre carotte.
Mai deviendra prairial ;
Avril sera germinal.
De la République
Suivons la rubrique.



XXVII

mot de germinal
Je vous vois sourire ;
Branche d’amandier ou pal
Il veut aussi dire.
Arrière les calotins !
Gare à vous les muscadins
Sous la république
Vous sentez la trique.



XXVIII

Proudhon vient de passer Dieu,
( L’autre a sa retraite )
Il régira de haut lieu
Tourbillon, planète.
Le soleil à l’occident
Se lèvera rayonnant
Sur la république :
Ce sera comique.



XXIX

De fou, de tête à l’envers,
Partout on le taxe ;
Et pourtant de l’univers
Il déplace l’axe.
Nous ne greloterons plus,
Nous cuirons dans notre jus,
Si la république
Se trouve au tropique.



XXX

Chauffés au plus haut degré,
Blancs ou patriotes,
Nous serons, bon gré mai gré,
Tous des sans-culottes.
Et ma maîtresse Goton
Sautera sans cotillon
Pour la république,
Sans être impudique.



XXXI

Trafiquant, gratte-papier,
Mignon à gant jaune,
Reprends la plume et l’encrier,
Le lorgnon ou l’aune ;
Sobrier, ce grand citoyen,
Réserve à ceux qui n’ont rien,
Dans la république,
Le képy civique.



XXXII

Nous marcherons sur tes pas,
Blanqui, forte tête !
Il pleuvra des cervelas,
Cabet, vieille bête !
Quand nous quitterons Paris,
Ses lorettes et ses ris,
Pour ta république
Maigre et famélique.



XXXIII

De ce bon Pierre Leroux
Vivent les agapes !
Avec du vin à cinq sous
Et veau froid sur nappes.
De Sparte le noir brouet
Ne sera jamais mon fait :
A la république
Il faut du tonique.



XXXIV

De Danton, maître Ledru
Affecte le rôle ;
Je le voudrais moins ventru.
Sa vive parole
De l’an deux a le parfum ;
Mais a-t-il, du fier tribun
De la république
La pose athlétique ?



XXXV

Point de consul, s’il vous plaît,
Président, exarque ;
Tout cela c’est blanc bonnet
Avec un monarque.
Un chicot tient bien souvent
Plus que la meilleure dent.
Pour la république
Triste viatique !



XXXVI

Loin bien loin Napoléon…,
O France niaise !
Avec liberté ce nom
Est en Antithèse.
Pas plus lui que Cavaignac,
Qui mit les rouges à sac !…
Sous la république
Fi de cette clique !



XXXVII

De nos prétendus héros
Craignons l’incivisme ;
Vaincus, à l’œuvre, bourreaux !
Vainqueurs, l’ostracisme !
Pour le salut de l’État
C’était vertu d’être ingrat,
Dans la république
D’Athènes l’antique.



XXXVIII

Qu’une vierge au fier regard
Et la gorge nue,
Empruntée aux Lupanar,
Charme notre vue !
Que ses robustes appas
Étonnent les fiers-à-bras !
Car la république
Tient en main la pique.



XXXIX

Tout ce qui date des rois
Hâtons-nous d’abattre ;
Que l’emblème soit de bois,
De bronze ou de plâtre !
Et que la saine raison,
Dans le plus petit canton
De la république,
Ait sa basilique !



XL

Niveleurs, des vieux, abus
Ne laissons point trace !
Le mien, le tien ne sont plus ;
Tout est à la masse.
Pour en finir au plutôt,
La lanterne ou l’échafaud :
De la république
C’est le spécifique !




