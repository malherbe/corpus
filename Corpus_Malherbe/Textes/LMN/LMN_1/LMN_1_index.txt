LMN
———
LMN_1

Louis LEMERCIER DE NEUVILLE
1830-1918

════════════════════════════════════════════
VERS DE VASE

AMORCES POÉTIQUES OFFERTES AUX PÊCHEURS MALHEUREUX
1906

1783 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ POÉSIES ET MONOLOGUES
	─────────────────────────────────────────────────────
	▫ I CALENDRIER DU PÉCHEUR
		─ poème	LMN1	I. JANVIER
		─ poème	LMN2	II. FÉVRIER
		─ poème	LMN3	III. MARS
		─ poème	LMN4	IV. AVRIL
		─ poème	LMN5	V. MAI
		─ poème	LMN6	VI. JUIN
		─ poème	LMN7	VII. JUILLET
		─ poème	LMN8	VIII. AOÛT
		─ poème	LMN9	IX. SEPTEMBRE
		─ poème	LMN10	X. OCTOBRE
		─ poème	LMN11	XI. NOVEMBRE
		─ poème	LMN12	XII. DÉCEMBRE
	─ poème	LMN13	LE PÉCHEUR A LA LIGNE
	─ poème	LMN14	NULLA DIES SINE LINEA
	─ poème	LMN15	INCORRIGIBLE
	─ poème	LMN16	L’ABLETTE
	─ poème	LMN17	LE GOUJON
	─ poème	LMN18	LA LÉGENDE DE SAINT-PIERRE
	─ poème	LMN19	LES CARPES
	─ poème	LMN20	LES TROIS PÊCHEUSES
	─ poème	LMN21	LA PÊCHE AU BARBILLON
	─ poème	LMN22	LE GARDE-PÊCHE ET LE MINISTRE
	─ poème	LMN23	ÇA MORD !
	─ poème	LMN24	MARAUDE NOCTURNE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES DEUX PÊCHEURS
	─ pièce	LMN25	Les deux Pêcheurs - SCÈNE SUR SEINE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ CHANSONS DE PÊCHE
	─ poème	LMN26	LA MARSEILLAISE DU PÊCHEUR
	─ poème	LMN27	LE VER DE VASE
	─ poème	LMN28	LA CHANSON DE LA BRÈME
	─ poème	LMN29	UNE JOURNÉE DE PÊCHE
	─ poème	LMN30	LA LÉGENDE DES PETITS POISSONS
