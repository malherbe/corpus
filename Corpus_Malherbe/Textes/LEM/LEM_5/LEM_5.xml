<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LA CHAINE D’OR</title>
				<title type="medium">Édition électronique</title>
				<author key="LEM">
					<name>
						<forename>Pamphile</forename>
						<surname>LE MAY</surname>
					</name>
					<date from="1837" to="1918">1837-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg Canada)</resp>
					<name id="RV">
						<forename>Rénald</forename>
						<surname>Lévesque</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LEM_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA CHAINE D’OR</title>
						<author>LÉON PAMPHILE LEMAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg Canada</publisher>
						<idno type="URL">https://gutenberg.ca/ebooks/lemay-chaine/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA CHAINE D’OR</title>
								<author>LÉON PAMPHILE LEMAY</author>
								<idno type="URL">https://archive.org/details/cihm_08650/</idno>
								<imprint>
									<pubPlace>QUÉBEC</pubPlace>
									<publisher>TYPOGRAPHIE DE C. DARVEAU</publisher>
									<date when="1879">1879</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p/>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) et les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="LEM156">
				<head type="main">LA CHAÎNE D’OR</head>
				<div type="section" n="1">
				<lg n="1">
					<l n="1" num="1.1">Ce que je conte est vrai. Ce n’est pas une histoire</l>
					<l n="2" num="1.2">Comme on en fait souvent et qu’on doit ne pas croire.</l>
					<l n="3" num="1.3">Au reste en ces temps durs il surgit bien des maux.</l>
					<l n="4" num="1.4">Tant de bras vigoureux demeurent en repos</l>
					<l n="5" num="1.5">Et qui travailleraient s’ils avaient de l’ouvrage !</l>
					<l n="6" num="1.6">Oui, l’on souffre partout. Puis il faut du courage</l>
					<l n="7" num="1.7">Pour redire les maux de l’humble pauvreté,</l>
					<l n="8" num="1.8">Comme, pour les guérir, il faut la charité,</l>
					<l n="9" num="1.9">La charité du Christ qui va courbant la tête,</l>
					<l n="10" num="1.10">Et que rien ici-bas ne rebute ou n’arrête.</l>
					<l n="11" num="1.11">J’étais donc, l’autre jour, au bureau. J’écrivais.</l>
					<l n="12" num="1.12">Et, le front dans la main, écrivant, je rêvais</l>
					<l n="13" num="1.13">Au passé qui n’est plus, au présent qui s’envole,</l>
					<l n="14" num="1.14">A l’avenir, ce grand problème qui désole</l>
					<l n="15" num="1.15">Ceux qui n’aiment pas Dieu, ceux qui n’ont pas la foi.</l>
					<l n="16" num="1.16">Jean Dumanoir entra. Marchant tout droit à moi :</l>
				</lg>
				<lg n="2">
					<l part="I" n="17" num="2.1"> — Comment te portes-tu ? dit-il.</l>
				</lg>
				<lg n="3">
					<l part="F" n="17">Et sa main blanche</l>
					<l n="18" num="3.1">Serre la mienne alors dans une étreinte franche.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">— Dieu merci, répondis-je, on se porte assez bien ;</l>
					<l n="20" num="4.2">Mais l’on vieillit toujours et l’on n’y gagne rien.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1">Il sourit d’un air triste en approchant un siège.</l>
				</lg>
				<lg n="6">
					<l n="22" num="6.1">Nous nous étions connus autrefois au collège,</l>
					<l n="23" num="6.2">Et nous étions amis. Alors assez souvent,</l>
					<l n="24" num="6.3">Dans les beaux jours d’automne, à l’époque où le vent</l>
					<l n="25" num="6.4">Avec un bruit plaintif traîne les feuilles mortes,</l>
					<l n="26" num="6.5">Nous marchions, en causant choses de toutes sortes,</l>
					<l n="27" num="6.6">Sous les ormes touffus qui protègent la cour.</l>
					<l n="28" num="6.7">Mais nous aimions surtout à parler de l’amour,</l>
					<l n="29" num="6.8">Car il était sensible, et moi, j’étais poète.</l>
					<l n="30" num="6.9">Nous perdîmes ainsi des jours que je regrette,</l>
					<l n="31" num="6.10">Je l’avoue à cette heure où je suis sans orgueil.</l>
					<l n="32" num="6.11">Si c’était à refaire… On est loin de l’écueil,</l>
					<l n="33" num="6.12">Disons qu’on ferait mieux : il est aisé de dire.</l>
				</lg>
				<lg n="7">
					<l n="34" num="7.1">Ainsi l’on oubliait Mélibée et Tytire</l>
					<l n="35" num="7.2">Pour songer au village où l’on avait quitté —</l>
					<l n="36" num="7.3">Dans les pleurs, pensait-on — quelque jeune beauté.</l>
					<l n="37" num="7.4">Jean rêvait une femme adorable et fidèle,</l>
					<l n="38" num="7.5">Belle comme Didon, amoureuse comme elle —</l>
					<l n="39" num="7.6">Un peu moins peut-être — et le plus beau des séjours,</l>
					<l n="40" num="7.7">Un séjour dans les champs pour y filer ses jours.</l>
					<l n="41" num="7.8">Je rêvais aussi moi de semblables délices ;</l>
					<l n="42" num="7.9">Je rends grâces aux cieux qui me furent propices.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Jean quitta de bonne heure Horace et Lucien,</l>
					<l n="44" num="8.2">Et le vieux séminaire où plus d’un doux lien,</l>
					<l n="45" num="8.3">Comme un charme inconnu, nous ramène sans cesse.</l>
					<l n="46" num="8.4">Après un long repos il entra dans la presse</l>
					<l n="47" num="8.5">Pour rédiger l’annonce et l’humble fait divers.</l>
					<l n="48" num="8.6">Ensuite il fut commis, puis, marchand. Les revers,</l>
					<l n="49" num="8.7">Qui ne sont épargnés souvent qu’à la sottise,</l>
					<l n="50" num="8.8">L’atteignirent bientôt. Ce fut une surprise</l>
					<l n="51" num="8.9">Pour les riches prêteurs qui perdaient leur argent ;</l>
					<l n="52" num="8.10">Ce fut pour lui la honte ! Il reprit indigent,</l>
					<l n="53" num="8.11">Pour nourrir sa famille, un emploi que j’ignore.</l>
				</lg>
				<lg n="9">
					<l n="54" num="9.1">Mais je reviens au fait. Si je digresse encore</l>
					<l n="55" num="9.2">Sois indulgent, lecteur, et ne murmure pas.</l>
					<l n="56" num="9.3">Mon récit n’est pas long, mais il est triste, hélas !</l>
					<l part="I" n="57" num="9.4">Jean me dit :</l>
				</lg>
				<lg n="10">
					<l part="F" n="57"> — Le Seigneur t’a fait digne d’envie :</l>
					<l n="58" num="10.1">Un emploi magnifique, et pour toute la vie !</l>
					<l n="59" num="10.2">Des livres ! ces amis aux cœurs toujours ouverts</l>
					<l n="60" num="10.3">Qui nous font oublier que le monde est pervers.</l>
					<l n="61" num="10.4">De l’argent ! et jamais cette peur qui fend l’âme,</l>
					<l n="62" num="10.5">De voir mourir de faim ses enfants et sa femme !</l>
					<l n="63" num="10.6">Non, tu ne fus pas, toi, marqué d’un sceau fatal !…</l>
				</lg>
				<lg n="11">
					<l n="64" num="11.1">Il s’animait ; son œil prit l’éclat du métal.</l>
				</lg>
				<lg n="12">
					<l n="65" num="12.1">— Es-tu donc malheureux, Jean Dumanoir, lui dis-je ?</l>
				</lg>
				<lg n="13">
					<l n="66" num="13.1">— Moi ? bah ! laissons cela : voilà que je t’afflige</l>
					<l n="67" num="13.2">Voulant t’être agréable et te féliciter…</l>
					<l n="68" num="13.3">Mais on voit tant de maux qu’on peut bien s’irriter.</l>
				</lg>
				<lg n="14">
					<l n="69" num="14.1">— S’irriter ? allons-donc ! est-ce là le remède ?</l>
				</lg>
				<lg n="15">
					<l n="70" num="15.1">— Non ! on courbe le front, on prie, on intercède,</l>
					<l n="71" num="15.2">On demande du bois et du pain s’il vous plaît,</l>
					<l n="72" num="15.3">Et l’on baise la main qui nous donne un soufflet !…</l>
					<l n="73" num="15.4">On commit bien cela, ça s’enseigne à l’école.</l>
				</lg>
				<lg n="16">
					<l n="74" num="16.1">Il se leva de suite après cette parole.</l>
				</lg>
				<lg n="17">
					<l n="75" num="17.1">— Attends un peu, lui dis-je, il faut encor causer.</l>
					<l n="76" num="17.2">Ouvre mes vieux bouquins, cela va t’amuser</l>
					<l n="77" num="17.3">Pendant que je termine une dernière lettre.</l>
				</lg>
				<lg n="18">
					<l n="78" num="18.1">— C’est bien, je t’attendrai si tu veux le permettre.</l>
				</lg>
				<lg n="19">
					<l part="I" n="79" num="19.1"> — Je t’en prie.</l>
				</lg>
				<lg n="20">
					<l part="F" n="79">Aussitôt il s’en alla plus loin,</l>
					<l n="80" num="20.1">Avec un in-quarto, se cacher dans un coin.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Alors entra sans bruit, marchant d’un pas timide,</l>
					<l n="82" num="21.2">Une enfant de dix ans. Son œil était humide.</l>
					<l n="83" num="21.3">Le rayon qu’il jetait en se levant sur vous</l>
					<l n="84" num="21.4">Valait une prière adressée à genoux.</l>
					<l n="85" num="21.5">Elle avait les terreurs d’une biche farouche ;</l>
					<l n="86" num="21.6">Et l’on ne voyait pas s’échapper de sa bouche</l>
					<l n="87" num="21.7">Le sourire si doux chez les enfants heureux.</l>
					<l n="88" num="21.8">Elle eut ôté jolie avec ses blonds cheveux</l>
					<l n="89" num="21.9">Et son chapeau de feutre appuyé sur l’oreille,</l>
					<l n="90" num="21.10">Si sa joue eut gardé quelque teinte vermeille ;</l>
					<l n="91" num="21.11">Mais elle était, hélas ! livide à faire peur.</l>
				</lg>
				<lg n="22">
					<l n="92" num="22.1">— Approche, mon enfant, lui dis-je avec douceur ;</l>
					<l part="I" n="93" num="22.2">Que veux-tu ?</l>
				</lg>
				<lg n="23">
					<l part="F" n="93"> — Je venais vous offrir une chaîne.</l>
				</lg>
				<lg n="24">
					<l part="I" n="94" num="24.1"> — Une chaîne ? Et pourquoi ?</l>
				</lg>
				<lg n="25">
					<l part="F" n="94"> — Nous sommes dans la gêne ;</l>
					<l n="95" num="25.1">L’hiver arrive vite, et chez nous il fait froid.</l>
				</lg>
				<lg n="26">
					<l n="96" num="26.1">— A ton air souffreteux, pauvre enfant, l’on te croit.</l>
					<l part="I" n="97" num="26.2">Comment te nommes-tu ?</l>
				</lg>
				<lg n="27">
					<l part="M" n="97">* — Bernadette.</l>
				</lg>
				<lg n="28">
					<l part="F" n="97"> — Et ton père ?</l>
				</lg>
				<lg n="29">
					<l n="98" num="29.1">— Mon père ? Excusez-moi, monsieur ; maman espère</l>
					<l n="99" num="29.2">Qu’on trouvera bientôt quelque place pour lui.</l>
					<l n="100" num="29.3">Et que nul ne saura ce qu’on souffre aujourd’hui.</l>
				</lg>
				<lg n="30">
					<l part="I" n="101" num="30.1"> — Il est donc sans emploi ?</l>
				</lg>
				<lg n="31">
					<l part="F" n="101"> — Les places sont bien rares,</l>
					<l n="102" num="31.1">Et les riches, monsieur, sont quelquefois avares.</l>
				</lg>
				<lg n="32">
					<l n="103" num="32.1">— Prends garde d’être injuste à force de souffrir.</l>
					<l n="104" num="32.2">Que de pleurs à sécher, d’indigents à nourrir,</l>
					<l n="105" num="32.3">Chère enfant, en ces jours de détresse où nous sommes !</l>
					<l n="106" num="32.4">Et puis Dieu vient à nous quand s’éloignent les hommes.</l>
				</lg>
				<lg n="33">
					<l n="107" num="33.1">Bernadette inclina la tête sur son sein ;</l>
					<l n="108" num="33.2">Je vis deux pleurs tomber sur sa petite main,</l>
					<l n="109" num="33.3">Et je craignis un peu d’avoir été sévère.</l>
					<l n="110" num="33.4">Enfants, n’êtes-vous pas les anges de la terre ?</l>
					<l n="111" num="33.5">Pourquoi vous contrister ? Mais je repris encor,</l>
					<l part="I" n="112" num="33.6">Et d’un ton caressant :</l>
				</lg>
				<lg n="34">
					<l part="F" n="112"> — La chaîne est-elle d’or ?</l>
				</lg>
				<lg n="35">
					<l part="I" n="113" num="35.1"> — Oui, monsieur, regardez.</l>
				</lg>
				<lg n="36">
					<l part="F" n="113">Sa voix était tremblante :</l>
					<l n="114" num="36.1">C’était l’espoir, sans doute. Elle ne fut pas lente</l>
					<l n="115" num="36.2">A me faire admirer le précieux bijou.</l>
				</lg>
				<lg n="37">
					<l n="116" num="37.1">— Ma mère ne veut plus la porter à son cou,</l>
					<l part="I" n="117" num="37.2">Dit-elle en soupirant.</l>
				</lg>
				<lg n="38">
					<l part="F" n="117">Cette chaîne était belle.</l>
				</lg>
				<lg n="39">
					<l n="118" num="39.1">— Ta mère veut la vendre ? Et qu’en demande-t-elle ?</l>
				</lg>
				<lg n="40">
					<l part="I" n="119" num="40.1"> — Rien, monsieur.</l>
				</lg>
				<lg n="41">
					<l part="F" n="119">Un sanglot vint étrangler sa voix.</l>
				</lg>
				<lg n="42">
					<l n="120" num="42.1">— C’est pour avoir du pain, c’est pour avoir du bois !</l>
					<l n="121" num="42.2">Ajouta-t-elle ensuite en joignant ses doigts maigres.</l>
				</lg>
				<lg n="43">
					<l n="122" num="43.1">J’entendis rire alors des enfants tout allègres,</l>
					<l n="123" num="43.2">Et cela me fit mal. Je cachai mon émoi.</l>
				</lg>
				<lg n="44">
					<l part="I" n="124" num="44.1"> — As-tu dîné ? repris-je,</l>
				</lg>
				<lg n="45">
					<l part="F" n="124"> — Aujourd’hui ? non, pas moi,…</l>
					<l n="125" num="45.1">Ni les autres non plus, excepté la petite.</l>
				</lg>
				<lg n="46">
					<l part="I" n="126" num="46.1"> — La petite ?</l>
				</lg>
				<lg n="47">
					<l part="F" n="126"> — Oui, monsieur ; son nom est Marguerite.</l>
					<l n="127" num="47.1">Elle a quatorze mois et commence à marcher.</l>
					<l n="128" num="47.2">Elle dîne toujours car je vais lui chercher,</l>
					<l n="129" num="47.3">Lorsque le soir arrive et qu’il fait un peu sombre,</l>
					<l n="130" num="47.4">Le pain qu’on jette aux chiens en des endroits sans nombre.</l>
				</lg>
				<lg n="48">
					<l part="I" n="131" num="48.1"> — Et ta mère ? et ton père ?</l>
				</lg>
				<lg n="49">
					<l part="F" n="131"> — Eux, il n’ont jamais faim.</l>
					<l n="132" num="49.1">Ils le disent, toujours, en nous donnant le pain.</l>
				</lg>
				<lg n="50">
					<l n="133" num="50.1">— C’est le premier objet que tu cherches à vendre ?</l>
				</lg>
				<lg n="51">
					<l n="134" num="51.1">— C’est le dernier, monsieur ; si vous voulez le prendre.</l>
				</lg>
				<lg n="52">
					<l n="135" num="52.1">— Non, garde-le. Vois-tu ? c’est encore un espoir.</l>
					<l n="136" num="52.2">Mais reçois cette obole et dînez tous ce soir.</l>
				</lg>
				<lg n="53">
					<l part="I" n="137" num="53.1"> — Merci ! merci, monsieur ! dit-elle.</l>
				</lg>
				<lg n="54">
					<l part="F" n="137">Et sa paupière</l>
					<l n="138" num="54.1">S’emplit à ce moment d’une ardente lumière ;</l>
					<l n="139" num="54.2">Et sur sa pâle joue, et sur son front pensif</l>
					<l n="140" num="54.3">Parut, dans un rayon, un bonheur fugitif.</l>
					<l n="141" num="54.4">Elle s’en retournait. Il me vint une idée :</l>
					<l n="142" num="54.5">La coupe des chagrins n’est pas encor vidée</l>
					<l n="143" num="54.6">Pour cette pauvre enfant et ses parents honteux,</l>
					<l n="144" num="54.7">Si j’allais voir quelqu’un et demander pour eux ?</l>
				</lg>
				<lg n="55">
					<l n="145" num="55.1">— Donne ta chaîne d’or, dis-je à la jeune fille.</l>
				</lg>
				<lg n="56">
					<l part="I" n="146" num="56.1"> — Oui, la voici, monsieur.</l>
				</lg>
				<lg n="57">
					<l part="F" n="146">Elle est lourde, elle brille,</l>
					<l n="147" num="57.1">Pensais-je en la faisant rebondir dans ma main.</l>
				</lg>
				<lg n="58">
					<l n="148" num="58.1">Mon ami Jean lisait je ne sais quel bouquin ;</l>
					<l n="149" num="58.2">Je m’approche de lui, le touche sur l’épaule :</l>
				</lg>
				<lg n="59">
					<l part="I" n="150" num="59.1"> — Veux-tu faire une aumône ?</l>
				</lg>
				<lg n="60">
					<l part="F" n="150"> — Une aumône ? Mon rôle,</l>
					<l n="151" num="60.1">Me répond-il, hélas ! serait d’en recevoir.</l>
				</lg>
				<lg n="61">
					<l n="152" num="61.1">Je crus qu’il plaisantait. Je ne pouvais le voir,</l>
					<l n="153" num="61.2">Incliné sur son livre et tout à sa lecture,</l>
					<l n="154" num="61.3">Il n’avait pas vers moi retourné sa figure.</l>
				</lg>
				<lg n="62">
					<l n="155" num="62.1">— Achète pour ta femme un bijou précieux,</l>
					<l n="156" num="62.2">Repris-je, lui mettant la chaîne sous les yeux.</l>
				</lg>
				<lg n="63">
					<l n="157" num="63.1">— D’où vient cela ? fit-il, bondissant sur sa chaise.</l>
				</lg>
				<lg n="64">
					<l n="158" num="64.1">— On garde le secret, mon cher, ne t’en déplaise.</l>
				</lg>
				<lg n="65">
					<l n="159" num="65.1">— Voilà, quand on est pauvre, à quoi l’on est réduit,</l>
					<l n="160" num="65.2">Et, quand tout est vendu, l’on meurt dans un réduit !</l>
				</lg>
				<lg n="66">
					<l n="161" num="66.1">— Tu connais cette chaîne, et tu sais quelle dame ?…</l>
				</lg>
				<lg n="67">
					<l part="I" n="162" num="67.1">Il m’arrête soudain, se reprend :</l>
				</lg>
				<lg n="68">
					<l part="F" n="162"> — Sur mon âme,</l>
					<l n="163" num="68.1">Ajoute-t-il alors, je ne sais rien du tout.</l>
					<l n="164" num="68.2">Je ne sais que cela : la misère est partout…</l>
					<l n="165" num="68.3">Mais cette chaîne, toi, combien l’a-tu payée ?</l>
				</lg>
				<lg n="69">
					<l n="166" num="69.1">Cette dernière phrase elle fut bégayée.</l>
				</lg>
				<lg n="70">
					<l n="167" num="70.1">— Elle n’est pas à moi, mon brave Dumanoir.</l>
				</lg>
				<lg n="71">
					<l n="168" num="71.1">— Non ! A qui donc alors ? Il faudrait le savoir.</l>
				</lg>
				<lg n="72">
					<l n="169" num="72.1">— A quelques nobles gens, honteux de leurs misères ;</l>
					<l n="170" num="72.2">Qui vont mourir de faim au milieu de leurs frères,</l>
					<l part="I" n="171" num="72.3">Plutôt que mendier,</l>
				</lg>
				<lg n="73">
					<l part="F" n="171">Jean dit : C’est vrai cela….</l>
					<l part="I" n="172" num="73.1">Mais qui donc t’a remis cette chaîne ?</l>
				</lg>
				<lg n="74">
					<l part="F" n="172"> — Voilà,</l>
					<l n="173" num="74.1">C’est une pauvre enfant qui m’attend à ma porte…</l>
					<l n="174" num="74.2">Moi je n’achète point d’objets de cette sorte,</l>
					<l n="175" num="74.3">Du moins en pareil cas. Je n’ai jamais goûté</l>
					<l n="176" num="74.4">Ce commode moyen de faire charité.</l>
				</lg>
				<lg n="75">
					<l n="177" num="75.1">— Si j’avais, repart-il, quelques sous dans ma bourse,</l>
					<l n="178" num="75.2">Je les donnerais bien à l’enfant sans ressource…..</l>
				</lg>
				<lg n="76">
					<l n="179" num="76.1">Il fouillait son gousset. D’un ton rauque et fiévreux</l>
					<l part="I" n="180" num="76.2">Il ajoute :</l>
				</lg>
				<lg n="77">
					<l part="F" n="180"> — Rien ! rien ! que je suis malheureux !</l>
				</lg>
				<lg n="78">
					<l n="181" num="78.1">Or, comme il prononçait cette triste parole,</l>
					<l n="182" num="78.2">La petite survint. Une crainte frivole</l>
					<l n="183" num="78.3">De ses jeunes esprits, je le crois, s’emparait.</l>
					<l n="184" num="78.4">Je tardais à venir et le temps lui durait.</l>
					<l n="185" num="78.5">Elle ne savait pas si j’étais bien honnête,</l>
					<l n="186" num="78.6">Et de sa chaîne d’or pouvait être inquiète.</l>
				</lg>
				<lg n="79">
					<l n="187" num="79.1">— Que je suis malheureux ! disait Jean, se levant.</l>
				</lg>
				<lg n="80">
					<l n="188" num="80.1">Et son regard tomba sur la naïve enfant</l>
					<l n="189" num="80.2">Qui venait de sourire en me voyant près d’elle.</l>
					<l n="190" num="80.3">L’enfant s’arrête alors comme un oiseau dont l’aile</l>
					<l n="191" num="80.4">Se brise tout à coup en volant dans les cieux.</l>
					<l n="192" num="80.5">Elle porte sur nous un regard anxieux</l>
					<l n="193" num="80.6">Et puis courbe la tête. On voit frémir sa lèvre :</l>
				</lg>
				<lg n="81">
					<l part="I" n="194" num="81.1"> — O mon père ! dit-elle.</l>
				</lg>
				<lg n="82">
					<l part="F" n="194">Et lui, l’œil plein de fièvre,</l>
					<l n="195" num="82.1">La bouche frémissant et le front en sueurs,</l>
					<l n="196" num="82.2">Il la prend dans ses bras et l’inonde de pleurs.</l>
				</lg>
			</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="197" num="1.1">L’airain des vieux clochers avait sonné six heures ;</l>
						<l n="198" num="1.2">Et déjà les remparts, les arbres, les demeures,</l>
						<l n="199" num="1.3">Comme dans un manteau, se drapaient dans la nuit.</l>
						<l n="200" num="1.4">Je sortis. Il neigeait, et la neige avec bruit</l>
						<l n="201" num="1.5">Tourbillonnait dans l’air et fouettait les fenêtres.</l>
						<l n="202" num="1.6">En marchant je songeais à tous ces pauvres êtres</l>
						<l n="203" num="1.7">Qui grelottent, serrés contre un foyer sans feu,</l>
						<l n="204" num="1.8">Et que semble oublier la charité de Dieu.</l>
					</lg>
					<lg n="2">
						<l n="205" num="2.1">Je marchais à grands pas comme c’est ma coutume.</l>
						<l n="206" num="2.2">De loin, à la clarté du fanal qui s’allume,</l>
						<l n="207" num="2.3">Je vois, dans le brouillard, un jeune couple heureux</l>
						<l n="208" num="2.4">S’avancer en riant sur le trottoir poudreux.</l>
						<l n="209" num="2.5">Au bras du cavalier, comme une vigne au chêne,</l>
						<l n="210" num="2.6">La femme est suspendue ; et ses cheveux d’ébène,</l>
						<l n="211" num="2.7">D’un turban de velours s’échappant à demi,</l>
						<l n="212" num="2.8">Effleurent, parfumés, les lèvres de l’ami.</l>
						<l n="213" num="2.9">Deux jeunes amoureux ont cent choses à dire ;</l>
						<l n="214" num="2.10">Bien gaiement ils causaient ; et leurs éclats de rire</l>
						<l n="215" num="2.11">Comme les blancs flocons s’éparpillaient au vent,</l>
						<l n="216" num="2.12">Je souffrais, leur bonheur me parut insolent.</l>
						<l n="217" num="2.13">Pourtant ne faut-il pas que la jeunesse chante ?</l>
						<l n="218" num="2.14">Le monde est ainsi fait : Près d’une âme méchante</l>
						<l n="219" num="2.15">Une âme pure exhale un parfum de vertu ;</l>
						<l n="220" num="2.16">Près d’un riche superbe un pauvre est demi-nu :</l>
						<l n="221" num="2.17">Un bouton s’ouvre encor près d’une fleur qui tombe,</l>
						<l n="222" num="2.18">Et le berceau sourit à côté de la tombe !</l>
					</lg>
					<lg n="3">
						<l n="223" num="3.1">En songeant à ces faits qui troublent la raison,</l>
						<l n="224" num="3.2">J’arrivai sur le seuil d’une haute maison.</l>
					</lg>
					<lg n="4">
						<l part="I" n="225" num="4.1"> — C’est bien ici ! me dis-je.</l>
					</lg>
					<lg n="5">
						<l part="F" n="225">Alors, dans les ténèbre</l>
						<l n="226" num="5.1">Le marteau me parut frapper des coups funèbres.</l>
						<l n="227" num="5.2">Une enfant descendit deux ou trois escaliers</l>
						<l n="228" num="5.3">Et se hâta d’ouvrir. De ses méchants souliers</l>
						<l n="229" num="5.4">Ses pieds mignons sortaient rougis par la froidure.</l>
						<l n="230" num="5.5">Ses dents claquaient bien dru. Libre, sa chevelure</l>
						<l n="231" num="5.6">Protégeait son épaule en la voilant un peu.</l>
						<l n="232" num="5.7">Elle souffrait. Hélas ! tout souffrait en ce lieu !</l>
					</lg>
					<lg n="6">
						<l n="233" num="6.1">— Vous êtes le docteur !… Montez, monsieur, dit-elle.</l>
						<l part="I" n="234" num="6.2">Papa ne vous suit point ?</l>
					</lg>
					<lg n="7">
						<l part="F" n="234">Et, tenant sa chandelle,</l>
						<l n="235" num="7.1">Afin d’éclairer mieux elle monta devant.</l>
					</lg>
					<lg n="8">
						<l n="236" num="8.1">— Je suis le médecin, dis-tu, ma pauvre enfant ?</l>
						<l part="I" n="237" num="8.2">Tu te trompes.</l>
					</lg>
					<lg n="9">
						<l part="F" n="237">— Montez quand même. Tout à l’heure</l>
						<l n="238" num="9.1">Le médecin viendra dans notre humble demeure.</l>
					</lg>
					<lg n="10">
						<l n="239" num="10.1">— Qui donc, chère petite, est malade chez vous ?</l>
					</lg>
					<lg n="11">
						<l part="I" n="240" num="11.1"> — Ma mère, monsieur.</l>
					</lg>
					<lg n="12">
						<l part="M" n="240"> — Ah !</l>
					</lg>
					<lg n="13">
						<l part="F" n="240">Alors un chant bien doux,</l>
						<l n="241" num="13.1">Un chant triste et dolent vint frapper mon oreille.</l>
					</lg>
					<lg n="14">
						<l n="242" num="14.1">— On n’entend pas partout, dis-je, une voix pareille ;</l>
						<l part="I" n="243" num="14.2">Qui chante donc ainsi ?</l>
					</lg>
					<lg n="15">
						<l part="F" n="243">La petite pleurait.</l>
					</lg>
					<lg n="16">
						<l n="244" num="16.1">— Mais dans cette maison, à ce qu’il me paraît,</l>
						<l n="245" num="16.2">Tout n’est pas désolé, me disais-je en moi-même.</l>
					</lg>
					<lg n="17">
						<l n="246" num="17.1">La porte s’ouvre alors, puis une femme blême</l>
						<l n="247" num="17.2">M’apparaît aussitôt dans un méchant fauteuil.</l>
						<l n="248" num="17.3">Je m’arrête, surpris, un instant sur le seuil,</l>
						<l n="249" num="17.4">Car c’est elle qui chante. Elle se tait de suite</l>
						<l n="250" num="17.5">Et veut, dans sa frayeur, je crois, prendre la fuite,</l>
						<l n="251" num="17.6">Mais sur son siège dur elle retombe. Non,</l>
						<l n="252" num="17.7">Je ne saurais conter quel étrange rayon</l>
						<l n="253" num="17.8">Jaillit en ce moment de sa morne paupière,</l>
						<l n="254" num="17.9">Et comme elle reprit une attitude fière !</l>
						<l n="255" num="17.10">Malgré son front livide, elle était belle encor</l>
						<l n="256" num="17.11">Avec sa robe blanche, avec sa chaîne d’or</l>
						<l n="257" num="17.12">Dont, les brillants anneaux flottaient sur sa poitrine.</l>
						<l n="258" num="17.13">Elle étendit vers moi sa main osseuse et fine :</l>
					</lg>
					<lg n="18">
						<l n="259" num="18.1">— Venez-vous en ce lieu chercher de la pitié ?</l>
						<l n="260" num="18.2">Me dit-elle soudain. Au nom de l’amitié</l>
						<l n="261" num="18.3">Venez-vous demander qu’on songe à la détresse,</l>
						<l n="262" num="18.4">Qu’on ranime les cœurs qui sont dans la tristesse ?</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="263" num="18.5">Se couche-t-on chez vous quelquefois sans souper,</l>
						<l n="264" num="18.6">Et voit-on au chevet les spectres se grouper ?</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="265" num="18.7">Je vous ferai l’aumône. Aimez la Providence,</l>
						<l n="266" num="18.8">Et du bien qu’on vous donne usez avec prudence,</l>
						<l n="267" num="18.9">Car après le soleil on voit monter la nuit,</l>
						<l n="268" num="18.10">Le bonheur passe vite et la douleur le suit !</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="269" num="18.11">J’ai de l’or ; je suis riche — Elle montrait sa chaîne —</l>
						<l n="270" num="18.12">Mes enfants n’ont jamais, monsieur, connu la gêne,</l>
						<l n="271" num="18.13">Et, s’ils souffrent un peu, c’est de me voir souffrir.</l>
					</lg>
					<lg n="19">
						<l n="272" num="19.1">Bernadette pleurait. L’enfant qui vint ouvrir,</l>
						<l n="273" num="19.2">Vous le pensiez sans doute, était ma Bernadette.</l>
						<l n="274" num="19.3">La pauvre femme, alors, se relève et rejette</l>
						<l n="275" num="19.4">Sur son cou grêle et blanc ses boucles de cheveux.</l>
						<l n="276" num="19.5">Quelques enfants jouaient et se croyaient heureux.</l>
					</lg>
					<lg n="20">
						<l n="277" num="20.1">— Venez ici, dit-elle. Et sa parole tremble.</l>
					</lg>
					<lg n="21">
						<l n="278" num="21.1">Eux, dans une autre pièce ils se sauvent ensemble.</l>
						<l n="279" num="21.2">Marguerite, pourtant, tombant à chaque pas,</l>
						<l n="280" num="21.3">S’avance vers sa mère et tend ses petits bras.</l>
						<l n="281" num="21.4">Et sa mère la prend, sur ses genoux l’a couche,</l>
						<l n="282" num="21.5">La couvre de ses mains afin qu’on ne la touche,</l>
						<l n="283" num="21.6">Et se met à chanter comme pour l’endormir.</l>
						<l n="284" num="21.7">Ah ! j’aurais aimé mieux entendre alors gémir,</l>
						<l n="285" num="21.8">Dans son mortel chagrin, la pauvre malheureuse !</l>
						<l n="286" num="21.9">Cela m’eut touché moins. C’est une chose affreuse</l>
						<l n="287" num="21.10">Que de rire ou chanter à force de douleurs !</l>
						<l n="288" num="21.11">De ma main j’essuyais mes paupières en pleurs ;</l>
						<l n="289" num="21.12">Je n’avais jamais vu de détresse aussi forte.</l>
						<l n="290" num="21.13">Et j’étais là, debout, toujours près de la porte,</l>
						<l n="291" num="21.14">N’osant aller plus loin, ne pouvant pas parler</l>
						<l n="292" num="21.15">Et tenté de m’enfuir ou de m’agenouiller.</l>
						<l n="293" num="21.16">Comme moi Bernadette aussi semblait attendre.</l>
					</lg>
					<lg n="22">
						<l n="294" num="22.1">Enfin sur l’escalier des pas se font entendre</l>
						<l n="295" num="22.2">Et Dumanoir arrive avec le médecin.</l>
						<l part="I" n="296" num="22.3">Nos mains se pressent :</l>
					</lg>
					<lg n="23">
						<l part="F" n="296"> — Jean, dis-moi dans quel dessein</l>
						<l n="297" num="23.1">Tu me cachais ainsi ta misère profonde :</l>
						<l n="298" num="23.2">Doutais-tu de mon cœur ou craignais-tu le monde ?</l>
					</lg>
					<lg n="24">
						<l n="299" num="24.1">Il secoua la tête et ne répondit rien ;</l>
						<l part="I" n="300" num="24.2">Mais je vis dans ses yeux deux pleurs brûlants.</l>
					</lg>
					<lg n="25">
						<l part="F" n="300"> — Eh bien !</l>
						<l n="301" num="25.1">Demandai-je au docteur, comment est la malade ?</l>
					</lg>
					<lg n="26">
						<l part="I" n="302" num="26.1">Avant qu’il répondit :</l>
					</lg>
					<lg n="27">
						<l part="F" n="302"> — Il faut que je m’évade,</l>
						<l n="303" num="27.1">Car depuis quinze jours l’on me tient en prison,</l>
						<l n="304" num="27.2">Dit la femme en délire, et cela sans raison.</l>
						<l n="305" num="27.3">Mes enfants avaient faim ; nous étions sans ressources ;</l>
						<l n="306" num="27.4">Je me suis mise alors à faire quelques courses</l>
						<l n="307" num="27.5">En mendiant du pain, en mendiant des sous,</l>
						<l n="308" num="27.6">Et tous m’ont refusée ! Alors moi, voyez-vous,</l>
						<l n="309" num="27.7">J’ai volé, quelque part, en passant, je suppose,</l>
						<l n="310" num="27.8">Un morceau de pain blanc, rien qu’un, pas autre chose,</l>
						<l n="311" num="27.9">Et voilà que de suite on m’a mise au cachot !…</l>
					</lg>
					<lg n="28">
						<l n="312" num="28.1">Le reste se perdit dans un amer sanglot.</l>
						<l n="313" num="28.2">Les enfants, tour à tour sortis de leur cachette,</l>
						<l n="314" num="28.3">Écoutaient étonnés. D’une façon discrète</l>
						<l part="I" n="315" num="28.4">Le médecin me dit :</l>
					</lg>
					<lg n="29">
						<l part="F" n="315"> — Non, je n’ai plus d’espoir.</l>
					</lg>
					<lg n="30">
						<l n="316" num="30.1">Deux semaines après, un jeudi, vers le soir,</l>
						<l n="317" num="30.2">J’allai m’agenouiller, un peu mélancolique,</l>
						<l n="318" num="30.3">Sur les dalles de bois de l’humble basilique.</l>
						<l n="319" num="30.4">Devant le tabernacle, avec humilité,</l>
						<l n="320" num="30.5">Quelques chrétiens priaient le Dieu de charité.</l>
						<l n="321" num="30.6">Sous les vastes arceaux flottaient d’étranges ombres ;</l>
						<l n="322" num="30.7">Et, comme l’œil d’un ange, au fond des voûtes sombres</l>
						<l part="I" n="323" num="30.8">La lampe d’or veillait.</l>
					</lg>
					<lg n="31">
						<l part="F" n="323">Une enfant à l’air doux</l>
						<l n="324" num="31.1">A l’autel de Marie était seule à genoux.</l>
						<l n="325" num="31.2">Une grande douleur semblait remplir son âme,</l>
						<l n="326" num="31.3">Et ses yeux suppliants invoquaient Notre Dame.</l>
						<l n="327" num="31.4">Sur des tables de marbre, en face de l’autel,</l>
						<l n="328" num="31.5">Des cierges et des fleurs faisaient monter au ciel,</l>
						<l n="329" num="31.6">Les premiers, les rayons de leurs paisibles flammes,</l>
						<l n="330" num="31.7">Les secondes, l’encens de leurs chastes dictames.</l>
						<l n="331" num="31.8">Et l’enfant se leva. Je ne sais quel émoi</l>
						<l n="332" num="31.9">Paraissait l’agiter dans sa naïve foi.</l>
						<l n="333" num="31.10">Elle était, ce soir-là, toute de noir vêtue.</l>
						<l n="334" num="31.11">Son regard se suspend à la blanche statue</l>
						<l n="335" num="31.12">De la mère de Dieu pleurant près de la croix,</l>
						<l n="336" num="31.13">Elle prie ardemment et sa pieuse voix,</l>
						<l n="337" num="31.14">Comme un écho sacré du ciel est entendue.</l>
						<l n="338" num="31.15">Elle ouvre le balustre et se trouve rendue</l>
						<l n="339" num="31.16">Près des tables de marbre où brillent fleurs et feux</l>
						<l n="340" num="31.17">De nouveau vers la Vierge elle lève les yeux,</l>
						<l n="341" num="31.18">Et dépose, en tremblant sous la clarté sereine,</l>
						<l n="342" num="31.19">Près des vases de fleurs, une brillante chaîne.</l>
					</lg>
					<lg n="32">
						<l n="343" num="32.1">En voyant Bernadette offrir sa chaîne d’or</l>
						<l n="344" num="32.2">La Mère des douleurs parut sourire encor.</l>
					</lg>
					<lg n="33">
						<l n="345" num="33.1">Quelques jours ont passé. Près d’un feu qui pétille</l>
						<l n="346" num="33.2">Dumanoir entouré de sa jeune famille,</l>
						<l n="347" num="33.3">Pleure silencieux. Le vent perce les toits.</l>
						<l n="348" num="33.4">Un petit mendiant, se soufflant dans les doigts,</l>
						<l n="349" num="33.5">Vient, pour l’amour de Dieu, demander l’assistance.</l>
						<l n="350" num="33.6">Dans la maison en deuil reparaît quelqu’aisance :</l>
						<l n="351" num="33.7">Bernadette a touché le Seigneur par sa foi,</l>
						<l n="352" num="33.8">Et son père, un matin, a trouvé de l’emploi.</l>
					</lg>
					<lg n="34">
						<l n="353" num="34.1">— Donne, fit Dumanoir, donne à chaque misère !</l>
						<l n="354" num="34.2">Donne, ma Bernadette, afin qu’une autre mère,</l>
						<l n="355" num="34.3">En ces jours de malheur, ne meure pas de faim !…</l>
					</lg>
					<lg n="35">
						<l n="356" num="35.1">L’enfant au petit pauvre apporta tout un pain</l>
					</lg>
				</div>
			</div>
		</body>
	</text>
</TEI>