MOR
———
MOR_2

Jean MORÉAS
1856-1910

════════════════════════════════════════════
Les Cantilènes

1886

1070 vers

─ poème	MOR24	"Le soir n’est plus des ganses et de la danse."

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ FUNÉRAILLES
	─ poème	MOR25	"Roses de Damas, pourpres roses, blanches roses,"
	─ poème	MOR26	"Voix qui revenez, bercez-nous, berceuses voix :"
	─ poème	MOR27	"Dans le jardin taillé comme une belle dame,"
	─ poème	MOR28	"Ses mains qu’elle tend comme pour des théurgies,"
	─ poème	MOR29	"Pleurer un peu, si je pouvais pleurer un peu,"
	─ poème	MOR30	"En son orgueil opiniâtre,"
	─ poème	MOR31	"Ô les cavales hennissant au vent limpide,"
	─ poème	MOR32	"Désir de vivre et d’être heureux, leurre et fallace,"
	─ poème	MOR33	"Sous vos longues chevelures, petites fées,"
	─ poème	MOR34	"Par la douce pitié qui s’attendrit au pli,"
	─ poème	MOR35	"Et j’irai le long de la mer éternelle"

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ INTERLUDE
	─ poème	MOR36	TOUTE LA BABIOLE
	─ poème	MOR37	LA LUNE SE LEVA
	─ poème	MOR38	GESTE
	─ poème	MOR39	NEVER MORE
	─ poème	MOR40	LE RHIN
	─ poème	MOR41	FLORENCE
	─ poème	MOR42	VIGNETTE
	─ poème	MOR43	MADRIGAL
	─ poème	MOR44	LE RUFFIAN
	─ poème	MOR45	INTIMITÉ

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ASSONANCES
	─ poème	MOR46	MARYÔ
	─ poème	MOR47	LA MAUVAISE MÈRE
	─ poème	MOR48	NOCTURNE
	─ poème	MOR49	AIR DE DANSE
	─ poème	MOR50	L’ÉPOUSE FIDÈLE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ CANTILÈNES
	─ poème	MOR51	LA COMTESSE ESMÉRÉE
	─ poème	MOR52	AGHA VELI
	─ poème	MOR53	LA FEMME PERFIDE
	─ poème	MOR54	LA VEUVE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LE PUR CONCEPT
	─ poème	MOR55	"Fi ! Du monitor attendu,"
	─ poème	MOR56	"Le burg immémorial, de ses meurtrières"
	─ poème	MOR57	"Sous la rouille des temps je suis un vieux blason."
	─ poème	MOR58	"Les pâles filles de l’argile"
	─ poème	MOR59	"Dans le chêne rugueux sculptée,"
	─ poème	MOR60	"La DÉTRESSE dit : ce sont des songes anciens,"

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ HISTOIRES MERVEILLEUSES
	─ poème	MOR61	MÉLUSINE
	─ poème	MOR62	LA VIEILLE FEMME DE BERKELEY
	─ poème	MOR63	TIDOGOLAIN
	─ poème	MOR64	LA CHEVAUCHÉE DE LA MORT
