MON
———
MON_1

Robert de MONTESQUIOU
1855-1921

════════════════════════════════════════════
LES HORTENSIAS BLEUS

1896

5364 vers

─ poème	MON1	A LA MÉMOIRE DE GABRIEL DE YTURRI

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I INTROÏT
	─ poème	MON2	I. MONTESQUIVI
	─ poème	MON3	II. PARTI
	─ poème	MON4	III. PATERNA RURA
	─ poème	MON5	IV. ORDINAIRE
	─ poème	MON6	V. FOI
	─ poème	MON7	VI. OUTILS
	─ poème	MON8	VII. FLEURS ET PLUMES
	─ poème	MON9	VIII. CALAME
	─ poème	MON10	IX. PAGINÆ
	─ poème	MON11	X. REGINA
	─ poème	MON12	XI. AVIS
	─ poème	MON13	XII. PORTENTA
	─ poème	MON14	XIII. A UNE COUSINE INGRATE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II CHAPELLE BLANCHE
	─ poème	MON15	XIV. SIGNES
	─────────────────────────────────────────────────────
	▫ BERCEUSES
		─ poème	MON16	XV. BERCEUSE D’EAU
		─ poème	MON17	XVI. BERCEUSE D’OMBRE
		─ poème	MON18	XVII. BERCEUSE D’ÂME
		─ poème	MON19	XVIII. BERCEUSE D’AILES
		─ poème	MON20	XIX. BERCEUSE BÊTE
		─ poème	MON21	XX. BERCEUSES VEUVES
		─ poème	MON22	XXI. BERCEUSE FÉROCE
		─ poème	MON23	XXII. BERCEUSE D’ÉMAIL
		─ poème	MON24	XXIII. RECOLLECTION
		─ poème	MON25	XXIV. RELIQUIÆ.
		─ poème	MON26	XXV. POTINÆ
		─ poème	MON27	XXVI. CHANDE
	─────────────────────────────────────────────────────
	▫ PUÉRILITÉS ET ENFANTILLAGES
		─ poème	MON28	XXVII. SQUARE
		─ poème	MON29	XXVIII. "L’enfant au sucre d’orge vert"
		─ poème	MON30	XXIX. CYCLE
		─ poème	MON31	XXX. "O petite Adrienne, ô frêle fleur, ô flamme"
		─ poème	MON32	XXXI. PASSER
		─ poème	MON33	XXXII. FLOS ALIGER
		─ poème	MON34	XXXIII. "O petite Beauté, petite grande Dame,"
		─ poème	MON35	XXXIV. "Elle est toute petite, elle est toute pensive,"
		─ poème	MON36	XXXV. "L’enfant dont la tristesse habite les châteaux"
		─ poème	MON37	XXXVI. DIVA
		─ poème	MON38	XXXVII. INFANTILLAGE
		─ poème	MON39	XXXVIII. "D’où vient qu’aux enfantins souvenirs tu tressailles,"
		─ poème	MON40	XXXIX. INDULGENCE PLÉNIÈRE
		─ poème	MON41	XL. MISSA EST
		─ poème	MON42	XLI. CONFESSE
		─ poème	MON43	XLII. HUMUS COLLÉGIAL
		─ poème	MON44	XLIII. THUS TURRIS
		─ poème	MON45	XLIV. BLANC MINEUR
		─ poème	MON46	XLV. EUCHARIS
		─ poème	MON47	XLVI. TABLES VIVES
		─ poème	MON48	XLVII. VÉRITÉS ESSENTIELLES
		─ poème	MON49	XLVIII. RHAPSODIES
		─ poème	MON50	XLIX. LOULOUPS
		─ poème	MON51	L. BON AMI
		─ poème	MON52	LI. "Pourquoi ces meurtriers qui, tous, devaient se taire,"
		─ poème	MON53	LII. CAPUT
		─ poème	MON54	LIII. MESSIEURS DE PARIS

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III CHAMBRE CLAIRE
	─────────────────────────────────────────────────────
	▫ INTUS - NOTATIONS ET MILIEUX
		─ poème	MON55	LIV. FAIRE
	─────────────────────────────────────────────────────
	▫ ZOTHECÆ AC MUSICÆ
		─ poème	MON56	LV. LOUIS-QUINZERIE
		─ poème	MON57	LVI. VESTER VESPER
		─ poème	MON58	LVII. SCRUPULE
		─ poème	MON59	LVIII. PIÈGES
		─ poème	MON60	LIX. MORCEAU
		─ poème	MON61	LX. VA LONTANO
		─ poème	MON62	LXI. FŒDERIS ARCA
		─ poème	MON63	LXII. LE BLANC ET LE NOIR
		─ poème	MON64	LXIII. DEUX REFRAINS
	─────────────────────────────────────────────────────
	▫ CÉANS
		─ poème	MON65	LXIV. MANIÈRES 
		─ poème	MON67	LXVI. TRANSFUSION
		─ poème	MON68	LXVII. HIC LOCUS
		─ poème	MON69	LXVIII. ANGLE DOCTE
		─ poème	MON70	LXIX. PUPITRE
		─ poème	MON71	LXX. ALLUVIONS
		─ poème	MON72	LXXI. THÉRAPEUTIQUE
		─ poème	MON73	LXXII. OBJETS
		─ poème	MON74	LXXIII. INTUS
		─ poème	MON75	LXXIV. ROSEUM MARE
		─ poème	MON76	LXXV. "Je veux faire de l’art japonais : une chose"
		─ poème	MON77	LXXVI. PIERROT
		─ poème	MON78	LXXVII. EMPIRISME
		─ poème	MON79	LXXVIII. "Les éventails anciens sont des papillons vastes"
		─ poème	MON80	LXXIX. BABEL
		─ poème	MON81	LXXX. PRODIGUE
		─ poème	MON82	LXXXI. QUIA PULVIS
	─────────────────────────────────────────────────────
	▫ ALTIOR
		─ poème	MON83	LXXXII. FLANDRES
		─ poème	MON84	LXXXIII. MUSIQUE DE JARDIN
		─ poème	MON85	LXXXIV. JUCUNDA JOCONDA
		─ poème	MON86	LXXXV. ORACLE
		─ poème	MON87	LXXXVI. BALNEA
		─ poème	MON88	LXXXVII. PLAFOND
		─ poème	MON89	LXXXVIII. LONGUS
		─ poème	MON90	LXXXIX. "Le ciel est treillagé dû carrelage fin"
		─ poème	MON91	XC. SUCCUBE
		─ poème	MON92	XCI. REPIT
		─ poème	MON93	XCII. PINACOTHÈQUE
		─ poème	MON94	XCIII. TEMPÉ
		─ poème	MON95	XCIV. UNE MUSICIENNE
		─ poème	MON96	XCV. MINET
		─ poème	MON97	XCVI. MAC-NEILLIANA
	─────────────────────────────────────────────────────
	▫ FORAS
		─ poème	MON98	XCVII. TRANSPORT
		─ poème	MON99	XCVIII. PORTÉES
		─ poème	MON100	XCIX. KO-TA-KY
		─ poème	MON101	C. RUINÆ
		─ poème	MON102	CI. CHAMBRE D’AMIS
		─ poème	MON103	CII. "Un portrait, au château d’Azay, dans son costume"
		─ poème	MON104	CIII. "Les tours du beau Castel sont noblement coiffées"
		─ poème	MON105	CIV. MONOCEROS
		─ poème	MON106	CV. VENATIO
		─ poème	MON107	CVI. COURRE
		─ poème	MON108	CVII. TAMA
	─────────────────────────────────────────────────────
	▫ STÈLES ET CIPPES
		─ poème	MON109	CVIII. NIX ET NOX
		─ poème	MON110	CIX. EXSULES
		─ poème	MON111	CX. LES DEUX ANGES
		─ poème	MON112	CXI. "Ce serait un penser plein de mélancolie"
		─ poème	MON113	CXII. PALPEBRÆ
		─ poème	MON114	CXIII. MELPOMÈNE
		─ poème	MON115	CXIV. DIADÈMES ET DRAPERIES
		─ poème	MON116	CXV. THALIE
		─ poème	MON117	CXVI. MADRIGAL
		─ poème	MON118	CXVII. OFFRANDE A EDMOND DE GONCOURT
		─ poème	MON119	CXVIII. DE VERLÂNÂ
		─ poème	MON120	CXIX. BUFFA-SERIA

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IV CHAMBRE OBSCURE
	─────────────────────────────────────────────────────
	▫ ALMA PARENS
		─ poème	MON121	CXX. DONNANT, DONNANT
		─ poème	MON122	CXXI. MONSTRA TE ESSE MATREM
		─ poème	MON123	CXXII. RÂTELIER
		─ poème	MON124	CXXIII. VIVRES
		─ poème	MON125	CXXIV. INDEMNITÉS
		─ poème	MON126	CXXV. CONDOLÉANCES
	─────────────────────────────────────────────────────
	▫ VIVES SAISONS
		─ poème	MON127	CXXVI. PANNEAUX
		─ poème	MON128	CXXVII. VERNAL
		─ poème	MON129	CXXVIII. VERT-VERT
		─ poème	MON130	CXXIX. PRIMEVÈRE
		─ poème	MON131	CXXX. ALLÉGORIE
		─ poème	MON132	CXXXI. PHYSIQUE DE L’AVENIR
	─────────────────────────────────────────────────────
	▫ MORTE SAISON
		─ poème	MON133	CXXXII. PÉNULTIÈMES
		─ poème	MON134	CXXXIII. IMPARES
		─ poème	MON135	CXXXIV. DAMÆ
		─ poème	MON136	CXXXV. FILLE-FEUILLE
		─ poème	MON137	CXXXVI. ÉTOILES MORTES
		─ poème	MON138	CXXXVII. L’INSEXUELLE
		─ poème	MON139	CXXXVIII. MACULE
		─ poème	MON140	CXXXIX. GLOSE
	─────────────────────────────────────────────────────
	▫ ECCE HOMO
		─ poème	MON141	CXL. LIBER
		─ poème	MON142	CXLI. REX
		─ poème	MON143	CXLII. CHAIR FRAÎCHE
		─ poème	MON144	CXLIII. ROIS DES AULNES
		─ poème	MON145	CXLIV. "Fermez cette cage ; leur chambre,"
		─ poème	MON146	CXLV. "Elle racontait une histoire"
		─ poème	MON147	CXLVI. BERCEUSE GRISE
		─ poème	MON148	CXLVII. "Cet enfant qu’on gardait comme une brebis sainte,"
		─ poème	MON149	CXLVIII. "Le gentil petit trépassé"
	─────────────────────────────────────────────────────
	▫ MATER ALMA
		─ poème	MON150	CXLIX. "Chaque soir, en des lits, nos fatigues s’allongent ;"
		─ poème	MON151	CL. GABELLE
		─ poème	MON152	CLI. P. P. C.
		─ poème	MON153	CLII. "La Mort n’est point un Être, elle n’est que l’absence"
		─ poème	MON154	CLIII. IN EXTREMIS
		─ poème	MON155	CLIV. SUNT LACRYMÆ HOMINUM
		─ poème	MON156	CLV. MORTUIS IGNOTIS
	─────────────────────────────────────────────────────
	▫ TÉNÈBRES
		─ poème	MON157	CLVI. DYSPNÉE
		─ poème	MON158	CLVII. IS PATER EST
		─ poème	MON159	CLVIII. IULE
	─────────────────────────────────────────────────────
	▫ NÉNIES
		─ poème	MON160	CLIX. CROISSANCE
		─ poème	MON161	CLX. ANCILLA
		─ poème	MON162	CLXI. "Le front pareil au front de Psyché de Capoue"
		─ poème	MON163	CLXII. CHÉRUBINE
		─ poème	MON164	CLXIII. A UNE MORTE
		─ poème	MON165	CLXIV. LE TRIO DES MASQUES
		─ poème	MON166	CLXV. "Vous avez bien raison non seulement, Madame,"
		─ poème	MON167	CLXVI. "Les vrais trépassés et les sûrs défunts"
		─ poème	MON168	CLXVII. BERCEUSE NOIRE
		─ poème	MON169	CLXVIII. REDIVIVA
		─ poème	MON170	CLXIX. "Je m’en remets à Dieu qui, s’il existe encore,"
	─────────────────────────────────────────────────────
	▫ V ITE
		─ poème	MON171	CLXX. DULCEDO AMARITUDINIS
		─ poème	MON172	CLXXI. SATIS
		─ poème	MON173	CLXXII. ORAISON FUNÈBRE
		─ poème	MON174	CLXXIII. SEPULCRUM PULCHRUM
		─ poème	MON175	CLXXIV. EXEGI
		─ poème	MON176	CLXXV. REQUIESCAT IN LUCE
