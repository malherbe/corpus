VIG
———
VIG_2

Alfred de VIGNY
1797-1863

════════════════════════════════════════════
HÉLÉNA

1826

942 vers

─ poème	VIG35	CHANT PREMIER - L’AUTEL
─ poème	VIG36	CHANT SECOND - LE NAVIRE
─ poème	VIG37	CHANT TROISIÈME - L’URNE
