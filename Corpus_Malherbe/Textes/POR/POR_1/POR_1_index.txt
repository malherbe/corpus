POR
———
POR_1

Georges de PORTO-RICHE
1849-1930

════════════════════════════════════════════
PRIMA VERBA

1872

1386 vers

─ poème	POR1	I. "Rien aujourd’hui n’est plus ennuyeux qu’un poète"
─ poème	POR2	II. L’IDÉE
─ poème	POR3	III. "Je ne 1’ai pas vue ; il est tard ; je suis brisé"
─ poème	POR4	IV. VENEZ !
─ poème	POR5	V. SONNET
─ poème	POR6	VI. "Tu souffres, je le sens ; tu pleures, je le sais."
─ poème	POR7	VII. PROH PUDOR !
─ poème	POR8	VIII. THÉORIE
─ poème	POR9	IX. A TRAVERS UNE LARME
─ poème	POR10	X. AU COIN DU FEU
─ poème	POR11	XI. A UNE ENFANT
─ poème	POR12	XII. LA BACCHANTE
─ poème	POR13	XIII. "Quand tu la vois au bal triomphante qui passe,"
─ poème	POR14	XIV. CHI LO LO SA ?…
─ poème	POR15	XV. SONNET
─ poème	POR16	XVI. SONNET
─ poème	POR17	XVII. "Elle ne sourit plus, la douce jeune fille."
─ poème	POR18	XVIII. LES ROIS !
─ poème	POR19	XIX. SONNET
─ poème	POR20	XX. "On valsait au salon attenant. Fou de joie,"
─ poème	POR21	XXI. SONNET
─ poème	POR22	XXII. OHIMÉ
─ poème	POR23	XXIII. A MA MÈRE
─ poème	POR24	XXIV. "Elle dormait. Sur la tenture épaisse et sombre"
─ poème	POR25	XXV. "Lorsque je t’ai parlé, puis remis au passage,"
─ poème	POR26	XXVI. "C’était toi qui venais. — Tu t’es soudain montrée"
─ poème	POR27	XXVII. LA LIBERTÉ
─ poème	POR28	XXVIII. "Vous me pardonnerez, si j’ai parlé, Madame ;"
─ poème	POR29	XXIX. "Des vers ? Oh ! m’as-tu dit ; en voici, mais pardon :"
─ poème	POR30	XXX. SONNET
─ poème	POR31	XXXI. "Un peuple ne meurt pas. Tu portes Machabée"
─ poème	POR32	XXXII. CHANSON
─ poème	POR33	XXXIII. "Jane, malgré votre âge insouciant et vain,"
─ poème	POR34	XXXIV. "Je suis un être vil. Je ressemble au tombeau"
─ poème	POR35	XXXV. SONNET - AU LECTEUR
