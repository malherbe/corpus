COP
———
COP_1

François COPPÉE
1842-1908

════════════════════════════════════════════
PROMENADES ET INTÉRIEURS

1872

1057 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I 
	─ poème	COP1	Promenades et Intérieurs

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II 
	─ poème	COP2	Mon père
	─ poème	COP3	Compliment
	─ poème	COP4	Morceau à quatre mains
	─ poème	COP5	Adagio
	─ poème	COP6	L’amazone
	─ poème	COP7	Ritournelle
	─ poème	COP8	La ferme
	─ poème	COP9	La cueillette des cerises
	─ poème	COP10	Le rêve du poète
	─ poème	COP11	La mémoire
	─ poème	COP12	Réponse
	─ poème	COP13	À un ange gardien
	─ poème	COP14	Romance
	─ poème	COP15	Lettre
	─ poème	COP16	Février
	─ poème	COP17	Avril
	─ poème	COP18	Mai
	─ poème	COP19	Juin
	─ poème	COP20	Août
	─ poème	COP21	Décembre

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III 
	─ poème	COP22	En faction
	─ poème	COP23	Le chien perdu
	─ poème	COP24	Tableau rural
	─ poème	COP25	Croquis de banlieue
	─ poème	COP26	Cheval de Renfort
	─ poème	COP27	Au bord de la Marne
	─ poème	COP28	Rythme des vagues
	─ poème	COP29	Matin d’octobre
	─ poème	COP30	Musée de marine
	─ poème	COP31	Nostalgie parisienne

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IV 
	─ poème	COP32	À mes jeunes camarades, aux équipiers du Club nautique de Chatou
	─ poème	COP33	Écrit sur l’Album des Chats d’Henriette Ronner
