THL
———
THL_1

Raymond de la TAILHÈDE
1867-1938

════════════════════════════════════════════
LES POÉSIES

Édition définitive
1887-1926

2209 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES ODES
	─ poème	THL1	ODE TRIOMPHALE
	─────────────────────────────────────────────────────
	▫ LE PREMIER LIVRE DES ODES - (1891-1893)
		─ poème	THL2	ODE I. A JEAN MORÉAS
		─ poème	THL3	ODE II. A JEAN MORÉAS
		─ poème	THL4	ODE III. A MAURICE DU PLESSYS
		─ poème	THL5	ODE IV. A CHARLES MAURRAS
	─────────────────────────────────────────────────────
	▫ LE DEUXIÈME LIVRE DES ODES - (1895-1918)
		─ poème	THL6	ODE I. ÉLOGE D’ATHÈNES
		─ poème	THL7	ODE II. A JEAN MORÉAS
		─ poème	THL8	ODE III. A MAURICE DU PLESSYS
		─ poème	THL9	ODE IV. CHANT DE VICTOIRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES HYMNES - (1892-1894)
	─ poème	THL10	HYMNE I. POUR LA COURONNE
	─ poème	THL11	HYMNE II. POUR LA VICTOIRE
	─ poème	THL12	HYMNE III. POUR LA GLOIRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES SONNETS - (1893-1910)
	─ poème	THL13	I. A JEAN MORÉAS
	─ poème	THL14	II. "Si l’espoir d’un laurier de semence inconnue,"
	─ poème	THL15	III. A ERNEST RAYNAUD
	─ poème	THL16	IV. HÉLÈNE
	─ poème	THL17	V. "Ce n’était pas l’Amour, mais une Ombre ennemie"
	─ poème	THL18	VI. "Impatient des nuits où je pourrai connaître"
	─ poème	THL19	VII. "Tu ne peux te douter, pauvre tête chérie,"
	─ poème	THL20	VIII. "Quand se penchent ce front et cette nuque d’ambre,"
	─ poème	THL21	IX. "Comme un séjour de nuit, habitacle de l’ombre,"
	─ poème	THL22	X. "Trois jours de ses beaux yeux j’ai vu la fleur vivante"
	─ poème	THL23	XI. "O nuit, ô nuit livide incrustée en ma chair,"
	─ poème	THL24	XII. A PAUL VERLAINE - Pour son monument.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES PREMIÈRES POÉSIES - (1887-1890)
	─────────────────────────────────────────────────────
	▫ I LES TRIOMPHES
		─ poème	THL25	I. "Un matin de printemps, plein de vives clartés,"
		─ poème	THL26	II. "Tel, et plus glorieux qu’en ces jours très anciens,"
		─ poème	THL27	APPARITION
		─ poème	THL28	ÉPIPHANIE
		─ poème	THL29	DOULEUR
		─ poème	THL30	A UN ENFANT
		─ poème	THL31	PRÉSAGE
	─────────────────────────────────────────────────────
	▫ II LE TOMBEAU DE JULES TELLIER
		─ poème	THL32	OMBRES
		─ poème	THL33	SOLITUDE
		─ poème	THL34	INVOCATION
		─ poème	THL35	ÉVOCATION
─ poème	THL36	DE LA MÉTAMORPHOSE DES FONTAINES - (1893)
─ poème	THL37	DEUXIÈME ODE TRIOMPHALE - (1926)

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES POÉSIES DIVERSES - (1898-1907)
	─ poème	THL38	I. L’ORÉABE
	─ poème	THL39	II. STROPHES - A CHARLES MAURRAS
	─ poème	THL40	III. A M. PAUL BOUJU
	─ poème	THL41	IV. A MONSIEUR ET A MADAME SILVAIN
	─ poème	THL42	V. A GABRIEL GAZALS
	─ poème	THL43	VI. A PAUL SOUDAY
	─ poème	THL44	VII. A HENRY CHARPENTIER
	─ poème	THL45	VIII. SUR DES EXEMPLAIRES D’ORPHÉE
	─ poème	THL46	IX. LA MUSE GUERRIÈRE
	─ poème	THL47	X. HYMNE POUR LA FRANCE
─ pièce	THL48	AJAX

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ FRAGMENTS ET POÈMES INACHEVÉS
	─ poème	THL49	I. DISCOURS
	─ poème	THL50	II. HYMNE POUR LA JOIE
	─ poème	THL51	III. ORPHÉE
	─ poème	THL52	IV. PROMÉTHÉE
