<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">La Gloire du souvenir</title>
				<title type="medium">Édition électronique</title>
				<author key="SIL">
					<name>
						<forename>Armand</forename>
						<surname>SILVESTRE</surname>
					</name>
					<date from="1837" to="1901">1837-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>385 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">SIL_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Armand Silvestre 1866-1872</title>
						<author>Armand Silvestre</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/armandsylvestrelagloiredusouvenir.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Armand Silvestre 1866-1872</title>
								<author>Armand Silvestre</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5460017w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-02" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="SIL91">
				<lg n="1">
					<l n="1" num="1.1">Ayant écrit ces vers ainsi qu’un testament</l>
					<l n="2" num="1.2">Où du peu que je fus quelque chose demeure,</l>
					<l n="3" num="1.3">Il n’importe aujourd’hui que je vive ou je meure,</l>
					<l n="4" num="1.4">Pourvu qu’ils aient conté mon immortel tourment !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pourvu qu’ils aient charmé, ne fût-ce qu’un moment,</l>
					<l n="6" num="2.2">Fugitifs et lointains comme une voix qui pleure,</l>
					<l n="7" num="2.3">Celle dont je serai, jusqu’à la dernière heure,</l>
					<l n="8" num="2.4">Le triste, le fidèle et l’inutile amant !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Donc, si quelqu’un me dit parjure à la pensée</l>
					<l n="10" num="3.2">Du meilleur de mon sang dans ces lignes tracée,</l>
					<l n="11" num="3.3">Sois là pour me défendre et pour le châtier,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1">Livre ! Car c’est à toi que ma fierté les fie,</l>
					<l n="13" num="4.2">Ces témoins de l’orgueil douloureux de ma vie :</l>
					<l n="14" num="4.3">— Étant tout mon amour, ils sont moi tout entier !</l>
				</lg>
			</div>
			<div type="poem" key="SIL92">
				<head type="main">Prologue</head>
				<head type="sub_1">L’Anniversaire</head>
				<lg n="1">
					<l n="1" num="1.1">Je ne respire plus, dans l’air tiède d’été,</l>
					<l n="2" num="1.2">Les parfums de ton corps et de ta chevelure ;</l>
					<l n="3" num="1.3">Mais comme un feu secret, au fond d’une brûlure,</l>
					<l n="4" num="1.4">Le désir de ta bouche à ma bouche est resté.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tu demeures le Rêve, ayant été la Vie ;</l>
					<l n="6" num="2.2">Mon front encor vaincu cherche ton pied vainqueur :</l>
					<l n="7" num="2.3">Car tu fis de mon être, en déchirant mon cœur,</l>
					<l n="8" num="2.4">Deux parts dont l’une est morte et l’autre inassouvie.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Que fait, à qui connut tes charmes sans pareils,</l>
					<l n="10" num="3.2">L’inutile beauté des songes et des choses ?</l>
					<l n="11" num="3.3">— Sur tes lèvres en fleur j’ai bu l’oubli des roses</l>
					<l n="12" num="3.4">Et dans tes yeux profonds le mépris des soleils !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Donc, n’espérant plus rien des cieux ni de la terre,</l>
					<l n="14" num="4.2">Ni des dieux, ni de toi, ni même de l’oubli,</l>
					<l n="15" num="4.3">Je ne sens vivre en moi, mort mal enseveli,</l>
					<l n="16" num="4.4">Qu’un souvenir pensif, profond et solitaire.</l>
				</lg>
			</div>
			<div type="poem" key="SIL93">
				<head type="number">I</head>
				<lg n="1">
					<l n="1" num="1.1">D’autres peuvent servir la beauté dont je meurs</l>
					<l n="2" num="1.2">Et tomber tour à tour du faîte de leur rêve,</l>
					<l n="3" num="1.3">Avec des cris profonds ou de vaines clameurs :</l>
					<l n="4" num="1.4">— Plus haut qu’eux, en plein ciel, mon rêve, à moi, s’achève.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Depuis que, demeuré sans guide par l’air bleu,</l>
					<l n="6" num="2.2">Pour expier l’affront de l’avoir contemplée,</l>
					<l n="7" num="2.3">S’abaissant pour jamais, ma paupière brûlée</l>
					<l n="8" num="2.4">Enferma sous mon front la vision du feu,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je n’ai jamais maudit, dans mon cœur solitaire,</l>
					<l n="10" num="3.2">Ni son éclat mortel, ni la hauteur des cieux,</l>
					<l n="11" num="3.3">Comme l’aigle aveuglé qui vient heurter la terre</l>
					<l n="12" num="3.4">Quand le soleil trahit l’audace de ses yeux ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Mais, sous la nue immense et par l’azur rebelle,</l>
					<l n="14" num="4.2">L’œil sans lumière, au fond de l’éternel séjour,</l>
					<l n="15" num="4.3">Je vais conter aux dieux qu’Elle seule étant belle,</l>
					<l n="16" num="4.4">Loin d’Elle mes regards n’ont plus souci du jour !</l>
				</lg>
			</div>
			<div type="poem" key="SIL94">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1">Sous la nue où j’erre en silence</l>
					<l n="2" num="1.2">Plus d’une étoile m’a parlé :</l>
					<l n="3" num="1.3">Que fais-tu sous la nuit immense ?</l>
					<l n="4" num="1.4">— J’ai dit : Je suis l’inconsolé.</l>
					<l n="5" num="1.5">J’ai les yeux percés d’une lance !</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Écoute, m’a dit la première,</l>
					<l n="7" num="2.2">Je suis l’Étoile de Pitié :</l>
					<l n="8" num="2.3">Je fais deux parts de ma lumière</l>
					<l n="9" num="2.4">Et je t’en donne la moitié.</l>
					<l n="10" num="2.5">— J’ai dit : Garde-la tout entière !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">L’Étoile d’Amour, la seconde,</l>
					<l n="12" num="3.2">M’a dit : Par mes baisers flottants,</l>
					<l n="13" num="3.3">J’ai sur la vieillesse du monde</l>
					<l n="14" num="3.4">Fermé les blessures du temps.</l>
					<l n="15" num="3.5">— J’ai dit : La mienne est plus profonde !</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">L’Étoile d’Espoir, la troisième,</l>
					<l n="17" num="4.2">M’a dit : Vers tes pas incertains</l>
					<l n="18" num="4.3">Je veux la guider elle-même.</l>
					<l n="19" num="4.4">— Pour rallumer mes yeux éteints</l>
					<l n="20" num="4.5">J’ai dit : Il faut celle que j’aime !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1">Plus de clarté gît en sa main</l>
					<l n="22" num="5.2">Qu’il n’en faudrait pour peupler l’ombre.</l>
					<l n="23" num="5.3">Tout luit en son front surhumain,</l>
					<l n="24" num="5.4">Et Dieu fit, des astres sans nombre,</l>
					<l n="25" num="5.5">La poussière de son chemin !</l>
				</lg>
			</div>
			<div type="poem" key="SIL95">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1">Tel mon cœur, astre obscur que la chaleur déserte,</l>
					<l n="2" num="1.2">Sentait, sous ses pieds nus, rayonner la fierté,</l>
					<l n="3" num="1.3">Et d’un sang rajeuni la vermeille clarté,</l>
					<l n="4" num="1.4">Sous ses ongles, monter à ma poitrine ouverte.</l>
					<l n="5" num="1.5">— Tel mon cœur, astre obscur que la chaleur déserte</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">O torture divine, ô poids doux et sacré</l>
					<l n="7" num="2.2">De son corps virginal en qui la mort nous tente !</l>
					<l n="8" num="2.3">Le rythme de mon souffle, à son pas mesuré,</l>
					<l n="9" num="2.4">S’éteignait au toucher de sa robe flottante.</l>
					<l n="10" num="2.5">— O torture divine, ô poids doux et sacré !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Mais depuis combien d’ans est-elle donc passée ?</l>
					<l n="12" num="3.2">Rien ne marque les temps le long de mon chemin :</l>
					<l n="13" num="3.3">C’est pour l’éternité que mon âme est blessée,</l>
					<l n="14" num="3.4">Et tous les jours sont hier pour un tel lendemain !</l>
					<l n="15" num="3.5">— Mais depuis combien d’ans est-elle donc passée ?</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Je suis épouvanté de me sentir vivant !</l>
					<l n="17" num="4.2">Ma douleur a compté tant de siècles dans l’ombre</l>
					<l n="18" num="4.3">Et tant de vains espoirs dans la plainte du vent !</l>
					<l n="19" num="4.4">Éternel est l’adieu qui fait ma route sombre.</l>
					<l n="20" num="4.5">— Je suis épouvanté de me sentir vivant !</l>
				</lg>
			</div>
			<div type="poem" key="SIL96">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1">Ah ! si l’Étoile de la Mort,</l>
					<l n="2" num="1.2">A ses propres feux consumée,</l>
					<l n="3" num="1.3">N’est plus l’hôtesse accoutumée</l>
					<l n="4" num="1.4">Du souvenir et du remord,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Où fuirai-je, si l’étendue</l>
					<l n="6" num="2.2">S’ouvre à mon vol sans le fermer,</l>
					<l n="7" num="2.3">S’il me faut à jamais t’aimer,</l>
					<l n="8" num="2.4">Toi qui m’es à jamais perdue ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Toi qui passes, rayonnes, luis</l>
					<l n="10" num="3.2">Et fais vivant ce que tu touches,</l>
					<l n="11" num="3.3">Lumière de mes yeux farouches,</l>
					<l n="12" num="3.4">Où fuirai-je, si tu me fuis ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Où s’en va le vent qui m’emporte</l>
					<l n="14" num="4.2">Où gît le repos de mon cœur,</l>
					<l n="15" num="4.3">Puisque, sur ton chemin vainqueur,</l>
					<l n="16" num="4.4">L’Étoile de la Mort est morte !</l>
				</lg>
			</div>
			<div type="poem" key="SIL97">
				<head type="number">V</head>
				<lg n="1">
					<l n="1" num="1.1">Le seul qui monte aux cieux est le bruit lent des flots,</l>
					<l n="2" num="1.2">Quand la Nuit à leur voix ouvre ses grands silences</l>
					<l n="3" num="1.3">Et, comme le sang perle à la cime des lances,</l>
					<l n="4" num="1.4">Égrène, dans l’air froid, leurs rythmiques sanglots.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ainsi que le plongeur qui garde en son oreille</l>
					<l n="6" num="2.2">Le retentissement cadencé de la mer,</l>
					<l n="7" num="2.3">J’ai gardé sous mon front et dans mon cœur amer</l>
					<l n="8" num="2.4">Une voix obstinée, au bruit des flots pareille,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Qui berce ma douleur comme en un lit profond</l>
					<l n="10" num="3.2">Et sur mes désespoirs passe comme une larme,</l>
					<l n="11" num="3.3">Sa voix, sa voix qui pleure et qui ment et qui charme</l>
					<l n="12" num="3.4">Et qui feint de guérir le mal que ses yeux font.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">La Nuit est sans pitié qui m’apporte ce leurre</l>
					<l n="14" num="4.2">D’entendre encor sa voix comme un chuchotement,</l>
					<l n="15" num="4.3">Sa chère voix qui charme et qui pleure et qui ment,</l>
					<l n="16" num="4.4">Sa douce voix qui ment et qui charme et qui pleure !</l>
				</lg>
			</div>
			<div type="poem" key="SIL98">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai rencontré les cygnes blancs</l>
					<l n="2" num="1.2">Qui, leurs grandes ailes tendues,</l>
					<l n="3" num="1.3">Allongeaient vers les étendues</l>
					<l n="4" num="1.4">Leurs cous nobles et nonchalants,</l>
					<l n="5" num="1.5">— Leurs cous pareils aux bras tremblants,</l>
					<l n="6" num="1.6">Dont les caresses sont perdues.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Leur vol égal et fraternel</l>
					<l n="8" num="2.2">Battait lourdement l’air qui passe ;</l>
					<l n="9" num="2.3">Moroses, ils fendaient l’espace</l>
					<l n="10" num="2.4">Où palpite un flux éternel,</l>
					<l n="11" num="2.5">— Et leur cortège solennel</l>
					<l n="12" num="2.6">Fuyait sans y creuser de trace.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">— Changez d’azur, doux exilés !</l>
					<l n="14" num="3.2">Désertez à jamais la terre.</l>
					<l n="15" num="3.3">Mais qui vous rendra le mystère</l>
					<l n="16" num="3.4">Des lacs par la nuit étoilés ?</l>
					<l n="17" num="3.5">Frères, je vais où vous allez :</l>
					<l n="18" num="3.6">Emportez mon cœur solitaire !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Un souffle amer nous a meurtris</l>
					<l n="20" num="4.2">Et sa grande aile est déchirée :</l>
					<l n="21" num="4.3">Celle qui me fut adorée</l>
					<l n="22" num="4.4">Loin d’elle a chassé mes esprits.</l>
					<l n="23" num="4.5">— C’est elle qui nous a proscrits,</l>
					<l n="24" num="4.6">Répondit la troupe sacrée.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Nos honneurs sont ensevelis :</l>
					<l n="26" num="5.2">Nous étions la blancheur ailée</l>
					<l n="27" num="5.3">Dont, un jour, s’était envolée</l>
					<l n="28" num="5.4">L’auréole des fronts pâlis !</l>
					<l n="29" num="5.5">— Nous étions la blancheur des lis</l>
					<l n="30" num="5.6">Et de la neige immaculée.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Mais devant son corps enchanté</l>
					<l n="32" num="6.2">Nos clartés sont des ombres vaines :</l>
					<l n="33" num="6.3">L’azur frémissant de ses veines</l>
					<l n="34" num="6.4">Court au bord de son front lacté.</l>
					<l n="35" num="6.5">— Elle est l’immortelle Beauté</l>
					<l n="36" num="6.6">Faite des choses souveraines.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Devant la grâce de ses traits</l>
					<l n="38" num="7.2">Toutes grâces sont défendues.</l>
					<l n="39" num="7.3">— Fuyez seuls vers les étendues,</l>
					<l n="40" num="7.4">Doux oiseaux, et mourez après ;</l>
					<l n="41" num="7.5">Car à ses pieds je volerais</l>
					<l n="42" num="7.6">Si des ailes m’étaient rendues !</l>
				</lg>
			</div>
			<div type="poem" key="SIL99">
				<head type="number">VII</head>
				<lg n="1">
					<l n="1" num="1.1">Ô pâleur, ô clarté nocturne de son front,</l>
					<l n="2" num="1.2">Rayon lunaire pris au frêle réseau d’ombre</l>
					<l n="3" num="1.3">Que jette autour de soi sa chevelure sombre,</l>
					<l n="4" num="1.4">Pour être éteints, mes yeux jamais ne t’oublîront.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Mon esprit veille encore à ta lueur brisée,</l>
					<l n="6" num="2.2">Lampe marmoréenne, admirable flambeau !</l>
					<l n="7" num="2.3">Si la pitié des dieux te laisse à mon tombeau,</l>
					<l n="8" num="2.4">Le sommeil sera doux à mon ombre apaisée.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">O blancheur, ô clarté froide de ses seins nus,</l>
					<l n="10" num="3.2">O soleil prisonnier sur la neige vivante,</l>
					<l n="11" num="3.3">O brûlure de glace, ô fraîcheur décevante,</l>
					<l n="12" num="3.4">Éclairs d’acier ! je meurs de vous avoir connus.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et mon cœur est pendu comme au bout d’une épée,</l>
					<l n="14" num="4.2">Au souvenir aigu de ses seins triomphants !</l>
					<l n="15" num="4.3">— Mais la douleur m’apprit la douceur des enfants</l>
					<l n="16" num="4.4">Et mon âme bénit celle qui l’a frappée.</l>
				</lg>
			</div>
			<div type="poem" key="SIL100">
				<head type="number">VIII</head>
				<lg n="1">
					<l n="1" num="1.1">Si tout mon sang fuit de mon cœur,</l>
					<l n="2" num="1.2">Je veux que, sous ton pied vainqueur,</l>
					<l n="3" num="1.3">En tombe la dernière goutte !</l>
					<l n="4" num="1.4">— Mon âme errante y sera toute.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Des roses rouges en naîtront</l>
					<l n="6" num="2.2">Qui vers ta bouche élèveront,</l>
					<l n="7" num="2.3">Comme des lèvres enflammées,</l>
					<l n="8" num="2.4">Mes blessures jamais fermées.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Pour le calice de ces fleurs</l>
					<l n="10" num="3.2">L’Aurore n’aura pas de pleurs,</l>
					<l n="11" num="3.3">Les voyant pleines tout entières</l>
					<l n="12" num="3.4">De ceux qu’ont versés mes paupières.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Si vers leur parfum languissant</l>
					<l n="14" num="4.2">Tu penches la tête, en passant,</l>
					<l n="15" num="4.3">Comme un souffle qu’on effarouche,</l>
					<l n="16" num="4.4">Mon âme en fuira sur ta bouche.</l>
				</lg>
			</div>
			<div type="poem" key="SIL101">
				<head type="number">IX</head>
				<lg n="1">
					<l n="1" num="1.1">La lèvre a bu le souffle à a lèvre des fleurs.</l>
					<l n="2" num="1.2">Lorsque tes yeux ont pris à l’Aube la lumière,</l>
					<l n="3" num="1.3">Sous l’Aube en feu ta lèvre a bu, parmi ses pleurs,</l>
					<l n="4" num="1.4">Leur grâce à peine ouverte et leur odeur première.</l>
					<l n="5" num="1.5">— Ta lèvre a bu le souffle à la lèvre des fleurs.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Et c’est pourquoi ton sein, gonflé de leur haleine,</l>
					<l n="7" num="2.2">Monte et s’épanouit dans la blancheur des lis,</l>
					<l n="8" num="2.3">Et toutes les splendeurs dont ta jeunesse est pleine</l>
					<l n="9" num="2.4">Exhalent les parfums longtemps ensevelis</l>
					<l n="10" num="2.5">Dans ton sein virginal gonflé de leur haleine.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Comme un ferment sacré qui tend vers les soleils,</l>
					<l n="12" num="3.2">Et jaloux de renaître en ta beauté profonde,</l>
					<l n="13" num="3.3">Ces germes odorants font tes charmes pareils</l>
					<l n="14" num="3.4">A l’épanouissement du printemps sur le monde.</l>
					<l n="15" num="3.5">— Tel un ferment sacré qui tend vers les soleils.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Et seules, du printemps éternel exilées,</l>
					<l n="17" num="4.2">Meurent les*tristes fleurs qui naissent de mon sang ;</l>
					<l n="18" num="4.3">Les fleurs, sur le sol nu par ton pied nu foulées,</l>
					<l n="19" num="4.4">Sans monter jusqu’à toi leur parfum languissant,</l>
					<l n="20" num="4.5">Les fleurs, les seules fleurs du printemps exilées !</l>
				</lg>
			</div>
			<div type="poem" key="SIL102">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1">Les aveugles et les amants,</l>
					<l n="2" num="1.2">A qui la clarté fut ravie,</l>
					<l n="3" num="1.3">Vivent exilés de la vie.</l>
					<l n="4" num="1.4">Et je sais leurs divins tourments.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ceux-là surtout dont la paupière</l>
					<l n="6" num="2.2">A connu l’Aube et la Beauté,</l>
					<l n="7" num="2.3">Dont le souvenir s’est sculpté,</l>
					<l n="8" num="2.4">Fixe, dans un rêve de pierre,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ceux que l’immuable a faits siens,</l>
					<l n="10" num="3.2">Prisonniers de la nuit profonde,</l>
					<l n="11" num="3.3">Et dont l’âme enferme le monde</l>
					<l n="12" num="3.4">De tous leurs bonheurs anciens.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Loin d’eux l’Aube et la Bien-aimée</l>
					<l n="14" num="4.2">Réveillent des cœurs et des yeux</l>
					<l n="15" num="4.3">Et, sur des fantômes joyeux,</l>
					<l n="16" num="4.4">Versent la grâce accoutumée.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Mais leurs yeux, dont l’ombre a banni</l>
					<l n="18" num="5.2">L’image troublante des choses,</l>
					<l n="19" num="5.3">Derrière leurs paupières closes,</l>
					<l n="20" num="5.4">Se sont tournés vers l’infini</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Où, pour la splendeur sidérale</l>
					<l n="22" num="6.2">Désertant le charme maudit,</l>
					<l n="23" num="6.3">L’Idole, étant dieu, resplendit</l>
					<l n="24" num="6.4">Dans une lumière idéale.</l>
				</lg>
			</div>
			<div type="poem" key="SIL103">
				<head type="number">XI</head>
				<lg n="1">
					<l n="1" num="1.1">Ah ! je me trompe en vain moi-même et j’ai menti :</l>
					<l n="2" num="1.2">Car le rayonnement de ta gloire charnelle</l>
					<l n="3" num="1.3">A brûlé dans mes yeux la lumière éternelle</l>
					<l n="4" num="1.4">Et le vide a peuplé mon front appesanti.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Comme au choc de la foudre un marbre se fait sable,</l>
					<l n="6" num="2.2">J’ai senti fondre en moi l’antique Vérité :</l>
					<l n="7" num="2.3">Rien n’est divin que Toi, n’est saint que ta Beauté,</l>
					<l n="8" num="2.4">Et rien n’est éternel que ton corps périssable !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Rien n’est vrai que ta bouche où la parole ment,</l>
					<l n="10" num="3.2">Juste que le caprice errant de ta pensée,</l>
					<l n="11" num="3.3">Doux que le mal cruel dont mon âme est blessée,</l>
					<l n="12" num="3.4">Et sûr que le fragile espoir de mon tourment !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Car je suis le damné de ta Beauté profonde,</l>
					<l n="14" num="4.2">Le douloureux amant que veut ta cruauté,</l>
					<l n="15" num="4.3">Et, pareil au Titan par les cieux emporté,</l>
					<l n="16" num="4.4">Où se heurte mon cœur j’y sens périr un monde !</l>
				</lg>
			</div>
			<div type="poem" key="SIL104">
				<head type="number">XII</head>
				<lg n="1">
					<l n="1" num="1.1">O poussière d’astres brisés</l>
					<l n="2" num="1.2">Que soulève le vent nocturne,</l>
					<l n="3" num="1.3">Jusqu’au firmament taciturne</l>
					<l n="4" num="1.4">Monte tes lumineux baisers.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">O lumière pâle et lactée,</l>
					<l n="6" num="2.2">Cendre d’argent d’astres éteints,</l>
					<l n="7" num="2.3">Au bord des horizons lointains</l>
					<l n="8" num="2.4">Monte ta blancheur enchantée.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— Tombé des cieux, je leur ai pris</l>
					<l n="10" num="3.2">Des étoiles, et vais, sans trêve,</l>
					<l n="11" num="3.3">Dans l’or dispersé de mon rêve,</l>
					<l n="12" num="3.4">Couronné de ses vains débris.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et parmi la vaine fumée</l>
					<l n="14" num="4.2">Où s’en va ce qui fut mon cœur,</l>
					<l n="15" num="4.3">Tout blanc, ton fantôme vainqueur</l>
					<l n="16" num="4.4">M’enveloppe, ô ma bien-aimée !</l>
				</lg>
			</div>
			<div type="poem" key="SIL105">
				<head type="number">XIII</head>
				<lg n="1">
					<l n="1" num="1.1">Ayant fait prisonniers mon esprit et mes yeux,</l>
					<l n="2" num="1.2">La chère vision sans cesse les promène</l>
					<l n="3" num="1.3">Sous la caresse lente et le frisson soyeux</l>
					<l n="4" num="1.4">Des formes où fleurit ta beauté surhumaine.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elle emplit de langueurs douloureuses ma chair,</l>
					<l n="6" num="2.2">Et fait monter le flot des baisers à ma bouche ;</l>
					<l n="7" num="2.3">Car dans l’inanité décevante de l’air,</l>
					<l n="8" num="2.4">C’est toi que je respire et c’est toi que je touche !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">C’est toi qui bois le souffle où mon âme a passé</l>
					<l n="10" num="3.2">Et creuses dans mon sein les profondes angoisses</l>
					<l n="11" num="3.3">Et, plus étroitement que l’habit que tu froisses,</l>
					<l n="12" num="3.4">Autour de ton beau corps tiens mon être enlacé.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et du mal dont je meurs je ne sais plus la place ;</l>
					<l n="14" num="4.2">Car, esclave meurtri de ton corps triomphant,</l>
					<l n="15" num="4.3">Sous sa blancheur de neige, une étreinte de glace,</l>
					<l n="16" num="4.4">Comme un arbre captif, a pris mon cœur vivant !</l>
				</lg>
			</div>
			<div type="poem" key="SIL106">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1">L’impérissable orgueil de mon cœur vient de celle</l>
					<l n="2" num="1.2">Qui daigna sur mon cœur poser son pied divin</l>
					<l n="3" num="1.3">Très longtemps et très fort,— afin qu’il se souvint :</l>
					<l n="4" num="1.4">Depuis je n’ai connu la douleur que par elle ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Car j’ai souffert des maux qu’elle n’espérait pas,</l>
					<l n="6" num="2.2">Fier du sillon saignant qu’elle ouvrit dans mon être</l>
					<l n="7" num="2.3">Et qui des dieux jaloux me fera reconnaître ;</l>
					<l n="8" num="2.4">O gloire ! j’ai servi de poussière à ses pas !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et je reste meurtri loin de la route ailée</l>
					<l n="10" num="3.2">Où sa course égarait le caprice des cieux,</l>
					<l n="11" num="3.3">Meurtri, vide, et pareil à l’air silencieux</l>
					<l n="12" num="3.4">Que brûle encor le vol d’une étoile envolée.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Sidérale blancheur du front pur qui, vers moi,</l>
					<l n="14" num="4.2">Pencha du firmament la lumière sacrée,</l>
					<l n="15" num="4.3">Vision tout entière en mon cœur demeurée,</l>
					<l n="16" num="4.4">L’impérissable orgueil de mon cœur vient de toi !</l>
				</lg>
			</div>
			<div type="poem" key="SIL107">
				<head type="number">XV</head>
				<lg n="1">
					<l n="1" num="1.1">Je dirai ta beauté perdue à ceux qu’offense</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"/>La superbe de ma douleur,</l>
					<l n="3" num="1.3">Ton front marmoréen, éternelle pâleur,</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"/>Ton sourire, éternelle enfance ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Et tes yeux au regard magnétique et profond</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"/>Pareil aux lampes vénérées</l>
					<l n="7" num="2.3">Qu’un jour intérieur illumine et qui font</l>
					<l n="8" num="2.4"><space quantity="8" unit="char"/>Palpiter les ombres sacrées ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et l’éclat de ton col dressé jusqu’à l’orgueil</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"/>De ta face où dort la lumière,</l>
					<l n="11" num="3.3">La fête de ton teint lilial et le deuil</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"/>De ta sombre et lourde crinière ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et tout ce qui me fut le suprême abandon</l>
					<l n="14" num="4.2"><space quantity="8" unit="char"/>Des Cieux, du Rêve et de la Vie,</l>
					<l n="15" num="4.3">Ta beauté surhumaine où mon âme asservie</l>
					<l n="16" num="4.4"><space quantity="8" unit="char"/>Trouve sa gloire et ton pardon !</l>
				</lg>
			</div>
			<div type="poem" key="SIL108">
				<head type="number">XVI</head>
				<lg n="1">
					<l n="1" num="1.1">Sous les cieux que peuplait de ses grâces robustes</l>
					<l n="2" num="1.2">L’héroïque troupeau des filles d’Astarté,</l>
					<l n="3" num="1.3">Calme, j’aurais été, durant l’éternité,</l>
					<l n="4" num="1.4">Le familier discret de tes formes augustes.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">A l’ombre des splendeurs sereines de ton corps</l>
					<l n="6" num="2.2">J’aurais dormi le rêve éternel que je pleure,</l>
					<l n="7" num="2.3">Absous des trahisons de l’espace et de l’heure</l>
					<l n="8" num="2.4">Qui font tous nos pensers douloureux et discords ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et d’une mort sans fin plus douce que la vie</l>
					<l n="10" num="3.2">Ta lèvre eût mesuré, seule, l’enivrement</l>
					<l n="11" num="3.3">A mes sens confondus dans l’immense tourment</l>
					<l n="12" num="3.4">Dont Vénus embrasait l’immensité ravie.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">O douleur !— le temps fuit ; le temps brise,— tu pars,</l>
					<l n="14" num="4.2">Et, des bûchers mortels dédaignant la brûlure,</l>
					<l n="15" num="4.3">Tu t’enfuis emportant, parmi ta chevelure,</l>
					<l n="16" num="4.4">De mes cieux déchirés tous les astres épars.</l>
				</lg>
			</div>
			<div type="poem" key="SIL109">
				<head type="number">XVII</head>
				<lg n="1">
					<l n="1" num="1.1">Et pourtant l’infini qu’en leur vol diaphane</l>
					<l n="2" num="1.2">Poursuivent, sous ton front, tes rêves surhumains</l>
					<l n="3" num="1.3">Je l’enfermai pour toi, moi mortel, moi profane,</l>
					<l n="4" num="1.4">Dans mon cœur élargi par mes sanglantes mains.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dans ma poitrine ouverte, argile sacrilège,</l>
					<l n="6" num="2.2">J’avais senti passer l’âme errante des Cieux,</l>
					<l n="7" num="2.3">Portant, comme un parfum, jusqu’à tes pieds de neige</l>
					<l n="8" num="2.4">L’immense amour qui fait l’azur silencieux,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Qui fait la mer pensive et tristes les étoiles</l>
					<l n="10" num="3.2">Dans l’air vibrant du soir que bat son aile en feu,</l>
					<l n="11" num="3.3">Qui fait la nuit sacrée et sème ses longs voiles</l>
					<l n="12" num="3.4">D’astres brûlants tombés des paupières d’un dieu.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ces pleurs divins, ces pleurs que ton orgueil réclame,</l>
					<l n="14" num="4.2">Cet infini qui fait ton mal et ta pâleur,</l>
					<l n="15" num="4.3">Pour toi je l’ai porté tour à tour, dans mon âme,</l>
					<l n="16" num="4.4">Vivant, dans mon amour, et mort, dans ma douleur !</l>
				</lg>
			</div>
			<div type="poem" key="SIL110">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1">La fierté de mon être ici gît tout entière :</l>
					<l n="2" num="1.2">Mesurant au tombeau l’amour enseveli,</l>
					<l n="3" num="1.3">J’ai jugé sa grandeur à peser sa poussière,</l>
					<l n="4" num="1.4">Et pour lui ne crains pas l’outrage de l’oubli.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">A l’horizon perdu des visions aimées,</l>
					<l n="6" num="2.2">Son spectre, chaque jour, se lève grandissant,</l>
					<l n="7" num="2.3">Et, comme un soleil rouge au travers des fumées,</l>
					<l n="8" num="2.4">Teint ces pâles brouillards du meilleur de mon sang.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">En fuyant vers l’azur malgré toi tu l’emportes</l>
					<l n="10" num="3.2">Dans le pli virginal de tes voiles sacrés,</l>
					<l n="11" num="3.3">Ce sang vermeil et doux des illusions mortes</l>
					<l n="12" num="3.4">Dont ma veine a rougi tes beaux pieds adorés.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et je monte, vivant, avec toi sur la cime</l>
					<l n="14" num="4.2">Où te suit sans merci mon amour obsesseur,</l>
					<l n="15" num="4.3">Palpitant comme toi de ton rêve sublime,</l>
					<l n="16" num="4.4">Fille auguste et terrible, ô cruelle ! ô ma sœur !</l>
				</lg>
			</div>
			<div type="poem" key="SIL111">
				<head type="main">Épilogue</head>
				<head type="sub_1">Le Testament</head>
				<lg n="1">
					<l n="1" num="1.1">Du temps et de l’oubli bravant la flétrissure,</l>
					<l n="2" num="1.2">J’ai porté mon amour superbe par les cieux ;</l>
					<l n="3" num="1.3">Laissant couler mon sang, j’ai caché ma blessure,</l>
					<l n="4" num="1.4">Et mon rire navré but les pleurs de mes yeux.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ne méritant de Toi ni pitié ni colère,</l>
					<l n="6" num="2.2">Brisé, mais non vaincu, j’attends l’heur de mourir,</l>
					<l n="7" num="2.3">— De ma longue vertu dédaignant le salaire,</l>
					<l n="8" num="2.4">Par Toi j’ai tout perdu, fors l’orgueil de souffrir !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je savais que t’aimer était une œuvre impie</l>
					<l n="10" num="3.2">Et j’ai jeté mon être en proie à ta Beauté.</l>
					<l n="11" num="3.3">— Jaloux de ma douleur, fier du mal que j’expie,</l>
					<l n="12" num="3.4">Je marche, en te chantant, vers l’immortalité.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et maintenant j’ai dit :— Sois belle et sois aimée ;</l>
					<l n="14" num="4.2">Ouvre à qui veut mourir le tombeau de ton cœur.</l>
					<l n="15" num="4.3">— Dans mon amour viril j’ai ma gloire enfermée,</l>
					<l n="16" num="4.4">Et de l’oubli, ton nom fera mon nom vainqueur !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">Août 1872</date>.
					</dateline>
				</closer>
			</div>
		</body>
	</text>
</TEI>