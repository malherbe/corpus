SIL
———
SIL_3

Armand SILVESTRE
1837-1901

════════════════════════════════════════════
La Gloire du souvenir

1872

385 vers

─ poème	SIL91	"Ayant écrit ces vers ainsi qu’un testament"
─ poème	SIL92	Prologue
─ poème	SIL93	I. "D’autres peuvent servir la beauté dont je meurs"
─ poème	SIL94	II. "Sous la nue où j’erre en silence"
─ poème	SIL95	III. "Tel mon cœur, astre obscur que la chaleur déserte,"
─ poème	SIL96	IV. "Ah ! si l’Étoile de la Mort,"
─ poème	SIL97	V. "Le seul qui monte aux cieux est le bruit lent des flots,"
─ poème	SIL98	VI. "J’ai rencontré les cygnes blancs"
─ poème	SIL99	VII. "Ô pâleur, ô clarté nocturne de son front,"
─ poème	SIL100	VIII. "Si tout mon sang fuit de mon cœur,"
─ poème	SIL101	IX. "La lèvre a bu le souffle à a lèvre des fleurs."
─ poème	SIL102	X. "Les aveugles et les amants,"
─ poème	SIL103	XI. "Ah ! je me trompe en vain moi-même et j’ai menti :"
─ poème	SIL104	XII. "O poussière d’astres brisés"
─ poème	SIL105	XIII. "Ayant fait prisonniers mon esprit et mes yeux,"
─ poème	SIL106	XIV. "L’impérissable orgueil de mon cœur vient de celle"
─ poème	SIL107	XV. "Je dirai ta beauté perdue à ceux qu’offense"
─ poème	SIL108	XVI. "Sous les cieux que peuplait de ses grâces robustes"
─ poème	SIL109	XVII. "Et pourtant l’infini qu’en leur vol diaphane"
─ poème	SIL110	XVIII. "La fierté de mon être ici gît tout entière :"
─ poème	SIL111	Épilogue
