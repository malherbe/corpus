Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
LAP
LAP_1

Victor de LAPRADE
1812-1883

LES PARFUMS DE MAGDELEINE
1839
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Les parfums de Magdeleine
Victor Laprade


https://gallica.bnf.fr/ark:/12148/bpt6k54059278?rk=364808;4


Les parfums de Magdeleine
Victor Laprade

LYON
IMPRIMERIE DE L. BOITEL
1839




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



LES PARFUMS DE MAGDELEINE

En ce temps-là, ce fut une joie infinie
Chez tous les habitants du bourg de Béthanie ;
Un pasteur avait vu, loin des chemins foulés,
Des voyageurs pensifs venir le long des blés,
Et, courant le premier, à la foule jalouse
Il avait annoncé le Seigneur et les Douze.
Or, comme aux jours anciens, par les vieillards rangé,
Le peuple s’assemblait près d’un puits ombragé
Et, marchant vers Jésus, les enfants et les femmes
Dont sa voix caressait si doucement les âmes,
Répandaient à ses pieds des palmes d’Amana,
Se pressaient pour l’entendre, et criaient : Hosanna !


Et la joie éclatait, plus féconde et plus vive,
Sous le toit où devait s’asseoir un tel convive;
Chez Simon qu’il aimait et qu’il avait guéri,
Les élus attendaient l’hôte illustre et chéri,
Et, mêlant de doux soins au chant des saints cantiques,
Des vases solennels puisaient les vins antiques.


Comme un ardent parvis aux Pâques préparé
Le cénacle s’ouvrait rayonnant et paré ;
Seule au bord du Cédron, pour en orner l’enceinte
Marie avait cueilli le lys et l’hyacinthe,
De myrrhe et d’aloès frotté le cèdre noir ;
La table reluisait claire comme un miroir,
Et des tresses de fleurs erraient, collier fragile,
Sur le col rougissant des amphores d’argile.


Marthe au divin banquet n’avait rien épargné ;
Une active rougeur parait son front baigné ;
Elle avait elle-même, entre des branches vertes.
Servi les blonds raisins, les grenades ouvertes,
Les figues du Carmel, le miel pur de Membré,
Et le poisson des lacs, et l’azyme doré,
Et l’agneau, qui n’avait qu’une semaine entière
Sur les Monts Galaad brouté la sauge amère,.
Et le vin parfumé des vignes d’Engaddi,
Que baise avec amour le soleil de midi.


Or, autour du festin les Douze se rangèrent,
Et des baisers de paix entre eux tous s’échangèrent.


Et le Maître s’assit ; ses regards étaient doux ;
Son front blanc, couronné par de longs cheveux roux,
Avait dans sa beauté sereine et reposée
Une grâce ineffable et pleine de pensée ;
L’ardente charité, nimbe d’or et de feu,
Rayonnait de sa face avec l’esprit de Dieu.
Un manteau bleu s’ouvrait sur sa rouge tunique,
Ouvrage de sa mère et d’une pièce unique,
Mystérieux tissu qu’un prophète chanta,
Voile du corps sacré promis au Golgotha.
Devant Jésus était le pêcheur d’hommes, Pierre,
Le futur fondement de son église entière,
Né pour la foi robuste, et fait à l’action,
Tête chauve et brunie où vit la passion.
Mais la meilleure place était celle d’un autre
Jeune homme aux blonds cheveux, chaste et suave apôtre,
Et qui, les yeux rêveurs et baignés à demi,
S’appuyait sur le sein de son divin ami,
Âme où le Christ versait sa parole secrète
Jean, l’élu de son cœur, le disciple poète !


Et la sainte amitié, vin des vignes du ciel.
Circulait entre tous au banquet fraternel.


Or, celle qu’on nommait Marie et Magdeleine,
Perle de beauté rare, et fleur de douce haleine,
Femme qui par le cœur avait beaucoup péché,
Magdeleine était là, triste et le corps penché ;
Cette âme avait tari plus d’une source amère,
Avant de rencontrer l’onde qui désaltère,
Et sa soif, survivant à mille espoirs déçus,
Puisait avec amour aux leçons de Jésus.
A genoux, et joignant ses deux mains, l’humble femme .
Priait et soupirait, du profond de son âme ;
Tremblante, se voilant sous l’or de ses cheveux,
Elle cherchait les yeux du Christ avec ses yeux;
Courbait son front rougi par une intime fièvre,.
Sur les pieds de Jésus purifiait sa lèvre,
Et pleurait doucement le passé plein d’ennui
Où ses larmes coulaient pour d’autres que pour lui,
Jésus était pensif; or, la sœur de Lazare
Dans un vase d’albâtre avait un baume rare
Apporté du désert, et, plus loin que Memphis,
Fait d’une fleur qui croît au bord des Oasis.
Le parfum s’épurait dans l’urne diaphane ;
Elle l’avait gardé de tout emploi profane,
Et venait à la fin, son jour s’étant levé,
Au Dieu de son attente offrir l’encens sauvé.
Dans un épanchement de douleur et d’extase,
Sur le corps de Jésus elle rompit le vase ;
De larmes et de baume elle baigna ses pieds,
Les retint doucement sur son sein appuyés,
Et de ses blonds cheveux pressant leur chaste ivoire,
Longtemps elle essuya le flot expiatoire.


Et le parfum montait ; la salle du festin
Fumait comme un bois vierge au soleil du matin ;
Et l’air, tout imprégné des essences divines,
Vivifiait le sang dans toutes les poitrines.
Alors, devant Jésus, il se fit un moment
D’un silence rêveur tout plein d’épanchement.


Mais tout-à-coup tombant, comme une pierre aride,
Une voix vint troubler cette extase limpide.


Elle disait : « Chassez cette femme d’ici !
« Les agneaux et les boucs se mêlent-ils ainsi?
« Le Maître ne sait pas quelles lèvres impures
« Osent à sa personne essuyer leurs souillures.
« Croit-on qu’un peu d’encens et de pleurs épanchés
« Achètent le pardon et lavent les péchés !
« Il faut pour sauver l’aine une foi plus active,
« La loi ne connaît point de pénitence oisive,
« Le luxe et lès parfums sont maudits des élus ;
« C’est mal de se complaire à ces biens superflus,
« De s’attendrir ainsi sur des larmes fleuries ;
« Le péché suit de près les molles rêveries.
« Et ce baume, d’ailleurs, valait beaucoup d’argent;
« Le perdre, c’est voler du pain à l’indigent ;
« Car, pour faire l’aumône, il aurait bien pu rendre
« Trois cents deniers au moins, si l’on eût su le vendre ! »
Et les frères troublés dans le fond de leur cœur
Tournèrent à la fois les yeux vers le Seigneur.


Et lui, sur l’humble femme étendant ses mains pures :
« Oh! ne là froissez pas de vos paroles dures;
« Hommes de peu d’amour, elle a fait mieux que vous !
« Voyez mes pieds meurtris qu’elle essuye à genoux,
« Ses yeux en ont lavé le sang et la poussière ;
« Elle a de ses parfums répandu l’urne entière,
« Et, tandis que ses pleurs jaillissaient en ruisseau,.
« Je n’ai pas eu de vous même une goutte d’eau !
« Vous n’êtes pas venus, mes hôtes, mes apôtres,
« Presser en m’abordant mes lèvres sur les vôtres;
« Marie a sur son cœur posé mes pieds brisés,
« Et les réchauffe encor de ses pieux baisers ;
« Son amour vigilant a pressenti mon heure;
« Sur mon corps embaumé, par avance, elle pleure…
« Oui, pour l’aumône même, un trésor amassé,
« Ne vaudrait pas l’encens que Marie a versé !
« Vous aurez jusqu’au bout des pauvres sur la terre,
« Hommes! espérez-vous m’avoir toujours pour frère?
« Magdeleine a péché, mais au livre des cieux
« Elle a blanchi sa page avec l’eau de ses yeux,
« Et le Seigneur lui doit, juste dans sa clémence,
« Un immense pardon pour son amour immense.
« Je vous le dis, tous ceux à qui sera porté
« Le Verbe de la pais et de la charité,
« Diront de cette femme, en chantant ses louanges,
» Qu’elle a fait ce qu’au ciel doivent faire les anges,
« Et qu’elle montera, ses péchés expiés,
« Poser encor, là haut, des baisers sur mes pieds ! »


Et le Maître sortit ; aux portes du cénacle
Des malades couchés attendaient un miracle.


Or, Jean restait le front dans sa main, et rêveur
Sondait comme une mer le discours du Sauveur.

❃

Oh ! s’ils viennent pensifs s’asseoir entre vos fêtes,
Versez l’ambre et le nard sur les pieds des prophètes ;
A vos larmes d’amour, au fond des urnes d’or,
Mêlez pour eux les pleurs des roses de Ségor !
Est-ce donc pour la brise ou l’ombre solitaire,
Que Dieu mit des : parfums dans les. fleurs de la terre ?
Est-ce pour y mourir, desséché par l’orgueil
Qu’un ruisseau tiède et pur tremble au fond,de chaque œil ;
Et pour s’éteindre, avant de jeter une flamme,
Qu’un doux soleil se lève au matin de notre âme?


SEIGNEUR, quand vous avez en un cœur sans détour
De la perfection semé le noble amour,
Qu’ensuite vous ouvrez à ces âmes ailées
Un champ libre à travers vos œuvres étoilées,
Vos splendides jardins, votre ciel argenté,
Et tout ce qui nous voile enfin votre beauté;
Si quelque pauvre enfant que votre soif dévore,
Et qui pour vous chercher s’est levé dès l’aurore,
D’une merveille à l’autre, avant de vous trouver,
Vole, et lassé s’y pose un instant pour rêver;
Dans le creux de sa main puise au bord des fontaines,
Et sans route frayée en ces terres lointaines,
S’égare et dort un soir,, doucement attiré,
Auprès d’une fleur rare ou d’un oiseau doré ;
Ou bien si tout meurtri des,pierres de la route,
Sans rien à l’horizon, il se couche et s’il doute;
Lorsqu’il voit luire enfin la splendeur de vos pieds
Et qu’il se traîne à vous, sur ses genoux pliés…
De ces larmes sous qui toute tache s’efface
Pourrez-vous, ô Seigneur, détourner votre face !


Les pleurs ne-sont-ils .pas, des diamants-cachés
Qui payent, en tombant, le prix de nos péchés?
Chaste sueur de l’âme impuissante et brisée,
Par un Dieu qui pleura seriez-vous méprisée?


Larmes du repentir ! eau féconde toujours !
Quand l’homme vous répand sur tous ses mauvais jours,
Vous chassez de son cœur les fanges entassées,
Sous les pieds remuants des coupables pensées ;
Puis, comme le soleil sur une terre en pleurs
Raffermit les chemins et relève les fleurs,
Un doux regard de Dieu, suivant l’ombre et la pluie,.
Se répand sur l’esprit, le réchauffe et l’essuie !


DANS l’urne aux blancs contours que de fleurs ont pleuré
Pour l’emplir jusqu’au-bord d’un encens épuré !
Oh! que tout soit pour lui, donnez, ô Magdeleine,
Versez, sur ses pieds nus, votre âme humide et pleine,
Versez le fond du vase et les parfums cachés,
Les regrets, les espoirs, tout, jusqu’à vos péchés !
Versez les chastes jours et les nuits profanées,
Et l’asphodèle vierge et les roses fanées ;
Versez votre douleur, versez votre beauté.
Tout en vous est parfum, et tout sera compté !
Brisez au pied du Christ ce cœur doux et fragile ;
Ce que la loi rejette est pris par l’Évangile,
Des épis oubliés sa moisson s’enrichit ;
A lui tout ce qui pleure, et tout ce qui fléchit,
A lui la pénitente obscure et méprisée,
A lui le nid sans mère, et la branche brisée,
A lui tout ce qui vit sans filer ni semer,
A lui le lys des champs qui ne sait qu’embaumer,
L’oiseau qui vole au ciel, insoucieux, et chante;
A lui la beauté frêle, et l’enfance touchante,
Et ces hommes rêveurs qui sont toujours enfants,
Tous ceux sur qui le fort met ses pieds triomphants !
Les faibles sont les siens, sa force les relève;
Il porte dans ses mains la grâce et non le glaive.


UNE eau mystérieuse a baigné vos genoux !
Le ciel même, ô Seigneur, a-t-il rien de plus doux?
A ces flots onctueux, fumant d’un double arome,
L’homme a fourni les pleurs et la terre le baume,
Tous les deux vous offrant leurs présents les meilleurs,
La nature ses fleurs, et Famé ses douleurs,
Puis, versant tous les deux sur vos traces sereines
Ce que vous-ayez mis de plus pur dans leurs veines!


Larmes ! trésor vivant, perles de vérité !
Seul don qu’offre le cœur sans l’avoir emprunté !
Baume que le soleil fait monter goutte à goutte
Et surnager de l’ame en la consumant toute !
Vin que fait du palmier jaillir un fer blessant,
Dernier présent du tronc qui meurt en le versant !
O Larmes, ô parfums des paupières écloses !
Parfums, esprits subtils tirés du fond des choses,
Essor de la matière à l’immatériel,
Fontaine où Dieu s’abreuve, atmosphère du ciel!


Hôtes mystérieux des tombes solennelles,
Parfums, éternité des reliques charnelles!
Ether incorruptible en qui la beauté vit ;
Ou toute forme pure à la mort se ravit,
Esprits, qui défendez de-toute lèpre immonde
Les corps-dans le sépulcre, et les cœurs dans le monde,
Huile qui fait briller les lampes jusqu’au jour !
O principe de vie aussi fort que l’amour !
Brise d’en-haut venue, haleine de cinname,
Qui descend du Seigneur et remonte de l’âme !


O larmes ! ô pardon de toute iniquité !
O parfums, gardiens de toute pureté!


PLEUREZ, ô Magdeleine ! et quand la sève monte
Laissez l’arbre saigner ! versez vos pleurs sans honte !
Épuisez lentement leur calice azuré ;
Oh ! les pleurs sont bénis, le Seigneur a pleuré !


Maître, je vous ai vu comme une âme exilée
Errer le soir, au bord des lacs de Galilée ;
La barque reposait dans l’eau bleue et sans plis,
Et les frères dormaient sur leurs filets remplis ;
Vous, sans qu’un bruit profane osa troubler vos rêves,
Vous marchiez lentement sur le sable des grèves,
Et vos regards, errants de l’un à l’autre azur,
Semblaient interroger la mer et le ciel pur.
Quelquefois, appuyé contre une roche grise,
Votre beau front levé du côté de la brise,
Debout, vous écoutiez, croisant vos bras distraits ;
Et là, quels bruits lointains, ineffables, secrets,
Quelles voix, du désert ou de la mer venues,
Quels mots mystérieux éclataient dans les nues,
Quelles choses parlaient et rayonnaient en vous?
Était-ce Nazareth, Marie à vos genoux,
Les frères attentifs, le cénacle et les fêtes,
Ou les murs de Sion teints du sang des prophètes?
Je ne sais, mais j*ai vu ce front transfiguré
Se baisser pâlissant et vous avez pleuré !


Ces lacs dont les grands flots se courbent à vos signes,
Ont reçu de vos yeux bien des perles insignes,
Et les jardins du ciel nous peuvent envier
La rosée accordée à plus d’un olivier.
Étoiles d’Orient ! belles nuits de Judée !
Plaine de Siloë de soleil inondée !
Lit pierreux du Cédron ! palmiers de Nazareth !
Flots de Tibériade et de Génézareth !
Grands vents qui balayez les roches désolées
L’Horizons infinis des grèves isolées!
Solitudes qu’il aime, où ses pas sont gravés,
Oh ! dites s’il pleura, dites, vous le savez !


Que de fois il allait, au mépris des scandales,
Loin des Pharisiens secouant ses sandales,
Marchant où l’appelait l’esprit de vérité,
Porter dans les déserts sa sainte oisiveté !
Cueillez-y sur ses pas les fleurs immaculées,
Lavez vos fronts dans l’eau des sources reculées !
Là, parmi la rosée et l’herbe vierge encor,
Sur la neige d’argent et sur le sable d’or,
Dans l’haleine des mers et dans celle des plaines,
Dans la vapeur qui fume au dessus des fontaines,
Dans l’ombrage odorant qui coule des forêts.
Des parfums sont restés, fruits de ses pleurs secrets!


Respirez au désert ces effluves divines ;
Secouez les rameaux baignés de perles fines ;
Puisez dans vos deux mains l’eau vive des rochers,
Que le vase déborde, et, sous son poids penchés,
Lorsque vous sentirez que votre âme est trop pleine,
Pour que rien ne s’en perde, oh! comme Magdeleine,
À genoux devant lui, brisez avec ferveur
L’urne d’élection sur les pieds du Sauveur !
Pendant que vous rêvez immobile, ô Marie,
L’eau sainte goutte à goutte emplit l’urne tarie ;
Écoutez votre cœur où la voix parle encor
Jusqu’au jour de verser ce qui tombe dans l’or ;
Au bord du puits divin tenez-vous appuyée ;
Si vos bras sont croisés, votre âme est déployée.
Et quand la voile au vent ouvre ses plis gonflés,
La rame est inutile aux navires ailés !
Dormez où votre espoir a jeté sa racine,.
Marthe jalouse en vain votre place divine,
A cette âme qui s’use à des soins superflus
Le Christ a répondu déjà pour ses élus :
« Le trépied fume encor sur les flammes pressées,
« Les fruits mûrs sont cueillis, les amphores dressées,
« Le miel et le froment pétris dès le matin,
« La salle radieuse est ouverte au festin,
« Les hôtes sont joyeux ; mais une voix réclame
« Marthe, qu’avez-vous fait pour les besoins de l’âme?
« Vous avez préparé le pain du serviteur,
« L’esclave est satisfait, mais qu’aura le Seigneur?
« Croyez-vous que la chair calme sa faim divine?
« N’a-t-il pas une soif que votre cœur devine ;
« À sa lèvre altérée il faut un vin plus doux,
« Vin qu’a versé Marie, ô Marthe, et non pas vous !
« Ne l’accusez donc pas d’être l’arbre inutile ;
« A qui s’endort sur moi le sommeil est fertile !
« Le travail de plusieurs qui s’en seront vantés
« Portera moins de fruit que cette oisiveté.
« Votre cœur s’est troublé du soin des choses vaines,
« Une seule pourtant est digne de vos peines,
« O Marthe, et votre sœur avant vous en fit choix ;
« Assise à mes genoux, elle écoute ma voix ;
« Nul ne lui ravira cette place chérie, »
« Car la meilleure part est celle de Marie ! »


Vous avez dit cela, jugeant un jour, Seigneur,
Les hommes du dehors et l’homme intérieur.


Il est des vases d’or scellés dans son royaume,
Des cœurs venus de lui pleins d’un céleste baume ;
Il est, même ici-bas, des encensoirs vivants,
Des calices vermeils respectés par les vents,
Où du ciel lentement la pluie est déposée ;
Le soleil frappe-t-il ces cœurs pleins de rosée,
Un enfant vers l’autel va-t-il les découvrir ;
Sans embaumer le temple, ils ne peuvent s’ouvrir !
Mais pour livrer sa neige au rayon qui l’effleure,
Pour fumer à l’autel, quand vient le jour et l’heure,
Il faut que le beau lys que nul doigt n’a meurtri
Loin des vents et de l’homme ait pu croître à l’abri,
Que les charbons ardents renfermés dans le vase
Attendent l’encens pur et le feu de l’extase,
Et qu’ils ne s’usent pas au souffle des passants
Ainsi qu’un fourneau vil ouvert à tous les vents !
Oserez-vous faucher l’iris et les narcisses
Comme le foin des prés, litière des génisses?
L’or pur des encensoirs est-il un or perdu?
Hommes ! malheur à vous quand vous l’aurez fondu,
Et pris pour puiser l’eau des terrestres fontaines
L’amphore où dort le vin jusqu’aux Pâques lointaines !


SEIGNEUR, dans le troupeau des robustes humains
Il est de beaux enfants, frêles et blanches mains,
Trop faibles pour lutter durant la vie entière
Et se voir obéir par la lourde matière ;
Ils ne savent pas faire avec les socs tranchants
Jaillir les blonds épis des veines de vos champs,
Aider les nations à construire leurs tentes,
Tisser de pourpre et d’or les robes éclatantes,
Et charger les vaisseaux sous un ciel reculé,
Des tapis d’Ecbatane ou du fer de Thulé.
Est-ce donc, ô mon Dieu, que leur grâce inféconde
Est livrée en opprobre aux puissants de ce monde,
Et qu’à votre soleil chacun leur peut ôter
L’humble coin qu’il leur faut pour prier et chanter ?
Est-ce qu’au jour marqué pour la grande justice,
Afin qu’aux yeux de tous, votre enfer accomplisse
L’anathême porté sur les rameaux oisifs,
Vous frapperez ces fronts amoureux et pensifs !


Préférez-vous au lac les grands flots des rivières,
Et la roche inflexible aux tremblantes bruyères?
Les fleurs et les oiseaux vous sont-ils odieux?
Mais le cèdre est chargé de nids mélodieux,
L’hysope entre ses pieds pousse une humble racine,
Et le Liban les berce en sa large poitrine !
Les auriez-vous mêlés dans la création
Pour bannir les plus doux de votre affection?
Oh ! vous aimez, Seigneur, la forme pure et belle,
Car c’est l’achèvement de l’idée.éternelle,
La splendeur de l’esprit .visible à l’œil mortel.
Chacun de son côté travaille pour l’autel ;
Si les forts ouvriers en sculptent les colonnes,
Les enfants les plus beaux tresseront des couronnes !
Ne faut-il pas des voix pour bénir, pour chanter?
Ce n’est pas être oisif que de vous écouter,
De recevoir de vous chaque soir l’huile sainte,
Lampe qui luit dans l’ombre et n’est jamais éteinte !


Oh ! quand les marteaux lourds se reposent, le soir,
Les hommes ont besoin de lyre et d’encensoir;
C’est l’immense désir de toute créature
De chercher vos rayons épars dans la nature,
Et c’est une vertu de lire avec clarté
Un peu de votre nom écrit dans la beauté ;
D’avoir le front marqué de votre sceau de flamme,
Et, mêlant des parfums aux musiques de l’âme
D’être l’urne de baume et le luth frémissant
Qui parfume la terre et chante en se brisant !

❃

L’APÔTRE fut long-temps perdu dans sa prière,
Et Jésus le cherchait et l’appelait : Mon frère!
Et Jean se releva, plus fort et plus charmé;
Il avait entendu la voix du bien-aimé.
Or, si vous demandez, quel était l’homme austère
Qui défendait aux fleurs de parfumer la terre,
Et sur l’humble faiblesse ainsi prompt à tonner,
Refusait à Jésus le droit de pardonner ;
Cet ennemi du luxe et des beautés futiles,
Laborieux chercheur de procédés utiles,
En qui le pauvre avait un avocat fervent,
Et la sainte pudeur un fidèle servant,
Sage dont la vertu, prompte aux chastes alarmes,
Fuyait comme la mort les parfums et les larmes,
Esprit rigide et fort, cœur qui ne rêvait pas….
Que son nom soit maudit ! cet homme était Judas !


Victor de La Prade.



