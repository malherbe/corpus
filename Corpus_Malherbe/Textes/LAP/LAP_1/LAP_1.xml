<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES PARFUMS DE MAGDELEINE</title>
				<title type="medium">Édition électronique</title>
				<author key="LAP">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>LAPRADE</surname>
					</name>
					<date from="1812" to="1883">1812-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>420 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">LAP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les parfums de Magdeleine</title>
						<author>Victor Laprade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54059278?rk=364808;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les parfums de Magdeleine</title>
								<author>Victor Laprade</author>
								<imprint>
									<pubPlace>LYON</pubPlace>
									<publisher>IMPRIMERIE DE L. BOITEL</publisher>
									<date when="1839">1839</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1839">1839</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>
			<div type="poem" key="LAP1">
				<head type="main">LES PARFUMS DE MAGDELEINE</head>
				<lg n="1">
					<l n="1" num="1.1">En ce temps-là, ce fut une joie infinie</l>
					<l n="2" num="1.2">Chez tous les habitants du bourg de Béthanie ;</l>
					<l n="3" num="1.3">Un pasteur avait vu, loin des chemins foulés,</l>
					<l n="4" num="1.4">Des voyageurs pensifs venir le long des blés,</l>
					<l n="5" num="1.5">Et, courant le premier, à la foule jalouse</l>
					<l n="6" num="1.6">Il avait annoncé le Seigneur et les Douze.</l>
					<l n="7" num="1.7">Or, comme aux jours anciens, par les vieillards rangé,</l>
					<l n="8" num="1.8">Le peuple s’assemblait près d’un puits ombragé</l>
					<l n="9" num="1.9">Et, marchant vers Jésus, les enfants et les femmes</l>
					<l n="10" num="1.10">Dont sa voix caressait si doucement les âmes,</l>
					<l n="11" num="1.11">Répandaient à ses pieds des palmes d’Amana,</l>
					<l n="12" num="1.12">Se pressaient pour l’entendre, et criaient : Hosanna !</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Et la joie éclatait, plus féconde et plus vive,</l>
					<l n="14" num="2.2">Sous le toit où devait s’asseoir un tel convive;</l>
					<l n="15" num="2.3">Chez Simon qu’il aimait et qu’il avait guéri,</l>
					<l n="16" num="2.4">Les élus attendaient l’hôte illustre et chéri,</l>
					<l n="17" num="2.5">Et, mêlant de doux soins au chant des saints cantiques,</l>
					<l n="18" num="2.6">Des vases solennels puisaient les vins antiques.</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1">Comme un ardent parvis aux Pâques préparé</l>
					<l n="20" num="3.2">Le cénacle s’ouvrait rayonnant et paré ;</l>
					<l n="21" num="3.3">Seule au bord du Cédron, pour en orner l’enceinte</l>
					<l n="22" num="3.4">Marie avait cueilli le lys et l’hyacinthe,</l>
					<l n="23" num="3.5">De myrrhe et d’aloès frotté le cèdre noir ;</l>
					<l n="24" num="3.6">La table reluisait claire comme un miroir,</l>
					<l n="25" num="3.7">Et des tresses de fleurs erraient, collier fragile,</l>
					<l n="26" num="3.8">Sur le col rougissant des amphores d’argile.</l>
				</lg>
				<lg n="4">
					<l n="27" num="4.1">Marthe au divin banquet n’avait rien épargné ;</l>
					<l n="28" num="4.2">Une active rougeur parait son front baigné ;</l>
					<l n="29" num="4.3">Elle avait elle-même, entre des branches vertes.</l>
					<l n="30" num="4.4">Servi les blonds raisins, les grenades ouvertes,</l>
					<l n="31" num="4.5">Les figues du Carmel, le miel pur de Membré,</l>
					<l n="32" num="4.6">Et le poisson des lacs, et l’azyme doré,</l>
					<l n="33" num="4.7">Et l’agneau, qui n’avait qu’une semaine entière</l>
					<l n="34" num="4.8">Sur les Monts Galaad brouté la sauge amère,.</l>
					<l n="35" num="4.9">Et le vin parfumé des vignes d’Engaddi,</l>
					<l n="36" num="4.10">Que baise avec amour le soleil de midi.</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1">Or, autour du festin les Douze se rangèrent,</l>
					<l n="38" num="5.2">Et des baisers de paix entre eux tous s’échangèrent.</l>
				</lg>
				<lg n="6">
					<l n="39" num="6.1">Et le Maître s’assit ; ses regards étaient doux ;</l>
					<l n="40" num="6.2">Son front blanc, couronné par de longs cheveux roux,</l>
					<l n="41" num="6.3">Avait dans sa beauté sereine et reposée</l>
					<l n="42" num="6.4">Une grâce ineffable et pleine de pensée ;</l>
					<l n="43" num="6.5">L’ardente charité, nimbe d’or et de feu,</l>
					<l n="44" num="6.6">Rayonnait de sa face avec l’esprit de Dieu.</l>
					<l n="45" num="6.7">Un manteau bleu s’ouvrait sur sa rouge tunique,</l>
					<l n="46" num="6.8">Ouvrage de sa mère et d’une pièce unique,</l>
					<l n="47" num="6.9">Mystérieux tissu qu’un prophète chanta,</l>
					<l n="48" num="6.10">Voile du corps sacré promis au Golgotha.</l>
					<l n="49" num="6.11">Devant Jésus était le pêcheur d’hommes, Pierre,</l>
					<l n="50" num="6.12">Le futur fondement de son église entière,</l>
					<l n="51" num="6.13">Né pour la foi robuste, et fait à l’action,</l>
					<l n="52" num="6.14">Tête chauve et brunie où vit la passion.</l>
					<l n="53" num="6.15">Mais la meilleure place était celle d’un autre</l>
					<l n="54" num="6.16">Jeune homme aux blonds cheveux, chaste et suave apôtre,</l>
					<l n="55" num="6.17">Et qui, les yeux rêveurs et baignés à demi,</l>
					<l n="56" num="6.18">S’appuyait sur le sein de son divin ami,</l>
					<l n="57" num="6.19">Âme où le Christ versait sa parole secrète</l>
					<l n="58" num="6.20">Jean, l’élu de son cœur, le disciple poète !</l>
				</lg>
				<lg n="7">
					<l n="59" num="7.1">Et la sainte amitié, vin des vignes du ciel.</l>
					<l n="60" num="7.2">Circulait entre tous au banquet fraternel.</l>
				</lg>
				<lg n="8">
					<l n="61" num="8.1">Or, celle qu’on nommait Marie et Magdeleine,</l>
					<l n="62" num="8.2">Perle de beauté rare, et fleur de douce haleine,</l>
					<l n="63" num="8.3">Femme qui par le cœur avait beaucoup péché,</l>
					<l n="64" num="8.4">Magdeleine était là, triste et le corps penché ;</l>
					<l n="65" num="8.5">Cette âme avait tari plus d’une source amère,</l>
					<l n="66" num="8.6">Avant de rencontrer l’onde qui désaltère,</l>
					<l n="67" num="8.7">Et sa soif, survivant à mille espoirs déçus,</l>
					<l n="68" num="8.8">Puisait avec amour aux leçons de Jésus.</l>
					<l n="69" num="8.9">A genoux, et joignant ses deux mains, l’humble femme .</l>
					<l n="70" num="8.10">Priait et soupirait, du profond de son âme ;</l>
					<l n="71" num="8.11">Tremblante, se voilant sous l’or de ses cheveux,</l>
					<l n="72" num="8.12">Elle cherchait les yeux du Christ avec ses yeux;</l>
					<l n="73" num="8.13">Courbait son front rougi par une intime fièvre,.</l>
					<l n="74" num="8.14">Sur les pieds de Jésus purifiait sa lèvre,</l>
					<l n="75" num="8.15">Et pleurait doucement le passé plein d’ennui</l>
					<l n="76" num="8.16">Où ses larmes coulaient pour d’autres que pour lui,</l>
					<l n="77" num="8.17">Jésus était pensif; or, la sœur de Lazare</l>
					<l n="78" num="8.18">Dans un vase d’albâtre avait un baume rare</l>
					<l n="79" num="8.19">Apporté du désert, et, plus loin que Memphis,</l>
					<l n="80" num="8.20">Fait d’une fleur qui croît au bord des Oasis.</l>
					<l n="81" num="8.21">Le parfum s’épurait dans l’urne diaphane ;</l>
					<l n="82" num="8.22">Elle l’avait gardé de tout emploi profane,</l>
					<l n="83" num="8.23">Et venait à la fin, son jour s’étant levé,</l>
					<l n="84" num="8.24">Au Dieu de son attente offrir l’encens sauvé.</l>
					<l n="85" num="8.25">Dans un épanchement de douleur et d’extase,</l>
					<l n="86" num="8.26">Sur le corps de Jésus elle rompit le vase ;</l>
					<l n="87" num="8.27">De larmes et de baume elle baigna ses pieds,</l>
					<l n="88" num="8.28">Les retint doucement sur son sein appuyés,</l>
					<l n="89" num="8.29">Et de ses blonds cheveux pressant leur chaste ivoire,</l>
					<l n="90" num="8.30">Longtemps elle essuya le flot expiatoire.</l>
				</lg>
				<lg n="9">
					<l n="91" num="9.1">Et le parfum montait ; la salle du festin</l>
					<l n="92" num="9.2">Fumait comme un bois vierge au soleil du matin ;</l>
					<l n="93" num="9.3">Et l’air, tout imprégné des essences divines,</l>
					<l n="94" num="9.4">Vivifiait le sang dans toutes les poitrines.</l>
					<l n="95" num="9.5">Alors, devant Jésus, il se fit un moment</l>
					<l n="96" num="9.6">D’un silence rêveur tout plein d’épanchement.</l>
				</lg>
				<lg n="10">
					<l n="97" num="10.1">Mais tout-à-coup tombant, comme une pierre aride,</l>
					<l n="98" num="10.2">Une voix vint troubler cette extase limpide.</l>
				</lg>
				<lg n="11">
					<l n="99" num="11.1">Elle disait : « Chassez cette femme d’ici !</l>
					<l n="100" num="11.2">« Les agneaux et les boucs se mêlent-ils ainsi?</l>
					<l n="101" num="11.3">« Le Maître ne sait pas quelles lèvres impures</l>
					<l n="102" num="11.4">« Osent à sa personne essuyer leurs souillures.</l>
					<l n="103" num="11.5">« Croit-on qu’un peu d’encens et de pleurs épanchés</l>
					<l n="104" num="11.6">« Achètent le pardon et lavent les péchés !</l>
					<l n="105" num="11.7">« Il faut pour sauver l’aine une foi plus active,</l>
					<l n="106" num="11.8">« La loi ne connaît point de pénitence oisive,</l>
					<l n="107" num="11.9">« Le luxe et lès parfums sont maudits des élus ;</l>
					<l n="108" num="11.10">« C’est mal de se complaire à ces biens superflus,</l>
					<l n="109" num="11.11">« De s’attendrir ainsi sur des larmes fleuries ;</l>
					<l n="110" num="11.12">« Le péché suit de près les molles rêveries.</l>
					<l n="111" num="11.13">« Et ce baume, d’ailleurs, valait beaucoup d’argent;</l>
					<l n="112" num="11.14">« Le perdre, c’est voler du pain à l’indigent ;</l>
					<l n="113" num="11.15">« Car, pour faire l’aumône, il aurait bien pu rendre</l>
					<l n="114" num="11.16">« Trois cents deniers au moins, si l’on eût su le vendre ! »</l>
					<l n="115" num="11.17">Et les frères troublés dans le fond de leur cœur</l>
					<l n="116" num="11.18">Tournèrent à la fois les yeux vers le Seigneur.</l>
				</lg>
				<lg n="12">
					<l n="117" num="12.1">Et lui, sur l’humble femme étendant ses mains pures :</l>
					<l n="118" num="12.2">« Oh! ne là froissez pas de vos paroles dures;</l>
					<l n="119" num="12.3">« Hommes de peu d’amour, elle a fait mieux que vous !</l>
					<l n="120" num="12.4">« Voyez mes pieds meurtris qu’elle essuye à genoux,</l>
					<l n="121" num="12.5">« Ses yeux en ont lavé le sang et la poussière ;</l>
					<l n="122" num="12.6">« Elle a de ses parfums répandu l’urne entière,</l>
					<l n="123" num="12.7">« Et, tandis que ses pleurs jaillissaient en ruisseau,.</l>
					<l n="124" num="12.8">« Je n’ai pas eu de vous même une goutte d’eau !</l>
					<l n="125" num="12.9">« Vous n’êtes pas venus, mes hôtes, mes apôtres,</l>
					<l n="126" num="12.10">« Presser en m’abordant mes lèvres sur les vôtres;</l>
					<l n="127" num="12.11">« Marie a sur son cœur posé mes pieds brisés,</l>
					<l n="128" num="12.12">« Et les réchauffe encor de ses pieux baisers ;</l>
					<l n="129" num="12.13">« Son amour vigilant a pressenti mon heure;</l>
					<l n="130" num="12.14">« Sur mon corps embaumé, par avance, elle pleure…</l>
					<l n="131" num="12.15">« Oui, pour l’aumône même, un trésor amassé,</l>
					<l n="132" num="12.16">« Ne vaudrait pas l’encens que Marie a versé !</l>
					<l n="133" num="12.17">« Vous aurez jusqu’au bout des pauvres sur la terre,</l>
					<l n="134" num="12.18">« Hommes! espérez-vous m’avoir toujours pour frère?</l>
					<l n="135" num="12.19">« Magdeleine a péché, mais au livre des cieux</l>
					<l n="136" num="12.20">« Elle a blanchi sa page avec l’eau de ses yeux,</l>
					<l n="137" num="12.21">« Et le Seigneur lui doit, juste dans sa clémence,</l>
					<l n="138" num="12.22">« Un immense pardon pour son amour immense.</l>
					<l n="139" num="12.23">« Je vous le dis, tous ceux à qui sera porté</l>
					<l n="140" num="12.24">« Le Verbe de la pais et de la charité,</l>
					<l n="141" num="12.25">« Diront de cette femme, en chantant ses louanges,</l>
					<l n="142" num="12.26">» Qu’elle a fait ce qu’au ciel doivent faire les anges,</l>
					<l n="143" num="12.27">« Et qu’elle montera, ses péchés expiés,</l>
					<l n="144" num="12.28">« Poser encor, là haut, des baisers sur mes pieds ! »</l>
				</lg>
				<lg n="13">
					<l n="145" num="13.1">Et le Maître sortit ; aux portes du cénacle</l>
					<l n="146" num="13.2">Des malades couchés attendaient un miracle.</l>
				</lg>
				<lg n="14">
					<l n="147" num="14.1">Or, Jean restait le front dans sa main, et rêveur</l>
					<l n="148" num="14.2">Sondait comme une mer le discours du Sauveur.</l>
				</lg>
				<ab type="star">❃</ab>
				<lg n="15">
					<l n="149" num="15.1">Oh ! s’ils viennent pensifs s’asseoir entre vos fêtes,</l>
					<l n="150" num="15.2">Versez l’ambre et le nard sur les pieds des prophètes ;</l>
					<l n="151" num="15.3">A vos larmes d’amour, au fond des urnes d’or,</l>
					<l n="152" num="15.4">Mêlez pour eux les pleurs des roses de Ségor !</l>
					<l n="153" num="15.5">Est-ce donc pour la brise ou l’ombre solitaire,</l>
					<l n="154" num="15.6">Que Dieu mit des : parfums dans les. fleurs de la terre ?</l>
					<l n="155" num="15.7">Est-ce pour y mourir, desséché par l’orgueil</l>
					<l n="156" num="15.8">Qu’un ruisseau tiède et pur tremble au fond,de chaque œil ;</l>
					<l n="157" num="15.9">Et pour s’éteindre, avant de jeter une flamme,</l>
					<l n="158" num="15.10">Qu’un doux soleil se lève au matin de notre âme?</l>
				</lg>
				<lg n="16">
					<l n="159" num="16.1">SEIGNEUR, quand vous avez en un cœur sans détour</l>
					<l n="160" num="16.2">De la perfection semé le noble amour,</l>
					<l n="161" num="16.3">Qu’ensuite vous ouvrez à ces âmes ailées</l>
					<l n="162" num="16.4">Un champ libre à travers vos œuvres étoilées,</l>
					<l n="163" num="16.5">Vos splendides jardins, votre ciel argenté,</l>
					<l n="164" num="16.6">Et tout ce qui nous voile enfin votre beauté;</l>
					<l n="165" num="16.7">Si quelque pauvre enfant que votre soif dévore,</l>
					<l n="166" num="16.8">Et qui pour vous chercher s’est levé dès l’aurore,</l>
					<l n="167" num="16.9">D’une merveille à l’autre, avant de vous trouver,</l>
					<l n="168" num="16.10">Vole, et lassé s’y pose un instant pour rêver;</l>
					<l n="169" num="16.11">Dans le creux de sa main puise au bord des fontaines,</l>
					<l n="170" num="16.12">Et sans route frayée en ces terres lointaines,</l>
					<l n="171" num="16.13">S’égare et dort un soir,, doucement attiré,</l>
					<l n="172" num="16.14">Auprès d’une fleur rare ou d’un oiseau doré ;</l>
					<l n="173" num="16.15">Ou bien si tout meurtri des,pierres de la route,</l>
					<l n="174" num="16.16">Sans rien à l’horizon, il se couche et s’il doute;</l>
					<l n="175" num="16.17">Lorsqu’il voit luire enfin la splendeur de vos pieds</l>
					<l n="176" num="16.18">Et qu’il se traîne à vous, sur ses genoux pliés…</l>
					<l n="177" num="16.19">De ces larmes sous qui toute tache s’efface</l>
					<l n="178" num="16.20">Pourrez-vous, ô Seigneur, détourner votre face !</l>
				</lg>
				<lg n="17">
					<l n="179" num="17.1">Les pleurs ne-sont-ils .pas, des diamants-cachés</l>
					<l n="180" num="17.2">Qui payent, en tombant, le prix de nos péchés?</l>
					<l n="181" num="17.3">Chaste sueur de l’âme impuissante et brisée,</l>
					<l n="182" num="17.4">Par un Dieu qui pleura seriez-vous méprisée?</l>
				</lg>
				<lg n="18">
					<l n="183" num="18.1">Larmes du repentir ! eau féconde toujours !</l>
					<l n="184" num="18.2">Quand l’homme vous répand sur tous ses mauvais jours,</l>
					<l n="185" num="18.3">Vous chassez de son cœur les fanges entassées,</l>
					<l n="186" num="18.4">Sous les pieds remuants des coupables pensées ;</l>
					<l n="187" num="18.5">Puis, comme le soleil sur une terre en pleurs</l>
					<l n="188" num="18.6">Raffermit les chemins et relève les fleurs,</l>
					<l n="189" num="18.7">Un doux regard de Dieu, suivant l’ombre et la pluie,.</l>
					<l n="190" num="18.8">Se répand sur l’esprit, le réchauffe et l’essuie !</l>
				</lg>
				<lg n="19">
					<l n="191" num="19.1">DANS l’urne aux blancs contours que de fleurs ont pleuré</l>
					<l n="192" num="19.2">Pour l’emplir jusqu’au-bord d’un encens épuré !</l>
					<l n="193" num="19.3">Oh! que tout soit pour lui, donnez, ô Magdeleine,</l>
					<l n="194" num="19.4">Versez, sur ses pieds nus, votre âme humide et pleine,</l>
					<l n="195" num="19.5">Versez le fond du vase et les parfums cachés,</l>
					<l n="196" num="19.6">Les regrets, les espoirs, tout, jusqu’à vos péchés !</l>
					<l n="197" num="19.7">Versez les chastes jours et les nuits profanées,</l>
					<l n="198" num="19.8">Et l’asphodèle vierge et les roses fanées ;</l>
					<l n="199" num="19.9">Versez votre douleur, versez votre beauté.</l>
					<l n="200" num="19.10">Tout en vous est parfum, et tout sera compté !</l>
					<l n="201" num="19.11">Brisez au pied du Christ ce cœur doux et fragile ;</l>
					<l n="202" num="19.12">Ce que la loi rejette est pris par l’Évangile,</l>
					<l n="203" num="19.13">Des épis oubliés sa moisson s’enrichit ;</l>
					<l n="204" num="19.14">A lui tout ce qui pleure, et tout ce qui fléchit,</l>
					<l n="205" num="19.15">A lui la pénitente obscure et méprisée,</l>
					<l n="206" num="19.16">A lui le nid sans mère, et la branche brisée,</l>
					<l n="207" num="19.17">A lui tout ce qui vit sans filer ni semer,</l>
					<l n="208" num="19.18">A lui le lys des champs qui ne sait qu’embaumer,</l>
					<l n="209" num="19.19">L’oiseau qui vole au ciel, insoucieux, et chante;</l>
					<l n="210" num="19.20">A lui la beauté frêle, et l’enfance touchante,</l>
					<l n="211" num="19.21">Et ces hommes rêveurs qui sont toujours enfants,</l>
					<l n="212" num="19.22">Tous ceux sur qui le fort met ses pieds triomphants !</l>
					<l n="213" num="19.23">Les faibles sont les siens, sa force les relève;</l>
					<l n="214" num="19.24">Il porte dans ses mains la grâce et non le glaive.</l>
				</lg>
				<lg n="20">
					<l n="215" num="20.1">UNE eau mystérieuse a baigné vos genoux !</l>
					<l n="216" num="20.2">Le ciel même, ô Seigneur, a-t-il rien de plus doux?</l>
					<l n="217" num="20.3">A ces flots onctueux, fumant d’un double arome,</l>
					<l n="218" num="20.4">L’homme a fourni les pleurs et la terre le baume,</l>
					<l n="219" num="20.5">Tous les deux vous offrant leurs présents les meilleurs,</l>
					<l n="220" num="20.6">La nature ses fleurs, et Famé ses douleurs,</l>
					<l n="221" num="20.7">Puis, versant tous les deux sur vos traces sereines</l>
					<l n="222" num="20.8">Ce que vous-ayez mis de plus pur dans leurs veines!</l>
				</lg>
				<lg n="21">
					<l n="223" num="21.1">Larmes ! trésor vivant, perles de vérité !</l>
					<l n="224" num="21.2">Seul don qu’offre le cœur sans l’avoir emprunté !</l>
					<l n="225" num="21.3">Baume que le soleil fait monter goutte à goutte</l>
					<l n="226" num="21.4">Et surnager de l’ame en la consumant toute !</l>
					<l n="227" num="21.5">Vin que fait du palmier jaillir un fer blessant,</l>
					<l n="228" num="21.6">Dernier présent du tronc qui meurt en le versant !</l>
					<l n="229" num="21.7">O Larmes, ô parfums des paupières écloses !</l>
					<l n="230" num="21.8">Parfums, esprits subtils tirés du fond des choses,</l>
					<l n="231" num="21.9">Essor de la matière à l’immatériel,</l>
					<l n="232" num="21.10">Fontaine où Dieu s’abreuve, atmosphère du ciel!</l>
				</lg>
				<lg n="22">
					<l n="233" num="22.1">Hôtes mystérieux des tombes solennelles,</l>
					<l n="234" num="22.2">Parfums, éternité des reliques charnelles!</l>
					<l n="235" num="22.3">Ether incorruptible en qui la beauté vit ;</l>
					<l n="236" num="22.4">Ou toute forme pure à la mort se ravit,</l>
					<l n="237" num="22.5">Esprits, qui défendez de-toute lèpre immonde</l>
					<l n="238" num="22.6">Les corps-dans le sépulcre, et les cœurs dans le monde,</l>
					<l n="239" num="22.7">Huile qui fait briller les lampes jusqu’au jour !</l>
					<l n="240" num="22.8">O principe de vie aussi fort que l’amour !</l>
					<l n="241" num="22.9">Brise d’en-haut venue, haleine de cinname,</l>
					<l n="242" num="22.10">Qui descend du Seigneur et remonte de l’âme !</l>
				</lg>
				<lg n="23">
					<l n="243" num="23.1">O larmes ! ô pardon de toute iniquité !</l>
					<l n="244" num="23.2">O parfums, gardiens de toute pureté!</l>
				</lg>
				<lg n="24">
					<l n="245" num="24.1">PLEUREZ, ô Magdeleine ! et quand la sève monte</l>
					<l n="246" num="24.2">Laissez l’arbre saigner ! versez vos pleurs sans honte !</l>
					<l n="247" num="24.3">Épuisez lentement leur calice azuré ;</l>
					<l n="248" num="24.4">Oh ! les pleurs sont bénis, le Seigneur a pleuré !</l>
				</lg>
				<lg n="25">
					<l n="249" num="25.1">Maître, je vous ai vu comme une âme exilée</l>
					<l n="250" num="25.2">Errer le soir, au bord des lacs de Galilée ;</l>
					<l n="251" num="25.3">La barque reposait dans l’eau bleue et sans plis,</l>
					<l n="252" num="25.4">Et les frères dormaient sur leurs filets remplis ;</l>
					<l n="253" num="25.5">Vous, sans qu’un bruit profane osa troubler vos rêves,</l>
					<l n="254" num="25.6">Vous marchiez lentement sur le sable des grèves,</l>
					<l n="255" num="25.7">Et vos regards, errants de l’un à l’autre azur,</l>
					<l n="256" num="25.8">Semblaient interroger la mer et le ciel pur.</l>
					<l n="257" num="25.9">Quelquefois, appuyé contre une roche grise,</l>
					<l n="258" num="25.10">Votre beau front levé du côté de la brise,</l>
					<l n="259" num="25.11">Debout, vous écoutiez, croisant vos bras distraits ;</l>
					<l n="260" num="25.12">Et là, quels bruits lointains, ineffables, secrets,</l>
					<l n="261" num="25.13">Quelles voix, du désert ou de la mer venues,</l>
					<l n="262" num="25.14">Quels mots mystérieux éclataient dans les nues,</l>
					<l n="263" num="25.15">Quelles choses parlaient et rayonnaient en vous?</l>
					<l n="264" num="25.16">Était-ce Nazareth, Marie à vos genoux,</l>
					<l n="265" num="25.17">Les frères attentifs, le cénacle et les fêtes,</l>
					<l n="266" num="25.18">Ou les murs de Sion teints du sang des prophètes?</l>
					<l n="267" num="25.19">Je ne sais, mais j*ai vu ce front transfiguré</l>
					<l n="268" num="25.20">Se baisser pâlissant et vous avez pleuré !</l>
				</lg>
				<lg n="26">
					<l n="269" num="26.1">Ces lacs dont les grands flots se courbent à vos signes,</l>
					<l n="270" num="26.2">Ont reçu de vos yeux bien des perles insignes,</l>
					<l n="271" num="26.3">Et les jardins du ciel nous peuvent envier</l>
					<l n="272" num="26.4">La rosée accordée à plus d’un olivier.</l>
					<l n="273" num="26.5">Étoiles d’Orient ! belles nuits de Judée !</l>
					<l n="274" num="26.6">Plaine de Siloë de soleil inondée !</l>
					<l n="275" num="26.7">Lit pierreux du Cédron ! palmiers de Nazareth !</l>
					<l n="276" num="26.8">Flots de Tibériade et de Génézareth !</l>
					<l n="277" num="26.9">Grands vents qui balayez les roches désolées</l>
					<l n="278" num="26.10">L’Horizons infinis des grèves isolées!</l>
					<l n="279" num="26.11">Solitudes qu’il aime, où ses pas sont gravés,</l>
					<l n="280" num="26.12">Oh ! dites s’il pleura, dites, vous le savez !</l>
				</lg>
				<lg n="27">
					<l n="281" num="27.1">Que de fois il allait, au mépris des scandales,</l>
					<l n="282" num="27.2">Loin des Pharisiens secouant ses sandales,</l>
					<l n="283" num="27.3">Marchant où l’appelait l’esprit de vérité,</l>
					<l n="284" num="27.4">Porter dans les déserts sa sainte oisiveté !</l>
					<l n="285" num="27.5">Cueillez-y sur ses pas les fleurs immaculées,</l>
					<l n="286" num="27.6">Lavez vos fronts dans l’eau des sources reculées !</l>
					<l n="287" num="27.7">Là, parmi la rosée et l’herbe vierge encor,</l>
					<l n="288" num="27.8">Sur la neige d’argent et sur le sable d’or,</l>
					<l n="289" num="27.9">Dans l’haleine des mers et dans celle des plaines,</l>
					<l n="290" num="27.10">Dans la vapeur qui fume au dessus des fontaines,</l>
					<l n="291" num="27.11">Dans l’ombrage odorant qui coule des forêts.</l>
					<l n="292" num="27.12">Des parfums sont restés, fruits de ses pleurs secrets!</l>
				</lg>
				<lg n="28">
					<l n="293" num="28.1">Respirez au désert ces effluves divines ;</l>
					<l n="294" num="28.2">Secouez les rameaux baignés de perles fines ;</l>
					<l n="295" num="28.3">Puisez dans vos deux mains l’eau vive des rochers,</l>
					<l n="296" num="28.4">Que le vase déborde, et, sous son poids penchés,</l>
					<l n="297" num="28.5">Lorsque vous sentirez que votre âme est trop pleine,</l>
					<l n="298" num="28.6">Pour que rien ne s’en perde, oh! comme Magdeleine,</l>
					<l n="299" num="28.7">À genoux devant lui, brisez avec ferveur</l>
					<l n="300" num="28.8">L’urne d’élection sur les pieds du Sauveur !</l>
					<l n="301" num="28.9">Pendant que vous rêvez immobile, ô Marie,</l>
					<l n="302" num="28.10">L’eau sainte goutte à goutte emplit l’urne tarie ;</l>
					<l n="303" num="28.11">Écoutez votre cœur où la voix parle encor</l>
					<l n="304" num="28.12">Jusqu’au jour de verser ce qui tombe dans l’or ;</l>
					<l n="305" num="28.13">Au bord du puits divin tenez-vous appuyée ;</l>
					<l n="306" num="28.14">Si vos bras sont croisés, votre âme est déployée.</l>
					<l n="307" num="28.15">Et quand la voile au vent ouvre ses plis gonflés,</l>
					<l n="308" num="28.16">La rame est inutile aux navires ailés !</l>
					<l n="309" num="28.17">Dormez où votre espoir a jeté sa racine,.</l>
					<l n="310" num="28.18">Marthe jalouse en vain votre place divine,</l>
					<l n="311" num="28.19">A cette âme qui s’use à des soins superflus</l>
					<l n="312" num="28.20">Le Christ a répondu déjà pour ses élus :</l>
					<l n="313" num="28.21">« Le trépied fume encor sur les flammes pressées,</l>
					<l n="314" num="28.22">« Les fruits mûrs sont cueillis, les amphores dressées,</l>
					<l n="315" num="28.23">« Le miel et le froment pétris dès le matin,</l>
					<l n="316" num="28.24">« La salle radieuse est ouverte au festin,</l>
					<l n="317" num="28.25">« Les hôtes sont joyeux ; mais une voix réclame</l>
					<l n="318" num="28.26">« Marthe, qu’avez-vous fait pour les besoins de l’âme?</l>
					<l n="319" num="28.27">« Vous avez préparé le pain du serviteur,</l>
					<l n="320" num="28.28">« L’esclave est satisfait, mais qu’aura le Seigneur?</l>
					<l n="321" num="28.29">« Croyez-vous que la chair calme sa faim divine?</l>
					<l n="322" num="28.30">« N’a-t-il pas une soif que votre cœur devine ;</l>
					<l n="323" num="28.31">« À sa lèvre altérée il faut un vin plus doux,</l>
					<l n="324" num="28.32">« Vin qu’a versé Marie, ô Marthe, et non pas vous !</l>
					<l n="325" num="28.33">« Ne l’accusez donc pas d’être l’arbre inutile ;</l>
					<l n="326" num="28.34">« A qui s’endort sur moi le sommeil est fertile !</l>
					<l n="327" num="28.35">« Le travail de plusieurs qui s’en seront vantés</l>
					<l n="328" num="28.36">« Portera moins de fruit que cette oisiveté.</l>
					<l n="329" num="28.37">« Votre cœur s’est troublé du soin des choses vaines,</l>
					<l n="330" num="28.38">« Une seule pourtant est digne de vos peines,</l>
					<l n="331" num="28.39">« O Marthe, et votre sœur avant vous en fit choix ;</l>
					<l n="332" num="28.40">« Assise à mes genoux, elle écoute ma voix ;</l>
					<l n="333" num="28.41">« Nul ne lui ravira cette place chérie, »</l>
					<l n="334" num="28.42">« Car la meilleure part est celle de Marie ! »</l>
				</lg>
				<lg n="29">
					<l n="335" num="29.1">Vous avez dit cela, jugeant un jour, Seigneur,</l>
					<l n="336" num="29.2">Les hommes du dehors et l’homme intérieur.</l>
				</lg>
				<lg n="30">
					<l n="337" num="30.1">Il est des vases d’or scellés dans son royaume,</l>
					<l n="338" num="30.2">Des cœurs venus de lui pleins d’un céleste baume ;</l>
					<l n="339" num="30.3">Il est, même ici-bas, des encensoirs vivants,</l>
					<l n="340" num="30.4">Des calices vermeils respectés par les vents,</l>
					<l n="341" num="30.5">Où du ciel lentement la pluie est déposée ;</l>
					<l n="342" num="30.6">Le soleil frappe-t-il ces cœurs pleins de rosée,</l>
					<l n="343" num="30.7">Un enfant vers l’autel va-t-il les découvrir ;</l>
					<l n="344" num="30.8">Sans embaumer le temple, ils ne peuvent s’ouvrir !</l>
					<l n="345" num="30.9">Mais pour livrer sa neige au rayon qui l’effleure,</l>
					<l n="346" num="30.10">Pour fumer à l’autel, quand vient le jour et l’heure,</l>
					<l n="347" num="30.11">Il faut que le beau lys que nul doigt n’a meurtri</l>
					<l n="348" num="30.12">Loin des vents et de l’homme ait pu croître à l’abri,</l>
					<l n="349" num="30.13">Que les charbons ardents renfermés dans le vase</l>
					<l n="350" num="30.14">Attendent l’encens pur et le feu de l’extase,</l>
					<l n="351" num="30.15">Et qu’ils ne s’usent pas au souffle des passants</l>
					<l n="352" num="30.16">Ainsi qu’un fourneau vil ouvert à tous les vents !</l>
					<l n="353" num="30.17">Oserez-vous faucher l’iris et les narcisses</l>
					<l n="354" num="30.18">Comme le foin des prés, litière des génisses?</l>
					<l n="355" num="30.19">L’or pur des encensoirs est-il un or perdu?</l>
					<l n="356" num="30.20">Hommes ! malheur à vous quand vous l’aurez fondu,</l>
					<l n="357" num="30.21">Et pris pour puiser l’eau des terrestres fontaines</l>
					<l n="358" num="30.22">L’amphore où dort le vin jusqu’aux Pâques lointaines !</l>
				</lg>
				<lg n="31">
					<l n="359" num="31.1">SEIGNEUR, dans le troupeau des robustes humains</l>
					<l n="360" num="31.2">Il est de beaux enfants, frêles et blanches mains,</l>
					<l n="361" num="31.3">Trop faibles pour lutter durant la vie entière</l>
					<l n="362" num="31.4">Et se voir obéir par la lourde matière ;</l>
					<l n="363" num="31.5">Ils ne savent pas faire avec les socs tranchants</l>
					<l n="364" num="31.6">Jaillir les blonds épis des veines de vos champs,</l>
					<l n="365" num="31.7">Aider les nations à construire leurs tentes,</l>
					<l n="366" num="31.8">Tisser de pourpre et d’or les robes éclatantes,</l>
					<l n="367" num="31.9">Et charger les vaisseaux sous un ciel reculé,</l>
					<l n="368" num="31.10">Des tapis d’Ecbatane ou du fer de Thulé.</l>
					<l n="369" num="31.11">Est-ce donc, ô mon Dieu, que leur grâce inféconde</l>
					<l n="370" num="31.12">Est livrée en opprobre aux puissants de ce monde,</l>
					<l n="371" num="31.13">Et qu’à votre soleil chacun leur peut ôter</l>
					<l n="372" num="31.14">L’humble coin qu’il leur faut pour prier et chanter ?</l>
					<l n="373" num="31.15">Est-ce qu’au jour marqué pour la grande justice,</l>
					<l n="374" num="31.16">Afin qu’aux yeux de tous, votre enfer accomplisse</l>
					<l n="375" num="31.17">L’anathême porté sur les rameaux oisifs,</l>
					<l n="376" num="31.18">Vous frapperez ces fronts amoureux et pensifs !</l>
				</lg>
				<lg n="32">
					<l n="377" num="32.1">Préférez-vous au lac les grands flots des rivières,</l>
					<l n="378" num="32.2">Et la roche inflexible aux tremblantes bruyères?</l>
					<l n="379" num="32.3">Les fleurs et les oiseaux vous sont-ils odieux?</l>
					<l n="380" num="32.4">Mais le cèdre est chargé de nids mélodieux,</l>
					<l n="381" num="32.5">L’hysope entre ses pieds pousse une humble racine,</l>
					<l n="382" num="32.6">Et le Liban les berce en sa large poitrine !</l>
					<l n="383" num="32.7">Les auriez-vous mêlés dans la création</l>
					<l n="384" num="32.8">Pour bannir les plus doux de votre affection?</l>
					<l n="385" num="32.9">Oh ! vous aimez, Seigneur, la forme pure et belle,</l>
					<l n="386" num="32.10">Car c’est l’achèvement de l’idée.éternelle,</l>
					<l n="387" num="32.11">La splendeur de l’esprit .visible à l’œil mortel.</l>
					<l n="388" num="32.12">Chacun de son côté travaille pour l’autel ;</l>
					<l n="389" num="32.13">Si les forts ouvriers en sculptent les colonnes,</l>
					<l n="390" num="32.14">Les enfants les plus beaux tresseront des couronnes !</l>
					<l n="391" num="32.15">Ne faut-il pas des voix pour bénir, pour chanter?</l>
					<l n="392" num="32.16">Ce n’est pas être oisif que de vous écouter,</l>
					<l n="393" num="32.17">De recevoir de vous chaque soir l’huile sainte,</l>
					<l n="394" num="32.18">Lampe qui luit dans l’ombre et n’est jamais éteinte !</l>
				</lg>
				<lg n="33">
					<l n="395" num="33.1">Oh ! quand les marteaux lourds se reposent, le soir,</l>
					<l n="396" num="33.2">Les hommes ont besoin de lyre et d’encensoir;</l>
					<l n="397" num="33.3">C’est l’immense désir de toute créature</l>
					<l n="398" num="33.4">De chercher vos rayons épars dans la nature,</l>
					<l n="399" num="33.5">Et c’est une vertu de lire avec clarté</l>
					<l n="400" num="33.6">Un peu de votre nom écrit dans la beauté ;</l>
					<l n="401" num="33.7">D’avoir le front marqué de votre sceau de flamme,</l>
					<l n="402" num="33.8">Et, mêlant des parfums aux musiques de l’âme</l>
					<l n="403" num="33.9">D’être l’urne de baume et le luth frémissant</l>
					<l n="404" num="33.10">Qui parfume la terre et chante en se brisant !</l>
				</lg>
				<ab type="star">❃</ab>
				<lg n="34">
					<l n="405" num="34.1">L’APÔTRE fut long-temps perdu dans sa prière,</l>
					<l n="406" num="34.2">Et Jésus le cherchait et l’appelait : Mon frère!</l>
					<l n="407" num="34.3">Et Jean se releva, plus fort et plus charmé;</l>
					<l n="408" num="34.4">Il avait entendu la voix du bien-aimé.</l>
					<l n="409" num="34.5">Or, si vous demandez, quel était l’homme austère</l>
					<l n="410" num="34.6">Qui défendait aux fleurs de parfumer la terre,</l>
					<l n="411" num="34.7">Et sur l’humble faiblesse ainsi prompt à tonner,</l>
					<l n="412" num="34.8">Refusait à Jésus le droit de pardonner ;</l>
					<l n="413" num="34.9">Cet ennemi du luxe et des beautés futiles,</l>
					<l n="414" num="34.10">Laborieux chercheur de procédés utiles,</l>
					<l n="415" num="34.11">En qui le pauvre avait un avocat fervent,</l>
					<l n="416" num="34.12">Et la sainte pudeur un fidèle servant,</l>
					<l n="417" num="34.13">Sage dont la vertu, prompte aux chastes alarmes,</l>
					<l n="418" num="34.14">Fuyait comme la mort les parfums et les larmes,</l>
					<l n="419" num="34.15">Esprit rigide et fort, cœur qui ne rêvait pas….</l>
					<l n="420" num="34.16">Que son nom soit maudit ! cet homme était Judas !</l>
				</lg>
				<closer>
					<signed>Victor de La Prade.</signed>
				</closer>
			</div>
		</body>
	</text>
</TEI>