BAN
———
BAN_7

Théodore de BANVILLE
1823-1891

════════════════════════════════════════════
RONDELS

1874

312 vers

─ poème	BAN316	I. Le Jour
─ poème	BAN317	II. La Nuit
─ poème	BAN318	III. Le Printemps
─ poème	BAN319	IV. L’été
─ poème	BAN320	V.  L’Automne
─ poème	BAN321	VI. L’Hiver
─ poème	BAN322	VII. L’Eau
─ poème	BAN323	VIII. Le Feu
─ poème	BAN324	IX. La Terre
─ poème	BAN325	X. L’Air
─ poème	BAN326	XI. Le Matin
─ poème	BAN327	XII. Le Midi
─ poème	BAN328	XIII. Le Soir
─ poème	BAN329	XIV. La Pêche
─ poème	BAN330	XV.  La Chasse
─ poème	BAN331	XVI. Le Thé
─ poème	BAN332	XVII. Le Café
─ poème	BAN333	XVIII. Le Vin
─ poème	BAN334	XIX.  Les Étoiles
─ poème	BAN335	XX. La Lune
─ poème	BAN336	XXI. La Paix
─ poème	BAN337	XXII. La Guerre
─ poème	BAN338	XXIII. Les Métaux
─ poème	BAN339	XXIV. Les Pierreries
