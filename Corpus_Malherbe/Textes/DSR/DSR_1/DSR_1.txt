Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
DSR
DSR_1

 DESROYS


LE TABAC
POËME
1801
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

LE TABAC
DESROYS


https://gallica.bnf.fr/ark:/12148/bpt6k58077223.r=Desroys?rk=21459;2


LE TABAC
DESROYS

Paris
Chez l’auteur...




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘




PRÉFACE

Si les vers ne désennuient pas ceux
qui les lisent, ils désennuient ceux: qui
les font.

 Voilà ce qu’il faut répondre aux
ignorons dédaigneux; mais on n’en est
pas réduit là avec les gens de goût.

Ils reçoivent avec indulgence, même
les plus légères bagatelles; car ces
bagatelles sont les doux préludes par
lesquels nous devons essayer la Lyre
d’Apollon avant de la monter sur ce
ton éclat snf nui charme un peuple entier,
le polit et finit par le rendre le
modèle de ses voisins et le premier de
tous les Peuples.



LE TABAC

Au Cn. D***.
FABRICANT DE TABAC


C’est un vrai tour de Jarnac
De me donner le Tabac
Pour sujet de Poésie,
Au lieu de Paméla de Flore ou d’Aspasie.
Il vous appartiendroit bien plus,
De vanter ses rares Vertus,
A vous qui, dans votre Fabrique,
De notre fureur tabagique
Faites si bien votre profit.
Le canevas est un peu rêche,
Et ma muse devient revêche.
Mais vous parlez, il me suffit.

🙪

Grands Romains maîtres du monde,
Vous qui dépeupliez les cieux la terre et l’onde
Pour fournir à vos déjeûnés
Et qui de Roses couronnés
Sur vos sophas couchés près d’une tendre blonde,
Faisiez l’amour en buvant à la ronde,
Que vous étiez infortunés !
Ce merveilleux sternutatoire,
Dont l’Amérique se fait gloire,
Ce composé mordant de sels oxygénés,


Par qui Santeuil et moi fumes empoissonnés,(a)
Le Tabac puisqu’il faut le nommer sans grimoire,
Mêts généreux, chéri des Enfans de la gloire,
De sa sève jamais ne réjouit vos nez.
Vous aviez appris d’Ovide
Le gracieux art d’aimer
Mais au sénat vous ruminiez à vide,
Ne sachant ni priser, ni chiquer(b), ni fumer,


Que faisoient donc vos gendarmes,
Lorsque leur arc débandé,
Avoit mis fin aux allarmes
Du beau sexe intimidé ?
Au fonds de leurs corps-de-garde»,
Apparemment ils filoient !
Les quenoilles figuroient
Vis-à-vis des hallebardes !
Chez nous on y voit, morbleu,
Le Dragon lancer le feu
Par les yeux,et par la bouche ;
Il prend, à ce, noble jeu,
L’air amical et farouche,
Qui lui sied et qui me touche ;
Il est dans son élément.


Mais vous, dites-moi comment,
Sans tabatière assis dans sa chaise curulle,
Ne ronfloit pas tout haut votre ivrogne Luculle ?


Que donniez vous, au lieu d’argent,
A votre juge incorruptible ?
N’ayant pas ce meuble obligeant,
Dont le métal irrésistible
Rend toute faute rémissible ?
Du progrès de nos arts monument enchanteur,
O Bijou qui contiens la poudre délectable
(Dont j’aime assez qu’on s’abstienne à ma table,
Tu ravis à-la-fois mes yeux, mon nez, mon cœur
Dans une image chérie,
Tu m’offres mon égérie ;
A tous les instans du jour,
Tu m’avertis que j’existe :
Et ton doux parfum m’assiste
Quand par fois je fais la cour
A mon parrain le Jeanséniste.


Dieu des pauvres rimeurs, Tabac, à ton secours,
Ma muse infortunée, à fréquemment recours,
Lorsque suant à grosses gouttes
Du Parnasse elle suit les raboteuses routes
Et de Pégase rendu
Presse le dos peu dodu
Pour aller accoucher de quelqu’enfant perdu.


Quoi ! Rome et la Grèce antique
Et les gaulois nos aînés
Ignoroient donc la pratique
De ce charmant Narcotique
Si chéri des nez bien nés
De la noblesse Helvétique(c) !
Il me font vraiment pitié :
Car n’en déplaise au critique,
Qui chérit le sel attique
Et se rit de mon cantique,
L’on n’existe qu’à moitié
N’ayant pas dans sa boutique
Cette poudre sympathique
Le lien de l’amitié,
L’Ame de la politique.


Pauvres Grecs, pauvres Latins,
Pauvres Gaulois je vous plains,
Du fond de mon palais, près Montmirelle en Brie,
Mon ame est sur vous attendrie
Vous avez tous passé le Bac,
Et n’avez pas dans votre vie,
Pris une prise de Tabac.

🙪

Cher ami pour te complaire
Voilà ce que j’ai pu faire,
Bien misérable salaire,
De ton hospitalité.
En seras tu contenté ?
Je m en flatte et je l’espère
Car je ne fais point mystère,
Cousin qui vaut mieux qu’un frère,
Que, dans l’ardeur qui me prit,
De courir après Voltaire,
Je n’ai pas fait mon affaire.
Ce malheur, qui, me surprit,
A mes propres frais m’apprit
Qu’il vaut mieux, dans ce siècle âne
Vendre la Nicotiâne(d)
Que de vendre de l’esprit.

🙪

Tout le monde sait l’histoire de Sonteuil : la mienne fut moins Tragique.
Expression technique pour dire macher du tabac.
On connoit le proverbe. Il prend du Tabac comme un Suisse.
Nom du Tabac en botanique.




