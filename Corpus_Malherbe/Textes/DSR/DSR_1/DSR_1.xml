<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="FRA" xsi:schemaLocation="https://git.unicaen.fr/malherbe/corpus/-/blob/master/XSD/TEI_Corpus_Malherbe_1.5.xsd">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LE TABAC</title>
				<title type="sub">POËME</title>
				<title type="medium">Une édition électronique</title>
				<author key="DSR">
					<name>
						<forename/>
						<surname>DESROYS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt> 
			<extent>109 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE TABAC</title>
						<author>DESROYS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k58077223.r=Desroys?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE TABAC</title>
								<author>DESROYS</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez l’auteur...</publisher>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1801">1801</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’auteur de bas de page ont été reportées en fin de poème.</p>
			</samplingDecl>
			<editorialDecl>
				<correction>
					<p>Le document d’origine comporte de nombreuses erreurs de numérisation. Une bonne partie ont été corrigées.</p>
				</correction>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader>
	<text>
		<body>
			<div type="part" n="1">
				<p>
					PRÉFACE<lb/>
					Si les vers ne désennuient pas ceux
					qui les lisent, ils désennuient ceux: qui
					les font.<lb/>
					 Voilà ce qu’il faut répondre aux
					ignorons dédaigneux; mais on n’en est
					pas réduit là avec les gens de goût.<lb/>
					Ils reçoivent avec indulgence, même
					les plus légères bagatelles; car ces
					bagatelles sont les doux préludes par
					lesquels nous devons essayer la Lyre
					d’Apollon avant de la monter sur ce
					ton éclat snf nui charme un peuple entier,
					le polit et finit par le rendre le
					modèle de ses voisins et le premier de
					tous les Peuples.
				</p>
				<head type="main"/>
				<div type="poem" key="DSR1">
					<head type="main">LE TABAC</head>
					<opener>
						<salute>Au C<hi rend="sup">n</hi>. D***.<lb/>FABRICANT DE TABAC</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"/>C’est un vrai tour de Jarnac</l>
						<l n="2" num="1.2"><space unit="char" quantity="10"/>De me donner le Tabac</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"/>Pour sujet de Poésie,</l>
						<l n="4" num="1.4">Au lieu de Paméla de Flore ou d’Aspasie.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"/>Il vous appartiendroit bien plus,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>De vanter ses rares Vertus,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>A vous qui, dans votre Fabrique,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"/>De notre fureur tabagique</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"/>Faites si bien votre profit.</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>Le canevas est un peu rêche,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"/>Et ma muse devient revêche.</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"/>Mais vous parlez, il me suffit.</l>
					</lg>
					<ab type="star">🙪</ab>
					<lg n="2">
						<l n="13" num="2.1"><space unit="char" quantity="8"/>Grands Romains maîtres du monde,</l>
						<l n="14" num="2.2">Vous qui dépeupliez les cieux la terre et l’onde</l>
						<l n="15" num="2.3"><space unit="char" quantity="8"/>Pour fournir à vos déjeûnés</l>
						<l n="16" num="2.4"><space unit="char" quantity="8"/>Et qui de Roses couronnés</l>
						<l n="17" num="2.5">Sur vos sophas couchés près d’une tendre blonde,</l>
						<l n="18" num="2.6"><space unit="char" quantity="4"/>Faisiez l’amour en buvant à la ronde,</l>
						<l n="19" num="2.7"><space unit="char" quantity="8"/>Que vous étiez infortunés !</l>
						<l n="20" num="2.8"><space unit="char" quantity="8"/>Ce merveilleux sternutatoire,</l>
						<l n="21" num="2.9"><space unit="char" quantity="8"/>Dont l’Amérique se fait gloire,</l>
						<l n="22" num="2.10">Ce composé mordant de sels oxygénés,</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1">Par qui Santeuil et moi fumes empoissonnés,<ref type="noteAnchor">(a)</ref></l>
						<l n="24" num="3.2">Le Tabac puisqu’il faut le nommer sans grimoire,</l>
						<l n="25" num="3.3">Mêts généreux, chéri des Enfans de la gloire,</l>
						<l n="26" num="3.4">De sa sève jamais ne réjouit vos nez.</l>
						<l n="27" num="3.5"><space unit="char" quantity="10"/>Vous aviez appris d’Ovide</l>
						<l n="28" num="3.6"><space unit="char" quantity="10"/>Le gracieux art d’aimer</l>
						<l n="29" num="3.7"><space unit="char" quantity="10"/>Mais au sénat vous ruminiez à vide,</l>
						<l n="30" num="3.8">Ne sachant ni priser, ni chiquer<ref type="noteAnchor">(b)</ref>, ni fumer,</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><space unit="char" quantity="10"/>Que faisoient donc vos gendarmes,</l>
						<l n="32" num="4.2"><space unit="char" quantity="10"/>Lorsque leur arc débandé,</l>
						<l n="33" num="4.3"><space unit="char" quantity="10"/>Avoit mis fin aux allarmes</l>
						<l n="34" num="4.4"><space unit="char" quantity="10"/>Du beau sexe intimidé ?</l>
						<l n="35" num="4.5"><space unit="char" quantity="10"/>Au fonds de leurs corps-de-garde»,</l>
						<l n="36" num="4.6"><space unit="char" quantity="10"/>Apparemment ils filoient !</l>
						<l n="37" num="4.7"><space unit="char" quantity="10"/>Les quenoilles figuroient</l>
						<l n="38" num="4.8"><space unit="char" quantity="10"/>Vis-à-vis des hallebardes !</l>
						<l n="39" num="4.9"><space unit="char" quantity="10"/>Chez nous on y voit, morbleu,</l>
						<l n="40" num="4.10"><space unit="char" quantity="10"/>Le Dragon lancer le feu</l>
						<l n="41" num="4.11"><space unit="char" quantity="10"/>Par les yeux,et par la bouche ;</l>
						<l n="42" num="4.12"><space unit="char" quantity="10"/>Il prend, à ce, noble jeu,</l>
						<l n="43" num="4.13"><space unit="char" quantity="10"/>L’air amical et farouche,</l>
						<l n="44" num="4.14"><space unit="char" quantity="10"/>Qui lui sied et qui me touche ;</l>
						<l n="45" num="4.15"><space unit="char" quantity="10"/>Il est dans son élément.</l>
					</lg>
					<lg n="5">
						<l n="46" num="5.1"><space unit="char" quantity="10"/>Mais vous, dites-moi comment,</l>
						<l n="47" num="5.2">Sans tabatière assis dans sa chaise curulle,</l>
						<l n="48" num="5.3">Ne ronfloit pas tout haut votre ivrogne Luculle ?</l>
					</lg>
					<lg n="6">
						<l n="49" num="6.1"><space unit="char" quantity="8"/>Que donniez vous, au lieu d’argent,</l>
						<l n="50" num="6.2"><space unit="char" quantity="8"/>A votre juge incorruptible ?</l>
						<l n="51" num="6.3"><space unit="char" quantity="8"/>N’ayant pas ce meuble obligeant,</l>
						<l n="52" num="6.4"><space unit="char" quantity="8"/>Dont le métal irrésistible</l>
						<l n="53" num="6.5"><space unit="char" quantity="8"/>Rend toute faute rémissible ?</l>
						<l n="54" num="6.6">Du progrès de nos arts monument enchanteur,</l>
						<l n="55" num="6.7">O Bijou qui contiens la poudre délectable</l>
						<l n="56" num="6.8"><space unit="char" quantity="4"/>(Dont j’aime assez qu’on s’abstienne à ma table,</l>
						<l n="57" num="6.9">Tu ravis à-la-fois mes yeux, mon nez, mon cœur</l>
						<l n="58" num="6.10"><space unit="char" quantity="10"/>Dans une image chérie,</l>
						<l n="59" num="6.11"><space unit="char" quantity="10"/>Tu m’offres mon égérie ;</l>
						<l n="60" num="6.12"><space unit="char" quantity="10"/>A tous les instans du jour,</l>
						<l n="61" num="6.13"><space unit="char" quantity="10"/>Tu m’avertis que j’existe :</l>
						<l n="62" num="6.14"><space unit="char" quantity="10"/>Et ton doux parfum m’assiste</l>
						<l n="63" num="6.15"><space unit="char" quantity="10"/>Quand par fois je fais la cour</l>
						<l n="64" num="6.16"><space unit="char" quantity="8"/>A mon parrain le Jeanséniste.</l>
					</lg>
					<lg n="7">
						<l n="65" num="7.1">Dieu des pauvres rimeurs, Tabac, à ton secours,</l>
						<l n="66" num="7.2">Ma muse infortunée, à fréquemment recours,</l>
						<l n="67" num="7.3"><space unit="char" quantity="8"/>Lorsque suant à grosses gouttes</l>
						<l n="68" num="7.4">Du Parnasse elle suit les raboteuses routes</l>
						<l n="69" num="7.5"><space unit="char" quantity="10"/>Et de Pégase rendu</l>
						<l n="70" num="7.6"><space unit="char" quantity="10"/>Presse le dos peu dodu</l>
						<l n="71" num="7.7">Pour aller accoucher de quelqu’enfant perdu.</l>
					</lg>
					<lg n="8">
						<l n="72" num="8.1"><space unit="char" quantity="10"/>Quoi ! Rome et la Grèce antique</l>
						<l n="73" num="8.2"><space unit="char" quantity="10"/>Et les gaulois nos aînés</l>
						<l n="74" num="8.3"><space unit="char" quantity="10"/>Ignoroient donc la pratique</l>
						<l n="75" num="8.4"><space unit="char" quantity="10"/>De ce charmant Narcotique</l>
						<l n="76" num="8.5"><space unit="char" quantity="10"/>Si chéri des nez bien nés</l>
						<l n="77" num="8.6"><space unit="char" quantity="10"/>De la noblesse Helvétique<ref type="noteAnchor">(c)</ref> !</l>
						<l n="78" num="8.7"><space unit="char" quantity="10"/>Il me font vraiment pitié :</l>
						<l n="79" num="8.8"><space unit="char" quantity="10"/>Car n’en déplaise au critique,</l>
						<l n="80" num="8.9"><space unit="char" quantity="10"/>Qui chérit le sel attique</l>
						<l n="81" num="8.10"><space unit="char" quantity="10"/>Et se rit de mon cantique,</l>
						<l n="82" num="8.11"><space unit="char" quantity="10"/>L’on n’existe qu’à moitié</l>
						<l n="83" num="8.12"><space unit="char" quantity="10"/>N’ayant pas dans sa boutique</l>
						<l n="84" num="8.13"><space unit="char" quantity="10"/>Cette poudre sympathique</l>
						<l n="85" num="8.14"><space unit="char" quantity="10"/>Le lien de l’amitié,</l>
						<l n="86" num="8.15"><space unit="char" quantity="10"/>L’Ame de la politique.</l>
					</lg>
					<lg n="9">
						<l n="87" num="9.1"><space unit="char" quantity="10"/>Pauvres Grecs, pauvres Latins,</l>
						<l n="88" num="9.2"><space unit="char" quantity="10"/>Pauvres Gaulois je vous plains,</l>
						<l n="89" num="9.3">Du fond de mon palais, près Montmirelle en Brie,</l>
						<l n="90" num="9.4"><space unit="char" quantity="8"/>Mon ame est sur vous attendrie</l>
						<l n="91" num="9.5"><space unit="char" quantity="8"/>Vous avez tous passé le Bac,</l>
						<l n="92" num="9.6"><space unit="char" quantity="8"/>Et n’avez pas dans votre vie,</l>
						<l n="93" num="9.7"><space unit="char" quantity="8"/>Pris une prise de Tabac.</l>
					</lg>
					<ab type="star">🙪</ab>
					<lg n="10">
						<l n="94" num="10.1"><space unit="char" quantity="10"/>Cher ami pour te complaire</l>
						<l n="95" num="10.2"><space unit="char" quantity="10"/>Voilà ce que j’ai pu faire,</l>
						<l n="96" num="10.3"><space unit="char" quantity="10"/>Bien misérable salaire,</l>
						<l n="97" num="10.4"><space unit="char" quantity="10"/>De ton hospitalité.</l>
						<l n="98" num="10.5"><space unit="char" quantity="10"/>En seras tu contenté ?</l>
						<l n="99" num="10.6"><space unit="char" quantity="10"/>Je m en flatte et je l’espère</l>
						<l n="100" num="10.7"><space unit="char" quantity="10"/>Car je ne fais point mystère,</l>
						<l n="101" num="10.8"><space unit="char" quantity="10"/>Cousin qui vaut mieux qu’un frère,</l>
						<l n="102" num="10.9"><space unit="char" quantity="10"/>Que, dans l’ardeur qui me prit,</l>
						<l n="103" num="10.10"><space unit="char" quantity="10"/>De courir après Voltaire,</l>
						<l n="104" num="10.11"><space unit="char" quantity="10"/>Je n’ai pas fait mon affaire.</l>
						<l n="105" num="10.12"><space unit="char" quantity="10"/>Ce malheur, qui, me surprit,</l>
						<l n="106" num="10.13"><space unit="char" quantity="10"/>A mes propres frais m’apprit</l>
						<l n="107" num="10.14"><space unit="char" quantity="10"/>Qu’il vaut mieux, dans ce siècle âne</l>
						<l n="108" num="10.15"><space unit="char" quantity="10"/>Vendre la <hi rend="ital">Nicotiâne</hi><ref type="noteAnchor">(d)</ref></l>
						<l n="109" num="10.16"><space unit="char" quantity="10"/>Que de vendre de l’esprit.</l>
					</lg>
					<ab type="star">🙪</ab>
					<closer>
						<note type="footnote" id="(a)">Tout le monde sait l’histoire de Sonteuil : la mienne fut moins Tragique.</note>
						<note type="footnote" id="(b)">Expression technique pour dire <hi rend="ital">macher du tabac</hi>.</note>
						<note type="footnote" id="(c)">On connoit le proverbe. Il prend du Tabac comme un Suisse.</note>
						<note type="footnote" id="(d)">Nom du Tabac en botanique.</note>
					</closer>
				</div>
			</div>
		</body>
	</text>
</TEI>