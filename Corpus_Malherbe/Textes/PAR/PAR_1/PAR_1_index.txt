PAR
———
PAR_1

Évariste de PARNY
1753-1814

════════════════════════════════════════════
Œuvres complètes

tome I
1775-1806

5617 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ POÉSIES ÉROTIQUES
	─────────────────────────────────────────────────────
	▫ LIVRE PREMIER
		─ poème	PAR1	I. LE LENDEMAIN - A ÉLÉONORE
		─ poème	PAR2	II. ÉGLOGUE
		─ poème	PAR3	III. LA DISCRÉTION
		─ poème	PAR4	IV. BILLET
		─ poème	PAR5	V. LA FRAYEUR
		─ poème	PAR6	VI. VERS - GRAVÉS SUR UN ORANGER
		─ poème	PAR7	VII. DIEU VOUS BÉNISSE
		─ poème	PAR8	VIII. LE REMÈDE DANGEREUX
		─ poème	PAR9	IX. DEMAIN
		─ poème	PAR10	X. LE REVENANT
		─ poème	PAR11	XI. LES PARADIS
		─ poème	PAR12	XII. FRAGMENT D’ALCÉE
		─ poème	PAR13	XIII. PLAN D’ÉTUDES
		─ poème	PAR14	XIV. PROJET DE SOLITUDE
		─ poème	PAR15	XV. BILLET
	─────────────────────────────────────────────────────
	▫ LIVRE SECOND
		─ poème	PAR16	I. LE REFROIDISSEMENT
		─ poème	PAR17	II. A LA NUIT
		─ poème	PAR18	III. LA RECHUTE
		─ poème	PAR19	IV. ÉLÉGIE
		─ poème	PAR20	V. DÉPIT
		─ poème	PAR21	VI. A UN AMI - TRAHI PAR SA MAÎTRESSE*
		─ poème	PAR22	VII. IL EST TROP TARD
		─ poème	PAR23	VIII. A MES AMIS
		─ poème	PAR24	IX. AUX INFIDÈLES
		─ poème	PAR25	XRETOUR A ÉLÉONORE
		─ poème	PAR26	XI. PALINODIE
		─ poème	PAR27	XII. LE RACCOMMODEMENT
	─────────────────────────────────────────────────────
	▫ LIVRE TROISIÈME
		─ poème	PAR28	I. LES SERMENS
		─ poème	PAR29	II. SOUVENIR
		─ poème	PAR30	III. LE SONGE
		─ poème	PAR31	IV. MA RETRAITE
		─ poème	PAR32	V. AU GAZONFOULÉ PAR ÉLÉONORE
		─ poème	PAR33	VI. LE VOYAGE MANQUÉ
		─ poème	PAR34	VII. LE CABINET DE TOILETTE
		─ poème	PAR35	VIII. L’ABSENCE
		─ poème	PAR36	IX. MA MORT
		─ poème	PAR37	X. L’IMPATIENCE
		─ poème	PAR38	XI. RÉFLEXION AMOUREUSE
		─ poème	PAR39	XII. LE BOUQUET DE L’AMOUR
		─ poème	PAR40	XIII. DÉLIRE
		─ poème	PAR41	XIV. LES ADIEUX
	─────────────────────────────────────────────────────
	▫ LIVRE QUATRIÈME
		─ poème	PAR42	ÉLÉGIE I
		─ poème	PAR43	ÉLÉGIE II
		─ poème	PAR44	ÉLÉGIE III
		─ poème	PAR45	ÉLÉGIE IV
		─ poème	PAR46	ÉLÉGIE V
		─ poème	PAR47	ÉLÉGIE VI
		─ poème	PAR48	ÉLÉGIE VII
		─ poème	PAR49	ÉLÉGIE VIII
		─ poème	PAR50	ÉLÉGIE IX
		─ poème	PAR51	ÉLÉGIE X
		─ poème	PAR52	ÉLÉGIE XI
		─ poème	PAR53	ÉLÉGIE XII
		─ poème	PAR54	ÉLÉGIE XIII
		─ poème	PAR55	ÉLÉGIE XIV
	─────────────────────────────────────────────────────
	▫ APPENDICE
		─ poème	PAR56	I. A ÉLÉONORE
		─ poème	PAR57	II. A AGLAÉ
		─ poème	PAR58	III. A UN AMANT
		─ poème	PAR59	IV. SUR LA MALADIE D’ÉLÉONORE
		─ poème	PAR60	V. LES IMPRÉCATIONS
		─ poème	PAR61	VI. PRIÈRES AU SOMMEIL
		─ poème	PAR62	VII. A MONSIEUR DE F.
		─ poème	PAR63	VIII. COURROUX D’UN AMANT
		─ poème	PAR64	IX. A ÉLÉONORE
		─ poème	PAR65	X. IL FAUT AIMER
		─ poème	PAR66	XI. CHANSON
─ poème	PAR67	LA JOURNÉE CHAMPÊTRE
─ poème	PAR68	LES FLEURS
─ poème	PAR69	JAMSEL

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES TABLEAUX
	─ poème	PAR70	TABLEAU PREMIER. LA ROSE
	─ poème	PAR71	TABLEAU II. LA MAIN
	─ poème	PAR72	TABLEAU III. LE SONGE
	─ poème	PAR73	TABLEAU IV. LE SEIN
	─ poème	PAR74	TABLEAU V. LE BAISER
	─ poème	PAR75	TABLEAU VI. LES RIDEAUX
	─ poème	PAR76	TABLEAU VII. LE LENDEMAIN
	─ poème	PAR77	TABLEAU VIII. L’INFIDÉLITÉ
	─ poème	PAR78	TABLEAU IX. LES REGRETS
	─ poème	PAR79	TABLEAU X. LE RETOUR

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES DÉGUISEMENS DE VÉNUS
	─ poème	PAR80	TABLEAU PREMIER. "Aux bergers la naissante Aurore"
	─ poème	PAR81	TABLEAU II. "Myrtis dans la forêt obscure"
	─ poème	PAR82	TABLEAU III. "» Dryades, pourquoi fuyez-vous ?"
	─ poème	PAR83	TABLEAU IV. "Dans sa cabane solitaire"
	─ poème	PAR84	TABLEAU V. "« Nymphe de ce riant bocage,"
	─ poème	PAR85	TABLEAU VI. "Sous des ombrages solitaires"
	─ poème	PAR86	TABLEAU VII. "Phébus achevait sa, carrière ;"
	─ poème	PAR87	TABLEAU VIII. "« Berger, j’appartiens à Diane :"
	─ poème	PAR88	TABLEAU IX. "D’Érigone c’était la fête."
	─ poème	PAR89	TABLEAU X. "« Jeune berger, respecte Égine."
	─ poème	PAR90	TABLEAU XI. "Le ciel est pur, mais sans lumière ;"
	─ poème	PAR91	TABLEAU XII. "Myrtis sur le fleuve rapide"
	─ poème	PAR92	TABLEAU XIII. "Caché dans une grotte humide"
	─ poème	PAR93	TABLEAU XIV. "« Qu’ordonnez-vous, chaste déesse ?"
	─ poème	PAR94	TABLEAU XV. "Dans l’onde fraîche une bergère"
	─ poème	PAR95	TABLEAU XVI. "Du midi s’élance l’orage."
	─ poème	PAR96	TABLEAU XVII. "« De Myrtis que la voix est tendre !"
	─ poème	PAR97	TABLEAU XVIII. "« Ma fidélité conjugale"
	─ poème	PAR98	TABLEAU XIX. "L’amour ne connaît point la crainte."
	─ poème	PAR99	TABLEAU XX. "De la jeune et belle prêtresse"
	─ poème	PAR100	TABLEAU XXI. "Des Dieux la prompte messagère"
	─ poème	PAR101	TABLEAU XXII. "Assise sur un faisceau d’armes"
	─ poème	PAR102	TABLEAU XXIII. "Viens, jeune et charmante Théone."
	─ poème	PAR103	TABLEAU XXIV. "Myrtis devant Junon s’incline."
	─ poème	PAR104	TABLEAU XXV. "Du haut des airs qu’elle colore,"
	─ poème	PAR105	TABLEAU XXVI. "Rêveuse et doucement émue,"
	─ poème	PAR106	TABLEAU XXVIII
	─ poème	PAR107	TABLEAU XXIX. "« Arrêtez, charmante Déesse !"
	─ poème	PAR108	TABLEAU XXX. "Il dort ; un baiser le réveille."
─ poème	PAR109	LE VOYAGE DE CÉLINE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ MÉLANGES
	─ poème	PAR110	A MON FRÈRE
	─ poème	PAR111	A BERTIN
	─ poème	PAR112	AU MÊME
	─ poème	PAR113	A M. LE CHEVALIER DE CUBIÈRES
	─ poème	PAR114	A M. DE P… DU S…
	─ poème	PAR115	ÉPÎTREAUX INSURGENS
	─ poème	PAR116	DIALOGUEENTRE UN POÈTE ET SA MUSE
	─ poème	PAR117	MADRIGAL
	─ poème	PAR118	MADRIGALA MADAME DE T…
	─ poème	PAR119	A M. LE CHEVALIER DE PARNY
	─ poème	PAR120	RÉPONSE
	─ poème	PAR121	A BERTIN
	─ poème	PAR122	ÉPITAPHE
	─ poème	PAR123	A LA HARPE, - SUR SA COMÉDIE DES MUSES RIVALES
	─ poème	PAR124	A BERTIN
	─ poème	PAR125	A MONSIEUR LE CHEVALIER DE PARNY
	─ poème	PAR126	RÉPONSE
	─ poème	PAR127	PORTRAIT
