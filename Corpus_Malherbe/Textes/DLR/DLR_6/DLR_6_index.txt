DLR
———
DLR_6

Lucie DELARUE-MARDRUS
1874-1945

════════════════════════════════════════════
SOUFFLES DE TEMPÊTE

1918

3271 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I L’AUTOMNE A CHEVAL
	─ poème	DLR553	LA RENCONTRE D’AUTOMNE
	─ poème	DLR554	COLÈRE
	─ poème	DLR555	AU PAS
	─ poème	DLR556	AU TROT
	─ poème	DLR557	AU GALOP
	─ poème	DLR558	APAISEMENT
	─ poème	DLR559	FÉERIE
	─ poème	DLR560	PEUR
	─ poème	DLR561	CHASSE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II ADMIRATIONS
	─ poème	DLR562	HONFLEUR
	─ poème	DLR563	EX-VOTO
	─ poème	DLR564	MA MAISON
	─ poème	DLR565	PAUVRE MORCEAU DE BOIS…
	─ poème	DLR566	INCANTATION
	─ poème	DLR567	PARIS
	─ poème	DLR568	RÉDEMPTION DU SOIR
	─ poème	DLR569	A MADEMOISELLE CHAUVELOT
	─ poème	DLR570	HARPE
	─ poème	DLR571	YVONNE ASTRUC
	─ poème	DLR572	SYMPHONIE FANTASTIQUE
	─ poème	DLR573	EN L’HONNEUR DE J. S. BACH
	─ poème	DLR574	VISITATION

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III LE SPHINX
	─ poème	DLR575	LE SPHINX
	─ poème	DLR576	ÉNIGMES
	─ poème	DLR577	HÉLIOPOLIS
	─ poème	DLR578	AUX TOMBEAUX DES KHALIFES
	─ poème	DLR579	A PHILÆ
	─ poème	DLR580	A LA DÉSSE MAUT
	─ poème	DLR581	D’ALEXANDRIE
	─ poème	DLR582	VŒUX
	─ poème	DLR583	A L’ANCRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IV CHEVAUX DE LA MER
	─ poème	DLR584	L’ACCUEIL
	─ poème	DLR585	COLÈRE MARINE
	─ poème	DLR586	PRÉSENCE
	─ poème	DLR587	MONTÉE DE MER
	─ poème	DLR588	PROMENADES
	─ poème	DLR589	NOCTURNE
	─ poème	DLR590	QUATRE POÈMES DE MER
	─ poème	DLR591	ORAISON

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ V ARRIÈRES-SAISONS
	─ poème	DLR592	D’ABORD
	─ poème	DLR593	TOMBEAU
	─ poème	DLR594	J’AI SI GRANDE AMITIÉ…
	─ poème	DLR595	FASCINANTE DOUCEUR DE L’EAU…
	─ poème	DLR596	ROMANICHELS
	─ poème	DLR597	TOURMENT
	─ poème	DLR598	L’ANGOISSE
	─ poème	DLR599	TRISTESSE
	─ poème	DLR600	A RAPHAËL SCHWARTZ
	─ poème	DLR601	STATUE
	─ poème	DLR602	AUMÔNE
	─ poème	DLR603	AUX ANONYMES
	─ poème	DLR604	JE NE COMPRENDS PAS…
	─ poème	DLR605	POUR EUX ET POUR ELLES
	─ poème	DLR606	UNE PRIÈRE A SAINT GEORGES
	─ poème	DLR607	RONDEL
	─ poème	DLR608	J’AI PENDANT SI LONGTEMPS…
	─ poème	DLR609	O MON DIEU…
	─ poème	DLR610	PETITE ÉLÉGIE
	─ poème	DLR611	LA GRANDE SOLITUDE
	─ poème	DLR612	ALLAH OUAHDOU

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VI PROPHÉTIQUES
	─ poème	DLR613	TÉNÈBRES
	─ poème	DLR614	RÊVERIE
	─ poème	DLR615	LIÈGE
	─ poème	DLR616	CHAMPIGNY
	─ poème	DLR617	IN EXCELSIS
	─ poème	DLR618	UNE RÉPONSE A RUDYARD KIPLING

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VII LA GUERRE
	─ poème	DLR619	BALLADE DU MOBILISÉ
	─ poème	DLR620	AU ROI ALBERT 1er
	─ poème	DLR621	LE DRAPEAU
	─ poème	DLR622	CROIX ROUGE
	─ poème	DLR623	AUX GAS NORMANDS
	─ poème	DLR624	DÉPART
	─ poème	DLR625	SALUT MARIN
	─ poème	DLR626	MALÉDICTION
	─ poème	DLR627	RESPONSABILITÉS
	─ poème	DLR628	SAINTE CATHERINE
	─ poème	DLR629	A HONFLEUR
	─ poème	DLR630	INVITATION A JEANNE D’ARC
	─ poème	DLR631	LES GARDIENS
	─ poème	DLR632	AU JARDIN DE MAI
	─ poème	DLR633	CONSCIENCE
	─ poème	DLR634	LES BEAUX POMMIERS
	─ poème	DLR635	PRINTEMPS
	─ poème	DLR636	MES BONNES ROUTES
	─ poème	DLR637	NOCTURNE À PARIS
	─ poème	DLR638	ZEPPELINS
	─ poème	DLR639	SAINTES ALLIÉES
	─ poème	DLR640	CE QUATORZE JUILLET…
	─ poème	DLR641	RÉGIMENTS
	─ poème	DLR642	BLEUETS
	─ poème	DLR643	PETITE ÉGLISE
	─ poème	DLR644	REFRAIN
	─ poème	DLR645	AU COIN DU FEU
	─ poème	DLR646	INVITATION
	─ poème	DLR647	CLAIR DE LUNE
	─ poème	DLR648	FANTÔMES
	─ poème	DLR649	AUX POMMES
	─ poème	DLR650	SOIRS D’AUTOMNE
	─ poème	DLR651	TOUSSAINT
	─ poème	DLR652	LE CORBEAU
	─ poème	DLR653	LA GRANDE OFFENSIVE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VIII DEUILS ROUGES
	─ poème	DLR654	ÉLÉGIE DOUBLE
	─ poème	DLR655	MARC
	─ poème	DLR656	JEAN
	─ poème	DLR657	A ROBERT D’HUMIÈRES
	─ poème	DLR658	A DES PARENTS
	─ poème	DLR659	LES FUNÉRAILLES DU SOLDAT
	─ poème	DLR660	CORTÈGE FUNÈBRE
	─ poème	DLR661	A GEORGES TROUILLOT
