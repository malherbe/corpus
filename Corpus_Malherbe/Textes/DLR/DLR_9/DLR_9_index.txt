DLR
———
DLR_9

Lucie DELARUE-MARDRUS
1874-1945

════════════════════════════════════════════
LES SEPT DOULEURS D’OCTOBRE

1930

2316 vers

─ poème	DLR783	PRÉFACE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ I L’AUTOMNE
	─ poème	DLR784	LITANIES
	─ poème	DLR785	CHEVEUX COUPÉS
	─ poème	DLR786	CONVERSATION
	─ poème	DLR787	CINQ PETITS TABLEAUX
	─ poème	DLR788	TEMPÊTE D’OCTOBRE
	─ poème	DLR789	SONNERIE
	─ poème	DLR790	LE VIEUX MANOIR
	─ poème	DLR791	CONSEILS DU FEU

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ II L’AVENUE
	─ poème	DLR792	MA MAISON
	─ poème	DLR793	CIMETIÈRE
	─ poème	DLR794	LE BEAU MOMENT
	─ poème	DLR795	AT HOME
	─ poème	DLR796	PERSUASION
	─ poème	DLR797	PLUIE
	─ poème	DLR798	TROIS NOCTURNES
	─ poème	DLR799	LA HAIE
	─ poème	DLR800	HIVER
	─ poème	DLR801	TOMBEAU

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ III MAI, JOLI MAI !
	─ poème	DLR802	MAI
	─ poème	DLR803	PÂQUES FLEURIES
	─ poème	DLR804	PRIMAVERA
	─ poème	DLR805	DANS LE PRÉ
	─ poème	DLR806	CRÉPUSCULE
	─ poème	DLR807	OUBLI
	─ poème	DLR808	MAI
	─ poème	DLR809	RONDEL ÉQUESTRE
	─ poème	DLR810	CATÉCHISME
	─ poème	DLR811	SOUPIR
	─ poème	DLR812	D’UN SOIR DE MAI
	─ poème	DLR813	PORTE DU PRINTEMPS

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IV DE LA VIE A LA MORT
	─ poème	DLR814	HYPOTHÈSE
	─ poème	DLR815	L’HOTE DE DEUX JOURS
	─ poème	DLR816	A MON CŒUR
	─ poème	DLR817	MOURIR
	─ poème	DLR818	FINALE
	─ poème	DLR819	L’HUMILIATION

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ V CI-GÎT
	─ poème	DLR820	CI-GÎT
	─ poème	DLR821	D’AMIENS
	─ poème	DLR822	LA SOMME
	─ poème	DLR823	RETOUR
	─ poème	DLR824	LA BELLE IMAGE
	─ poème	DLR825	AUX AUTRES
	─ poème	DLR826	GAIE
	─ poème	DLR827	POSTÉRITÉ

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VI SPECTRES
	─ poème	DLR828	L’ABSENTE
	─ poème	DLR829	LUEURS
	─ poème	DLR830	A MES QUATRE
	─ poème	DLR831	QUE M’IMPORTE
	─ poème	DLR832	BIEN-ÊTRE
	─ poème	DLR833	PROMENADES
	─ poème	DLR834	FINALEMENT
	─ poème	DLR835	LA MESSAGÈRE
	─ poème	DLR836	LE CERCUEIL VIVANT
	─ poème	DLR837	DEUX BARQUES
	─ poème	DLR838	LA PETITE TOMBE
	─ poème	DLR839	SÉPULTURES

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VII L’ANGOISSE
	─ poème	DLR840	LAUDES
	─ poème	DLR841	JE NE SAIS PAS…
	─ poème	DLR842	L’ADIEU
	─ poème	DLR843	TRAVAIL
	─ poème	DLR844	LASSITUDE
	─ poème	DLR845	QU’AI-JE BESOIN…
	─ poème	DLR846	UN RÊVE
	─ poème	DLR847	NON CREDO
	─ poème	DLR848	LA PEUR
	─ poème	DLR849	UN DIMANCHE
	─ poème	DLR850	MON DIEU…
	─ poème	DLR851	PRIÈRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VIII BONHEURS
	─ poème	DLR852	MÉDITATION
	─ poème	DLR853	O TEMPS !…
	─ poème	DLR854	CONFESSION
	─ poème	DLR855	LE BONHEUR
	─ poème	DLR856	MON CHIEN
	─ poème	DLR857	KIKI
	─ poème	DLR858	LE BEAU SOUHAIT
	─ poème	DLR859	COUP D’ŒIL
	─ poème	DLR860	POMMES
	─ poème	DLR861	PROMENADE
	─ poème	DLR862	MODES
	─ poème	DLR863	ART POÉTIQUE
	─ poème	DLR864	MUSIQUE
	─ poème	DLR865	A CEUX QUI L’AIMENT
	─ poème	DLR866	BONHEURS
	─ poème	DLR867	ÉPILOGUE
