BRB
———
BRB_1

Auguste BARBIER
1805-1882

════════════════════════════════════════════
Ïambes et poèmes

1831

4203 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ÏAMBES
	─ poème	BRB1	TOUTES LES MUSES
	─ poème	BRB2	PROLOGUE
	─ poème	BRB3	LA CURÉE
	─ poème	BRB4	LE LION
	─ poème	BRB5	QUATRE-VINGT-TREIZE
	─ poème	BRB6	L’ÉMEUTE
	─ poème	BRB7	LA POPULARITÉ
	─ poème	BRB8	L’IDOLE
	─ poème	BRB9	VARSOVIE
	─ poème	BRB10	DANTE
	─ poème	BRB11	MELPOMÈNE
	─ poème	BRB12	TERPSICHORE
	─ poème	BRB13	L’AMOUR DE LA MORT
	─ poème	BRB14	LA REINE DU MONDE
	─ poème	BRB15	LES VICTIMES
	─ poème	BRB16	LE PROGRÈS
	─ poème	BRB17	LE RIRE
	─ poème	BRB18	LA CUVE
	─ poème	BRB19	DÉSOLATION

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IL PIANTO
	─ poème	BRB20	"Il est triste de voir partout l’œuvre du mal,"
	─ poème	BRB21	LE DÉPART
	─ poème	BRB22	LE CAMPO SANTO
	─ poème	BRB23	MAZACCIO
	─ poème	BRB24	MICHEL-ANGE
	─ poème	BRB25	ALLEGRI
	─ poème	BRB26	LE CAMPO VACCINO
	─ poème	BRB27	RAPHAËL
	─ poème	BRB28	LE CORRÉGE
	─ poème	BRB29	CIMAROSA
	─ poème	BRB30	CHIAIA
	─ poème	BRB31	DOMINIQUIN
	─ poème	BRB32	LÉONARD DE VINCI
	─ poème	BRB33	TITIEN
	─ poème	BRB34	BIANCA
	─ poème	BRB35	L’ADIEU

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LAZARE
	─ poème	BRB36	PROLOGUE
	─ poème	BRB37	LONDRES
	─ poème	BRB38	BEDLAM
	─ poème	BRB39	LE GIN
	─ poème	BRB40	LE MINOTAURE
	─ poème	BRB41	LES BELLES COLLINES D’IRLANDE
	─ poème	BRB42	LA LYRE D’AIRAIN
	─ poème	BRB43	CONSCIENCE
	─ poème	BRB44	LA TAMISE
	─ poème	BRB45	LE FOUET
	─ poème	BRB46	LES MINEURS DE NEWCASTLE
	─ poème	BRB47	WESTMINSTER
	─ poème	BRB48	MENACE ET CORRUPTION
	─ poème	BRB49	LE PILOTE
	─ poème	BRB50	SHAKSPEARE
	─ poème	BRB51	LE SPLEEN
	─ poème	BRB52	LA NATURE
	─ poème	BRB53	ÉPILOGUE
