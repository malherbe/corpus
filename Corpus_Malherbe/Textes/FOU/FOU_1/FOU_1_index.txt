FOU
———
FOU_1

Georges FOUREST
1864-1945

════════════════════════════════════════════
LA NÉGRESSE BLONDE

1909

1172 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LA NÉGRESSE BLONDE
	─ poème	FOU1	LA NÉGRESSE BLONDE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ RENONCEMENT
	─ poème	FOU2	RENONCEMENT

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ SIX PSEUDO-SONNETS TRUCULENTS ET ALLÉGORIQUES
	─ poème	FOU3	PSEUDO-SONNET  PLUS SPÉCIALEMENT TRUCULENT ET ALLÉGORIQUE
	─ poème	FOU4	PSEUDO-SONNET PESSIMISTE ET OBJURGATOIRE
	─ poème	FOU5	PSEUDO-SONNET AFRICAIN ET GASTRONOMIQUE OU (PLUS SIMPLEMENT) REPAS DE FAMILLE
	─ poème	FOU6	PSEUDO-SONNET IMBRIAQUE ET DÉSESPÉRÉ
	─ poème	FOU7	PSEUDO-SONNET ASIATIQUE ET LITTÉRAIRE
	─ poème	FOU8	PSEUDO-SONNET QUE LES AMATEURS DE PLAISANTERIE FACILE PROCLAMERONT LE PLUS BEAU DU RECUEIL

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LA SINGESSE
	─ poème	FOU9	LA SINGESSE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ PETITES ÉLÉGIES FALOTES
	─ poème	FOU10	SARDINES À L’HUILE
	─ poème	FOU11	LE DOIGT DE DIEU
	─ poème	FOU12	LE VIEUX SAINT
	─ poème	FOU13	LES POISSONS MÉLOMANES
	─ poème	FOU14	FLEURS DES MORTS
	─ poème	FOU15	SOUVENIR OU AUTRE REPAS DE FAMILLE
	─ poème	FOU16	PETITS LAPONS
	─ poème	FOU17	JARDINS D’AUTOMNE
	─ poème	FOU18	PETITS CALICOTS

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ÉPITRE FALOTE ET BALNÉAIRE
	─ poème	FOU19	ÉPITRE FALOTE ET BALNÉAIRE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ CARNAVAL DE CHEFS-D’ŒUVRE
	─ poème	FOU20	LE CID
	─ poème	FOU21	PHÈDRE
	─ poème	FOU22	IPHIGÉNIE
	─ poème	FOU23	ANDROMAQUE
	─ poème	FOU24	BÉRÉNICE
	─ poème	FOU25	À LA VÉNUS DE MILO

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ BALLADE POUR FAIRE CONNAÎTRE MES OCCUPATIONS ORDINAIRES
	─ poème	FOU26	BALLADE - POUR FAIRE CONNAÎTRE MES OCCUPATIONS ORDINAIRES

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ BALLADE EN L’HONNEUR DES POÈTES FALOTS
	─ poème	FOU27	BALLADE - EN L’HONNEUR DES POÈTES FALOTS

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ ÉPITRE FALOTE ET TESTAMENTAIRE POUR RÉGLER L’ORDRE ET LA MARCHE DE MES FUNÉRAILLES
	─ poème	FOU28	ÉPITRE FALOTE ET TESTAMENTAIRE POUR RÉGLER L’ORDRE ET LA MARCHE DE MES FUNÉRAILLES
