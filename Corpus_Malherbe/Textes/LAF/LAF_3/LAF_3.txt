Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
LAF
LAF_3

Jules LAFORGUE
1860-1887

L’IMITATION DE NOTRE-DAME LA LUNE
1886
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

L’Imitation de Notre-Dame la Lune
Jules Laforgue



ATILF
Analyse et Traitement Informatique de la Langue Française


L493


L’Imitation de Notre-Dame la Lune
JULES LAFORGUE

Paris
Mercure de France
1922



_________________________________________________________________
Édition de référence pour les corrections métriques :

ŒUVRES COMPLÈTES I ‒ Poésies
JULES LAFORGUE

Slatkine Reprints
1979

ŒUVRES COMPLÈTES ‒ tome II
JULES LAFORGUE

L’Age d’Homme
1995


┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



UN MOT AU SOLEIL 
POUR COMMENCER

Soleil ! Soudard plaqué d’ordres et de crachats,
Planteur mal élevé, sache que les vestales
À qui la lune, en son équivoque œil-de-chat,
Est la rosace de l’unique cathédrale,


Sache que les Pierrots, phalènes des dolmens
Et des nymphéas blancs des lacs où dort Gomorrhe,
Et tous les bienheureux qui pâturent l’Éden
Toujours printanier des renoncements, — t’abhorrent.


Et qu’ils gardent pour toi des mépris spéciaux,
Bellâtre, maquignon, ruffian, rastaqouère
À breloques d’œufs d’or qui le prends de si haut
Avec la terre et son orpheline lunaire.


Continue à fournir de couchants avinés
Les lendemains vomis des fêtes nationales,
À styler tes saisons, à nous bien déchaîner
Les drames de l’apothéose ombilicale !


Va, Phœbus ! Mais, Dèva, dieu des réveils cabrés,
Regarde un peu parfois ce port-royal d’esthètes
Qui, dans leurs décamérons lunaires au frais,
Ne parlent de rien moins que mettre à prix ta tête.


Certes, tu as encor devant toi de beaux jours ;
Mais la tribu s’accroît, de ces vieilles pratiques
De l’a quoi bon ? qui vont rêvant l’art et l’amour
Au seuil lointain de l’agrégat inorganique.


Pour aujourd’hui, vieux beau, nous nous contenterons
De mettre sous le nez de ta badauderie
Le mot dont l’homme t’a déjà marqué au front ;
Tu ne t’en étais jamais douté, je parie ?


— Sache qu’on va disant d’une belle phrase, os
Sonore, mais très nul comme suc médullaire,
De tout boniment creux enfin : c’est du pathos,
C’est du phœbus !— ah ! Pas besoin de commentaires…


Ô vision du temps où l’être trop puni,
D’un : « eh ! Va donc, Phœbus ! » te rentrera ton prêche
De vieux crescite et multiplicamini,
Pour s’inoculer à jamais la lune fraîche !



LITANIES 
DES PREMIERS 
QUARTIERS DE LUNE

Lune bénie
Des insomnies,


Blanc médaillon
Des endymions,


Astre fossile
Que tout exile,


Jaloux tombeau
De Salammbô,


Embarcadère
Des grands mystères,


Madone et miss
Diane-Artémis,


Sainte vigie
De nos orgies,


Jettatura
Des baccarats,


Dame très lasse
De nos terrasses,


Philtre attisant
Les vers-luisants,


Rosace et dôme
Des derniers psaumes,


Bel œil-de-chat
De nos rachats,


Sois l’ambulance
De nos croyances !


Sois l’édredon
Du grand-pardon !



AU LARGE

Comme la nuit est lointainement pleine
De silencieuse infinité claire !
Pas le moindre écho des gens de la terre,
Sous la lune méditerranéenne !


Voilà le néant dans sa pâle gangue,
Voilà notre hostie et sa sainte-table,
Le seul bras d’ami par l’inconnaissable,
Le seul mot solvable en nos folles langues !


Au delà des cris choisis des époques,
Au delà des sens, des larmes, des vierges,
Voilà quel astre indiscutable émerge,
Voilà l’immortel et seul soliloque !


Et toi, là-bas, pot-au-feu, pauvre terre !
Avec tes essais de mettre en rubriques
Tes reflets perdus du grand dynamique !
Tu fais un métier, ah ! Bien sédentaire !



CLAIR DE LUNE

Penser qu’on vivra jamais dans cet astre,
Parfois me flanque un coup dans l’épigastre.


Ah ! Tout pour toi, lune, quand tu t’avances
Aux soirs d’août par les féeries du silence !


Et quand tu roules, démâtée, au large
À travers les brisants noirs des nuages !


Oh ! Monter, perdu, m’étancher à même
Ta vasque de béatifiants baptêmes !


Astre atteint de cécité, fatal phare
Des vols migrateurs des plaintifs Icares !


Œil stérile comme le suicide,
Nous sommes le congrès des las, préside ;


Crâne glacé, raille les calvities
De nos incurables bureaucraties ;


Ô pilule des léthargies finales,
Infuse-toi dans nos durs encéphales !


Ô Diane à la chlamyde très dorique,
L’amour cuve, prend ton carquois et pique


Ah ! D’un trait inoculant l’être aptère,
Les cœurs de bonne volonté sur terre !


Astre lavé par d’inouïs déluges,
Qu’un de tes chastes rayons fébrifuges,


Ce soir, pour inonder mes draps, dévie,
Que je m’y lave les mains de la vie !



CLIMAT, FAUNE, FLORE DE LA LUNE

Des nuits, ô lune d’immaculée-conception,
Moi, vermine des nébuleuses d’occasion,
J’aime, du frais des toits de notre Babylone,
Concevoir ton climat et ta flore et ta faune.


Ne sachant qu’inventer pour t’offrir mes ennuis,
Ô radeau du nihil aux quais seuls de nos nuits !


Ton atmosphère est fixe, et tu rêves, figée
En climats de silence, écho de l’hypogée
D’un ciel atone où nul nuage ne s’endort
Par des vents chuchotant tout au plus qu’on est mort ?
Des montagnes de nacre et des golfes d’ivoire
Se renvoient leurs parois de mystiques ciboires,
En anses où, sur maint pilotis, d’un air lent,
Des sirènes font leurs nattes, lèchent leurs flancs,


Blêmes d’avoir gorgé de lunaires luxures
Là-bas, ces gais dauphins aux geysers de mercure.


Oui, c’est l’automne incantatoire et permanent
Sans thermomètre, embaumant mers et continents,
Étangs aveugles, lacs ophtalmiques, fontaines
De Léthé, cendres d’air, déserts de porcelaine,
Oasis, solfatares, cratères éteints,
Arctiques sierras, cataractes l’air en zinc,
Hauts-plateaux crayeux, carrières abandonnées,
Nécropoles moins vieilles que leurs graminées,
Et des dolmens par caravanes, — et tout très
Ravi d’avoir fait son temps, de rêver au frais.


Salut, lointains crapauds ridés, en sentinelles
Sur les pics, claquant des dents à ces tourterelles
Jeunes qu’intriguent vos airs ! Salut, cétacés
Lumineux ! Et vous, beaux comme des cuirassés,
Cygnes d’antan, nobles témoins des cataclysmes ;
Et vous, paons blancs cabrés en aurores de prismes ;
Et vous, fœtus voûtés, glabres contemporains
Des sphinx brouteurs d’ennuis aux moustaches d’airain
Qui, dans le clapotis des grottes basaltiques,
Ruminez l’enfin ! Comme une immortelle chique !


Oui, rennes aux andouillers de cristal ; ours blancs
Graves comme des mages, vous déambulant,
Les bras en croix vers les miels du divin silence !
Porcs-épics fourbissant sans but vos blêmes lances ;
Oui, papillons aux reins pavoisés de joyaux
Ouvrant vos ailes à deux battants d’in-folios ;
Oui, gélatines d’hippopotames en pâles
Flottaisons de troupeaux éclaireurs d’encéphales ;
Pythons en intestins de cerveaux morts d’abstrait,
Bancs d’éléphas moisis qu’un souffle effriterait !


Et vous, fleurs fixes ! Mandragores à visages,
Cactus obéliscals aux fruits en sarcophages,
Forêts de cierges massifs, parcs de polypiers,
Palmiers de corail blanc aux résines d’acier !
Lys marmoréens à sourires hystériques,
Qui vous mettez à débiter d’albes musiques
Tous les cent ans, quand vous allez avoir du lait !
Champignons aménagés comme des palais !


Ô fixe ! On ne sait plus à qui donner la palme
Du lunaire ; et surtout quelle leçon de calme !
Tout a l’air émané d’un même acte de foi
Au néant quotidien sans comment ni pourquoi !
Et rien ne fait de l’ombre, et ne se désagrège ;
Ne naît, ni ne mûrit ; tout vit d’un sortilège
Sans foyer qui n’induit guère à se mettre en frais
Que pour des amours blancs, lunaires et distraits…


Non, l’on finirait par en avoir mal de tête,
Avec le rire idiot des marbres égynètes
Pour jamais tant tout ça stagne en un miroir mort !
Et l’on oublierait vite comment on en sort.


Et pourtant, ah ! C’est là qu’on en revient encore
Et toujours, quand on a compris le madrépore.



GUITARE

Astre sans cœur et sans reproche,
Ô maintenon de vieille roche !


Très révérende supérieure
Du cloître où l’on ne sait plus l’heure,


D’un port-royal port de Circée
Où Pascal n’a d’autres pensées


Que celles du roseau qui jase
Ne sait plus quoi, ivre de vase…


Oh ! Qu’un Philippe De Champaigne,
Mais ne pierrot, vienne et te peigne !


Un rien, une miniature
De la largeur d’une tonsure ;


Ça nous ferait un scapulaire
Dont le contact anti-solaire,


Par exemple aux pieds de la femme,
Ah ! Nous serait tout un programme !



PIERROTS I

I

C’est, sur un cou qui, raide, émerge
D’une fraise empesée idem,
Une face imberbe au cold-cream,
Un air d’hydrocéphale asperge.


Les yeux sont noyés de l’opium
De l’indulgence universelle,
La bouche clownesque ensorcèle
Comme un singulier géranium.


Bouche qui va du trou sans bonde
Glacialement désopilé,
Au transcendantal en-allé
Du souris vain de la Joconde.


Campant leur cône enfariné
Sur le noir serre-tête en soie,
Ils font rire leur patte d’oie
Et froncent en trèfle leur nez.


Ils ont comme chaton de bague
Le scarabée égyptien,
À leur boutonnière fait bien
Le pissenlit des terrains vagues.


Ils vont, se sustentant d’azur,
Et parfois aussi de légumes,
De riz plus blanc que leur costume,
De mandarines et d’œufs durs.


Ils sont de la secte du blême,
Ils n’ont rien à voir avec Dieu,
Et sifflent : « tout est pour le mieux
« Dans la meilleur’des mi-carême ! »



II

Le cœur blanc tatoué
De sentences lunaires,
Ils ont : « faut mourir, frères ! »
Pour mot-d’ordre-évohé.


Quand trépasse une vierge,
Ils suivent son convoi,
Tenant leur cou tout droit
Comme on porte un beau cierge.


Rôle très fatigant,
D’autant qu’ils n’ont personne
Chez eux, qui les frictionne
D’un conjugal onguent.


Ces dandys de la lune
S’imposent, en effet,
De chanter « s’il vous plaît ? »
De la blonde à la brune.


Car c’est des gens blasés ;
Et s’ils vous semblent dupes,
çà et là, de la jupe,
Lange à cicatriser,


Croyez qu’ils font la bête
Afin d’avoir des seins,
Pis-aller de coussins
À leurs savantes têtes.


Écarquillant le cou
Et feignant de comprendre
De travers, la voix tendre,
Mais les yeux si filous !


— D’ailleurs, de mœurs très fines,
Et toujours fort corrects,
(école des cromlechs
Et des tuyaux d’usines).



III

Comme ils vont molester, la nuit,
Au profond des parcs, les statues,
Mais n’offrant qu’aux moins dévêtues
Leur bras et tout ce qui s’ensuit,


En tête à tête avec la femme
Ils ont toujours l’air d’être un tiers,
Confondent demain avec hier,
Et demandent rien avec âme !


Jurent « je t’aime ! » l’air là-bas,
D’une voix sans timbre, en extase,
Et concluent aux plus folles phrases
Par des : « mon dieu, n’insistons pas ? »


Jusqu’à ce qu’ivre, elle s’oublie,
Prise d’on ne sait quel besoin
De lune ? Dans leurs bras, fort loin
Des convenances établies.



IV

Maquillés d’abandon, les manches
En saule, ils leur font des serments,
Pour être vrais trop véhéments !
Puis tumultuent en gigues blanches,


Beuglant : ange ! Tu m’as compris,
À la vie, à la mort ! — et songent :
Ah ! Passer là-dessus l’éponge ! …
Et c’est pas chez eux parti pris,


Hélas ! Mais l’idée de la femme
Se prenant au sérieux encor
Dans ce siècle, voilà, les tord
D’un rire aux déchirantes gammes !


Ne leur jetez pas la pierre, ô
Vous qu’affecte une jarretière !
Allez, ne jetez pas la pierre
Aux blancs parias, aux purs pierrots !



V

Blancs enfants de chœur de la lune,
Et lunologues éminents,
Leur église ouvre à tout venant,
Claire d’ailleurs comme pas une.


Ils disent, d’un œil faisandé,
Les manches très sacerdotales,
Que ce bas monde de scandale
N’est qu’un des mille coups de dé


Du jeu que l’idée et l’amour,
Afin sans doute de connaître
Aussi leur propre raison d’être,
Ont jugé bon de mettre au jour.


Que nul d’ailleurs ne vaut le nôtre,
Qu’il faut pas le traiter d’hôtel
Garni vers un plus immortel,
Car nous sommes faits l’un pour l’autre ;


Qu’enfin, et rien de moins subtil,
Ces gratuites antinomies
Au fond ne nous regardant mie,
L’art de tout est l’ainsi soit-il ;


Et que, chers frères, le beau rôle
Est de vivre de but en blanc
Et, dût-on se battre les flancs,
De hausser à tout les épaules.




PIERROTS II
(on a des principes.)

Elle disait, de son air vain fondamental :
« Je t’aime pour toi seul ! » — oh ! Là, là, grêle histoire ;
Oui, comme l’art ! Du calme, ô salaire illusoire
Du capitaliste l’Idéal !


Elle faisait : « j’attends, me voici, je sais pas… »
Le regard pris de ces larges candeurs des lunes ;
— Oh ! Là, là, ce n’est pas peut-être pour des prunes,
Qu’on a fait ses classes ici-bas ?


Mais voici qu’un beau soir, infortunée à point,
Elle meurt ! — oh ! Là, là ; bon, changement de thème !
On sait que tu dois ressusciter le troisième
Jour, sinon en personne, du moins


Dans l’odeur, les verdures, les eaux des beaux mois !
Et tu iras, levant encor bien plus de dupes
Vers le zaïmph de la Joconde, vers la jupe !
Il se pourra même que j’en sois.



PIERROTS III
(scène courte, mais typique.)

Il me faut, vos yeux ! Dès que je perds leur étoile,
Le mal des calmes plats s’engouffre dans ma voile,
Le frisson du vae soli ! gargouille en mes moelles…


Vous auriez dû me voir après cette querelle !
J’errais dans l’agitation la plus cruelle,
Criant aux murs : mon dieu ! Mon dieu ! Que dira-t-elle ?


Mais aussi, vrai, vous me blessâtes aux antennes
De l’âme, avec les mensonges de votre traîne.
Et votre tas de complications mondaines.


Je voyais que vos yeux me lançaient sur des pistes,
Je songeais : oui, divins, ces yeux ! Mais rien n’existe
Derrière ! Son âme est affaire d’oculiste.


Moi, je suis laminé d’esthétiques loyales !
Je hais les trémolos, les phrases nationales ;
Bref, le violet gros deuil est ma couleur locale.


Je ne suis point « ce gaillard-là ! » ni le superbe !
Mais mon âme, qu’un cri un peu cru exacerbe,
Est au fond distinguée et franche comme une herbe.


J’ai des nerfs encor sensibles au son des cloches,
Et je vais en plein air sans peur et sans reproche,
Sans jamais me sourire en un miroir de poche.


C’est vrai, j’ai bien roulé ! J’ai râlé dans des gîtes
Peu vous ; mais, n’en ai-je pas plus de mérite
À en avoir sauvé la foi en vos yeux ? Dites…


— Allons, faisons la paix, venez, que je vous berce,
Enfant. Eh bien ?
— C’est que, votre pardon me verse
Un mélange (confus) d’impressions… diverses…



LOCUTIONS DES PIERROTS

I

Les mares de vos yeux aux joncs de cils,
Ô vaillante oisive femme,
Quand donc me renverront-ils
La lune-levante de ma belle âme ?


Voilà tantôt une heure qu’en langueur
Mon cœur si simple s’abreuve
De vos vilaines rigueurs,
Avec le regard bon d’un terre-neuve.


Ah ! Madame, ce n’est vraiment pas bien,
Quand on n’est pas la Joconde,
D’en adopter le maintien
Pour induire en spleens tout bleus le pauv’monde.



II

Ah ! Le divin attachement
Que je nourris pour Cydalise,
Maintenant qu’elle échappe aux prises
De mon lunaire entendement !


Vrai, je me ronge en des détresses,
Parmi les fleurs de son terroir
À seule fin de bien savoir
Quelle est sa faculté-maîtresse !


— C’est d’être la mienne, dis-tu ?
Hélas ! Tu sais bien que j’oppose
Un démenti formel aux poses
Qui sentent par trop l’impromptu.



III

Ah ! Sans lune, quelles nuits blanches,
Quels cauchemars pleins de talent !
Vois-je pas là nos cygnes blancs ?
Vient-on pas de tourner la clenche ?


Et c’est vers toi que j’en suis là.
Que ma conscience voit double,
Et que mon cœur pèche en eau trouble,
Ève, Joconde et Dalida !


Ah ! Par l’infini circonflexe
De l’ogive où j’ahanne en croix,
Vends-moi donc une bonne fois
La raison d’être de ton sexe !



IV

Tu dis que mon cœur est à jeun
De quoi jouer tout seul son rôle,
Et que mon regard ne t’enjôle
Qu’avec des infinis d’emprunt !


Et tu rêvais avoir affaire
À quelque pauvre in-octavo…
Hélas ! C’est vrai que mon cerveau
S’est vu, des soirs, trois hémisphères.


Mais va, l’œillet de tes vingt ans,
Je l’arrose aux plus belles âmes
Qui soient ! — surtout, je n’en réclame
Pas, sais-tu, de ta part autant !



V

T’occupe pas, sois ton regard,
Et sois l’âme qui s’exécute ;
Tu fournis la matière brute,
Je me charge de l’œuvre d’art.


Chef-d’œuvre d’art sans idée-mère
Par exemple ! Oh ! Dis, n’est-ce pas,
Faut pas nous mettre sur les bras
Un cri des limbes prolifères ?


Allons, je sais que vous avez
L’égoïsme solide au poste,
Et même prêt aux holocaustes
De l’ordre le plus élevé.



VI

Je te vas dire : moi, quand j’aime,
C’est d’un cœur, au fond sans apprêts,
Mais dignement élaboré
Dans nos plus singuliers problèmes.


Ainsi, pour mes mœurs et mon art,
C’est la période védique
Qui seule a bon droit revendique
Ce que j’en " attelle à ton char " .


Comme c’est notre bible hindoue
Qui, tiens, m’amène à caresser,
Avec ces yeux de cétacé,
Ainsi, et bien sans but, ta joue.



VII

Cœur de profil, petite âme douillette,
Tu veux te tremper un matin en moi,
Comme on trempe, en levant le petit doigt,
Dans son café au lait une mouillette !


Et mon amour, si blanc, si vert, si grand,
Si tournoyant ! Ainsi ne te suggère
Que pas-de-deux, silhouettes légères
À enlever sur ce solide écran !


Adieu. — qu’est-ce encor ? Allons bon, tu pleures !
Aussi pourquoi ces grands airs de vouloir,
Quand mon étoile t’ouvre son peignoir,
D’hélas, chercher midi flambant à d’autres heures !



VIII

Ah ! Tout le long du cœur
Un vieil ennui m’effleure…
M’est avis qu’il est l’heure
De renaître moqueur.


Eh bien ? Je t’ai blessée ?
Ai-je eu le sanglot faux,
Que tu prends cet air sot
De la cruche cassée ?


Tout divague d’amour ;
Tout, du cèdre à l’hysope,
Sirote sa syncope ;
J’ai fait un joli four.



IX

Ton geste,
Houri,
M’a l’air d’un memento mori
Qui signifie au fond : va, reste…


Mais, je te dirai ce que c’est,
Et pourquoi je pars, foi d’honnête
Poète
Français.


Ton cœur a la conscience nette,
Le mien n’est qu’un individu
Perdu
De dettes.



X

Que loin l’âme type
Qui m’a dit adieu
Parce que mes yeux
Manquaient de principes !


Elle, en ce moment,
Elle, si pain tendre,
Oh ! Peut-être engendre
Quelque garnement.


Car on l’a unie,
Avec un monsieur,
Ce qu’il y a de mieux,
Mais pauvre en génie.



XI

Et je me console avec la
Bonne fortune
De l’alme lune.
Ô lune, ave Paris stella !


Tu sais si la femme est cramponne ;
Eh bien, déteins,
Glace sans tain,
Sur mon œil ! Qu’il soit tout atone,


Qu’il déclare : ô folles d’essais,
je vous invite
À prendre vite,
Car c’est à prendre et à laisser.



XII

Encore un livre ; ô nostalgies
Loin de ces très goujates gens,
Loin des saluts et des argents,
Loin de nos phraséologies !


Encore un de mes pierrots mort ;
Mort d’un chronique orphelinisme ;
C’était un cœur plein de dandysme
Lunaire, en un drôle de corps.


Les dieux s’en vont ; plus que des hures ;
Ah ! ça devient tous les jours pis ;
J’ai fait mon temps, je déguerpis
Vers l’inclusive sinécure !



XIII

Eh bien, oui, je l’ai chagrinée,
Tout le long, le long de l’année ;
Mais quoi ! S’en est-elle étonnée ?


Absolus, drapés de layettes,
Aux lunes de miel de l’hymette,
Nous avions par trop l’air vignette !


Ma vitre pleure, adieu ! L’on bâille
Vers les ciels couleur de limaille
Où la lune a ses funérailles.


Je ne veux accuser nul être,
Bien qu’au fond tout m’ait pris en traître.
Ah ! Paître, sans but là-bas ! Paître…



XIV

Les mains dans les poches,
Le long de la route,
j’écoute
Mille cloches
Chantant : « les temps sont proches,
« sans que tu t’en doutes ! »


Ah ! Dieu m’est égal !
Et je suis chez moi !
Mon toit
Très natal
C’est tout. Je marche droit,
je fais pas de mal.


Je connais l’histoire,
Et puis la nature,
Ces foires
Aux ratures ;
Aussi je vous assure
Que l’on peut me croire !



XV

J’entends battre mon sacré-cœur
Dans le crépuscule de l’heure,
Comme il est méconnu, sans sœur,
Et sans destin, et sans demeure !


J’entends battre ma jeune chair
Équivoquant par mes artères,
Entre les Édens de mes vers
Et la province de mes pères.


Et j’entends la flûte de Pan
Qui chante : « bats, bats la campagne !
« Meurs, quand tout vit à tes dépens ;
« Mais entre nous, va, qui perd gagne ! »



XVI

Je ne suis qu’un viveur lunaire
Qui fait des ronds dans les bassins,
Et cela, sans autre dessein
Que devenir un légendaire.


Retroussant d’un air de défi
Mes manches de mandarin pâle,
J’arrondis ma bouche et-j’exhale
Des conseils doux de crucifix.


Ah ! Oui, devenir légendaire,
Au seuil des siècles charlatans !
Mais où sont les lunes d’antan ?
Et que Dieu n’est-il à refaire ?




DIALOGUE 
AVANT LEVER DE LA LUNE

— Je veux bien vivre ; mais vraiment,
L’idéal est trop élastique !


— C’est l’idéal, son nom l’implique,
Hors son non-sens, le verbe ment.


— Mais, tout est conteste ; les livres
S’accouchent, s’entretuent sans lois !


— Certes ! L’absolu perd ses droits,
Là, où le vrai consiste à vivre.


— Et, si j’amène pavillon
Et repasse au néant ma charge ?


— L’infini, qui souffle du large,
Dit : « pas de bêtises, voyons ! »


— Ces chantiers du possible ululent
À l’inconcevable, pourtant !


— Un degré, comme il en est tant
Entre l’aube et le crépuscule.


— Être actuel, est-ce, du moins,
Être adéquat à quelque chose ?


— Conséquemment, comme la rose
Est nécessaire à ses besoins.


— Façon de dire peu commune
Que tout est cercles vicieux ?


— Vicieux, mais tout !
— j’aime mieux
Donc m’en aller selon la lune.



LUNES EN DÉTRESSE

Vous voyez, la lune chevauche
Les nuages noirs à tous crins,
Cependant que le vent embouche
Ses trente-six mille buccins !


Adieu, petits cœurs benjamins
Choyés comme Jésus en crèche,
Qui vous vantiez d’être orphelins
Pour avoir toute la brioche !


Partez dans le vent qui se fâche,
Sous la lune sans lendemains,
Cherchez la pâtée et la niche
Et les douceurs d’un traversin.


Et vous, nuages à tous crins,
Rentrez ces profils de reproche,
C’est les trente-six mille buccins
Du vent qui m’ont rendu tout lâche.


D’autant que je ne suis pas riche,
Et que ses yeux dans leurs écrins
Ont déjà fait de fortes brèches
Dans mon patrimoine enfantin.


Partez, partez, jusqu’au matin !
Ou, si ma misère vous touche,
Eh bien, cachez aux traversins
Vos têtes, naïves autruches,


Éternelles, chères embûches
Où la chimère encor trébuche !



PETITS MYSTÈRES

Chut ! Oh ! Ce soir, comme elle est près !
Vrai, je ne sais ce qu’elle pense,
Me ferait-elle des avances ?
Est-ce là le rayon qui fiance
Nos cœurs humains à son cœur frais ?


Par quels ennuis kilométriques
Mener ma silhouette encor,
Avant de prendre mon essor
Pour arrimer, veuf de tout corps,
À ses dortoirs madréporiques.


Mets de la lune dans ton vin,
M’a dit sa moue cadenassée ;
Je ne bois que de l’eau glacée,
Et de sa seule panacée
Mes tissus qui stagnent ont faim.


Lune, consomme mon baptême,
Lave mes yeux de ton linceul ;
Qu’aux hommes, je sois ton filleul ;
Et pour nos compagnes, le seul
Qui les délivre d’elles-mêmes.


Lune, mise au ban du progrès
Des populaces des étoiles.
Volatilise-moi les moelles,
Que je t’arrive à pleines voiles,
Dolmen, cyprès, amen, au frais !



NUITAMMENT

Ô lune, coule dans mes veines
Et que je me soutienne à peine,


Et croie t’aplatir sur mon cœur !
Mais, elle est pâle à faire peur !


Et montre par son teint, sa mise,
Combien elle en a vu de grises !


Et ramène, se sentant mal,
Son cachemire sidéral,


Errante Delos, nécropole,
Je veux que tu fasses école ;


Je te promets en ex-voto
Les Putiphars de mes manteaux !


Et tiens, adieu ; je rentre en ville
Mettre en train deux ou trois idylles,


En m’annonçant par un péan
D’épithalame à ton néant.



ÉTATS

Ah ! Ce soir, j’ai le cœur mal, le cœur à la lune.
Ô nappes du silence, étalez vos lagunes ;
Ô toits, terrasses, bassins, colliers dénoués
De perles, tombes, lys, chats en peine, louez
La lune, notre maîtresse à tous, dans sa gloire :
Elle est l’hostie ! Et le silence est son ciboire !
Ah ! Qu’il fait bon, oh ! Bel et bon, dans le halo
De deuil de ce diamant de la plus belle eau !
Ô lune, vous allez me trouver romanesque,
Mais voyons, oh ! Seulement de temps en temps est-c’que
Ce serait fol à moi de me dire, entre nous,
Ton Christophe Colomb, ô colombe, à genoux ?
Allons, n’en parlons plus ; et déroulons l’office
Des minuits, confits dans l’alcool de tes délices.
ralentendo vers nous, ô dolente cité,
Cellule en fibroïne aux organes ratés !
Rappelle-toi les centaures, les villes mortes,
Palmyre, et les sphinx camards des Thèbe aux cent portes ;
Et quelle Gomorrhe a sous ton lac de Léthé
Ses catacombes vers la stérile Astarté !
Et combien l’homme, avec ses relatifs « je t’aime  »,
Est trop anthropomorphe au delà de lui-même,
Et ne sait que vivoter comm’ça des bonjours
Aux bonsoirs tout en s’arrangeant avec l’amour.
— Ah ! Je vous disais donc, et cent fois plutôt qu’une
Que j’avais le cœur mal, le cœur bien à la lune.



LA LUNE EST STÉRILE

Lune, pape abortif à l’amiable, pape
Des mormons pour l’art, dans la jalouse Paphos
Où l’état tient gratis les fils de la soupape
D’échappement des apoplectiques cosmos !


C’est toi, léger manuel d’instincts, toi qui circules,
Glaçant, après les grandes averses, les œufs
Obtus de ces myriades d’animalcules
Dont les simouns mettraient nos muqueuses en feu !


Tu ne sais que la fleur des sanglantes chimies ;
Et perces nos rideaux, nous offrant le lotus
Qui constipe les plus larges polygamies,
Tout net, de l’excrément logique des fœtus.


Carguez-lui vos rideaux, citoyens de mœurs lâches ;
C’est l’extase qui paie comptant, donne son ut
Des deux sexes et veut pas même que l’on sache
S’il se peut qu’elle ait, hors de l’art pour l’art, un but.


On allèche de vie humaine, à pleines voiles,
Les Tantales virtuels, peu intéressants
D’ailleurs, sauf leurs cordiaux, qui rêvent dans nos moelles.
Et c’est un produit net qu’encaissent nos bons sens.


Et puis, l’atteindrons-nous, l’oasis aux citernes,
Où nos cœurs toucheraient les payes qu’on leur doit ?
Non, c’est la rosse aveugle aux cercles sempiternes
Qui tourne pour autrui les bons chevaux de bois.


Ne vous distrayez pas, avec vos grosses douanes ;
Clefs de fa, clefs de sol, huit stades de claviers,
Laissez faire, laissez passer la caravane
Qui porte à l’idéal ses plus riches dossiers !


L’art est tout, du droit divin de l’inconscience ;
Après lui, le déluge ! Et son moindre regard
Est le cercle infini dont la circonférence
Est partout, et le centre immoral nulle part.


Pour moi, déboulonné du pôle de stylite
Qui me sied, dès qu’un corps a trop de son secret,
J’affiche : celles qui voient tout, je les invite
À venir, à mon bras, des soirs, prendre le frais.


Or voici : nos deux cris, abaissant leurs visières,
Passent mutuellement, après quiproquos,
Aux chers peignes du cru leurs moelles épinières
D’où lèvent débusqués tous les archets locaux.


Et les ciels familiers liserés de folie
Neigeant en charpie éblouissante, faut voir
Comme le moindre appel : c’est pour nous seuls ! Rallie
Les louables efforts menés à l’abattoir !


Et la santé en deuil ronronne ses vertiges,
Et chante, pour la forme : « hélas ! Ce n’est pas bien,
« Par ces pays, pays si tournoyants, vous dis-je,
« Où la faim d’infini justifie les moyens. »


Lors, qu’ils sont beaux les flancs tirant leurs révérences
Au sanglant capitaliste berné des nuits,
En s’affalant cuver ces jeux sans conséquence !
Oh ! N’avoir à songer qu’à ses propres ennuis !


— Bons aïeux qui geigniez semaine sur semaine,
Vers mon cœur, baobab des védiques terroirs,
Je m’agite aussi ! Mais l’inconscient me mène ;
Or, il sait ce qu’il fait, je n’ai rien à y voir.



STÉRILITÉS

Cautérise et coagule
En virgules
Ses lagunes des cerises
Des félines Ophélies
Orphelines en folie.


Tarentule de feintises
La remise
Sans rancune des ovules
Aux félines Ophélies
Orphelines en folie.


Sourd aux brises des scrupules,
Vers la bulle
De la lune, adieu, nolise
Ces félines Ophélies
Orphelines en folie !



LES LINGES, LE CYGNE

Ce sont les linges, les linges,
Hôpitaux consacrés aux cruors et aux fanges ;
Ce sont les langes, les langes,
Où l’on voudrait, ah ! Redorloter ses méninges !


Vos linges pollués, Noëls de Bethléem !
De la lessive des linceuls des requiems
De nos touchantes personnalités, aux langes
Des berceaux, vite à bas, sans doubles de rechange,
Qui nous suivent, transfigurés (fatals vauriens
Que nous sommes) ainsi que des langes gardiens.
C’est la guimpe qui dit, même aux trois quarts meurtrie :
« Ah ! Pas de ces familiarités, je vous prie… »
C’est la peine avalée aux édredons d’eider ;
C’est le mouchoir laissé, parlant d’âme et de chair
Et de scènes ! (je vous pris la main sous la table,
J’eus même des accents vraiment inimitables),
Mais ces malentendus ! L’adieu noir ! — je m’en vais !
— Il fait nuit ! — que m’importe ! à moi, chemins mauvais !
Puis, comme Phèdre en ses illicites malaises :
« Ah ! Que ces draps d’un lit d’occasion me pèsent ! »
Linges adolescents, nuptiaux, maternels ;
Nappe qui drape la sainte-table ou l’autel,
Purificatoire au calice, manuterges,
Refuges des baisers convolant vers les cierges.
Ô langes invalides, linges aveuglants !
Oreillers du bon cœur toujours convalescent
Qui dit, même à la sœur, dont le toucher l’écœure :
« Rien qu’une cuillerée, ah ! Toutes les deux heures… »
Voie lactée à charpie en surplis : lourds jupons
À plis d’ordre dorique à lesquels nous rampons
Rien que pour y râler, doux comme la tortue
Qui grignote au soleil une vieille laitue.
Linges des grandes maladies ; champs-clos des draps
Fleurant : soulagez-vous, va, tant que ça ira !
Et les cols rabattus des jeunes filles fières,
Les bas blancs bien tirés, les chants des Lavandières,
Le peignoir sur la chair de poule après le bain,
Les cornettes des sœurs, les voiles, les béguins,
La province et ses armoires, les lingeries
Du lycée et du cloître ; et les bonnes prairies
Blanches des traversins rafraîchissant leurs creux
De parfums de famille aux tempes sans aveux,
Et la mort ! Pavoisez les balcons de draps pâles,
Les cloches ! Car voici que des rideaux s’exhale
La procession du beau cygne ambassadeur
Qui mène Lohengrin au pays des candeurs !


Ce sont les linges, les linges,
Hôpitaux consacrés aux cruors et aux fanges !
Ce sont les langes, les langes,
Où l’on voudrait, ah ! Redorloter ses méninges.



NOBLES ET TOUCHANTES DIVAGATIONS 
SOUS LA LUNE

Un chien perdu grelotte en abois à la lune…
Oh ! Pourquoi ce sanglot quand nul ne l’a battu ?
Et, nuits ! Que partout la même âme ! En est-il une
Qui n’aboie à l’exil ainsi qu’un chien perdu ?


Non, non ; pas un caillou qui ne rêve un ménage,
Pas un soir qui ne pleure : encore un aujourd’hui !
Pas un moi qui n’écume aux barreaux de sa cage
Et n’épluche ses jours en filaments d’ennui.


Et les bons végétaux ! Des fossiles qui gisent
En pliocènes tufs de squelettes parias,
Aux printemps aspergés par les steppes kirghyses,
Aux roses des contreforts de l’Himalaya !


Et le vent qui beugle, apocalyptique bête
S’abattant sur des toits aux habitants pourris,
Qui secoue en vain leur huis-clos, et puis s’arrête,
Pleurant sur son cœur à sept-glaives d’incompris.


Tout vient d’un seul impératif catégorique,
Mais qu’il a le bras long, et la matrice loin !
L’amour, l’amour qui rêve, ascétise et fornique ;
Que n’aimons-nous pour nous dans notre petit coin ?


Infini, d’où sors-tu ? Pourquoi nos sens superbes
Sont-ils fous d’au delà les claviers octroyés,
Croient-ils à des miroirs plus heureux que le verbe,
Et se tuent ? Infini, montre un peu tes papiers !


Motifs décoratifs, et non but de l’histoire,
Non le bonheur pour tous, mais de coquets moyens
S’objectivant en nous, substratums sans pourboires,
Trinité de Molochs, le vrai, le beau, le bien.


Nuages à profils de kaïns ? Vents d’automne
Qui, dans l’antiquité des Pans soi-disant gais,
Vous lamentiez aux toits des temples heptagones,
Voyez, nous rebrodons les mêmes anankès.


Jadis les gants violets des révérendissimes
De la théologie en conciles cités,
Et l’évêque d’Hippone attelant ses victimes
Au char du Jaggernaut œcuménicité ;


Aujourd’hui, microscope de télescope ! Encore,
Nous voilà relançant l’ogive au toujours lui,
Qu’il y tourne casaque, à neuf qu’il s’y redore
Pour venir nous bercer un printemps notre ennui.


Une place plus fraîche à l’oreiller des fièvres,
Un mirage inédit au détour du chemin,
Des rampements plus fous vers le bonheur des lèvres,
Et des opiums plus longs à rêver. Mais demain ?


Recommencer encore ? Ah ! Lâchons les écluses,
À la fin ! Oublions tout ! Nous faut convoyer
Vers ces ciels où, s’aimer et paître étant les muses,
Cuver sera le dieu pénate des foyers !


Oh ! L’Éden immédiat des braves empirismes !
Peigner ses fiers cheveux avec l’arête des
Poissons qu’on lui offrit crus dans un paroxysme
De dévouement ! S’aimer sans serments, ni rabais.


Oui, vivre pur d’habitudes et de programmes,
Pacageant mes milieux, à travers et à tort,
Choyant comme un beau chat ma chère petite âme,
N’arriver qu’ivre-mort de moi-même à la mort !


Oui, par delà nos arts, par delà nos époques
Et nos hérédités, tes îles de candeur,
Inconscience ! Et elle, au seuil, là, qui se moque
De mes regards en arrière, et fait : n’aie pas peur.


Que non, je n’ai plus peur ; je rechois en enfance ;
Mon bateau de fleurs est prêt, j’y veux rêver à
L’ombre de tes maternelles protubérances,
En t’offrant le miroir de mes et cætera…



JEUX

Ah ! La lune, la lune m’obsède…
Croyez-vous qu’il y ait un remède ?


Morte ? Se peut-il pas qu’elle dorme
Grise de cosmiques chloroformes ?


Rosace en tombale efflorescence
De la basilique du silence.


Tu persistes dans ton attitude,
Quand je suffoque de solitude !


Oui, oui, tu as la gorge bien faite ;
Mais, si jamais je ne m’y allaite ? …


Encore un soir, et mes berquinades
S’en iront rire à la débandade,


Traitant mon platonisme si digne
D’extase de pêcheur à la ligne !


Salve, regina des lys ! reine,
Je te veux percer de mes phalènes !


Je veux baiser ta patène triste,
Plat veuf du chef de saint Jean-Baptiste !


Je veux trouver un lied ! qui te touche
À te faire émigrer vers ma bouche !


— Mais, même plus de rimes à lune…
Ah ! Quelle regrettable lacune !



LITANIES 
DES DERNIERS QUARTIERS 
DE LUNE

Eucharistie
De l’Arcadie,


Qui fais de l’œil
Aux cœurs en deuil,


Ciel des idylles
Qu’on veut stériles,


Fonts baptismaux
Des blancs pierrots.


Dernier ciboire
De notre histoire,


Vortex-nombril
Du tout-nihil,


Miroir et bible
Des impassibles,


Hôtel garni
De l’infini,


Sphinx et Joconde
Des défunts mondes,


Ô Chanaan
Du bon néant,


Néant, la Mecque
Des bibliothèques,


Léthé, Lotos,
Exaudi nos !



AVIS, JE VOUS PRIE

Hélas ! Des lunes, des lunes,
Sur un petit air en bonne fortune…
Hélas ! De choses en choses
Sur la criarde corde des virtuoses ! …


Hélas ! Agacer d’un lys
La violette d’Isis ! …
Hélas ! M’esquinter, sans trêve, encore,
Mon encéphale anomaliflore
En floraison de chair par guirlandes d’ennuis !
Ô mort, et puis ?


Mais ! J’ai peur de la vie
Comme d’un mariage !
Oh ! Vrai, je n’ai pas l’âge
Pour ce beau mariage ! …


Oh ! J’ai été frappé de cette vie à moi,
L’autre dimanche, m’en allant par une plaine !
Oh ! Laissez-moi seulement reprendre haleine,
Et vous aurez un livre enfin de bonne foi.


En attendant, ayez pitié de ma misère !
Que je vous sois à tous un être bienvenu !
Et que je sois absous pour mon âme sincère,
Comme le fut Phryné pour son sincère nu.



