<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS IMPOSSIBLES</title>
				<title type="medium">Édition électronique</title>
				<author key="PDE">
					<name>
						<forename>Joseph</forename>
						<surname>POISLE-DESGRANGES</surname>
					</name>
					<date from="1823" to="1879">1823-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>192 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PDE_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les sonnets impossibles</title>
						<author>JOSEPH POISLE-DESGRANGES</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96672083.r=JOSEPH%20POISLE-DESGRANGES?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les sonnets impossibles</title>
								<author>JOSEPH POISLE-DESGRANGES</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Bachelin-Deflorenne</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<front>
			<p>
				Vita brevis.

				COURTE PRÉFACE

				Si l’enfant des Bords de la Bièvre, si l’auteur du livre intitulé les Sonneurs de Sonnets,
				livre qui, par parenthèse. est aujourd’hui d’un prix fort élevé à cause de sa rareté,
				si l’humouriste Alfred Delvau vivait encore, que dirait-il de nos Sonnets impossibles ?
				lui qui s’est fait l’aristarque des sonnettistes anciens et modernes. et qui n’a eu
				réellement d’admiration que pour le vieux Ronsard.
				– Ça ! des sonnets ! s’écrierait-il. Fi ! ce
				sont des monosyllabes rimés. Est-ce à la suite d’un défi ?
				– Peut-être. Bons ou mauvais, les voilà ; nous vous les offrons en dépit de tous les sonnettistes
				du monde. Les lise qui voudra, les imite qui pourra.
				Comme ils étaient tout nus. les pauvres petits maigre-échine, M. Alfred Taiée. aquafortiste,
				a bien voulu prendre la peine de les habiller.
				Et sa sollicitude mérite bien un remerciement sincère.

				J.P.D. 
			</p>
		</front>
		<body>
		<div type="poem" key="PDE11">
			<head type="main">A LA CRITIQUE</head>
			<lg n="1">
				<l n="1" num="1.1">Qu’a-</l>
				<l n="2" num="1.2">T-elle</l>
				<l n="3" num="1.3">Celle-</l>
				<l n="4" num="1.4">Là ?</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Bah !</l>
				<l n="6" num="2.2">Telle</l>
				<l n="7" num="2.3">Quelle</l>
				<l n="8" num="2.4">Va,</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Muse</l>
				<l n="10" num="3.2">Sans</l>
				<l n="11" num="3.3">Gants,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Use</l>
				<l n="13" num="4.2">Ses</l>
				<l n="14" num="4.3">Trait.</l>
			</lg>
		</div>
		<div type="poem" key="PDE12">
			<head type="main">IMAGE</head>
			<lg n="1">
				<l n="1" num="1.1">L’onde</l>
				<l n="2" num="1.2">Suit,</l>
				<l n="3" num="1.3">Fuit,</l>
				<l n="4" num="1.4">Gronde.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Monde</l>
				<l n="6" num="2.2">Luit,</l>
				<l n="7" num="2.3">Nuit,</l>
				<l n="8" num="2.4">Fronde.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Sort</l>
				<l n="10" num="3.2">Mord</l>
				<l n="11" num="3.3">L’homme,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Toi</l>
				<l n="13" num="4.2">Comme</l>
				<l n="14" num="4.3">Moi.</l>
			</lg>
		</div>
		<div type="poem" key="PDE13">
			<head type="main">AU PRINTEMPS</head>
			<lg n="1">
				<l n="1" num="1.1">Gai</l>
				<l n="2" num="1.2">Lai</l>
				<l n="3" num="1.3">Fève</l>
				<l n="4" num="1.4">Lève</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">J’ai</l>
				<l n="6" num="2.2">Mai,</l>
				<l n="7" num="2.3">Rêve</l>
				<l n="8" num="2.4">D’Ève,</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Fleur,</l>
				<l n="10" num="3.2">Coeur,</l>
				<l n="11" num="3.3">Ame,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Jeux,</l>
				<l n="13" num="4.2">Feux,</l>
				<l n="14" num="4.3">Flamme.</l>
			</lg>
		</div>
		<div type="poem" key="PDE14">
			<head type="main">LA NEIGE</head>
			<lg n="1">
				<l n="1" num="1.1">Piège</l>
				<l n="2" num="1.2">Mou</l>
				<l n="3" num="1.3">Ou</l>
				<l n="4" num="1.4">Neige,</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Sais-je,</l>
				<l n="6" num="2.2">Fou,</l>
				<l n="7" num="2.3">Où,</l>
				<l n="8" num="2.4">Vais-je ?</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Là</l>
				<l n="10" num="3.2">Ma</l>
				<l n="11" num="3.3">Tombe,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Si</l>
				<l n="13" num="4.2">J’y</l>
				<l n="14" num="4.3">Tombe.</l>
			</lg>
		</div>
		<div type="poem" key="PDE15">
			<head type="main">PAYSAGE</head>
			<lg n="1">
				<l n="1" num="1.1">L’eau</l>
				<l n="2" num="1.2">Mène</l>
				<l n="3" num="1.3">Au</l>
				<l n="4" num="1.4">Maine.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Beau</l>
				<l n="6" num="2.2">Chêne</l>
				<l n="7" num="2.3">Haut.</l>
				<l n="8" num="2.4">Plaine.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Deux</l>
				<l n="10" num="3.2">Boeufs</l>
				<l n="11" num="3.3">Mornes,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Grands</l>
				<l n="13" num="4.2">Sans</l>
				<l n="14" num="4.3">Cornes.</l>
			</lg>
		</div>
		<div type="poem" key="PDE16">
			<head type="main">A LA GUERRE</head>
			<lg n="1">
				<l n="1" num="1.1">Guerre,</l>
				<l n="2" num="1.2">Sois</l>
				<l n="3" num="1.3">Fière !</l>
				<l n="4" num="1.4">Vois :</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Terre,</l>
				<l n="6" num="2.2">Bois,</l>
				<l n="7" num="2.3">Pierre,</l>
				<l n="8" num="2.4">Croix,</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Glaives,</l>
				<l n="10" num="3.2">Rêves</l>
				<l n="11" num="3.3">Morts…</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Peste</l>
				<l n="13" num="4.2">Reste :</l>
				<l n="14" num="4.3">Sors !</l>
			</lg>
		</div>
		<div type="poem" key="PDE17">
			<head type="main">PEINTURE</head>
			<lg n="1">
				<l n="1" num="1.1">Gouache,</l>
				<l n="2" num="1.2">Son</l>
				<l n="3" num="1.3">Ton</l>
				<l n="4" num="1.4">Lâche.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Flache.</l>
				<l n="6" num="2.2">Fond</l>
				<l n="7" num="2.3">Blond.</l>
				<l n="8" num="2.4">Vache.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Sol</l>
				<l n="10" num="3.2">Fol</l>
				<l n="11" num="3.3">D’ombre.</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Long</l>
				<l n="13" num="4.2">Pont</l>
				<l n="14" num="4.3">Sombre.</l>
			</lg>
		</div>
		<div type="poem" key="PDE18">
			<head type="main">MYOSOTIS</head>
			<head type="sub">BÉATRIX ET DANTE</head>
			<lg n="1">
				<l n="1" num="1.1">– Ah !</l>
				<l n="2" num="1.2">Dante,</l>
				<l n="3" num="1.3">La</l>
				<l n="4" num="1.4">Sente</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">A</l>
				<l n="6" num="2.2">Là</l>
				<l n="7" num="2.3">Gente</l>
				<l n="8" num="2.4">Plante :</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">– C’est</l>
				<l n="10" num="3.2">Qu’elle</l>
				<l n="11" num="3.3">Sait,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Belle,</l>
				<l n="13" num="4.2">Ton</l>
				<l n="14" num="4.3">Nom.</l>
			</lg>
		</div>
		<div type="poem" key="PDE19">
			<head type="main">AU VIEUX PÊCHEUR</head>
			<lg n="1">
				<l n="1" num="1.1">Pêche</l>
				<l n="2" num="1.2">A</l>
				<l n="3" num="1.3">La</l>
				<l n="4" num="1.4">Fraîche.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Sèche</l>
				<l n="6" num="2.2">Là</l>
				<l n="7" num="2.3">Ta</l>
				<l n="8" num="2.4">Dèche,</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Mon</l>
				<l n="10" num="3.2">Bon,</l>
				<l n="11" num="3.3">Sauve</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Ton</l>
				<l n="13" num="4.2">Front</l>
				<l n="14" num="4.3">Chauve.</l>
			</lg>
		</div>
		<div type="poem" key="PDE20">
			<head type="main">LA LUNE</head>
			<lg n="1">
				<l n="1" num="1.1">Qu’une</l>
				<l n="2" num="1.2">Lune :</l>
				<l n="3" num="1.3">Deux</l>
				<l n="4" num="1.4">Yeux.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Dune</l>
				<l n="6" num="2.2">Brune,</l>
				<l n="7" num="2.3">Cieux</l>
				<l n="8" num="2.4">Bleus.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Belle,</l>
				<l n="10" num="3.2">Elle</l>
				<l n="11" num="3.3">Sort !</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">L’onde</l>
				<l n="13" num="4.2">Blonde</l>
				<l n="14" num="4.3">Dort.</l>
			</lg>
		</div>
		<div type="poem" key="PDE21">
			<head type="main">LA SCIE</head>
			<lg n="1">
				<l n="1" num="1.1">Plie,</l>
				<l n="2" num="1.2">Ma</l>
				<l n="3" num="1.3">Scie,</l>
				<l n="4" num="1.4">Va…</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Crie !</l>
				<l n="6" num="2.2">Ma</l>
				<l n="7" num="2.3">Scie,</l>
				<l n="8" num="2.4">A</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Lame,</l>
				<l n="10" num="3.2">Trame,</l>
				<l n="11" num="3.3">Pleurs,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Comme</l>
				<l n="13" num="4.2">L’homme</l>
				<l n="14" num="4.3">Meurs !</l>
			</lg>
		</div>
		<div type="poem" key="PDE22">
			<head type="main">A MA MUSE</head>
			<lg n="1">
				<l n="1" num="1.1">Ta</l>
				<l n="2" num="1.2">Chaîne,</l>
				<l n="3" num="1.3">Ma</l>
				<l n="4" num="1.4">Reine,</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1">Jà</l>
				<l n="6" num="2.2">Mène</l>
				<l n="7" num="2.3">La</l>
				<l n="8" num="2.4">Gêne.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1">Je</l>
				<l n="10" num="3.2">Te</l>
				<l n="11" num="3.3">Laisse,</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1">Ton</l>
				<l n="13" num="4.2">Ton</l>
				<l n="14" num="4.3">Baisse.</l>
			</lg>
		</div>
		<div type="poem" key="PDE23">
			<head type="main">ENVOI</head>
			<head type="sub">A CHARLES MONSELET</head>
			<head type="form">TRIOLETS</head>
			<lg n="1">
				<l n="1" num="1.1">Monselet, ce gai pèlerin,</l>
				<l n="2" num="1.2">A de l’esprit fin dans sa gourde ;</l>
				<l n="3" num="1.3">Il en aura jusqu’à la fin,</l>
				<l n="4" num="1.4">Monselet, ce gai pèlerin</l>
				<l n="5" num="1.5">Qui rime le long du chemin,</l>
				<l n="6" num="1.6">Triola René Pincebourde.</l>
				<l n="7" num="1.7">Monselet, ce gai pèlerin,</l>
				<l n="8" num="1.8">A de l’esprit fin dans sa gourde.</l>
			</lg>
			<lg n="2">
				<l n="9" num="2.1">Pour bien tourner un triolet,</l>
				<l n="10" num="2.2">Lui donner le parfum des roses,</l>
				<l n="11" num="2.3">Je ne connais que Monselet,</l>
				<l n="12" num="2.4">Pour bien tourner un triolet</l>
				<l n="13" num="2.5">Et savoir trouver ce qui plaît</l>
				<l n="14" num="2.6">Parmi les fleurs fraîches écloses ;</l>
				<l n="15" num="2.7">Pour bien tourner un triolet,</l>
				<l n="16" num="2.8">Lui donner le parfum des roses.</l>
			</lg>
			<lg n="3">
				<l n="17" num="3.1">S’il veut agréer mes sonnets,</l>
				<l n="18" num="3.2">Ce sont des Sonnets impossibles,</l>
				<l n="19" num="3.3">Je relirai ses triolets.</l>
				<l n="20" num="3.4">S’il veut agréer mes sonnets,</l>
				<l n="21" num="3.5">Sans couleur vive et sans reflets,</l>
				<l n="22" num="3.6">Et qui sont nés non éligibles.</l>
				<l n="23" num="3.7">S’il veut agréer mes sonnets,</l>
				<l n="24" num="3.8">Ce sont des Sonnets impossibles.</l>
			</lg>
		</div>
		</body>
	</text>
</TEI>