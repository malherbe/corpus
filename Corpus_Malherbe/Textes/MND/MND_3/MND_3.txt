Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
MND
MND_3

Louis MÉNARD
1822-1901

Autres poèmes
1895
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

Poëmes
Louis Ménard


http://www.poesies.net/louismenardpoemes.txt
L’édition électronique ne mentionne pas l’édition imprimée d’origine.
_________________________________________________________________
Édition de référence pour les corrections métriques :

Poèmes et Rèveries d’un Paien mystiqe
Louis Ménard

Librairie de "l’ART NDÉPENDANT"
1995


┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



Vers d’Album

À Théodore de Banville


Je déclare, soutiens, certifie et proclame
Envers et contre tous, maintenant et toujours,
Qu’il n’est pas d’être au monde heureux comme la femme.
Pour elle nos respects, nos égards, nos amours,
On la fête, on lui fait des jours d’or et de soie,
On la flatte, on l’encense, on l’adule, on la choie,


On lui parle à genoux, on lui cède le pas.
Que Monsieur se démène à la Bourse, à la Chambre,
Madame peut chanter, dormir, croiser les bras
Ou lire des romans de janvier à décembre.
Elle parle ? On écoute, on admire, on sourit,
Ses moindres mots sont pleins de finesse et d’esprit.


Autour d’elle, des flots d’adorateurs mystiques
Se pressent chaque jour, elle n’a qu’à choisir.
Et pour récompenser leurs ardeurs extatiques,
Ils ne demandent rien que l’idéal plaisir,
Ah grand Dieu, de baiser un gant de sa main blanche
Ou d’étreindre en valsant son frêle corps qui penche.


Elle résiste aux uns, on vante sa vertu.
Mais pour peu qu’elle cède à son premier caprice,
Ah ! sans doute elle avait bien longtemps combattu,
Pauvre âme ! Mais aussi, pour un tel sacrifice,
Que n’a-t-elle pas droit de réclamer de nous ?
Et notre temps perdu, morbleu, l’oubliez-vous ?


Un mot met à ses pieds l’homme choisi par elle ;
Jamais ni cruautés, ni délais, ni rigueurs.
Mais jusqu’au dernier jour il doit rester fidèle.
S’il desserre sa chaîne, elle s’écrie en pleurs
Que c’était bien assez de son mari, qui brise
Chaque jour à plaisir sa pauvre âme incomprise.


O vous qui préparez, législateurs pieux,
L’émancipation complète de la femme,
Soyez bénis ! Hâtez, s’il se peut, par vos vœux,
L’égalité, que nul plus que moi ne réclame,
Car il me tarde d’être inhumain, à mon tour,
Et d’avoir des rigueurs, si l’on méfait la cour.


Mais quand vous lui direz d’accepter, en échange
De son inviolable et douce royauté
Les charges du travail et de l’égalité,
Et d’être un homme, au lieu d’une fleur ou d’un ange,
Alors, réformateurs vertueux et profonds,
Qui vous dit que la femme acceptera vos dons ?



Double Rêve

J’ai cru qu’on m’enfermait au couvent : c’est un rêve !
Je suis morte, il est mort aussi : je bénis Dieu !
Là-bas, sur sa tombe une ombre se lève :
Viens, mon bien-aimé, viens me dire adieu.


— J’ai cru qu’on m’enchaînait dans la tour, sur la pierre,
Seul, loin d’elle et du jour ; mais non, ce cachot noir,
C’était mon tombeau dans le cimetière.
Que Dieu soit béni, je vais la revoir !


— C’est toi ! Je savais bien que tu m’aurais suivie,
Tu me l’avais promis. Cette félicité
Q’on nous refusait pendant notre vie,
La mort nous la rend pour l’éternité.


— Je rêvais de prison, et toi de monastère :
Un baiser ! oublions et mon rêve et le tien.
Dieu, qui sépara nos cœurs sur la terre,
Les unit au ciel : je le savais bien !


— Écoute ! un son de cloche a retenti : c’est l’heure
Du dernier jugement pour tous les trépassés ;
Faut-il nous quitter si tôt ? — Non, demeure :
Qu’importe le ciel ? restons embrassés !


La cloche du matin sonne pour la prière ;
A travers les barreaux glisse un rayon du jour,
Tous deux à lajois ouvrent leur paupière,
Elle en sa cellule, et lui dans la tour.



Gloria victis !


juin 1848



Puisque vos ennemis couronnent d’immortelle
Le cercueil triomphal où reposent leurs morts,
Pendant que, sans honneurs, entassés pêle-mêle,
Dans la fosse commune on va jeter vos corps ;


Recevez le tribut de nos larmes muettes,
Frères, nous suivrons seuls vos restes vénérés,
Et nous visiterons, pendant les nuits discrètes,
Le coin du cimetière où vous reposerez.


Mais non : derrière vous nous marcherons sans larmes,
Car vous êtes tombés pendant les saints combats,
L’espérance dans l’âme et la main sur vos armes ;
Nous qui vous survivons, nous ne vous pleurons pas.


O frères, lorsqu’il faut que la Liberté meure,
Heureux ceux qui la vont retrouver dans la mort !
La part qui vous est faite, hélas ! est la meilleure,
Et c’est à vous, sans doute, a pleurer notre sort.


Martyrs, dormez en paix : votre cause était sainte !
Et vos noms blasphémés, qu’on veut enfin ternir,
Après les jours de haine affronteront sans crainte
Le calme jugement d’un plus juste avenir.


Vous avez supporté, depuis votre victoire,
Bien des nuits d’agonie et bien des mornes jours,
Confiants, résignés, et ne voulant pas croire
Que vos élus aussi vous trahiraient toujours.


Chacun de vous trouvait, en rentrant dans son bouge,
Pour hôtes obstinés la misère et la faim
Jusqu’au jour où l’on vit flotter le drapeau rouge
Où vous aviez écrit : « Du travail et du pain ! »


Mais vos maîtres, devant les saintes barricades,
Au testament sinistre inscrit sur vos drapeaux,
Répondaient, à travers les longues fusillades :
« L’ordre de Varsovie et la paix des tombeaux. »


Et vous tombiez, les uns sur le pavé des rues,
Sous le fer et le plomb, moins cruels que la faim,
Les autres, désarmés, le long des avenues,
Sur le sable sanglant de l’abattoir humain.


Ah ! du moins, vous n’avez pas vu sous la mitraille
Vos femmes et vos sœurs s’élancer pour mourir ;
Aux yeux fermés pendant la dernière bataille.
La bienfaisante mort dérobe l’avenir.


O plus heureux que nous ! vous ne pouvez entendre
La calomnie hurlant autour de vos tombeaux,
Sans qu’il se lève un seul ami pour vous défendre
Et rejeter l’injure au front de vos bourreaux.


Vous quittez avant nous une terre maudite
Où Dieu même est toujours du parti du plus fort,
Où le pauvre est esclave, où sa race est proscrite,
Où la faim n’eût jamais qu’un remède, la mort.


Lorsque vous nous tendiez, au plus fondes batailles,
Votre arme vengeresse échappée à vos bras,
Nous vous avions promis de justes représailles,
Et nos bras enchaînés ne vous vengeront pas.


Vous ignoriez le sort qu’ils gardaient à vos frères,
L’ivresse des vainqueurs, leurs rires insultants,
Et la sanglante orgie, et les froides colères ;
Frères, dormez en paix : vous êtes morts à temps.



Adrastée

Si l’aveugle hasard me donnait la puissance
Pour un jour, je voudrais tenir
Le glaive justicier de la sainte vengeance
Et le droit sacré de punir.


J’irais sur le cadavre épeler les tortures :
Au jour de l’expiation
Œil pour œil, dent pour dent, blessure pour blessure
L’antique loi du talion.


Et je voudrais aussi, secouant la poussière
Des siècles dans l’oubli plongés
Évoquer leur douleur muette et satisfaire
Tous les morts qu’on n’a pas vengés,


Car l’expiation est chose grande et sainte
Et corne un reproche éternel,
Les douleurs sans vengeance élèvent une plainte
Qui monte de la terre au ciel.


Et de peur qu’il fût dit que cette loi suprême
Put être oubliée une fois,
Pour absoudre le ciel, l’homme a cru que Dieu même
Dût s’immoler sur une croix.


La revanche viendra : le Jour inévitable
Des Justes expiations
Luira pour balayer une race coupable
Au vent des révolutions ;


Alors on nous dira : « La vengeance est impie,
Il faut pardonner, non punir ».
Et quand le sang versé veut du sang qui l’expie
On parlera de repentir.


Pas de grâce. Pensons à la mort de nos frères,
A tant de maux inexpiés,
Et que leur souvenir en profondes colères
Transforme les lâches pitiés ;


Pensons aux jours de sang, de pillage et de ruine,
Ou dans nos faubourg bombardés
Le canon répondait aux cris de la famine,
A nos murs de sang inondés


Le viol impur souillait les vierges sur les places,
Les morts s’entassaient par milliers
Et quand les massacreurs, dont les mains étaient lasses,
Eurent tué trois Jours entiers,


Vous couronniez leurs fronts et vos femmes si fières
Bâtaient des mains, et croyant voir
Ces cosaques maudits, chers jadis à leurs mères,
Agitaient vers eux le mouchoir.


Et puis le lendemain de la victoire impie
L’insulte et la délation,
Après l’assassinat, la lâche calomnie,
L’implacable proscription.


Corne ils ont bien d’avance absous nos représailles
Quand nos bras seront déchaînés,
Pensons aux morts : il faut de grandes funérailles
A nos frères assassinés.


Ce sera votre tour, pas de pardon, nos maîtres,
Nos représentants, nos élus,
Vil troupeau d’assassins, de lâches et de traîtres
A genoux, malheur aux vaincus !


Le jour de la justice est venu : pas de grâce,
Ni prières, ni repentirs
Ne vous empêcheront de baiser chaque place
Où coula le sang des martyrs.


Toi, l’aveugle instrument de leur froide colère,
Vis, d’exécration chargé.
Pourvu qu’à ton chevet le spectre de ton frère
Se lève, le peuple est vengé.


Vous serfs de tout pouvoir, automates stupides,
Bourreaux au meurtre condamnés
Qui tournez sans remords vos armes parricides
Contre vos frères enchaînés,


Et vous vils trafiquants, race basse et rampante.
Qui dans ces jours maudits alliez
Soûlant d’or et de vin la horde rugissante
Des égorgeurs stipendiés,


Loin d’ici ! vous souillez l’air pur de la patrie.
Déjà terrible et menaçant.
Le peuple est là qui veille : oh fuyez, qu’il oublie
Que le sang seul lave le sang.



Ïambes
(Variantes D’Adrastée, Neue rheinische Zeitung de 1850)

Quand le jour espéré, le jour inévitable
Des justes expiations
Viendra pour balayer une race coupable
Au vent des révolutions ;


Alors, tous les pleureurs qui parlent de clémence.
Ceux à qui le bourreau fait peur,
Ceux pour qui la justice est colère et vengeance,
Le crime faiblesse et malheur.


Reviendront nous crier que la peine est impie,
qu’il faut pardonner, non punir.
Et, quand le sang versé veut du sang qui l’expie,
on parlera de repentir.


Déesse qu’invoquaient les siècles forts et rudes,
Par qui tout meurtre était vengé,
O Sainte Némésis, vois nos décrépitudes,
Ton glaive en férule est changé.


Philosophes profonds, déclamateurs sublimes
Qui jetez un regard d’amour
Sur l’assassin maudit, que le sang des victimes
Sur vous retombe aux derniers jours.


Pas de grâce, pensons à la mort de nos frères,
A tant de maux inexpiés,
Et que leur souvenir en profondes colères
Transforme les lâches pitiés


Pensons aux jours de sang, de pillage et de ruine
Où dans nos faubourgs bombardés
Le canon répondait aux cris de la famine
A nos murs de sang inondés.


Au viol impur, souillant la vierge à l’agonie
Qui lutte et maudit sa beauté.
Quand sur les corps sanglants aux râlements unie
Hurle l’immonde volupté ;


Aux vaincus désarmés dont la foule sanglante
Sous le feu se crispe et se tord ;
Le sang ruisselle a flots sur la chair pantelante.
Cris de meurtre et plainte de mort !


Aux applaudissements des femmes, sur les places.
Les corps tombèrent par milliers.
Et quand les massacreurs dont les mains étaient lasses
Eurent tué trois jours entiers.


Vous couronniez leur fronts, et vos femmes si fières
Battaient des mains et croyant voir
Ces cosaques sanglants, chers jadis à leurs mères
Agitaient vers eux leur mouchoir.


Et puis le lendemain de la victoire impie,
La hideuse délation.
Après l’assassinat, la froide calomnie,
L’implacable proscription ;


Puis les cachots sans air, dont les voûtes obscures
Des mourants étouffaient les cris
Où les bourreaux rouvraient les récentes blessures
Et broyaient les membres meurtris.


Oh qu’ils ont bien d’avance absous nos représailles !
Quand nos bras seront déchaînés.
Pensons aux morts : il faut de grandes funérailles
A nos frères assassinés.


A notre tour enfin, à vous d’abord, nos maîtres,
Nos représentants, nos élus.
Vil troupeau d’assassins, de lâches et de traîtres,
A genoux ! malheur aux vaincus !


Le jour de la justice est venu, pas de grâce,
Ni prières ni repentirs
Ne vous empocheront de baiser chaque place
Où coula le sang des martyrs.


Toi, l’aveugle Instrument de leur froide colère.
Vis, d’exécration chargé ;
Pourvu qu’à ton chevet le spectre de ton frère
Se lève, le Peuple est vengé.


Vous qui nés dans nos rangs avez trahi vos frères,
des Peuples éternels fléaux.
Du pouvoir qui vous paie implacables sicaires,
Esclaves, valets de bourreaux.


Et vous, vils trafiquants, race basse et rampante,
Qui, dans ces jours maudits, alliez
Soûlant d’or et de vin la horde rugissante
Des égorgeurs stipendiés.


Loin d’ici, vous souillez l’air pur de la patrie !
Déjà, terrible et menaçant
Le Peuple est la qui veille ; oh fuyez, qu’il oublie
Que le sang seul lave le sang.


Pour moi, si j’ai rêvé le sceptre et la puissance.
C’est pour le bonheur de tenir
L’impassible couteau de la sainte vengeance,
Et le droit sacré de punir.


Mais au crime partout j’égalerais la peine ;
Le crime est prompt, le remords lent.
Et souvent l’assassin ronge et brise sa chaîne
Pendant que la victime attend.


J’irais sur le cadavre épeler les tortures :
Au jour de l’expiation.
Œil pour œil, dent pour dent, blessure pour blessure.
L’antique loi du talion.


Et je voudrais aussi secouant la poussière
Des siècles dans l’oubli plongés.
Évoquer leur douleur muette, et satisfaire
Tous les morts qu’on n’a pas vengés.


Car l’expiation est chose grande et sainte,
Et comme un reproche éternel
Les douleurs sans vengeance élèvent une plainte
Qui monte de la terre au ciel.


Et de peur qu’il fut dit que cette loi suprême
Put être oubliée une fois,
Pour absoudre le ciel, l’homme a cru que Dieu-même
Dut expirer sur une croix.



