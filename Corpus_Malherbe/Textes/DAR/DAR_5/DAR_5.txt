Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
DAR
DAR_5

Pierre DARU
1767-1829

DISCOURS EN VERS SUR LES FACULTÉS DE L’HOMME
SUR LES PROGRÈS DE LA CIVILISATION
1825
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Richard Renault
  (Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

DISCOURS EN VERS SUR LES FACULTÉS DE L’HOMME
Pierre Daru


https://www.academie-francaise.fr/discours-en-vers-sur-les-facultes-de-lhomme


DISCOURS EN VERS SUR LES FACULTÉS DE L’HOMME
Pierre Daru
https://gallica.bnf.fr/ark:/12148/bpt6k5740772p?rk=21459;2#

Paris
FIRMIN DIDOT, PÈRE ET FILS, LIBRAIRES
1825




┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘



DISCOURS EN VERS SUR LES FACULTÉS DE L’HOMME

L’HOMME n’est qu’un roseau, nous dit dans sa colère
Un sublime rêveur, pieux atrabilaire ;
L’Homme n’est qu’un roseau, mais un roseau pensant.
Qu’est-ce donc que penser ? Sentir ? — La brute sent. —
Retenir ?— Elle apprend. — Juger ?— Elle combine ;
Au gré de ses besoins elle se détermine.
Dans un cercle tracé par d’immuables lois,
Soumise à la nature, elle écoute sa voix.
Par cette voix, dit-on, les brutes sont guidées ;
Elles n’ont que l’instinct, et l’Homme a des idées.
Je le veux ; mais enfin de qui les tenez-vous ?
De vos sens. Et la brute a des sens comme nous.
Que m’importent les noms qu’une vaine science
Donne aux actes divers de cette intelligence,
Qui, plus ou moins parfaite, et fidèle au plaisir,
Sait d’un œil assuré comparer et choisir ?


Tout dans les animaux atteste la mémoire :
Sensibles à l’amour, ils le sont à la gloire.
L’Homme s’enorgueillit de nobles sentiments,
Dont l’exemple chez eux le frappe à tous moments :
Le castor est prudent ; l’abeille est prévoyante ;
Le coursier reconnaît une main caressante ;
Le chien suit vers la tombe un maître infortuné,
Par des enfants ingrats peut-être abandonné.
Et l’Homme, raisonnant sur les œuvres divines,
Dans ces êtres vivants voudrait voir des machines,
Se mouvant par ressorts, libres sans volonté.
Ayant des sens et non la sensibilité !
Non, Descartes, en vain tu soutins cette cause ;
Ton vaste esprit s’égare et ton nom nous impose ;
Mais, même en t’égarant, tu fis ce que jamais
La brute ne peut faire au sein de ses forêts.


Au-dessus de la terre élever son génie :
Des mondes radieux comprendre l’harmonie ;
Conquérir la nature ; avec de faibles yeux,
Observer un insecte et mesurer les cieux ;
Redescendre en soi-même ; interroger son être ;
Se sentir tourmenté du besoin de connaître ;
Gardien du trésor par le temps amassé,
Transmettre à l’avenir les leçons du passé ;
Se former des vertus une image chérie ;
Connaître des devoirs, des lois, une patrie ;
Sonder de ses regards l’espace illimité
De la source des temps jusqu’à l’éternité,
Et, toujours s’élevant de problème en problème,
Arriver jusqu’aux pieds du créateur suprême ;
Voilà l’être animé par le souffle divin,
La puissance de l’homme et son noble destin.


Mais, hélas ! au milieu de toutes ces merveilles.
Dans l’orgueil du triomphe obtenu par ses veilles.
Tandis qu’il lit au front des astres étonnés
Les mouvements futurs qui leur sont ordonnés,
Que faut-il à ce roi de la terre soumise
Pour tomber au-dessous des brutes qu’il méprise,
Pour que de la raison s’éteigne le flambeau ?
Qu’un peu de vin fermente en son étroit cerveau,
Qu’un abject animal de son écume immonde
Ait souillé cette main qui sut peser le monde.
Le voilà furieux, troublant l’air de ses cris,
Stupide, objet d’effroi, d’horreur et de mépris.
Quel est donc cet esprit, sublime intelligence,
Dont le plus faible atome a corrompu l’essence ?
Comment d’un être pur, sans organes, sans corps,
La matière peut-elle altérer les ressorts ?
O grandeur ! ô néant ! tout dans l’homme est mystère.
Demandez donc aussi par quel effet contraire
Un être tout moral, tel que ma volonté,
Peut soulever ce bras qui pend à mon côté ;
Comment, par quel pouvoir ignoré de moi-même,
Un levier de mes nerfs ébranle le système ;
Comment sur ce levier mon esprit peut agir ?
Mes membres aujourd’hui sont prêts-à m’obéir ;
Demain, je ne puis plus, par ma fibre émoussée,
Envoyer à ma main l’ordre de ma pensée.
Ce miracle à nos yeux s’opère à chaque instant,
Et, sans le remarquer, la brute en fait autant.


Ainsi donc tour-à-tour l’esprit et la matière
Agissent l’un sur l’autre en diverse manière ;
Nos sens en sont témoins, et, malgré nos efforts,
Il faut, sans les comprendre, avouer ces rapports.
De subtils raisonneurs, que le doute embarrasse,
Prompts à tout expliquer, disent avec audace,
L’un que tout est prestige et que rien n’est connu ;
Que la vie est un songe obscur et continu ;
Qu’à nos yeux l’univers n’offre qu’une apparence :
L’autre, que tout est corps, que la matière pense :
Paradoxes hautains, dont la témérité,
Loin d’éclairer l’esprit, le laisse épouvanté.
Non, tout n’est point matière, et le monde physique
N’est point l’illusion d’un rêve fantastique :
Car, en nous abusant, ce prestige trompeur
Prouve notre existence autant que notre erreur.


Un secret sentiment, commun à tous les hommes,
Nous dit que nous pensons ; si nous pensons, nous sommes.
S’il est vrai que j’existe, et comment et pourquoi
Nier que l’univers existe autour de moi ?
Autour de moi ! que dis-je ? ah ! toutes ces conquêtes,
Que l’art fit dans les cieux suspendus sur nos têtes.
Ont enflé mon orgueil et m’ont anéanti.
Autrefois, pour nous seuls de l’orient sorti,
Le soleil, chaque jour, fidèle à sa carrière,
Nous portait en tribut sa féconde lumière :
La terre était l’amour, le chef-d’œuvre des cieux.
Aujourd’hui, ce soleil, cet époux radieux,
N’est qu’un point immobile, et, sans être aperçue,
La terre autour de lui nage dans l’étendue.
Dix globes inégaux, dans leur course emportés,
Du souverain des cieux reçoivent les clartés,
Et la fille d’Herschell décrit son orbe immense,
Loin des torrents de flamme où Mercure s’élance.
Si le sort permettait qu’elle changeât son cours,
Quinze ans verraient rouler et les nuits et les jours,
Avant qu’elle atteignît au centre de la sphère
L’astre dominateur qui l’attire et l’éclaire.
Cet astre, ce foyer d’où partent tant de feux
Peut-être par lui-même il n’est pas lumineux.
Trois cent mille soleils, s’il divisait sa masse,
Tous égaux à la terre, iraient peupler l’espace.
Et pourtant ce flambeau, ce superbe géant,
Quel est-il ? une étoile, un atome, un néant :
Invisible, perdu dans l’étendue immense,
Pour les mondes lointains il n’a pas d’existence.
Pour eux il se confond clans ces pales vapeurs
Que nous offrent des cieux les vastes profondeurs,
Et qu’à l’aide de l’art, présent de Galilée,
L’œil croit apercevoir dans la voûte étoilée.


Eh ! que peut être auprès de ce roi glorieux,
Qui lui-même s’éteint dans l’abîme des cieux,
Un satellite obscur, un amas de poussière,
Qui reçoit tout de lui, la vie et la lumière ?
Si de nous ce flambeau retirait sa clarté,
Notre globe à l’instant stérile, inhabité,
Couvert de noirs frimas et de voiles funèbres,
Roulerait engourdi dans l’horreur des ténèbres.
Déjà de la chaleur le partage inégal,
De l’un et l’autre pôle aux bords du Sénégal,
Ne laisse entre la glace et la brûlante arène
Qu’une zone habitable à la nature humaine,
Et le roi de ce monde, à l’empire appelé,
Des trois parts de la terre est lui-même exilé.


Et ce serait pour lui que si loin de sa vue
Roulerait de soleils une foule inconnue !
L’atonie se croirait, dans son illusion,
Le chef-d’œuvre et la fin de la création !
Non ; mais l’atome pense, il doute, il imagine ;
Il ose remonter à sa noble origine.
S’il ne peut concevoir ni la nuit du chaos,
Ni du temps arrêté l’immuable repos.
Ni le néant fécond, ni cette heure première
On commença le temps, où naquit la matière,
Il conçoit sa faiblesse, et voit de tout côté
L’abîme impénétrable et la nécessité.


Aussitôt accourant au secours des problèmes,
L’imagination vient offrir ses systèmes.
Elle a vu le soleil, s’élançant glorieux,
Prendre possession de l’empire des cieux,
Et de l’homme nouveau le bonheur solitaire,
Quand le premier printemps vint sourire à la terre.
Elle dit les géants contre le ciel armés,
Et les foudres divins par leur crime allumés.
L’enfer ouvre à sa voix sa caverne embrasée ;
Les sages vont peupler le riant .Élysée.
La matière et l’esprit, le temps, l’immensité,
La nature, à ses yeux n’ont point d’obscurité ;
Elle explique, elle peint, et l’aine, et le grand être
Que l’univers atteste et ne fait point connaître.


Mais combien sont divers, bizarres, insensés,
Les portraits que sa main sans modèle a tracés !
Une foule de dieux vainement implorée,
Vils ouvrages de l’homme, assiégent l’empyrée.
L’inceste et l’adultère ont profané le ciel :
Anubis encensé hurle sur son autel ;
Troupe digne d’admettre à son banquet immonde
Les tyrans à qui Rome offrait le sang du monde,
D’un dieu non moins étrange austère adorateur,
Le stoïque Zénon confond l’œuvre et l’auteur,
Lorsqu’il crie, attestant la puissance suprême :
Tout ce que vous voyez, c’est Jupiter lui-même.
Eh quoi ! si l’univers eut un commencement,
S’il doit finir, s’il peut périr dans un moment.
L’univers serait Dieu. Quoi l’inerte matière.
Qui ne peut se mouvoir, et qui tombe en poussière,
Entrerait en partage avec la majesté,
Avec l’intelligence et la divinité !
Tout serait tout-puissant, sage dans la nature !
Partout le créateur, jamais la créature !
Tout serait Dieu, la brute et les méchants aussi !
Non, le sage blasphème, et n’a rien éclairci.


Cherchons la vérité près des sources limpides,
Qui l’offriront peut-être à des yeux plus timides.
Cette image de Dieu, de votre bienfaiteur,
Si l’on peut s’en faire une, elle est dans votre cœur.
Ne me demandez point son nom et son essence,
Adorez sa justice et voyez sa puissance.
Jamais vous n’atteindrez, par vos efforts constans,
A. celui qui remplit et l’espace et le temps,
Seul être illimité, seul être nécessaire.
S’il demeure invisible au fond du sanctuaire.
Si ses mains ont peuplé d’êtres moins imparfaits
Tous ces mondes brillants que sa parole a faits.
Lorsque l’éternité devient notre héritage,
Assez de gloire encor de l’homme est le partage.
Sensible, intelligent, né pour la liberté,
Sa vie est le travail, son but la vérité :
Faible, de son génie il reçoit la puissance :
Sa sagesse est le doute, et son bien l’espérance.
Il ennoblit son être en regardant le ciel :
Il y voit sa patrie et se sent immortel.
Source de nos vertus, dogme sur qui se fonde
La dignité de l’homme et le bonheur du monde,
Devant l’unique Dieu qui régit l’univers,
Tôt ou tard de nos mains tu fais tomber nos fers.
Au principe éternel tout peuple rend hommage
Sous des traits différents il s’en forme une image,
Et, courbant dans la poudre un front religieux,
Il croit à sa misère intéresser les cieux.
Aide notre œil débile, ô sagesse suprême.
Quel spectacle plus beau, plus digne du ciel même,
Que l’homme sociable, heureux, reconnaissant ;
La prière qui monte aux pieds du Tout-Puissant,
Chaîne d’or de la terre et du monde invisible ;
Et du sein des clartés du trône inaccessible.
Le père de la vie et de la vérité
Jetant sur la nature un regard de bonté ?
Un jour peut-être, un jour le flambeau d’Uranie
Dans les cieux mieux connus guidera le génie :
Une voix nous dira des secrets qu’à nos yeux
Dérobaient jusqu’ici les mondes radieux ;
Un spectacle nouveau pourra nous apparaître
Mais Dieu, l’éternité, les mystères de l’être,
Voilà ce qu’aux humains, si fiers de leur savoir,
Il ne fut pas donné seulement d’entrevoir.
Ingrats, n’accusez point l’injuste destinée :
Vos sens sont imparfaits, votre raison bornée :
Consolez-vous, mortels, et puisez le bonheur
Aux grandes vérités qui jaillissent du cœur.



