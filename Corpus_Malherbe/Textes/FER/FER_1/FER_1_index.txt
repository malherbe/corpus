FER
———
FER_1

Albert FERLAND
1872-1943

════════════════════════════════════════════
MÉLODIES POÉTIQUES

1893

1014 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VERS L’IDÉAL
	─ poème	FER1	LES ASTRES DANS LES CIEUX
	─ poème	FER2	LE POÈTE
	─ poème	FER3	AU CIEL

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ CROQUIS ET PASTELS
	─ poème	FER4	MIGNONNETTE
	─ poème	FER5	DÉSIRS ENFANTINS
	─ poème	FER6	L’AURORE
	─ poème	FER7	LE PRINTEMPS
	─ poème	FER8	HYMNE MATINAL
	─ poème	FER9	LA DÉBÂCLE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ FANTAISIES
	─ poème	FER10	LA CLOCHETTE
	─ poème	FER11	LA BULLE DE SAVON
	─ poème	FER12	LE CERF-VOLANT
	─ poème	FER13	L’AÏEUL ET L’ENFANT

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ MÉLANCOLIES
	─ poème	FER14	À UNE JEUNE FILLE
	─ poème	FER15	AU GRÉ DE L’ONDE
	─ poème	FER16	RIMES AUTOMNALES
	─ poème	FER17	TRISTESSE
	─ poème	FER18	LES CŒURS
	─ poème	FER19	ESPOIR

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ SUR LES FIBRES DU CŒUR
	─ poème	FER20	UNE PETITE VENGEANCE
	─ poème	FER21	CHARMES DE L’ŒIL
	─ poème	FER22	PROFIL D’AMANTE
	─ poème	FER23	PRÉFÉRENCE
	─ poème	FER24	EXPANSION
	─ poème	FER25	SECRET DU CŒUR
	─ poème	FER26	PARLER D’AMOUR
	─ poème	FER27	TIMIDITÉ VIRGINALE
	─ poème	FER28	ALTERNATIVE ÉROTIQUE
	─ poème	FER29	AMOUR DIVINISÉ

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ VOIX INTÉRIEURES
	─ poème	FER30	AMOUR DIVIN
	─ poème	FER31	STANCES
	─ poème	FER32	L’AUTOMNE
	─ poème	FER33	APPARENCES ILLUSOIRES
	─ poème	FER34	JÉHOVAH

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ D’ICI DE LÀ
	─ poème	FER35	CHANTE ENCORE !
	─ poème	FER36	L’AUBE D’UNE FEMME
	─ poème	FER37	RÉPONSE À UNE INVITATION CHARMANTE
	─ poème	FER38	TENDRES CHOSES
	─ poème	FER39	POUR UN ALBUM
