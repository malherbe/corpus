HUG
———
HUG_30

Victor HUGO
1802-1885

════════════════════════════════════════════
POÉSIES DIVERSES

édition de 1822
1867

408 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ RAYMOND D’ASCOLI
	─ poème	HUG1948	RAYMOND D’ASCOLI

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ IDYLLE
	─ poème	HUG1949	IDYLLE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES DERNIERS BARDES
	─ poème	HUG1950	LES DERNIERS BARDES
