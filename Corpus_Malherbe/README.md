<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Corpus Malherbe (corpus principal)

#### Textes au format XML-TEI et TXT


Chaque répertoire d'un auteur (voir le fichier **liste_des_répertoires.txt** pour la liste des codes
des auteurs) contient un ou plusieurs répertoires correspondant chacun à une pièce de théâtre
ou à un recueil de poésies.

Chaque répertoire d'une pièce de théâtre ou d'un recueil de poésies contient trois fichiers :

* Le texte au format XML_TEI
* Le texte au format TXT
* Une table des matières

Le texte au format TXT est généré automatiquement à partir du fichier
XML au moyen du script **faire_texte_brut.sh**.

La table des matières est générée automatiquement à partir du fichier
XML au moyen du script **faire_index.sh**.



