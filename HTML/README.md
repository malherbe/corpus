<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Pages HTML 


- **liste_textes.html** : Version html du document **Corpus_Malherbe_liste_des_textes.pdf** du répertoire Documents. Le fichier PDF est créé automatiquement à partir de cette page (script Bash : **liste_textes.sh** du répertoire Scripts).
- ** treemap_corpus.html** : Carte proportionnelle interactive du corpus (Google treemap) créée à partir du script bash **faire_treemap.sh** du répertoire Scripts.
