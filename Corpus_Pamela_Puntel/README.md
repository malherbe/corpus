<img src="../Images/Projet/corpus_malherbe_1.svg" alt="Corpus Malherbe" width="80"/>

### Corpus préparé par Pamela Puntel dans le cadre de sa thèse (première partie : 1870-1871)

Thèse :
_La défaite de 1870 ou comment traduire un événement historique en littérature. Enquête autour de la poésie patriotique dans la France fin-de-siècle (1870-1898)_


Thèse en préparation à l'université de Lyon-2 en cotutelle avec l'Universita Degli Studi Di Udine, dans le cadre de _Lettres, Langues, Linguistique, Arts_, en partenariat avec l'_Institut d'Histoire des Représentations et des Idées dans les Modernités_ (UMR 5317), sous la direction d'Anna Zoppellari et de Sarah Al-Matary.

[page personnelle (IHRIM)](http://ihrim.ens-lyon.fr/auteur/puntel-pamela)

[présentation de la thèse (youtube.com)](https://www.youtube.com/watch?v=H49RWroIlbQ)

Chaque répertoire d'un auteur (voir le fichier **liste_des_répertoires.txt** pour la liste des codes
des auteurs) contient un ou plusieurs répertoires correspondant chacun à une pièce de théâtre
ou à un recueil de poésies.

Chaque répertoire d'une pièce de théâtre ou d'un recueil de poésies contient trois fichiers :

* Le texte au format XML_TEI
* Le texte au format TXT
* Une table des matières

Le texte au format TXT est généré automatiquement à partir du fichier
XML au moyen du script **faire_texte_brut.sh**.

La table des matières est générée automatiquement à partir du fichier
XML au moyen du script **faire_index.sh**.
