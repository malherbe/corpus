GRI
———
GRI_1

Émile GRIMAUD
1831-1901

════════════════════════════════════════════
REVUE DE BRETAGNE ET DE VENDÉE

QUATORZIÈME ANNÉE
TROISIÈME SÉRIE — TOME VIII
1870

413 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ Octobre 1870
	─ poème	GRI1	LA MARSEILLAISE VENDÉENNE
	─ poème	GRI2	LA STATUE
	─ poème	GRI3	UN SOLDAT DU PAPE
	─ poème	GRI4	A VICTOR DE LAPRADE

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ Décembre 1870
	─ poème	GRI5	LES DEUX ÉPÉES
	─ poème	GRI6	LE FILS D’UN PREUX
