BRG
———
BRG_7

Émile BERGERAT
1845-1923

════════════════════════════════════════════
POËMES DE LA GUERRE

1870-1871Édition partielle
1871

2140 vers

─ pièce	BRG7	LES DEUX MÈRES
─ poème	BRG8	SAINT-CLOUD
─ poème	BRG9	LA NUIT DE VERSAILLES
─ poème	BRG10	LES NEUTRES
─ poème	BRG11	BISTU
