FRK
———
FRK_2

Félix FRANK
1837-1895

════════════════════════════════════════════
CHANTS DE COLÈRE

1871

1139 vers

─ poème	FRK2	ÉCRASONS L’IMFAME

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ L’EMPIRE
	─ poème	FRK3	"Comme je n’ai jamais caché ma haine intense"
	─ poème	FRK4	L’HOMME DES RASTELLS
	─ poème	FRK5	AVE CÆSAR
	─ poème	FRK6	LA VOLONTÉ
	─ poème	FRK7	LES PROSCRITS
	─ poème	FRK8	PAR EDMOND CHRISTIAN
	─ poème	FRK9	LE RÉVEIL DE LA MUSE
	─ poème	FRK10	LE SPECTRE DE BAUDIN
	─ poème	FRK11	JACQUES BONHOMME

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ L’INVASION
	─ poème	FRK12	L’ÂME DES GÉANTS
	─ poème	FRK13	APRÈS SEDAN
	─ poème	FRK14	DEBOUT, FRANCE !
	─ poème	FRK15	LES HÉROS DE CHATEAUDUN
	─ poème	FRK16	LE CRIS DE PARIS
	─ poème	FRK17	A GAMBETTA - EXSECRATUS

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ LES ÉPAVES
	─────────────────────────────────────────────────────
	▫ PATRIE
		─ poème	FRK18	I. "QUAND la France aura vu fuir l’étranger rapace,"
		─ poème	FRK19	II. "Demeure donc, ô France, en ton calme foyer !"
		─ poème	FRK20	III. "Montrons, France, montrons à ces hommes de proie"
	─ poème	FRK21	L’ÉTERNELLE ÉPOPÉE
	─ poème	FRK22	A LA FRANCE
	─ poème	FRK23	DEVANT UNE BIÈRE
	─ poème	FRK24	AU BORD DU GOUFFRE
	─ poème	FRK25	LES INCENDIAIRES
	─ poème	FRK26	L’ÉCOLE
	─ poème	FRK27	LA REVANCHE
─ poème	FRK28	VIVE LA RÉPUBLIQUE !
