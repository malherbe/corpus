DLP
———
DLP_2

Albert DELPIT
1849-1893

════════════════════════════════════════════
POÉSIES DE GUERRE

Poèmes publiés dans LA REVUE DES DEUX MONDES (1871)
1871

154 vers

─ poème	DLP36	I. — LES ÉTRENNES DE PARIS. "Allons ! pille, assassine, arrache, égorge encore,"
─ poème	DLP37	II. — LE VOLONTAIRE. "— Chère femme, je viens te dire un gros mystère"
─ poème	DLP38	III. — L’ORPHELIN. "La mère est accoudée à la table de chêne ;"
