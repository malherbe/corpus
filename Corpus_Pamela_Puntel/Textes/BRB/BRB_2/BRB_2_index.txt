BRB
———
BRB_2

Auguste BARBIER
1805-1882

════════════════════════════════════════════
DEVANT L’ENNEMI

Poémes publiés dans la REVUE DES DEUX MONDES (1870)
1870

124 vers


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
▪ DEVANT L’ENNEMI
	─ poème	BRB54	LE FILS DES HUNS
	─ poème	BRB55	AUX ALLEMANDS
	─ poème	BRB56	MACTE ANIMO…
