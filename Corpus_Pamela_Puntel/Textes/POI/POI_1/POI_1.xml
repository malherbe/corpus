<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE MOMENT PSYCHOLOGIQUE</title>
				<title type="sub">Poème publié dans le journal LE GAULOIS (1871)</title>
				<title type="medium">Édition électronique</title>
				<author key="POI">
					<name>
						<forename>Jules</forename>
						<surname>POIRET</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>96 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">POI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title/>
						<author>JULES POIRET</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k520074d/f1.image.r=Le%20Gaulois%20litt%C3%A9raire%20et%20politique%201871</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Le Gaulois : littéraire et politique</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Le Gaulois : littéraire et politique</publisher>
									<date when="1871">1871-01-04</date>
								</imprint>
								<biblScope unit="issue">913</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-23" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<front>
			<p>
				Vous vous rappelez cette gazette de Berlin qui contait à ses sensibles compatriotes que M. de Bismarck attendait, pour nous bombarder, le moment psychologique.
				Ce mot, qui nous a fait sourire, nous autres Parisiens, car nous sommes de bonnes pâtes d’hommes, a arraché ce cri d’indignation railleuse à un jeune poëte, M. Jules Poiret.
				Il a épanché sa verve dans la pièce que l’on va lire. Elle a été déjà récitée à l’un
				de ces nombreux spectacles organisés par des bataillons de la garde nationale, et elle y a été vivement applaudie. Nous avons pensé qu’elle ferait plaisir aux lecteurs du Gaulois.
				Mais nous croyons devoir prévenir que nous accordons par exception l’hospitalité à ces vers ; que celte exception ne se renouvellera pas, et que nous ne manquerons plus, qu’à moins de cas extraordinaires, à la loi que nous nous sommes imposée de ne jamais insérer de poésie dans le Gaulois.
				F.S.
			</p>
		</front>
		<body>
			<div type="poem" key="POI1">
				<head type="main">LE MOMENT PSYCHOLOGIQUE</head>
				<lg n="1">
					<l n="1" num="1.1">Il faut tirer l’échelle après psychologique ;</l>
					<l n="2" num="1.2">Le trait est sheakspearien formidable et comique.</l>
					<l n="3" num="1.3">Soyons donc pardonnés, si le rire nous prend,</l>
					<l n="4" num="1.4">Bien que plus d’un coté des choses soit navrant.</l>
					<l n="5" num="1.5">Guillaume, tu me plais dans ta nouvelle phase</l>
					<l n="6" num="1.6">Comme Hector déposant le casque qui l’écrase,</l>
					<l n="7" num="1.7">Tu te fais plein de grâce et revêts le docteur,</l>
					<l n="8" num="1.8">Disant : Je suis savant puisque je suis vainqueur,</l>
					<l n="9" num="1.9">Je veux, après avoir achevé Bonaparte,</l>
					<l n="10" num="1.10">A ma discrétion mettre aussi leur Descarte.</l>
					<l n="11" num="1.11">J’exprime du présent l’avenir inconnu ;</l>
					<l n="12" num="1.12">Paris est devant moi comme un malade nu ;</l>
					<l n="13" num="1.13">J’ai la main sur son pouls, je presse ses artères,</l>
					<l n="14" num="1.14">Et lis à l’horizon ses traits pleins de mystères.</l>
					<l n="15" num="1.15">Je dirige sa crise et mène sa langueur,</l>
					<l n="16" num="1.16">Et du temps à son mal rapportant la longueur,</l>
					<l n="17" num="1.17">Je fixe le moment où je veux qu’il se livre ;</l>
					<l n="18" num="1.18">C’est fini, c’est écrit, c’est réglé comme un livre.</l>
					<l n="19" num="1.19">S’il faut s’y résigner, on le bombardera,</l>
					<l n="20" num="1.20">Mais le plus tard pourtant et le moins qu’on pourra.</l>
					<l n="21" num="1.21">Ce peuple est fort méchant et, si je l’exaspère,</l>
					<l n="22" num="1.22">Il tuera mes enfants dont plus d’un est grand-père.</l>
					<l n="23" num="1.23">Au feu, quant à présent, je préfère la faim ;</l>
					<l n="24" num="1.24">C’est pour tuer les gens encor le plus humain.</l>
					<l n="25" num="1.25">Strasbourg en feu me voue une éternelle haine,</l>
					<l n="26" num="1.26">Et la famine fait un agneau de Bazaine.</l>
					<l n="27" num="1.27">Et puis, pur des raisons à moi, la faim ma plaît.</l>
					<l n="28" num="1.28">Des femmes, sur leur terme, elle tarit le lait :</l>
					<l n="29" num="1.29">Augusta dévorait, quand elle était enceinte</l>
					<l n="30" num="1.30">(Mais de ce côté-là nous n’avons plus de crainte.)</l>
					<l n="31" num="1.31">Les bébés périront, surtout le sexe fille ;</l>
					<l n="32" num="1.32">Nous autres Allemands, nous aimons la famille.</l>
					<l n="33" num="1.33">Baste ! les miens sont grands, insondable est le ciel.</l>
					<l n="34" num="1.34">Parisiens, vous aurez des verges à Noël.</l>
					<l n="35" num="1.35">Au fond, mes bons amis, c’est un peu votre faute</l>
					<l n="36" num="1.36">Tu ne veux pas Paris, recevoir ton doux hôte</l>
					<l n="37" num="1.37">Aussi tu mangeras pour gâteau du brouet</l>
					<l n="38" num="1.38">Et moi j’aurai l’empire, historique jouet,</l>
					<l n="39" num="1.39">Et je rapporterai pour son cadeau d’étrenne,</l>
					<l n="40" num="1.40">A ma blanche Augusta, l’Alsace et la Lorraine.</l>
				</lg>
				<lg n="2">
					<l n="41" num="2.1"><space unit="char" quantity="10"/>Non, Jamais crime heureux, jamais</l>
					<l n="42" num="2.2"><space unit="char" quantity="10"/>Bassesse montée aux sommets</l>
					<l n="43" num="2.3"><space unit="char" quantity="10"/>Pleins de vertige et de démence,</l>
					<l n="44" num="2.4"><space unit="char" quantity="10"/>Où la déchéance commence,</l>
					<l n="45" num="2.5"><space unit="char" quantity="10"/>N’en ont plus tôt pris leur parti.</l>
					<l n="46" num="2.6"><space unit="char" quantity="10"/>Jamais, quand leur nombre est sorti,</l>
					<l n="47" num="2.7"><space unit="char" quantity="10"/>Joueurs n’ont eu plus d’insolence</l>
					<l n="48" num="2.8"><space unit="char" quantity="10"/>Et plus de dédain pour la chance.</l>
				</lg>
				<lg n="3">
					<l n="49" num="3.1"><space unit="char" quantity="10"/>Passe encor leur croyance en soi !</l>
					<l n="50" num="3.2"><space unit="char" quantity="10"/>Ces gens à dégoûter la foi,</l>
					<l n="51" num="3.3"><space unit="char" quantity="10"/>Et dont l’aspect tourne en sophisme</l>
					<l n="52" num="3.4"><space unit="char" quantity="10"/>Le magnifique syllogisme</l>
					<l n="53" num="3.5"><space unit="char" quantity="10"/>Qui va du monde à son auteur,</l>
					<l n="54" num="3.6"><space unit="char" quantity="10"/>Et de l’homme à son créateur</l>
					<l n="55" num="3.7"><space unit="char" quantity="10"/>Ces êtres ont l’outrecuidance</l>
					<l n="56" num="3.8"><space unit="char" quantity="10"/>De croire en une Providence.</l>
				</lg>
				<lg n="4">
					<l n="57" num="4.1"><space unit="char" quantity="10"/>Le nôtre eut aussi son Credo :</l>
					<l n="58" num="4.2"><space unit="char" quantity="10"/>Récusant son sanglant fardeau,</l>
					<l n="59" num="4.3"><space unit="char" quantity="10"/>Il prenait Dieu pour son compère ;</l>
					<l n="60" num="4.4"><space unit="char" quantity="10"/>Tous les deux vous faites la paire.</l>
					<l n="61" num="4.5"><space unit="char" quantity="10"/>Misérables, vous affichez</l>
					<l n="62" num="4.6"><space unit="char" quantity="10"/>Des lambeaux de pourpre arrachés</l>
					<l n="63" num="4.7"><space unit="char" quantity="10"/>Au trône éternel de justice</l>
					<l n="64" num="4.8"><space unit="char" quantity="10"/>Pour en parer votre immondice.</l>
				</lg>
				<lg n="5">
					<l n="65" num="5.1">Le bon Dieu luit pour tous, sachez le bien mon maître ;</l>
					<l n="66" num="5.2">Il abat l’orgueilleux, comme il punit le traître.</l>
					<l n="67" num="5.3">Attends Paris, Guillaume Hohenzollern, attends ;</l>
					<l n="68" num="5.4">Pour toi le Moindre risque est d’attendre longtemps.</l>
					<l n="69" num="5.5">Paris donnera tort à ta psychologie,</l>
					<l n="70" num="5.6">Et tu verras sa fièvre après sa léthargie.</l>
					<l n="71" num="5.7">Oui, Paris, qu’en respect tu couches, chien d’arrêt,</l>
					<l n="72" num="5.8">Et dont tu crois avoir énervé le jarret,</l>
					<l n="73" num="5.9">Paris qui se contient garde un élan sublime</l>
					<l n="74" num="5.10">Pour te faire mentir, ô cruel magnanime,</l>
					<l n="75" num="5.11">Il ira te chercher, quand il le jugera bon</l>
					<l n="76" num="5.12">Pour disserter ensemble à grands coups de canon.</l>
					<l n="77" num="5.13">Tu nous vois affamés, c’est une erreur profonde :</l>
					<l n="78" num="5.14">Comme dans Sybaris nous vivons loin du monde ;</l>
					<l n="79" num="5.15">Nous mangeons l’ami chien, le serviteur cheval ;</l>
					<l n="80" num="5.16">Ce ne sont que mets doux, crêpes de carnaval :</l>
					<l n="81" num="5.17">Nourriture abondante et surtout variée.</l>
					<l n="82" num="5.18">Quoique sur certains points parfois avariée.</l>
					<l n="83" num="5.19">Nous réconcilions la souris et le chat,</l>
					<l n="84" num="5.20">Et consommons, après son fromage, le rat ;</l>
					<l n="85" num="5.21">Mais dussions-nous broyer, pour sauver notre force,</l>
					<l n="86" num="5.22">Des animaux muets et des arbres l’écorce,</l>
					<l n="87" num="5.23">Au cheval qui n’est plus ajouter le harnais ;</l>
					<l n="88" num="5.24">Dussions-nous sans leur pointe absorber ces objets</l>
					<l n="89" num="5.25">Dont tes profonds soldats, excellant dans la fuite,</l>
					<l n="90" num="5.26">Du Français curieux amusent la poursuite,</l>
					<l n="91" num="5.27">Sois dûment informé que le moment viendra</l>
					<l n="92" num="5.28">Où de notre courroux l’amas débordera,</l>
					<l n="93" num="5.29">Où nous irons cracher cette âcre nourriture</l>
					<l n="94" num="5.30">Avec de bons obus, Guillaume, à ta figure,</l>
					<l n="95" num="5.31">Et tu verras alors, philosophe, à ton tour,</l>
					<l n="96" num="5.32">Que, comme les tyrans, les peuples ont leur tour.</l>
				</lg>
				<closer>
					<signed>JULES POIRET.</signed>
				</closer>
			</div>


		</body>
	</text>
</TEI>