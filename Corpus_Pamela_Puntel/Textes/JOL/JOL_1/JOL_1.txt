Version TXT du texte obtenue par extraction à partir du fichier XML.
┌─────────────────────┐
│Entête du fichier XML│
└─────────────────────┘
JOL
JOL_1

Gaston JOLLIVET
1842-1927

LES SOLDATS D’AUTREFOIS
Poème publié dans le journal LE GAULOIS (1870)
1870
_________________________________________________________________
Édition électronique
Corpus Malherbə
Laboratoire CRISCO, Université de Caen
Contact : crisco.incipit@unicaen.fr
_________________________________________________________________
Préparation du texte :

▫ Pamela Puntel
  (Préparation des textes)

▫ Richard Renault
  (Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées)

_________________________________________________________________
Origine de la source électronique :

LES SOLDATS D’AUTREFOIS
GASTON JOLLIVET


https://gallica.bnf.fr/ark:/12148/bpt6k519993r/f3.item


Le Gaulois : littéraire et politique

PARIS
Le Gaulois : littéraire et politique
1870-11-19

867



┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉┉
texte hors droits distribué sous licence libre
Licence Creative Commons : CC-BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┌─────┐
│TEXTE│
└─────┘






LES SOLDATS D’AUTREFOIS

Les portraits des vieux combattants,
Qu’on aimait à les voir, naguère,
Au musée, à Versaille, au temps
Où ce n’était pas la frontière !


Tous beaux, ces preux bardés de fer !
Cheveux noirs ou crinière blonde,
Des bras à défier l’enfer,
Une épaule à porter le monde !


C’étaient les héros d’autrefois,
Les paladins et les grands reîtres.
L’Arioste a dit leurs exploits ;
Bayard les saluait ses maîtres.


Ils descendaient du vieux manoir,
Ces fiers guerriers à haute taille,
Et c’était plaisir de les voir
Monter le cheval de bataille.


L’épée au soleil reluisait.
L’épouse attachait la ceinture
Et l’enfant se dressant baisait
Et rebaisait la lourde armure.


En selle, et sus à l’ennemi !
Cavaliers, au vent la bannière !
On se retrouvera parmi
Les vainqueurs ou dans la poussière.


Et c’étaient des combats joyeux,
Passes d’armes sans fin ni trêve,
Corps à corps, les yeux dans les yeux,
Et le glaive contre le glaive.


Sur le baudrier se drapait
L’écharpe aux couleurs de la belle
Et l’on savait où l’on frappait
Sans longue-vue et sans jumelle.


Soi-même on comptait les effets
De son bras, lorsque venait l’ombre ;
Car des morts que l’o avait faits
Le sol sanglant disait le nombre !


Salut, hélas ! et sans retour,
Nobles luttes du moyen âge,
Où l’on se battait en plein jour,
En plein soleil, en plein courage !


Où l’on n’avait pas, dans son coin,
Courbé sur des calculs bizarres,
Un vieux savant gagnant de loin
La bataille entre deux catarrhes ;


Où nos bons aïeux du vieux temps,
Bravant les ruses taciturnes,
Laissaient la plaine aux combattants
Et les bois aux voleurs nocturnes !






