<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HAINE AUX BARBARES</title>
				<title type="sub_1">CHANTS PATRIOTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="MDC">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>MÉRI DE LA CANORGUE</surname>
					</name>
					<date from="1805" to="1875">1805-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>272 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MDC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
						<author>VICTOR DE MÉRI DE LA CANORGUE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30929677r.public</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
								<author>VICTOR DE MÉRI DE LA CANORGUE</author>
								<imprint>
									<pubPlace>MARSEILLE</pubPlace>
									<publisher>CAMOIN LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<front>
			<p>
				Il faut remonter à quinze cents ans dans l’histoire, pour trouver quelque chose d’analogue à cette nouvelle invasion de barbares, qui vient de porter la désolation, le deuil, le ravage et la mort dans notre belle France, naguère encore si prospère, et qui pleure aujourd’hui désolée sur ses champs dévastés, et sur les ruines fumantes de ses villes, de ses palais, de ses églises et même des asiles sacrés, où la religion et la charité se donnaient rendez-vous pour y soulager et y consoler tous les genres de misères humaines.
				Attila, le fléau de Dieu, ne pouvait certes se reproduire à nos yeux sous des traits plus semblables aux siens que ceux de son féroce imitateur. Toutefois le roi des Huns me parait moins exécrable que son rival en barbarie, En ce sens que, n’étant point chrétien, et que n’ayant point paru sur la terre à une époque de civilisation avancée comme la nôtre, il lui était permis de méconnaitre le droit des gens, les lois de l’humanité, qui sont un frein aux lois de la guerre ; tandis que Guillaume osait s’autoriser du nom de Dieu pour permettre à ses soldats de se livrer à tous les genres de crimes, tels que le meurtre des gens inoffensifs, le pillage, le vol, l’incendie et le viol, et
				c’est ainsi qu’il a prétendu nous moraliser, le féroce hypocrite !
				Tel est l’effrayant tableau de toutes les horreurs que j’ai essayé de représenter dans mes vers, non-seulement pour en flétrir les auteurs, mais encore pour faire ainsi ressortir l’héroïsme de nos soldats dans une lutte désespérée et sublime et rendue si désastreuse, par la félonie, l’incurie et la lâcheté d’un homme à jamais infâme et de quelques-uns de ses généraux.
				Les nations qui nous devaient le plus et qui nous ont abandonné sans nous prêter secours, si ce n’est par de stériles sympathies, ont eu aussi leur part de flétrissure dans mes chants patriotiques.
				Si mon âge avancé ne m’a pas permis de prendre les armes pour la défense de mon malheureux pays, j’ai voulu lui prouver du moins combien je l’aimais, et tout ce que mon cœur de Français a souffert et souffre encore de ses douleurs imméritées, do ses désastres et de ses profondes humiliations.

				AVANT-PROPOS

				LES BARBARES EN FRANCE
			</p>
		</front>
		<body>
			<div type="poem" key="MDC1">
				<head type="main">UN CRI VENGEUR</head>
				<head type="form">1er CHANT</head>
				<lg n="1">
					<l n="1" num="1.1">Toi, la mère des Preux, ma noble patrie !</l>
					<l n="2" num="1.2">Sous le poids des revers, vas-tu tomber flétrie ?</l>
					<l n="3" num="1.3">Vas-tu laisser ton sol souillé par l’étranger,</l>
					<l n="4" num="1.4">Sans saisir l’insolent, le vaincre et te venger,</l>
					<l n="5" num="1.5">Sans faire resplendir, sur le champ de bataille,</l>
					<l n="6" num="1.6">L’honneur de ton drapeau, broyé par la mitraille ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Les barbares sont là sur tes champs dévastés ;</l>
					<l n="8" num="2.2">Arme-toi de ta faulx, et, d’un élan superbe,</l>
					<l n="9" num="2.3">Les courbant sous tes pieds, fauche-les comme l’herbe</l>
					<l n="10" num="2.4">Tes bataillons surpris n’ont point été domptés :</l>
					<l n="11" num="2.5">Dans les rangs ennemis, ces lions pleins d’audace</l>
					<l n="12" num="2.6">Sont tombés en laissant une sanglante trace.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Il n’est pas une ville, il n’est pas un hameau</l>
					<l n="14" num="3.2">Qui de nos ennemis ne creuse le tombeau,</l>
					<l n="15" num="3.3">La France provoquée a frémi de colère,</l>
					<l n="16" num="3.4">Et je la vois déjà, dans sa fureur guerrière,</l>
					<l n="17" num="3.5">Coucher dans les sillons de ses champs désolés,</l>
					<l n="18" num="3.6">Ces ravageurs hideux, h sa rage immolés ;</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Faire partout la chasse à ces incendiaires</l>
					<l n="20" num="4.2">Qui brûlent les châteaux, les villes, les chaumières,</l>
					<l n="21" num="4.3">Et laissent sur leurs pas des larmes et du sang.</l>
					<l n="22" num="4.4">O ! France ! étouffe-les dans un effort puissant ;</l>
					<l n="23" num="4.5">Agrandis leurs tombeaux, jettes-y leurs cohortes,</l>
					<l n="24" num="4.6">Et du pays sauvé ferme sur eux les portes !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">L’étranger qui te foule, à chacun de ses pas,</l>
					<l n="26" num="5.2">Fait surgir des héros qui ne reculent pas,</l>
					<l n="27" num="5.3">Qui vengent ton honneur, ô France bien-aimée !</l>
					<l n="28" num="5.4">Dont le cœur héroïque, à son dernier soupir,</l>
					<l n="29" num="5.5">En s’éteignant pour toi palpite de plaisir.</l>
					<l n="30" num="5.6">N’es-tu pas des héros la terre renommée ?</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Qui pourrait t’asservir, mon vaillant pays ?</l>
					<l n="32" num="6.2">Tous les bras sont armés pour venger ta querelle ;</l>
					<l n="33" num="6.3">Quand tu cours des dangers, tous les cœurs sont unis,</l>
					<l n="34" num="6.4">Non, tu ne mourras point, ô patrie immortelle !</l>
					<l n="35" num="6.5">Depuis quinze cents ans aux siècles tu survis,</l>
					<l n="36" num="6.6">Et ton Dieu, dans le Ciel, est le Dieu de Clovis.</l>
				</lg>
			</div>
			<div type="poem" key="MDC2">
				<head type="main">DEUX INVASIONS COMPARÉES</head>
				<lg n="1">
					<l n="1" num="1.1">Lorsqu’Attila, jadis, vint fondre sur la Gaule,</l>
					<l n="2" num="1.2">Qu’il traînait après lui le ravage et la mort ;</l>
					<l n="3" num="1.3">Que, tressaillant d’horreur, la terre en était folle,</l>
					<l n="4" num="1.4">Un guerrier de ce temps, Mérovée, au cœur fort,</l>
					<l n="5" num="1.5">Rassembla de ses Francs la valeureuse armée,</l>
					<l n="6" num="1.6">Et, marchant le premier, dédaigneux du trépas,</l>
					<l n="7" num="1.7">Il écrasa les Huns des coups de sa framée</l>
					<l n="8" num="1.8">Il vainquit l’ennemi, mais ne se rendit pas.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Héritière des Huns et leur barbare émule,</l>
					<l n="10" num="2.2">La Prusse vient chez nous, comme ces ravageurs,</l>
					<l n="11" num="2.3">Nous apporter la guerre et toutes ses horreurs,</l>
					<l n="12" num="2.4">Et, les réunissant, sur nous les accumule.</l>
					<l n="13" num="2.5">Celui qui, malgré nous, nous valut ce fléau,</l>
					<l n="14" num="2.6">A-t-il pu le dompter, ainsi que Mérovée,</l>
					<l n="15" num="2.7">En refoulant des Huns le féroce troupeau,</l>
					<l n="16" num="2.8">En montrant d’un héros la valeur éprouvée ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Dans ce champ de carnage où grondait le canon,</l>
					<l n="18" num="3.2">A-t-il cherché la mort pour mériter son nom ?</l>
					<l n="19" num="3.3">Comme à Waterloo, sur son armée en poudre,</l>
					<l n="20" num="3.4">Autre Napoléon, a-t-il bravé la foudre ?</l>
					<l n="21" num="3.5">Et tandis que le fer moissonnait nos guerriers,</l>
					<l n="22" num="3.6">Est-il tombé comme eux, couché sur ses lauriers ?</l>
					<l n="23" num="3.7">Sur son sein tout sanglant, et, dans sa main crispée,</l>
					<l n="24" num="3.8">Comme un noble vaincu pressait-il son épée ?</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Qu’ai-je dit ? son épée !… à son heureux vainqueur,</l>
					<l n="26" num="4.2">Intacte il l’a remise, en dépit de l’honneur.</l>
					<l n="27" num="4.3">Que la honte à jamais à ce traître s’attache ;</l>
					<l n="28" num="4.4">Sans férir un seul coup il s’est rendu, le lâche !</l>
					<l n="29" num="4.5">Il a pu se sauver dans ce fleuve de sang</l>
					<l n="30" num="4.6">Qui déborde aujourd’hui dans notre belle France.</l>
					<l n="31" num="4.7">Dont naguère il était le maître tout-puissant.</l>
					<l n="32" num="4.8">Et qui sur lui, du Ciel, appelle la vengeance.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Un jour la trahison le mit sur le pavois ;</l>
					<l n="34" num="5.2">Il monta sur le trône l’aide d’un parjure,</l>
					<l n="35" num="5.3">Et depuis dix-huit ans, il nous faisait l’injure,</l>
					<l n="36" num="5.4">Lui qui les méconnut, de nous dicter des lois.</l>
					<l n="37" num="5.5">Le Ciel qui l’a brisé dans un jour de colère,</l>
					<l n="38" num="5.6">A détruit pour toujours sa race tout entière.</l>
					<l n="39" num="5.7">Que notre sol lui soit à jamais interdit ;</l>
					<l n="40" num="5.8">Qu’il soit honni de tous, du monde entier maudit.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Et quant à ce troupeau, ce vil amas d’esclaves</l>
					<l n="42" num="6.2">Qui voudrait de ses fers nous mettre les entraves,</l>
					<l n="43" num="6.3">Qu’il y renonce enfin, car la France est debout,</l>
					<l n="44" num="6.4">Et son sein se soulève et de colère bout.</l>
					<l n="45" num="6.5">La Prusse de son rang no la fera descendre :</l>
					<l n="46" num="6.6">Semblable à cet oiseau qui sortait de sa cendre,</l>
					<l n="47" num="6.7">Ainsi que Le Phénix la France renaitra,</l>
					<l n="48" num="6.8">Et Dieu qui l’aime encor bientôt la vengera.</l>
				</lg>
			</div>
			<div type="poem" key="MDC3">
				<head type="main">A NOS ENVAHISSEURS</head>
				<lg n="1">
					<l n="1" num="1.1">Vous êtes des brigands et non pas des soldats !</l>
					<l n="2" num="1.2">Au sortir des forêts, vous marchez aux combats</l>
					<l n="3" num="1.3">Ainsi, favorisés par l’épaisseur de l’ombre,</l>
					<l n="4" num="1.4">Vous frappez à coups sûrs, en cachant votre nombre ;</l>
					<l n="5" num="1.5">Et puis, sur les vieillards, les femmes, les enfants,</l>
					<l n="6" num="1.6">Vous dirigez l’effort de vos bras triomphants.</l>
					<l n="7" num="1.7">Vous ne respectez rien : dans leurs maisons sacrées,</l>
					<l n="8" num="1.8">Les vierges, sous vos coups, tombent déshonorées.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Pour signaler partout vos barbares exploits,</l>
					<l n="10" num="2.2">Vous pillez, vous volez, vous tuez à la fois.</l>
					<l n="11" num="2.3">Vous foulez à vos pieds toutes les lois divines ;</l>
					<l n="12" num="2.4">Des temples les plus saints vous faites des ruines.</l>
					<l n="13" num="2.5">C’est Dieu, disent vos chefs, qui combat avec nous :</l>
					<l n="14" num="2.6">Ils mentent c’est l’enfer qui marche devant vous !</l>
					<l n="15" num="2.7">Votre roi porte au Ciel des yeux pleins de tendresse,</l>
					<l n="16" num="2.8">Et, tout en l’insultant, c’est à lui qu’il s’adresse.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Vous, les dignes soldats d’un roi si bon croyant,</l>
					<l n="18" num="3.2">Vous allez comme lui contre Dieu guerroyant.</l>
					<l n="19" num="3.3">C’est ainsi que, mêlant l’ironie aux blasphèmes,</l>
					<l n="20" num="3.4">Vous frappez sans pitié sur les blessés eux-mêmes :</l>
					<l n="21" num="3.5">Que vos plus gros canons criblent de leurs boulets</l>
					<l n="22" num="3.6">Même les hôpitaux, à l’égal des palais :</l>
					<l n="23" num="3.7">Que, tenant dans les mains la torche incendiaire,</l>
					<l n="24" num="3.8">Vous changez des vivants en ardente poussière.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Ne vous a-t-on pas vus, aux flammes d’un brasier,</l>
					<l n="26" num="4.2">Jeter les habitants d’un pays tout entier ?</l>
					<l n="27" num="4.3">Barbares, c’est assez ! Rentrez dans vos repaires !</l>
					<l n="28" num="4.4">Débarrassez le sol qu’ont illustré nos pères !</l>
					<l n="29" num="4.5">Barbares, arrêtez le cours de vos forfaits !</l>
					<l n="30" num="4.6">Nous sommes assez forts pour secouer le faix</l>
					<l n="31" num="4.7">Des maux dont vous avez accablé notre France</l>
					<l n="32" num="4.8">Et l’heure sonne enfin de notre délivrance.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Contre vous rassemblés tous les Francs s’uniront ;</l>
					<l n="34" num="5.2">Les guerriers, les vieillards, les enfants marcheront ;</l>
					<l n="35" num="5.3">Pour venger leurs affronts, les filles et les femmes</l>
					<l n="36" num="5.4">Crieront contre vous : Abattons ces infâmes !</l>
					<l n="37" num="5.5">Et le pays, jetant ses vêtements de deuil,</l>
					<l n="38" num="5.6">Sortira tout vivant du fond de son cercueil</l>
					<l n="39" num="5.7">Le tocsin sonnera de village en village ;</l>
					<l n="40" num="5.8">La foudre grondera comme en un jour d’orage ;</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">De votre sang maudit le sol regorgera ;</l>
					<l n="42" num="6.2">Poursuivi sans merci, pas un n’échappera ;</l>
					<l n="43" num="6.3">Le Ciel, enfin, touché des cris de vos victimes,</l>
					<l n="44" num="6.4">Vous anéantira sous le poids de vos crimes.</l>
					<l n="45" num="6.5">Veuves et orphelins, allons ! séchez vos yeux,</l>
					<l n="46" num="6.6">Car s’il n’est plus pour vous de jour qui soit heureux,</l>
					<l n="47" num="6.7">Si vos cœurs désolés sont tout h la souffrance,</l>
					<l n="48" num="6.8">Ah ! souriez du moins au jour de la vengeance !</l>
				</lg>
			</div>
			<div type="poem" key="MDC4">
				<head type="main">LES NEUTRES</head>
				<lg n="1">
					<l n="1" num="1.1">Vous n’êtes d’aucun genre, ô nations vieillies,</l>
					<l n="2" num="1.2">Et vous montrez ainsi votre virilité.</l>
					<l n="3" num="1.3">Dans un lâche repos restez ensevelies ;</l>
					<l n="4" num="1.4">Gardez-vous de sortir de la neutralité !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Après tout, ce n’est rien, c’est la France éplorée</l>
					<l n="6" num="2.2">Qui traine dans le sang sa robe déchirée,</l>
					<l n="7" num="2.3">Qu’on voudrait immoler en la frappant au cœur.</l>
					<l n="8" num="2.4">Et qui, seule, défend sa vie et son honneur.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">A l’abri des obus qui sifflent aux oreilles,</l>
					<l n="10" num="3.2">Vous laissez bombarder la ville des merveilles,</l>
					<l n="11" num="3.3">Sauf venir un jour, dans ce même Paris,</l>
					<l n="12" num="3.4">Avec le monde entier, pleurer sur ses débris.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Pourquoi vous émouvoir ? Ce sont de pauvres femmes,</l>
					<l n="14" num="4.2">Des enfants, des vieillards, qu’on jette dans les flammes.</l>
					<l n="15" num="4.3">Des pays dévastés, des toits pleins de douleurs,</l>
					<l n="16" num="4.4">Des ruines, du sang, des familles en pleurs ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Des blessés achevés par d’affreuses tortures ;</l>
					<l n="18" num="5.2">Des prisonniers sans pain ; de pauvres créatures</l>
					<l n="19" num="5.3">Qui vont errant partout, et qui tendent la main</l>
					<l n="20" num="5.4">Aux passants effarés, et tombent en chemin.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">O neutres impuissants ! de vos secours avares,</l>
					<l n="22" num="6.2">Laissez-nous inonder par ces flots de barbares ;</l>
					<l n="23" num="6.3">Mais s’ils venaient un jour se rejeter sur vous,</l>
					<l n="24" num="6.4">Vous verrait-on encor tourner les yeux sur nous ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Supportez tout le poids de votre ingratitude,</l>
					<l n="26" num="7.2">Et n’ayez de nos maux nulle sollicitude !</l>
					<l n="27" num="7.3">La France est généreuse, et souvent ses soldats</l>
					<l n="28" num="7.4">Sont tombés moissonnés, pour vous, dans les combats.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Admirez aujourd’hui l’élan patriotique</l>
					<l n="30" num="8.2">Qui la fait résister dans sa lutte héroïque !</l>
					<l n="31" num="8.3">Oh ! non, malgré vos rois, elle ne mourra pas,</l>
					<l n="32" num="8.4">Et vous la reverrez prendre partout le pas ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Se montrer vos yeux et plus grande et plus fière ;</l>
					<l n="34" num="9.2">Dans nos jours ténébreux épandre sa lumière ;</l>
					<l n="35" num="9.3">Vaincre la barbarie, et de la liberté</l>
					<l n="36" num="9.4">Faire fleurir partout l’empire incontesté ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et secouant enfin une étreinte fatale,</l>
					<l n="38" num="10.2">Substituer le droit la force brutale.</l>
					<l n="39" num="10.3">Qu’importe qu’elle soit envahie h moitié ?</l>
					<l n="40" num="10.4">Elle n’exige rien, pas même la pitié.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Va donc, roi déloyal, va donc trôner Rome,</l>
					<l n="42" num="11.2">De la vertu des rois si tu n’as plus l’arôme !</l>
					<l n="43" num="11.3">Hâte-toi d’arracher le diadème saint</l>
					<l n="44" num="11.4">Du pontife sacré qui, de par Dieu, l’a ceint</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Souviens-toi cependant que la fortune vole,</l>
					<l n="46" num="12.2">Que le roc Tarpéïen est près du Capitole,</l>
					<l n="47" num="12.3">Et quo tes devanciers de ce haut piédestal,</l>
					<l n="48" num="12.4">Sont tombés foudroyés, dans un moment fatal.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Allons ! roi galant-homme, attache à ta couronne</l>
					<l n="50" num="13.2">La tiare…, et ce poids entraînera ton trône.</l>
					<l n="51" num="13.3">Et toi, vieille Angleterre, au lion édenté,</l>
					<l n="52" num="13.4">De tes vaisseaux brûlés touche l’indemnité !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Et toi, pays du Cid, n’ai-je rien à te dire ?</l>
					<l n="54" num="14.2">Incline-toi bien bas devant ton nouveau sire,</l>
					<l n="55" num="14.3">Ce roi que t’a légué ce vaillant hidalgo ;</l>
					<l n="56" num="14.4">Ce Prim qui, châtié, s’écriait : Castigo<ref target="1" type="noteAnchor">1</ref>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Peuples dégénérés que la crainte rassemble,</l>
					<l n="58" num="15.2">Aux genoux de Guillaume allez tomber ensemble !</l>
					<l n="59" num="15.3">Et vous, qui nous devez et du sang et de l’or,</l>
					<l n="60" num="15.4">Gardez le tout ensemble, et nos mépris encor.</l>
				</lg>
				<closer>
					<note type="footnote" id="1">(1) Castigo, châtiment en langue espagnole.</note>
				</closer>
			</div>
			<div type="poem" key="MDC5">
				<head type="main">A LA FRANCE</head>
				<lg n="1">
					<l n="1" num="1.1">Quel sort sera le tien, ô France bien-aimée !</l>
					<l n="2" num="1.2">Vas-tu servir de proie la horde affamée</l>
					<l n="3" num="1.3">Qui déchire ton sein, s’abreuve de ton sang,</l>
					<l n="4" num="1.4">Et laisse sur ton sol sa souillure, en passant ?</l>
					<l n="5" num="1.5">Ou de Charles-Martel, reprenant la massue,</l>
					<l n="6" num="1.6">Vas-tu broyer encor, comme au jour de Poitiers,</l>
					<l n="7" num="1.7">Une race maudite et dans l’enfer conçue,</l>
					<l n="8" num="1.8">Et convertir ainsi tes cyprès en lauriers ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Quoi ! tu ne réponds rien, ô ma chère mourante,</l>
					<l n="10" num="2.2">Et la terre h tes maux demeure indifférente !</l>
					<l n="11" num="2.3">Hélas ! autour de toi quel silence effrayant :</l>
					<l n="12" num="2.4">Tout tremble, tout se tait et l’abîme est béant !</l>
					<l n="13" num="2.5">Pourtant tu n’es point morte, ô France que j’adore !</l>
					<l n="14" num="2.6">Ton cœur, quoique saignant, ton cœur palpite encore.</l>
					<l n="15" num="2.7">Nul peuple, nul héros ne vient te secourir,</l>
					<l n="16" num="2.8">Et, seule, sans secours te faudra-t-il mourir ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Ton barbare ennemi, contre toi plein de rage,</l>
					<l n="18" num="3.2">Te foule sous ses pieds, te dépouille et t’outrage :</l>
					<l n="19" num="3.3">Il a juré ta mort, le poignard sur ton sein,</l>
					<l n="20" num="3.4">Et de roi qu’il était il s’est fait assassin !</l>
					<l n="21" num="3.5">Qui donc viendra panser tes blessures profondes ?</l>
					<l n="22" num="3.6">J’interroge le Ciel, et la terre, et les ondes ;</l>
					<l n="23" num="3.7">A défaut des vivants pour chasser ton bourreau,</l>
					<l n="24" num="3.8">Si j’évoquais soudain les morts de leur tombeau !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Vous, guerriers d’autrefois, sortez donc de la tombe,</l>
					<l n="26" num="4.2">Et ne permettez pas que la France succombe !</l>
					<l n="27" num="4.3">Vous, Bayard, Duguesclin, et Lahire, et Dunois,</l>
					<l n="28" num="4.4">Armez-vous du drapeau que portaient nos vieux rois ;</l>
					<l n="29" num="4.5">Jeanne d’Arc et Clisson, accourez aux batailles .</l>
					<l n="30" num="4.6">Et chassez l’ennemi debout sur nos murailles ;</l>
					<l n="31" num="4.7">Vous, Turenne et Condé, Catinat et Villars</l>
					<l n="32" num="4.8">Faites fuir sous vos coups ces bandes de pillards.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Point de Pape Léon, de Geneviève sainte</l>
					<l n="34" num="5.2">Qui de nos murs sacrés sauvegarde l’enceinte,</l>
					<l n="35" num="5.3">Qui fasse reculer ce moderne Attila</l>
					<l n="36" num="5.4">Que Dieu, pour nous punir, de nos jours appela.</l>
					<l n="37" num="5.5">De notre capitale il franchit la barrière,</l>
					<l n="38" num="5.6">Et point de bras vengeur qui le tienne en arrière,</l>
					<l n="39" num="5.7">Qui détourne de nous ce barbare oppresseur</l>
					<l n="40" num="5.8">Qui traîne sur ses pas le carnage et l’horreur !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Vous, les vaillants, les forts, vous qui vivez encore,</l>
					<l n="42" num="6.2">Vous que la gloire appelle et que l’honneur décore ;</l>
					<l n="43" num="6.3">Et vous, fiers Vendéens, vous, les fils des Croisés,</l>
					<l n="44" num="6.4">Que déjà cent combats ont immortalisés,</l>
					<l n="45" num="6.5">Laisserez-vous ainsi notre vieille patrie</l>
					<l n="46" num="6.6">Pencher vers sa ruine, et s’abimer flétrie,</l>
					<l n="47" num="6.7">Et ne ferez-vous point à ce noble pays</l>
					<l n="48" num="6.8">Quelque nouveau rempart couvert de vos débris ?</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">Mais vous, mon Dieu, mais vous, laisserez-vous la France</l>
					<l n="50" num="7.2">S’affaisser sous le poids de sa rude souffrance ?</l>
					<l n="51" num="7.3">N’a-t-elle point encore épuisé devant vous,</l>
					<l n="52" num="7.4">Tout son amer calice, et de votre courroux</l>
					<l n="53" num="7.5">Doit-elle ressentir les suites effroyables</l>
					<l n="54" num="7.6">Sans toucher votre cœur ? Ses accents lamentables</l>
					<l n="55" num="7.7">Sont montés jusqu’au Ciel, ne la repoussez pas,</l>
					<l n="56" num="7.8">Ouvrez-lui votre sein et tendez-lui les bras !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1">Pitié, mon Dieu, pitié ! Seigneur, pardon pour elle !</l>
					<l n="58" num="8.2">Rendez-lui, désormais, une vie immortelle.</l>
					<l n="59" num="8.3">Venez à son secours, vous, maître tout-puissant !</l>
					<l n="60" num="8.4">Elle s’est épurée des torrents de sang,</l>
					<l n="61" num="8.5">Et parmi les combats subissant leur martyre,</l>
					<l n="62" num="8.6">Ses héros invoquaient, dans leur pieux délire,</l>
					<l n="63" num="8.7">Le nom de votre Fils ; de leur mourante voix,</l>
					<l n="64" num="8.8">Ils priaient comme lui, tout en baisant sa croix.</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1">Et je priais ainsi, lorsque du sein des ombres</l>
					<l n="66" num="9.2">Qui couvraient mon pays, couché sur ses décombres ;</l>
					<l n="67" num="9.3">Le soleil s’élança, brillant et radieux,</l>
					<l n="68" num="9.4">Et j’aperçus là-haut, dans la splendeur des cieux,</l>
					<l n="69" num="9.5">Le Christ qui demandait notre grâce à son Père ;</l>
					<l n="70" num="9.6">Et la paix aussitôt, divine messagère,</l>
					<l n="71" num="9.7">Apparut à nos yeux, ravis de ce bonheur,</l>
					<l n="72" num="9.8">Conduisant par la main notre unique Sauveur.</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1">La France, sous ses lois, et forte, et rajeunie,</l>
					<l n="74" num="10.2">Reprenait, grâce lui, sa force et son génie ;</l>
					<l n="75" num="10.3">Reniant du passé les fatales erreurs,</l>
					<l n="76" num="10.4">Elle ne livrait plus sa vie aux empereurs,</l>
					<l n="77" num="10.5">A ces hideux Césars qui, montés sur le trône,</l>
					<l n="78" num="10.6">Sur leur front sans pudeur attachaient la couronne,</l>
					<l n="79" num="10.7">Dans la boue et le sang se vautraient à plaisir,</l>
					<l n="80" num="10.8">Et, nous faisant tuer, se gardaient de mourir.</l>
				</lg>
			</div>
		</body>
	</text>
</TEI>