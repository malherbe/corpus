<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" lang="FRA">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE LEGS D’UNE LORRAINE</title>
				<title type="medium">Édition électronique</title>
				<author key="THE">
					<name>
						<forename>André</forename>
						<surname>THEURIET</surname>
					</name>
					<date from="1833" to="1907">1833-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>72 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">THE_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE LEGS D’UNE LORRAINE</title>
						<author>ANDRÉ THEURIET</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k853927s.r=ANDR%C3%89%20THEURIET%20LE%20LEGS%20D%27UNE%20LORRAINE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE LEGS D’UNE LORRAINE</title>
								<author>ANDRÉ THEURIET</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n’ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2025-02-02" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.7.1.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader>
	<text>
		<body>

			<div type="poem" key="THE2">
				<head type="main">LE LEGS D’UNE LORRAINE</head>
				<lg n="1">
					<l n="1" num="1.1">Je me sens bien lasse et ne vivrai guère</l>
					<l n="2" num="1.2">Passé la moisson… Mon mal est trop fort,</l>
					<l n="3" num="1.3">Et ce que j’ai vu dans ces temps de guerre,</l>
					<l n="4" num="1.4">Enfant, m’a donné le coup de la mort.</l>
					<l n="5" num="1.5">Tu n’as pas dix ans, toi, mais à ton âge</l>
					<l n="6" num="1.6">Les yeux sont ouverts et l’on se souvient.</l>
					<l n="7" num="1.7">Je vais te montrer, petit, l’héritage</l>
					<l n="8" num="1.8">Trop lourd pour mes bras, et qui t’appartient.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Viens, allons d’abord vers ce champ de seigle .</l>
					<l n="10" num="2.2">Les nôtres y sont morts, assassinés</l>
					<l n="11" num="2.3">Par ces loups prussiens au front ceint d’un aigle ;</l>
					<l n="12" num="2.4">Là dorment ton père et tes deux aînés.</l>
					<l n="13" num="2.5">Ce qu’ils défendaient contre cette bande,</l>
					<l n="14" num="2.6">C’était leur maison, leur terre et la loi !</l>
					<l n="15" num="2.7">L’herbe sur leurs corps a poussé plus grande…</l>
					<l n="16" num="2.8">Regarde, mon fils, et rappelle-toi !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Viens dans ces prés verts, tout bordés d’aunée.</l>
					<l n="18" num="3.2">Là fut une ferme aux hôtes nombreux,</l>
					<l n="19" num="3.3">Et l’on y voyait encor l’autre année</l>
					<l n="20" num="3.4">Des vergers en fleur et des gens heureux…</l>
					<l n="21" num="3.5">Regarde à présent : seule, la couleuvre</l>
					<l n="22" num="3.6">Habite ces murs qu’a noircis le feu.</l>
					<l n="23" num="3.7">La Prusse a passé par là… Voici l’œuvre</l>
					<l n="24" num="3.8">De ceux qu’on nommait les soldats de Dieu.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Leur maître disait : « C’est à Bonaparte,</l>
					<l n="26" num="4.2">C’est à l’empereur que j’en veux… » Mais non !</l>
					<l n="27" num="4.3">Il voulait, vois-tu, rayer de la carte</l>
					<l n="28" num="4.4">Le peuple de France et son vieux renom ;</l>
					<l n="29" num="4.5">Et quand un matin, au fond des Ardennes,</l>
					<l n="30" num="4.6">L’Empire est tombé, honteux et honni,</l>
					<l n="31" num="4.7">Ils se sont rués comme des hyènes</l>
					<l n="32" num="4.8">Sur ce grand pays qu’ils croyaient fini.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Ils sont encor là, l’œil plein de menaces…</l>
					<l n="34" num="5.2">Leur odeur maudite imprègne nos seuils,</l>
					<l n="35" num="5.3">Leur musique joue au cœur de nos places,</l>
					<l n="36" num="5.4">Et leur rire épais insulte nos deuils.</l>
					<l n="37" num="5.5">Les voici, mon fils ! Parlons bas. — Écoute</l>
					<l n="38" num="5.6">Leur galop qui met la rue en émoi,</l>
					<l n="39" num="5.7">Et leurs sabres lourds traînant sur la route…</l>
					<l n="40" num="5.8">Écoute, regarde, et puis souviens-toi !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Souviens-toi !… Vois-tu cette longue file</l>
					<l n="42" num="6.2">De chariots poudreux et de voyageurs ?</l>
					<l n="43" num="6.3">C’est tout un village, enfant, qui s’exile</l>
					<l n="44" num="6.4">Pour ne pas manger le pain des vainqueurs.</l>
					<l n="45" num="6.5">Pauvres gens ! ils vont chercher la patrie</l>
					<l n="46" num="6.6">Loin des champs aimés où fut leur maison.</l>
					<l n="47" num="6.7">Regarde, et jamais que ton cœur n’oublie</l>
					<l n="48" num="6.8">Ce convoi qui fuit, triste, à l’horizon.</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">Mets ces souvenirs en toi comme un germe.</l>
					<l n="50" num="7.2">Le jour, au soleil ; la nuit, en rêvant,</l>
					<l n="51" num="7.3">Nourris-en ton âme et travaille… Enferme</l>
					<l n="52" num="7.4">Dans un corps de fer l’esprit d’un savant,</l>
					<l n="53" num="7.5">Afin que ton corps, comme ton courage,</l>
					<l n="54" num="7.6">Soit prêt pour le jour qui doit nous venger…</l>
					<l n="55" num="7.7">C’est mon legs, petit, c’est ton héritage,</l>
					<l n="56" num="7.8">Le seul que nous ait laissé l’étranger.</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1">Quand luira ce jour du réveil ?… Personne</l>
					<l n="58" num="8.2">Ne peut le savoir… Mais sûr, il viendra !</l>
					<l n="59" num="8.3">Des mers de Bretagne aux forêts d’Argonne</l>
					<l n="60" num="8.4">Un cri de colère alors montera…</l>
					<l n="61" num="8.5">Comme un jeune vin au fond des futailles,</l>
					<l n="62" num="8.6">Tous ces souvenirs en toi gronderont,</l>
					<l n="63" num="8.7">Et tu t’en iras aux grandes batailles,</l>
					<l n="64" num="8.8">La sagesse au cœur et l’audace au front.</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1">Nous ne verrons pas ce jour des revanches,</l>
					<l n="66" num="9.2">Nous ; nos yeux seront depuis longtemps clos,</l>
					<l n="67" num="9.3">Et depuis longtemps sur nos pierres blanches</l>
					<l n="68" num="9.4">Le vent secouera l’herbe des tombeaux ;</l>
					<l n="69" num="9.5">Mais nous entendrons votre cri de guerre,</l>
					<l n="70" num="9.6">Et quand, tout fumants d’un juste courroux,</l>
					<l n="71" num="9.7">Vous nous vengerez, au fond de la terre</l>
					<l n="72" num="9.8">Nos os dormiront d’un sommeil plus doux.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Juillet 1871.</date>
					</dateline>
				</closer>
			</div>

		</body>
	</text>
</TEI>