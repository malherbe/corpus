<div align="center">
<img src="Images/Projet/corpus_malherbe_3.svg" alt="Corpus Malherbe" />
</div>

<div align="center">
<center><h2>Corpus de textes versifiés au format XML-TEI</h2></center>
</div>

<div align="center">
![CC](https://img.shields.io/badge/licence-CC%20BY--NC--SA%203.0-red.svg?style=flat-square)
![CeCill](https://img.shields.io/badge/licence-CeCILL_2.1-red.svg?style=flat-square)
</div>

<div align="center">
![XML](https://img.shields.io/badge/XML-1.0-blue.svg?style=flat-square)
![XSLT](https://img.shields.io/badge/XSLT-2.0-blue.svg?style=flat-square)
![XML_Schema](https://img.shields.io/badge/XML Schema-1.0-blue.svg?style=flat-square)
![TEI](https://img.shields.io/badge/TEI-P5-yellow.svg?style=flat-square)
![Unicode](https://img.shields.io/badge/Unicode-UTF--8-yellowgreen.svg?style=flat-square)
![Bash](https://img.shields.io/badge/Bash-4.3-green.svg?style=flat-square)
![Python](https://img.shields.io/badge/Python-3.6-green.svg?style=flat-square)
![Java](https://img.shields.io/badge/Java-1.8-green.svg?style=flat-square)
</div>

##
Ce corpus de textes en vers a été constitué dans le cadre d'un projet de recherche initié
par Éliane Delente et Richard Renault en 2007 et ayant pour objet le traitement automatique des formes et structures métriques.

Responsable du corpus (conception, préparation et maintenance) : Richard Renault


CRISCO (EA 4255) - Université de Caen Normandie

site du projet : [métrique en ligne](https://crisco2.unicaen.fr/verlaine/)

<div align="center">

![partition](Documents/partition_corpus.svg )

</div>
